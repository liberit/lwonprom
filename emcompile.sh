#!/bin/bash -l
# export POCL_DEVICES="pthread"
#make clean
#rm library/*a
source /opt/emsdk/emsdk_env.sh
cp configure.ac.emscripten configure.ac
cp Makefile.am.emscripten Makefile.am
rm binary/*
make distclean
./autogen.sh || exit 1
emconfigure ./configure  || exit 1
EMCC_DEBUG=1 emmake make -j8  || exit 1
cd binary/ || exit 1
#for i in *wasm
#do
#  #mv "$i" "$i".bc
#EMCC_DEBUG=1 emcc  -s ASSERTIONS=1  "$i" -o "$i".js \
#  -s EXPORTED_FUNCTIONS='["_main"]' \
#  -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]' -s WASM=1 || exit 1
#EMCC_DEBUG=1 emcc  -s ASSERTIONS=1 "$i" -o "$i".html \
#  -s EXPORTED_FUNCTIONS='["_main"]' \
#  -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap"]' -s WASM=1 || exit 1
#done
