#!/bin/bash
sudo         apt-get update && \
sudo apt-get install -y --no-install-recommends \
          ocl-icd-opencl-dev\
          ocl-icd-libopencl1 \
          pocl-opencl-icd \
          opencl-headers \
          complexity \
          valgrind \
          automake \
          clang \
          make

