#!/bin/bash
if (( $(whereis clang-format | grep -c /)  == 0 )) ; 
   then echo "clan-format is missing"; exit 1;
   fi
for file in program/main.c program/interpret.c program/interpret.h \
  program/sort.c program/lwonprom.c program/lwonprom.cl
do
clang-format "$file" > /tmp/format
cat /tmp/format > "$file"
done
