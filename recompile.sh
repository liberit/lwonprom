#!/bin/bash
#make clean
#./autogen.sh || exit 1
#./configure || exit 1
ctags -R program/
rm binary/*
rm library/library*a
make -j$(nproc) || exit 1 
export POCL_DEVICES="pthread"
#binary/lwonprom || exit 1
./binary/kwim 2>&1 | cat > quiz.log
make check  | tee make_check.log || (cat quiz.log | grep not.ok -B 15 && exit 1;)
grep FAIL make_check.log
grep XPASS make_check.log

