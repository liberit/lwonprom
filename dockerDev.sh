#!/bin/bash
# refresh repo
# build docker images
docker build -t lwonprom-dev -f docker/dev.docker docker
echo " COMPILING  --------------------------"
docker run \
  --cap-add=SYS_PTRACE --security-opt seccomp=unconfined \
  --mount type=bind,source="$PWD",target=/srv/lwonprom \
  -u $(id -u):$(id -g) \
  -ti lwonprom-dev /bin/bash 
