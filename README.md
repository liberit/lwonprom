
### Requirements 

Clang is required (wont work with GCC). 
Needs OpenCL PoCL recommended.

### Dependencies
```
apt-get update && apt-get install -y --no-install-recommends ocl-icd-opencl-dev\
          ocl-icd-libopencl1 opencl-headers automake clang make complexity\
          valgrind
```


### Installation

After unzipping or cloning.
In a command line shell:

```
git clone https://gitlab.com/liberit/lwonprom.git
cd lwonprom
./compile.sh
```

Can run through all the tests and you'll end up in the line interpreter.

```
make check
```

### Example commands

```
hyikdoyu hyikdoka plustu
```

means "increase the number one by the number one!" or more literally 
"increase (plus) the number (do ka) one (hyik) by (yu) the number (do) one
(hyik) ! (tu)"

The sentence is translated to an bytecode encoding,
```
0x(0149 881D 0001 287E  881D 0001 245E 8A64 295E)
```

Which is taken by the sentence interpreter,
because it ends with a "tu" it is then taken by the deontic-mood interpreter,
which generates a hash based on the cases, types and verb "doyu doka plustu",
and matches it to a base command. the base command then takes the values out of
the statement and makes the resulting statement.

the result it should give you is

```
tyutdoka li
```
which means "the number two."

it can also be written as "tyut do ka li" the literal gloss being "two _number
_accusative_case _realis_mood."

### Documentation

The majority of the documentation is in documentation/pyac.pdf

### Translation

If you come across a Pyac word,
can do a quick lookup using 

```
./pyacpwih.sh word
```
