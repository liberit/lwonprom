#!/bin/bash
# refresh repo
sudo rm -Rf docker/lwonprom
git clone -b dev . docker/lwonprom
# build docker images
# docker build -t lwonprom-compile -f docker/compile.docker docker
docker build -t lwonprom-emcompile -f docker/emcompile.docker docker
#docker build -t lwonprom-quiz -f docker/quiz.docker docker
docker build -t lwonprom-emquiz -f docker/emquiz.docker docker
echo " COMPILING  --------------------------"
#sudo docker run \
#  --mount type=bind,source="$PWD/docker/lwonprom",target=/srv/lwonprom \
#  -ti lwonprom-compile || exit 1
docker run \
  --mount type=bind,source="$PWD/docker/lwonprom",target=/srv/lwonprom \
  -ti lwonprom-emcompile || exit 1
#  -u $(id -u):$(id -g) \
echo " TESTING  --------------------------"

#sudo docker run \
#  --mount type=bind,source="$PWD/docker/lwonprom",target=/srv/lwonprom \
#  -ti lwonprom-quiz || exit 1
#
docker run \
  --mount type=bind,source="$PWD/docker/lwonprom",target=/srv/lwonprom \
  -ti lwonprom-emquiz || exit 1
#  -u $(id -u):$(id -g) \
