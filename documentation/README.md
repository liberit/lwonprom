
### Requirements 

Clang is required (wont work with gcc). 
Needs OpenCL PoCL recommended.

### Installation

After unzipping or cloning.
In a command line shell:

```
cd lwonprom
./quiz.sh
```

### Documentation

The majority of the documentation is in documentation/pyac.pdf
