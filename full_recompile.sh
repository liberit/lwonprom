#!/bin/bash
make distclean 
./autogen.sh || exit 1
./configure || exit 1
make -j8 || exit 1 
make check || exit 1
