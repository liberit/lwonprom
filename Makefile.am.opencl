#SPEL virtual machine
#Copyright (C) 2016  Logan Streondj
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#contact: streondj at gmail dot com
#
SUBDIRS = .

noinst_LIBRARIES =  library/library_seed.a \
                    library/library_sort.a \
                    library/library_math.a \
                    library/library_dictionary.a \
                    library/library_analyst.a \
                    library/library_parser.a \
                    library/library_encoding.a \
                    library/library_translation.a \
                    library/library_dialogue.a \
		    library/library_pyash.a \
		    library/library_interpret.a \
		    library/library_lwonprom.a \
		    library/library_compile.a \
		    library/library_linenoise.a \
                    library/library_random.a \
                    library/library_quiz.a \
		    library/library_genericParallel.a


library_library_seed_a_SOURCES = program/pyash.h program/seed.h program/seed.c \
				 program/analyst.h program/analyst.c  \
				 program/pyash.c
library_library_seed_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)


library_library_parser_a_SOURCES =  program/sort.h program/parser.c \
                                       program/parser.h \
	program/translation.c
library_library_math_a_SOURCES =  program/sort.h program/math.c \
                                       program/math.h 

library_library_parser_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)
#\
 #        -Wstack-usage=256 # data bytes per warp

#library_library_genericOpenCL_a_SOURCES = program/genericOpenCL.c program/genericOpenCL.h
#library_library_genericOpenCL_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)
#      #   -Wstack-usage=256 # data bytes per warp

#library_library_simpleCL_a_SOURCES = program/simple-opencl/simpleCL.c program/simple-opencl/simpleCL.h
#library_library_simpleCL_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)
#      #   -Wstack-usage=256 # data bytes per warp

library_library_linenoise_a_SOURCES = program/linenoise/linenoise.c program/linenoise/linenoise.h
library_library_linenoise_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)
#      #   -Wstack-usage=256 # data bytes per warp

library_library_dialogue_a_SOURCES = program/dialogue.c \
                program/dialogue.h
library_library_dialogue_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)

library_library_lwonprom_a_SOURCES = program/lwonprom.c program/parser.c \
                program/lwonprom.h  program/sort.h program/sort.c
library_library_lwonprom_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)

library_library_sort_a_SOURCES = program/sort.c \
                program/sort.h
library_library_sort_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)

library_library_pyash_a_SOURCES = program/pyash.c \
                program/pyash.h
library_library_pyash_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)


library_library_compile_a_SOURCES = program/compile.c \
                program/compile.h
library_library_compile_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)

library_library_interpret_a_SOURCES = program/interpret.c \
                program/interpret.h program/compile.h program/sort.h \
		program/compile.c program/translation.c program/math.c \
		program/prng.c program/encoding.c program/seed.c \
		program/analyst.c program/found.c program/compare.c \
		program/parser.c program/pyash.h program/pyash.c \
		program/sort.c
library_library_interpret_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)
library_library_interpret_a_LIBADD =  library/library_compile.a  \
				      library/library_math.a \
				      library/library_translation.a 



library_library_analyst_a_SOURCES = program/analyst.c \
                program/analyst.h program/sort.h 
library_library_analyst_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)

library_library_translation_a_SOURCES = program/prng.c program/translation.c \
		program/math.c  program/sort.c program/sort.h
library_library_translation_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)

library_library_encoding_a_SOURCES = program/encoding.h \
                program/encoding.c program/sort.h program/sort.c \
                program/analyst.h program/analyst.c program/pyash.c \
		program/pyash.h program/parser.h program/parser.c \
		program/seed.c 
library_library_encoding_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)


library_library_dictionary_a_SOURCES = program/dictionary.c \
                program/dictionary.h
library_library_dictionary_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)#\
     #    -Wstack-usage=32 # data bytes per warp

#library_library_programmer_a_SOURCES = program/machine_programmer/programmer.c
#library_library_programmer_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)\
#         -Wstack-usage=256 # data bytes per warp

library_library_random_a_SOURCES = program/prng.c
library_library_random_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS)
#         -Wstack-usage=256 # data bytes per warp

library_library_genericParallel_a_SOURCES = program/genericOpenCL.c  
library_library_genericParallel_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS) 


library_library_quiz_a_SOURCES = program/quiz.c  \
  program/compare.c program/translation.c program/prng.c \
  program/parser.c program/dialogue.c program/seed.c program/sort.c \
  program/dictionary.c program/encoding.c program/genericOpenCL.c \
  program/pyash.c program/lwonprom.c program/found.c program/compile.c \
  program/sort.h program/sort.c program/pyash.h 
# program/json.c
library_library_quiz_a_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS) 
#library_library_quiz_a_LIBADD =  

# vm 
#bin_PROGRAMS = binary/lwonprom binary/tfik binary/kwim
bin_PROGRAMS = binary/tfik binary/kwim

#binary_lwonprom_SOURCES = program/main.c  program/sort.c program/sort.h \
#			  program/parser.c program/pyash.h program/pyash.c
#binary_lwonprom_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS) 
#binary_lwonprom_LDADD =  library/library_seed.a \
#          library/library_dictionary.a  \
#        library/library_dialogue.a \
# 	library/library_encoding.a \
#	library/library_translation.a \
#	library/library_random.a \
#	library/library_genericParallel.a \
#	library/library_interpret.a \
#	library/library_quiz.a


#          library/library_simpleCL.a 
#library/library_genericOpenCL.a 
binary_tfik_SOURCES = program/ksiktfikge.c program/pyash.c program/sort.c \
		      program/parser.c program/translation.c program/encoding.c \
		      program/analyst.c program/found.c program/compile.c \
		      program/quiz.c program/interpret.c program/compare.c \
	              program/prng.c  program/math.c  program/lwonprom.c  \
		      program/linenoise/linenoise.c
binary_tfik_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS) 
binary_tfik_LDADD =  library/library_seed.a \
          library/library_dictionary.a  \
        library/library_dialogue.a \
	library/library_lwonprom.a \
	library/library_random.a \
	library/library_genericParallel.a \
	library/library_interpret.a \
	library/library_linenoise.a 


binary_kwim_SOURCES = program/kwim.c  program/sort.c \
	program/parser.c program/pyash.c program/found.c \
	program/quiz.c program/interpret.c program/compare.c \
	program/prng.c  program/math.c program/compile.c \
	program/analyst.c program/lwonprom.c  program/translation.c \
	program/encoding.c

# program/json.c
binary_kwim_CFLAGS = -I$(top_srcdir) $(MY_CFLAGS) 
binary_kwim_LDADD =  \
	library/library_genericParallel.a \
	library/library_linenoise.a \
	library/library_encoding.a \
	library/library_lwonprom.a \
	library/library_translation.a


# TESTS
TEST_LOG_DRIVER = env AM_TAP_AWK='$(AWK)' $(SHELL) \
               $(top_srcdir)/tap-driver.sh
TESTS = quiz.test complexity.test
EXTRA_DIST = $(TESTS)
#check_PROGRAMS = check_vm
#check_vm_SOURCES = check/check_ACC_vm.c \
#    $(top_builddir)/program/seed/seed.h
#check_vm_CFLAGS = @CHECK_CFLAGS@
#check_vm_LDADD = $(top_builddir)/library/library_seed.a @CHECK_LIBS@
