#!/bin/bash
export POCL_DEVICES="pthread"
#make clean
#rm library/*a
cp configure.ac.opencl configure.ac
cp Makefile.am.opencl Makefile.am
./autogen.sh || exit 1
./configure || exit 1
make -j$(nproc) || exit 1
