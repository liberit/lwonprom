/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*SPEL machine programmer
Copyright (C) 2016  Logan Streondj

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact: streondj at gmail dot com
*/
#include "dialogue.h"
#include "seed.h"
#include <assert.h>

// translation dictionary and code file be input
// language text file be produce

// parallel algorithm begin
//  produce buffer evenly spaced, 32 bytes (maximum foreign word length) times
//  vector long (16), times number of vectors.
//  use maximum that will fit in cache
//
// language unique allotment _nom begin _rea
//    each worker _nom single independentClause _acc processing _rea
//      number of code words to be translated counter
//      index of applicable words, quoted treated aside.
//    each translation dictionary code _nom each code word _acc comparison _rea
//      after each successful translation decrement code words to be translated
//        counter.
//    if code word remains counter equals zero then skip to next word if
//    available.
//    if same then put translation into produce buffer
// language unique allotment _nom done _rea
//
//  after complete run a script to transform produce buffer into plain text.
//  write to file or stdout
//
//

// translation dictionary and code file be input
// language text file be produce

// parallel algorithm begin
//  produce buffer evenly spaced, 32 bytes (maximum foreign word length) times
//  vector long (16), times number of vectors.
//  use maximum that will fit in cache
//
// language unique allotment _nom begin _rea
//    each worker _nom single independentClause _acc processing _rea
//      number of code words to be translated counter
//      index of applicable words, quoted treated aside.
//    each translation dictionary code _nom each code word _acc comparison _rea
//      after each successful translation decrement code words to be translated
//        counter.
//    if code word remains counter equals zero then skip to next word if
//    available.
//    if same then put translation into produce buffer
// language unique allotment _nom done _rea
//
//  after complete run a script to transform produce buffer into plain text.
//  write to file or stdout
//
//
////////////////////////////////////////////
///  code

// translation dictionary and code file be input
// language text file be produce
//

void dictionary_database_word_found(const uint32_t dictionary_long,
                                    const char *dictionary,
                                    const uint16_t code_word,
                                    uint8_t *word_long, char *word) {
  assert(dictionary_long > 0);
  assert(dictionary != NULL);
  assert(code_word != 0);
  assert(word_long != NULL);
  assert(*word_long >= MAXIMUM_FOREIGN_WORD_LONG);
  assert(word != NULL);
  uint32_t dictionary_indexFinger = 0;
  uint16_t dictionary_code_word = 0;
  uint8_t word_indexFinger = 0;
  char letter = (char)0;
  for (; dictionary_indexFinger < dictionary_long;
       dictionary_indexFinger += DICTIONARY_LINE_LONG) {
    // match to first uint16_t of dictionary line, if no match, skip to next
    // line.
    dictionary_code_word =
        (uint16_t)((uint8_t)dictionary[dictionary_indexFinger] |
                   (uint16_t)(dictionary[dictionary_indexFinger + 1] << 8));
    if (dictionary_code_word == code_word) {
      // printf("%s:%d code_word 0x%X\n ",__FILE__, __LINE__, code_word);
      // found the definition, return the foreign word
      for (word_indexFinger = 0; word_indexFinger < MAXIMUM_FOREIGN_WORD_LONG;
           ++word_indexFinger) {
        letter =
            dictionary[dictionary_indexFinger + CODE_LONG + word_indexFinger];
        if (letter != ' ' && letter != '\n') {
          word[word_indexFinger] = letter;
          // printf("%c", letter);
        } else {
          *word_long = word_indexFinger;
          break;
        }
      }
      break;
    }
  }
}
// parallel algorithm begin
void code_file_translation(const uint32_t dictionary_long,
                           const char *dictionary, const uint32_t code_long,
                           const line_t *code, uint32_t *produce_pad_long,
                           char *produce_pad) {
  //  produce buffer evenly spaced, maximum foreign word length (30 bytes) times
  //  vector long (16), times number of vectors.
  //  use maximum that will fit in cache
  assert(produce_pad_long != NULL);
  assert(*produce_pad_long >=
         MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * code_long);
  assert(dictionary_long > 0);
  assert(dictionary != NULL);
  assert(code_long > 0);
  assert(code != NULL);
  assert(produce_pad != NULL);
  // parallel egg import
  // parallel egg fertilize
  // parallel seed birth
  // parallel children observation
  // parallel children discharge
  // language unique allotment _nom begin _rea
  //    each worker _nom single independentClause _acc processing _rea
  //    translate one phrase at a time
  uint8_t code_indexFinger = 0;
  uint8_t tablet_indexFinger = 0;
  uint8_t vector_long = 0;
  uint8_t scalar_thick = 0;
  uint16_t code_word = 0;
  uint16_t indicator_list = 0;
  uint8_t indicator = 0;
  line_t tablet = {0};
  uint8_t quote_long = 0;
  uint8_t word_long = MAXIMUM_FOREIGN_WORD_LONG;
  uint32_t pad_indexFinger = 0;
  for (code_indexFinger = 0; code_indexFinger < code_long; ++code_indexFinger) {
    tablet = code[code_indexFinger];
    indicator_list = (uint16_t)tablet.s0;
    indicator = indicator_list & 1;
    for (tablet_indexFinger = 1; tablet_indexFinger < LINE_LONG;
         ++tablet_indexFinger) {
      code_word = line_t_read(tablet_indexFinger, tablet);
      if (((indicator_list >> (tablet_indexFinger - 1)) & 1) == indicator &&
          (code_word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) {
        // is at a potential quote place
        // find quote length
        vector_long = (uint8_t)vector_long_translate(
            (code_word & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN);
        scalar_thick = (code_word & SCALAR_THICK_MASK) >> SCALAR_THICK_BEGIN;
        printf("%s:%d scalar_thick 0x%X, vector_long 0x%X\n", __FILE__,
               __LINE__, scalar_thick, vector_long);
        quote_long = scalar_thick * vector_long;
        // translate quote
        // put indexFinger after the quote
        tablet_indexFinger += quote_long;
        printf(
            "%s:%d check if quote_long exceeds end of tablet, code_long 0x%X\n",
            __FILE__, __LINE__, code_long);
      }
      if (code_word == 0)
        break;
      // if (((indicator_list >> tablet_indexFinger) & 1) == indicator) {
      // is end of a phrase
      // translate it look it up in the dictionary  database
      // printf(
      //    "%s:%d code_word 0x%X\n",
      //    __FILE__, __LINE__, code_word);
      dictionary_database_word_found(dictionary_long, dictionary, code_word,
                                     &word_long, produce_pad + pad_indexFinger);
      printf("%s:%d word_long 0x%X word %s\n", __FILE__, __LINE__, word_long,
             produce_pad + pad_indexFinger);
      // translate words or quote in phrase
      //}

      //    each translation dictionary code _nom each code word _acc comparison
      //    _rea
      //      after each successful translation decrement code words to be
      //      translated
      //        counter.
      //    if code word remains counter equals zero then skip to next word if
      //    available.
      //    if same then put translation into produce buffer
      pad_indexFinger += MAXIMUM_FOREIGN_WORD_LONG;
      word_long = MAXIMUM_FOREIGN_WORD_LONG;
    }
  }
  // language unique allotment _nom done _rea
  //
  //
}

void pad_text_adaptation(const uint32_t produce_pad_long,
                         const char *produce_pad, uint32_t *produce_text_long,
                         char *produce_text) {
  assert(produce_pad_long > 0);
  assert(produce_pad != NULL);
  assert(produce_text_long != NULL);
  assert(produce_text != NULL);
  assert(*produce_text_long >= produce_pad_long);
  uint32_t pad_indexFinger = 0;
  const uint32_t max_produce_text_long = *produce_text_long;
  uint32_t produce_indexFinger = 0;
  char stream_letter = (char)0;
  uint32_t retrospective_letter_indexFinger = 0;
  for (; pad_indexFinger < produce_pad_long; ++pad_indexFinger) {
    stream_letter = produce_pad[pad_indexFinger];
    if (stream_letter != 0) {
      if (pad_indexFinger - retrospective_letter_indexFinger > 1) {
        produce_text[produce_indexFinger] = ' ';
        ++produce_indexFinger;
      }
      retrospective_letter_indexFinger = pad_indexFinger;
      produce_text[produce_indexFinger] = stream_letter;
      ++produce_indexFinger;
    }
  }
  assert(produce_indexFinger < max_produce_text_long);
  produce_text[produce_indexFinger] = (char)0;
  *produce_text_long = produce_indexFinger;
}

void gross_code_line_line_t_code_translation(const uint32_t gross_code_long,
                                            const char *gross_code,
                                            line_t *code) {
  assert(gross_code_long >= LINE_BYTE_LONG);
  assert(code != NULL);
  uint16_t code_word = 0;
  uint8_t code_indexFinger = 0;
  for (; code_indexFinger < LINE_LONG; ++code_indexFinger) {
    code_word =
        (uint16_t)((uint8_t)gross_code[code_indexFinger * 2] |
                   ((uint16_t)gross_code[code_indexFinger * 2 + 1]) << 0x8);
    // printf("%s:%d gross_code0 0x%X, gross_code1 0x%X, code_word 0x%X.\n ",
    // __FILE__, __LINE__, (uint8_t) gross_code[code_indexFinger *2], (uint16_t)
    // gross_code[code_indexFinger *2 +1] << 0x8, code_word);
    line_t_write(code_indexFinger, code_word, code);
  }
}
void gross_code_line_t_code_translation(const uint32_t gross_code_long,
                                       const char *gross_code,
                                       uint16_t *code_long, line_t *code) {
  // translate each line
  assert(gross_code_long > 0);
  assert(gross_code_long % LINE_BYTE_LONG == 0);
  assert(gross_code != NULL);
  assert(code_long != NULL);
  assert(*code_long >= gross_code_long / LINE_BYTE_LONG);
  assert(code != NULL);
  uint32_t gross_code_indexFinger = 0;
  uint16_t code_indexFinger = 0;
  for (; gross_code_indexFinger < gross_code_long;
       gross_code_indexFinger += LINE_BYTE_LONG) {
    gross_code_line_line_t_code_translation(
        gross_code_long - gross_code_indexFinger,
        gross_code + gross_code_indexFinger, code + code_indexFinger);
    ++code_indexFinger;
  }
  *code_long = code_indexFinger;
}

int main(int argument_count, const char *argument_sequence[]) {
  // get input args
  if (argument_count < 2) {
    printf("no dictionary filename\n");
    return 1;
  } else if (argument_count < 3) {
    printf("no code filename\n");
    return 2;
  }
  const char *dictionary_filename = argument_sequence[1];
  const char *code_filename = argument_sequence[2];
  // find
  //  produce buffer evenly spaced, 32 bytes (maximum foreign word length) times
  //  tablet long (16), times number of vectors.
  //  use maximum that will fit in cache  (Ryzen cache best under 4MiB, l2 cache
  //  under 512KiB)
  char produce_pad[MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * PAGE_LONG] =
      {0};
  uint32_t produce_pad_long =
      MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * PAGE_LONG;
  //  after complete, run a script to transform produce buffer into plain text.
  char produce_text[MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * PAGE_LONG] =
      {0};
  uint32_t produce_text_long =
      MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * PAGE_LONG;

  // load dictionary from file
  char dictionary[DICTIONARY_DATABASE_LONG] = {0};
  const uint32_t max_dictionary_long = DICTIONARY_DATABASE_LONG;
  uint32_t dictionary_long = max_dictionary_long;
  dictionary_long = text_file_read(dictionary_filename, dictionary_long, dictionary);
  // load code from file into line_t
  char gross_code[PAGE_LONG * LINE_BYTE_LONG] = {0};
  const uint32_t max_gross_code_long = PAGE_LONG * LINE_BYTE_LONG;
  uint32_t gross_code_long = max_gross_code_long;
  gross_code_long = text_file_read(code_filename, gross_code_long, gross_code);

  printf("%s:%d:%s %s filename, gross_code_long 0x%X, gross_code:\n %s",
         __FILE__, __LINE__, __FUNCTION__, code_filename, gross_code_long,
         gross_code);
  // convert gross code to line_t array
  line_t code[PAGE_LONG] = {0};
  uint16_t code_long = PAGE_LONG;
  gross_code_line_t_code_translation(gross_code_long, gross_code, &code_long,
                                    code);

  code_file_translation(dictionary_long, dictionary, code_long, code,
                        &produce_pad_long, produce_pad);

  pad_text_adaptation(produce_pad_long, produce_pad, &produce_text_long,
                      produce_text);
  printf("%s\n", produce_text);
  //  write to file or stdout
}
