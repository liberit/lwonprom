/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/* AGPL-3.0
 * LiberIT
 * */
#define DEBUGING
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "dialogue.h"
#include "encoding.h"
#ifndef EMSCRIPTEN
#include "genericOpenCL.h"
#endif
#include "interpret.h"
#include "lwonprom.h"
#include "prng.h"
#include "pyashWords.h"
#include "sort.h"
#include "translation.h"
#include "quiz.h"

#define CARDINAL

void champion_print(line_t *champion, uint32_t health, uint16_t generation,
                    uint16_t champion_generation) {
  uint8_t program_long = program_long_diagnose(PERSON_LONG, champion);
  printf("%d is champion_generation\n ", champion_generation);
  tablet_print(program_long, champion);
  printf(" is champion, 0x%X is health  after %d generations of compression\n",
         health, generation - champion_generation);
  char dictionary_filename[] = "quiz/en.kwon";
  char dictionary[DICTIONARY_DATABASE_LONG] = {0}; /* Flawfinder: ignore */
  const uint32_t max_dictionary_long = DICTIONARY_DATABASE_LONG;
  uint32_t dictionary_long = max_dictionary_long;
  dictionary_long = text_file_read(dictionary_filename, dictionary_long, dictionary);
  uint32_t produce_pad_long =
      MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * PAGE_LONG;
  char /* Flawfinder: ignore */ produce_pad[MAXIMUM_FOREIGN_WORD_LONG *
                                            LINE_LONG * PAGE_LONG] = {
      0};
  code_file_translation(dictionary_long, dictionary, program_long, champion,
                        &produce_pad_long, produce_pad);
  printf("champion in English:\n %s\n", produce_pad);
}

void file_encoding(char *file_abl, char *file_dat) {
  uint32_t recipe_text_magnitude = MAXIMUM_PAPER_LONG;
  char recipe_text[MAXIMUM_PAPER_LONG] = ""; /* Flawfinder: ignore */
  uint16_t recipe_long = 0xFF;
  line_t recipe[0xFF] = {0};
  uint16_t text_remainder = 0;
  recipe_text_magnitude = text_file_read(file_abl, recipe_text_magnitude, recipe_text);
  assert(recipe_text_magnitude <= 0xFFFF);
  recipe_long = old_text_encoding((uint16_t)recipe_text_magnitude, recipe_text, recipe_long,
                recipe, &text_remainder);
  paper_write(file_dat, 0, recipe_long * sizeof(line_t), (char *)&recipe);
}

#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
void file_c_encoding(char *file_abl, char *file_dat) {
  uint32_t recipe_text_magnitude = MAXIMUM_PAPER_LONG;
  char recipe_text[MAXIMUM_PAPER_LONG] = "";  /* Flawfinder: ignore */
  char produce_text[MAXIMUM_PAPER_LONG] = ""; /* Flawfinder: ignore */
  const uint16_t max_produce_text_long = MAXIMUM_PAPER_LONG;
  uint16_t recipe_long = 0xFF;
  line_t recipe[0xFF] = {0};
  uint16_t text_remainder = 0;
  recipe_text_magnitude = text_file_read(file_abl, recipe_text_magnitude, recipe_text);
  recipe_long = old_text_encoding((uint16_t)recipe_text_magnitude, recipe_text, recipe_long,
                recipe, &text_remainder);
  uint8_t recipe_iterator = 0;
  uint8_t person_iterator = 0;
  uint16_t produce_indexFinger = 0;
  char open_braces[] = "{";
  uint8_t open_braces_long = (uint8_t)stringlen(open_braces, 2);
  produce_indexFinger += text_copy(open_braces_long, open_braces,
                                   max_produce_text_long - produce_indexFinger,
                                   produce_text + produce_indexFinger);
  for (recipe_iterator = 0; recipe_iterator < recipe_long; ++recipe_iterator) {
    produce_indexFinger +=
        text_copy(1, "{", max_produce_text_long - produce_indexFinger,
                  produce_text + produce_indexFinger);
    for (person_iterator = 0; person_iterator < V16_LONG; ++person_iterator) {
      produce_indexFinger +=
          form_print(max_produce_text_long - produce_indexFinger,
                     produce_text + produce_indexFinger, "0x%X",
                     line_t_read(person_iterator, recipe[recipe_iterator]));
      if (person_iterator + 1 != V16_LONG) {
        produce_indexFinger +=
            form_print(max_produce_text_long - produce_indexFinger,
                       produce_text + produce_indexFinger, ", ");
      }
    }
    if (recipe_iterator + 1 == recipe_long) {
      produce_indexFinger +=
          form_print(max_produce_text_long - produce_indexFinger,
                     produce_text + produce_indexFinger, "}};\n");
    } else {
      produce_indexFinger +=
          form_print(max_produce_text_long - produce_indexFinger,
                     produce_text + produce_indexFinger, "},\n");
    }
    // DEBUGPRINT("0x%X produce_indexFinger\n", produce_indexFinger);
  }
  assert(produce_indexFinger < max_produce_text_long);
  paper_write(file_dat, 0, produce_indexFinger, produce_text);
  // assert(1== 0);
}
#endif

int main() {
  int result = cardinal_quiz();
  if (result != 0) return result;

#define TRAINING_SEQUENCE_LONG 11
  const uint16_t training_sequence_long = TRAINING_SEQUENCE_LONG;
  const line_t training_sequence[TRAINING_SEQUENCE_LONG][2] = {
      //{'a', 'A'}, {'b', 'B'}, {'z', 'Z'}}; // return input
      {'A', 'a'}, {'B', 'b'},   {'Z', 'z'},   {'D', 'd'},
      {'C', 'c'}, {0x10, 0x10}, {0x30, 0x30}, {'@', '@'},
      {'[', '['}, {'z', 'z'}}; // return input
  line_t program_produce[TRAINING_SEQUENCE_LONG] = {0};

#define RECIPE_LONG 1
  const uint16_t recipe_long = RECIPE_LONG;
  const line_t recipe[RECIPE_LONG] = {
      // {0x529, 0x881D, 0x40, 0x4127, 0xE868, 0x29BE, 0x881D, 0x1, 0x287E,
      // 0x8C64, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0},
      {0xA529, 0x881D, 0x1, 0x4127, 0xE868, 0x29BE, 0x881D, 0x21, 0x4127,
       0xEA10, 0x29BE, 0x881D, 0x1, 0x287E, 0x8C64, 0x217E}};
  //{0x529, 0x881D, 0x5, 0x4127, 0xEA10, 0x29BE, 0x881D, 0x3, 0x287E, 0x8C64,
  // 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0},
  //{0x29, 0x881D, 0x1, 0x287E, 0x8C64, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  // 0x0, 0x0, 0x0, 0x0}};
  //{0x529, 0x881D, 0x1, 0x4127, 0xE868, 0x29BE, 0x881D, 0x1, 0x287E, 0xABD4,
  // 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0},
  //{0x29, 0x881D, 0x1, 0x287E, 0xABD4, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  // 0x0, 0x0, 0x0, 0x0},
  //{0x5, 0xAAD2, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  // 0x0, 0x0}};
  //{
  //      {0x29, 0x881D, 0x1, 0x287E, 0x8C64, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0,
  //      0x0,
  //       0x0, 0x0, 0x0, 0x0},
  //      {0x29, 0x881D, 0x1, 0x287E, 0xABD4, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0,
  //      0x0,
  //       0x0, 0x0, 0x0, 0x0},
  //      {0x5, 0xAAD2, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
  //      0x0,
  //       0x0, 0x0, 0x0}};
  //
  // {
  //     {0x5, plus_WORD, realis_mood_GRAMMAR},
  //     {0x5, return_WORD, realis_mood_GRAMMAR},
  //     {0x5, invert_WORD, realis_mood_GRAMMAR}};

  file_encoding("quiz/sric.txt", "quiz/sric.byin");

#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
  file_c_encoding("quiz/sric.txt", "quiz/sric.c");
#endif

  const uint8_t population_long = POPULATION_LONG;
  line_t population[POPULATION_LONG][PERSON_LONG] = {{0}};
  line_t fresh_population[POPULATION_LONG][PERSON_LONG] = {{0}};
  uint32_t population_health[POPULATION_LONG] = {0};
  const uint8_t max_program_long = PERSON_LONG;

  // uint64_t random_seed = (uint64_t) time(NULL);
  uint64_t random_seed[2] = {0};
  random_seed_establish((uint64_t)time(NULL), 2, random_seed);
  // srand((unsigned int)random_seed[0]);
  printf("0x%lX  random_seed\n", random_seed[0]);
//  uint64_t champion_iteration_sequence[POPULATION_LONG] = {0};
//  // getInfo();
#ifndef EMSCRIPTEN
  sclHard hardware;
// sclGetHardware(0, &hardware);
  centre_hardware_grab(&hardware, 0);
  sclSoft software;
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom",
                           &software);
//  ///* Set OpenCL Kernel Parameters */
//  // ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobj);
//
//  ///* Execute OpenCL Kernel */
//  // size_t global_item_size = 4;
//  // size_t local_item_size = 4;
//  //  int result = 0;
//  //	result = clEnqueueNDRangeKernel(hardware.queue, software.kernel, 1,
//  // NULL,
//  //	&global_item_size, &local_item_size, 0, NULL, NULL);
//  //  result = parallel_program_begin(hardware, software);
//  //
//  ///* Copy results from the memory buffer */
//  // ret = clEnqueueReadBuffer(command_queue, memobj, CL_TRUE, 0,
//  // MEM_SIZE * sizeof(char),string, 0, NULL, NULL);
#endif 
//
//  evolutionary_island_establish(random_seed, recipe_long, recipe,
//                                max_program_long, training_sequence_long,
//                                training_sequence, program_produce,
//                                population_long, population, fresh_population,
//                                population_health, champion_iteration_sequence);
//  random_seed_establish((uint64_t)time(NULL), 2, random_seed);
//  evolutionary_island_establish(random_seed, recipe_long, recipe,
//                                max_program_long, training_sequence_long,
//                                training_sequence, program_produce,
//                                population_long, population, fresh_population,
//                                population_health, champion_iteration_sequence);
  return 0;
}
