/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "found.h"

/** finds the previous  */
uint16_t /*independentClause_long*/
retrospective_independentClause_by_perspective_found(
    const uint16_t tablet_long, const line_t tablet[],
    const uint16_t perspective, uint16_t *independentClause_begin) {
  guarantee(independentClause_begin != NULL);
  uint16_t independentClause_final = 0;
  uint16_t perspective_indexFinger, comparison_perspective = 0;
  down_repeat(
      tablet_long, if (finalPage_ri(tablet[iterator - 1]) == truth_WORD) {
        perspective_indexFinger =
            final_perspective_found((uint16_t)iterator, tablet);
        comparison_perspective =
            tablet_read(perspective_indexFinger, (uint8_t)iterator, tablet);
        if (comparison_perspective == perspective) {
          independentClause_final = (uint16_t)iterator;
          break;
        }
      });
  uint16_t htin_begin = 0;
  return retrospective_independentClause_long_found(independentClause_final,
                                                    tablet, &htin_begin);
}

uint16_t /*phrase_long*/ final_phrase_found(const uint16_t begin_indexFinger,
                                            const uint16_t input_long,
                                            const line_t input[],
                                            uint16_t *ending_indexFinger) {
  uint16_t grammar_word = 0;
  uint16_t sequence_indexFinger = 0;
  uint16_t indexFinger = begin_indexFinger;
  uint16_t grammaticalCase_indexFinger = indexFinger;
  uint16_t phrase_long = 0;
  uint16_t phrase_end = 0;
  uint16_t phrase_begin = 0;
  // DEBUGPRINT(("0x%X indexFinger, 0x%X input_long\n", indexFinger,
  // input_long)); tablet_print(input_long, input);
  // uint16_t behind_txik = indexFinger+1;
  down_repeat_with(
      indexFinger,

      grammar_word = tablet_retrospective_grammar_read(
          indexFinger, (uint8_t)input_long, input, &indexFinger);
      // DEBUGPRINT(("0x%X grammar_word\n", grammar_word));
      if (indexFinger == 0) return 0;
      sequence_indexFinger =
          gen_indexFinger(grammar_word, GRAMMATICALCASE_WORD_SEQUENCE_LONG,
                          grammaticalCase_word_sequence);
      if (sequence_indexFinger != GRAMMATICALCASE_WORD_SEQUENCE_LONG) { // found
        grammaticalCase_indexFinger = indexFinger;
        break;
      } sequence_indexFinger =
          gen_indexFinger(grammar_word, PERSPECTIVE_WORD_SEQUENCE_LONG,
                          perspective_word_sequence);
      if (sequence_indexFinger != PERSPECTIVE_WORD_SEQUENCE_LONG) { // found
        grammaticalCase_indexFinger = indexFinger;
        break;
      });
  phrase_end = grammaticalCase_indexFinger;
  // DEBUGPRINT(("0x%X indexFinger, 0x%X phrase_end\n", indexFinger,
  // phrase_end));
  --indexFinger;
  down_repeat_with(
      indexFinger,

      // DEBUGPRINT(
      //     ("0x%X indexFinger, 0x%X phrase_end\n", indexFinger, phrase_end));
      grammar_word = tablet_retrospective_grammar_read(indexFinger, input_long,
                                                       input, &indexFinger);
      // DEBUGPRINT(
      //     ("0x%X indexFinger, 0x%X grammar_word\n", indexFinger,
      //     grammar_word));
      if (indexFinger == 0 || (indexFinger & LINE_LONG_MASK) == 0) {
        phrase_begin = indexFinger + 1;
        break;
      } sequence_indexFinger =
          gen_indexFinger(grammar_word, GRAMMATICALCASE_WORD_SEQUENCE_LONG,
                          grammaticalCase_word_sequence);
      if (sequence_indexFinger != GRAMMATICALCASE_WORD_SEQUENCE_LONG) { // found
        phrase_begin = indexFinger + 1;
        break;
      } sequence_indexFinger =
          gen_indexFinger(grammar_word, PERSPECTIVE_WORD_SEQUENCE_LONG,
                          perspective_word_sequence);
      if (sequence_indexFinger != PERSPECTIVE_WORD_SEQUENCE_LONG) { // found
        phrase_begin = indexFinger + 1;
        break;
      });
  *ending_indexFinger = phrase_begin;
  phrase_long = phrase_end + 1 - phrase_begin;
  // DEBUGPRINT(("0x%x phrase_begin, 0x%X phrase_long, 0x%X phrase_end\n",
  //            phrase_begin, phrase_long, phrase_end));

  return phrase_long;
}

uint16_t /*phrase_long*/
first_phrase_long_found(const uint16_t begin_indexFinger,
                        const uint16_t input_long, const line_t input[],
                        uint16_t *ending_indexFinger) {
  // if none found return 0
  uint16_t max_input_indexFinger = input_long * LINE_LONG;
  uint16_t grammar_word = 0;
  uint16_t sequence_indexFinger = 0;
  uint16_t indexFinger = begin_indexFinger;
  uint16_t grammaticalCase_indexFinger = begin_indexFinger;
  repeat_with(
      max_input_indexFinger, indexFinger,
      grammar_word = tablet_upcoming_grammar_read(indexFinger, input_long,
                                                  input, &indexFinger);
      sequence_indexFinger =
          gen_indexFinger(grammar_word, GRAMMATICALCASE_WORD_SEQUENCE_LONG,
                          grammaticalCase_word_sequence);
      if (sequence_indexFinger != GRAMMATICALCASE_WORD_SEQUENCE_LONG) { // found
        grammaticalCase_indexFinger = indexFinger;
        break;
      } sequence_indexFinger =
          gen_indexFinger(grammar_word, PERSPECTIVE_WORD_SEQUENCE_LONG,
                          perspective_word_sequence);
      if (sequence_indexFinger != PERSPECTIVE_WORD_SEQUENCE_LONG) { // found
        grammaticalCase_indexFinger = indexFinger;
        break;
      });
  // tablet_print(input_long, input);
  // DEBUGPRINT(("0x%X grammarWord\n",grammar_word));
  // DEBUGPRINT(("0x%X begin_indexFinger, 0x%X indexFinger, 0x%X gcif\n",
  //    begin_indexFinger, indexFinger, grammaticalCase_indexFinger));
  *ending_indexFinger = indexFinger;
  return grammaticalCase_indexFinger + 1 - begin_indexFinger;
}

/** finds a similar sentence, matching the perspective and cases.
 *
 */
uint16_t /* cousin_long */ old_retrospective_perspective_htin_cousin_found(
    const uint16_t perspective, const uint16_t knowledge_long,
    const line_t knowledge[], uint16_t *cousin_begin) {
  // find previous htin, if it has proper perspective return it
  // DEBUGPRINT(
  //    ("0x%X perspective, 0x%X knowledge_long\n", perspective,
  //    knowledge_long));
  guarantee(knowledge_long > 0);
  guarantee(knowledge_long < MAX_KNOWLEDGE_LONG /*max knowledge long*/);
  uint16_t txik_begin = knowledge_long * LINE_LONG;
  uint16_t htin_begin = 0;
  uint16_t htin_long = 0;
  uint16_t comparison_perspective;
  uint16_t input_range = 0;
  down_repeat_with(
      txik_begin,
      input_range = txik_begin < LINE_LONG ? 1 : txik_begin / LINE_LONG;
      htin_long = retrospective_independentClause_long_found(
          input_range, knowledge, &htin_begin);
      txik_begin = htin_begin * LINE_LONG;
      conditional(htin_long == 0, return htin_long);
      comparison_perspective = tablet_read(
          final_perspective_found(htin_long, knowledge + htin_begin), htin_long,
          knowledge + htin_begin);
      // DEBUGPRINT(("0x%X comparison_perspective\n", comparison_perspective));
      if (comparison_perspective == 0) {
        tablet_print(htin_long, knowledge + htin_begin);
      }
      // DEBUGPRINT(("0x%X final_perspective_found, 0x%X perspective\n",
      //            comparison_perspective, perspective));
      conditional(comparison_perspective == perspective,
                  *cousin_begin = htin_begin;
                  return htin_long;));
  return 0;
}

/** wrapper for old_retrospective_perspective_htin_cousin_found */
struct Phrase /* found htin */
retrospective_perspective_htin_cousin_found(const word_t perspective,
                                            const struct Phrase knowledge) {
  txik_t htin_begin = knowledge.length / LINE_LONG + 1;
  struct Phrase found_htin = knowledge;
  found_htin.length =
      LINE_LONG * old_retrospective_perspective_htin_cousin_found(
                      perspective, knowledge.page.plength, knowledge.page.lines,
                      &htin_begin);
  found_htin.begin = htin_begin * LINE_LONG + 1;
  found_htin.length = found_htin.length > 0? found_htin.length -2: 
    found_htin.length;
  return found_htin;
}

uint16_t finalPage_ri(const line_t tablet) {
#ifdef ARRAYLINE
  return (tablet[0] & 1) == 1 ? truth_WORD : lie_WORD;
#else
  return (tablet.s0 & 1) == 1 ? truth_WORD : lie_WORD;
#endif
}

uint16_t /* htin_long */ retrospective_independentClause_long_found(
    const uint16_t input_long, const line_t input[], uint16_t *htin_begin) {
  uint16_t iterator = input_long;
  uint16_t final_tablet = input_long;
  uint16_t first_tablet = input_long;
  // find final_tablet
  // tablet_print(input_long, input);
  down_repeat_with(iterator,
                   // DEBUGPRINT(("0x%X iterator, 0x%X input[iterator -
                   // 1][0]\n", iterator, input[iterator - 1][0]));
                   conditional((input[iterator - 1][0] & 1) == 1,
                               final_tablet = iterator - 1;
                               break));
  // DEBUGPRINT(("0x%X final_tablet, 0x%X first_tablet, 0x%X iterator\n",
  // final_tablet, first_tablet, iterator));
  if (final_tablet == input_long)
    return 0;
  first_tablet = final_tablet;
  down_repeat_with(iterator,
                   conditional((input[iterator - 1][0] & 1) == 1, break);
                   first_tablet--;);
  *htin_begin = first_tablet;
  uint16_t htin_long = number_subtract(final_tablet + 1, first_tablet);
  // DEBUGPRINT(("0x%X final_tablet, 0x%X first_tablet, 0x%X htin_long\n",
  //           final_tablet, first_tablet, htin_long));
  return htin_long;
}
struct Phrase /*perspective Phrase*/
upcoming_independent_perspective_found(const struct Phrase input) {
  conditional(input.length == 0, return input);
  struct Phrase word;
  struct Phrase search = input;
  const txik_t max_txik = search.begin + search.length;
  repeat_with(
      max_txik, search.begin, conditional(search.begin > max_txik, break);
      guarantee(search.begin < input.page.plength * LINE_LONG);
      // DEBUGPRINT(("%X search.begin, %X search.length\n", search.begin,
      // search.length));
      word = upcoming_grammar_read(search);
      // DEBUGPRINT(("%X word.begin, %X word.length\n", word.begin,
      // word.length));
      conditional(word.length == 0, break);
      search.begin = word.begin + word.length - 1;
      conditional(
          gen_indexFinger(phrase_word_read(word, 0),
                          INDEPENDENTCLAUSE_PERSPECTIVE_WORD_SEQUENCE_LONG,
                          independentClause_perspective_word_sequence) !=
                  INDEPENDENTCLAUSE_PERSPECTIVE_WORD_SEQUENCE_LONG &&
              phrase_word_read(word, 0) != conditional_mood_GRAMMAR,
          break) word.length = 0;);
  return word;
}

// grammatical_mood grammaticalMood
struct Phrase /*perspective Phrase*/
upcoming_perspective_found(const struct Phrase input) {
  struct Phrase word;
  struct Phrase search = input;
  const txik_t max_txik = search.begin + search.length;
  repeat_with(max_txik, search.begin,
              conditional(search.begin > max_txik, break);
              word = upcoming_grammar_read(search);
              search.begin = word.begin + word.length - 1;
              conditional(gen_indexFinger(phrase_word_read(word, 0),
                                          PERSPECTIVE_WORD_SEQUENCE_LONG,
                                          perspective_word_sequence) !=
                              PERSPECTIVE_WORD_SEQUENCE_LONG,
                          return word));
  return word;
}

uint16_t /*perspective indexFinger*/
final_perspective_found(const uint16_t tablet_long, const line_t tablet[]) {
  uint16_t tablet_indexFinger = tablet_long * LINE_LONG - 1;
  uint16_t word = 0;
  // DEBUGPRINT(("0x%X tablet_indexFinger\n", tablet_indexFinger));
  word = tablet_retrospective_grammar_read(tablet_indexFinger, tablet_long,
                                           tablet, &tablet_indexFinger);
  // DEBUGPRINT(("0x%X word\n", word));
  conditional(gen_indexFinger(word, PERSPECTIVE_WORD_SEQUENCE_LONG,
                              perspective_word_sequence) !=
                  PERSPECTIVE_WORD_SEQUENCE_LONG,
              return tablet_indexFinger);
  return 0;
}

const uint16_t *subset_pointer_return(const uint16_t tablet_long,
                                      const line_t tablet[],
                                      const uint16_t indexFinger) {
  guarantee(tablet_long * LINE_LONG > indexFinger);
  uint16_t line_indexFinger = indexFinger / LINE_LONG;
  uint16_t word_indexFinger = indexFinger % LINE_LONG;
  return tablet[line_indexFinger] + word_indexFinger;
}

uint16_t /*cousin_long*/ retrospective_phrase_cousin_found(
    const uint16_t example_long, const line_t example[],
    const uint16_t phrase_long, const uint16_t phrase_begin,
    const uint16_t knowledge_long, const line_t knowledge[],
    uint16_t *cousin_begin) {
  // find phrase in comparision, which is the same as the one in example
  uint16_t phrase_code_indexFinger = 0;
  uint16_t phrase_code = tablet_retrospective_grammar_read(
      phrase_begin + phrase_long - 1, example_long, example,
      &phrase_code_indexFinger);
  uint16_t comparison_phrase_begin = 0;
  uint8_t comparison_phrase_long = 0;
  uint16_t max_knowledge_indexFinger = knowledge_long * LINE_LONG - 1;
  retrospective_phrase_situate(
      knowledge_long, knowledge, max_knowledge_indexFinger, phrase_code,
      &comparison_phrase_long, &comparison_phrase_begin);
  const uint16_t *example_phrase_pointer =
      subset_pointer_return(example_long, example, phrase_begin);
  const uint16_t *comparison_phrase_pointer =
      subset_pointer_return(knowledge_long, knowledge, comparison_phrase_begin);

  uint16_t comparison_result = number_sequence_compare(
      phrase_long, example_phrase_pointer, comparison_phrase_long,
      comparison_phrase_pointer);

  if (comparison_result == truth_WORD) {
    *cousin_begin = comparison_phrase_begin;
    return comparison_phrase_long;
  }
  // DEBUGPRINT(("0x%X phrase_code, 0x%X comparison_phrase_long, 0x%X
  // comparison_phrase_begin, 0x%X comparison_ewault\n", phrase_code,
  // comparison_phrase_long, comparison_phrase_begin, comparison_result));
  // compare the phrases

  return 0;
}

uint16_t independentClause_long_found(const uint16_t input_long,
                                      const line_t input[]) {
  repeat(input_long,
         conditional((input[iterator][0] & 1) == 1, return iterator + 1));
  return 0;
}
uint16_t /* cousin_long */
retrospective_htin_cousin_found(const uint16_t example_long,
                                const line_t example[],
                                const uint16_t knowledge_long,
                                const line_t knowledge[],
                                uint16_t *cousin_begin) {
  // find previous htin with example mood,
  guarantee(knowledge_long > 0 && knowledge_long < MAX_KNOWLEDGE_LONG);
  uint16_t perspective = tablet_read(
      final_perspective_found(example_long, example), example_long, example);
  uint16_t htin_begin = knowledge_long;
  uint16_t htin_begin_indexFinger = knowledge_long * LINE_LONG;
  uint16_t htin_long = 0;
  uint16_t cousin = lie_WORD;
  // DEBUGPRINT(("0x%X htin_begin_indexFinger, 0x%X htin_begin / LINE_LONG",
  // htin_begin_indexFinger, htin_begin / LINE_LONG));
  // What does this do?
  // for each phrase find match, else exit.
  down_repeat_with(
      htin_begin_indexFinger,
      // DEBUGPRINT(("0x%X htin_begin_indexFinger, 0x%X htin_begin",
      // htin_begin_indexFinger, htin_begin));
      // if (htin_begin_indexFinger < LINE_LONG) break;
      htin_begin = htin_begin_indexFinger / LINE_LONG;
      htin_long = old_retrospective_perspective_htin_cousin_found(
          perspective, htin_begin, knowledge, &htin_begin);
      // DEBUGPRINT(("0x%X htin_long, 0x%X htin_begin\n", htin_long,
      // htin_begin)); tablet_print(htin_long, knowledge + htin_begin);
      if (htin_long == 0) { return htin_long; } cousin = htin_subset_comparison(
          example_long, example, htin_long, knowledge + htin_begin);
      htin_begin_indexFinger = htin_begin * LINE_LONG + 1;

      // DEBUGPRINT(
      //    ("0x%X htin_begin_indexFinger, 0x%X htin_long, 0x%X htin_begin, "
      //     " 0x%X cousin, 0x%X truth_WORD\n",
      //     htin_begin_indexFinger, htin_long, htin_begin, cousin,
      //     truth_WORD));
      if (cousin == truth_WORD || htin_begin == 0) break;

  );
  // DEBUGPRINT(("0x%X htin_begin_indexFinger, 0x%X htin_long, 0x%X
  // htin_begin\n",
  //            htin_begin_indexFinger, htin_long, htin_begin));
  *cousin_begin = (cousin == truth_WORD) ? htin_begin : 0;
  return (cousin == truth_WORD) ? htin_long : 0;
}

/** is the sentence a variable? is_theSentence_aVariable
 */
word_t htinna_prifka_ri(struct Sentence htin) {
  // text_phrase_print(htin);
  struct Phrase perspective = retrospective_perspective_found(htin);
  // text_phrase_print(perspective);
  conditional(perspective.length == 0, return lie_WORD);
  word_t perspective_word = phrase_word_read(perspective, 0);
  conditional(perspective_word != realis_mood_GRAMMAR, return lie_WORD);
  struct Phrase gvak = retrospective_gvak_found(htin);
  // text_phrase_print(gvak);
  conditional(gvak.begin + gvak.length == perspective.begin, return truth_WORD);
  return lie_WORD;
}
/** finds the previous similar sentence */
struct Phrase /* cousin */
retrospective_variable_htin_cousin_found(const struct Phrase example_phrase,
                                         const struct Phrase knowledge) {
  // find previous htin with example mood,
  // how does one return not found? I think it is of zero length?
  guarantee(knowledge.page.plength > 0 &&
            knowledge.page.plength < MAX_KNOWLEDGE_LONG);
  //DEBUGPRINT(("%X knowledge.length\n", knowledge.length));
  //text_phrase_print(knowledge);
  //text_phrase_print(example_phrase);
  //
  if (knowledge.length == 0 && example_phrase.length != 0) {
    return knowledge;
  }
  struct Phrase perspective_phrase =
      retrospective_perspective_found(example_phrase);
  uint16_t perspective = phrase_word_read(perspective_phrase, 0);
  //  text_phrase_print(perspective_phrase);
  uint16_t htin_begin = knowledge.page.plength;
  uint16_t htin_begin_indexFinger = knowledge.page.plength * LINE_LONG;
  uint16_t cousin = lie_WORD;
  // DEBUGPRINT(("0x%X htin_begin_indexFinger, 0x%X htin_begin / LINE_LONG",
  // htin_begin_indexFinger, htin_begin / LINE_LONG));
  // What does this do?
  // for each phrase find match, else exit.
  struct Phrase found_htin = knowledge;
  struct Phrase search_knowledge = knowledge;
  NewPagePhrase(found_htin_phrase, 2);
  txik_t txik = example_phrase.begin;
  code_name_t code_name = code_name_derive(example_phrase);
  //code_name_t code_name = neo_code_name_derive(example_phrase, &txik);
  code_name_t found_code_name;
  down_repeat_with(
      htin_begin_indexFinger,
      //DEBUGPRINT(("%X htin_Begin_indexFinger\n", htin_begin_indexFinger));
      //DEBUGPRINT(("%X prevIterator\n", prevIterator));
      guarantee(search_knowledge.page.plength <= knowledge.page.plength);
      search_knowledge.length = htin_begin_indexFinger;
      found_htin = retrospective_perspective_htin_cousin_found(
          perspective, search_knowledge);
      //DEBUGPRINT(("found_htin:\n"));
      //text_phrase_print(found_htin);
      // if none found, then return none
      if (found_htin.length == 0) return found_htin;
      // before the perspective must be a grammatical case
      phrase_zero(found_htin_phrase);
      found_htin_phrase = phrase_addenda(found_htin, found_htin_phrase);
      // Page_print(found_htin_phrase.page); DEBUGPRINT(("%X
      // is_theSentence_aVariable(found_htin_phrase)\n",
      //              is_theSentence_aVariable(found_htin_phrase)));
      if (is_theSentence_aVariable(found_htin_phrase) == lie_WORD) {
        //DEBUGPRINT(("%X htin_begin_indexFinger, %X found_htin.begin\n",
        //             htin_begin_indexFinger, found_htin.begin));
        htin_begin_indexFinger = found_htin.begin - search_knowledge.begin;
        search_knowledge.page.plength = found_htin.begin / LINE_LONG;
        found_htin.length = 0;
        //if (htin_begin_indexFinger <= prevIterator) --htin_begin_indexFinger;
        continue;
      } // text_phrase_print(found_htin);
      conditional(found_htin.length == 0, return found_htin);
      // text_page_print(found_htin_phrase.page);
      // text_page_print(example_phrase.page);

      txik = 1;
      found_code_name = code_name_derive(found_htin_phrase);
      //found_code_name = neo_code_name_derive(found_htin_phrase, &txik);
      cousin = found_code_name == code_name ? truth_WORD : lie_WORD;
      // make sure the nominative case is a match
      struct Phrase nom_example = neo_retrospective_phrase_situate(
          example_phrase, nominative_case_GRAMMAR);
      if (cousin == truth_WORD && nom_example.length > 0) {
        struct Phrase nom_found = neo_retrospective_phrase_situate(
            found_htin_phrase, nominative_case_GRAMMAR);
        // text_phrase_print(nom_found);
        // text_phrase_print(nom_example);
        // DEBUGPRINT(("%X phrase_to_phrase_compare(nom_example, nom_found)\n",
        // phrase_to_phrase_compare(nom_example, nom_found)));
        cousin = phrase_to_phrase_compare(nom_example, nom_found);
      }
      //DEBUGPRINT(("%X cousin, %lX %lX\n", cousin, found_code_name, code_name)); 
      //DEBUGPRINT(("%X htin_begin_indexFinger, %X found_htin.begin\n",
      //             htin_begin_indexFinger, found_htin.begin));
      htin_begin_indexFinger = found_htin.begin - search_knowledge.begin;
      search_knowledge.page.plength = found_htin.begin / LINE_LONG;
      if (cousin == truth_WORD || htin_begin == 0) break; found_htin.length = 0;
  );
  // text_phrase_print(found_htin);
  return found_htin;
}

struct Page /* htin range */
retrospective_htin_long_found(const struct Page input) {
  struct Page htin;
  uint16_t htin_begin = 0;
  htin.plength = retrospective_independentClause_long_found(
      input.plength, input.lines, &htin_begin);
  htin.lines = input.lines + htin_begin;
  return htin;
}

struct Sentence /* htin range */
retrospective_htin_found(const struct Paragraph input) {
  uint16_t htin_begin = 0;
  uint8_t page_length = (((input.length + input.begin)| LINE_LONG_MASK) +1)
    /LINE_LONG;
  uint8_t plength = retrospective_independentClause_long_found(
      page_length, input.page.lines, &htin_begin);
  guarantee(htin_begin < page_length * LINE_LONG);
  struct Sentence produce;
  produce.page = input.page;
  produce.begin = plength > 0 ? htin_begin * LINE_LONG + 1 : 0;
  guarantee(plength == 0 || produce.begin % 0x10 == 1);
  produce.length = plength * LINE_LONG -2;
  produce.length = phrase_long_diagnose(produce);
  return produce;
}

struct Phrase /* perspective phrasae */
neo_final_perspective_found(const struct Phrase page) {
  guarantee(page.page.lines != NULL);
  struct Phrase perspective;
  perspective.length = 0;
  perspective.begin = 0;
  uint16_t perspective_txik =
      final_perspective_found(page.page.plength, page.page.lines);
  conditional(perspective_txik == 0, return perspective);
  perspective.page = page.page;
  perspective.length = 1;
  perspective.begin = perspective_txik;
  return perspective;
}

struct Phrase /* found htin phrase */
htin_long_found(const struct Phrase input) {
  // find final line
  // txik_t page_begin = input.begin / LINE_LONG;
  // txik_t final_line = independentClause_long_found(
  //    input.page.plength, input.page.lines + page_begin);
  //// find final word
  // txik_t final_begin = final_line * (LINE_LONG - 1);
  // txik_t max_txik = min(LINE_LONG, input.begin + input.length - final_begin);
  // struct Phrase word;
  // struct Phrase search;

  struct Phrase perspective = upcoming_perspective_found(input);
  NewHollowPhrase(produce);
  conditional(perspective.length == 0, return produce);
  produce = input;
  produce.length = (perspective.begin + perspective.length) - produce.begin;
  return produce;
}

struct Phrase /* found htin */ forward_htin_found(const struct Phrase input) {
  NewHollowPhrase(htin);
  // find next perspective word,
  // DEBUGPRINT(("%X input.length, %X input.begin\n",
  //       input.length, input.begin));
  struct Phrase perspective = upcoming_independent_perspective_found(input);
  // DEBUGPRINT(("%X perspective.length, %X perspective.begin\n",
  //       perspective.length, perspective.begin));
  if (perspective.begin == 0 || perspective.length <= 0)
    return htin;
  // text_phrase_print(perspective);
  htin = input;
  htin.length = (perspective.length + perspective.begin) - htin.begin;
  guarantee(htin.length < htin.page.plength * LINE_LONG);
  return htin;
}

struct Phrase /* found_phrase */
// if not found, returns 0 as produce.begin
// retrospective_example_gvakfyak_found(input, example_gvak)
neo_retrospective_phrase_situate(const struct Phrase input,
                                 const word_t phrase_code) {
  txik_t input_indexFinger = input.begin + input.length;
  struct Phrase produce = input;
  // text_phrase_print(input);
  // DEBUGPRINT(("%X phrase_code\n", phrase_code));
  // DEBUGPRINT(("%X input_indexFinger\n", input_indexFinger));
  retrospective_phrase_situate(input.page.plength, input.page.lines,
                               input_indexFinger, phrase_code, &produce.length,
                               &produce.begin);
  // DEBUGPRINT(("0x%X produce.begin, 0x%X produce.length\n", produce.begin,
  //      produce.length));
  return produce;
}

struct Phrase retrospective_perspective_found(const struct Phrase input) {

  word_t word = 0;
  struct Phrase blank_range;
  blank_range.begin = 0;
  blank_range.length = 0;
  struct Phrase perspective_range;
  struct Phrase search_range;
  search_range.begin = input.begin;
  search_range.length = input.length;
  down_repeat_with(
      search_range.length,
      perspective_range = neo_tablet_retrospective_grammar_read(input);
      word = phrase_word_read(perspective_range, 0);
      //DEBUGPRINT(("%X word, %X perspective.begin, %X perpective.length\n",
      //    word, perspective_range.begin, perspective_range.length));
      if (is_perspective_word(word) == truth_WORD) { 
         return perspective_range; 
         });
  return blank_range;
}

// struct Phrase

struct Phrase upcoming_gvak_found(const struct Phrase input) {
  guarantee(input.page.lines != NULL);
  // DEBUGPRINT(("%X input.length\n",input.length));
  word_t word = 0;
  struct Phrase blank_range = input;
  blank_range.begin = 0;
  blank_range.length = 0;
  struct Phrase gvak_range;
  gvak_range.page = input.page;
  struct Phrase search = input;
  search.begin = search.begin == 1 ? search.begin - 1 : search.begin;
  // DEBUGPRINT(("%X input.length\n", input.length));
  repeat_with(
      input.length, search.begin,
      // DEBUGPRINT(("%X search.begin, %X search.length, %X input.length\n",
      //     search.begin, search.length, input.length));
      gvak_range = neo_tablet_upcoming_grammar_read(search);
      // text_phrase_print(gvak_range);
      // phrase_print(search);
      // text_phrase_print(search);
      // DEBUGPRINT(("%X gvak_range.begin, %X gvak_range.length\n",
      //    gvak_range.begin, gvak_range.length));
      if (gvak_range.begin == gvak_range.page.plength * LINE_LONG) {
        return blank_range;
      } word = phrase_word_read(gvak_range, 0);
      // DEBUGPRINT(("%X word, %X gvak.begin, %X perpective.length,  %X"
      //    "search.length\n",
      //     word, gvak_range.begin, gvak_range.length, search.length));
      if (gen_indexFinger(word, GRAMMATICALCASE_WORD_SEQUENCE_LONG,
                          grammaticalCase_word_sequence) !=
          GRAMMATICALCASE_WORD_SEQUENCE_LONG) { return gvak_range; }
      // if (gvak_range.begin == 0) return blank_range;
      search.begin = gvak_range.begin + 1;
      search.length = input.length - search.begin;);
  return blank_range;
}
struct Phrase upcoming_gvakkupwik_found(const struct Phrase input) {
  guarantee(input.page.lines != NULL);
  // DEBUGPRINT(("%X input.length\n",input.length));
  word_t word = 0;
  struct Phrase blank_range = input;
  blank_range.begin = 0;
  blank_range.length = 0;
  struct Phrase gvak_range;
  gvak_range.page = input.page;
  struct Phrase search = input;
  search.begin = search.begin == 1 ? search.begin - 1 : search.begin;
  // DEBUGPRINT(("%X input.length\n", input.length));
  repeat_with(
      input.length + input.begin, search.begin,
      // DEBUGPRINT(("%X search.begin, %X search.length, %X input.length\n",
      //     search.begin, search.length, input.length));
      // text_phrase_print(search);
      gvak_range = neo_tablet_upcoming_grammar_read(search);
      // text_phrase_print(gvak_range);
      // phrase_print(search);
      // text_phrase_print(search);
      // DEBUGPRINT(("%X gvak_range.begin, %X gvak_range.length\n",
      //    gvak_range.begin, gvak_range.length));
      if (gvak_range.begin == gvak_range.page.plength * LINE_LONG) {
        return blank_range;
      } word = phrase_word_read(gvak_range, 0);
      // DEBUGPRINT(("%X word, %X gvak.begin, %X perpective.length,  %X"
      //    "search.length\n",
      //     word, gvak_range.begin, gvak_range.length, search.length));
      if (is_grammatical_case_word(word) == truth_WORD ||
          is_perspective_word(word) == truth_WORD) { return gvak_range; }
      // if (gvak_range.begin == 0) return blank_range;
      search.begin = gvak_range.begin + 1;
      search.length = input.length - search.begin;);
  return blank_range;
}

struct Phrase retrospective_gvak_found(const struct Phrase input) {
  guarantee(input.page.lines != NULL);
  word_t word = 0;
  struct Phrase blank_range = input;
  blank_range.begin = 0;
  blank_range.length = 0;
  struct Phrase gvak_range;
  gvak_range.page = input.page;
  struct Phrase search = input;
  down_repeat_with(
      search.length, gvak_range = neo_tablet_retrospective_grammar_read(search);
      //text_phrase_print(search);
      //DEBUGPRINT(("%X search.length\n", search.length));
      word = phrase_word_read(gvak_range, 0);
      if (gen_indexFinger(word, GRAMMATICALCASE_WORD_SEQUENCE_LONG,
                          grammaticalCase_word_sequence) !=
          GRAMMATICALCASE_WORD_SEQUENCE_LONG) { return gvak_range; }
      // if (gvak_range.begin == 0) return blank_range;
      search.length = gvak_range.begin - search.begin;);
  return blank_range;
}

// with mood inclusive
struct Phrase /* verb_phrase */
retrospective_verb_phrase_found(const struct Phrase input) {
  struct Phrase verb_phrase;
  verb_phrase.page = input.page;
  verb_phrase.begin = 0;
  verb_phrase.length = 0;
  struct Phrase perspective = retrospective_perspective_found(input);
  conditional(perspective.length == 0, return verb_phrase);
  struct Phrase retrospective_phrase_range;
  retrospective_phrase_range.begin = input.begin;
  retrospective_phrase_range.length = perspective.begin - input.begin;
  retrospective_phrase_range = retrospective_gvak_found(input);
  verb_phrase.begin =
      retrospective_phrase_range.begin + retrospective_phrase_range.length;
  conditional(verb_phrase.begin == 0, verb_phrase.begin += 1);
  verb_phrase.length =
      perspective.begin + perspective.length - verb_phrase.begin;
  return verb_phrase;
}

struct Phrase forward_finally_found(const struct Phrase search_space) {
  NewHollowPhrase(finally_found);
  finally_found.page = search_space.page;
  // NewTextPage(finally_example, 1, "fe");

  // Page_print(finally_example);
  // assert(1==0);
  NewPage(finally_example, 1);
  finally_example.lines[0][0] = 3;
  finally_example.lines[0][1] = finally_GRAMMAR;
  line_t *lines = search_space.page.lines;
  repeat(search_space.length,
         if (tablet_compare(1, finally_example.lines, 1, &lines[iterator]) ==
             truth_WORD) {
           finally_found.page.plength = 1;
           finally_found.page.lines = search_space.page.lines + iterator;
           finally_found.begin = 1;
           finally_found.length = 1;
           break;
         });
  return finally_found;
}

struct Paragraph retrospective_recipe_found(const struct Paragraphs knowledge,
                                            const struct Sentence input) {
  NewHollowParagraph(recipe);
  recipe.page = knowledge.page;
  const struct Sentence ksim =
      retrospective_recipe_ksim_found(knowledge, input);
  conditional(ksim.length == 0, return recipe);
  recipe.page.lines = ksim.page.lines;
  // find the finally_GRAMMAR that comes after the ksim,
  struct Sentence finally_search = ksim;
  finally_search.page.plength =
      knowledge.page.plength - (ksim.page.lines - knowledge.page.lines);
  struct Phrase finally = forward_finally_found(finally_search);
  guarantee(finally.length > 0);
  // make that the length of the recipe
  recipe.page.plength = (finally.page.lines + 1 - ksim.page.lines);
  recipe.begin = ksim.begin;
  recipe.length = neo_htinti_final_word_found(recipe.page) + 1 - ksim.begin;
  return recipe;
}

// struct Phrase /* gvak found */
// retrospective_example_gvak_found(const struct Phrase input,
//                                 const word_t example_gvak) {
//
//  return input;
//}
//

struct Phrase
retrospective_variable_htin_found(const struct Phrase knowledge_tablet,
                                  const struct Phrase variable_name) {
  guarantee(variable_name.page.lines != NULL);
  guarantee(variable_name.length > 0);
  struct Page variable_tablet = variable_name.page;
  // Page_print(variable_tablet);
  // phrase_print(variable_name);
  guarantee(neo_tablet_read(variable_name.begin + variable_name.length - 1,
                            variable_tablet) == name_GRAMMAR);
  guarantee(variable_name.length > 0);
  NewPagePhrase(question, 1);
  neo_extract_words_ins_htinka_addenda(variable_name, question);
  // tablet_print(question_long, &question);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR,
                                              question);
  // tablet_print(question_long, &question);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, question);
  // tablet_print(question_long, &question);
  // find matching declaration.
  struct Phrase cousin =
      neo_retrospective_htin_cousin_found(question, knowledge_tablet);
  // struct Page htin;
  // htin.lines =cousin.lines;
  // htin.length = cousin.length;
  return cousin;
}
struct Phrase /*recipe_ksim*/
retrospective_recipe_ksim_found(const struct Phrase knowledge,
                                const struct Phrase input) {
  // get code name of input
  code_name_t code_name = code_name_derive(input);
  //code_name_t code_name = neo_code_name_derive(input, &txik);
  // get verb from input
  struct Phrase verb_phrase = retrospective_verb_phrase_found(input);
  // append verb to example page
  NewPagePhrase(example, 1);
  DEBUGPRINT(("%X verb_phrase.length\n", verb_phrase.length));
  verb_phrase.length = verb_phrase.length - 1;
  DEBUGPRINT(("%X verb_phrase.length\n", verb_phrase.length));
  phrase_print(verb_phrase);
  neo_extract_words_ins_htinka_addenda(verb_phrase, example);
  Page_print(example.page);
  // append declarative mood to example page
  neo_literal_grammar_word_ins_htinka_addenda(declarative_mood_GRAMMAR,
                                              example);
  Page_print(example.page);
  code_name_t ksim_code_name = 0;
  uint16_t ksim_txik = 0;
  struct Phrase search_knowledge = knowledge;
  struct Phrase ksim;

  // htin cousin found
  down_repeat(
      search_knowledge.length, search_knowledge.length = iterator;
      ksim = neo_retrospective_htin_cousin_found(example, search_knowledge);
      // get code name of output
      ksim_txik = 1;
      //ksim_code_name = neo_code_name_derive(ksim, &ksim_txik); 
      ksim_code_name = code_name_derive(ksim); 

      conditional(ksim_code_name == code_name, break);
      search_knowledge.length = (ksim.page.lines - search_knowledge.page.lines);
      iterator = search_knowledge.length < iterator - 1
                     ? search_knowledge.length
                     : iterator;);
  // if not same as input keep looking.
  return ksim;
}
struct Phrase /* phrase range */
neo_final_phrase_found(const struct Phrase input) {
  guarantee(input.page.lines != NULL);
  struct Phrase phrase = input;
  uint16_t phrase_begin = 0;
  uint16_t phrase_long =
      final_phrase_found(input.begin + input.length, input.page.plength,
                         input.page.lines, &phrase_begin);
  conditional(phrase_long == 0, NewHollowPhrase(empty); empty.page = input.page;
              return empty;);
  phrase.page = input.page;
  phrase.length = phrase_long;
  phrase.begin = phrase_begin;
  return phrase;
}

//  find_theParagraphZContents_toThePhrase()
//  find_theParagraphZContents_toTheParagraph
struct Paragraph /* recipe_content */
tyafyi_tyafti_hnokka_hfantu(struct Paragraph hkakhnikya) {
  // drop first sentence of paragraph
  hkakhnikya = drop_theParagraphZFirstSentence_toTheParagraph(hkakhnikya);
  text_phrase_print(hkakhnikya);
  // drop last sentence of paragraph
  hkakhnikya = drop_theParagraphZFinalSentence_toTheParagraph(hkakhnikya);
  text_phrase_print(hkakhnikya);
  return hkakhnikya;
}

// find_thePrevious_GrammaticalCasePhrase(input)
// if not found result.begin = 0
struct Phrase rwekci_gvakfyakka_hfantu(const struct Phrase hkakya) {
  struct Phrase gvak = retrospective_gvak_found(hkakya);
  word_t gvaktlat = phrase_word_read(gvak, 0);
  return retrospective_example_gvakfyak_found(hkakya, gvaktlat);
}

// find_theNext_Phrase(input)
// // upcoming_phrase_found(input)
// if not found result.begin = 0
struct Phrase hvatci_fyakka_hfantu(const struct Phrase hkakya) {
  conditional(hkakya.length == 0, return hkakya);
  struct Phrase gvak = upcoming_gvakkupwik_found(hkakya);
  // text_phrase_print(hkakya);
  // text_phrase_print(gvak);
  word_t gvaktlat = phrase_word_read(gvak, 0);
  // DEBUGPRINT(("%X gvaktlat\n", gvaktlat));
  // text_phrase_print(hkakya);
  struct Phrase hkakya_search = hkakya;
  if (gvak.length != 0) {
    hkakya_search.length = (gvak.begin + gvak.length) - hkakya.begin;
  }
  // DEBUGPRINT(("%X hkakya_search.length, %X hkakya_search.begin\n",
  //      hkakya_search.length, hkakya_search.begin));
  // text_phrase_print(hkakya_search);
  struct Phrase long_phrase =
      retrospective_example_gvakfyak_found(hkakya_search, gvaktlat);
  // DEBUGPRINT(("%X long_phrase.length\n", long_phrase.length));
  if (long_phrase.length < 1) {
    return gvak;
  } else {
    return long_phrase;
  }
}
// find_theNext_GrammaticalCasePhrase(input)
// // upcoming_nounphrase_found(input)
// if not found result.begin = 0
struct Phrase hvatci_gvakfyakka_hfantu(const struct Phrase hkakya) {
  conditional(hkakya.length == 0, return hkakya);
  struct Phrase gvak = upcoming_gvak_found(hkakya);
  // text_phrase_print(hkakya);
  // text_phrase_print(gvak);
  word_t gvaktlat = phrase_word_read(gvak, 0);
  // DEBUGPRINT(("%X gvaktlat\n", gvaktlat));
  // text_phrase_print(hkakya);
  struct Phrase hkakya_search = hkakya;
  if (gvak.length != 0) {
    hkakya_search.length = (gvak.begin + gvak.length) - hkakya.begin;
  }
  // DEBUGPRINT(("%X hkakya_search.length, %X hkakya_search.begin\n",
  //      hkakya_search.length, hkakya_search.begin));
  // text_phrase_print(hkakya_search);
  struct Phrase long_phrase =
      retrospective_example_gvakfyak_found(hkakya_search, gvaktlat);
  // DEBUGPRINT(("%X long_phrase.length\n", long_phrase.length));
  if (long_phrase.length < 1) {
    return gvak;
  } else {
    return long_phrase;
  }
}

//    find_theParagraphZLastSentence_toTheSentence(theParagraph)
struct Sentence htinyi_tyafti_tlichtinka_hfantu(struct Paragraph tyafya) {
  uint16_t first_line_of_paragraph = tyafya.begin / LINE_LONG_MASK;
  // DEBUGPRINT(("%X first_line_of_paragraph\n", first_line_of_paragraph));
  uint16_t adjusted_length_of_paragraph =
      tyafya.page.plength - first_line_of_paragraph;
  // DEBUGPRINT(("%X adjusted_length_of_paragraph\n",
  // adjusted_length_of_paragraph));
  uint16_t last_txik = tyafya.begin + tyafya.length + 1;
  // so last_txik becomes where the sentence starts, and the return value is the
  // length
  uint16_t lastSentenceLength = retrospective_independentClause_long_found(
      adjusted_length_of_paragraph, tyafya.page.lines + first_line_of_paragraph,
      &last_txik);
  DEBUGPRINT(("%X last_txik, %X lastSentenceLength, \n", last_txik,
              lastSentenceLength));
  uint16_t lastSentence_begin =
      (first_line_of_paragraph + last_txik) * LINE_LONG + 1;
  uint16_t inputZtotalLength = tyafya.begin + tyafya.length;

  uint16_t lastSentence_length = inputZtotalLength - lastSentence_begin;
  DEBUGPRINT(
      ("%X lastSentence_begin, %X inputZtotalLength, %X lastSentence_length \n",
       lastSentence_begin, inputZtotalLength, lastSentence_length));
  tyafya.begin = lastSentence_begin;
  tyafya.length = lastSentence_length;

  DEBUGPRINT(("%X last_txik\n", last_txik));
  DEBUGPRINT(("%X lastSentenceLength\n", lastSentenceLength));
  return tyafya;
}

//    find_theParagraphZFirstSentence_toTheSentence(theParagraph)
struct Sentence htinyi_tyafti_hpamhtinka_hfantu(struct Paragraph tyafya) {
  // find the first sentence that the paragraph begins at
  uint16_t first_line_of_paragraph = tyafya.begin / LINE_LONG_MASK;
  uint16_t adjusted_length_of_paragraph =
      tyafya.page.plength - first_line_of_paragraph;
  uint16_t length_of_htin =
      independentClause_long_found(adjusted_length_of_paragraph,
                                   tyafya.page.lines + first_line_of_paragraph);
  DEBUGPRINT(("%X length_of_htin\n", length_of_htin));
  tyafya.length = length_of_htin * LINE_LONG;
  tyafya.begin &= LINE_LONG_MASK;
  return tyafya;
}

void phrase_situate(const uint8_t tablet_long, const line_t *tablet,
                    const uint16_t phrase_code, uint8_t *phrase_long,
                    uint16_t *phrase_place) {
// phrase_place is begining of phrase,
// if not found, returns 0 as phrase_place
#ifdef ARRAYLINE
  guarantee(tablet[0][0] != 0);
#else
  guarantee(tablet[0].s0 != 0);
#endif
  guarantee(phrase_place != NULL);
  guarantee(phrase_long != NULL);
  uint16_t tablet_indexFinger = 0;
  // printf("grammaticalCase_word %04X \n", phrase_code);
  // (uint16_t)tablet[tablet_indexFinger].s0;
  // const uint16_t referential = (uint8_t)binary_phrase_list & 1;
  // uint8_t indexFinger = 0;
  // uint8_t found = FALSE;
  uint16_t phrase_termination = 0;
  uint16_t phrase_begin = 0;
  uint16_t phrase_word = 0;
  for (; tablet_indexFinger < tablet_long * LINE_LONG; ++tablet_indexFinger) {
    phrase_word = tablet_upcoming_grammar_read(tablet_indexFinger, tablet_long,
                                               tablet, &tablet_indexFinger);
    if (phrase_word == 0) {
      *phrase_place = 0;
      *phrase_long = 0;
      break;
    }
    // printf("%s:%d:%s\t 0x%X tablet_indexFinger, 0x%X phrase_word, "
    //     "0x%X phrase_code\n", __FILE__,
    //  __LINE__, __FUNCTION__, tablet_indexFinger, phrase_word, phrase_code);
    if (phrase_word == phrase_code) {
      phrase_termination = tablet_indexFinger;
      --tablet_indexFinger;
      tablet_retrospective_grammar_read(tablet_indexFinger, tablet_long, tablet,
                                        &tablet_indexFinger);
      phrase_begin = tablet_indexFinger + 1;
      // printf("%s:%d:%s\t 0x%X tablet_indexFinger\n", __FILE__,
      //  __LINE__, __FUNCTION__, tablet_indexFinger);
      break;
    }
  }
  *phrase_place = phrase_begin;
  *phrase_long = (uint8_t)(phrase_termination + 1 - phrase_begin);
}

uint16_t /*final_word_indexFinger*/
htinti_final_word_found(const uint16_t tablet_long, const line_t tablet[]) {
  guarantee(tablet_long > 0);
  const uint16_t max_indexFinger = tablet_long * LINE_LONG - 1;
  uint16_t indexFinger = max_indexFinger;
  uint16_t word = 0;
  down_repeat_except(max_indexFinger, word != 0,
                     word = tablet_retrospective_word_read(
                         iterator - 1, tablet_long, tablet, &indexFinger););
  // DEBUGPRINT(("0x%X final_word, 0x%X indexFinger\n", tablet_read(indexFinger,
  // tablet_long, tablet), indexFinger)); tablet_print(tablet_long, tablet);
  // conditional(word == 0) return max_indexFinger;
  guarantee(indexFinger <= max_indexFinger);
  return indexFinger;
}

void retrospective_phrase_situate(const uint8_t tablet_long,
                                  const line_t *tablet,
                                  const uint16_t input_tablet_indexFinger,
                                  const uint16_t phrase_code,
                                  uint8_t *phrase_long,
                                  uint16_t *phrase_place) {
  // phrase_place is begining of phrase,
  // if not found, returns 0 as phrase_place
  guarantee(tablet[0][0] != 0);
  guarantee(phrase_place != NULL);
  guarantee(phrase_long != NULL);
  uint16_t tablet_indexFinger = input_tablet_indexFinger;
  uint16_t phrase_termination = 0;
  uint16_t phrase_begin = 0;
  uint16_t phrase_word = 0;
  for (; tablet_indexFinger > 0; --tablet_indexFinger) {
    // DEBUGPRINT(("0x%X tablet_indexFinger, 0x%X phrase_word, "
    //     "0x%X phrase_code, 0x%X tablet_long\n", tablet_indexFinger,
    //     phrase_word, phrase_code, tablet_long));
    // tablet_print(tablet_long, tablet);
    /*ASDFASD problem with tablet_retrospective_grammar_read? ASDFASDF */
    tablet_indexFinger = tablet_indexFinger % LINE_LONG == 0
                             ? tablet_indexFinger - 1
                             : tablet_indexFinger;
    phrase_word = tablet_retrospective_grammar_read(
        tablet_indexFinger, tablet_long, tablet, &tablet_indexFinger);
    // DEBUGPRINT(("0x%X tablet_indexFinger, 0x%X phrase_word, "
    //      "0x%X phrase_code\n", tablet_indexFinger, phrase_word,
    //      phrase_code));
    if (phrase_word == 0) {
      *phrase_place = 0;
      *phrase_long = 0;
      break;
    }
    uint8_t temp_phrase_long;
    word_t temp_word = 0;
    if (phrase_word == phrase_code) {
      phrase_termination = tablet_indexFinger;
      --tablet_indexFinger;
      // word_t retrospective_phrase_word =
      tablet_retrospective_grammar_read(tablet_indexFinger, tablet_long, tablet,
                                        &tablet_indexFinger);
      phrase_begin = tablet_indexFinger + 1;
      // skip blanks
      temp_phrase_long = phrase_termination - phrase_begin;
      // find_next_non_zero_word(
      // phrase_begin --;
      repeat(temp_phrase_long,
             //++phrase_begin;
             temp_word = tablet_upcoming_word_read(phrase_begin, tablet_long,
                                                   tablet, &phrase_begin);
             if (temp_word != 0) { break; } else { ++phrase_begin; });

      // DEBUGPRINT(("0x%X r_phrase_word, 0x%X phrase_begin, 0x%X
      // phrase_termination\n",
      //  retrospective_phrase_word, phrase_begin, phrase_termination));
      // tablet_print(tablet_long, tablet);
      break;
    }
  }
  // guarantee(tablet_long < 6);
  *phrase_place = phrase_begin;
  *phrase_long =
      phrase_begin == 0 ? 0 : (uint8_t)(phrase_termination + 1 - phrase_begin);
}

struct Text letter_found(struct Text text, struct Text letter) {
  uint16_t second_iterator = 0;
  struct Text search_text;
  NewTextPad(result, 1);
  result.length = 0;
  repeat(text.length - letter.length, search_text = text;
         search_text.letters += iterator; search_text.length -= iterator;
         second_iterator = 0;
         repeat_with(letter.length, second_iterator,
                     conditional(letter.letters[second_iterator] !=
                                     search_text.letters[second_iterator],
                                 break))
             conditional(second_iterator == letter.length,
                         search_text.length = letter.length;
                         result = search_text; break));
  return result;
}

/**
 * // get list of grammar and quote words from phrase
// derive a list of grammar and quote words from phrase
//  fyakpwih kyitwahgafti lwatka practu
// derive_aListOfQuoteWordsAndGrammarWords_fromThePhrase
// output doesn't set index, just uses length
// */
struct Phrase
fyakpwih_kyittlatwa_hgaftlatti_lwatka_practu(struct Phrase input,
                                             struct Phrase produce) {
  struct Phrase search = input;
  struct Phrase word = input;
  uint8_t quote_length = 0;
  const txik_t max_txik = input.begin + input.length;
  repeat(input.length,
         word = upcoming_grammar_or_quote_found(search);
         // if is zero then break;
         conditional(word.length == 0, break);
         // add word to produce;
         produce = phrase_addenda(word, produce);
         // if is a quote, skip quoted contents.
         quote_length = derive_quote_byte_length(word) / 2;
         // adjust length of search
         search.begin = word.begin + word.length + quote_length;
         conditional(max_txik <= search.begin, break);
         search.length = max_txik - search.begin; quote_length = 0);
  return produce;
}

struct Phrase upcoming_grammar_or_quote_found(struct Phrase search) {
  // repeat through the phrase, searching for grammar or quote.
  // the only thing we have to be careful of is avoiding stuff inside quotes,
  // which may look deceptively like a grammar or quote word.
  //
  // first get line index,
  // then find current place in it, (based on search.begin)
  // find next point that is either a grammatical case word,
  // or just after a grammar word (a potential quote);
  // if is a quote, return the quote.
  //

  // check if on a grammar word

  word_t word = 0;
  // struct Phrase found = search;

  repeat(
      search.length,
      // conditional((iterator & LINE_LONG_MASK) == 0, continue);
      word = phrase_word_read(search, iterator);
      //DEBUGPRINT(("%X word, %lX iterator, %X is_grammar_word(word), %X "
      //            "is_quote_code(word)\n",
      //            word, iterator, is_grammar_word(word), is_quote_code(word)));
      conditional(is_grammar_word(word) == truth_WORD ||
                      is_quote_code(word) == truth_WORD,
                  search.begin += iterator;
                  search.length = 1; return search););
  // txik_t txik = search.begin + search.length  -1;
  // tablet_upcoming_grammar_or_quote_read(txik, search.page.plength,
  //    search.page.lines, &txik);
  // found.length = 1;
  // found.begin = txik;
  search.length = 0;
  return search;
}

// retrospective example word in phrase found
// fyaknweh_tlamtlatka_rwekcihfantu
// retrospectively_find the example word in the phrase
// retrospectivelyFind_theExampleWord_inThePhrase
// retrospectivelyFind_theExampleWord_inThePhrase(_example_word,_phrase) \
//   fyaknweh_tlamtlatka_rwekcihfantu(_phrase,_example_word)
// retrospective_example_word_in_phrase_found \
//   fyaknweh_tlamtlatka_rwekcihfantu

// if it doesn't find it it returns a length of 0
struct Phrase fyaknweh_tlamtlatka_rwekcihfantu(struct Phrase input, 
    word_t example_word) {
  word_t word;
  struct Phrase produce = input;
  produce.length = 0;
  down_repeat(input.length + 1, 
      word = phrase_word_read(input, iterator -1); 
      DEBUGPRINT(("%X word, %X example_word, %lX iterator\n", word, example_word, iterator -1));
      if (word == example_word) {
        produce.length = 1;
        produce.begin = iterator -1 + input.begin;
        break;
      });
  return produce;
}
// upcoming example word in phrase found
// fyaknweh_tlamtlatka_hvatcihfantu
// upcomingly_find the example word in the phrase
// upcominglyFind_theExampleWord_inThePhrase
// upcominglyFind_theExampleWord_inThePhrase(_example_word,_phrase) \
//   fyaknweh_tlamtlatka_hvatcihfantu(_phrase,_example_word)
// upcoming_example_word_in_phrase_found \
//   fyaknweh_tlamtlatka_hvatcihfantu

// if it doesn't find it it returns a length of 0
struct Phrase fyaknweh_tlamtlatka_hvatcihfantu(struct Phrase input, 
    word_t example_word) {
  word_t word;
  struct Phrase produce = input;
  produce.length = 0;
  repeat(input.length, 
      word = phrase_word_read(input, iterator); 
      DEBUGPRINT(("%X word, %X example_word, %lX iterator\n", word, example_word, iterator));
      if (word == example_word) {
        produce = input;
        produce.length = 1;
        produce.begin = iterator + input.begin;
        break;
      });
  return produce;
}
// #define derive_aQuoteLength_byThePhrase fyakyu_kyitlyanka_practu
uint16_t fyakyu_kyitlyanka_practu(struct Phrase quote_phrase) {
  uint16_t quote_length = 0;
  word_t quote_code = phrase_word_read(quote_phrase, 0);
  word_t vector_length = phrase_word_read(quote_phrase, 1);
  switch(quote_code) {
    example(PARAGRAPH_LETTER_QUOTE, quote_length = vector_length);
    default:
      text_phrase_print(quote_phrase);
      DEBUGPRINT(("this is not a supported quote\n"));
       guarantee(1 == 0);
  }
  return quote_length;
}

