/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
// file for converting pyash to JSON
#ifndef JSON_H
#define JSON_H
#include <stdio.h>
#include <string.h>
#include "jansson/jansson.h"
#include "sort.h"
#include "encoding.h"
#include "translation.h"

/** Pyash to JSON conversion function 
 * @param pyash text
 * @returns JSON text
 */
struct Text /* produce */ pyash_to_json(struct Text input, struct Text produce);

/** Pyash to JSON conversion function 
 * @param pyash input Phrase
 * @param produce JSON object
 * @returns JSON object
 */
json_t* /* produce */ pyash_htin_to_json(struct Phrase input_htin, 
    json_t *root_object);

/** Pyash phrase to JSON conversion function 
 * @param pyash input Phrase
 * @param produce JSON object
 * @returns JSON object
 */
json_t* /* produce */ pyash_phrase_to_json(struct Phrase input_htin, 
    json_t *root_object);

#endif
