#include "sort.h"
kernel void code_to_en_translation(constant const uint32_t dictionary_long,
                           constant const char *dictionary, 
                           constant const uint32_t code_long,
                           constant const v16us *code, 
                           global uint32_t *produce_pad_long,
                           global char *produce_pad) {
  // language unique allotment _nom begin _rea
  //    each worker _nom single independentClause _acc processing _rea
  //    translate one phrase at a time
  const uint16_t indicator_list = v16us.s0;
  uint8_t indicator = indicator_list & 1;
  uint8_t tablet_indexFinger = 0;
  uint8_t vector_long = 0;
  uint8_t scalar_thick = 0;
  uint16_t code_word = 0;
  for (tablet_indexFinger = 1; tablet_indexFinger < HTIN_LONG; ++tablet_indexFinger) {
    if ((indicator_list >> tablet_indexFinger - 1 ) & 1 = indicator &&
            (v16us_read(tablet_indexFinger, tablet) & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) { 
        // is at a potential quote place
        // find quote length
        vector_long = & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN;
        // translate quote
        // put indexFinger after the quote
    }
    if ((indicator_list >> tablet_indexFinger) & 1 = indicator) { 
        // is end of a phrase
    }
  //    each translation dictionary code _nom each code word _acc comparison
  //    _rea
  //      after each successful translation decrement code words to be
  //      translated
  //        counter.
  //    if code word remains counter equals zero then skip to next word if
  //    available.
  //    if same then put translation into produce buffer
  }
  // language unique allotment _nom done _rea
  //
  //
}
