/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef DIALOGUE_H
#define DIALOGUE_H
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#define MAXIMUM_PAPER_LONG 4096
#define MAXIMUM_PAPER_MAGNITUDE 4096

void paper_read(const char *file_name, const size_t paper_number,
                       uint16_t *paper_size, char *paper_storage) ;

void paper_write(const char *file_name, const size_t paper_number,
                        uint16_t paper_size, char *paper_storage);
void text_file_addenda(const uint32_t text_long, const char *text,
                       const char *filename);
uint32_t /*text_long*/ text_file_read(const char *filename, uint32_t text_long, char *text);

void region_language_identity();
#endif
