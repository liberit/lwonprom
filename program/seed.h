/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*SPEL virtual machine
Copyright (C) 2016  Logan Streondj

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact: streondj at gmail dot com
*/
#ifndef SEED_H
#define SEED_H

#include "analyst.h"
#include "parser.h"
#include "pyash.h"
#include "pyashWords.h"

//#ifdef __APPLE__
//#include <OpenCL/opencl.h>
//#else
//#include <CL/cl.h>
//#endif
#define MAX_WRITE_MEMORY 0x8000 // GPU comput unit memory limit

void delete_empty_glyph(const uint16_t size, const char *letter,
                        uint16_t *fresh_size, char *tidyLetter);


void play(const v4us coded_name, v8us *hook_list);
void burden_hook_list(const uint8_t tablet_size, const line_t *tablet,
                      uint8_t *tablet_indexFinger, v4us *coded_name,
                      v8us *hook_list);
void play_independentClause(const uint8_t tablet_size, const line_t *tablet,
                            v4us *coded_name, v8us *hook_list);
void play_text(const uint16_t tablet_size, const line_t *tablet,
               v4us *coded_name, v8us *hook_list);
void derive_code_name(const uint8_t tablet_magnitude, const line_t *tablet,
                      v4us *code_name);

uint16_t line_t_read(const uint8_t indexFinger, const line_t vector);
void line_t_write(const uint8_t indexFinger, const uint16_t number,
                 line_t *vector);
uint64_t v4us_uint64_translation(const v4us vector);
void code_opencl_translate(const uint16_t recipe_magnitude, const line_t *recipe,
                           uint16_t *text_long, char *produce_text,
                           uint16_t *filename_long, char *filename,
                           uint16_t *file_sort);

void derive_filename(const uint16_t filename_long, const char *filename,
                     const uint16_t file_sort, uint16_t *gross_filename_long,
                     char *gross_filename);

uint8_t vector_long_find(uint16_t number);
uint16_t vector_code(uint8_t vector_long);
uint16_t vector_long_translate(uint16_t vector_code);
#endif
