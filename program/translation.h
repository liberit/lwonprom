/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef TRANSLATION_H
#define TRANSLATION_H
#include "math.h"
#include "found.h"
#include "prng.h"
#include "pyash.h"
#include "sort.h"
#include "compile.h"
#define code_name_t uint32_t


struct SortSeries {
  uint8_t length;
  uint8_t max_length;
  uint64_t* series;
};

#define NewSortSeries(__name, __size)  \
  uint64_t __name ## _series[__size] = {0}; \
  struct SortSeries __name; \
  __name.length = 0; \
  __name.max_length = __size; \
  __name.series = __name ## _series;\

#define _(STRING) STRING
// printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);

void word_code_translation(const uint16_t word_code, uint16_t *text_long,
                           char *text);
/* sorts accepted, brief, fluent (spaces after cases) and fancy (IPA) */
uint16_t tablet_translate(const uint8_t sequence_long, const line_t *tablet,
                          const uint16_t sort, const uint16_t text_long,
                          char *text);
void code_opencl_translate(const uint16_t recipe_magnitude,
                           const line_t *recipe, uint16_t *produce_text_long,
                           char *text, uint16_t *filename_long, char *filename,
                           uint16_t *file_sort);
void derive_code_name(const uint8_t tablet_magnitude, const line_t *tablet,
                      v4us *code_name);
void code_file_translation(const uint32_t dictionary_long,
                           const char *dictionary, const uint32_t code_long,
                           const line_t *code, uint32_t *produce_pad_long,
                           char *produce_pad);
void old_code_name_derive(const uint8_t tablet_long, const line_t *tablet,
                      const uint16_t input_tablet_indexFinger,
                      code_name_t *code_name,
                      uint16_t *produce_tablet_indexFinger);
code_name_t /* code_name */ neo_code_name_derive(struct Phrase input,
                                              uint16_t *neo_txik);
code_name_t code_name_derive(struct Phrase input);

int minimum(int x, int y);

struct Text /* produce */ page_translate(const struct Page input, uint16_t sort,
                                         struct Text produce_pad);

struct Text /* translated text */ neo_phrase_translate(const struct Phrase phrase, 
    struct Text text, uint16_t sort);

struct SortSeries sort_series_establish(struct Phrase input, 
    struct SortSeries sort_series);

uint64_t phrase_code_establish(const struct Phrase input);


/** derive quote length from quote word 
 * kyithkompwih kyitlyanka practu
 * **/
uint32_t kyitbrethkompwih_kyitlyanka_practu(struct Phrase);
#define derive_quote_byte_length kyitbrethkompwih_kyitlyanka_practu

uint8_t derive_scalar_thick_from_quote_code(word_t quote_code);
uint8_t derive_vector_thick_from_quote_code(word_t quote_code);
struct Phrase derive_paragraph_quote_phrase(struct Phrase input_phrase);

uint16_t number_to_text(const uint16_t number, const uint16_t base,
                        const uint16_t endian,
                        const uint16_t number_dictionary_long,
                        constant char number_dictionary[][NUMBER_WORD_LONG],
                        const uint16_t text_long, char text[]);
struct Text number_to_text_translate(const uint64_t number, const uint8_t base, 
    struct Text produce_text) ;
struct Text paragraph_letter_quote_translate(struct Phrase quote_phrase, 
    struct Text produce_text);
#endif
