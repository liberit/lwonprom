/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "lwonprom.h"
/* what does old_phrase_improve do?
 * changes number in phrase randomly depending on temperature
 * no change if temperature is 0 */
struct Phrase /* result_phrase, should be same as input */
phrase_improve(struct Climate climate, struct Phrase input_phrase) {
  if (climate.temperature == 0)
    return input_phrase;
  uint16_t mobility = (climate.temperature + 1) * 2; // needs to be even
  uint16_t gvak = 0;
  uint16_t variable = 0;
  txik_t place = 1;
  // check the quote type
  uint16_t quote_sort = read_thePhraseZWord_atThePlace(input_phrase, 0);
  switch (quote_sort) {
    example(NUMBER_QUOTE,
            gvak = read_thePhraseZWord_atThePlace(input_phrase, place););
  default:
    return input_phrase;
  }
  // number = return_thePhraseZNumber(input_phrase);
  variable = (seed_random(climate.random_seed) & (mobility - 1));
  // DEBUGPRINT("%d var\n", variable);
  if ((seed_random(climate.random_seed) & 1) == 0) {
    gvak = number_plus(gvak, variable);
  } else {
    gvak = (uint16_t)number_subtract(gvak, variable);
  }
  write_theWord_toThePhrase_atThePlace(gvak, input_phrase, place);
  // put_theNumber_toThePhrase(number, input_phrase);
  return input_phrase;
}

struct Paragraph /*neo person*/
neo_person_establish(const struct Climate climate,
                     const struct Paragraph recipe, struct Paragraph person) {
  page_zero(person.page);
  guarantee((PERSON_LONG & 1) == 0);
  uint8_t program_long = 0;
  uint8_t person_iteration = 0;
  guarantee(recipe.length < 0xFF);
  program_long =
      (uint8_t)modulo(seed_random(climate.random_seed), person.page.plength);
  program_long += 1;
  for (person_iteration = 0; person_iteration < program_long;
       ++person_iteration) {
    uint8_t recipe_line =
        (uint8_t)seed_random(climate.random_seed) % recipe.page.plength;
    recipe_improve(climate, recipe_line, recipe, person_iteration, person);
  }
  person.begin = 1;
  person.length = (person.page.plength * LINE_LONG - 1);
  person.length = phrase_long_diagnose(person);
  guarantee(((person.length | LINE_LONG_MASK) + 1) / LINE_LONG == program_long);
  guarantee(person.begin + person.length <= person.page.plength * LINE_LONG);
  guarantee(person.begin > 0);
  return person;
}

/*
temperature is for synthetic annealing,
 at higher temperatures there are more changes.
*/
struct Population /*new population*/
neo_population_establish(const struct Climate climate,
                         const struct Paragraph recipe,
                         struct Population population) {
  struct Climate thisClimate = climate;
  down_to_zero_repeat(population.length - 1,
                      seed_random(thisClimate.random_seed);
                      population.people[iterator] = neo_person_establish(
                          thisClimate, recipe, population.people[iterator]););
  return population;
}

int champion_comparison(const void *a, const void *b) {
  return (int)((*(uint64_t *)b >> POPULATION_TIDBIT_LONG) -
               (*(uint64_t *)a >> POPULATION_TIDBIT_LONG));
}
int champion_comparison_m(const uint64_t a, const uint64_t b) {
  return ((b >> POPULATION_TIDBIT_LONG) <= (a >> POPULATION_TIDBIT_LONG));
}

// A utility function to swap two elements
void swap(uint64_t *a, uint64_t *b) {
  uint64_t t = *a;
  *a = *b;
  *b = t;
}

/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
    array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */
int partition(uint64_t arr[], int low, int high) {
  uint64_t pivot = arr[high]; // pivot
  int i = (low - 1);          // Index of smaller element

  for (int j = low; j <= high - 1; j++) {
    // If current element is smaller than or
    // equal to pivot
    if (champion_comparison_m(arr[j], pivot)) // arr[j] <= pivot)
    {
      i++; // increment index of smaller element
      swap(&arr[i], &arr[j]);
    }
  }
  swap(&arr[i + 1], &arr[high]);
  return (i + 1);
}

/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(uint64_t arr[], int low, int high) {
  if (low < high) {
    /* pi is partitioning index, arr[p] is now
       at right place */
    int pi = partition(arr, low, high);

    // Separately sort elements before
    // partition and after partition
    quickSort(arr, low, pi - 1);
    quickSort(arr, pi + 1, high);
  }
}

/* Function to merge the two haves arr[l..m] and arr[m+1..r] of array arr[] */
void championMergeSortPart(generic uint64_t arr[], int l, int m, int r) {
  int i, j, k;
  int n1 = m - l + 1;
  int n2 = r - m;

  /* create temp arrays */
  // uint64_t L[n1], R[n2];
  uint64_t L[POPULATION_LONG], R[POPULATION_LONG];

  /* Copy data to temp arrays L[] and R[] */
  for (i = 0; i < n1; i++)
    L[i] = arr[l + i];
  for (j = 0; j < n2; j++)
    R[j] = arr[m + 1 + j];

  /* Merge the temp arrays back into arr[l..r]*/
  i = 0;
  j = 0;
  k = l;
  while (i < n1 && j < n2) {
    // if (L[i] <= R[j])
    // DEBUGPRINT("%d ccm, %lX, %lX\n", champion_comparison_m(L[i], R[j]),
    //     L[i], R[j]);
    if (champion_comparison_m(L[i], R[j])) {
      arr[k] = L[i];
      i++;
    } else {
      arr[k] = R[j];
      j++;
    }
    k++;
  }

  /* Copy the remaining elements of L[], if there are any */
  while (i < n1) {
    arr[k] = L[i];
    i++;
    k++;
  }

  /* Copy the remaining elements of R[], if there are any */
  while (j < n2) {
    arr[k] = R[j];
    j++;
    k++;
  }
}

/* Iterative mergesort function to sort arr[0...n-1] */
void championMergeSort(generic uint64_t arr[], int n) {
  int curr_size;  // For current size of subarrays to be merged
                  // curr_size varies from 1 to n/2
  int left_start; // For picking starting index of left subarray
                  // to be merged

  // Merge subarrays in bottom up manner.  First merge subarrays of
  // size 1 to create sorted subarrays of size 2, then merge subarrays
  // of size 2 to create sorted subarrays of size 4, and so on.
  for (curr_size = 1; curr_size <= n - 1; curr_size = 2 * curr_size) {
    // Pick starting point of different subarrays of current size
    for (left_start = 0; left_start < n - 1; left_start += 2 * curr_size) {
      // Find ending point of left subarray. mid+1 is starting
      // point of right
      int mid = left_start + curr_size - 1;

      int right_end = minimum(left_start + 2 * curr_size - 1, n - 1);

      // Merge Subarrays arr[left_start...mid] & arr[mid+1...right_end]
      championMergeSortPart(arr, left_start, mid, right_end);
    }
  }
}

void champion_choose(const uint8_t population_long,
                     const uint32_t *population_health,
                     uint64_t *champion_iteration_sequence) {
  // sort iteration sequence based on health
  guarantee(champion_iteration_sequence !=
            NULL); // assumed to be population_long
  guarantee(population_health != NULL);
  uint16_t iteration = 0;
  // uint16_t fittest_health = 0;
  // uint16_t fittest_iteration = 0; // expand to array for multiple fittest
  // uint16_t health = 0;
  for (iteration = 0; iteration < population_long; ++iteration) {
    // put iteration with health so can associate them for sorting
    champion_iteration_sequence[iteration] = iteration;
    // DEBUGPRINT("0x%lX champion_iteration_sequence[iteration]\n",
    // champion_iteration_sequence[iteration]);
    champion_iteration_sequence[iteration] |=
        ((uint64_t)population_health[iteration]) << sizeof(population_long) * 8;
    // DEBUGPRINT("0x%lX champion_iteration_sequence[iteration]\n",
    // champion_iteration_sequence[iteration]);
  }
  // uint iteration;
  // printf("\nbefore:\n");
  // sequence_print("%lX ", population_long, champion_iteration_sequence);
  // quickSort(champion_iteration_sequence, 0, population_long-1 );
  championMergeSort(champion_iteration_sequence, population_long);
  // qsort(champion_iteration_sequence, population_long, sizeof(uint64_t),
  //      champion_comparison);
  // printf("after:\n");
  // sequence_print("%lX ", population_long, champion_iteration_sequence);
  // printf(":after\n");

  for (iteration = 0; iteration < population_long; ++iteration) {
    champion_iteration_sequence[iteration] &=
        POPULATION_MASK; // keep just the iteration information
  }
}

uint16_t activity_comparison(const line_t activity,
                             const line_t comparison_activity) {
  uint8_t iteration = 0;
  uint8_t identity = 0;
  uint16_t deviation = 0;
  uint16_t one_activity = 0;
  uint16_t two_activity = 0;
  for (iteration = 0; iteration < V16_LONG; ++iteration) {
    one_activity = line_t_read(iteration, activity);
    two_activity = line_t_read(iteration, comparison_activity);
    if (one_activity == two_activity) {
      ++identity;
    } else {
      deviation += (uint16_t)absolute(one_activity - two_activity);
    }
  }
  if (identity == iteration) {
    return 0;
  }
  return deviation;
}
uint16_t /* deviation */ person_comparison(const line_t *person,
                                           const line_t *comparison_person) {
  // uint8_t iteration = 0;
  uint8_t person_iteration = 0;
  line_t activity = {V16_INIT};
  line_t comparison_activity = {V16_INIT};
  uint16_t deviation = 0;
  for (person_iteration = 0; person_iteration < PERSON_LONG;
       ++person_iteration) {
    line_copy(person[person_iteration], activity);
    line_copy(comparison_person[person_iteration], comparison_activity);
    deviation += activity_comparison(activity, comparison_activity);
  }
  return deviation;
}

void health_assess(const struct TrainingSequence training_sequence,
                   const struct Paragraph program,
                   struct Paragraph program_produce, uint32_t *health) {
  // text_phrase_print(program_produce);
  // Page_print(program_produce.page);
  // text_phrase_print(training_sequence.produce);
  // Page_print(training_sequence.produce.page);
  const uint8_t training_sequence_long = training_sequence.input.page.plength;
  // DEBUGPRINT(("%X training_sequence_long\n", training_sequence_long));
  guarantee(training_sequence_long > 0);
  guarantee(training_sequence_long == training_sequence.produce.page.plength);
  guarantee(training_sequence_long == program_produce.page.plength);
  guarantee(training_sequence.input.page.lines != NULL);
  guarantee(program_produce.page.lines != NULL);
  guarantee(health != NULL);
  uint16_t iteration = 0;
  uint16_t health_collector = 0;
  uint32_t incomplete_health_collector = INCOMPLETE_HEALTH_MASK;
  uint8_t program_economy = 0;
  uint8_t program_long = 0;
  uint8_t max_program_long = program.page.plength;
  uint8_t line_iteration = 0;
  //   Page_print(training_sequence.produce.page);
  //   Page_print(program_produce.page);
  for (iteration = 0; iteration < training_sequence_long; ++iteration) {
    // TODO fix incomplete health collector to fit within health mask
    // have to get the queried case
    // or simply difference between the two
    // DEBUGPRINT(("%X iteration\n", iteration));
    // DEBUGPRINT(("%X training_sequence_long\n", training_sequence_long));
    for (line_iteration = 0; line_iteration < LINE_LONG; ++line_iteration) {
      uint16_t training_produce_number =
          training_sequence.produce.page.lines[iteration][line_iteration];
      uint16_t program_produce_number =
          program_produce.page.lines[iteration][line_iteration];
      // DEBUGPRINT(("%X tpn, %X ppn\n", training_produce_number,
      //       program_produce_number));
      // if (training_produce_number == program_produce_number){
      //  health_collector += 1;
      //} else {
      incomplete_health_collector = (uint16_t)(
          number_subtract(incomplete_health_collector,
                          (uint64_t)absolute(program_produce_number -
                                             training_produce_number)));
      // DEBUGPRINT(("%X incomplete_health_collector\n",
      // incomplete_health_collector));
      //}
      // DEBUGPRINT(("%X health_collector, %X ihc, %X program_produce, "
      //            "  %X training_produce \n",
      //            health_collector, incomplete_health_collector,
      //            program_produce_number, training_produce_number));
    }
  }
  // DEBUGPRINT(("0x%X health\n", health_collector));
  program_long = program_long_diagnose(max_program_long, program.page.lines);
  // DEBUGPRINT(("%X program_long, %X max_program_long \n", program_long,
  //       max_program_long));
  // calculate program economy mask, so lower size has higher fitness.
  program_economy = PROGRAM_ECONOMY_MASK -
                    ((program_long * PROGRAM_ECONOMY_MASK) / max_program_long);
  *health = 0;
  //*health = (uint32_t)health_collector
  //          << (INCOMPLETE_HEALTH_TIDBIT_LONG + PROGRAM_ECONOMY_TIDBIT_LONG);
  *health += incomplete_health_collector << PROGRAM_ECONOMY_TIDBIT_LONG;
  *health += program_economy;
  // TODO health += inverse_of_mutation_algorithm_length
  // TODO probability_of_challenger_supplanting_champion = 2^(1/health);
  // DEBUGPRINT(("0x%X health\n", *health));
}

/**
 * @param training_sequence[2] -- [0] is the training input, and [1] is the
 *                                expected produce. note that the plength should
 *                                be equal to the number of training inputs, it
 *                                doesn't like empty inputs.
 * @param program_phrase -- is the program that is compiling it.
 */
struct Paragraph /*produce*/
program_quiz(const struct TrainingSequence training_sequence,
             const struct Paragraph program_phrase, struct Phrase produce) {
  guarantee(produce.page.lines != NULL);
  uint16_t iteration = 0;
  uint8_t training_sequence_long = training_sequence.length;
  NewPagePhrase(training_phrase, 0x10);
  guarantee(training_phrase.page.plength >= training_sequence.input_length);
  NewPagePhrase(produce_phrase, 0x4);
  const uint16_t input_length = training_sequence.input_length;
  page_zero(produce.page);
  struct Paragraph produce_region = produce;
  produce_region.page.plength = training_sequence.produce_length;
  for (iteration = 0; iteration < training_sequence_long; ++iteration) {
    produce_phrase.begin = 1;
    produce_phrase.length = 0;
    page_zero(produce_phrase.page);
    phrase_zero(produce_phrase);
    page_zero(training_phrase.page);
    repeat(training_sequence.input_length,
           line_copy(training_sequence.input.page
                         .lines[iteration * input_length + iterator],
                     training_phrase.page.lines[iterator]));
    training_phrase.length = LINE_LONG * input_length - 1;
    training_phrase.begin = 1;
    produce_phrase =
        knowledge_interpret(training_phrase, program_phrase, produce_phrase);
    guarantee(training_sequence.produce_length >= 1);
    guarantee(produce_phrase.length <=
              LINE_LONG * training_sequence.produce_length);
    produce_region.begin = 1;
    produce_region.length = 0;
    produce_region.page.lines =
        produce.page.lines + iteration * training_sequence.produce_length;
    produce_region =
        reform_theParagraph_byTheVariable(produce_region, produce_phrase);
  }
  produce.begin = 1;
  produce.length = produce.page.plength * LINE_LONG - 2;
  produce.length = phrase_long_diagnose(produce);
  return produce;
}

void population_quiz(const struct Population population,
                     const struct TrainingSequence training_sequence,
                     /* local */ struct Paragraph program_produce,
                     uint32_t *population_health) {
  guarantee(population.people[0].page.lines != NULL);
  guarantee(training_sequence.input.page.lines != NULL);
  guarantee(population_health != NULL);

  uint16_t iteration = 0;
  // line_t program[PERSON_LONG] = {0};
  uint32_t health = 0;
  struct Paragraph program;
  for (iteration = 0; iteration < population.length; ++iteration) {
    // DEBUGPRINT(
    //     ("%X population.length, %X iteration\n", population.length,
    //     iteration));
    phrase_zero(program_produce);
    page_zero(program_produce.page);
    // text_phrase_print(program_produce);
    program = population.people[iteration];
    // text_phrase_print(program);
    program_produce = program_quiz(training_sequence, program, program_produce);
    health_assess(training_sequence, program, program_produce, &health);
    // DEBUGPRINT(("%X health\n", health));
    population_health[iteration] = health;
    // text_phrase_print(program_produce);
    // DEBUGPRINT(
    //     ("%X population.length, %X iteration\n", population.length,
    //     iteration));
  }
}

void verb_read(const line_t program, uint8_t *verb_long,
               uint16_t *verb_produce) {

  line_t activity = {V16_INIT};
  uint16_t mood_indexFinger = 0;
  uint16_t verb_indexFinger = 0;
  uint16_t verb = {0};
  line_copy(program, activity);
  tablet_retrospective_grammar_read(V16_LONG - 1, 1, &activity,
                                    &mood_indexFinger);
  // DEBUGPRINT("0x%X mood_indexFinger\n", mood_indexFinger);
  verb = tablet_retrospective_word_read(mood_indexFinger - 1, 1, &activity,
                                        &verb_indexFinger);
  verb_produce[0] = verb;
  *verb_long = 1;
}
struct Phrase recipe_switch(struct Climate climate,
                            const struct Paragraph recipe,
                            struct Phrase fresh_phrase) {
  guarantee(fresh_phrase.begin / LINE_LONG <= fresh_phrase.page.plength);
  line_t fresh_recipe = {0};
  uint8_t recipe_line = seed_random(climate.random_seed) % recipe.page.plength;
  uint8_t phrase_line = fresh_phrase.begin / LINE_LONG;
  line_copy(recipe.page.lines[recipe_line], fresh_recipe);
  line_copy(fresh_recipe, fresh_phrase.page.lines[phrase_line]);
  fresh_phrase =
      recipe_improve(climate, recipe_line, recipe, phrase_line, fresh_phrase);
  // fresh_phrase.length = LINE_LONG;
  return fresh_phrase;
}

/** recipe merge,
 * merges two sentences that are next to each other
 */
struct Phrase recipe_merge(const struct Climate climate,
                           const struct Phrase person, const uint8_t iteration,
                           const uint16_t grammar_word,
                           struct Phrase fresh_recipe) {
  uint16_t phrase_begin = 0;
  uint8_t phrase_long = 0;
  uint16_t number_indexFinger = 0;
  uint16_t retrospective_phrase_begin = 0;
  uint8_t retrospective_phrase_long = 0;
  uint16_t retrospective_number_indexFinger = 0;
  uint16_t merge_number = 0;
  phrase_situate(1, &person.page.lines[iteration], grammar_word, &phrase_long,
                 &phrase_begin);
  phrase_situate(1, &person.page.lines[iteration - 1], grammar_word,
                 &retrospective_phrase_long, &retrospective_phrase_begin);
  // DEBUGPRINT("0x%X grammar_word, 0x%X phrase_begin, 0x%X
  // retrospective_phrase_begin\n",
  //    grammar_word, phrase_begin, retrospective_phrase_begin);
  if (phrase_begin > 0 && retrospective_phrase_begin > 0 &&
      retrospective_phrase_long > 2 && phrase_long > 2) {
    merge_number = tablet_upcoming_word_read(phrase_begin + 1, 1,
                                             &person.page.lines[iteration],
                                             &number_indexFinger);
    // DEBUGPRINT("0x%X pre merge_number\n", merge_number);
    switch (seed_random(climate.random_seed) % 1) {
    case 0: // add the numbers together
      merge_number += tablet_upcoming_word_read(
          retrospective_phrase_begin + 1, 1, &person.page.lines[iteration - 1],
          &retrospective_number_indexFinger);
      break;
    case 1: // subtract the previous number
      merge_number -= tablet_upcoming_word_read(
          retrospective_phrase_begin + 1, 1, &person.page.lines[iteration - 1],
          &retrospective_number_indexFinger);
      break;
    case 2: // xor the numbers
      merge_number ^= tablet_upcoming_word_read(
          retrospective_phrase_begin + 1, 1, &person.page.lines[iteration - 1],
          &retrospective_number_indexFinger);
      break;
    }
    // DEBUGPRINT("0x%X post merge_number\n", merge_number);
    if (seed_random(climate.random_seed) % 2 == 1) {
      line_copy(person.page.lines[iteration - 1], fresh_recipe.page.lines[0]);
      tablet_write(retrospective_number_indexFinger, merge_number, 1,
                   fresh_recipe.page.lines);
    } else {
      line_copy(person.page.lines[iteration], fresh_recipe.page.lines[0]);
      tablet_write(number_indexFinger, merge_number, 1,
                   fresh_recipe.page.lines);
    }
    // then merge the instrumental case;
  }
  fresh_recipe.length = LINE_LONG - 2;
  return fresh_recipe;
}

/** Person compression recipe merg
 * what does this do exactly? presumably compresses statements
 * @param {Climate} climate
 * @param {Paragraph} person
 * @param {Paragraph} fresh_person
 */
struct Paragraph
person_compression_recipe_merge(const struct Climate climate,
                                const struct Paragraph person,
                                struct Paragraph fresh_person) {
  guarantee(person.page.lines != fresh_person.page.lines);
  guarantee(fresh_person.page.plength >= person.page.plength);
  uint8_t iteration = 0;
  uint8_t max_program_long = person.page.plength;
  // uint64_t random_seed[] = {input_random_seed[0], input_random_seed[1]};
  uint8_t fresh_iteration = 0;
  NewPagePhrase(fresh_recipe, 1);
  uint8_t verb_long = 1;
  uint16_t verb[1] = {0};
  uint8_t retrospective_verb_long = 1;
  uint16_t retrospective_verb[1] = {0};
  // clear fresh_person
  for (iteration = 0; iteration < max_program_long; ++iteration) {
    line_t temporary = {0};
    line_copy(temporary, fresh_person.page.lines[iteration]);
  }
  for (iteration = 0;
       iteration < max_program_long && fresh_iteration < max_program_long;
       ++iteration) {
    if (person.page.lines[iteration][0] == 0) {
      line_t temporary = {0};
      line_copy(temporary, fresh_person.page.lines[fresh_iteration]);
      break;
    }
    // merge recipe
    if ((seed_random(climate.random_seed) % TRANSMUTATION_PROBABILITY_MASK <=
             climate.radioactivity &&
         fresh_iteration > 0)) {
      // if previous fresh_iteration has same verb
      // code_name_derive(iteration +1, program, tablet_indexFinger,
      //    &code_name, &tablet_indexFinger);
      verb_read(person.page.lines[iteration], &verb_long, verb);
      // TODO: use code_name_derive instead of verb_read
      // code_name_derive(iteration + 1, person, tablet_indexFinger,
      // &code_name,
      //                 &tablet_indexFinger);
      verb_read(person.page.lines[iteration - 1], &retrospective_verb_long,
                retrospective_verb);
      // verb_read(fresh_recipe.page.lines[fresh_iteration],
      //    &fresh_verb_long, fresh_verb);
      if (retrospective_verb[0] == verb[0]) {
        // and has an instrumental case
        //
        // if (seed_random(climate.random_seed) % 2 == 1) {
        // merge one of whichever they both have one of.
        fresh_recipe = recipe_merge(climate, person, iteration,
                                    instrumental_case_GRAMMAR, fresh_recipe);
        --fresh_iteration;
        line_copy(fresh_recipe.page.lines[0],
                  fresh_person.page.lines[fresh_iteration]);
        // DEBUGPRINT(("%X fresh_iteration\n", fresh_iteration));
        // DEBUGPRINT(("%X iteration\n", iteration));
        // text_page_print(fresh_person.page);
      }
      // check if previous is inverse of this one, if so then remove
      // if (invert(retrospective_verb[0] == verb[0]) {
      if (fresh_person.page.lines[fresh_iteration][0] == 0) {
        line_copy(person.page.lines[iteration], fresh_recipe.page.lines[0]);
        line_copy(fresh_recipe.page.lines[0],
                  fresh_person.page.lines[fresh_iteration]);
      }
    } else {
      line_copy(person.page.lines[iteration], fresh_recipe.page.lines[0]);
      line_copy(fresh_recipe.page.lines[0],
                fresh_person.page.lines[fresh_iteration]);
    }
    // DEBUGPRINT("0x%X fresh_iteration, fresh_person:\n",
    // fresh_iteration);
    // tablet_print(max_program_long, fresh_person);
    ++fresh_iteration;
  }
  fresh_person.length = LINE_LONG * page_long_diagnose(fresh_person.page);
  return fresh_person;
}

void old_recipe_delete(const uint8_t max_program_long, const line_t *person,
                       const uint8_t recipe_number, line_t *fresh_person) {
  guarantee(recipe_number < max_program_long);
  uint8_t iteration = 0;
  uint8_t fresh_iteration = 0;
  for (; iteration < max_program_long; ++iteration) {
    if (iteration != recipe_number) {
      line_copy(person[iteration], fresh_person[fresh_iteration]);
      ++fresh_iteration;
    }
  }
}

struct Paragraph recipe_delete(const struct Paragraph person,
                               const uint8_t recipe_number,
                               struct Paragraph fresh_person) {
  guarantee(fresh_person.page.plength >= person.page.plength - 1);
  uint8_t max_program_long = person.page.plength;
  guarantee(recipe_number < max_program_long);
  uint8_t iteration = 0;
  uint8_t fresh_iteration = 0;
  for (; iteration < max_program_long; ++iteration) {
    if (iteration != recipe_number) {
      line_copy(person.page.lines[iteration],
                fresh_person.page.lines[fresh_iteration]);
      ++fresh_iteration;
    }
  }
  fresh_person.begin = person.begin;
  fresh_person.length = person.length - LINE_LONG;
  return fresh_person;
}

struct Paragraph /*produce*/ person_copy(generic const struct Paragraph input,
                                         generic struct Paragraph produce) {
  guarantee(input.page.plength <= produce.page.plength);
  guarantee(input.begin + input.length <= input.page.plength * LINE_LONG);
  uint8_t activity_iteration = 0;
  for (activity_iteration = 0; activity_iteration < input.page.plength;
       ++activity_iteration) {
    line_copy(input.page.lines[activity_iteration],
              produce.page.lines[activity_iteration]);
  }
  produce.begin = input.begin;
  produce.length = input.length;
  guarantee(produce.begin + produce.length <= produce.page.plength * LINE_LONG);
  return produce;
}

struct Population champion_copy(const struct Population population,
                                const uint64_t *champion_iteration_sequence,
                                const uint8_t population_ration,
                                struct Population fresh_population) {
  guarantee(population_ration < population.length);
  guarantee(fresh_population.people != NULL);
  uint8_t iteration = 0;
  for (iteration = 0; iteration < population_ration; ++iteration) {
    fresh_population.people[iteration] =
        person_copy(population.people[champion_iteration_sequence[iteration]],
                    fresh_population.people[iteration]);
  }
  return fresh_population;
}

/** changes or mutates a sentence(htin), using various methods
 * @param {Climate} climate
 * @param {Sentence} recipe -- possible new sentences to work with
 * @param {Sentence} htin -- the original htin
 * @param {Sentence} fresh_htin -- the mutatated htin
 * @return fresh_htin
 */
struct Sentence htin_transmutation(const struct Climate climate,
                                   const struct Sentence recipe,
                                   const struct Sentence input_htin,
                                   struct Sentence fresh_htin) {
  // DEBUGPRINT(("%X climate.temperature, %X climate.radioactivity, "
  //      " 0x%lX 0x%lX climate.random_seed\n",
  //      climate.temperature, climate.radioactivity, climate.random_seed[0],
  //      climate.random_seed[1]));
  // text_phrase_print(recipe);
  // Page_print(recipe.page);
  // text_phrase_print(input_htin);
  // Page_print(input_htin.page);
  struct Phrase htin = input_htin;
  guarantee((htin.length + htin.begin) <= htin.page.plength * LINE_LONG);
  guarantee(htin.page.lines != fresh_htin.page.lines);
  struct Climate fresh_climate = climate;
  seed_random(fresh_climate.random_seed);
  line_t fresh_recipe = {V16_INIT};
  uint16_t grow_probability = 0;
  uint8_t branch = 0;
  uint8_t max_branch = 5;
  uint8_t iteration = htin.begin / LINE_LONG;
  uint8_t fresh_iteration = fresh_htin.begin / LINE_LONG;
  guarantee(iteration < htin.page.plength);
  guarantee(fresh_iteration < fresh_htin.page.plength);
  uint8_t program_long = 1;
  // switch recipe
  if (fresh_climate.temperature < 0x20) {
    max_branch = 2;
  } else if (fresh_climate.temperature < 0x40) {
    max_branch = 3;
  } else if (fresh_climate.temperature < 0x60) {
    max_branch = 4;
  }
  repeat(
      0x5,
      if (seed_random(fresh_climate.random_seed) %
              TRANSMUTATION_PROBABILITY_MASK <
          fresh_climate.radioactivity + grow_probability) {
        branch = seed_random(fresh_climate.random_seed) % max_branch;
      } else { branch = 0; } htin.length = phrase_long_diagnose(htin);
      conditional(htin.length == 0, branch = 4); uint8_t recipe_line = 0;
      // DEBUGPRINT(("%X branch\n", branch));
      switch (branch) {
        case 0: // copy recipe
          line_copy(htin.page.lines[iteration], fresh_recipe);
          line_copy(fresh_recipe, fresh_htin.page.lines[fresh_iteration]);
          fresh_htin.length = phrase_long_diagnose(fresh_htin);
          fresh_htin = recipe_improve(fresh_climate, iteration, htin,
                                      fresh_iteration, fresh_htin);
          break;
        case 1: // improve recipe
          fresh_htin = recipe_improve(fresh_climate, iteration, htin,
                                      fresh_iteration, fresh_htin);
          break;
        case 2:
          // delete recipe
          // recipe_delete(max_program_long, htin, iteration, fresh_htin);
          if (program_long > 1) {
            if (fresh_iteration > 0) {
              --fresh_iteration;
            }
          } else {
            fresh_htin = recipe_improve(fresh_climate, iteration, htin,
                                        fresh_iteration, fresh_htin);
          }
          break;
        case 3:
          // recipe switch
          fresh_htin = recipe_switch(fresh_climate, recipe, fresh_htin);
          break;
        case 4:
          // insert recipe
          recipe_line =
              seed_random(fresh_climate.random_seed) % recipe.page.plength;
          fresh_htin = recipe_improve(climate, recipe_line, recipe,
                                      fresh_iteration, fresh_htin);
          break;
        default:
          guarantee(1 == 0);
      } conditional(fresh_htin.length != 0 && fresh_htin.page.lines[0][0] != 0,
                    break););
  guarantee(fresh_htin.length > 0 && fresh_htin.page.lines[0][0] != 0);
  guarantee(fresh_htin.page.lines[0][1] != 0);
  return fresh_htin;
}

/** changes or mutates a program(person), using various methods
 * @param {Climate} climate
 * @param {Paragraph} recipe -- possible new sentences to work with
 * @param {Paragraph} person -- the original person
 * @param {Paragraph} fresh_person -- the mutatated person
 * @return fresh_person
 */
struct Paragraph person_transmutation(const struct Climate climate,
                                      const struct Paragraph recipe,
                                      const struct Paragraph person,
                                      struct Paragraph fresh_person) {
  // for each point in program, change recipe based on
  // transmutation_probability
  // uint64_t random_seed[] = { input_random_seed[0], input_random_seed[1]};
  guarantee(person.page.lines != NULL);
  guarantee(person.page.lines != fresh_person.page.lines);
  guarantee(fresh_person.page.plength >= person.page.plength);
  guarantee(person.begin + person.length <= person.page.plength * LINE_LONG);
  uint8_t iteration = 0;
  uint8_t fresh_iteration = 0;
  uint8_t reiterate = 0;
  uint8_t max_reiterate = 0x20; // how many times to try changing the person
  uint8_t program_long =
      program_long_diagnose(person.page.plength, person.page.lines);
  // DEBUGPRINT("0x%X program_long, 0x%X temperature\n", program_long,
  // temperature);
  // line_t_print(person[0]);
  //
  // should we grow the program length, reduce it, or keep it the same?
  const uint8_t max_program_long = person.page.plength;
  //  const uint8_t program_vacancy  = max_program_long - program_long;
  struct Climate fresh_climate = climate;
  if (climate.temperature > 0x40) {
    // then can increase or decrease or stay the same.
    const uint8_t extend_branch = seed_random(climate.random_seed) % 3;
    switch (extend_branch) {
      example(0, program_long -= program_long > 1 ? 1 : 0); // decreasing
      example(1, );                                         // staying the same
      example(2, program_long +=
                 program_long < max_program_long ? 1 : 0); // increasing
    }
  } else {
    // then can only decrease or stay the same.
    const uint8_t extend_branch = seed_random(climate.random_seed) % 2;
    switch (extend_branch) {
      example(0, program_long -= program_long > 1 ? 1 : 0); // decreasing
      example(1, );                                         // staying the same
    }
  }
  // DEBUGPRINT(("%X program_long\n", program_long));
  struct Sentence sentence = person;
  NewPagePhrase(fresh_sentence, 2);
  seed_random(fresh_climate.random_seed);
  if (program_long == 0) {
    return neo_person_establish(fresh_climate, recipe, fresh_person);
  }
  // text_phrase_print(person);
  guarantee(max_program_long <= fresh_person.page.plength);
  phrase_zero(fresh_person);
  fresh_person.begin = 1;
  page_zero(fresh_person.page);
  for (reiterate = 0; reiterate < max_reiterate; ++reiterate) {
    fresh_iteration = 0;
    for (iteration = 0;
         iteration < program_long && fresh_iteration < program_long;
         ++iteration) { // a change the person loop
      // transmute a line or phrase
      seed_random(fresh_climate.random_seed);
      sentence.begin = iteration * LINE_LONG + 1;
      sentence.length = LINE_LONG - 1;
      phrase_zero(fresh_sentence);
      fresh_sentence =
          htin_transmutation(fresh_climate, recipe, sentence, fresh_sentence);
      ++fresh_iteration;
      guarantee(fresh_sentence.page.lines[0][1] != 0);
      fresh_person = phrase_addenda(fresh_sentence, fresh_person);
      guarantee(fresh_person.page.lines[fresh_person.length / LINE_LONG][1] !=
                0);
    }
    if (reiterate == max_reiterate - 1) {
      // DEBUGPRINT("0x%X transmutation_probability may be low\n",
      //           transmutation_probability);
      // text_phrase_print(fresh_person);
      // DEBUGPRINT(("fails inspection\n"));
      break;
      // return fresh_person;
    }
    if (phrase_to_phrase_compare(person, fresh_person) == lie_WORD &&
        fresh_person.page.lines[0][0] != 0 && fresh_person.length != 0) {
      // grow_probability += 0x1000;
      // fresh_climate.temperature += 0x10;
      // change random seed
      break;
    } else {
      phrase_zero(fresh_person);
      page_zero(fresh_person.page);
    }
  }
  // fresh_person.begin = 1;
  // fresh_person.length = fresh_person.page.plength * LINE_LONG -1;
  // fresh_person.length = phrase_long_diagnose(fresh_person);
  guarantee(fresh_person.begin + fresh_person.length <=
            fresh_person.page.plength * LINE_LONG);
  return fresh_person;
}

struct Paragraph
person_compression_recipe_delete(const struct Climate climate,
                                 const struct Paragraph person,
                                 struct Paragraph fresh_person) {
  guarantee(fresh_person.page.plength >= person.page.plength);
  const uint8_t max_program_long = person.page.plength;
  uint8_t iteration = 0;
  // uint64_t random_seed[] = {input_random_seed[0], input_random_seed[1]};
  uint8_t fresh_iteration = 0;
  uint16_t grow_probability = 0;
  line_t fresh_recipe = {0};
  uint8_t reiterate = 0;
  uint8_t max_reiterate = 0xF;
  fresh_person.begin = person.begin;
  fresh_person.length = person.length;
  for (reiterate = 0; reiterate < max_reiterate; ++reiterate) {
    for (iteration = 0;
         iteration < max_program_long && fresh_iteration < max_program_long;
         ++iteration) {
      if (person.page.lines[iteration][0] == 0) {
        line_t temporary = {0};
        line_copy(temporary, fresh_person.page.lines[fresh_iteration]);
        break;
      }
      // delete recipe
      if (seed_random(climate.random_seed) % TRANSMUTATION_PROBABILITY_MASK <=
              climate.radioactivity &&
          fresh_iteration > 0) { // remove a line
        --fresh_iteration;
        fresh_person.length -= LINE_LONG;
      } else {
        line_copy(person.page.lines[iteration], fresh_recipe);
        line_copy(fresh_recipe, fresh_person.page.lines[fresh_iteration]);
      }
      ++fresh_iteration;
    }
    if (phrase_to_phrase_compare(person, fresh_person) == 0) {
      grow_probability += 0x800;
    } else {
      break;
    }
  }
  if (reiterate == max_reiterate) {
    // how many to delete
    uint8_t delete_long = seed_random(climate.random_seed) % max_program_long;
    uint8_t recipe_number = 0;
    for (iteration = 0; iteration <= delete_long; ++iteration) {
      recipe_number =
          seed_random(climate.random_seed) % (max_program_long - iteration);
      fresh_person = recipe_delete(person, recipe_number, fresh_person);
    }
  }
  return fresh_person;
}

struct Paragraph person_compression(const struct Climate climate,
                                    const struct Paragraph recipe,
                                    const struct Paragraph person,
                                    struct Paragraph fresh_person) {
  guarantee(fresh_person.page.plength >= person.page.plength);
  guarantee(person.begin + person.length <= person.page.plength * LINE_LONG);
  // for each point in program, change recipe based on
  // transmutation_probability
  uint8_t reiterate = 0;
  uint8_t max_reiterate = 0x10;
  // uint64_t random_seed[] = {input_random_seed[0], input_random_seed[1]};
  uint16_t grow_probability = 0;
  uint8_t branch = 0;
  uint64_t *random_seed = climate.random_seed;
  struct Climate fresh_climate = climate;
  NewPagePhrase(temporary_person, 0x10);
  guarantee(fresh_person.page.plength <= 0x10);
  temporary_person.page.plength = fresh_person.page.plength;
  const uint8_t max_branch =
      climate.temperature >= 0x30 ? 3 : climate.temperature / 0x10 + 1;
  // DEBUGPRINT(("%X max_branch\n", max_branch));
  for (reiterate = 0; reiterate < max_reiterate; ++reiterate) {
    branch = (seed_random(random_seed) % max_branch);
    // DEBUGPRINT(("%X branch, %lX%lX, %Xseed_random(random_seed) % 3\n",
    // branch, random_seed[0], random_seed[1],
    //       seed_random(random_seed) % 3));
    switch (branch) {
    case 0:
      fresh_person = person_copy(person, fresh_person);
      break;
    case 1:
      fresh_climate.radioactivity += grow_probability;
      person_compression_recipe_delete(fresh_climate, person, fresh_person);
      break;
    case 2:
      fresh_climate.radioactivity += grow_probability;
      fresh_person =
          person_transmutation(fresh_climate, recipe, person, fresh_person);
      break;
    default:
      guarantee(1 == 0);
    }
    struct Climate new_climate = climate;
    new_climate.radioactivity += grow_probability;
    // text_phrase_print(fresh_person);
    // text_phrase_print(person);
    temporary_person = person_copy(fresh_person, temporary_person);
    // text_phrase_print(temporary_person);
    fresh_person = person_compression_recipe_merge(
        new_climate, temporary_person, fresh_person);
    // text_phrase_print(fresh_person);
    seed_random(random_seed);
    if (phrase_to_phrase_compare(person, fresh_person) == 0 ||
        fresh_person.page.lines[0][0] == 0) {
      grow_probability += 0x800;
    } else {
      break;
    }
  }
  if (reiterate == max_reiterate) {
    // DEBUGPRINT("0x%X transmutation_probability may be low\n",
    //           transmutation_probability);
  }
  fresh_person.begin = person.begin;
  fresh_person.length = phrase_long_diagnose(fresh_person);
  guarantee(fresh_person.begin + fresh_person.length <=
            fresh_person.page.plength * LINE_LONG);
  return fresh_person;
}

struct Population champion_compression(const struct Climate climate,
                                       const struct Paragraph recipe,
                                       const struct Paragraph champion_program,
                                       struct Population fresh_population) {
  const struct Paragraph person = champion_program;
  // uint64_t random_seed[] = {input_random_seed[0], input_random_seed[1]};

  uint8_t iteration = 0;
  uint16_t max_iteration = 0;
  fresh_population.people[0] =
      person_copy(champion_program, fresh_population.people[0]);
  struct Climate fresh_climate = climate;
  for (iteration = 1; iteration < fresh_population.length; ++iteration) {
    // text_phrase_print(champion_program);
    struct Paragraph fresh_person = fresh_population.people[iteration];
    // fresh_climate.radioactivity += grow_probability;
    seed_random(fresh_climate.random_seed);
    fresh_person = person_compression(fresh_climate, recipe, champion_program,
                                      fresh_person);
    // text_phrase_print(fresh_person);
    // DEBUGPRINT(("%X fresh_climate.radioactivity, %X iteration\n",
    //             fresh_climate.radioactivity, iteration));
    // if clone, try again
    if (phrase_to_phrase_compare(person, fresh_person) == truth_WORD) {
      --iteration;
      fresh_climate.radioactivity += 0x800;
    } else {
      fresh_population.people[iteration] = fresh_person;
    }
    ++max_iteration;
    if (max_iteration >= fresh_population.length) {
      // DEBUGPRINT(("0x%X transmutation_probability may be low\n",
      //             fresh_climate.radioactivity));
      break;
    }
  }
  guarantee(phrase_to_phrase_compare(champion_program,
                                     fresh_population.people[0]) == truth_WORD);
  return fresh_population;
}

struct Population population_transmutation(const struct Climate climate,
                                           const struct Paragraph recipe,
                                           const struct Population population,
                                           struct Population fresh_population) {
  guarantee(fresh_population.length >= population.length);
  guarantee(fresh_population.people[0].page.plength >=
            population.people[0].page.plength);
  struct Paragraph person;
  // uint64_t random_seed[] = {input_random_seed[0], input_random_seed[1]};
  struct Paragraph fresh_person;
  uint8_t iteration = 0;
  uint16_t max_iteration = 0;
  struct Climate fresh_climate = climate;
  uint16_t reiteration = 0;
  struct Paragraph retrospective_person;
  for (iteration = 0;
       iteration < population.length && reiteration < population.length * 2;
       ++iteration) {
    person = population.people[iteration];
    retrospective_person = iteration == 0
                               ? population.people[population.length - 1]
                               : population.people[iteration - 1];
    fresh_person = fresh_population.people[iteration];
    phrase_zero(fresh_person);
    fresh_person =
        person_transmutation(fresh_climate, recipe, person, fresh_person);
    guarantee(fresh_person.length > 0);
    guarantee(fresh_person.page.lines[0][0] != 0);
    guarantee(phrase_letters_compare(fresh_person, "") == lie_WORD);
    seed_random(fresh_climate.random_seed);
    // if clone, try again
    if (phrase_to_phrase_compare(person, fresh_person) == truth_WORD ||
        phrase_to_phrase_compare(fresh_person, retrospective_person) ==
            truth_WORD) {
      --iteration;
      fresh_population.people[iteration] = fresh_person;
      seed_random(fresh_climate.random_seed);
      ++reiteration;
    } else {
      fresh_population.people[iteration] = fresh_person;
      guarantee(fresh_population.people[iteration].length > 0);
    }
    ++max_iteration;
    if (max_iteration == 0xFF) {
      DEBUGPRINT(("0x%X transmutation_probability may be low\n",
                  fresh_climate.radioactivity));
      break;
    }
  }
  return fresh_population;
}

struct Population
population_refresh(const struct Climate climate, const struct Paragraph recipe,
                   const uint64_t *champion_iteration_sequence,
                   const struct Population population,
                   struct Population fresh_population) {
  /* 80:20 rule
   * we keep the top 20% of the population for the next generation.
   * each has 3 kids (60% of the next generation).
   * and 20% of the next generation is randomly generated.
   */

  guarantee(population.length % 5 == 0); // population must be divisible by 5
  guarantee(fresh_population.length >=
            population.length); // population must be divisible by 5
  guarantee(fresh_population.people[0].page.plength >=
            population.people[0].page.plength);
  guarantee(fresh_population.people[0].page.lines !=
            population.people[0].page.lines);
  // copy the champions
  // uint64_t random_seed[] = {input_random_seed[0], input_random_seed[1]};
  uint8_t population_ration = population.length / 5;
  uint8_t iteration = 0;
  // is the people variable the same for fresh_population? do we need another
  // one?
  // need a separate array for it or something... could use the original
  // population

  struct Population champion_population = fresh_population;
  struct Paragraph champion_people[0xFF];
  uint16_t fresh_population_begin = 0;
  champion_population.people = champion_people;
  /// CHAMPION COPIES FIRST POPULATION SEGMENT
  repeat(population_ration,
         if (population.people[iterator].begin +
                 population.people[iterator].length >
             population.people[iterator].page.plength * LINE_LONG) {
           DEBUGPRINT(("%X population.people[iterator].begin"
                       " %X population.people[iterator].length \n",

                       population.people[iterator].begin,
                       population.people[iterator].length));
         } guarantee(population.people[iterator].begin +
                         population.people[iterator].length <=
                     population.people[iterator].page.plength * LINE_LONG);
         champion_population.people[iterator] =
             population.people[champion_iteration_sequence[iterator]]);
  fresh_population = champion_copy(population, champion_iteration_sequence,
                                   population_ration, fresh_population);
  champion_population.length = population_ration;
  // text_phrase_print(champion_population.people[0]);
  // text_phrase_print(population.people[champion_iteration_sequence[0]]);
  // guarantee(champion_population.people[0].page.lines !=
  //     fresh_population.people[champion_iteration_sequence[0]].page.lines);
  // each champion has three children
  // if temperature below 0x80 then 4
  /// SECOND POPULATION SEGMENT, Champion Children
  uint8_t children = 3;
  if (climate.temperature < 0x80) {
    children = 4;
  }
  struct Climate fresh_climate = climate;
  struct Paragraph ration_people[0xFF];
  struct Population fresh_population_ration = fresh_population;
  fresh_population_ration.people = ration_people;
  fresh_population_ration.length = population_ration;
  for (iteration = 0; iteration < children; ++iteration) {
    // DEBUGPRINT(("%X iteration\n", iteration));
    // text_phrase_print(fresh_population.people[0]);
    guarantee(phrase_to_phrase_compare(
                  fresh_population.people[0],
                  population.people[champion_iteration_sequence[0]]) ==
              truth_WORD);
    seed_random(fresh_climate.random_seed);
    // DEBUGPRINT(
    //    ("%lX fresh_climate.random_seed\n", fresh_climate.random_seed[0]));
    fresh_population_begin = (population_ration * (1 + iteration));
    // text_phrase_print(fresh_population.people[0]);
    repeat(population_ration,
           fresh_population_ration.people[iterator] =
               fresh_population.people[fresh_population_begin + iterator];
           // DEBUGPRINT(("%X pop_ration, %lX \n", population_ration,
           //            fresh_population_begin + iterator));
    );
    // text_phrase_print(fresh_population.people[0]);
    guarantee(&fresh_population_ration.people[0].page.lines[0] !=
              &champion_population.people[0].page.lines[0]);
    // text_phrase_print(champion_population.people[0]);
    // text_phrase_print(fresh_population.people[0]);
    fresh_population_ration = population_transmutation(
        fresh_climate, recipe, champion_population, fresh_population_ration);
    guarantee(fresh_population_ration.people[0].length > 0);
    // DEBUGPRINT(("%lX fresh_population_ration.people\n",
    //             fresh_population_ration.people));
    repeat(population_ration,
           fresh_population.people[fresh_population_begin + iterator] =
               fresh_population_ration.people[iterator]);
    guarantee(&fresh_population_ration.people[0].page.lines[0] !=
              &champion_population.people[0].page.lines[0]);
    // text_phrase_print(champion_population.people[0]);
    // text_phrase_print(fresh_population_ration.people[0]);
  }
  // text_phrase_print(champion_population.people[0]);
  // generate new people
  fresh_population_ration.people =
      &fresh_population.people[population_ration * 4];
  seed_random(fresh_climate.random_seed);
  // DEBUGPRINT((
  //     "%lX fresh_population_ration.people, %X
  //     fresh_population_ration.length\n", fresh_population_ration.people,
  //     fresh_population_ration.length));
  if (climate.temperature >= 0x80) {
    neo_population_establish(fresh_climate, recipe, fresh_population_ration);
  }
  return fresh_population;
}

struct Population /* produce */ population_copy(const struct Population input,
                                                struct Population produce) {
  uint8_t iteration = 0;
  for (iteration = 0; iteration < input.length; ++iteration) {
    produce.people[iteration] =
        person_copy(input.people[iteration], produce.people[iteration]);
  }
  return produce;
}

/**
 * if not then generates a fresh population based on j
 * @return {Paragraph} -- if population  then champion found,
 *                      fresh_population then not found;
 */
struct Population evolve_generation(
    const struct Climate climate, const struct Paragraph recipe,
    const struct TrainingSequence training_sequence,
    const struct Population population, struct Population fresh_population,
    uint32_t population_health[], uint64_t champion_iteration_sequence[]) {
  guarantee(fresh_population.people[0].page.lines !=
            population.people[0].page.lines);
  guarantee(fresh_population.people[0].page.plength >=
            population.people[0].page.plength);
  guarantee(training_sequence.input.page.plength < 0xFF);
  NewPagePhrase(program_produce, 0xFF);
  program_produce.page.plength = training_sequence.input.page.plength;
  struct Climate fresh_climate = climate;
  population_quiz(population, training_sequence, program_produce,
                  population_health);
  champion_choose(population.length, population_health,
                  champion_iteration_sequence);
  seed_random(fresh_climate.random_seed);
  fresh_population =
      population_refresh(fresh_climate, recipe, champion_iteration_sequence,
                         population, fresh_population);
  return fresh_population;
}

struct Population island_champion_compression(
    uint8_t repeat_count, struct Climate climate, struct Paragraph recipe,
    struct Population population, struct Population fresh_population,
    const struct TrainingSequence training_sequence, uint32_t population_health[],
    uint64_t champion_iteration_sequence[]) {

  uint16_t champion_iteration = champion_iteration_sequence[0];
  uint8_t program_long = program_long_diagnose(
      PERSON_LONG, population.people[champion_iteration].page.lines);
  const uint16_t final_health = population_health[champion_iteration];
  struct Climate fresh_climate = climate;
  struct Paragraph champion;
  uint16_t champion_compression_iteration = 0;
  NewPagePhrase(program_produce, 0xFF);
//  DEBUGPRINT(("%X  training_sequence.produce.page.plength \n", 
//training_sequence.produce.page.plength));
  guarantee(training_sequence.produce.page.plength <= 0xFF);
  program_produce.page.plength = training_sequence.produce.page.plength;
        population_quiz(population, training_sequence, program_produce,
                        population_health);
        champion_choose(population.length, population_health,
                        champion_iteration_sequence);
  repeat(
      repeat_count /* this number is arbitrary */,
      champion_iteration = champion_iteration_sequence[0];
      //text_phrase_print(population.people[champion_iteration_sequence[0]]);
      program_long = program_long_diagnose(
          PERSON_LONG, population.people[champion_iteration].page.lines);
      if (program_long == 1) {
        // text_phrase_print(population.people[champion_iteration]);
        break;
      } else {
        fresh_population = champion_compression(
            climate, recipe, population.people[champion_iteration],
            fresh_population);
        seed_random(fresh_climate.random_seed);
        population_copy(fresh_population, population);
        population_quiz(population, training_sequence, program_produce,
                        population_health);
        champion_choose(population.length, population_health,
                        champion_iteration_sequence);
        champion_iteration = champion_iteration_sequence[0];
        champion =  population.people[champion_iteration];
        //text_phrase_print(champion);
        //DEBUGPRINT(("%X health\n", population_health[champion_iteration]));
        ++champion_compression_iteration;
      });
        champion_iteration = champion_iteration_sequence[0];
        champion =  population.people[champion_iteration];
        //text_phrase_print(champion);
        //program_produce = 
        //program_quiz(training_sequence, champion,
        //    program_produce);
        //health_assess(training_sequence, champion, program_produce, 
        //    population_health + champion_iteration);
     //   DEBUGPRINT(("%X health\n", population_health[champion_iteration]));
     // DEBUGPRINT(("%X population_health[champion_iteration], %X final_health\n",
     //             population_health[champion_iteration], final_health));
      guarantee(population_health[champion_iteration] >= final_health);
  //text_phrase_print(champion);
  return population;
}

struct Population evolutionary_island_establish(
    const struct Climate climate, const struct Paragraph recipe,
    const struct TrainingSequence training_sequence,
    struct Population population, struct Population fresh_population,
    uint32_t population_health[], uint64_t champion_iteration_sequence[]) {
  struct Climate fresh_climate = climate;
  printf("0x%lX island begin\n", fresh_climate.random_seed[0]);
  guarantee("temperature > 0");
  // DEBUGPRINT(("temperature > 0 %d", temperature));
  neo_population_establish(fresh_climate, recipe, population);
  NewPagePhrase(program_produce, 0xFF);
  guarantee(training_sequence.produce.page.plength <= 0xFF);
  // DEBUGPRINT();
  program_produce.page.plength = training_sequence.produce.page.plength;
#define MAX_GENERATION 0xFF
  // evolution
  uint16_t max_length = 0;
  repeat(
      MAX_GENERATION, seed_random(fresh_climate.random_seed);
      fresh_population = evolve_generation(
          fresh_climate, recipe, training_sequence, population,
          fresh_population, population_health, champion_iteration_sequence);
      population_copy(fresh_population, population);
      if ((population_health[champion_iteration_sequence[0]] >>
           (PROGRAM_ECONOMY_TIDBIT_LONG)) == INCOMPLETE_HEALTH_MASK) {
        break;
      } if (fresh_climate.temperature > 0) {
        --fresh_climate.temperature;
      } uint16_t second_iterator = 0;
      repeat_with(population.length, second_iterator,
                  max_length =
                      max(population.people[iterator].length, max_length);
                  // if (population.people[iterator].length > 0x30) {
                  // text_phrase_print(population.people[iterator]);
                  //}
      ););
  //DEBUGPRINT(("%X max_length\n", max_length));
  population_quiz(population, training_sequence, program_produce,
                  population_health);
  champion_choose(population.length, population_health,
                  champion_iteration_sequence);
  // compression
  // uint64_t champion_health =
  // population_health[champion_iteration_sequence[0]]; if (champion_health >=
  // 0xFFF0) {

  //long_text_phrase_print(training_sequence.input);
  //long_text_phrase_print(training_sequence.produce);
  //DEBUGPRINT(("%X population_health[champion_iteration_sequence[0]\n",
  //      population_health[champion_iteration_sequence[0]]
  //      ));
  population = island_champion_compression(
      0x20 /*arbitrary number*/, climate, recipe, population, fresh_population,
      training_sequence, population_health, champion_iteration_sequence);
  //}
  // setup champion_iteration_sequence and population_health
  population_quiz(fresh_population, training_sequence, program_produce,
                  population_health);
  champion_choose(fresh_population.length, population_health,
                  champion_iteration_sequence);
  return fresh_population;
}

struct Paragraph /*fresh_recipe*/
recipe_improve(const struct Climate climate, const uint8_t line,
               const generic struct Paragraph person, const uint8_t fresh_line,
               generic struct Paragraph fresh_person) {

  uint8_t iteration = line;
  guarantee(person.page.plength > iteration);
  guarantee(fresh_person.page.plength > fresh_line);
  guarantee(climate.radioactivity < TRANSMUTATION_PROBABILITY_MASK);
  // if line is not final line then return fresh_person
  // is_theIndexfinger_inAFinalIndependentclauseLine

  NewPagePhrase(fresh_recipe, 1);
  line_copy(person.page.lines[iteration], fresh_recipe.page.lines[0]);
  fresh_recipe.length = (uint8_t)neo_htinti_final_word_found(fresh_recipe.page);
  fresh_recipe.begin = 1;
  // text_phrase_print(fresh_recipe);
  // DEBUGPRINT("0x%X indexFinger", indexFinger);
  // need to operate on each phrase individually.
  // if it contains a number then chance of improving it
  uint16_t tablet_indexFinger = iteration * LINE_LONG;
  struct Phrase search_recipe = fresh_recipe;
  for (; tablet_indexFinger < (iteration + 1) * LINE_LONG;
       ++tablet_indexFinger) {
    // improve all phrases:
    // get next phrase (upcoming_phrase_found)
    // improve it (phrase_improve)
    // repeat
    // text_phrase_print(search_recipe);
    struct Phrase upcoming_phrase =
        find_theNext_GrammaticalCasePhrase(search_recipe);
    // text_page_print(upcoming_phrase.page);
    // text_phrase_print(upcoming_phrase);
    // DEBUGPRINT(("%X upcoming_phrase.begin\n", upcoming_phrase.begin));
    // if fail then break
    if (upcoming_phrase.begin == 0)
      break;
    search_recipe.begin = upcoming_phrase.begin + upcoming_phrase.length;
    search_recipe.length = (uint8_t)(
        (fresh_recipe.length + fresh_recipe.begin) - search_recipe.begin);
    // DEBUGPRINT(("%X search_recipe.begin, %X search_recipe.length\n",
    //            search_recipe.begin, search_recipe.length));
    if ((((seed_random(climate.random_seed)) &
          TRANSMUTATION_PROBABILITY_MASK) <= (uint16_t)climate.radioactivity)) {
      // DEBUGPRINT("0x%lX start, 0x%X tablet_indexFinger\n",  *random_seed,
      // tablet_indexFinger);
      // tablet_print(1, &fresh_recipe);
      // phrase_improve(climate, tablet_indexFinger & LINE_LONG_MASK, word,
      //                   &fresh_recipe);
      phrase_improve(climate, upcoming_phrase);
      // tablet_print(1, &fresh_recipe);
      // DEBUGPRINT("0x%lX end\n",  *random_seed);
    }
  }
  line_copy(fresh_recipe.page.lines[0], fresh_person.page.lines[fresh_line]);
  // text_phrase_print(fresh_recipe);
  // Page_print(fresh_person.page);
  fresh_person.begin = 1;
  fresh_person.length =
      phrase_long_diagnose(fresh_person); // not sure this is good idea
  if ((fresh_person.length + fresh_person.begin) >
      fresh_person.page.plength * LINE_LONG) {
    DEBUGPRINT(("%X fresh_person.begin, %X fresh_person.length\n",
                fresh_person.begin, fresh_person.length));
  }
  guarantee((fresh_person.length + fresh_person.begin) <=
            fresh_person.page.plength * LINE_LONG);
  guarantee(fresh_person.begin > 0);
  // text_phrase_print(fresh_person);
  return fresh_person;
}
struct Climate rapid_evolutionary_island_climate_found(
    const struct Climate climate, const struct Paragraph recipe,
    const struct TrainingSequence training_sequence) {

  // create a temporary_climate with a random seed
  // repeat for each random_seed
  //// generate the population based on the random seed
  //// evolutionary_island_establish
  //// add to best champion climate list
  //// check if we have a passing champion
  //// if yes, then break
  //// otherwise generate new random_seed
  // return best champion
  //  NewClimate(temp_climate, 0x22743190, 0x30, 0xC000);
  struct Climate temp_climate = climate;
  NewHollowPopulation(population, 0xFF, 0x20);
  NewHollowPopulation(fresh_population, 0xFF, 0x20);
  population.length = climate.max_population;
  repeat(climate.max_population,
         population.people[iterator].page.plength = climate.max_program_length);
  fresh_population.length = climate.max_population;
  repeat(climate.max_population,
         fresh_population.people[iterator].page.plength =
             climate.max_program_length);
  uint32_t population_health[0xFF];
  uint64_t champion_iteration_sequence[0xFF];
  random_t begin_random_seed[2];
  // start of main code
  repeat(30, begin_random_seed[0] = temp_climate.random_seed[0];
         begin_random_seed[1] = temp_climate.random_seed[1]; DEBUGPRINT(
             ("%lX %lX temp_climate.random_seed, %X temp_climate.temperature, "
              "%X temp_climate.radioactivity\n",
              temp_climate.random_seed[0], temp_climate.random_seed[1],
              temp_climate.temperature, temp_climate.radioactivity));
         neo_population_establish(temp_climate, recipe, population);
         //// generate the population based on the random seed
         //// evolutionary_island_establish

         fresh_population = evolutionary_island_establish(
             temp_climate, recipe, training_sequence, population,
             fresh_population, population_health, champion_iteration_sequence);
         //// add to best champion climate list
         //// check if we have a passing champion
         //DEBUGPRINT(("%X health, best champion:\n",
         //            population_health[champion_iteration_sequence[0]]));
         //text_phrase_print(
         //    fresh_population.people[champion_iteration_sequence[0]]);
         uint8_t passing_champion =
             population_health[champion_iteration_sequence[0]] >= 0xFFF0;
         //// if yes, then break
         conditional(passing_champion, break);
         //// otherwise generate new random_seed
         seed_random(temp_climate.random_seed));
  // return best champion climate
  temp_climate.random_seed[0] = begin_random_seed[0];
  temp_climate.random_seed[1] = begin_random_seed[1];
  return temp_climate;
}
