/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef LWONPROM_H
#define LWONPROM_H
#include "interpret.h"
#include "prng.h"
#include "pyash.h"
#include "pyashWords.h"
#include "sort.h"
#include "translation.h"
#define POPULATION_LONG 60
#define POPULATION_TIDBIT_LONG 8
#define POPULATION_MASK 0xFF
#define INCOMPLETE_HEALTH_TIDBIT_LONG 0x12
#define INCOMPLETE_HEALTH_MASK 0xFFF
#define PROGRAM_ECONOMY_TIDBIT_LONG 0x4
#define PROGRAM_ECONOMY_MASK 0xF
#define PERSON_LONG 0x10
#define TRANSMUTATION_PROBABILITY_MASK 0xFFFF

struct Population {
  uint8_t length;
  struct Paragraph *people;
};

enum Transmutation {Copy = 0, Improve = 1, Delete = 2, Switch = 3, Inject = 4 }; 

struct TrainingSequence {
  uint16_t length;
  uint16_t input_length;
  struct Paragraph input;
  uint16_t produce_length;
  struct Paragraph produce;
};

#define NewHollowPopulation(_name, _population_length, _max_person_length)     \
  struct Population _name;                                                     \
  _name.length = _population_length;                                           \
  struct Paragraph _name##_people[_population_length];                         \
  struct Page _name##_page_array[_population_length];                          \
  line_t _name##_lines_array[_population_length][_max_person_length] = {{0}};  \
  down_to_zero_repeat(                                                         \
      _population_length - 1, _name##_people[iterator].begin = 0;              \
      _name##_people[iterator].length = 0;                                     \
      _name##_people[iterator].page = _name##_page_array[iterator];            \
      _name##_people[iterator].page.plength = _max_person_length;              \
      _name##_people[iterator].page.lines = _name##_lines_array[iterator]);    \
  _name.people = _name##_people;

#define NewPopulation(_name, _population_length, _max_person_length, _climate, \
  _recipe) \
  NewHollowPopulation(_name, _population_length, _max_person_length); \
  neo_population_establish(_climate, _recipe, _name);

struct Climate {
  uint16_t temperature; // how large a change is
  uint64_t *random_seed;
  uint16_t radioactivity; // probability of change
  uint8_t max_population; // maximum_population_size_on_island
  uint8_t max_program_length; // maximum_program_length < 0x20
};

#define NewClimate(_name, _random_number, _temperature, _radioactivity) \
  uint64_t _name ## random_seed[2] = {0}; \
  random_seed_establish(_random_number, 2, _name ## random_seed); \
  struct Climate _name; \
  climate.random_seed = _name ## random_seed; \
  climate.temperature = _temperature; \
  climate.radioactivity = _radioactivity;


#define NewTrainingSequence(_name, _number, _input, _produce) \
  NewTextPhrase( _name ## input, _number, _input); \
  NewTextPhrase( _name ## produce, _number, _produce); \
  struct TrainingSequence _name; \
  _name.input_length = 1; \
  _name.length = _number; \
  _name.input = _name ## input;  \
  _name.produce_length = 1; \
  _name.produce = _name ## produce;

#define NewLongTrainingSequence(_name, _number, _input_length, _input, _produce) \
  NewLongTextPhrase( _name ## input, _number * _input_length, _input); \
  NewLongTextPhrase( _name ## produce, _number * _input_length, _produce); \
  struct TrainingSequence _name; \
  _name.input_length = _input_length; \
  _name.length = _number; \
  _name.input = _name ## input;  \
  _name.produce_length = _input_length; \
  _name.produce = _name ## produce;

#define NewHollowTrainingSequence(_name, _number) \
  NewPagePhrase( _name ## input, _number); \
  NewPagePhrase( _name ## produce, _number); \
  struct TrainingSequence _name; \
  _name.input_length = 1; \
  _name.input = _name ## input;  \
  _name.produce = _name ## produce;

uint8_t /*program_long*/
person_establish(const uint16_t temperature, uint64_t *random_seed,
                 const uint16_t transmutation_probability,
                 const uint16_t recipe_long, const line_t *recipe,
                 const uint8_t max_program_long, line_t *person);

struct Paragraph /*neo person*/
neo_person_establish(const struct Climate, const struct Paragraph recipe,
                     struct Paragraph person);

void population_establish(const uint16_t temperature, uint64_t *random_seed,
                          const uint16_t transmutation_probability,
                          const uint16_t recipe_long, const line_t *recipe,
                          const uint8_t max_program_long,
                          const uint8_t population_long,
                          line_t population[][PERSON_LONG]);

struct Population /*new population*/
neo_population_establish(const struct Climate climate,
                         const struct Paragraph recipe,
                         struct Population population);

void champion_choose(const uint8_t population_long,
                     const uint32_t *population_health,
                     uint64_t *champion_iteration_sequence);

uint16_t person_comparison(const line_t *person,
                           const line_t *comparison_person);
//void old_population_quiz(const uint8_t population_long,
//                         line_t population[][PERSON_LONG],
//                         const uint16_t training_sequence_long,
//                         line_t training_sequence[][2],
//                         /*local*/ line_t *program_produce,
//                         uint32_t *population_health);
void population_quiz(const struct Population population,
                     const struct TrainingSequence training_sequence,
                     struct Paragraph program_produce,
                     uint32_t population_health[]);
void old_champion_compression(const uint16_t transmutation_probability,
                          uint64_t *random_seed, const uint8_t max_program_long,
                          const uint8_t recipe_long, const line_t *recipe,
                          const uint8_t population_long,
                          const line_t *champion_program,
                          line_t fresh_population[][PERSON_LONG]);
struct Population champion_compression(const struct Climate climate,
                          const struct Paragraph recipe,
                          const struct Paragraph champion_program,
                          struct Population fresh_population);
void old_population_copy(const uint8_t population_long,
                         const line_t population_abl[][PERSON_LONG],
                         line_t population_dat[][PERSON_LONG]);
struct Population /* produce */ population_copy(const struct Population input,
                                                struct Population produce);
void old_population_refresh(const uint16_t temperature,
                            const uint16_t transmutation_probability,
                            uint64_t *random_seed, const uint8_t recipe_long,
                            const line_t *recipe,
                            const uint8_t max_program_long,
                            const uint8_t population_long,
                            const uint64_t *champion_iteration_sequence,
                            const line_t population[][PERSON_LONG],
                            line_t fresh_population[][PERSON_LONG]);

struct Population population_refresh(const struct Climate climate,
                        const struct Paragraph recipe,
                        const uint64_t *champion_iteration_sequence,
                        const struct Population population,
                        struct Population fresh_population);

void old_champion_copy(const uint8_t population_long,
                       const line_t population[][PERSON_LONG],
                       const uint64_t *champion_iteration_sequence,
                       const uint8_t population_ration,
                       line_t fresh_population[][PERSON_LONG]);
struct Population champion_copy(const struct Population population,
                                const uint64_t *champion_iteration_sequence,
                                const uint8_t population_ration,
                                struct Population fresh_population);

struct Population evolve_generation(const struct Climate climate,
                       const struct Paragraph recipe,
                       const struct TrainingSequence training_sequence,
                       const struct Population population,
                       struct Population fresh_population,
                       uint32_t population_health[],
                       uint64_t champion_iteration_sequence[]);
struct Population
 evolutionary_island_establish(
    const struct Climate climate, 
    const struct Paragraph recipe,
    const struct TrainingSequence training_sequence, 
    struct Population popultion,
    struct Population fresh_population, uint32_t population_health[],
    uint64_t champion_iteration_sequence[]);

struct Climate 
 rapid_evolutionary_island_climate_found(
    const struct Climate climate, const struct Paragraph recipe,
    const struct TrainingSequence training_sequence);

void old_evolutionary_island_establish(
    uint64_t *random_seed, const uint8_t recipe_long, const line_t *recipe,
    const uint8_t max_program_long, const uint16_t training_sequence_long,
    line_t training_sequence[][2], line_t program_produce[],
    const uint8_t population_long, line_t population[][PERSON_LONG],
    line_t fresh_population[][PERSON_LONG], uint32_t population_health[],
    uint64_t champion_iteration_sequence[]);
ulong text_long_derive(const char *str, ulong max_len);

struct Phrase /* result_phrase */
phrase_improve(struct Climate climate, const struct Phrase input_phrase);

struct Paragraph recipe_improve(const struct Climate climate,
                                const uint8_t line,
                                const generic struct Paragraph person,
                                const uint8_t fresh_line,
                                generic struct Paragraph fresh_person);


void health_assess(const struct TrainingSequence training_sequence,
                   const struct Paragraph program,
                   struct Phrase program_produce, uint32_t *health);
struct Paragraph /* produce */
program_quiz(const struct TrainingSequence training_sequence,
             const struct Paragraph program, struct Phrase produce);
struct Paragraph /*produce*/ person_copy(generic const struct Paragraph input,
                                         generic struct Paragraph produce);
struct Population population_transmutation(const struct Climate climate,
                                           const struct Paragraph recipe,
                                           const struct Population population,
                                           struct Population fresh_population);
struct Paragraph person_transmutation(const struct Climate climate,
                                      const struct Paragraph recipe,
                                      const struct Paragraph person,
                                      struct Paragraph fresh_person);
struct Phrase recipe_switch(struct Climate climate,
                            const struct Paragraph recipe,
                            struct Phrase fresh_phrase);
void old_person_compression_recipe_merge(
    const uint16_t transmutation_probability, uint64_t *random_seed,
    const uint8_t max_program_long, const line_t *person, line_t *fresh_person);
struct Paragraph person_compression_recipe_merge(const struct Climate climate,
                                                 const struct Paragraph person,
                                                 struct Paragraph fresh_person);
void old_recipe_merge(uint64_t *random_seed, const line_t *person,
                  const uint8_t iteration, const uint16_t grammar_word,
                  line_t *fresh_recipe);
struct Phrase recipe_merge(const struct Climate climate,
                  const struct Paragraph person,
                  const uint8_t iteration,
                  const uint16_t grammar_word,
                  struct Phrase fresh_recipe);
void old_person_compression(const uint16_t transmutation_probability,
                        uint64_t *random_seed, const uint8_t recipe_long,
                        const line_t *recipe, const uint8_t max_program_long,
                        const line_t *person, line_t *fresh_person);
struct Paragraph person_compression(const struct Climate climate, 
                        const struct Paragraph recipe, 
                        struct Paragraph person, 
                        struct Paragraph fresh_person);
void old_person_compression_recipe_delete(const uint16_t transmutation_probability,
                                      uint64_t *random_seed,
                                      const uint8_t max_program_long,
                                      const line_t *person,
                                      line_t *fresh_person);
struct Paragraph person_compression_recipe_delete(const struct Climate climate,
                                      const struct Paragraph person,
                                      struct Paragraph fresh_person);
void old_recipe_delete(const uint8_t max_program_long, const line_t *person,
                   const uint8_t recipe_number, line_t *fresh_person);
struct Paragraph recipe_delete(const struct Paragraph person,
                   const uint8_t recipe_number, struct Paragraph fresh_person);
struct Sentence htin_transmutation(const struct Climate climate,
                                   const struct Sentence recipe,
                                   const struct Sentence htin,
                                   struct Sentence fresh_htin);
struct Population island_champion_compression(uint8_t repeat_count,
            struct Climate climate, struct Paragraph recipe, 
            struct Population population, 
            struct Population fresh_population,
            const struct TrainingSequence training_sequence,
            uint32_t population_health[],
            uint64_t champion_iteration_sequence[]);
#endif
