; ModuleID = 'lwonprom.cl'
source_filename = "lwonprom.cl"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@grammaticalCase_word_series = local_unnamed_addr constant [59 x i16] [i16 9438, i16 9310, i16 9566, i16 10430, i16 8542, i16 9278, i16 8318, i16 10366, i16 18703, i16 18727, i16 16719, i16 9854, i16 11614, i16 9822, i16 12606, i16 16679, i16 18775, i16 16727, i16 18447, i16 18783, i16 16735, i16 16919, i16 22871, i16 16695, i16 22879, i16 11454, i16 24919, i16 11902, i16 18743, i16 25431, i16 22583, i16 22863, i16 18839, i16 17559, i16 24695, i16 21031, i16 24359, i16 26407, i16 23455, i16 24215, i16 22903, i16 18903, i16 17567, i16 24927, i16 17551, i16 19615, i16 11966, i16 23703, i16 9950, i16 23695, i16 24479, i16 -30121, i16 22687, i16 24231, i16 10814, i16 24855, i16 21287, i16 24926, i16 17758], align 16
@perspective_word_series = local_unnamed_addr constant [34 x i16] [i16 8574, i16 10590, i16 10686, i16 8510, i16 8670, i16 8830, i16 17751, i16 12670, i16 16495, i16 16799, i16 17703, i16 16935, i16 23127, i16 20823, i16 16759, i16 16815, i16 16655, i16 11358, i16 22839, i16 10622, i16 16783, i16 21783, i16 10302, i16 12414, i16 26199, i16 20815, i16 10718, i16 21015, i16 23063, i16 17319, i16 13534, i16 24591, i16 13470, i16 28215], align 16
@number_word_series = local_unnamed_addr constant [21 x i16] [i16 27860, i16 10264, i16 -22006, i16 26634, i16 -30398, i16 10592, i16 -30632, i16 18504, i16 18728, i16 -13654, i16 18512, i16 27497, i16 -22134, i16 -29878, i16 -29880, i16 -21728, i16 -29624, i16 -21592, i16 18840, i16 -6042, i16 -30711], align 16
@grammaticalCase_list = constant [6 x i16] [i16 9310, i16 10366, i16 8318, i16 9566, i16 9438, i16 16679], align 2
@perspective_list = local_unnamed_addr constant [4 x i16] [i16 8574, i16 10686, i16 8670, i16 10590], align 2

; Function Attrs: norecurse nounwind readnone uwtable
define i64 @modulo(i64, i64) local_unnamed_addr #0 {
  %3 = and i64 %1, 1
  %4 = icmp eq i64 %3, 0
  br i1 %4, label %5, label %8

; <label>:5:                                      ; preds = %2
  %6 = add i64 %1, -1
  %7 = and i64 %6, %0
  br label %10

; <label>:8:                                      ; preds = %2
  %9 = urem i64 %0, %1
  br label %10

; <label>:10:                                     ; preds = %8, %5
  %11 = phi i64 [ %7, %5 ], [ %9, %8 ]
  ret i64 %11
}

; Function Attrs: norecurse nounwind uwtable
define void @v4us_write(i8 zeroext, i8 zeroext, i16 zeroext, <4 x i32>* nocapture) local_unnamed_addr #1 {
  %5 = icmp ult i8 %0, %1
  br i1 %5, label %6, label %19

; <label>:6:                                      ; preds = %4
  switch i8 %0, label %19 [
    i8 0, label %7
    i8 1, label %11
    i8 2, label %15
  ]

; <label>:7:                                      ; preds = %6
  %8 = zext i16 %2 to i32
  %9 = load <4 x i32>, <4 x i32>* %3, align 16
  %10 = insertelement <4 x i32> %9, i32 %8, i64 0
  store <4 x i32> %10, <4 x i32>* %3, align 16
  br label %19

; <label>:11:                                     ; preds = %6
  %12 = zext i16 %2 to i32
  %13 = load <4 x i32>, <4 x i32>* %3, align 16
  %14 = insertelement <4 x i32> %13, i32 %12, i64 1
  store <4 x i32> %14, <4 x i32>* %3, align 16
  br label %19

; <label>:15:                                     ; preds = %6
  %16 = zext i16 %2 to i32
  %17 = load <4 x i32>, <4 x i32>* %3, align 16
  %18 = insertelement <4 x i32> %17, i32 %16, i64 2
  store <4 x i32> %18, <4 x i32>* %3, align 16
  br label %19

; <label>:19:                                     ; preds = %6, %4, %7, %15, %11
  ret void
}

; Function Attrs: norecurse nounwind readnone uwtable
define i64 @v4us_uint64_translation(<4 x i32>) local_unnamed_addr #0 {
  %2 = extractelement <4 x i32> %0, i64 0
  %3 = zext i32 %2 to i64
  %4 = extractelement <4 x i32> %0, i64 1
  %5 = zext i32 %4 to i64
  %6 = shl nuw nsw i64 %5, 16
  %7 = add nuw nsw i64 %6, %3
  %8 = extractelement <4 x i32> %0, i64 2
  %9 = zext i32 %8 to i64
  %10 = shl nuw i64 %9, 32
  %11 = add i64 %7, %10
  %12 = extractelement <4 x i32> %0, i64 3
  %13 = zext i32 %12 to i64
  %14 = shl i64 %13, 48
  %15 = add i64 %11, %14
  ret i64 %15
}

; Function Attrs: norecurse nounwind uwtable
define void @v16us_write(i8 zeroext, i16 zeroext, <16 x i16>* nocapture) local_unnamed_addr #1 {
  %4 = icmp ult i8 %0, 16
  br i1 %4, label %5, label %9

; <label>:5:                                      ; preds = %3
  %6 = zext i8 %0 to i64
  %7 = load <16 x i16>, <16 x i16>* %2, align 32
  %8 = insertelement <16 x i16> %7, i16 %1, i64 %6
  store <16 x i16> %8, <16 x i16>* %2, align 32
  br label %9

; <label>:9:                                      ; preds = %3, %5
  ret void
}

; Function Attrs: norecurse nounwind readonly uwtable
define zeroext i16 @v16us_read(i8 zeroext, <16 x i16>* byval nocapture readonly align 32) local_unnamed_addr #2 {
  %3 = load <16 x i16>, <16 x i16>* %1, align 32
  switch i8 %0, label %36 [
    i8 0, label %4
    i8 1, label %6
    i8 2, label %8
    i8 3, label %10
    i8 4, label %12
    i8 5, label %14
    i8 6, label %16
    i8 7, label %18
    i8 8, label %20
    i8 9, label %22
    i8 10, label %24
    i8 11, label %26
    i8 12, label %28
    i8 13, label %30
    i8 14, label %32
    i8 15, label %34
  ]

; <label>:4:                                      ; preds = %2
  %5 = extractelement <16 x i16> %3, i64 0
  br label %36

; <label>:6:                                      ; preds = %2
  %7 = extractelement <16 x i16> %3, i64 1
  br label %36

; <label>:8:                                      ; preds = %2
  %9 = extractelement <16 x i16> %3, i64 2
  br label %36

; <label>:10:                                     ; preds = %2
  %11 = extractelement <16 x i16> %3, i64 3
  br label %36

; <label>:12:                                     ; preds = %2
  %13 = extractelement <16 x i16> %3, i64 4
  br label %36

; <label>:14:                                     ; preds = %2
  %15 = extractelement <16 x i16> %3, i64 5
  br label %36

; <label>:16:                                     ; preds = %2
  %17 = extractelement <16 x i16> %3, i64 6
  br label %36

; <label>:18:                                     ; preds = %2
  %19 = extractelement <16 x i16> %3, i64 7
  br label %36

; <label>:20:                                     ; preds = %2
  %21 = extractelement <16 x i16> %3, i64 8
  br label %36

; <label>:22:                                     ; preds = %2
  %23 = extractelement <16 x i16> %3, i64 9
  br label %36

; <label>:24:                                     ; preds = %2
  %25 = extractelement <16 x i16> %3, i64 10
  br label %36

; <label>:26:                                     ; preds = %2
  %27 = extractelement <16 x i16> %3, i64 11
  br label %36

; <label>:28:                                     ; preds = %2
  %29 = extractelement <16 x i16> %3, i64 12
  br label %36

; <label>:30:                                     ; preds = %2
  %31 = extractelement <16 x i16> %3, i64 13
  br label %36

; <label>:32:                                     ; preds = %2
  %33 = extractelement <16 x i16> %3, i64 14
  br label %36

; <label>:34:                                     ; preds = %2
  %35 = extractelement <16 x i16> %3, i64 15
  br label %36

; <label>:36:                                     ; preds = %2, %34, %32, %30, %28, %26, %24, %22, %20, %18, %16, %14, %12, %10, %8, %6, %4
  %37 = phi i16 [ %35, %34 ], [ %33, %32 ], [ %31, %30 ], [ %29, %28 ], [ %27, %26 ], [ %25, %24 ], [ %23, %22 ], [ %21, %20 ], [ %19, %18 ], [ %17, %16 ], [ %15, %14 ], [ %13, %12 ], [ %11, %10 ], [ %9, %8 ], [ %7, %6 ], [ %5, %4 ], [ 0, %2 ]
  ret i16 %37
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start(i64, i8* nocapture) #3

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end(i64, i8* nocapture) #3

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @v16us_print(<16 x i16>* byval nocapture readonly align 32) local_unnamed_addr #2 {
  %2 = load <16 x i16>, <16 x i16>* %0, align 32
  %3 = extractelement <16 x i16> %2, i64 15
  %4 = zext i16 %3 to i32
  ret i32 %4
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @tablet_print(i8 zeroext, <16 x i16>* nocapture readonly) local_unnamed_addr #2 {
  ret i32 0
}

; Function Attrs: norecurse nounwind uwtable
define void @v16us_forward_copy(i8 zeroext, i8 zeroext, i8 zeroext, <16 x i16>* nocapture) local_unnamed_addr #1 {
  %5 = add i8 %2, %1
  %6 = icmp ult i8 %5, %0
  br i1 %6, label %56, label %7

; <label>:7:                                      ; preds = %4
  %8 = load <16 x i16>, <16 x i16>* %3, align 32, !tbaa !1
  br label %9

; <label>:9:                                      ; preds = %7, %51
  %10 = phi <16 x i16> [ %52, %51 ], [ %8, %7 ]
  %11 = phi i8 [ %53, %51 ], [ %5, %7 ]
  %12 = sub i8 %11, %2
  switch i8 %12, label %45 [
    i8 0, label %13
    i8 1, label %15
    i8 2, label %17
    i8 3, label %19
    i8 4, label %21
    i8 5, label %23
    i8 6, label %25
    i8 7, label %27
    i8 8, label %29
    i8 9, label %31
    i8 10, label %33
    i8 11, label %35
    i8 12, label %37
    i8 13, label %39
    i8 14, label %41
    i8 15, label %43
  ]

; <label>:13:                                     ; preds = %9
  %14 = extractelement <16 x i16> %10, i64 0
  br label %45

; <label>:15:                                     ; preds = %9
  %16 = extractelement <16 x i16> %10, i64 1
  br label %45

; <label>:17:                                     ; preds = %9
  %18 = extractelement <16 x i16> %10, i64 2
  br label %45

; <label>:19:                                     ; preds = %9
  %20 = extractelement <16 x i16> %10, i64 3
  br label %45

; <label>:21:                                     ; preds = %9
  %22 = extractelement <16 x i16> %10, i64 4
  br label %45

; <label>:23:                                     ; preds = %9
  %24 = extractelement <16 x i16> %10, i64 5
  br label %45

; <label>:25:                                     ; preds = %9
  %26 = extractelement <16 x i16> %10, i64 6
  br label %45

; <label>:27:                                     ; preds = %9
  %28 = extractelement <16 x i16> %10, i64 7
  br label %45

; <label>:29:                                     ; preds = %9
  %30 = extractelement <16 x i16> %10, i64 8
  br label %45

; <label>:31:                                     ; preds = %9
  %32 = extractelement <16 x i16> %10, i64 9
  br label %45

; <label>:33:                                     ; preds = %9
  %34 = extractelement <16 x i16> %10, i64 10
  br label %45

; <label>:35:                                     ; preds = %9
  %36 = extractelement <16 x i16> %10, i64 11
  br label %45

; <label>:37:                                     ; preds = %9
  %38 = extractelement <16 x i16> %10, i64 12
  br label %45

; <label>:39:                                     ; preds = %9
  %40 = extractelement <16 x i16> %10, i64 13
  br label %45

; <label>:41:                                     ; preds = %9
  %42 = extractelement <16 x i16> %10, i64 14
  br label %45

; <label>:43:                                     ; preds = %9
  %44 = extractelement <16 x i16> %10, i64 15
  br label %45

; <label>:45:                                     ; preds = %9, %13, %15, %17, %19, %21, %23, %25, %27, %29, %31, %33, %35, %37, %39, %41, %43
  %46 = phi i16 [ %44, %43 ], [ %42, %41 ], [ %40, %39 ], [ %38, %37 ], [ %36, %35 ], [ %34, %33 ], [ %32, %31 ], [ %30, %29 ], [ %28, %27 ], [ %26, %25 ], [ %24, %23 ], [ %22, %21 ], [ %20, %19 ], [ %18, %17 ], [ %16, %15 ], [ %14, %13 ], [ 0, %9 ]
  %47 = icmp ult i8 %11, 16
  br i1 %47, label %48, label %51

; <label>:48:                                     ; preds = %45
  %49 = zext i8 %11 to i64
  %50 = insertelement <16 x i16> %10, i16 %46, i64 %49
  store <16 x i16> %50, <16 x i16>* %3, align 32
  br label %51

; <label>:51:                                     ; preds = %45, %48
  %52 = phi <16 x i16> [ %10, %45 ], [ %50, %48 ]
  %53 = add i8 %11, -1
  %54 = icmp ult i8 %53, %0
  br i1 %54, label %55, label %9

; <label>:55:                                     ; preds = %51
  br label %56

; <label>:56:                                     ; preds = %55, %4
  ret void
}

; Function Attrs: norecurse nounwind readonly uwtable
define zeroext i16 @tablet_read(i16 zeroext, i8 zeroext, <16 x i16>* nocapture readonly) local_unnamed_addr #2 {
  %4 = and i16 %0, 15
  %5 = zext i16 %4 to i64
  %6 = lshr i16 %0, 4
  %7 = zext i16 %6 to i64
  %8 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %7
  %9 = load <16 x i16>, <16 x i16>* %8, align 32, !tbaa !1
  %10 = extractelement <16 x i16> %9, i64 %5
  ret i16 %10
}

; Function Attrs: norecurse nounwind uwtable
define void @tablet_write(i16 zeroext, i16 zeroext, i8 zeroext, <16 x i16>* nocapture) local_unnamed_addr #1 {
  %5 = and i16 %0, 15
  %6 = zext i16 %5 to i64
  %7 = lshr i16 %0, 4
  %8 = zext i16 %7 to i64
  %9 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %8
  %10 = load <16 x i16>, <16 x i16>* %9, align 32
  %11 = insertelement <16 x i16> %10, i16 %1, i64 %6
  store <16 x i16> %11, <16 x i16>* %9, align 32
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @tablet_word_write(i16 zeroext, i16 zeroext, i8 zeroext, <16 x i16>* nocapture, i16* nocapture) local_unnamed_addr #1 {
  %6 = zext i16 %0 to i32
  store i16 %0, i16* %4, align 2, !tbaa !4
  %7 = and i32 %6, 15
  %8 = sub nsw i32 %6, %7
  %9 = trunc i32 %8 to i16
  %10 = and i16 %9, 15
  %11 = zext i16 %10 to i64
  %12 = lshr i16 %9, 4
  %13 = zext i16 %12 to i64
  %14 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %13
  %15 = load <16 x i16>, <16 x i16>* %14, align 32, !tbaa !1
  %16 = extractelement <16 x i16> %15, i64 %11
  %17 = icmp eq i16 %16, 0
  br i1 %17, label %18, label %20

; <label>:18:                                     ; preds = %5
  %19 = insertelement <16 x i16> %15, i16 1, i64 %11
  store <16 x i16> %19, <16 x i16>* %14, align 32
  br label %20

; <label>:20:                                     ; preds = %18, %5
  %21 = icmp eq i32 %7, 0
  br i1 %21, label %22, label %39

; <label>:22:                                     ; preds = %20
  %23 = load i16, i16* %4, align 2, !tbaa !4
  %24 = add i16 %23, 1
  store i16 %24, i16* %4, align 2, !tbaa !4
  %25 = lshr i16 %0, 4
  %26 = and i16 %25, 15
  %27 = icmp eq i16 %26, 0
  br i1 %27, label %39, label %28

; <label>:28:                                     ; preds = %22
  %29 = zext i16 %26 to i64
  %30 = add nsw i64 %29, -1
  %31 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %30
  %32 = load <16 x i16>, <16 x i16>* %31, align 32
  %33 = extractelement <16 x i16> %32, i64 0
  %34 = and i16 %33, 1
  %35 = icmp eq i16 %34, 0
  br i1 %35, label %39, label %36

; <label>:36:                                     ; preds = %28
  %37 = xor i16 %33, -1
  %38 = insertelement <16 x i16> %32, i16 %37, i64 0
  store <16 x i16> %38, <16 x i16>* %31, align 32
  br label %39

; <label>:39:                                     ; preds = %28, %22, %36, %20
  %40 = load i16, i16* %4, align 2, !tbaa !4
  %41 = and i16 %40, 15
  %42 = zext i16 %41 to i64
  %43 = lshr i16 %40, 4
  %44 = zext i16 %43 to i64
  %45 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %44
  %46 = load <16 x i16>, <16 x i16>* %45, align 32
  %47 = insertelement <16 x i16> %46, i16 %1, i64 %42
  store <16 x i16> %47, <16 x i16>* %45, align 32
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @tablet_retrospective_word_write(i16 zeroext, i16 zeroext, i8 zeroext, <16 x i16>* nocapture, i16* nocapture) local_unnamed_addr #1 {
  %6 = and i16 %0, 15
  %7 = icmp eq i16 %6, 0
  %8 = sext i1 %7 to i16
  %9 = add i16 %8, %0
  store i16 %9, i16* %4, align 2, !tbaa !4
  %10 = and i16 %9, 15
  %11 = zext i16 %10 to i64
  %12 = lshr i16 %9, 4
  %13 = zext i16 %12 to i64
  %14 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %13
  %15 = load <16 x i16>, <16 x i16>* %14, align 32
  %16 = insertelement <16 x i16> %15, i16 %1, i64 %11
  store <16 x i16> %16, <16 x i16>* %14, align 32
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define zeroext i16 @tablet_upcoming_word_read(i16 zeroext, i8 zeroext, <16 x i16>* nocapture readonly, i16* nocapture) local_unnamed_addr #1 {
  %5 = and i16 %0, 15
  %6 = icmp eq i16 %5, 0
  %7 = zext i1 %6 to i16
  %8 = add i16 %7, %0
  store i16 %8, i16* %3, align 2, !tbaa !4
  %9 = and i16 %8, 15
  %10 = zext i16 %9 to i64
  %11 = lshr i16 %8, 4
  %12 = zext i16 %11 to i64
  %13 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %12
  %14 = load <16 x i16>, <16 x i16>* %13, align 32, !tbaa !1
  %15 = extractelement <16 x i16> %14, i64 %10
  ret i16 %15
}

; Function Attrs: norecurse nounwind uwtable
define zeroext i16 @tablet_retrospective_word_read(i16 zeroext, i8 zeroext, <16 x i16>* nocapture readonly, i16* nocapture) local_unnamed_addr #1 {
  %5 = and i16 %0, 15
  %6 = icmp eq i16 %5, 0
  %7 = sext i1 %6 to i16
  %8 = add i16 %7, %0
  store i16 %8, i16* %3, align 2, !tbaa !4
  %9 = and i16 %8, 15
  %10 = zext i16 %9 to i64
  %11 = lshr i16 %8, 4
  %12 = zext i16 %11 to i64
  %13 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %12
  %14 = load <16 x i16>, <16 x i16>* %13, align 32, !tbaa !1
  %15 = extractelement <16 x i16> %14, i64 %10
  ret i16 %15
}

; Function Attrs: norecurse nounwind uwtable
define void @tablet_grammar_write(i16 zeroext, i16 zeroext, i8 zeroext, <16 x i16>* nocapture, i16* nocapture) local_unnamed_addr #1 {
  %6 = zext i16 %0 to i32
  store i16 %0, i16* %4, align 2, !tbaa !4
  %7 = and i32 %6, 15
  %8 = sub nsw i32 %6, %7
  %9 = trunc i32 %8 to i16
  %10 = and i16 %9, 15
  %11 = zext i16 %10 to i64
  %12 = lshr i16 %9, 4
  %13 = zext i16 %12 to i64
  %14 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %13
  %15 = load <16 x i16>, <16 x i16>* %14, align 32, !tbaa !1
  %16 = extractelement <16 x i16> %15, i64 %11
  %17 = icmp eq i16 %16, 0
  %18 = select i1 %17, i16 1, i16 %16
  %19 = icmp eq i32 %7, 0
  br i1 %19, label %20, label %40

; <label>:20:                                     ; preds = %5
  %21 = xor i16 %18, -1
  %22 = insertelement <16 x i16> %15, i16 %21, i64 %11
  store <16 x i16> %22, <16 x i16>* %14, align 32
  %23 = load i16, i16* %4, align 2, !tbaa !4
  %24 = add i16 %23, 1
  store i16 %24, i16* %4, align 2, !tbaa !4
  %25 = lshr i16 %0, 4
  %26 = and i16 %25, 15
  %27 = icmp eq i16 %26, 0
  br i1 %27, label %40, label %28

; <label>:28:                                     ; preds = %20
  %29 = zext i16 %26 to i64
  %30 = add nsw i64 %29, -1
  %31 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %30
  %32 = load <16 x i16>, <16 x i16>* %31, align 32
  %33 = extractelement <16 x i16> %32, i64 0
  %34 = and i16 %33, 1
  %35 = icmp eq i16 %34, 0
  br i1 %35, label %40, label %36

; <label>:36:                                     ; preds = %28
  %37 = xor i16 %33, -1
  %38 = insertelement <16 x i16> %32, i16 %37, i64 0
  store <16 x i16> %38, <16 x i16>* %31, align 32
  %39 = load i16, i16* %4, align 2, !tbaa !4
  br label %40

; <label>:40:                                     ; preds = %36, %20, %28, %5
  %41 = phi i16 [ %0, %5 ], [ %24, %28 ], [ %24, %20 ], [ %39, %36 ]
  %42 = phi i16 [ %18, %5 ], [ 1, %28 ], [ 1, %20 ], [ 1, %36 ]
  %43 = phi i16 [ %9, %5 ], [ %0, %28 ], [ %0, %20 ], [ %0, %36 ]
  %44 = and i16 %41, 15
  %45 = zext i16 %44 to i32
  %46 = shl i32 1, %45
  %47 = zext i16 %42 to i32
  %48 = or i32 %46, %47
  %49 = trunc i32 %48 to i16
  %50 = and i16 %43, 15
  %51 = zext i16 %50 to i64
  %52 = lshr i16 %43, 4
  %53 = zext i16 %52 to i64
  %54 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %53
  %55 = load <16 x i16>, <16 x i16>* %54, align 32
  %56 = insertelement <16 x i16> %55, i16 %49, i64 %51
  store <16 x i16> %56, <16 x i16>* %54, align 32
  %57 = load i16, i16* %4, align 2, !tbaa !4
  %58 = and i16 %57, 15
  %59 = zext i16 %58 to i64
  %60 = lshr i16 %57, 4
  %61 = zext i16 %60 to i64
  %62 = getelementptr inbounds <16 x i16>, <16 x i16>* %3, i64 %61
  %63 = load <16 x i16>, <16 x i16>* %62, align 32
  %64 = insertelement <16 x i16> %63, i16 %1, i64 %59
  store <16 x i16> %64, <16 x i16>* %62, align 32
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define zeroext i16 @tablet_upcoming_grammar_read(i16 zeroext, i8 zeroext, <16 x i16>* nocapture readonly, i16* nocapture) local_unnamed_addr #1 {
  %5 = zext i16 %0 to i32
  store i16 %0, i16* %3, align 2, !tbaa !4
  %6 = lshr i16 %0, 4
  %7 = trunc i16 %6 to i8
  %8 = icmp ult i8 %7, %1
  br i1 %8, label %9, label %56

; <label>:9:                                      ; preds = %4
  %10 = trunc i16 %0 to i8
  %11 = and i8 %10, 15
  %12 = and i16 %6, 255
  %13 = zext i16 %12 to i64
  %14 = zext i8 %1 to i64
  br label %15

; <label>:15:                                     ; preds = %50, %9
  %16 = phi i64 [ %13, %9 ], [ %52, %50 ]
  %17 = trunc i64 %16 to i32
  %18 = shl nuw nsw i32 %17, 4
  %19 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %16
  %20 = load <16 x i16>, <16 x i16>* %19, align 32, !tbaa !1
  %21 = extractelement <16 x i16> %20, i64 0
  %22 = zext i16 %21 to i32
  %23 = trunc i16 %21 to i8
  %24 = and i8 %23, 1
  %25 = zext i8 %24 to i32
  br label %28

; <label>:26:                                     ; preds = %28
  %27 = icmp ult i8 %39, 16
  br i1 %27, label %28, label %50

; <label>:28:                                     ; preds = %15, %26
  %29 = phi i8 [ 1, %15 ], [ %39, %26 ]
  %30 = zext i8 %29 to i32
  %31 = add nuw nsw i32 %30, %18
  %32 = icmp ult i32 %31, %5
  %33 = select i1 %32, i8 %11, i8 %29
  %34 = zext i8 %33 to i32
  %35 = and i32 %34, 31
  %36 = lshr i32 %22, %35
  %37 = and i32 %36, 1
  %38 = icmp eq i32 %37, %25
  %39 = add i8 %33, 1
  br i1 %38, label %40, label %26

; <label>:40:                                     ; preds = %28
  %41 = add nuw nsw i32 %34, %18
  %42 = trunc i32 %41 to i16
  store i16 %42, i16* %3, align 2, !tbaa !4
  %43 = and i16 %42, 15
  %44 = zext i16 %43 to i64
  %45 = lshr i16 %42, 4
  %46 = zext i16 %45 to i64
  %47 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %46
  %48 = load <16 x i16>, <16 x i16>* %47, align 32, !tbaa !1
  %49 = extractelement <16 x i16> %48, i64 %44
  br label %57

; <label>:50:                                     ; preds = %26
  %51 = icmp eq i8 %24, 0
  %52 = add nuw nsw i64 %16, 1
  %53 = icmp ult i64 %52, %14
  %54 = and i1 %51, %53
  br i1 %54, label %15, label %55

; <label>:55:                                     ; preds = %50
  br label %56

; <label>:56:                                     ; preds = %55, %4
  store i16 0, i16* %3, align 2, !tbaa !4
  br label %57

; <label>:57:                                     ; preds = %56, %40
  %58 = phi i16 [ %49, %40 ], [ 0, %56 ]
  ret i16 %58
}

; Function Attrs: nounwind uwtable
define zeroext i16 @tablet_upcoming_grammar_or_quote_read(i16 zeroext, i8 zeroext, <16 x i16>* nocapture readonly, i16* nocapture) local_unnamed_addr #4 {
  %5 = zext i16 %0 to i32
  store i16 %0, i16* %3, align 2, !tbaa !4
  %6 = lshr i16 %0, 4
  %7 = trunc i16 %6 to i8
  %8 = icmp ult i8 %7, %1
  br i1 %8, label %9, label %79

; <label>:9:                                      ; preds = %4
  %10 = trunc i16 %0 to i8
  %11 = and i8 %10, 15
  %12 = zext i8 %11 to i32
  %13 = and i16 %6, 255
  %14 = zext i16 %13 to i64
  %15 = zext i8 %1 to i64
  br label %16

; <label>:16:                                     ; preds = %73, %9
  %17 = phi i64 [ %14, %9 ], [ %75, %73 ]
  %18 = trunc i64 %17 to i32
  %19 = shl nuw nsw i32 %18, 4
  %20 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %17
  %21 = load <16 x i16>, <16 x i16>* %20, align 32, !tbaa !1
  %22 = extractelement <16 x i16> %21, i64 0
  %23 = zext i16 %22 to i32
  %24 = trunc i16 %22 to i8
  %25 = and i8 %24, 1
  %26 = or i32 %19, %12
  %27 = zext i8 %25 to i32
  br label %28

; <label>:28:                                     ; preds = %16, %70
  %29 = phi i8 [ 1, %16 ], [ %71, %70 ]
  %30 = zext i8 %29 to i32
  %31 = add nuw nsw i32 %30, %19
  %32 = icmp ult i32 %31, %5
  %33 = select i1 %32, i32 %26, i32 %31
  %34 = select i1 %32, i8 %11, i8 %29
  %35 = trunc i32 %33 to i16
  %36 = zext i8 %34 to i32
  %37 = and i32 %36, 31
  %38 = lshr i32 %23, %37
  %39 = and i32 %38, 1
  %40 = icmp eq i32 %39, %27
  br i1 %40, label %41, label %49

; <label>:41:                                     ; preds = %28
  store i16 %35, i16* %3, align 2, !tbaa !4
  %42 = and i16 %35, 15
  %43 = zext i16 %42 to i64
  %44 = lshr i16 %35, 4
  %45 = zext i16 %44 to i64
  %46 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %45
  %47 = load <16 x i16>, <16 x i16>* %46, align 32, !tbaa !1
  %48 = extractelement <16 x i16> %47, i64 %43
  br label %80

; <label>:49:                                     ; preds = %28
  %50 = add nuw nsw i32 %36, 31
  %51 = and i32 %50, 31
  %52 = lshr i32 %23, %51
  %53 = and i32 %52, 1
  %54 = icmp eq i32 %53, %27
  br i1 %54, label %55, label %70

; <label>:55:                                     ; preds = %49
  %56 = and i16 %35, 15
  %57 = icmp eq i16 %56, 0
  %58 = zext i1 %57 to i16
  %59 = add nuw nsw i16 %58, %35
  %60 = and i16 %59, 15
  %61 = zext i16 %60 to i64
  %62 = lshr i16 %59, 4
  %63 = zext i16 %62 to i64
  %64 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %63
  %65 = load <16 x i16>, <16 x i16>* %64, align 32, !tbaa !1
  %66 = extractelement <16 x i16> %65, i64 %61
  %67 = and i16 %66, 31
  %68 = icmp eq i16 %67, 29
  br i1 %68, label %69, label %70

; <label>:69:                                     ; preds = %55
  store i16 %59, i16* %3, align 2, !tbaa !4
  br label %80

; <label>:70:                                     ; preds = %49, %55
  %71 = add i8 %34, 1
  %72 = icmp ult i8 %71, 16
  br i1 %72, label %28, label %73

; <label>:73:                                     ; preds = %70
  %74 = icmp eq i8 %25, 0
  %75 = add nuw nsw i64 %17, 1
  %76 = icmp ult i64 %75, %15
  %77 = and i1 %74, %76
  br i1 %77, label %16, label %78

; <label>:78:                                     ; preds = %73
  br label %79

; <label>:79:                                     ; preds = %78, %4
  store i16 -1, i16* %3, align 2, !tbaa !4
  br label %80

; <label>:80:                                     ; preds = %79, %69, %41
  %81 = phi i16 [ %48, %41 ], [ %66, %69 ], [ 0, %79 ]
  ret i16 %81
}

; Function Attrs: norecurse nounwind readonly uwtable
define i64 @text_long_derive(i8* nocapture readonly, i64) local_unnamed_addr #2 {
  br label %3

; <label>:3:                                      ; preds = %6, %2
  %4 = phi i64 [ 0, %2 ], [ %10, %6 ]
  %5 = icmp ult i64 %4, %1
  br i1 %5, label %6, label %11

; <label>:6:                                      ; preds = %3
  %7 = getelementptr inbounds i8, i8* %0, i64 %4
  %8 = load i8, i8* %7, align 1, !tbaa !1
  %9 = icmp eq i8 %8, 0
  %10 = add i64 %4, 1
  br i1 %9, label %11, label %3

; <label>:11:                                     ; preds = %6, %3
  %12 = phi i64 [ %10, %6 ], [ %4, %3 ]
  ret i64 %12
}

; Function Attrs: norecurse nounwind uwtable
define zeroext i16 @tablet_retrospective_grammar_read(i16 zeroext, i8 zeroext, <16 x i16>* nocapture readonly, i16* nocapture) local_unnamed_addr #1 {
  store i16 %0, i16* %3, align 2, !tbaa !4
  %5 = trunc i16 %0 to i8
  %6 = and i8 %5, 15
  %7 = lshr i16 %0, 4
  %8 = trunc i16 %7 to i8
  br label %9

; <label>:9:                                      ; preds = %51, %4
  %10 = phi i8 [ %6, %4 ], [ 0, %51 ]
  %11 = phi i8 [ %8, %4 ], [ -1, %51 ]
  %12 = icmp ult i8 %11, %1
  br i1 %12, label %13, label %61

; <label>:13:                                     ; preds = %9
  %14 = zext i8 %11 to i64
  %15 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %14
  %16 = load <16 x i16>, <16 x i16>* %15, align 32, !tbaa !1
  %17 = extractelement <16 x i16> %16, i64 0
  %18 = zext i16 %17 to i32
  %19 = trunc i16 %17 to i8
  %20 = and i8 %19, 1
  %21 = zext i8 %10 to i32
  %22 = icmp eq i8 %10, 0
  br i1 %22, label %51, label %23

; <label>:23:                                     ; preds = %13
  %24 = zext i8 %20 to i32
  br label %29

; <label>:25:                                     ; preds = %29
  %26 = zext i8 %37 to i32
  %27 = icmp eq i8 %37, 0
  %28 = add nsw i32 %30, -1
  br i1 %27, label %50, label %29

; <label>:29:                                     ; preds = %23, %25
  %30 = phi i32 [ %21, %23 ], [ %28, %25 ]
  %31 = phi i32 [ %21, %23 ], [ %26, %25 ]
  %32 = and i32 %31, 31
  %33 = lshr i32 %18, %32
  %34 = and i32 %33, 1
  %35 = icmp eq i32 %34, %24
  %36 = trunc i32 %30 to i8
  %37 = add nsw i8 %36, -1
  br i1 %35, label %38, label %25

; <label>:38:                                     ; preds = %29
  %39 = zext i8 %11 to i32
  %40 = shl nuw nsw i32 %39, 4
  %41 = add nuw nsw i32 %31, %40
  %42 = trunc i32 %41 to i16
  store i16 %42, i16* %3, align 2, !tbaa !4
  %43 = and i16 %42, 15
  %44 = zext i16 %43 to i64
  %45 = lshr i16 %42, 4
  %46 = zext i16 %45 to i64
  %47 = getelementptr inbounds <16 x i16>, <16 x i16>* %2, i64 %46
  %48 = load <16 x i16>, <16 x i16>* %47, align 32, !tbaa !1
  %49 = extractelement <16 x i16> %48, i64 %44
  br label %63

; <label>:50:                                     ; preds = %25
  br label %51

; <label>:51:                                     ; preds = %50, %13
  %52 = phi i32 [ %21, %13 ], [ %26, %50 ]
  %53 = icmp eq i8 %11, 0
  br i1 %53, label %9, label %54

; <label>:54:                                     ; preds = %51
  %55 = icmp eq i8 %20, 0
  br i1 %55, label %62, label %56

; <label>:56:                                     ; preds = %54
  %57 = zext i8 %11 to i32
  %58 = shl nuw nsw i32 %57, 4
  %59 = add nuw nsw i32 %52, %58
  %60 = trunc i32 %59 to i16
  store i16 %60, i16* %3, align 2, !tbaa !4
  br label %63

; <label>:61:                                     ; preds = %9
  br label %62

; <label>:62:                                     ; preds = %61, %54
  store i16 0, i16* %3, align 2, !tbaa !4
  br label %63

; <label>:63:                                     ; preds = %62, %56, %38
  %64 = phi i16 [ %49, %38 ], [ 0, %62 ], [ 0, %56 ]
  ret i16 %64
}

; Function Attrs: norecurse nounwind readnone uwtable
define zeroext i8 @tidbit_read(i8 zeroext, i8 zeroext, i64) local_unnamed_addr #0 {
  %4 = and i8 %0, 63
  %5 = zext i8 %4 to i64
  %6 = lshr i64 %2, %5
  %7 = trunc i64 %6 to i8
  %8 = and i8 %7, 1
  ret i8 %8
}

; Function Attrs: norecurse nounwind uwtable
define void @tidbit_write(i8 zeroext, i8 zeroext, i8 zeroext, i16* nocapture) local_unnamed_addr #1 {
  %5 = icmp eq i8 %1, 1
  %6 = and i8 %0, 31
  %7 = zext i8 %6 to i32
  %8 = shl i32 1, %7
  br i1 %5, label %9, label %13

; <label>:9:                                      ; preds = %4
  %10 = load i16, i16* %3, align 2, !tbaa !4
  %11 = zext i16 %10 to i32
  %12 = or i32 %11, %8
  br label %18

; <label>:13:                                     ; preds = %4
  %14 = xor i32 %8, -1
  %15 = load i16, i16* %3, align 2, !tbaa !4
  %16 = zext i16 %15 to i32
  %17 = and i32 %16, %14
  br label %18

; <label>:18:                                     ; preds = %13, %9
  %19 = phi i32 [ %17, %13 ], [ %12, %9 ]
  %20 = trunc i32 %19 to i16
  store i16 %20, i16* %3, align 2, !tbaa !4
  ret void
}

; Function Attrs: nounwind uwtable
define void @tablet_upcoming_copy(i16 zeroext, i16 zeroext, i16 zeroext, i8 zeroext, <16 x i16>* nocapture) local_unnamed_addr #4 {
  %6 = add i16 %2, %1
  %7 = icmp ult i16 %6, %1
  br i1 %7, label %39, label %8

; <label>:8:                                      ; preds = %5
  %9 = add i16 %2, %0
  br label %10

; <label>:10:                                     ; preds = %8, %10
  %11 = phi i16 [ %35, %10 ], [ %9, %8 ]
  %12 = phi i16 [ %36, %10 ], [ %6, %8 ]
  %13 = and i16 %11, 15
  %14 = icmp eq i16 %13, 0
  %15 = sext i1 %14 to i16
  %16 = add i16 %15, %11
  %17 = and i16 %16, 15
  %18 = zext i16 %17 to i64
  %19 = lshr i16 %16, 4
  %20 = zext i16 %19 to i64
  %21 = getelementptr inbounds <16 x i16>, <16 x i16>* %4, i64 %20
  %22 = load <16 x i16>, <16 x i16>* %21, align 32, !tbaa !1
  %23 = extractelement <16 x i16> %22, i64 %18
  %24 = and i16 %12, 15
  %25 = icmp eq i16 %24, 0
  %26 = sext i1 %25 to i16
  %27 = add i16 %26, %12
  %28 = and i16 %27, 15
  %29 = zext i16 %28 to i64
  %30 = lshr i16 %27, 4
  %31 = zext i16 %30 to i64
  %32 = getelementptr inbounds <16 x i16>, <16 x i16>* %4, i64 %31
  %33 = load <16 x i16>, <16 x i16>* %32, align 32
  %34 = insertelement <16 x i16> %33, i16 %23, i64 %29
  store <16 x i16> %34, <16 x i16>* %32, align 32
  %35 = add i16 %16, -1
  %36 = add i16 %27, -1
  %37 = icmp ult i16 %36, %1
  br i1 %37, label %38, label %10

; <label>:38:                                     ; preds = %10
  br label %39

; <label>:39:                                     ; preds = %38, %5
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define i64 @text_copy(i64, i8* nocapture readonly, i64, i8* nocapture) local_unnamed_addr #1 {
  %5 = icmp ne i64 %0, 0
  %6 = icmp ne i64 %2, 0
  %7 = and i1 %5, %6
  br i1 %7, label %8, label %23

; <label>:8:                                      ; preds = %4
  br label %9

; <label>:9:                                      ; preds = %8, %16
  %10 = phi i64 [ %17, %16 ], [ 0, %8 ]
  %11 = getelementptr inbounds i8, i8* %1, i64 %10
  %12 = load i8, i8* %11, align 1, !tbaa !1
  %13 = getelementptr inbounds i8, i8* %3, i64 %10
  store i8 %12, i8* %13, align 1, !tbaa !1
  %14 = load i8, i8* %11, align 1, !tbaa !1
  %15 = icmp eq i8 %14, 0
  br i1 %15, label %21, label %16

; <label>:16:                                     ; preds = %9
  %17 = add nuw i64 %10, 1
  %18 = icmp ult i64 %17, %0
  %19 = icmp ult i64 %17, %2
  %20 = and i1 %18, %19
  br i1 %20, label %9, label %21

; <label>:21:                                     ; preds = %16, %9
  %22 = phi i64 [ %17, %16 ], [ %10, %9 ]
  br label %23

; <label>:23:                                     ; preds = %21, %4
  %24 = phi i64 [ 0, %4 ], [ %22, %21 ]
  ret i64 %24
}

; Function Attrs: norecurse nounwind uwtable
define i64 @perfect_copy(i64, i8* nocapture readonly, i64, i8* nocapture) local_unnamed_addr #1 {
  %5 = icmp ne i64 %0, 0
  %6 = icmp ne i64 %2, 0
  %7 = and i1 %5, %6
  br i1 %7, label %8, label %19

; <label>:8:                                      ; preds = %4
  br label %9

; <label>:9:                                      ; preds = %8, %9
  %10 = phi i64 [ %14, %9 ], [ 0, %8 ]
  %11 = getelementptr inbounds i8, i8* %1, i64 %10
  %12 = load i8, i8* %11, align 1, !tbaa !1
  %13 = getelementptr inbounds i8, i8* %3, i64 %10
  store i8 %12, i8* %13, align 1, !tbaa !1
  %14 = add nuw i64 %10, 1
  %15 = icmp ult i64 %14, %0
  %16 = icmp ult i64 %14, %2
  %17 = and i1 %15, %16
  br i1 %17, label %9, label %18

; <label>:18:                                     ; preds = %9
  br label %19

; <label>:19:                                     ; preds = %18, %4
  %20 = phi i64 [ 0, %4 ], [ %14, %18 ]
  ret i64 %20
}

; Function Attrs: norecurse nounwind readonly uwtable
define i64 @number_situate(i16 zeroext, i64, i16* nocapture readonly) local_unnamed_addr #2 {
  %4 = icmp eq i64 %1, 0
  br i1 %4, label %16, label %5

; <label>:5:                                      ; preds = %3
  br label %6

; <label>:6:                                      ; preds = %5, %11
  %7 = phi i64 [ %12, %11 ], [ 0, %5 ]
  %8 = getelementptr inbounds i16, i16* %2, i64 %7
  %9 = load i16, i16* %8, align 2, !tbaa !4
  %10 = icmp eq i16 %9, %0
  br i1 %10, label %14, label %11

; <label>:11:                                     ; preds = %6
  %12 = add nuw i64 %7, 1
  %13 = icmp ult i64 %12, %1
  br i1 %13, label %6, label %14

; <label>:14:                                     ; preds = %11, %6
  %15 = phi i64 [ %1, %11 ], [ %7, %6 ]
  br label %16

; <label>:16:                                     ; preds = %14, %3
  %17 = phi i64 [ 0, %3 ], [ %15, %14 ]
  ret i64 %17
}

; Function Attrs: norecurse nounwind uwtable
define i64 @xorshift128plus(i64* nocapture) local_unnamed_addr #1 {
  %2 = load i64, i64* %0, align 8, !tbaa !6
  %3 = getelementptr inbounds i64, i64* %0, i64 1
  %4 = load i64, i64* %3, align 8, !tbaa !6
  store i64 %4, i64* %0, align 8, !tbaa !6
  %5 = shl i64 %2, 23
  %6 = xor i64 %5, %2
  %7 = xor i64 %6, %4
  %8 = lshr i64 %6, 17
  %9 = lshr i64 %4, 26
  %10 = xor i64 %7, %9
  %11 = xor i64 %10, %8
  store i64 %11, i64* %3, align 8, !tbaa !6
  %12 = add i64 %11, %4
  ret i64 %12
}

; Function Attrs: norecurse nounwind uwtable
define i64 @seed_random(i64* nocapture) local_unnamed_addr #1 {
  %2 = load i64, i64* %0, align 8, !tbaa !6
  %3 = getelementptr inbounds i64, i64* %0, i64 1
  %4 = load i64, i64* %3, align 8, !tbaa !6
  store i64 %4, i64* %0, align 8, !tbaa !6
  %5 = shl i64 %2, 23
  %6 = xor i64 %5, %2
  %7 = xor i64 %6, %4
  %8 = lshr i64 %6, 17
  %9 = lshr i64 %4, 26
  %10 = xor i64 %7, %9
  %11 = xor i64 %10, %8
  store i64 %11, i64* %3, align 8, !tbaa !6
  %12 = add i64 %11, %4
  ret i64 %12
}

; Function Attrs: nounwind uwtable
define void @retrospective_phrase_situate(i8 zeroext, <16 x i16>* nocapture readonly, i16 zeroext, i16 zeroext, i16* nocapture, i8* nocapture) local_unnamed_addr #4 {
  br label %7

; <label>:7:                                      ; preds = %62, %6
  %8 = phi i16 [ %2, %6 ], [ %64, %62 ]
  %9 = icmp eq i16 %8, 0
  br i1 %9, label %121, label %10

; <label>:10:                                     ; preds = %7
  %11 = trunc i16 %8 to i8
  %12 = and i8 %11, 15
  %13 = lshr i16 %8, 4
  %14 = trunc i16 %13 to i8
  br label %15

; <label>:15:                                     ; preds = %44, %10
  %16 = phi i8 [ %12, %10 ], [ 0, %44 ]
  %17 = phi i8 [ %14, %10 ], [ -1, %44 ]
  %18 = icmp ult i8 %17, %0
  br i1 %18, label %19, label %59

; <label>:19:                                     ; preds = %15
  %20 = zext i8 %17 to i64
  %21 = getelementptr inbounds <16 x i16>, <16 x i16>* %1, i64 %20
  %22 = load <16 x i16>, <16 x i16>* %21, align 32, !tbaa !1
  %23 = extractelement <16 x i16> %22, i64 0
  %24 = zext i16 %23 to i32
  %25 = zext i8 %16 to i32
  %26 = icmp eq i8 %16, 0
  br i1 %26, label %44, label %27

; <label>:27:                                     ; preds = %19
  %28 = and i16 %23, 1
  %29 = zext i16 %28 to i32
  br label %34

; <label>:30:                                     ; preds = %34
  %31 = zext i8 %42 to i32
  %32 = icmp eq i8 %42, 0
  %33 = add nsw i32 %35, -1
  br i1 %32, label %43, label %34

; <label>:34:                                     ; preds = %30, %27
  %35 = phi i32 [ %25, %27 ], [ %33, %30 ]
  %36 = phi i32 [ %25, %27 ], [ %31, %30 ]
  %37 = and i32 %36, 31
  %38 = lshr i32 %24, %37
  %39 = and i32 %38, 1
  %40 = icmp eq i32 %39, %29
  %41 = trunc i32 %35 to i8
  %42 = add nsw i8 %41, -1
  br i1 %40, label %46, label %30

; <label>:43:                                     ; preds = %30
  br label %44

; <label>:44:                                     ; preds = %43, %19
  %45 = icmp eq i8 %17, 0
  br i1 %45, label %15, label %59

; <label>:46:                                     ; preds = %34
  %47 = zext i8 %17 to i32
  %48 = shl nuw nsw i32 %47, 4
  %49 = add nuw nsw i32 %36, %48
  %50 = trunc i32 %49 to i16
  %51 = and i16 %50, 15
  %52 = zext i16 %51 to i64
  %53 = lshr i16 %50, 4
  %54 = zext i16 %53 to i64
  %55 = getelementptr inbounds <16 x i16>, <16 x i16>* %1, i64 %54
  %56 = load <16 x i16>, <16 x i16>* %55, align 32, !tbaa !1
  %57 = extractelement <16 x i16> %56, i64 %52
  %58 = icmp eq i16 %57, 0
  br i1 %58, label %60, label %62

; <label>:59:                                     ; preds = %15, %44
  br label %61

; <label>:60:                                     ; preds = %46
  br label %61

; <label>:61:                                     ; preds = %60, %59
  store i16 0, i16* %4, align 2, !tbaa !4
  store i8 0, i8* %5, align 1, !tbaa !1
  br label %122

; <label>:62:                                     ; preds = %46
  %63 = icmp eq i16 %57, %3
  %64 = add nsw i16 %50, -1
  br i1 %63, label %65, label %7

; <label>:65:                                     ; preds = %62
  %66 = trunc i16 %64 to i8
  %67 = and i8 %66, 15
  %68 = lshr i16 %64, 4
  %69 = trunc i16 %68 to i8
  br label %70

; <label>:70:                                     ; preds = %105, %65
  %71 = phi i8 [ %67, %65 ], [ 0, %105 ]
  %72 = phi i8 [ %69, %65 ], [ -1, %105 ]
  %73 = icmp ult i8 %72, %0
  br i1 %73, label %74, label %115

; <label>:74:                                     ; preds = %70
  %75 = zext i8 %72 to i64
  %76 = getelementptr inbounds <16 x i16>, <16 x i16>* %1, i64 %75
  %77 = load <16 x i16>, <16 x i16>* %76, align 32, !tbaa !1
  %78 = extractelement <16 x i16> %77, i64 0
  %79 = zext i16 %78 to i32
  %80 = trunc i16 %78 to i8
  %81 = and i8 %80, 1
  %82 = zext i8 %71 to i32
  %83 = icmp eq i8 %71, 0
  br i1 %83, label %105, label %84

; <label>:84:                                     ; preds = %74
  %85 = zext i8 %81 to i32
  br label %90

; <label>:86:                                     ; preds = %90
  %87 = zext i8 %98 to i32
  %88 = icmp eq i8 %98, 0
  %89 = add nsw i32 %91, -1
  br i1 %88, label %104, label %90

; <label>:90:                                     ; preds = %86, %84
  %91 = phi i32 [ %82, %84 ], [ %89, %86 ]
  %92 = phi i32 [ %82, %84 ], [ %87, %86 ]
  %93 = and i32 %92, 31
  %94 = lshr i32 %79, %93
  %95 = and i32 %94, 1
  %96 = icmp eq i32 %95, %85
  %97 = trunc i32 %91 to i8
  %98 = add nsw i8 %97, -1
  br i1 %96, label %99, label %86

; <label>:99:                                     ; preds = %90
  %100 = zext i8 %72 to i32
  %101 = shl nuw nsw i32 %100, 4
  %102 = add nuw nsw i32 %92, %101
  %103 = trunc i32 %102 to i16
  br label %116

; <label>:104:                                    ; preds = %86
  br label %105

; <label>:105:                                    ; preds = %104, %74
  %106 = phi i32 [ %82, %74 ], [ %87, %104 ]
  %107 = icmp eq i8 %72, 0
  br i1 %107, label %70, label %108

; <label>:108:                                    ; preds = %105
  %109 = icmp eq i8 %81, 0
  br i1 %109, label %116, label %110

; <label>:110:                                    ; preds = %108
  %111 = zext i8 %72 to i32
  %112 = shl nuw nsw i32 %111, 4
  %113 = add nuw nsw i32 %106, %112
  %114 = trunc i32 %113 to i16
  br label %116

; <label>:115:                                    ; preds = %70
  br label %116

; <label>:116:                                    ; preds = %115, %108, %99, %110
  %117 = phi i16 [ %114, %110 ], [ %103, %99 ], [ 0, %108 ], [ 0, %115 ]
  %118 = add nsw i16 %117, 1
  %119 = trunc i32 %49 to i8
  %120 = add i8 %119, 1
  br label %122

; <label>:121:                                    ; preds = %7
  br label %122

; <label>:122:                                    ; preds = %121, %116, %61
  %123 = phi i8 [ 1, %61 ], [ %120, %116 ], [ 1, %121 ]
  %124 = phi i16 [ 0, %61 ], [ %118, %116 ], [ 0, %121 ]
  store i16 %124, i16* %4, align 2, !tbaa !4
  %125 = trunc i16 %124 to i8
  %126 = sub i8 %123, %125
  store i8 %126, i8* %5, align 1, !tbaa !1
  ret void
}

; Function Attrs: norecurse nounwind readnone uwtable
define zeroext i16 @number_subtract(i16 zeroext, i16 zeroext) local_unnamed_addr #0 {
  %3 = zext i16 %0 to i32
  %4 = zext i16 %1 to i32
  %5 = sub nsw i32 %3, %4
  %6 = icmp sgt i32 %5, 0
  %7 = select i1 %6, i32 %5, i32 0
  %8 = trunc i32 %7 to i16
  ret i16 %8
}

; Function Attrs: norecurse nounwind readnone uwtable
define zeroext i16 @number_plus(i16 zeroext, i16 zeroext) local_unnamed_addr #0 {
  %3 = zext i16 %0 to i32
  %4 = zext i16 %1 to i32
  %5 = add nuw nsw i32 %4, %3
  %6 = icmp ult i32 %5, 65535
  %7 = select i1 %6, i32 %5, i32 65535
  %8 = trunc i32 %7 to i16
  ret i16 %8
}

; Function Attrs: nounwind uwtable
define void @phrase_improve(i16 zeroext, i64* nocapture, i16 zeroext, i16 zeroext, <16 x i16>* nocapture) local_unnamed_addr #4 {
  %6 = alloca i16, align 2
  %7 = alloca i8, align 1
  %8 = bitcast i16* %6 to i8*
  call void @llvm.lifetime.start(i64 2, i8* nonnull %8) #5
  store i16 0, i16* %6, align 2, !tbaa !4
  call void @llvm.lifetime.start(i64 1, i8* nonnull %7) #5
  store i8 0, i8* %7, align 1, !tbaa !1
  %9 = shl i16 %0, 1
  %10 = add i16 %9, 2
  %11 = zext i16 %10 to i64
  switch i16 %3, label %63 [
    i16 9310, label %12
    i16 10366, label %12
    i16 8318, label %12
    i16 9566, label %12
    i16 9438, label %12
    i16 16679, label %12
  ]

; <label>:12:                                     ; preds = %5, %5, %5, %5, %5, %5
  call void @retrospective_phrase_situate(i8 zeroext 1, <16 x i16>* %4, i16 zeroext %2, i16 zeroext %3, i16* nonnull %6, i8* nonnull %7)
  %13 = load i16, i16* %6, align 2, !tbaa !4
  %14 = icmp eq i16 %13, 0
  br i1 %14, label %63, label %15

; <label>:15:                                     ; preds = %12
  %16 = add i16 %13, 1
  %17 = and i16 %16, 15
  %18 = zext i16 %17 to i64
  %19 = lshr i16 %16, 4
  %20 = zext i16 %19 to i64
  %21 = getelementptr inbounds <16 x i16>, <16 x i16>* %4, i64 %20
  %22 = load <16 x i16>, <16 x i16>* %21, align 32, !tbaa !1
  %23 = extractelement <16 x i16> %22, i64 %18
  %24 = load i64, i64* %1, align 8, !tbaa !6
  %25 = getelementptr inbounds i64, i64* %1, i64 1
  %26 = load i64, i64* %25, align 8, !tbaa !6
  %27 = shl i64 %24, 23
  %28 = xor i64 %27, %24
  %29 = xor i64 %28, %26
  %30 = lshr i64 %28, 17
  %31 = lshr i64 %26, 26
  %32 = xor i64 %29, %31
  %33 = xor i64 %32, %30
  %34 = add i64 %33, %26
  %35 = add nuw nsw i64 %11, 65535
  %36 = and i64 %34, %35
  %37 = trunc i64 %36 to i32
  store i64 %33, i64* %1, align 8, !tbaa !6
  %38 = shl i64 %26, 23
  %39 = xor i64 %38, %26
  %40 = xor i64 %39, %33
  %41 = lshr i64 %39, 17
  %42 = lshr i64 %33, 26
  %43 = xor i64 %40, %42
  %44 = xor i64 %43, %41
  store i64 %44, i64* %25, align 8, !tbaa !6
  %45 = add i64 %44, %33
  %46 = and i64 %45, 1
  %47 = icmp eq i64 %46, 0
  %48 = zext i16 %23 to i32
  %49 = and i32 %37, 65535
  br i1 %47, label %50, label %54

; <label>:50:                                     ; preds = %15
  %51 = add nuw nsw i32 %49, %48
  %52 = icmp ult i32 %51, 65535
  %53 = select i1 %52, i32 %51, i32 65535
  br label %58

; <label>:54:                                     ; preds = %15
  %55 = sub nsw i32 %48, %49
  %56 = icmp sgt i32 %55, 0
  %57 = select i1 %56, i32 %55, i32 0
  br label %58

; <label>:58:                                     ; preds = %54, %50
  %59 = phi i32 [ %57, %54 ], [ %53, %50 ]
  %60 = trunc i32 %59 to i16
  %61 = load <16 x i16>, <16 x i16>* %21, align 32
  %62 = insertelement <16 x i16> %61, i16 %60, i64 %18
  store <16 x i16> %62, <16 x i16>* %21, align 32
  br label %63

; <label>:63:                                     ; preds = %5, %58, %12
  call void @llvm.lifetime.end(i64 1, i8* nonnull %7) #5
  call void @llvm.lifetime.end(i64 2, i8* nonnull %8) #5
  ret void
}

; Function Attrs: nounwind uwtable
define void @ceremony_improve(i16 zeroext, i64* nocapture, i16 zeroext, i8 zeroext, i8 zeroext, <16 x i16>* nocapture readonly, i8 zeroext, i8 zeroext, <16 x i16>* nocapture) local_unnamed_addr #4 {
  %10 = alloca <16 x i16>, align 32
  %11 = zext i8 %3 to i32
  %12 = bitcast <16 x i16>* %10 to i8*
  call void @llvm.lifetime.start(i64 32, i8* nonnull %12) #5
  %13 = zext i8 %3 to i64
  %14 = getelementptr inbounds <16 x i16>, <16 x i16>* %5, i64 %13
  %15 = load <16 x i16>, <16 x i16>* %14, align 32, !tbaa !1
  store <16 x i16> %15, <16 x i16>* %10, align 32, !tbaa !1
  %16 = zext i8 %3 to i16
  %17 = shl nuw nsw i16 %16, 4
  %18 = zext i16 %17 to i32
  %19 = shl nuw nsw i32 %11, 4
  %20 = add nuw nsw i32 %19, 16
  %21 = icmp ult i32 %18, %20
  br i1 %21, label %22, label %101

; <label>:22:                                     ; preds = %9
  %23 = zext i8 %4 to i64
  %24 = getelementptr inbounds i64, i64* %1, i64 1
  %25 = zext i16 %2 to i64
  br label %26

; <label>:26:                                     ; preds = %22, %95
  %27 = phi i32 [ %18, %22 ], [ %97, %95 ]
  %28 = phi i16 [ %17, %22 ], [ %96, %95 ]
  %29 = lshr i16 %28, 4
  %30 = trunc i16 %29 to i8
  %31 = icmp ult i8 %30, %4
  br i1 %31, label %32, label %78

; <label>:32:                                     ; preds = %26
  %33 = trunc i16 %28 to i8
  %34 = and i8 %33, 15
  %35 = and i16 %29, 255
  %36 = zext i16 %35 to i64
  br label %37

; <label>:37:                                     ; preds = %72, %32
  %38 = phi i64 [ %36, %32 ], [ %74, %72 ]
  %39 = trunc i64 %38 to i32
  %40 = shl nuw nsw i32 %39, 4
  %41 = getelementptr inbounds <16 x i16>, <16 x i16>* %5, i64 %38
  %42 = load <16 x i16>, <16 x i16>* %41, align 32, !tbaa !1
  %43 = extractelement <16 x i16> %42, i64 0
  %44 = zext i16 %43 to i32
  %45 = trunc i16 %43 to i8
  %46 = and i8 %45, 1
  %47 = zext i8 %46 to i32
  br label %50

; <label>:48:                                     ; preds = %50
  %49 = icmp ult i8 %61, 16
  br i1 %49, label %50, label %72

; <label>:50:                                     ; preds = %48, %37
  %51 = phi i8 [ 1, %37 ], [ %61, %48 ]
  %52 = zext i8 %51 to i32
  %53 = add nuw nsw i32 %52, %40
  %54 = icmp ult i32 %53, %27
  %55 = select i1 %54, i8 %34, i8 %51
  %56 = zext i8 %55 to i32
  %57 = and i32 %56, 31
  %58 = lshr i32 %44, %57
  %59 = and i32 %58, 1
  %60 = icmp eq i32 %59, %47
  %61 = add i8 %55, 1
  br i1 %60, label %62, label %48

; <label>:62:                                     ; preds = %50
  %63 = add nuw nsw i32 %56, %40
  %64 = trunc i32 %63 to i16
  %65 = and i16 %64, 15
  %66 = zext i16 %65 to i64
  %67 = lshr i16 %64, 4
  %68 = zext i16 %67 to i64
  %69 = getelementptr inbounds <16 x i16>, <16 x i16>* %5, i64 %68
  %70 = load <16 x i16>, <16 x i16>* %69, align 32, !tbaa !1
  %71 = extractelement <16 x i16> %70, i64 %66
  br label %78

; <label>:72:                                     ; preds = %48
  %73 = icmp eq i8 %46, 0
  %74 = add nuw nsw i64 %38, 1
  %75 = icmp ult i64 %74, %23
  %76 = and i1 %75, %73
  br i1 %76, label %37, label %77

; <label>:77:                                     ; preds = %72
  br label %78

; <label>:78:                                     ; preds = %77, %26, %62
  %79 = phi i16 [ %64, %62 ], [ 0, %26 ], [ 0, %77 ]
  %80 = phi i16 [ %71, %62 ], [ 0, %26 ], [ 0, %77 ]
  %81 = load i64, i64* %1, align 8, !tbaa !6
  %82 = load i64, i64* %24, align 8, !tbaa !6
  store i64 %82, i64* %1, align 8, !tbaa !6
  %83 = shl i64 %81, 23
  %84 = xor i64 %83, %81
  %85 = xor i64 %84, %82
  %86 = lshr i64 %84, 17
  %87 = lshr i64 %82, 26
  %88 = xor i64 %85, %87
  %89 = xor i64 %88, %86
  store i64 %89, i64* %24, align 8, !tbaa !6
  %90 = add i64 %89, %82
  %91 = and i64 %90, 16383
  %92 = icmp ugt i64 %91, %25
  br i1 %92, label %95, label %93

; <label>:93:                                     ; preds = %78
  %94 = and i16 %79, 15
  call void @phrase_improve(i16 zeroext %0, i64* nonnull %1, i16 zeroext %94, i16 zeroext %80, <16 x i16>* nonnull %10)
  br label %95

; <label>:95:                                     ; preds = %78, %93
  %96 = add i16 %79, 1
  %97 = zext i16 %96 to i32
  %98 = icmp ult i32 %97, %20
  br i1 %98, label %26, label %99

; <label>:99:                                     ; preds = %95
  %100 = load <16 x i16>, <16 x i16>* %10, align 32, !tbaa !1
  br label %101

; <label>:101:                                    ; preds = %99, %9
  %102 = phi <16 x i16> [ %100, %99 ], [ %15, %9 ]
  %103 = zext i8 %6 to i64
  %104 = getelementptr inbounds <16 x i16>, <16 x i16>* %8, i64 %103
  store <16 x i16> %102, <16 x i16>* %104, align 32, !tbaa !1
  call void @llvm.lifetime.end(i64 32, i8* nonnull %12) #5
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @returner(i16* nocapture) local_unnamed_addr #1 {
  store i16 0, i16* %0, align 2, !tbaa !4
  ret void
}

; Function Attrs: nounwind uwtable
define void @bloopers() local_unnamed_addr #4 {
  %1 = alloca [2 x <16 x i16>], align 32
  %2 = alloca <16 x i16>, align 32
  %3 = alloca i16, align 2
  %4 = alloca i8, align 1
  %5 = bitcast [2 x <16 x i16>]* %1 to i8*
  call void @llvm.lifetime.start(i64 64, i8* nonnull %5) #5
  %6 = bitcast <16 x i16>* %2 to i8*
  %7 = bitcast i16* %3 to i8*
  br label %8

; <label>:8:                                      ; preds = %0, %45
  %9 = phi i16 [ 0, %0 ], [ %47, %45 ]
  %10 = zext i16 %9 to i32
  %11 = lshr i16 %9, 4
  %12 = trunc i16 %11 to i8
  %13 = icmp eq i8 %12, 0
  br i1 %13, label %14, label %45

; <label>:14:                                     ; preds = %8
  %15 = trunc i16 %9 to i8
  %16 = and i8 %15, 15
  %17 = and i16 %11, 255
  %18 = zext i16 %17 to i64
  %19 = shl nuw nsw i16 %17, 4
  %20 = zext i16 %19 to i32
  %21 = getelementptr inbounds [2 x <16 x i16>], [2 x <16 x i16>]* %1, i64 0, i64 %18
  %22 = load <16 x i16>, <16 x i16>* %21, align 32, !tbaa !1
  %23 = extractelement <16 x i16> %22, i64 0
  %24 = zext i16 %23 to i32
  %25 = and i16 %23, 1
  %26 = zext i16 %25 to i32
  br label %29

; <label>:27:                                     ; preds = %29
  %28 = icmp ult i8 %40, 16
  br i1 %28, label %29, label %44

; <label>:29:                                     ; preds = %27, %14
  %30 = phi i8 [ 1, %14 ], [ %40, %27 ]
  %31 = zext i8 %30 to i32
  %32 = add nuw nsw i32 %31, %20
  %33 = icmp ult i32 %32, %10
  %34 = select i1 %33, i8 %16, i8 %30
  %35 = zext i8 %34 to i32
  %36 = and i32 %35, 31
  %37 = lshr i32 %24, %36
  %38 = and i32 %37, 1
  %39 = icmp eq i32 %38, %26
  %40 = add i8 %34, 1
  br i1 %39, label %41, label %27

; <label>:41:                                     ; preds = %29
  %42 = add nuw nsw i32 %35, %20
  %43 = trunc i32 %42 to i16
  br label %45

; <label>:44:                                     ; preds = %27
  br label %45

; <label>:45:                                     ; preds = %44, %8, %41
  %46 = phi i16 [ %43, %41 ], [ 0, %8 ], [ 0, %44 ]
  call void @llvm.lifetime.start(i64 32, i8* nonnull %6) #5
  call void @llvm.lifetime.start(i64 2, i8* nonnull %7) #5
  call void @llvm.lifetime.start(i64 1, i8* nonnull %4) #5
  call void @retrospective_phrase_situate(i8 zeroext 1, <16 x i16>* nonnull %2, i16 zeroext 1, i16 zeroext 1, i16* nonnull %3, i8* nonnull %4)
  call void @llvm.lifetime.end(i64 1, i8* nonnull %4) #5
  call void @llvm.lifetime.end(i64 2, i8* nonnull %7) #5
  call void @llvm.lifetime.end(i64 32, i8* nonnull %6) #5
  %47 = add nsw i16 %46, 1
  %48 = icmp ult i16 %47, 16
  br i1 %48, label %8, label %49

; <label>:49:                                     ; preds = %45
  call void @llvm.lifetime.end(i64 64, i8* nonnull %5) #5
  ret void
}

; Function Attrs: nounwind uwtable
define void @lwonprom2() local_unnamed_addr #4 !kernel_arg_addr_space !8 !kernel_arg_access_qual !8 !kernel_arg_type !8 !kernel_arg_base_type !8 !kernel_arg_type_qual !8 {
  %1 = alloca [2 x <16 x i16>], align 32
  %2 = alloca <16 x i16>, align 32
  %3 = alloca i16, align 2
  %4 = alloca i8, align 1
  %5 = alloca [2 x <16 x i16>], align 32
  %6 = alloca [2 x i64], align 16
  %7 = bitcast [2 x <16 x i16>]* %5 to i8*
  call void @llvm.lifetime.start(i64 64, i8* nonnull %7) #5
  %8 = bitcast [2 x i64]* %6 to i8*
  call void @llvm.lifetime.start(i64 16, i8* nonnull %8) #5
  %9 = getelementptr inbounds [2 x i64], [2 x i64]* %6, i64 0, i64 0
  %10 = getelementptr inbounds [2 x <16 x i16>], [2 x <16 x i16>]* %5, i64 0, i64 0
  call void @ceremony_improve(i16 zeroext 10, i64* nonnull %9, i16 zeroext 10, i8 zeroext 10, i8 zeroext 10, <16 x i16>* nonnull %10, i8 zeroext 10, i8 zeroext 10, <16 x i16>* nonnull %10)
  %11 = bitcast [2 x <16 x i16>]* %1 to i8*
  call void @llvm.lifetime.start(i64 64, i8* nonnull %11) #5
  %12 = bitcast <16 x i16>* %2 to i8*
  %13 = bitcast i16* %3 to i8*
  br label %14

; <label>:14:                                     ; preds = %51, %0
  %15 = phi i16 [ 0, %0 ], [ %53, %51 ]
  %16 = zext i16 %15 to i32
  %17 = lshr i16 %15, 4
  %18 = trunc i16 %17 to i8
  %19 = icmp eq i8 %18, 0
  br i1 %19, label %20, label %51

; <label>:20:                                     ; preds = %14
  %21 = trunc i16 %15 to i8
  %22 = and i8 %21, 15
  %23 = and i16 %17, 255
  %24 = zext i16 %23 to i64
  %25 = shl nuw nsw i16 %23, 4
  %26 = zext i16 %25 to i32
  %27 = getelementptr inbounds [2 x <16 x i16>], [2 x <16 x i16>]* %1, i64 0, i64 %24
  %28 = load <16 x i16>, <16 x i16>* %27, align 32, !tbaa !1
  %29 = extractelement <16 x i16> %28, i64 0
  %30 = zext i16 %29 to i32
  %31 = and i16 %29, 1
  %32 = zext i16 %31 to i32
  br label %35

; <label>:33:                                     ; preds = %35
  %34 = icmp ult i8 %46, 16
  br i1 %34, label %35, label %50

; <label>:35:                                     ; preds = %33, %20
  %36 = phi i8 [ 1, %20 ], [ %46, %33 ]
  %37 = zext i8 %36 to i32
  %38 = add nuw nsw i32 %37, %26
  %39 = icmp ult i32 %38, %16
  %40 = select i1 %39, i8 %22, i8 %36
  %41 = zext i8 %40 to i32
  %42 = and i32 %41, 31
  %43 = lshr i32 %30, %42
  %44 = and i32 %43, 1
  %45 = icmp eq i32 %44, %32
  %46 = add i8 %40, 1
  br i1 %45, label %47, label %33

; <label>:47:                                     ; preds = %35
  %48 = add nuw nsw i32 %41, %26
  %49 = trunc i32 %48 to i16
  br label %51

; <label>:50:                                     ; preds = %33
  br label %51

; <label>:51:                                     ; preds = %50, %47, %14
  %52 = phi i16 [ %49, %47 ], [ 0, %14 ], [ 0, %50 ]
  call void @llvm.lifetime.start(i64 32, i8* nonnull %12) #5
  call void @llvm.lifetime.start(i64 2, i8* nonnull %13) #5
  call void @llvm.lifetime.start(i64 1, i8* nonnull %4) #5
  call void @retrospective_phrase_situate(i8 zeroext 1, <16 x i16>* nonnull %2, i16 zeroext 1, i16 zeroext 1, i16* nonnull %3, i8* nonnull %4) #5
  call void @llvm.lifetime.end(i64 1, i8* nonnull %4) #5
  call void @llvm.lifetime.end(i64 2, i8* nonnull %13) #5
  call void @llvm.lifetime.end(i64 32, i8* nonnull %12) #5
  %53 = add nsw i16 %52, 1
  %54 = icmp ult i16 %53, 16
  br i1 %54, label %14, label %55

; <label>:55:                                     ; preds = %51
  call void @llvm.lifetime.end(i64 64, i8* nonnull %11) #5
  call void @llvm.lifetime.end(i64 16, i8* nonnull %8) #5
  call void @llvm.lifetime.end(i64 64, i8* nonnull %7) #5
  ret void
}

attributes #0 = { norecurse nounwind readnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind }
attributes #4 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.1-6 (tags/RELEASE_401/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"omnipotent char", !3, i64 0}
!3 = !{!"Simple C/C++ TBAA"}
!4 = !{!5, !5, i64 0}
!5 = !{!"short", !2, i64 0}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !2, i64 0}
!8 = !{}
