/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef INTERPRET_H
#define INTERPRET_H
#include "compile.h"
#include "found.h"
#include "math.h"
#include "sort.h"
#include "compare.h"
#include "tlep.h"
//#include "string.h"
#include "translation.h"
#define ACCUSATIVE_SHORTTERM 0
#define INSTRUMENTAL_SHORTTERM 1
#define DATIVE_SHORTTERM 2
#define NOMINATIVE_SHORTTERM 3
#define COMMAND_SHORTTERM 8
// void plus_interpret(const line_t input, const uint8_t max_program_long,
//                    const line_t *program,
//                    const uint16_t input_tablet_indexFinger, line_t *produce);
// void return_interpret(const line_t input, const uint8_t max_program_long,
//                      const line_t *program, line_t *produce);
void invert_interpret(const line_t input, const uint8_t max_program_long,
                      const line_t *program,
                      const uint16_t input_tablet_indexFinger, line_t *produce);
void down_conditional_interpret(const line_t input,
                                const uint8_t max_program_long,
                                const line_t *program,
                                const uint16_t input_tablet_indexFinger,
                                line_t *produce);
void high_conditional_interpret(const line_t input,
                                const uint8_t max_program_long,
                                const line_t *program,
                                const uint16_t input_tablet_indexFinger,
                                line_t *produce);
void subtract_interpret(const line_t input, const uint8_t max_program_long,
                        const line_t *program,
                        const uint16_t input_tablet_indexFinger,
                        line_t *produce);
void conditional_interpret(const line_t input, const uint8_t max_program_long,
                           const line_t *program, line_t *produce);

uint16_t /*produce_long*/ independentClause_interpret(
    uint16_t knowledge_attribute[], line_t *knowledge, uint16_t *indexFinger,
    uint8_t *iteration, uint16_t *done, const uint16_t input_long,
    const line_t *input, const uint16_t produce_max_long,
    line_t *extern_produce);

struct Phrase /*produce*/ neo_independentClause_interpret(
    struct Paragraph knowledge_phrase,
     uint16_t *done, 
    const struct Phrase input_phrase, struct Phrase produce_phrase);

struct Paragraph
knowledge_interpret(struct Paragraph knowledge_phrase,
                    const struct Phrase input_phrase,
                    struct Phrase produce_phrase);
uint16_t pyash_to_pyash_interpret(struct Paragraph knowledge, 
                                  const struct Text input,
                                  const uint16_t sort,
                                  const uint16_t produce_text_long,
                                  char produce_text[]);

struct Text /* produce_text */ 
  neo_pyash_to_pyash_interpret(struct Phrase program, const struct Text input, 
      const uint16_t sort, struct Text produce);

// translates a single variable from a name to a value
// // returns the length of the produce phrase
uint16_t /*amount_added_to_produce_indexFinger*/
upcoming_variable_to_worth_translate(
    const uint16_t knowledge_long, const line_t knowledge[],
    const uint16_t input_long, const line_t input[], uint16_t *indexFinger,
    const uint16_t produce_indexFinger, const uint16_t produce_long,
    line_t produce[]);

struct Phrase /* produce */
tlasci_fyakyi_tsinci_fyakyu_hvatprifci_fyakka_ryantu(
    struct Phrase htikya, const struct Phrase hyakya,
    const struct Phrase hkakya);

#define /*produce*/                                                            \
    translate_theUpcomingVariablePhrase_ToWorthPhrase_ByKnowledgePhrase(       \
        thePhrase, toPhrase, byPhrase)                                         \
  tlasci_fyakyi_tsinci_fyakyu_hvatprifci_fyakka_ryantu(toPhrase, byPhrase,     \
                                                       thePhrase)

struct Phrase /* input phrase, and produce phrase */
neo_upcoming_variable_to_worth_translate(const struct Phrase knowledge,
                                         const struct Phrase input,
                                         struct Phrase produce);

uint16_t /*produce_long*/
all_variable_to_worth_translate(const uint16_t knowledge_long,
                                const line_t knowledge[],
                                const uint16_t input_long, const line_t input[],
                                const uint16_t produce_long, line_t produce[]);
struct Phrase /* produce */
neo_all_variable_to_worth_translate(const struct Phrase knowledge,
                                    const struct Phrase input,
                                    struct Phrase produce);


struct Phrase /*command_list */ recipe_load(const struct Phrase knowledge,
                                            const struct Phrase input,
                                            struct Phrase command_list);


#define neo_number_extract(program, phrase)                                    \
  number_extract(program.page.plength, program.page.lines, phrase.begin,       \
                 phrase.length);








uint16_t phrase_number_extract(const struct Phrase htin,
                               const word_t phrase_code);
struct Phrase /* produce */ dat_plus_interpret(const struct Phrase knowledge,
                                               const struct Phrase input,
                                               struct Phrase produce);
struct Phrase /* ksim phrase */
kwonyi_rwekgvakfyakti_nwonhtinka_plus(const struct Phrase ksim,
                                      const struct Phrase input,
                                      struct Phrase dictionary);

struct Phrase /* produce */
kwonyi_wigvakfyakti_nwonhtinka_plus(const struct Phrase ksim,
                                    const struct Phrase input,
                                    struct Phrase dictionary);

struct Phrase /*produce */
nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu(struct Phrase htikya,
                                            const struct Phrase hkakya);

// #define /*produce*/
//    translate_theFinalIndependentClauseZGrammaticalCasePhrases_
//    toNominativeIndependentClauses(
//        input, knowledge)
//  nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu(knowledge, input)

struct Phrase /*htikya */
nwonhtinyi_gvakfyakka_ryantu(struct Phrase htikya, const struct Phrase hkakya);
#define /*produce*/                                                            \
    translate_theGrammaticalCasePhrase_toNominativeIndependentClause(input,    \
                                                                     produce)  \
  nwonhtinyi_gvakfyakka_ryantu(produce, input)

struct Phrase /* input remainder */
conditionalMood_interpret(const struct Phrase knowledge, struct Phrase input);

struct Phrase /* produce */
base_command_interpret(const struct Phrase knowledge, const struct Phrase input,
                       struct Phrase produce);

struct Phrase /* produce */
neo_num_acc_ins_plus_interpret(const struct Phrase knowledge,
                               const struct Phrase input,
                               struct Phrase produce);

struct Phrase /* produce */ neo_plus_interpret(struct Phrase knowledge,
                                               struct Phrase input,
                                               struct Phrase produce);

struct Phrase /* produce */ neo_subtract_interpret(struct Phrase knowledge,
                                               struct Phrase input,
                                               struct Phrase produce);
struct Phrase /*produce*/
dopwih_doka_giant_interpret(const struct Phrase knowledge,
                            const struct Phrase input, struct Phrase produce);

struct Phrase /*produce*/ deonticMood_interpret(struct Phrase knowledge,
                                                struct Phrase input,
                                                struct Phrase produce);


word_t /* gvakhbistlat */ hbistlatyi_gvakka_ryantu(word_t gvak);
#define translate_grammaticalCase_toBaseWord(grammaticalCase)                  \
  hbistlatyi_gvakka_ryantu(grammaticalCase)



uint16_t /*produce_long*/ realisMood_interpret(const uint16_t input_long,
                                               const line_t input[],
                                               const uint16_t max_produce_long,
                                               line_t produce[]);
struct Phrase  /*produce*/ neo_realisMood_interpret(struct Phrase input,
                                               struct Phrase produce);
uint16_t /*produce_long*/
interrogativeMood_interpret(const uint16_t input_long, const line_t input[],
                            const uint16_t max_produce_long, line_t produce[]);
struct Phrase /*produce*/
neo_interrogativeMood_interpret(const struct Phrase input,
                            struct Phrase produce);

void conditional_independentClause_interpret(uint16_t program_attribute[],
                                             const line_t program[],
                                             uint16_t *tablet_indexFinger,
                                             uint8_t *iteration,
                                             line_t *variable_produce);
struct Phrase /*produce*/
neo_conditional_independentClause_interpret(struct Phrase program,
                                             uint16_t *tablet_indexFinger,
                                             uint8_t *iteration,
                                             struct Phrase produce);
struct Paragraph
  reform_theParagraph_byTheVariable(struct Paragraph produce_phrase, 
      const struct Sentence variable_phrase);
struct Paragraph drop_htin(struct Paragraph paragraph, const uint16_t quantum);
uint16_t phrase_letter_extract(const struct Phrase htin,
                               const uint16_t phrase_code);
struct Phrase maximize_variable(struct Phrase variable_phrase, struct Phrase produce);

#define interpretGo_inputSentence_withKnowledgeParagraph_toProduceSentence(input, knowledge, produce) \
	hkuttyafmwah_psashtinyi_hkinhtinka_tyiftu(knowledge, produce, input)

struct Sentence /*psashtin*/ hkuttyafmwah_psashtinyi_hkinhtinka_tyiftu(struct Paragraph hkut, 
		struct Sentence psas, const struct Sentence nrup);

#endif

