/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef KSIKTFIKGE_H
#define KSIKTFIKGE_H
#include "linenoise/linenoise.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "dialogue.h"
#include "encoding.h"
#include "tlep.h"
#include "interpret.h"
#include "lwonprom.h"
#include "prng.h"
#include "pyashWords.h"
#include "pyashWords.h"
#include "sort.h"
#include "translation.h"

#define Text_metadata uint16_t *
#define Text_pad_iterator 0
#define Line_iterator 1

#endif
