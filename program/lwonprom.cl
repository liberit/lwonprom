#define OPENCL
#define OPENCL_1_2_COMPAT
#define ARRAYLINE
//#define LLVM
#include "interpret.c"
#include "lwonprom.c"
#include "lwonprom.h"
#include "parser.c"
#include "prng.c"
#include "math.h"
#include "math.c"
#include "pyash.c"
#include "pyash.h"
#include "pyashWords.h"
#include "sort.c"
#include "sort.h"
#include "translation.c"
#include "compile.h"
#include "compile.c"
#include "found.c"
#include "compare.c"
//#include "encoding.c"
//#include "analyst.c"

#define TRANSMUTATION_PROBABILITY_MASK 0x3FFF
#define GRAMMATICALCASE_LIST_LONG 6

// okay what should we pass to this to make evolutionary islands?
// we need the recipe
// we need the training sequence
// we need the climate

// do we need the population?
// do we need the fresh population?
// do we need population health?
// do we need champion_iteration_sequence?

kernel void lwonprom3(random_t random_seed_input, 
                               __global char* output_buffer) {
  size_t world_iteration = get_global_id(0);
  size_t regional_iteration = get_local_id(0);
       output_buffer[world_iteration] = 'a';
}

kernel void lwonprom4(random_t random_seed_input, 
                               __global uint16_t* output_buffer) {
  size_t world_iteration = get_global_id(0);
  size_t regional_iteration = get_local_id(0);
       output_buffer[0x100*world_iteration] = 'a';
}

kernel void lwonprom2(random_t random_seed_input) {
  // size_t world_iteration = get_global_id(0);
  // size_t regional_iteration = get_local_id(0);

}
kernel void lwonprom5(random_t random_seed_input, 
                               global line_t* output_buffer) {
  size_t world_iteration = get_global_id(0);
  size_t regional_iteration = get_local_id(0);
  output_buffer[world_iteration][0] = 0x61;
}
kernel void lwonprom6(random_t random_seed_input, 
                               global line_t* recipe_lines, 
                               global line_t* output_buffer) {
  size_t world_iteration = get_global_id(0);
  size_t regional_iteration = get_local_id(0);
  NewClimate(climate, random_seed_input, 0x100, 0xC000);
//   NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  NewPagePhrase(recipe, 1);
  recipe.begin = 1;
  recipe.length = 0xF;
  recipe.page.plength = 1;
  repeat(LINE_LONG,
    recipe.page.lines[0][iterator] = recipe_lines[0][iterator]);

  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
//   // training sequence
//   NewTrainingSequence(training_sequence, 2, "hlishyikdokali hlistyutdokali ",
//                       "ksashyikdokali ksastyutdokali ");
//   // population quiz
//   uint32_t population_health[30];
//   uint64_t champion_iteration_sequence[30];
//   fresh_population = evolutionary_island_establish(
//       climate, recipe, training_sequence, population, fresh_population,
//       population_health, champion_iteration_sequence);
}
kernel void lwonprom7(random_t random_seed_input, 
                               global line_t* recipe_lines, 
                               global line_t* input_lines, 
                               global line_t* produce_lines, 
                               global line_t* output_buffer) {
  size_t world_iteration = get_global_id(0);
  size_t regional_iteration = get_local_id(0);
  repeat(world_iteration, seed_random(random_seed_input));
  NewClimate(climate, random_seed_input, 0x100, 0xC000);
//   NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  NewPagePhrase(recipe, 1);
  recipe.begin = 1;
  recipe.length = 0xF;
  recipe.page.plength = 1;
  repeat(LINE_LONG,
    recipe.page.lines[0][iterator] = recipe_lines[0][iterator]);

  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
//   // training sequence
//   NewTrainingSequence(training_sequence, 2, "hlishyikdokali hlistyutdokali ",
//                       "ksashyikdokali ksastyutdokali ");
  NewHollowTrainingSequence(training_sequence, 2);
  uint8_t line_number = 0;
  repeat_with(2, line_number,
  repeat(LINE_LONG,
          training_sequence[0].page.lines[line_number][iterator] = 
          input_lines[line_number][iterator]));
  line_number = 0;
  repeat_with(2, line_number,
  repeat(LINE_LONG,
          training_sequence[1].page.lines[line_number][iterator] = 
          produce_lines[line_number][iterator]));
//   // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
       climate, recipe, training_sequence, population, fresh_population,
       population_health, champion_iteration_sequence);
  global_line_copy(fresh_population.people[champion_iteration_sequence[0]].page.lines,
              output_buffer[world_iteration]);
  
}

kernel void lwonprom() {
  size_t world_iteration = get_global_id(0);
  size_t regional_iteration = get_local_id(0);
  char hello[] = "hello";
  char hello2[16];
  //  // text_copy(6, hello, 16, hello2);
  uint64_t random_seed[2] = {0xBEEFBEEF, 0xFEEDFEED};
  random_seed[0] += world_iteration;
  random_seed_establish(random_seed[0], 2, random_seed);
  const uint8_t max_program_long = 0x10;
#define TRAINING_SERIES_LONG 11
  const uint16_t training_series_long = TRAINING_SERIES_LONG;
  const line_t training_series[TRAINING_SERIES_LONG][2] = {
      //{'a', 'A'}, {'b', 'B'}, {'z', 'Z'}}; // return input
      {'A', 'a'}, {'B', 'b'},   {'Z', 'z'},   {'D', 'd'},
      {'C', 'c'}, {0x10, 0x10}, {0x30, 0x30}, {'@', '@'},
      {'[', '['}, {'z', 'z'}}; // return input
#define CEREMONY_LONG 1
  const uint16_t ceremony_long = CEREMONY_LONG;
  const line_t ceremony[CEREMONY_LONG] = {
      // {0x529, 0x881D, 0x40, 0x4127, 0xE868, 0x29BE, 0x881D, 0x1, 0x287E,
      // 0x8C64, 0x217E, 0x0, 0x0, 0x0, 0x0, 0x0},
      {0xA529, 0x881D, 0x1, 0x4127, 0xE868, 0x29BE, 0x881D, 0x21, 0x4127,
       0xEA10, 0x29BE, 0x881D, 0x1, 0x287E, 0x8C64, 0x217E}};
  line_t program_produce[TRAINING_SERIES_LONG] = {0};
  const uint8_t population_long = POPULATION_LONG;
  line_t population[POPULATION_LONG][PERSON_LONG];
  line_t fresh_population[POPULATION_LONG][PERSON_LONG];
  // printf("0x(%#v16hX)", fresh_population[0][0]);
  uint32_t population_health[POPULATION_LONG];
  uint64_t champion_iteration_series[POPULATION_LONG];
  uint8_t iteration = 10;
  uint16_t end_index = 0;
  uint16_t new_index = 0;
  uint16_t check = (iteration + 1) * HTIN_LONG;
  uint16_t transmutation_probability = 0x4000;
  uint16_t intermediate =
      seed_random(random_seed) & TRANSMUTATION_PROBABILITY_MASK;
  intermediate = intermediate <= transmutation_probability;
  //old_evolutionary_island_establish(random_seed, ceremony_long, ceremony,
  //                              max_program_long, training_series_long,
  //                              training_series, program_produce,
  //                              population_long, population, fresh_population,
  //                              population_health, champion_iteration_series);
  printf("hello world, %d global_id, %d local_id, 0x%lX seed\n",
         world_iteration, regional_iteration, random_seed[0]);
}
