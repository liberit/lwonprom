/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
//#include <guarantee.h>
//#include <stdio.h>
//#include <string.h>

//#include "analyst.h"
#include "encoding.h"
//#include "pyash.h"
//#include "seed.h"
//#include "sort.h"

/** phrase_encode
 * is a secondary encoding pass, where number words are turned into numbers,
 * and address names are turned into their values */
struct Phrase /* produce */ phrase_encode(word_t number, struct Phrase input) {
  switch (number) {
  case number_GRAMMAR:
    input = number_grammar_encode(input);
    break;
  case referential_GRAMMAR:
    input = number_grammar_encode(input);
    break;
    // it's a hash name, so can use something like in the normal name
  default:
    if (is_grammatical_case_word(number) == truth_WORD) {
      input.length = input.length - 1;
      input = addenda_theGrammarWord_toThePhrase(number, input);
      input.length += 1;
    } else if (is_perspective_word(number) == truth_WORD) {
      input.length = input.length - 1;
      input = addenda_theGrammarWord_toThePhrase(number, input);
      input.length += 1;
      // What is this?
      // assuming it is a regular word
    } else {
      input.length = input.length - 1;
      input = addenda_theWord_toThePhrase(number, input);
      input.length += 1;
    }
    break;
  }
  return input;
}
/** phrase_encode
 * is a secondary encoding pass, where number words are turned into numbers,
 * and address names are turned into their values */
struct Phrase /* produce */ neo_phrase_encode(word_t number,
                                              struct Phrase input) {
  // text_phrase_print(input);
  // Page_print(input.page);
  switch (number) {
  case number_GRAMMAR:
    input = number_grammar_encode(input);
    input.length -= 1;
    break;
  case referential_GRAMMAR:
    input = number_grammar_encode(input);
    break;
    // it's a hash name, so can use something like in the normal name
  default:
    if (is_grammatical_case_word(number) == truth_WORD) {
      input = addenda_theGrammarWord_toThePhrase(number, input);
    } else if (is_perspective_word(number) == truth_WORD) {
      input = addenda_theGrammarWord_toThePhrase(number, input);
      // What is this?
      // assuming it is a regular word
    } else {
      input = addenda_theWord_toThePhrase(number, input);
    }
    break;
  }
  return input;
}

extern inline void code_ACC_consonant_one(const uint8_t type,
                                          const uint8_t consonant_one,
                                          uint16_t *number) {
  uint8_t i, consonant_number = CONSONANT_ONE_ENCODE_LONG;
  guarantee(consonant_Q((char)consonant_one) == TRUE);
  if (consonant_one != 0 && type != SHORT_ROOT) {
    for (i = 0; i < CONSONANT_ONE_ENCODE_LONG; i++) {
      if (consonant_one_code_group[i][0] == consonant_one) {
        consonant_number = consonant_one_code_group[i][1];
        guarantee(consonant_number < CONSONANT_ONE_ENCODE_LONG);
        if (type == LONG_ROOT) {
          *number = consonant_number;
          break;
        } else if (type == LONG_GRAMMAR) {
          *number |= (uint16_t)(consonant_number << BANNER_THICK);
          break;
        } else if (type == SHORT_GRAMMAR) {
          *number |= (uint16_t)(consonant_number << CONSONANT_ONE_THICK);
          break;
        }
      }
    }
  }
  guarantee(consonant_number != CONSONANT_ONE_ENCODE_LONG);
}

uint8_t vector_long_find(uint16_t number) {
  if (number < 5)
    return (uint8_t)number;
  if (number <= 8)
    return 8;
  if (number <= 16)
    return 16;
  return 0;
}

uint16_t vector_code(uint8_t vector_long) {
  // returns greater than MAXIMUM_VECTOR_CODE if invalid
  if (vector_long == 1)
    return VECTOR_THICK_1;
  if (vector_long == 2)
    return VECTOR_THICK_2;
  if (vector_long == 3)
    return VECTOR_THICK_3;
  if (vector_long == 4)
    return VECTOR_THICK_4;
  if (vector_long == 8)
    return VECTOR_THICK_8;
  if (vector_long == 16)
    return VECTOR_THICK_16;
  guarantee(1 == 0);
  return MAXIMUM_VECTOR_CODE + 1;
}

extern inline void code_ACC_consonant_two(const uint8_t type,
                                          const uint8_t consonant_two,
                                          uint16_t *number) {
  uint8_t i, consonant_number = CONSONANT_TWO_ENCODE_LONG;
  uint16_t start_number = *number;
  // printf("n %04X, t %X c2 %c \n", (uint) *number,
  //       (uint) type, (char) consonant_two);
  guarantee(consonant_Q((char)consonant_two) == TRUE);
  if (consonant_two != 0 && type == SHORT_ROOT) {
    for (i = 0; i < CONSONANT_ONE_ENCODE_LONG; i++) {
      if (consonant_one_code_group[i][0] == consonant_two) {
        consonant_number = consonant_one_code_group[i][1];
        guarantee(consonant_number < CONSONANT_ONE_ENCODE_LONG);
        *number |= (uint16_t)(consonant_number << BANNER_THICK);
        break;
      }
    }
  }
  if (consonant_two != 0 && type != SHORT_ROOT && type != SHORT_GRAMMAR) {
    for (i = 0; i < CONSONANT_TWO_ENCODE_LONG; i++) {
      if (consonant_two_code_group[i][0] == consonant_two) {
        consonant_number = consonant_two_code_group[i][1];
        guarantee(consonant_number < CONSONANT_TWO_ENCODE_LONG);
        if (type == LONG_ROOT) {
          // printf("C2LR cn %04X\n", (uint) consonant_number);
          // printf("C2LR %04X\n", (uint) consonant_number <<
          //       (CONSONANT_ONE_THICK));
          *number |= (uint16_t)(consonant_number << CONSONANT_ONE_THICK);
          break;
        } else if (type == LONG_GRAMMAR) {
          *number |= (uint16_t)(consonant_number
                                << (BANNER_THICK + CONSONANT_ONE_THICK));
          break;
        }
      }
    }
  }
  guarantee(consonant_number == 0 || *number != start_number);
  guarantee(consonant_number != CONSONANT_ONE_ENCODE_LONG);
}

extern inline void code_ACC_type(const char *word,
                                 const uint8_t ACC_GEN_magnitude, uint8_t *type,
                                 uint16_t *number) {
  guarantee(word != NULL);
  guarantee(ACC_GEN_magnitude <= WORD_LONG);
  // printf("w %s\n", word);
  if (ACC_GEN_magnitude == 2 || ACC_GEN_magnitude == 3) {
    *type = SHORT_GRAMMAR;
    *number = 30;
  } else if (ACC_GEN_magnitude == 4 && word[3] == 'h') {
    *type = LONG_GRAMMAR;
    *number = 7;
  } else if (ACC_GEN_magnitude == 4 && word[0] == 'h') {
    *type = SHORT_ROOT;
    *number = 0;
  } else if (ACC_GEN_magnitude == 4) {
    *type = LONG_ROOT;
    *number = 0;
  } else if (ACC_GEN_magnitude == 5 && word[0] == 'h') {
    *type = SHORT_ROOT;
    *number = 0;
  } else if (ACC_GEN_magnitude == 5 && word[4] == 'h') {
    *type = LONG_GRAMMAR;
    *number = 7;
  } else if (ACC_GEN_magnitude == 5) {
    *type = LONG_ROOT;
    *number = 0;
  } else {
    *type = ERROR_BINARY;
    *number = 0;
  }
  guarantee(*type != ERROR_BINARY);
}

#define code_exit                                                              \
  *DAT_number = 0;                                                             \
  return;
void word_code_encode(const uint8_t ACC_GEN_magnitude, const char *word,
                      uint16_t *DAT_number) {
  /* Algorithm:
      TEL set ACC NUM zero DAT number DEO
      identify type of word
      if ACC word class ESS short root word
      then TEL mark ACC first three binary DAT NUM zero
      else-if ACC word class ESS long grammar word
      then TEL mark ACC first three binary DAT NUM one
      else-if ACC word class ESS short grammar word
      then TEL mark ACC first byte DAT NUM thirty
      UNQ glyph INE indexFinger POSC word QUOT process
      ATEL search ACC code table BEN glyph DAT glyph number DEO
      TEL multiply ACC glyph number INS indexFinger DAT indexFinger number DEO
      TEL add ACC indexFinger number DAT number DEO
      process QUOT DEO
  */
  uint8_t consonant_one = 0, consonant_two = 0, vowel = 0, tone = 0,
          consonant_three = 0, type = LONG_ROOT;
  uint16_t number = 0;
  guarantee(ACC_GEN_magnitude > 0);
  guarantee(ACC_GEN_magnitude <= WORD_LONG);
  guarantee(word != NULL);
  guarantee(DAT_number != NULL);
  code_ACC_type(word, ACC_GEN_magnitude, &type, &number);
  // printf("type %X\n", (uint) number);
  /* TEL fill ACC glyph variable PL */
  consonant_one = (uint8_t)word[0];
  if (consonant_Q((char)consonant_one) == FALSE) {
    code_exit;
  }
  guarantee(consonant_Q((char)consonant_one) == TRUE);
  if (ACC_GEN_magnitude == 2) {
    vowel = (uint8_t)word[1];
    if (vowel_Q((char)vowel) == FALSE) {
      code_exit;
    }
    guarantee(vowel_Q((char)vowel) == TRUE);
  } else if (ACC_GEN_magnitude == 3) {
    vowel = (uint8_t)word[1];
    tone = (uint8_t)word[2];
    if (vowel_Q((char)vowel) == FALSE || tone_Q((char)tone) == FALSE) {
      code_exit;
    }
    guarantee(vowel_Q((char)vowel) == TRUE);
    guarantee(tone_Q((char)tone) == TRUE);
  } else if (ACC_GEN_magnitude == 4) {
    consonant_two = (uint8_t)word[1];
    vowel = (uint8_t)word[2];
    consonant_three = (uint8_t)word[3];
    if (consonant_Q((char)consonant_two) == FALSE ||
        vowel_Q((char)vowel) == FALSE ||
        consonant_Q((char)consonant_three) == FALSE) {
      code_exit;
    }
    guarantee(consonant_Q((char)consonant_two) == TRUE);
    guarantee(vowel_Q((char)vowel) == TRUE);
    guarantee(consonant_Q((char)consonant_three) == TRUE);
  } else if (ACC_GEN_magnitude == 5) {
    consonant_two = (uint8_t)word[1];
    vowel = (uint8_t)word[2];
    tone = (uint8_t)word[3];
    consonant_three = (uint8_t)word[4];
    if (consonant_Q((char)consonant_two) == FALSE ||
        vowel_Q((char)vowel) == FALSE || tone_Q((char)tone) == FALSE ||
        consonant_Q((char)consonant_three) == FALSE) {
      code_exit;
    }
    guarantee(consonant_Q((char)consonant_two) == TRUE);
    guarantee(vowel_Q((char)vowel) == TRUE);
    guarantee(tone_Q((char)tone) == TRUE);
    guarantee(consonant_Q((char)consonant_three) == TRUE);
  }
  if (consonant_one != 0 && type != SHORT_ROOT) {
    code_ACC_consonant_one(type, consonant_one, &number);
    // printf("c1 %X\n", (uint) number);
  }
  if (consonant_two != 0) {
    code_ACC_consonant_two(type, consonant_two, &number);
    // printf("c2 %X\n", (uint) number);
  }
  code_ACC_vowel(type, vowel, &number);
  // printf("v %X\n", (uint) number);
  if (tone != 0) {
    code_ACC_tone(type, tone, &number);
    // printf("tone %X\n", (uint) number);
  } else {
    code_ACC_tone(type, 'M', &number);
  }
  if (consonant_three != 0 && type != LONG_GRAMMAR) {
    code_ACC_consonant_three(type, consonant_three, tone, &number);
    // printf("c3 %X\n", (uint) number);
  }
  *DAT_number = number;
}
word_t neo_word_code_encode(struct Text word) {
  word_t number = 0;
  word_code_encode(word.length, word.letters, &number);
  return number;
}

extern inline void code_ACC_vowel(const uint8_t type, const uint8_t vowel,
                                  uint16_t *number) {
  uint8_t i, vowel_number = VOWEL_ENCODE_LONG;
  // uint16_t start_number = *number;
  // printf("n %04X, t %X v %c \n", (uint) *number,
  //       (uint) type, (char) vowel);
  guarantee(vowel_Q((char)vowel) == TRUE);
  if (vowel != 0) {
    for (i = 0; i < VOWEL_ENCODE_LONG; i++) {
      if (vowel_code_group[i][0] == vowel) {
        vowel_number = vowel_code_group[i][1];
        guarantee(vowel_number < VOWEL_ENCODE_LONG);
        if (type == LONG_ROOT) {
          // printf("VLR %04X\n", (uint) vowel_number << (
          //       CONSONANT_ONE_THICK + CONSONANT_TWO_THICK));
          *number |= (uint16_t)(vowel_number
                                << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK));
          break;
        } else if (type == SHORT_ROOT) {
          *number |=
              (uint16_t)(vowel_number << (BANNER_THICK + CONSONANT_ONE_THICK));
          break;
        } else if (type == LONG_GRAMMAR) {
          // printf("VLG %04X\n", (uint) vowel_number << (BANNER_THICK +
          //       CONSONANT_ONE_THICK + CONSONANT_TWO_THICK));
          *number |=
              (uint16_t)(vowel_number << (BANNER_THICK + CONSONANT_ONE_THICK +
                                          CONSONANT_TWO_THICK));
          break;
        } else if (type == SHORT_GRAMMAR) {
          *number |= (uint16_t)(vowel_number << (CONSONANT_ONE_THICK * 2));
          break;
        }
      }
    }
  }
  // guarantee(vowel_number == 0 || *number != start_number);
  guarantee(vowel_number != VOWEL_ENCODE_LONG);
}

uint16_t set_tone_by_type(uint16_t type, uint16_t tone_number) {
  uint16_t number;
  switch (type) {
  case LONG_ROOT:
    // printf("TLR %X\n", (uint)(tone_number <<
    //       (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK + VOWEL_THICK)));
    number = (uint16_t)(tone_number << (CONSONANT_ONE_THICK +
                                        CONSONANT_TWO_THICK + VOWEL_THICK));
    break;
  case SHORT_ROOT:
    number = (uint16_t)(tone_number
                        << (BANNER_THICK + CONSONANT_ONE_THICK + VOWEL_THICK));
    break;
  case LONG_GRAMMAR:
    number = (uint16_t)(tone_number << (BANNER_THICK + CONSONANT_ONE_THICK +
                                        CONSONANT_TWO_THICK + VOWEL_THICK));
    break;
  case SHORT_GRAMMAR:
    number = (uint16_t)(tone_number << (CONSONANT_ONE_THICK * 2 + VOWEL_THICK));
    break;
  }
  //DEBUGPRINT(("%X type, %X tone_number, %X number\n", type, tone_number, number));
  return number;
}
extern inline void code_ACC_tone(const uint8_t type, const uint8_t tone,
                                 uint16_t *number) {
  uint8_t i, tone_number = TONE_ENCODE_LONG;
  uint16_t start_number = *number;
  // printf("n %04X, t %X tn %c \n", (uint) *number,
  //       (uint) type, (char) tone);
  guarantee(tone_Q((char)tone) == TRUE);
  //DEBUGPRINT(("%c tone\n", tone));
  if (tone != 0) {
    for (i = 0; i < TONE_ENCODE_LONG; i++) {
      if (tone_code_group[i][0] == tone) {
        tone_number = tone_code_group[i][1];
        *number |= (uint16_t)set_tone_by_type(type, tone_number);
      }
    }
  }
  guarantee(tone_number == 0 || *number != start_number);
  guarantee(tone_number != TONE_ENCODE_LONG);
}

extern inline word_t
code_ACC_consonant_three_root(const uint8_t consonant_number,
                              const uint8_t type, const uint8_t tone,
                              uint16_t *number) {
  if (type == LONG_ROOT && tone == 0) {
    // printf("C3LR %04X \n", (uint) (consonant_number
    //               << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK +
    //                  VOWEL_THICK + TONE_THICK)));
    *number |= (uint16_t)(consonant_number
                          << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK +
                              VOWEL_THICK + TONE_THICK));
    return cessative_aspect_WORD;
  } else if (type == SHORT_ROOT && tone == 0) {
    // printf("SR %04X \n", (uint) (consonant_number
    //               << (BANNER_THICK + CONSONANT_ONE_THICK +
    //               VOWEL_THICK)));
    *number |=
        (uint16_t)(consonant_number << (BANNER_THICK + CONSONANT_ONE_THICK +
                                        VOWEL_THICK + TONE_THICK));
    return cessative_aspect_WORD;
  } else if (type == LONG_ROOT && tone != 0) {
    *number |= (uint16_t)(consonant_number
                          << (CONSONANT_ONE_THICK + CONSONANT_TWO_THICK +
                              VOWEL_THICK + TONE_THICK));
    return cessative_aspect_WORD;
  } else if (type == SHORT_ROOT && tone != 0) {
    *number |=
        (uint16_t)(consonant_number << (BANNER_THICK + CONSONANT_ONE_THICK +
                                        VOWEL_THICK + TONE_THICK));
    return completeness_WORD;
  }
  return continutive_aspect_WORD;
}

extern inline void code_ACC_consonant_three(const uint8_t type,
                                            const uint8_t consonant_three,
                                            const uint8_t tone,
                                            uint16_t *number) {
  uint8_t i, consonant_number = CONSONANT_THREE_ENCODE_LONG;
  uint16_t start_number = *number;
  // printf("n %04X, t %X c %c  tn %c\n", (uint) *number,
  //       (uint) type, (char) consonant_three, (char) tone);
  if (consonant_three != 0 && type != SHORT_GRAMMAR && type != LONG_GRAMMAR) {
    for (i = 0; i < CONSONANT_THREE_ENCODE_LONG; i++) {
      if (consonant_three_code_group[i][0] == consonant_three) {
        consonant_number = consonant_three_code_group[i][1];
        word_t aspect =
            code_ACC_consonant_three_root(consonant_number, type, tone, number);
        if (aspect == cessative_aspect_WORD) {
          break;
        }
      }
    }
    // DEBUGPRINT(("0x%X consonant_number\n", consonant_number));
    guarantee(consonant_number == 0 || *number != start_number);
    guarantee(consonant_number != CONSONANT_THREE_ENCODE_LONG);
  }
}

ulong number_indexFinger_found(const ulong sequence_long,
                               constant uint16_t sequence[],
                               const uint16_t example) {
  repeat(sequence_long, if (sequence[iterator] == example) return iterator);
  //     DEBUGPRINT(("0x%X sequence[iterator], 0x%X example\n",
  //     sequence[iterator], example)));
  // DEBUGPRINT(("0x%lX sequence_long", sequence_long));
  return sequence_long;
}

void number_word_to_number(const uint16_t number_word, uint8_t *number) {
  // returns greater than MAXIMUM_WORD_NUMBER if invalid,
  // so be sure to guarantee(number <= MAXIMUM_WORD_NUMBER); afterwards;
  uint16_t temporary_number = number_indexFinger_found(
      NUMBER_WORD_SEQUENCE_LONG, number_word_sequence, number_word);
  // DEBUGPRINT(("0x%X number_word, 0x%X number\n", number_word,
  // temporary_number));
  *number = temporary_number >= NUMBER_WORD_SEQUENCE_LONG ? MAXIMUM_WORD_NUMBER
                                                          : temporary_number;
  // DEBUGPRINT(("0x%X *number\n", *number));
}

void retrospective_phrase_indexFinger_found(
    const uint16_t tablet_indexFinger, const uint8_t sequence_long,
    const line_t *tablet, uint16_t *retrospective_phrase_indexFinger) {
  guarantee(LINE_LONG * sequence_long > tablet_indexFinger);
  guarantee(tablet != NULL);
  uint16_t binary_phrase_list = 0;
  uint8_t indicator = 0;
  uint8_t sequence_indexFinger = (uint8_t)tablet_indexFinger / LINE_LONG;
  uint8_t champion = FALSE;
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  uint8_t vector_indexFinger = 0;
  for (; sequence_indexFinger < sequence_long; --sequence_indexFinger) {
// line_t_print(tablet[sequence_indexFinger]);
#ifdef ARRAYLINE
    binary_phrase_list = (uint16_t)tablet[sequence_indexFinger][0];
#else
    binary_phrase_list = (uint16_t)tablet[sequence_indexFinger].s0;
#endif
    // printf("\t binary_phrase_list 0x%04X\n\t ", binary_phrase_list);
    indicator = binary_phrase_list & 1;
    vector_indexFinger =
        (tablet_indexFinger - sequence_indexFinger * LINE_LONG) % LINE_LONG;
    // printf("vector_indexFinger 0x%X, ", vector_indexFinger);
    for (; vector_indexFinger < LINE_LONG; --vector_indexFinger) {
      // printf("indicator 0x%X, ", indicator);
      // printf("indexFinger 0x%X, ", vector_indexFinger);
      // printf("tidbit 0x%X,\n\t ",
      //       (binary_phrase_list >> vector_indexFinger) & 1);

      if (((binary_phrase_list >> vector_indexFinger) & 1) == indicator) {
        *retrospective_phrase_indexFinger =
            vector_indexFinger + sequence_indexFinger * LINE_LONG;
        // printf("champion 0x%X, ", vector_indexFinger);
        champion = TRUE;
        break;
      }
    }
    if (champion == TRUE) {
      break;
    }
    *retrospective_phrase_indexFinger = 0;
  }
  // printf("\n");
}
extern void convert_last_number_to_quote(uint16_t *terminator_indexFinger,
                                         const uint8_t sequence_long,
                                         line_t *tablet) {
  uint16_t tablet_indexFinger = 0;
  uint8_t number_indexFinger = 0;
  uint64_t number = 0;
  uint8_t finish = FALSE;
  // uint16_t indicator_list = (uint16_t)tablet[0].s0;
  // uint8_t indicator = indicator_list & 1;
  uint16_t retrospective_phrase_indexFinger = 0;
  uint16_t quote_indexFinger = 0;
  uint16_t quote_word = 0;
  uint8_t letter_number = 0;
  guarantee(*terminator_indexFinger > 0);
  guarantee(tablet != NULL);
  tablet_indexFinger = *terminator_indexFinger;
  retrospective_phrase_indexFinger_found(tablet_indexFinger, sequence_long,
                                         tablet,
                                         &retrospective_phrase_indexFinger);
  uint16_t number_word = 0;
  // see how long the number is
  for (; tablet_indexFinger > 0; --tablet_indexFinger) {
    number_word = tablet_retrospective_word_read(
        tablet_indexFinger, sequence_long, tablet, &tablet_indexFinger);
    number_word_to_number(number_word, &letter_number);
    if (letter_number < NUMBER_WORD_SEQUENCE_LONG) { // if found
      number += letter_number *
                number_exponent(DEFAULT_NUMBER_BASE, number_indexFinger);
      ++number_indexFinger;
    } else {
      finish = TRUE;
    }
    if (finish == TRUE) {
      guarantee(retrospective_phrase_indexFinger ==
                *terminator_indexFinger - number_indexFinger);
      break;
    }
  }

  // DEBUGPRINT(("0x%lX number\n", number));
  /* set up quote and number if necessary (number_indexFinger > 2)*/
  // printf("number %X, tablet_indexFinger %X, number_indexFinger %X\n",
  // (uint)number,
  //       (uint)tablet_indexFinger, (uint)number_indexFinger);
  // printf("number %lX\n", number);
  if (number <= 0xFFFF) {
    quote_word = QUOTE_DENOTE |
                 SIXTEEN_TIDBIT_SCALAR_THICK << SCALAR_THICK_BEGIN |
                 UINT_SORT_DENOTE << SORT_DENOTE_BEGIN;
    // printf("%s:%d ", __FILE__, __LINE__);
    // line_t_print(tablet[0]);
    // printf("\tRPI %X\n", retrospective_phrase_indexFinger);
    quote_indexFinger = (uint16_t)retrospective_phrase_indexFinger + 1;
    tablet_word_write(quote_indexFinger, (uint16_t)(quote_word), sequence_long,
                      tablet, &quote_indexFinger);
    ++quote_indexFinger;
    tablet_word_write(quote_indexFinger, (uint16_t)(number), sequence_long,
                      tablet, &quote_indexFinger);
    *terminator_indexFinger = (uint8_t)(quote_indexFinger);
    // printf("RPI %X\n", retrospective_phrase_indexFinger);
  }
  if (number_indexFinger > 4) {
    DEBUGPRINT(("0x%X number_indexFinger\n", number_indexFinger));
    guarantee(number_indexFinger <= 4);
  }
}

uint16_t /* tablet_long*/ old_text_encoding(const uint16_t max_text_magnitude,
                                            const char *text_array,
                                            uint16_t tablet_sequence_long,
                                            line_t *tablet,
                                            uint16_t *text_remainder) {
  struct Text text;
  text.length = max_text_magnitude;
  text.letters = text_array;
  struct Phrase phrase;
  phrase.begin = 1;
  phrase.length = 0;
  phrase.page.lines = tablet;
  phrase.page.plength = tablet_sequence_long;
  text_encoding(text, phrase, text_remainder);
  uint16_t tablet_long = phrase.length > 0 ? phrase.length / LINE_LONG + 1 : 0;
  return (phrase.length / LINE_LONG + 1);
}

void word_sequence_encode(const uint16_t text_long, const char *text,
                          uint16_t *word_sequence_long,
                          uint16_t *word_sequence) {
  guarantee(word_sequence != NULL);
  guarantee(word_sequence_long != NULL);
  guarantee(*word_sequence_long > 0);

  // till end of text
  uint16_t text_indexFinger = 0;
  uint16_t word_sequence_indexFinger = 0;
  uint8_t word_long = 0;
  uint16_t word_begin = 0;
  uint16_t word_number = 0;
  for (word_sequence_indexFinger = 0;
       word_sequence_indexFinger < *word_sequence_long &&
       text_long - text_indexFinger > 1;
       ++word_sequence_indexFinger) {
    printf("%s:%d:\ttext_indexFinger 0x%X text_long 0x%X text %s\n", __FILE__,
           __LINE__, text_indexFinger, text_long - text_indexFinger,
           text + text_indexFinger);
    first_word_derive((uint8_t)(text_long - text_indexFinger),
                      text + text_indexFinger, &word_long, &word_begin);
    if (word_long == 0) {
      break;
    }
    text_indexFinger += word_begin;
    word_code_encode(word_long, text + text_indexFinger, &word_number);
    word_sequence[word_sequence_indexFinger] = word_number;
    text_indexFinger += word_long;
  }
  // get words
  // add to array
  // return  length
  *word_sequence_long = word_sequence_indexFinger;
}

struct Phrase /* produce */ number_grammar_encode(struct Phrase input) {
  /* if preceded by a quote word, then do as a default.
     if preceded by numbers then convert them to a quote, */
  line_t *tablet = input.page.lines;
  uint16_t tablet_sequence_long = input.page.plength;
  txik_t tablet_indexFinger = input.begin + input.length;
  txik_t retrospective_phrase_indexFinger;
  uint8_t phrase_long;
  word_t word_code, quote_word;
  word_code =
      tablet_upcoming_word_read(tablet_indexFinger - 1, tablet_sequence_long,
                                tablet, &tablet_indexFinger);
  // DEBUGPRINT(("0x%X word_code in number grammar\n", word_code));
  if ((word_code & QUOTED_DENOTE) == QUOTED_DENOTE) {
    // tablet[0][tablet_indexFinger] = number;
    //++tablet_indexFinger;
  } else if (number_word_INT(word_code) == truth_WORD) {
    /* if last of tablet is a valid number word */
    // convert last of tablet to number quote
    // printf("pre tablet_indexFinger %X\n",
    // (uint)tablet_indexFinger);
    // printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
    // tablet_print(*tablet_sequence_long, tablet);
    // DEBUGPRINT(("%s", "number word detected\n"));
    convert_last_number_to_quote(&tablet_indexFinger, tablet_sequence_long,
                                 tablet);
    // printf("post tablet_indexFinger %X\n",
    // (uint)tablet_indexFinger);
    // tablet[0][tablet_indexFinger] = number;
    ++tablet_indexFinger;
  } else { // else default

    quote_word = QUOTE_DENOTE |
                 SIXTEEN_TIDBIT_SCALAR_THICK << SCALAR_THICK_BEGIN |
                 UINT_SORT_DENOTE << SORT_DENOTE_BEGIN;
    // printf("%s:%d:%s \n", __FILE__, __LINE__, __FUNCTION__);
    // tablet_print(tablet_sequence_long, tablet);
    // get previous phrase marker,
    tablet_retrospective_grammar_read(tablet_indexFinger, tablet_sequence_long,
                                      tablet,
                                      &retrospective_phrase_indexFinger);
    DEBUGPRINT(("0x%X retrospective_phrase_indexFinger,"
                " 0x%X tablet_indexFinger\n",
                retrospective_phrase_indexFinger, tablet_indexFinger));
    // copy all the words forward by one
    phrase_long = tablet_indexFinger - (retrospective_phrase_indexFinger);
    if (phrase_long > 0)
      tablet_upcoming_copy(retrospective_phrase_indexFinger + 1,
                           tablet_indexFinger + 1, phrase_long,
                           tablet_sequence_long, tablet);
    // insert quote  word
    tablet_word_write(retrospective_phrase_indexFinger + 1, quote_word,
                      tablet_sequence_long, tablet, &tablet_indexFinger);
    // printf("%s:%d:%s 0x%X phrase_long, 0x%X tablet_indexFinger\n",
    // __FILE__, __LINE__, __FUNCTION__,
    //    phrase_long, tablet_indexFinger);
    // tablet_print(tablet_sequence_long, tablet);
    tablet_indexFinger += phrase_long;
    // copy name
    // tablet_print(tablet_sequence_long, tablet);
    quote_word = 0;
    ++tablet_indexFinger;
    if (phrase_long == 0)
      ++tablet_indexFinger;
    // ++tablet_indexFinger;
  }
  input.length = tablet_indexFinger;
  return input;
}
// struct Phrase(
//      //derive_first_word(word_magnitude, word, &derived_word_magnitude,
//      //                  derived_word);
//
//
// encode_theTextZFirstIndependentClause_toThePhrase
struct Phrase /* htikya */
fyakyi_hwusti_hpamhtinka_kfontu(struct Phrase htikya, struct Text hkakhnikya) {
  /* new algorithm:
   * get phrase
   * if phrase ends with independent clause end then done
   */
  /* algorithm:
      loop through glyphs,
      derive words
      if word is quote then add it to tablet,
          and add proper quote identifier.
      if word is accusative, instrumental, or dative,
          flip the index representation for it.
      if word is deontic-mood then set as ending tablet.
      otherwise add word as name
  */
  // struct Text theText = hkakhnikya;
  // struct Text firstWord = read_theTextZFirstWord(theText);
  return htikya;
}

struct Paragraph process_derived_word(struct Phrase tablet, word_t number) {
  struct Phrase input;
  // DEBUGPRINT(("%X number\n", number));
  if (number != 0) {
    // input = default_phrase_encode(tablet);
    input = tablet;
    input.begin = 1;
    input.length = tablet.length;
    // DEBUGPRINT(("%X number, %X input.length\n", number, input.length));
    input = neo_phrase_encode(number, input);
    // text_phrase_print(input);
    tablet.length = input.length;
  }
  return tablet;
}

struct Text find_letter_quote(const struct Text text) {
  // what to do here?
  // what assumptions do we have?
  // quotes start with a zi, so look for that;
  conditional(text.length == 0, return text);
  NewText(zi, "zi");
  struct Text first_zi = letter_found(text, zi);
  // that the dot is next, may need the first word also?
  // guess it would be easier for a general algorithm
  // okay so grab until next dot, that will be the quote
  NewText(dot, ".");
  // find first dot
  struct Text first_dot = letter_found(text, dot);
  // if not found then return text length 0
  struct Text produce = text;
  conditional(first_dot.length == 0, produce.length = 0; return produce);
  // DEBUGPRINT(("%s first_dot\n", first_dot.letters));
  // adjust search text to be remainder of text
  struct Text search_text = first_dot;
  search_text.letters += dot.length;
  search_text.length = text.length + text.letters - search_text.letters;
  struct Text second_dot = letter_found(search_text, dot);
  struct Text quote_indicator = first_dot;
  quote_indicator.length =
      (second_dot.letters + second_dot.length) - first_dot.letters;
  // DEBUGPRINT(("%s second_dot, %X quote_indicator.length\n",
  // second_dot.letters,
  //             quote_indicator.length));
  // find next quote_indicator
  search_text.letters = quote_indicator.letters + quote_indicator.length;
  search_text.length = text.length + text.letters - search_text.letters;
  struct Text next_quote_indicator = letter_found(search_text, quote_indicator);
  if (next_quote_indicator.length == 0) {
    produce.length = 0;
    return produce;
  }
  struct Text zi_search = search_text;
  zi_search.letters =
      next_quote_indicator.letters + next_quote_indicator.length;
  zi_search.length -=
      search_text.letters + search_text.length - zi_search.letters;
  struct Text final_zi = letter_found(zi_search, zi);
  // text_print(next_quote_indicator);
  // DEBUGPRINT(("%lX next_quote_indicator.letters\n",
  // next_quote_indicator.letters
  //      - text.letters));
  // make sure is followed by zi #TODO
  // update text_indexFinger
  // find the difference
  struct Text quoted_text = first_zi;
  quoted_text.length = final_zi.letters + final_zi.length - quoted_text.letters;
  return quoted_text;
}
struct Text find_letter_quote_contents(const struct Text text) {
  conditional(text.length == 0, return text);
  // what to do here?
  // what assumptions do we have?
  // quotes start with a zi, so look for that;
  // that the dot is next, may need the first word also?
  // guess it would be easier for a general algorithm
  // okay so grab until next dot, that will be the quote
  NewText(dot, ".");
  // find first dot
  struct Text first_dot = letter_found(text, dot);
  // if not found then return text length 0
  struct Text produce = text;
  conditional(first_dot.length == 0, produce.length = 0; return produce);
  // DEBUGPRINT(("%s first_dot\n", first_dot.letters));
  // adjust search text to be remainder of text
  struct Text search_text = first_dot;
  search_text.letters += dot.length;
  search_text.length = text.length + text.letters - search_text.letters;
  struct Text second_dot = letter_found(search_text, dot);
  struct Text quote_indicator = first_dot;
  quote_indicator.length =
      (second_dot.letters + second_dot.length) - first_dot.letters;
  // DEBUGPRINT(("%s second_dot, %X quote_indicator.length\n",
  // second_dot.letters,
  //             quote_indicator.length));
  // find next quote_indicator
  search_text.letters = quote_indicator.letters + quote_indicator.length;
  search_text.length = text.length + text.letters - search_text.letters;
  struct Text next_quote_indicator = letter_found(search_text, quote_indicator);
  if (next_quote_indicator.length == 0) {
    produce.length = 0;
    return produce;
  }
  // text_print(next_quote_indicator);
  // DEBUGPRINT(("%lX next_quote_indicator.letters\n",
  // next_quote_indicator.letters
  //      - text.letters));
  // make sure is followed by zi #TODO
  // update text_indexFinger
  // find the difference
  struct Text quoted_text = search_text;
  quoted_text.length = next_quote_indicator.letters - quoted_text.letters;
  return quoted_text;
}

struct Phrase process_letter_quote(struct Phrase tablet, const struct Text text,
                                   uint8_t *text_indexFinger) {
  struct Text letter_quote = find_letter_quote(text);
  struct Text quoted_text = find_letter_quote_contents(text);
  uint8_t full_quote_length =
      letter_quote.length + letter_quote.letters - text.letters;
  //DEBUGPRINT(("%X full_quote_length, %X letter_quote.length\n",
  //            full_quote_length, letter_quote.length));
  *text_indexFinger += full_quote_length;
  uint8_t lineVacancy = 0;
  switch (quoted_text.length) {
    example(0, tablet = addenda_theWord_toThePhrase(LETTER_QUOTE, tablet);
            tablet = addenda_theWord_toThePhrase(0, tablet));
    example(1,
            // make sure there are at least 2 word_width available,
            // is_there_room_available(amount of room)
            // how much line vacancy remains at the phrase termination
            //   fyakhzotlwoh kehlaskfiskfamka twicri
            // otherwise go to next tablet line
            // otherwise return error or something( guarantee)
            lineVacancy =
                howMuch_lineVacancy_remains_atThePhraseTermination(tablet);
            conditional(lineVacancy < 2,
                        tablet = grow_thePhrase_untilTheUpcomingLine(tablet));
            tablet = addenda_theWord_toThePhrase(LETTER_QUOTE, tablet);
            tablet =
                addenda_theWord_toThePhrase(quoted_text.letters[0], tablet));
  default:
    DEBUGPRINT(("%X quote length currently not supported, check blockquote\n",
                quoted_text.length));
    guarantee(1 == 0);
  }
  return tablet;
}

struct Phrase process_quote(struct Phrase tablet, struct Text text,
                            uint8_t *text_indexFinger) {
  conditional(text.length == 0, return tablet);
  struct Text quote_text = text;
  // first letter must be a dot so skip
  struct Text search_text = quote_text;
  search_text.letters++;
  search_text.length--;
  NewTextPad(quote_word, 5);
  quote_word = derive_first_word(search_text, quote_word);
  conditional(quote_word.length == 0, return tablet);
  // text_indexFinger += quote_word.length;
  // DEBUGPRINT(("%s quote_word, %s quote_text\n", quote_word.letters,
  //             quote_text.letters));
  word_t number = neo_word_code_encode(quote_word);
  switch (number) {
    example(letter_GRAMMAR,
            tablet = process_letter_quote(tablet, text, text_indexFinger));
  default:
    return tablet;
  }

  return tablet;
}

struct Phrase encode_paragraph_quote(struct Phrase phrase, struct Text text,
                                     uint8_t *text_indexFinger) {
  text_print(text);
  // assumptions:
  DEBUGPRINT(("encoding paragraph quote\n"));
  struct Sentence declaration = retrospective_htin_found(phrase);
  word_t perspective = phrase_word_read(phrase, declaration.length - 1);
  word_t quoted = phrase_word_read(phrase, declaration.length - 2);
  guarantee(perspective = declarative_mood_GRAMMAR);
  guarantee(quoted = quoted_WORD);
  // then find the type and length,
  // sort_phrase is the verb
  struct Phrase verb_phrase = retrospective_verb_phrase_found(phrase);
  text_phrase_print(verb_phrase);
  // struct Phrase sort_phrase =
  // guarantee(sort_phrase.length == 2); // other size not supported
  word_t sort_word = phrase_word_read(verb_phrase, verb_phrase.length - 3);
  DEBUGPRINT(("%X sort_word\n", sort_word));
  uint8_t scalar_length = 0;
  switch (sort_word) {
    example(letter_WORD, scalar_length = 2);
  default:
    DEBUGPRINT(("%X sort_word unsupported\n", sort_word));
    guarantee(1 == 0);
  }
  // find the number_GRAMMAR, replace with PARAGRAPH_LETTER_QUOTE
  struct Phrase quote_code =
      upcoming_example_word_in_phrase_found(NUMBER_QUOTE, verb_phrase);
  guarantee(phrase_word_read(quote_code, 0) == NUMBER_QUOTE);
  Page_print(verb_phrase.page);

  // #TODO
  // later on we can support it without an explicit definition of length
  // by getting the length of the quote that it is following,
  // but that would take extra work so lets not do it now.
  quote_code.length += 1;
  uint16_t vector_length = phrase_word_read(quote_code, 1);
  uint32_t byte_length = (vector_length * scalar_length);
  uint32_t length = byte_length / 2;
  //     // and multiply them to get the full length
  //     // add that to the length of the tablet.
  DEBUGPRINT(("%X quote_length\n", quote_code.length));
  text_phrase_print(quote_code);
  write_theWord_toThePhrase_atThePlace(PARAGRAPH_LETTER_QUOTE, quote_code, 0);
  write_theWord_toThePhrase_atThePlace(vector_length, quote_code, 1);
  DEBUGPRINT(("%X phrase.length\n", phrase.length));
  repeat(3, phrase = delete_aGrammarWord_atThePlace_inThePhrase(
                phrase.length - 1, phrase));
  // delete the trailing verb words we no longer needed
  // addenda declarative_mood_GRAMMAR
  addenda_theGrammarWord_toThePhrase(declarative_mood_GRAMMAR, phrase);
  // DEBUGPRINT(("%X length, phrase.length %X \n", length, phrase.length));
  phrase = round_thePhrase_toRoofLine(phrase);
  text_print(text);
  Page_print(phrase.page);
  // #TODO
  // add the quote contents
  // maybe with some version of process_letter_quote
  // need a "get quote length" kinda thing to know how much text it involves.
  struct Text full_quote = find_letter_quote(text);
  text_print(text);
  DEBUGPRINT(("%X  full_quote.length\n", full_quote.length));
  if (full_quote.length > 0) {
    *text_indexFinger =
        full_quote.letters + full_quote.length - full_quote.letters;
    struct Text quote_contents = find_letter_quote_contents(full_quote);
    text_print(quote_contents);
    DEBUGPRINT(
        ("%X phrase.length, %X phrase.begin\n", phrase.length, phrase.begin));
    phrase.begin -= phrase.begin % LINE_LONG_MASK;
    guarantee(phrase.begin % LINE_LONG_MASK == 0);
    phrase = addenda_theText_toThePhrase(quote_contents, phrase);
    txik_t paragraph_grammar_txik = length+1;
    DEBUGPRINT(("%X length\n", length));
    phrase = write_theGrammarWord_toThePhrase_atThePlace(paragraph_GRAMMAR, 
        phrase, paragraph_grammar_txik);
    phrase_print(phrase);
    Page_print(phrase.page);
    phrase_print(phrase);
  }
  //
  //
  // DEBUGPRINT(("%X length, phrase.length %X \n", length, phrase.length));
  phrase.length += length;
  // DEBUGPRINT(("%X length, phrase.length %X \n", length, phrase.length));
  phrase = round_thePhrase_toRoofLine(phrase);
  phrase.length -= 1;
  // DEBUGPRINT(("%X length, phrase.length %X \n", length, phrase.length));
  return phrase;
}

struct Text word_encoding(struct Text derived_word, struct Text quote_text,
                          uint16_t *number, struct Text text,
                          uint8_t *text_indexFinger, struct Paragraph tablet,
                          uint8_t *repeat_stop_eh) {
  NewWord(word);
  string_zero(WORD_LONG, derived_word.letters);
  derived_word.length = WORD_LONG;
  derived_word = derive_first_word(word, derived_word);
  // derived a blest word
  if (derived_word.length > 0) {
    // found a healthy word
    string_zero(WORD_LONG, word.letters);
    word.length = 0;
    *number = neo_word_code_encode(derived_word);
    if (*number == quoted_GRAMMAR) {
      // found a quote
      quote_text = text;
      quote_text.letters += *text_indexFinger + 1;
      quote_text.length -= *text_indexFinger - 1;
      tablet = process_quote(tablet, quote_text, text_indexFinger);
      //   //process quote
      //   // find quote type
      //   // find end of quote
      //   // encode quote
      //   // adjust indexFinger accordingly
    } else {
      // is a simple  word, thus a simple phrase
      tablet = process_derived_word(tablet, *number);
      *number = phrase_word_read(tablet, tablet.length - 1);
      if (is_perspective_word(*number) == truth_WORD &&
          *number != conditional_mood_GRAMMAR)
        *repeat_stop_eh = TRUE;
    }
  }
  return derived_word;
}

/** independentClause_encoding does the bulk of the encoding process,
 * passing various subroutines to other functions. */
struct Paragraph /* produce */
neo_independentClause_encoding(const struct Text text, struct Paragraph tablet,
                               uint16_t *text_remainder) {
  /* algorithm:
      loop through glyphs,
      derive words
      if word is quote then add it to tablet,
          and add proper quote identifier.
      if word is accusative, instrumental, or dative,
          flip the index representation for it.
      if word is deontic-mood then set as ending tablet.
      otherwise add word as name
  */
  char glyph;
  uint8_t text_indexFinger = 0;
  tablet.length = 0;
  tablet.begin = 1;
  NewTextPad(word, WORD_LONG);
  NewTextPad(derived_word, WORD_LONG);
  uint16_t number = 0;
  // uint16_t binary_phrase_list = (uint16_t)1;
  // uint8_t quote_tablet_sequence_long = 0;
  string_zero(WORD_LONG, word.letters);
  string_zero(WORD_LONG, derived_word.letters);
  guarantee(text.letters != NULL);
  guarantee(text.length > 0);
  guarantee(tablet.page.lines != NULL);
  // guarantee(tablet_sequence_long != NULL);
  guarantee(text_remainder != NULL);
  struct Text quote_text = text;
  word.length = 0;
  for (text_indexFinger = 0; text_indexFinger < text.length;
       ++text_indexFinger) {
    glyph = text.letters[text_indexFinger];
    if (glyph == 0) {
      // text magnitude not detected properly
      // DEBUGPRINT(("%d text_magnitude not detected properly\n",
      // text_magnitude));
      // tablet_long = (uint8_t)(tablet.length) / LINE_LONG + 1;
      *text_remainder = (uint16_t)(text.length - text_indexFinger);
      return tablet;
      // guarantee(1 == 0);
    }
    if (is_letter(glyph) == truth_WORD) {
      word.letters[word.length] = glyph;
      word.length += 1;
    }
    if (word.length >= 2) {
      // then we have found a word actually, we are processing a regular length
      // word!
      // word_encoding
      string_zero(WORD_LONG, derived_word.letters);
      derived_word.length = WORD_LONG;
      derived_word = derive_first_word(word, derived_word);
      if (derived_word.length > 0) {
        // found a healthy word
        string_zero(WORD_LONG, word.letters);
        word.length = 0;
        /// hmmm  word can be defined
        number = neo_word_code_encode(derived_word);
        if (number == quoted_GRAMMAR) {
          // quote encoding
          quote_text = text;
          quote_text.letters += text_indexFinger + 1;
          quote_text.length -= text_indexFinger - 1;
          tablet = process_quote(tablet, quote_text, &text_indexFinger);
        } else {
          //
          // is a official word, thus presumably a official phrase
          tablet = process_derived_word(tablet, number);
          number = phrase_word_read(tablet, tablet.length - 1);
          if (is_perspective_word(number) == truth_WORD &&
              number != conditional_mood_GRAMMAR)
            break;
        }
      }
    }
  }
  // if number is declarative_mood, then check if is paragraph quote declaration
  if (number == declarative_mood_GRAMMAR) {
    // then check if second last word is quoted_WORD
    word_t word = phrase_word_read(tablet, tablet.length - 2);
    if (word == quoted_WORD) {
      // then find the type and length,
      // and multiply them to get the full length
      // add that to the length of the tablet.
      quote_text.letters += text_indexFinger + 1;
      quote_text.length -= text_indexFinger + 1;
      text_print(quote_text);
      text_print(text);
      DEBUGPRINT(("%X tablet.length\n", tablet.length));
      tablet = encode_paragraph_quote(tablet, quote_text, &text_indexFinger);
      DEBUGPRINT(("%X tablet.length\n", tablet.length));
    }
  }
  ++text_indexFinger;
  *text_remainder = (uint16_t)(text.length - text_indexFinger);
  if (*text_remainder > text.length)
    *text_remainder = 0;
  return tablet;
}

extern inline void establish_ACC_binary_phrase_list(
    const uint16_t *code_text, const uint8_t independentClause_magnitude,
    uint16_t *binary_phrase_list, uint16_t *tablet) {
  uint8_t current = 0;
  uint8_t i = 0;
  guarantee(code_text != NULL);
  guarantee(independentClause_magnitude != 0);
  guarantee(independentClause_magnitude <=
            PAGE_WORD_LONG * MAX_INDEPENDENTCLAUSE_PAGE + 1);
  guarantee(binary_phrase_list != NULL);
  if (*binary_phrase_list == 0) {
    current = (uint8_t)~current;
  }
  for (i = 0; i < independentClause_magnitude; i++) {
    if (current == 2)
      break;
    switch (current) {
    /*case 0:
        *binary_phrase_list |= 0 << (i + 1);
        break; */
    case 0xFF:
      *binary_phrase_list = (uint16_t)(*binary_phrase_list | (1 << (i + 1)));
      break;
    default:
      break;
    }
    tablet[i + 1] = code_text[i];
    switch (code_text[i]) {
    case accusative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case instrumental_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case dative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case allative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case ablative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case vocative_case_GRAMMAR:
      current = (uint8_t)(~current);
      break;
    case deontic_mood_GRAMMAR:
      current = 2;
      break;
    case epistemic_mood_GRAMMAR:
      current = 2;
      break;
    case realis_mood_GRAMMAR:
      current = 2;
      break;
    case declarative_mood_GRAMMAR:
      current = 2;
      break;
    case interrogative_mood_GRAMMAR:
      current = 2;
      break;
    case finally_GRAMMAR:
      current = 2;
      break;
    default:
      break;
    }
  }
  tablet[0] = *binary_phrase_list;
}

uint16_t /*text_remainder*/ neo_text_encoding(const struct Text text,
                                              struct Page tablet) {
  uint16_t text_remainder = 0;
  // uint16_t tablet_long = old_text_encoding(
  //    text.length, text.letters, tablet.plength, tablet.lines,
  //    &text_remainder);
  NewHollowPhrase(input);
  input.page = tablet;
  //input = text_encoding(text, input, &text_remainder);
  text_encoding(text, input, &text_remainder);
  if (text_remainder >= 2) {
    DEBUGPRINT(("WARNING: insufficient space in tablet, 0x%X text_remainder, \n"
                "`%s' text\n `%s' remainder\n",
                text_remainder, text.letters,
                text.letters + text.length - text_remainder));
    text_page_print(input.page);
    Page_print(input.page);
    //guarantee(1==0);
    // print_backtrace();
  }
  //guarantee(text_remainder < 2);
  return text_remainder;
}

struct Paragraph /* produce*/ text_encoding(const struct Text text,
                                            struct Paragraph tablet,
                                            uint16_t *text_remainder) {
  /*
   * while skipping quotes
   * find end of independentClause for each,
    then pass each independentClause to independentClause code,
    return the result */
  struct Phrase produce;
  struct Text input_text = text;
  struct Phrase input_tablet = tablet;
  uint16_t line_number = 0;
  *text_remainder = text.length;
  repeat(text.length,
         // set it up correctly
         // DEBUGPRINT(("%X text_remainder, %X text.length\n", *text_remainder,
         //             text.length));
         input_text = text_tail(text, *text_remainder);
         // DEBUGPRINT(("%X line_number, %X tablet.page.plength\n", line_number,
         //            tablet.page.plength));
         guarantee(line_number < tablet.page.plength);
         input_tablet.page.lines = tablet.page.lines + line_number;
         input_tablet.page.plength = tablet.page.plength - line_number;
         produce = neo_independentClause_encoding(input_text, input_tablet,
                                                  text_remainder);
         line_number += produce.length / LINE_LONG + 1;
         conditional(*text_remainder == 0 || line_number >= tablet.page.plength,
                     break););
  struct Phrase full_produce = tablet;
  full_produce.length = line_number * LINE_LONG;
  return full_produce;
}
/**
 * how much line vacancy remains at the phrase termination
 *howMuch_lineVacancy_remains_atThePhraseTermination
 */
uint8_t fyakhzotlwoh_kehlaskfiskfamka_twicri(struct Phrase phrase) {
  // find where we are in the phrase
  // and return the difference to the end of the line
  return LINE_LONG - ((phrase.begin + phrase.length) & LINE_LONG_MASK);
}
