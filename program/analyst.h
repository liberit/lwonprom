/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef ANALYST_H
#define ANALYST_H
#include "seed.h"
#include "sort.h"
void detect_ACC_quote_magnitude(const uint8_t text_magnitude,
                                              const char *text,
                                              uint8_t *quote_magnitude,
                                              uint8_t *quote_indexFinger);
void old_derive_first_word(const uint8_t ACC_GEN_magnitude,
                              const char *ACC_independentClause,
                              uint8_t *DAT_GEN_magnitude, char *DAT_word);
struct Text /*produce*/ derive_first_word(const struct Text input, 
    struct Text produce);
#endif
