/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
// file for composing or compiling sentences
#ifndef COMPILE_H
#define COMPILE_H
#include "encoding.h"
#include "tlep.h"
#include "pyash.h"
#include "sort.h"
#include "translation.h"

// htinka is independentClause _accusative_case
// literal_grammar_word_ins_htinka_addenda(nominaitve_case);
// literal_word_ins_htinka_addenda
// extract_words_ins_htinka_addenda

void extract_words_ins_htinka_addenda(
    const uint16_t input_long, const line_t input[], const uint16_t words_long,
    const uint16_t words_begin, const uint16_t produce_long, line_t produce[]);

#define neo_extract_words_ins_htinka_addenda(phrase, produce)                  \
  guarantee(phrase.page.lines != NULL);                                        \
  extract_words_ins_htinka_addenda(phrase.page.plength, phrase.page.lines,     \
                                   phrase.length, phrase.begin,                \
                                   produce.page.plength, produce.page.lines);  \
  produce.length = phrase_long_diagnose(produce);
#define phrase_addenda(phrase, produce) fyakyi_fyakka_dva2ttu(produce, phrase)

struct Phrase /* psas */ wohgafka_fyakyi_dva2ttu(const word_t wohgaf,
                                                 struct Phrase psas);
//#define /*toThePhrase*/ addenda_theGrammarWord_toThePhrase(theGrammarWord,     \
//                                                           toThePhrase)        \
//  wohgafka_fyakyi_dva2ttu(theGrammarWord, toThePhrase);

struct Phrase /* psas */ wosramka_fyakyi_dva2ttu(const word_t wosram,
                                                 struct Phrase psas);
#define /*produce phrase*/ addenda_theLiteralWord_toThePhrase(theLiteralWord,  \
                                                              toThePhrase)     \
  fyakyi_tlatka_dva2ttu(toThePhrase, theLiteralWord);

struct Phrase /* fyakyi */ fyakyi_kyitka_dva2ttu(struct Phrase htikya,
                                                 struct Phrase hkakya);
#define /*toPhrase*/ quote_addenda(hkakya, htikya) \
    fyakyi_kyitka_dva2ttu(htikya, hkakya)

struct Phrase /* fyakyi */ fyakyi_fyakka_dva2ttu(struct Phrase htikya,
                                                 struct Phrase hkakya);

#define /*toPhrase*/ addenda_thePhrase_toThePhrase(thePhrase, toThePhrase)     \
  fyakyi_fyakka_dva2ttu(toThePhrase, thePhrase);

void literal_grammar_word_ins_htinka_addenda(const uint16_t grammar_word,
                                             const uint16_t produce_long,
                                             line_t produce[]);

#define neo_literal_grammar_word_ins_htinka_addenda(grammar_word, produce)     \
  literal_grammar_word_ins_htinka_addenda(grammar_word, produce.page.plength,  \
                                          produce.page.lines); \
  produce.length = phrase_long_diagnose(produce);

void literal_word_ins_htinka_addenda(const uint16_t word, struct Page produce);

void literal_number_ins_htinka_addenda(const uint16_t number,
                                       struct Phrase produce);
void literal_letter_ins_htinka_addenda(const uint16_t number,
                                       struct Phrase produce);

void string_zero(const size_t string_long, char * string);
void tablet_zero(const uint16_t tablet_long, line_t tablet[]);
#define page_zero(_page) tablet_zero(_page.plength, _page.lines)
#define phrase_zero(_phrase) page_zero(_phrase.page); _phrase.begin = 1; \
  _phrase.length = 0;

/** clears the contents between begin and length of the phrase
 */
struct Phrase  hollow_thePhrase_byThePhrase(struct Phrase input, struct Phrase phrase);

uint16_t /*Sequence_finally*/ tablet_addenda(const uint16_t tablet_long, const line_t tablet[],
    uint16_t  knowledge_attribute[], line_t knowledge []);

#define grammar_txik_verify tlichtinhlasnweh_txikka_ri
#define is_theIndexFinger_inAFinalIndependentclauseLine \
  tlichtinhlasnweh_txikka_ri
uint16_t /*boolean*/ tlichtinhlasnweh_txikka_ri(const uint16_t txik,
                                         const uint16_t input_long,
                                         const line_t input[]);

word_t /* blu7n */ fyaknweh_txikka_hgaftxikri(const struct Phrase nwikya,
                                              const txik_t hkakya);

#define /*boolean word*/                                                       \
    is_theIndexfinger_inThePhrase_aGrammarIndexfinger(the, inThe)              \
  fyaknweh_txikka_hgaftxikri(inThe, the);

txik_t hvatci_hlaslweh_txikka_nwattu(txik_t htikya);

#define grow_theIndexFinger_untilTheUpcomingLine(theIndexFinger)               \
  hvatci_hlaslweh_txikka_nwattu(theIndexFinger)

struct Phrase hvatci_hlaslweh_fyakka_nwattu(struct Phrase phrase);
#define grow_thePhrase_untilTheUpcomingLine(theIndexFinger)               \
  hvatci_hlaslweh_fyakka_nwattu(theIndexFinger)




struct Phrase /*htikya*/ fyakyi_fyaktlatka_dva2ttu(struct Phrase hkakya,
                                                   const struct Phrase htikya);

#define addenda_thePhraseWord_toThePhrase(thePhraseWord, toThePhrase)          \
  fyakyi_fyaktlatka_dva2ttu(toThePhrase, thePhraseWord);


word_t /*boolean*/ txikti_hlasna_pfunhtinka_ri(txik_t txikya,
                                               struct Phrase hnakya);

#define is_theIndexfingerZLine_aPartialIndependentClause(indexfinger, line)    \
  txikti_hlasna_pfunhtinka_ri(indexfinger, line)

struct Phrase /*htikya*/ fyakyi_hgaftlatka_dva2ttu(struct Phrase htikya,
                                                   const word_t word);
#define addenda_theGrammarWord_toThePhrase(theGrammarWord, toThePhrase)        \
  fyakyi_hgaftlatka_dva2ttu(toThePhrase, theGrammarWord)

struct Phrase /*htikya*/ fyakyi_tlatka_dva2ttu(struct Phrase htikya,
                                               const word_t word);

#define addenda_theWord_toThePhrase(theWord, toThePhrase)               \
  fyakyi_tlatka_dva2ttu(toThePhrase, theWord)

#define word_addenda(theWord, toThePhrase) \
  fyakyi_tlatka_dva2ttu(toThePhrase, theWord)

word_t /* boolean */ hpifci_txikna_hlassyoslwoh_ri(txik_t hnakya);

#define is_thePageZIndexfinger_atTheLineSummary(thePageZIndexFinger)           \
  hpifci_txikna_hlassyoslwoh_ri(thePageZIndexFinger)

word_t /*boolean*/
fyakti_tlicci_htinna_tcunci_htinka_ri(struct Phrase hnakhnikya);
#define is_thePhraseZFinalIndependentClause_aDoneIndependentClause(thePhrase)  \
  fyakti_tlicci_htinna_tcunci_htinka_ri(thePhrase)

struct Phrase /*htikya*/ fyakyi_hcochtinka_hva2ttu(struct Phrase htikya);
#define addenda_aHollowIndependentClause_toThePhrase(toThePhrase)              \
  fyakyi_hcochtinka_hva2ttu(toThePhrase)

struct Phrase copy_thePhrase_toThePhrase(const struct Phrase input_phrase, 
    struct Phrase produce_phrase);


struct Paragraph tyafyi_tyafti_tlicci_htinka_hfuttu(struct Paragraph tyafya);
#define /*toTheParagraph*/ drop_theParagraphZFirstSentence_toTheParagraph(     \
    theParagraph)                                                              \
  tyafyi_tyafti_hpamci_htinka_hfuttu(theParagraph)

struct Paragraph tyafyi_tyafti_hpamci_htinka_hfuttu(struct Paragraph tyafya);

#define /*toTheParagraph*/ drop_theParagraphZFinalSentence_toTheParagraph(     \
    theParagraph)                                                              \
  tyafyi_tyafti_tlicci_htinka_hfuttu(theParagraph)


struct Text text_addenda(struct Text input, struct Text produce);
#define string_text_addenda(string, _produce) \
   do { \
    NewText(_string, string); \
    _produce = text_addenda(_string, _produce); \
   } while(0) 

#define addenda_theText_toThePhrase(text, phrase) \
  fyakyi_hwuska_dva2ttu(phrase, text)
struct Phrase fyakyi_hwuska_dva2ttu(struct Phrase, struct Text);

#define addenda_theLetter_toThePhrase(letter, phrase) \
  fyakyi_lyatka_dva2ttu(phrase, letter)
struct Phrase fyakyi_lyatka_dva2ttu(struct Phrase, uint16_t);

#endif
