/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "json.h"

/** Pyash to JSON conversion function
 * @param pyash text
 * @param produce json object
 * @returns JSON text
 */
json_t* /* produce */ pyash_phrase_to_json(struct Phrase input_htin, 
    json_t *root_object){
  if (input_htin.length == 0) {
  } else {
    text_phrase_print(input_htin);
    struct Phrase phrase = upcoming_phrase_found(input_htin);
    text_phrase_print(phrase);
    struct Phrase perspective = upcoming_perspective_found(phrase);
    struct Phrase content = phrase;
    content.length = phrase.length - perspective.length;
    NewTextPad(phrase_word, LINE_LONG * WORD_LONG);
    NewTextPad(phrase_content, LINE_LONG * WORD_LONG);
    phrase_word = neo_phrase_translate(perspective, phrase_word, 0);
    phrase_content = neo_phrase_translate(content, phrase_content, 0);
    DEBUGPRINT(("%s phrase_word, %s phrase_content\n", phrase_word.letters,
          phrase_content.letters));
    json_object_set_new(root_object, phrase_word.letters, 
        json_string(phrase_content.letters));
  }
  return root_object;
}

/** Pyash to JSON conversion function
 * @param pyash text
 * @param produce json object
 * @returns JSON text
 */
json_t* /* produce */ pyash_htin_to_json(struct Phrase input_htin, 
    json_t *root_object){
  if (input_htin.length == 0) {
  } else {
    struct Phrase perspective = upcoming_perspective_found(input_htin);
    NewTextPad(phrase_word, LINE_LONG * WORD_LONG);
    phrase_word = neo_phrase_translate(perspective, phrase_word, 0);
    json_object_set_new(root_object, "mood", json_string(phrase_word.letters));
  }
  return root_object;
}

/** Pyash to JSON conversion function
 * @param pyash text
 * @param produce json array
 * @returns JSON text
 */
struct Text /* produce */ pyash_to_json(struct Text input,
                                        struct Text produce) {
  json_t *root = json_array();
  if (input.length == 0) {
    json_array_append(root, json_object());
  } else {
    /* algorithm:
     * get the first independent clause
     * set the mood
     * set the verb
     * then go through each of the phrases
     *
     * note, probably will need to break it up into some subfunctions
     * like htin_to_json
     * and phrase_to_json
     * should also include some version information in htin or something
     * though could wait for later, can simply be undefined version for now.
     */
    NewTextPhrase(input_phrase, HTIN_LONG, input.letters);
    json_t *htin = json_object();
    htin = pyash_htin_to_json(input_phrase, htin);
    json_array_append(root, htin);
  }
  NewText(temp_produce, json_dumps(root, 0));
  produce = neo_text_copy(temp_produce, produce);
  DEBUGPRINT(("%s produce\n", produce.letters));
  json_decref(root);
  return produce;
}
