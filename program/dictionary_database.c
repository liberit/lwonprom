/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*SPEL machine programmer
Copyright (C) 2016  Logan Streondj

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact: streondj at gmail dot com
*/

//#include <CL/cl.h>
//#include <CL/cl_platform.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "agree.h"
#include "dialogue.h"
#include "encoding.h"
//#include "genericOpenCL.h"
#include "parser.h"
#include "seed.h"
#define VALGRIND
#define NEWLINE '\n'
#define HOLLOW_LETTER '\t'

int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("no input filename\n");
    return 0;
  } else if (argc < 3) {
    printf("no produce filename\n");
  }
  const char *produce_filename = argv[2];
  char dictionary_file[DICTIONARY_DATABASE_LONG] = {0};
  uint32_t dictionary_file_long = 0;
  char dictionary_database[DICTIONARY_DATABASE_LONG] = {0};
  uint32_t dictionary_long = 0;
  uint32_t line_begin = 0;
  uint32_t line_long = 0;
  uint32_t dictionary_file_indexFinger = 0;
  uint8_t word_long = 0;
  uint16_t word_begin = 0;
  uint16_t word_code = 0;
  uint32_t foreign_word_begin = 0;
  uint16_t foreign_word_long = 0;
  //text_file_read(code_filename, &gross_code_long, gross_code);
  for (; dictionary_file_indexFinger < dictionary_file_long;
       ++dictionary_file_indexFinger) {
    if (dictionary_file[dictionary_file_indexFinger] == '\n') {
      // printf("%s:%d dictionary_long 0x%X\n", __FILE__,
      // __LINE__,dictionary_long);
      line_long = dictionary_file_indexFinger - line_begin;
      // get the word to encode
      first_word_derive(WORD_LONG, dictionary_file + line_begin, &word_long,
                        &word_begin);
      // encode the word
      // printf("%s:%d word_long 0x%X\n", __FILE__, __LINE__, word_long);
      if (word_long == 0) {
        break;
      }
      word_number_encode(word_long, dictionary_file + line_begin + word_begin,
                         &word_code);
      // get the foreign word
      foreign_word_begin = line_begin + word_begin + word_long + TAB_SIZE;
      foreign_word_long =
          (uint16_t)(dictionary_file_indexFinger - foreign_word_begin);
      assert(foreign_word_long < MAXIMUM_FOREIGN_WORD_LONG);
      // addenda the word code
      memcpy(dictionary_database + dictionary_long, &word_code, CODE_LONG);
      dictionary_long += CODE_LONG;
      // addenda the foreign word
      memcpy(dictionary_database + dictionary_long,
             dictionary_file + foreign_word_begin, foreign_word_long);
      dictionary_long += foreign_word_long;
      // addenda any extra spaces
      memset(dictionary_database + dictionary_long, SPACE_LETTER,
             MAXIMUM_FOREIGN_WORD_LONG - foreign_word_long);
      dictionary_long += MAXIMUM_FOREIGN_WORD_LONG - foreign_word_long;
      dictionary_database[dictionary_long - 1] = '\n';
      assert(dictionary_long <= DICTIONARY_DATABASE_LONG);
      line_begin = dictionary_file_indexFinger;
    }
  }
  // dictionary_database[dictionary_long-1] = (char) 0;

  // append to file after each system page of output
  //
  // uint16_t paper_magnitude = (uint16_t)(
  // dictionary_long/MAXIMUM_PAPER_MAGNITUDE);
  //   paper_long = MAXIMUM_PAPER_MAGNITUDE;
  // for (paper_indexFinger = 0;
  //     paper_indexFinger <= paper_magnitude;
  //     ++paper_indexFinger) {
  //   if(dictionary_long < MAXIMUM_PAPER_MAGNITUDE ) {
  //     paper_long = (uint16_t) dictionary_long;
  //   }
  // printf("%s:%d dictionary_long 0x%X\n", __FILE__, __LINE__,dictionary_long);
  produce_filename = "probe/en.kwon";
  remove(produce_filename);
  printf("%s:%d writing dictionary database '%s'\n", __FILE__, __LINE__,
         produce_filename);
  text_file_addenda(dictionary_long, dictionary_database, produce_filename);
  return 0;
  }
