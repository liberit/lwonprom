/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
//#include "tlep.h"
//#include "pyash.h"
#include "sort.h"

#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
#include <errno.h>
#include <execinfo.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#endif
//#include <assert.h>
//#include <stdio.h>

uint64_t modulo(const uint64_t value, const uint64_t modulo) {
  if ((modulo & 1) == 0) {
    return (value & (modulo - 1));
  } else {
    return value % modulo;
  }
}
void v4us_write(uint8_t code_indexFinger, uint8_t maximum_code_indexFinger,
                uint16_t code_number, v4us *code_name) {
  if (code_indexFinger >= maximum_code_indexFinger)
    return;
  else if (code_indexFinger == 0) {
    (*code_name).s0 = code_number;
  } else if (code_indexFinger == 1) {
    (*code_name).s1 = code_number;
  } else if (code_indexFinger == 2)
    (*code_name).s2 = code_number;
}

uint64_t v4us_uint64_translation(const v4us vector) {
  return (uint64_t)(
      ((uint64_t)vector.s0 << (16 * 0)) + ((uint64_t)vector.s1 << (16 * 1)) +
      ((uint64_t)vector.s2 << (16 * 2)) + ((uint64_t)vector.s3 << (16 * 3)));
}

void line_t_write(const uint8_t indexFinger, const uint16_t number,
                  line_t *vector) {
  guarantee(indexFinger < LINE_LONG);
  guarantee(vector != NULL);
#ifdef ARRAYLINE
  (*vector)[indexFinger] = number;
#else
  switch (indexFinger) {
  case 0:
    (*vector).s0 = number;
    break;
  case 1:
    (*vector).s1 = number;
    break;
  case 2:
    (*vector).s2 = number;
    break;
  case 3:
    (*vector).s3 = number;
    break;
  case 4:
    (*vector).s4 = number;
    break;
  case 5:
    (*vector).s5 = number;
    break;
  case 6:
    (*vector).s6 = number;
    break;
  case 7:
    (*vector).s7 = number;
    break;
  case 8:
    (*vector).s8 = number;
    break;
  case 9:
    (*vector).s9 = number;
    break;
  case 0xA:
    (*vector).sA = number;
    break;
  case 0xB:
    (*vector).sB = number;
    break;
  case 0xC:
    (*vector).sC = number;
    break;
  case 0xD:
    (*vector).sD = number;
    break;
  case 0xE:
    (*vector).sE = number;
    break;
  case 0xF:
    (*vector).sF = number;
    break;
  }
#endif
}
uint16_t line_t_read(const uint8_t indexFinger, const line_t vector) {
  guarantee(indexFinger < LINE_LONG);
  uint16_t result = 0;
#ifdef ARRAYLINE
  result = vector[indexFinger];
#else
  switch (indexFinger) {
  case 0:
    result = (uint16_t)vector.s0;
    break;
  case 1:
    result = (uint16_t)vector.s1;
    break;
  case 2:
    result = (uint16_t)vector.s2;
    break;
  case 3:
    result = (uint16_t)vector.s3;
    break;
  case 4:
    result = (uint16_t)vector.s4;
    break;
  case 5:
    result = (uint16_t)vector.s5;
    break;
  case 6:
    result = (uint16_t)vector.s6;
    break;
  case 7:
    result = (uint16_t)vector.s7;
    break;
  case 8:
    result = (uint16_t)vector.s8;
    break;
  case 9:
    result = (uint16_t)vector.s9;
    break;
  case 0xA:
    result = (uint16_t)vector.sA;
    break;
  case 0xB:
    result = (uint16_t)vector.sB;
    break;
  case 0xC:
    result = (uint16_t)vector.sC;
    break;
  case 0xD:
    result = (uint16_t)vector.sD;
    break;
  case 0xE:
    result = (uint16_t)vector.sE;
    break;
  case 0xF:
    result = (uint16_t)vector.sF;
    break;
  default:
    guarantee(1 == 0); //"invalid value"
    result = 0;
  }
#endif
  return result;
}

int line_t_print(const line_t tablet) {
  // print_backtrace();
#if defined(OPENCL) == 0 && defined(EMCRIPTEN) == 0
#ifdef ARRAYLINE
  return printf("0x(%04X %04X %04X %04X  %04X %04X %04X %04X  %04X %04X %04X "
                "%04X  %04X %04X %04X %04X)\n",
                tablet[0], tablet[1], tablet[2], tablet[3], tablet[4],
                tablet[5], tablet[6], tablet[7], tablet[8], tablet[9],
                tablet[0xA], tablet[0xB], tablet[0xC], tablet[0xD], tablet[0xE],
                tablet[0xF]);
#else
  return printf("0x(%04X %04X %04X %04X  %04X %04X %04X %04X  %04X %04X %04X "
                "%04X  %04X %04X %04X %04X)\n",
                tablet.s0, tablet.s1, tablet.s2, tablet.s3, tablet.s4,
                tablet.s5, tablet.s6, tablet.s7, tablet.s8, tablet.s9,
                tablet.sA, tablet.sB, tablet.sC, tablet.sD, tablet.sE,
                tablet.sF);
#endif
#else
  return printf("0x(%#v16hX)\n", tablet);
#endif
}
int _tablet_print(const uint8_t sequence_long, const line_t *tablet) {
  int print_gross = 0;
  uint8_t indexFinger = 0;
  for (; indexFinger < sequence_long; ++indexFinger) {
    //if (tablet[indexFinger][0] == 0) {
    //  printf("\t0x(0) ");
    //  // break;
    //} else {
      printf("\t");
      print_gross = line_t_print(tablet[indexFinger]);
    //}
  }
  printf("\n");
  return print_gross;
}

void line_t_forward_copy(const uint8_t abl, const uint8_t dat,
                         const uint8_t ins, line_t *tablet) {
  guarantee(dat + ins <= 0xF);
  guarantee(abl < dat);
  guarantee(tablet != NULL);
  uint8_t indexFinger = dat + ins;
  for (; indexFinger >= abl; --indexFinger) {
    line_t_write(indexFinger, line_t_read(indexFinger - ins, *tablet), tablet);
  }
}

uint16_t tablet_read(const txik_t indexFinger, const uint8_t sequence_long,
                     const line_t *tablet) {
  guarantee(sequence_long > 0);
  if (!(LINE_LONG * sequence_long > indexFinger)) {
    DEBUGPRINT(
        ("0x%X indexFinger, 0x%X sequence_long\n", indexFinger, sequence_long));
    tablet_print(sequence_long, tablet);
  }
  if ((LINE_LONG * sequence_long) < indexFinger) {
    DEBUGPRINT(("0x%X sequence_long, 0x%X indexFinger\n",
                LINE_LONG * sequence_long, indexFinger));
  }
  guarantee((LINE_LONG * sequence_long) > indexFinger);
  guarantee(tablet != NULL);
  return line_t_read(indexFinger & LINE_LONG_MASK,
                     tablet[indexFinger / LINE_LONG]);
}
void tablet_write(const uint16_t indexFinger, const uint16_t number,
                  const uint8_t sequence_long, line_t *tablet) {
  if (LINE_LONG * sequence_long <= indexFinger) {
    DEBUGPRINT(
        ("%X indexFinger, %X sequence_long\n", indexFinger, sequence_long));
    guarantee(LINE_LONG * sequence_long > indexFinger);
  }
  guarantee(tablet != NULL);
  line_t_write(indexFinger & LINE_LONG_MASK, number,
               tablet + indexFinger / LINE_LONG);
}

void tablet_word_write(const uint16_t indexFinger, const uint16_t number,
                       const uint8_t sequence_long, line_t *tablet,
                       uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  uint16_t list_indexFinger = indexFinger - (indexFinger & LINE_LONG_MASK);
  uint16_t binary_phrase_list =
      tablet_read(list_indexFinger, sequence_long, tablet);
  if (binary_phrase_list == 0) {
    binary_phrase_list = 1;
    tablet_write(list_indexFinger, binary_phrase_list, sequence_long, tablet);
  }
  uint8_t vector_indexFinger = indexFinger & LINE_LONG_MASK;
  uint8_t sequence_indexFinger = 0;
  uint8_t indicator = 0;
  if (vector_indexFinger == 0) {
    ++*neo_indexFinger;
    // also invert the previous binary_phrase_list, if it exists.
    sequence_indexFinger = (uint8_t)indexFinger / LINE_LONG;
    if (sequence_indexFinger > 0) {
#ifdef ARRAYLINE
      binary_phrase_list = (uint16_t)tablet[sequence_indexFinger - 1][0];
#else
      binary_phrase_list = (uint16_t)tablet[sequence_indexFinger - 1].s0;
#endif
      indicator = binary_phrase_list & 1;
      if (indicator == HTIN_FINALLY_INDICATOR) {
        binary_phrase_list = ~binary_phrase_list;
#ifdef ARRAYLINE
        tablet[sequence_indexFinger - 1][0] = binary_phrase_list;
#else
        tablet[sequence_indexFinger - 1].s0 = binary_phrase_list;
#endif
      }
    }
  }
  tablet_write(*neo_indexFinger, number, sequence_long, tablet);
}
void tablet_retrospective_word_write(const uint16_t indexFinger,
                                     const uint16_t number,
                                     const uint8_t sequence_long,
                                     line_t *tablet,
                                     uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  if ((indexFinger & LINE_LONG_MASK) == 0) {
    --*neo_indexFinger;
  }
  tablet_write(*neo_indexFinger, number, sequence_long, tablet);
}
word_t tablet_upcoming_word_read(const uint16_t indexFinger,
                                 const uint8_t sequence_long,
                                 const line_t *tablet,
                                 uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  const txik_t hxittxik = (sequence_long * LINE_LONG) - 1;
  uint8_t remaining_lines = sequence_long - (indexFinger / LINE_LONG);
  word_t word = 0;
  repeat(remaining_lines,
         if ((indexFinger & LINE_LONG_MASK) == 0) {
           //        // check if tablet has index, if not then skip to next one.
           word = tablet_read(*neo_indexFinger, sequence_long, tablet);
           if (word != 0) {
             ++*neo_indexFinger;
             word = tablet_read(*neo_indexFinger, sequence_long, tablet);
             break;
           } else {
             if ((*neo_indexFinger + LINE_LONG) > hxittxik) {
               return 0;
             } else {
               // *neo_indexFinger += LINE_LONG;
               // continue;
             }
           }
         }
         //
         //      } else {
         word = tablet_read(*neo_indexFinger, sequence_long, tablet);
         if (word != 0) { break; }
         //        *neo_indexFinger =
         //        round_theIndexFinger_toRoofLine(*neo_indexFinger);
         //      }
  );
  return word;
}
word_t upcoming_word_read(const uint16_t indexFinger,
                          const uint8_t sequence_long, const line_t *tablet,
                          uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  const txik_t hxittxik = (sequence_long * LINE_LONG) - 1;
  uint8_t remaining_lines = sequence_long - (indexFinger / LINE_LONG);
  word_t word = 0;
  repeat(remaining_lines,
         if ((indexFinger & LINE_LONG_MASK) == 0) {
           //        // check if tablet has index, if not then skip to next one.
           word = tablet_read(*neo_indexFinger, sequence_long, tablet);
           if (word != 0) {
             ++*neo_indexFinger;
             word = tablet_read(*neo_indexFinger, sequence_long, tablet);
             break;
           } else {
             if ((*neo_indexFinger + LINE_LONG) > hxittxik) {
               return 0;
             } else {
               *neo_indexFinger += LINE_LONG;
               continue;
             }
           }
         }
         //
         //      } else {
         word = tablet_read(*neo_indexFinger, sequence_long, tablet);
         if (word != 0) { break; }
         //        *neo_indexFinger =
         //        round_theIndexFinger_toRoofLine(*neo_indexFinger);
         //      }
  );
  return word;
}
uint16_t /*word*/ tablet_retrospective_word_read(const uint16_t indexFinger,
                                                 const uint8_t sequence_long,
                                                 const line_t *tablet,
                                                 uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  if (indexFinger == 0)
    return 0;
  if ((indexFinger & LINE_LONG_MASK) == 0) {
    --*neo_indexFinger;
  }
  return tablet_read(*neo_indexFinger, sequence_long, tablet);
}

void tablet_grammar_write(const uint16_t indexFinger, const uint16_t number,
                          const uint8_t sequence_long, line_t *tablet,
                          uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  *neo_indexFinger = indexFinger;
  uint16_t list_indexFinger = indexFinger - (indexFinger & LINE_LONG_MASK);
  uint16_t indicator_list =
      tablet_read(list_indexFinger, sequence_long, tablet);
  if (indicator_list == 0) {
    indicator_list = 1;
  }
  // printf("%s:%d list_indexFinger 0x%X, indicator_list 0x%04X \n", __FILE__,
  //       __LINE__, list_indexFinger, indicator_list);
  // check if writing beyond end, then skip to next sequence, and invert
  // previous
  // quote.
  uint16_t binary_phrase_list = 0;
  uint8_t vector_indexFinger = indexFinger & LINE_LONG_MASK;
  uint8_t sequence_indexFinger = 0;
  uint8_t indicator = 1;
  if (vector_indexFinger == 0) {
    indicator_list = ~indicator_list;
    tablet_write(list_indexFinger, indicator_list, sequence_long, tablet);
    list_indexFinger = indexFinger;
    // printf("%s:%d indexFinger %X\n", __FILE__, __LINE__, indexFinger);
    ++*neo_indexFinger;
    // also invert the previous binary_phrase_list, if it exists.
    sequence_indexFinger = (uint8_t)indexFinger / LINE_LONG;
    if (sequence_indexFinger > 0) {
#ifdef ARRAYLINE
      binary_phrase_list = (uint16_t)tablet[sequence_indexFinger - 1][0];
#else
      binary_phrase_list = (uint16_t)tablet[sequence_indexFinger - 1].s0;
#endif
      indicator = binary_phrase_list & 1;
      // printf("%s:%d indicator %X\n", __FILE__, __LINE__, indicator);
      if (indicator == HTIN_FINALLY_INDICATOR) {
        binary_phrase_list = ~binary_phrase_list;
        // printf("%s:%d binary_phrase_list %04X\n", __FILE__, __LINE__,
        //      binary_phrase_list);
#ifdef ARRAYLINE
        tablet[sequence_indexFinger - 1][0] = binary_phrase_list;
#else
        tablet[sequence_indexFinger - 1].s0 = binary_phrase_list;
#endif
      }
    }
    indicator_list = 1;
  }
  // add to indicator list if is case or mood grammar_word
  // assume is mood or grammar word because this was called
  indicator_list |= 1 << (*neo_indexFinger & LINE_LONG_MASK);
  // printf("%s:%d neo_indexFinger 0x%X, indicator_list 0x%04X \n", __FILE__,
  //       __LINE__, *neo_indexFinger, indicator_list);
  tablet_write(list_indexFinger, indicator_list, sequence_long, tablet);
  // tablet[sequence_indexFinger].s0 = indicator_list;
  tablet_write(*neo_indexFinger, number, sequence_long, tablet);
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  // printf("%s:%d\n\t ", __FILE__, __LINE__);
  // line_t_print(tablet[0]);
}
/**
 * @return neo_indexFinger -- if not found returns maximum indexFinger
 */
uint16_t tablet_upcoming_grammar_read(
    const uint16_t indexFinger, const uint8_t sequence_long,
    const line_t *tablet,
    uint16_t *neo_indexFinger /* if not found, returns maximumg indexFinger*/) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  // get indicator_list for the relevant indexFinger,
  uint16_t indicator_list = 0;
  // get indicator from it (is first bit).
  uint8_t indicator = 0;
  // check for set indicators after indexFinger
  uint8_t list_indexFinger = 0;
  const uint16_t max_indexFinger = sequence_long * LINE_LONG;
  uint8_t sequence_indexFinger = (uint8_t)(indexFinger / LINE_LONG);
  const uint8_t begin_sequence_indexFinger = sequence_indexFinger;
  uint16_t tablet_indexFinger = 0;
  for (; sequence_indexFinger < sequence_long; ++sequence_indexFinger) {
    indicator_list =
        tablet_read(sequence_indexFinger * LINE_LONG, sequence_long, tablet);
    indicator = indicator_list & 1;
    // if (begin_sequence_indexFinger == sequence_indexFinger) {
    //   list_indexFinger = indexFinger % LINE_LONG;
    // } else {
    list_indexFinger = 1;
    // }
    for (; list_indexFinger < LINE_LONG; ++list_indexFinger) {
      tablet_indexFinger = sequence_indexFinger * LINE_LONG + list_indexFinger;
      if (tablet_indexFinger < indexFinger) {
        list_indexFinger = indexFinger & LINE_LONG_MASK;
      }
      if (((indicator_list >> list_indexFinger) & 1) == indicator) {
        //  if found then return
        *neo_indexFinger = sequence_indexFinger * LINE_LONG + list_indexFinger;
        // DEBUGPRINT(("%X *neo_indexFinger\n", *neo_indexFinger));
        return tablet_read(*neo_indexFinger, sequence_long, tablet);
      }
    }
    //  if not found then check if indicator is a 0, then can rerun for next in
    //  sequence.
    // if (indicator == 1) {
    //  tablet_print(sequence_long, tablet);
    //  break;
    //}
    list_indexFinger = 0;
  }
  *neo_indexFinger = max_indexFinger; //(sequence_indexFinger + 1)* LINE_LONG;
  // if no grammar words found, then return neo_indexFinger  as length
  // printf("%s:%d:%s ", __FILE__, __LINE__, __FUNCTION__);
  // line_t_print(*tablet);
  return 0;
}

/* if finds nothing then returns 0 */
uint16_t tablet_upcoming_grammar_or_quote_read(const uint16_t indexFinger,
                                               const uint8_t sequence_long,
                                               const line_t *tablet,
                                               uint16_t *neo_indexFinger) {
  guarantee(LINE_LONG * sequence_long > indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  *neo_indexFinger = indexFinger;
  uint16_t tablet_indexFinger = 0;
  uint8_t sequence_indexFinger = (uint8_t)(indexFinger / LINE_LONG);
  // get indicator_list for the relevant indexFinger,
  uint16_t indicator_list = 0;
  // get indicator from it (is first bit).
  uint8_t indicator = 0;
  // check for set indicators after indexFinger
  uint8_t list_indexFinger = (indexFinger & LINE_LONG_MASK);
  uint16_t word_code = 0;

  // DEBUGPRINT("0x%X sequence_indexFinger, 0x%X sequence_long\n",
  //     sequence_indexFinger, sequence_long);
  for (; sequence_indexFinger < sequence_long; ++sequence_indexFinger) {
    // tablet_print(sequence_long, tablet);
    indicator_list =
        tablet_read(sequence_indexFinger * LINE_LONG, sequence_long, tablet);
    indicator = indicator_list & 1;
    // printf("%s:%d:%s\n\t 0x%X indicator_list, 0x%X indicator, 0x%X
    // sequence_indexFinger\n", __FILE__, __LINE__,
    //    __FUNCTION__, indicator_list, indicator, sequence_indexFinger);
    for (list_indexFinger = 1; list_indexFinger < LINE_LONG;
         ++list_indexFinger) {
      tablet_indexFinger = sequence_indexFinger * LINE_LONG + list_indexFinger;
      if (tablet_indexFinger < indexFinger) {
        list_indexFinger = (indexFinger & LINE_LONG_MASK);
        tablet_indexFinger =
            sequence_indexFinger * LINE_LONG + list_indexFinger;
      }
      // DEBUGPRINT("0x%X list_indexFinger, 0x%X tablet_indexFinger, 0x%X
      // result\n",
      //    list_indexFinger, indexFinger, (uint16_t)
      //   ((indicator_list >> list_indexFinger) & 1));
      if (((indicator_list >> list_indexFinger) & 1) == indicator) {
        //  if found then return
        *neo_indexFinger = tablet_indexFinger;
        return tablet_read(tablet_indexFinger, sequence_long, tablet);
      }
      if (((indicator_list >> (list_indexFinger - 1)) & 1) == indicator) {
        word_code = upcoming_word_read(tablet_indexFinger, sequence_long,
                                       tablet, &tablet_indexFinger);
        if ((word_code & (uint16_t)QUOTE_DENOTE_MASK) ==
            (uint16_t)QUOTE_DENOTE) {
          // printf("%s:%d:%s\n\t 0x%X quote_detected\n", __FILE__, __LINE__,
          //      __FUNCTION__, word_code);
          *neo_indexFinger = tablet_indexFinger;
          return word_code;
        }
      }
      //  if not found then check if indicator is a 0, then can rerun for next
      //  in
      //  sequence.
    }
    // if (indicator == 1) {
    //   break;
    // }
    // DEBUGPRINT(("0x%X sequence_indexFinger\n", sequence_indexFinger));
    list_indexFinger = 0;
  }
  // if no grammar words found, then return neo_indexFinger at max value
  *neo_indexFinger = 0xFFFF;
  return 0;
}

ulong text_long_derive(const char *str, ulong max_len) {
  ulong iteration = 0;
  for (; iteration < max_len; ++iteration) {
    if (str[iteration] == 0) {
      ++iteration;
      break;
    }
  }
  return iteration;
}

uint16_t /*grammar word*/ tablet_retrospective_grammar_read(
    const uint16_t gross_indexFinger, const uint8_t sequence_long,
    const line_t *tablet, uint16_t neo_indexFinger[]) {
  // if (!(LINE_LONG * sequence_long > indexFinger)) {
  //  DEBUGPRINT(("0x%X sequence_long, 0x%X indexFinger", sequence_long,
  //  indexFinger));
  //  tablet_print(sequence_long, tablet);
  //}
  uint16_t indexFinger =
      gross_indexFinger % LINE_LONG == 0 && gross_indexFinger > 0
          ? gross_indexFinger - 1
          : gross_indexFinger;
  guarantee(LINE_LONG * sequence_long >= indexFinger);
  guarantee(tablet != NULL);
  guarantee(neo_indexFinger != NULL);
  neo_indexFinger[0] = indexFinger;
  // get indicator_list for the relevant indexFinger,
  uint16_t indicator_list = 0;
  uint16_t previous_indicator_list = 0;
  // get indicator from it (is first bit).
  uint8_t indicator = 0;
  uint16_t word = 0;
  uint8_t previous_indicator = 0;
  // uint8_t iteration = 0;
  // check for set indicators after indexFinger
  uint8_t list_indexFinger = (indexFinger & LINE_LONG_MASK);
  uint8_t sequence_indexFinger =
      indexFinger == list_indexFinger ? 0 : (uint8_t)(indexFinger / LINE_LONG);
  // print_backtrace();
  uint16_t retrospective_indicator_list = 0;
  // DEBUGPRINT(("0x%X indexFinger, 0x%X sequence_indexFinger\n",
  //      indexFinger, sequence_indexFinger));
  for (; sequence_indexFinger < sequence_long; --sequence_indexFinger) {
    // DEBUGPRINT(("0x%X sequence_indexFinger\n", sequence_indexFinger));
    // get indicator_list
    indicator_list =
        tablet_read(sequence_indexFinger * LINE_LONG, sequence_long, tablet);
    if (indicator_list == 0)
      continue;
    // DEBUGPRINT(("0x%X indicator_list\n", indicator_list));
    //  DEBUGPRINT(("0x%X list_indexFinger\n", list_indexFinger));
    // check kind of indicator_list
    indicator = indicator_list & 1;
    if (sequence_indexFinger > 0 && list_indexFinger == 0) {
      retrospective_indicator_list = tablet_read(
          (sequence_indexFinger - 1) * LINE_LONG, sequence_long, tablet);
      if (retrospective_indicator_list != 0 &&
          (retrospective_indicator_list & 1) == 0) {
        list_indexFinger = 0xF;
        continue;
      }
    }
    for (; list_indexFinger > 0; --list_indexFinger) {
      // DEBUGPRINT(("0x%X list_indexFinger\n", list_indexFinger));
      if (((indicator_list >> list_indexFinger) & 1) == indicator) {
        //  if found then return
        *neo_indexFinger = sequence_indexFinger * LINE_LONG + list_indexFinger;
        // DEBUGPRINT(("0x%X *neo_indexFinger\n", *neo_indexFinger));
        word = tablet_read(*neo_indexFinger, sequence_long, tablet);
        // DEBUGPRINT(
        //    ("0x%X *neo_indexFinger, 0x%X word\n", *neo_indexFinger, word));
        guarantee(*neo_indexFinger <= indexFinger);
        return word;
      }
    }
    //  if not found then check if previous_indicator is not a htin finally
    //  indicator.
    //  then can rerun for previous one;
    if (sequence_indexFinger > 0) {
      txik_t txik = (sequence_indexFinger - 1) * LINE_LONG;
      previous_indicator_list = tablet_read(txik, sequence_long, tablet);
      previous_indicator = previous_indicator_list & 1;
      // DEBUGPRINT(("%X txik, %X sequence_indexFinger, %X
      // previous_indicator\n",
      //      txik, sequence_indexFinger, previous_indicator));
      if (previous_indicator != HTIN_FINALLY_INDICATOR) {
        list_indexFinger = LINE_LONG - 1;
        continue;
      } else {
        *neo_indexFinger = sequence_indexFinger * LINE_LONG + list_indexFinger;
        guarantee(*neo_indexFinger <= indexFinger);
        return 0;
      }
    }
  }
  // if no grammar words found, then return neo_indexFinger 0
  *neo_indexFinger = 0;
  return 0;
}

uint8_t tidbit_read(const uint8_t tidbit_indexFinger, const uint8_t number_long,
                    const uint64_t number) {
  guarantee(number_long > tidbit_indexFinger);
  return (uint8_t)((number >> tidbit_indexFinger) & 1);
}
void tidbit_write(const uint8_t tidbit_indexFinger, const uint8_t fresh_tidbit,
                  const uint8_t number_long, uint16_t *number) {
  guarantee(number_long > tidbit_indexFinger);
  guarantee(fresh_tidbit == 0 || fresh_tidbit == 1);
  if (fresh_tidbit == 1) {
    *number |= 1 << tidbit_indexFinger;
  } else {
    *number &= ~(1 << tidbit_indexFinger);
  }
}

void tablet_upcoming_copy(const uint16_t abl /* source*/,
                          const uint16_t dat /* destination*/,
                          const uint16_t ins /* length*/,
                          const uint8_t sequence_long, line_t *tablet) {
  guarantee(dat + ins <= sequence_long * LINE_LONG);
  guarantee(abl < dat);
  guarantee(tablet != NULL);
  // printf("%s:%d:%s\n\t", __FILE__, __LINE__, __FUNCTION__);
  // line_t_print(*tablet);

  // printf("%s:%d:%s\n\t abl 0x%X dat 0x%X ins 0x%X \n", __FILE__, __LINE__,
  //       __FUNCTION__, abl, dat, ins);
  uint8_t abl_vector_number = 0;
  uint16_t abl_binary_phrase_list = 0;
  // check for edge cases like where destination is a different vector than the
  // source
  uint8_t dat_vector_number = 0;
  uint8_t dat_indicator = 0;
  uint8_t abl_indicator = 0;
  uint8_t dat_tidbit = 0;
  uint8_t abl_tidbit = 0;
  uint16_t dat_binary_phrase_list = 0;
  uint16_t dat_indexFinger = dat + ins;
  uint16_t abl_indexFinger = abl + ins;
  uint16_t abl_word = 0;
  for (; dat_indexFinger >= dat; --dat_indexFinger) {
    abl_word = tablet_retrospective_word_read(abl_indexFinger, sequence_long,
                                              tablet, &abl_indexFinger);
    tablet_retrospective_word_write(dat_indexFinger, abl_word, sequence_long,
                                    tablet, &dat_indexFinger);
    // read abl indicator, write dat indicator, delete abl indicator
    abl_vector_number = (uint8_t)abl_indexFinger / LINE_LONG;
    abl_binary_phrase_list = (uint16_t)tablet[abl_vector_number][0];
    abl_indicator = abl_binary_phrase_list & 1;
    dat_vector_number = (uint8_t)dat_indexFinger / LINE_LONG;
    if (dat_vector_number == abl_vector_number) {
      dat_binary_phrase_list = abl_binary_phrase_list;
    } else {
      dat_binary_phrase_list = (uint16_t)tablet[dat_vector_number][0];
    }
    dat_indicator = dat_binary_phrase_list & 1;
    abl_tidbit = tidbit_read((uint8_t)(dat_indexFinger & LINE_LONG_MASK),
                             CODE_WORD_TIDBIT_LONG, dat_binary_phrase_list);
    if (abl_tidbit == abl_indicator) {
      dat_tidbit = dat_indicator;
    } else {
      dat_tidbit = (~dat_indicator) & 1;
    }
    tidbit_write((uint8_t)(dat_indexFinger & LINE_LONG_MASK), dat_tidbit,
                 CODE_WORD_TIDBIT_LONG, &dat_binary_phrase_list);
    --abl_indexFinger;
    //  printf("%s:%d:%s\n\t ", __FILE__, __LINE__, __FUNCTION__);
    //  line_t_print(*tablet);
  }
}
ulong text_copy(const ulong text_long, constant char *text,
                const ulong produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(text != NULL);
  ulong iteration = 0;
  for (; iteration < text_long && iteration < produce_text_long; ++iteration) {
    produce_text[iteration] = text[iteration];
    if (text[iteration] == 0) {
      break;
    }
  }
  return iteration;
}
ulong constant_text_copy(constant char *text, const ulong produce_text_long,
                         char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(text != NULL);
  uint16_t iterator = 0;
  repeat_with_except(produce_text_long, iterator, text[iterator] == 0,
                     produce_text[iterator] = text[iterator]);
  return iterator;
}
ulong error_text_copy(constant char *text, const char *topic_text,
                      const ulong produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(text != NULL);
  uint16_t iterator = 0;
  uint16_t text_iterator = 0;
  uint16_t topic_iterator = 0;
  uint16_t vacant_text_long = produce_text_long;
  repeat_with_except(vacant_text_long, iterator,
                     text[iterator] == 0 || text[iterator] == '%',
                     produce_text[iterator] = text[iterator]);
  text_iterator = iterator;
  vacant_text_long -= iterator;
  // if (text[iterator] == '%')
  repeat_with_except(
      vacant_text_long, iterator,
      topic_text[iterator - text_iterator] == 0 || text[iterator] == '%',
      produce_text[iterator] = topic_text[iterator - (text_iterator)]);
  ++iterator;
  vacant_text_long = produce_text_long - iterator;
  topic_iterator = iterator;
  repeat_with_except(vacant_text_long, iterator,
                     text[iterator - topic_iterator + text_iterator] == 0 ||
                         text[iterator] == '%',
                     produce_text[iterator] =
                         text[iterator - topic_iterator + text_iterator + 1]);
  return iterator;
}
ulong perfect_copy(const ulong text_long, const char *text,
                   const ulong produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(text != NULL);
  ulong iteration = 0;
  for (; iteration < text_long && iteration < produce_text_long; ++iteration) {
    produce_text[iteration] = text[iteration];
  }
  return iteration;
}
#if defined(EMSCRIPTEN) == 0 && defined(OPENCL) == 0
#include <stdarg.h>
int form_print(const ulong pad_long, char *pad, const char *form, ...) {
  va_list variable;
  va_start(variable, form);
  int consequence =
      vsnprintf(pad, pad_long, form, variable); /*FlawFinder: ignore*/
  // do something with the error
  va_end(variable);
  return consequence;
}
#endif

ulong number_situate(const uint16_t number, const ulong number_sequence_long,
                     constant uint16_t *number_sequence) {
  // returns the length of the array if can't find it.
  guarantee(number_sequence != NULL);
  ulong iteration = 0;
  for (; iteration < number_sequence_long; ++iteration) {
    if (number == number_sequence[iteration]) {
      return iteration;
    }
  }
  return number_sequence_long;
}
int64_t absolute(const int64_t number) {
  if (number < 0) {
    return -number;
  }
  return number;
}

// indexOf
ulong gen_indexFinger(const uint16_t example, const ulong sequence_long,
                      constant uint16_t sequence[]) {
  repeat(sequence_long,
         conditional(sequence[iterator] == example, return iterator));
  return sequence_long;
}
#ifdef ARRAYLINE
void line_copy(const line_t input, line_t produce) {
  repeat(LINE_LONG, produce[iterator] = input[iterator];
         //  DEBUGPRINT(("%X produce[%lX]\n", produce[iterator], iterator));
  );
}
#else
#define line_copy(input, produce) produce = input
#endif
void global_line_copy(const global line_t input, global line_t produce) {
  repeat(LINE_LONG, produce[iterator] = input[iterator];
         //  DEBUGPRINT(("%X produce[%lX]\n", produce[iterator], iterator));
  );
}
uint16_t /* produce_long */ tablet_copy(const uint16_t tablet_long,
                                        const line_t tablet[],
                                        const uint16_t max_produce_long,
                                        line_t produce[]) {
  guarantee(tablet_long <= max_produce_long);
  uint16_t iterator = 0;
  repeat_with(tablet_long, iterator,
              line_copy(tablet[iterator], produce[iterator]));
  return iterator;
}
uint8_t code_tablet_long_found(const uint16_t recipe_long,
                               const line_t *recipe) {

  uint16_t final_vector_indexFinger = 0;
  for (; final_vector_indexFinger < recipe_long; ++final_vector_indexFinger) {
    if ((recipe[final_vector_indexFinger][0] & 1) == 1) {
      break;
    }
  }
  return (uint8_t)final_vector_indexFinger + 1;
}
uint8_t knowledge_long_found(const uint16_t knowledge_long,
                             const line_t *knowledge) {

  uint16_t iterator = 0;
#ifdef ARRAYLINE
  conditional(knowledge[0][0] == 0, return 0);
  repeat_with_except(knowledge_long, iterator, knowledge[iterator][0] == 0, );
#else
  conditional(knowledge[0].s0 == 0, return 0);
  repeat_with_except(knowledge_long, iterator, knowledge[iterator].s0 == 0, );
#endif
  return iterator;
}
#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
/* from stack overflow
 * https://stackoverflow.com/questions/6934659/how-to-make-backtrace-backtrace-symbols-print-the-function-names
 */
static void full_write(int fd, const char *buf, size_t len) {
  while (len > 0) {
    ssize_t ret = write(fd, buf, len);

    if ((ret == -1) && (errno != EINTR))
      break;

    buf += (size_t)ret;
    len -= (size_t)ret;
  }
}

void print_backtrace(void) {
  static const char start[] = "BACKTRACE ------------\n";
  static const char end[] = "----------------------\n";

  void *bt[1024];
  int bt_size;
  char **bt_syms;
  int i;

  bt_size = backtrace(bt, 0x10);
  bt_syms = backtrace_symbols(bt, bt_size);
  full_write(STDOUT_FILENO, start, strlen(start)); /*flawfinder: ignore*/
  for (i = 1; i < bt_size; i++) {
    size_t len = strlen(bt_syms[i]); /*flawfinder: ignore*/
    full_write(STDOUT_FILENO, bt_syms[i], len);
    full_write(STDOUT_FILENO, "\n", 1);
  }
  full_write(STDOUT_FILENO, end, strlen(end)); /*flawfinder: ignore*/
  free(bt_syms);
}
#else
void print_backtrace(void) {}
#endif

void _phrase_print(const struct Phrase phrase) {
  // guarantee(phrase.page.lines != NULL);
  conditional(phrase.page.lines == NULL, DEBUGPRINT(("\n")); return );
  if (!(phrase.page.plength * LINE_LONG >= phrase.begin + phrase.length)) {
    DEBUGPRINT(("%X phrase.page.plength, %X phrase.begin, %X phrase.length\n",
                phrase.page.plength, phrase.begin, phrase.length));
    guarantee(phrase.page.plength * LINE_LONG >= phrase.begin + phrase.length);
  }
  uint16_t word_txik = phrase.begin;
  // DEBUGPRINT(("%X iterator\n", word_txik));
  repeat_with(phrase.begin + phrase.length, word_txik,
              // DEBUGPRINT(("%X iterator\n", word_txik));
              // print_backtrace();
              printf("0x%04X ", neo_tablet_upcoming_word_read(
                                    word_txik, phrase.page, &word_txik)));
  printf("\n");
}

struct Phrase /*grammar_word*/
neo_tablet_retrospective_grammar_read(const struct Phrase input) {
  guarantee(input.page.lines != NULL);
  struct Phrase grammar_word_range;
  grammar_word_range.page = input.page;
  grammar_word_range.length = 1;
  // phrase_print(input);
  // DEBUGPRINT(("%x input.begin %x input.length, %x txik\n", input.begin,
  //      input.length, input.length + input.begin));
  txik_t txik = 0;
  word_t word = tablet_retrospective_grammar_read(
      input.length + input.begin, input.page.plength, input.page.lines, &txik);
  grammar_word_range.begin = txik;
  // DEBUGPRINT(("%X begin, %X length\n" ,(*grammar_word_range).begin,
  // (*grammar_word_range).length));
  return grammar_word_range;
}

struct Phrase /*grammar_word*/
neo_tablet_upcoming_grammar_read(const struct Phrase input) {
  guarantee(input.page.lines != NULL);
  struct Phrase grammar_word_range;
  grammar_word_range.page = input.page;
  grammar_word_range.length = 1;
  // phrase_print(input);
  // DEBUGPRINT(("%x input.begin %x input.length\n", input_range.begin,
  // input_range.length));
  txik_t txik = input.begin;
  word_t word = tablet_upcoming_grammar_read(input.begin, input.page.plength,
                                             input.page.lines, &txik);
  // DEBUGPRINT(("%X txik, %X word\n", txik, word));
  grammar_word_range.begin = txik;
  // if not found
  if (txik == input.page.plength * LINE_LONG)
    grammar_word_range.length = 0;
  // DEBUGPRINT(("%X begin, %X length\n" ,(*grammar_word_range).begin,
  // (*grammar_word_range).length));
  return grammar_word_range;
}

/** if attempts to read index then returns 0 */
word_t /* word */ read_thePhraseZWord_atThePlace(struct Phrase phrase,
                                                 txik_t word_txik) {
  guarantee(phrase.page.lines != NULL);
  conditional(((phrase.begin + word_txik) & LINE_LONG_MASK) == 0, return 0);
  conditional(word_txik + 1 > phrase.length, return 0);
  return tablet_read(phrase.begin + word_txik, phrase.page.plength,
                     phrase.page.lines);
}

struct Phrase /* grammar phrase */
upcoming_grammar_read(const struct Phrase input) {
  txik_t txik = 0;
  NewHollowPhrase(produce);
  // DEBUGPRINT(("0x%X txik\n", txik));
  // phrase_print(input);
  tablet_upcoming_grammar_read(input.begin, input.page.plength,
                               input.page.lines, &txik);
  // if returns maximum txik then return empty phrase
  conditional(txik >= input.page.plength * LINE_LONG, return produce);
  // DEBUGPRINT(("0x%X txik\n", txik));
  // DEBUGPRINT(("0x%X word\n", tablet_read(txik, input.page.plength,
  // input.page.lines)));
  if (txik > input.begin + input.length) {
    // return hollow phase
    // DEBUGPRINT(("0x%X txik\n", txik));
    return produce;
  }
  guarantee(txik < input.begin + input.length);
  produce = input;
  produce.begin = txik;
  produce.length = 1;
  return produce;
}

word_t /* blu7n */ bwitnweh_fyakka_ri(struct Phrase fyak) {
  txik_t max_txik = fyak.page.plength * LINE_LONG - 1;
  conditional((fyak.begin + fyak.length) <= max_txik, return truth_WORD);
  return lie_WORD;
}
/*------------------------------------------------------------------
 * stringlen_s.h
 *
 * October 2008, Bo Berry
 * Modified 2015, James Benner <james.benner@gmail.com>
 *------------------------------------------------------------------*/

ulong stringlen(const char *s, ulong maxsize) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare"
  if (s == NULL) {
#pragma clang diagnostic pop
    return 0;
  }
  size_t count = 0;
  while (*s++ && maxsize--) {
    count++;
  }
  return count;
}
ulong cstringlen(constant char *s, ulong maxsize) {
  if (s == NULL) {
    return 0;
  }

  size_t count = 0;

  while (*s++ && maxsize--) {
    count++;
  }

  return count;
}
struct Phrase /* modified phrse */
write_theWord_toThePhrase_atThePlace(word_t word, struct Phrase phrase,
                                     txik_t place) {
  tablet_write(phrase.begin + place, word, phrase.page.plength,
               phrase.page.lines);
  return phrase;
}

struct Phrase /* modified phrse */
write_theGrammarWord_toThePhrase_atThePlace(word_t word, struct Phrase phrase,
                                     txik_t place) {
  tablet_grammar_write(phrase.begin + place, word, phrase.page.plength,
               phrase.page.lines, &place);
  return phrase;
}

#ifndef OPENCL
/** copies Text to produce
 * @param input -- text to be copied
 * @param produce -- text pad to put input text into
 * @return produce has length set to input
 */
struct Text /* produce */ neo_text_copy(struct Text input,
                                        struct Text produce) {
  guarantee(input.length <= produce.length);
  text_copy(input.length, input.letters, produce.length, produce.letters);
  produce.length = input.length;
  return produce;
}
#endif
// page_long_diagnose
uint8_t program_long_diagnose(const uint8_t max_program_long,
                              const line_t *program) {
  // find program length
  uint8_t iteration = 0;
  uint8_t program_long = 0;
  //_tablet_print(max_program_long, program);
  for (iteration = max_program_long; iteration > 0; --iteration) {
    // DEBUGPRINT(("%X program[%X][0]\n", program[iteration][0], iteration));
    if (program[iteration - 1][0] == 0) {
      continue;
    } else {
      program_long = iteration;
      break;
    }
  }
  return program_long;
}
uint16_t phrase_long_diagnose(struct Phrase phrase) {
  // find program length
  struct Page new_page = phrase.page;
  new_page.plength =
      (((phrase.length + phrase.begin) | LINE_LONG_MASK) / LINE_LONG) + 1;
  new_page.plength = new_page.plength > phrase.page.plength
                         ? phrase.page.plength
                         : new_page.plength;
  uint8_t page_length = page_long_diagnose(new_page);
  // DEBUGPRINT(("%X page_length\n", page_length));
  uint16_t phrase_long =
      page_length == 0 || phrase.begin > page_length * LINE_LONG
          ? 0
          : (page_length)*LINE_LONG - phrase.begin;
  // DEBUGPRINT(("%X phrase_long\n", phrase_long));
  txik_t phrase_txik = phrase_long == 0 ? 0 : phrase_long - 1;
  phrase.length = phrase_long;
  // DEBUGPRINT(("%X phrase_txik\n", phrase_txik));
  down_repeat_with(
      phrase_txik,
      conditional(phrase_word_read(phrase, phrase_txik) != 0, break));
  // DEBUGPRINT(("%X phrase_txik\n", phrase_txik));
  phrase_txik += phrase_word_read(phrase, phrase_txik) == 0 ? 0 : 1;
  guarantee(phrase_txik < phrase.page.plength * LINE_LONG);
  return phrase_txik;
}

struct Text text_zero(struct Text text) {
  repeat(text.length, text.letters[iterator] = 0);
  text.length = 0;
  return text;
}
struct Text text_tail(struct Text text, uint16_t length) {
  guarantee(text.length >= length);
  text.letters = text.letters + text.length - length;
  text.length = length;
  return text;
}
// #define round_thePhrase_toRoofLine hrathlasma_fyakka_hyuctu
struct Phrase hrathlasma_fyakka_hyuctu(struct Phrase phrase) {
  if ((LINE_LONG_MASK & phrase.length) != 0) {
    phrase.length |= LINE_LONG_MASK;
    phrase.length += 1;
  }
  return phrase;
}
// #define round_theIndexFinger_toRoofLine hrathlasma_fyakka_hyuctu
txik_t hrathlasma_txikka_hyuctu(txik_t txik) {
  if ((LINE_LONG_MASK & txik) != 0) {
    txik |= LINE_LONG_MASK;
    txik += 1;
  }
  return txik;
}

// #define delete_aGrammarWord_atThePlace_inThePhrase(_word, _place, _phrase) \
//  fyaknweh_tcaslwoh_hgaftlatka_dlastu(_phrase, _place, _word)
struct Phrase fyaknweh_tcaslwoh_hgaftlatka_dlastu(struct Phrase phrase,
                                                  txik_t place) {
  write_theWord_toThePhrase_atThePlace(0, phrase, place);
  //Page_print(phrase.page);
  // get index and modify it accordingly
  const txik_t txik = place + phrase.begin;
  //DEBUGPRINT(("%X place, %X txik\n", place, txik));
  const txik_t indicator_list_place = (txik | LINE_LONG_MASK) - LINE_LONG_MASK;
  word_t indicator_list =
      tablet_read(indicator_list_place, phrase.page.plength, phrase.page.lines);
  // get indicator tidbit
  const word_t indicator_tidbit = indicator_list & 1;
  // check if grammar bit flipped for place,
  txik_t line_txik = txik & LINE_LONG_MASK;
  conditional(
      ((indicator_list >> line_txik) & 1) == indicator_tidbit,
      // if flipped, then flip back.
      indicator_list ^= 1 << line_txik;
      //Page_print(phrase.page);
      //DEBUGPRINT(("%X indicator_list_place\n", indicator_list_place));
      tablet_write(indicator_list_place, indicator_list, phrase.page.plength,
        phrase.page.lines));
  // if word is at end of length, then reduce length by one
  txik_t length_txik = phrase.begin + phrase.length;
  conditional(txik + 1 == length_txik, phrase.length -= 1);
  return phrase;
}
