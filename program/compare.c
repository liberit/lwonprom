/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "compare.h"
#ifndef OPENCL
word_t /*boolean*/ tablet_letters_compare(const struct Page example,
                                          constant char *letters) {
  guarantee(example.plength <= HTIN_LONG);
  conditional(example.plength == 0 &&
                  cstringlen(letters, MAX_STRING_LENGTH) > 0,
              return lie_WORD);
  conditional(example.plength == 0 &&
                  cstringlen(letters, MAX_STRING_LENGTH) == 0,
              return truth_WORD);
  NewTextPage(comparison, HTIN_LONG, letters);
  comparison.plength = example.plength;
  //DEBUGPRINT(("%x example.plength \n", example.plength ));
  //Page_print(example);
  //Page_print(comparison);
  return neo_tablet_compare(example, comparison);
}

word_t phrase_letters_compare(const struct Phrase phrase,
                              constant char *letters) {
  // guarantee(phrase.page.lines != NULL);
  if (phrase.length == 0 || phrase.page.lines == NULL) {
    if (cstringlen(letters, 2) == 0) {
      return truth_WORD;
    } else {
      return lie_WORD;
    }
  }
  // DEBUGPRINT(("%X phrase.length\n", phrase.length));
  guarantee(phrase.length < HTIN_LONG * LINE_LONG);
  NewPagePhrase(example_page, HTIN_LONG);
  example_page.page.plength = phrase.length/LINE_LONG + 1;
  if (phrase.length > 0) {
    // neo_extract_words_ins_htinka_addenda(phrase, example_page);
    fyakyi_fyakka_dva2ttu(example_page, phrase);
  }
  return tablet_letters_compare(example_page.page, letters);
}
#endif

word_t phrase_to_phrase_compare(const struct Phrase phrase,
                                const struct Phrase comparison) {
  guarantee(phrase.page.lines != NULL);
  NewPagePhrase(example_phrase, HTIN_LONG);
  NewPagePhrase(comparison_phrase, HTIN_LONG);
  example_phrase = phrase_addenda(phrase, example_phrase);
  comparison_phrase = phrase_addenda(comparison, comparison_phrase);
  if (example_phrase.length != comparison_phrase.length ||
      example_phrase.begin != comparison_phrase.begin ||
      example_phrase.page.plength != comparison_phrase.page.plength) {
    return lie_WORD;
  }
  return neo_tablet_compare(example_phrase.page, comparison_phrase.page);
}

uint16_t /*boolean*/ number_sequence_compare(const size_t example_long,
                                             const uint16_t example[],
                                             const size_t comparison_long,
                                             const uint16_t comparison[]) {
  if (comparison_long != example_long)
    return lie_WORD;
  repeat(example_long,
         if (example[iterator] != comparison[iterator]) return lie_WORD);
  return truth_WORD;
}
uint16_t /* truth or lie WORD */
htin_subset_comparison(const uint16_t example_long, const line_t example[],
                       const uint16_t comparison_long,
                       const line_t comparison[]) {
  guarantee(example_long < MAX_KNOWLEDGE_LONG);
  uint16_t indexFinger = example_long * LINE_LONG - 1;
  uint16_t phrase_begin, phrase_long = 0;
  uint16_t cousin_phrase_begin, cousin_phrase_long = 0;
  uint16_t cousin = lie_WORD;
  //  tablet_print(example_long, example);
  //  DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  down_repeat_with(
      indexFinger,
      // get a phrase
      phrase_long =
          final_phrase_found(indexFinger, example_long, example, &phrase_begin);
      if (phrase_long == 0) break;

      //  DEBUGPRINT(("0x%X phrase_begin, 0x%X indexFinger\n", phrase_begin,
      //  indexFinger));
      guarantee(phrase_begin <= indexFinger);
      if (phrase_begin <= indexFinger) { indexFinger = phrase_begin; }
      // find it's cousin
      cousin_phrase_long = retrospective_phrase_cousin_found(
          example_long, example, phrase_long, phrase_begin, comparison_long,
          comparison, &cousin_phrase_begin);

      cousin = cousin_phrase_long > 0 ? truth_WORD : lie_WORD;
      if (cousin == lie_WORD) break;);
  return cousin;
}

word_t /* boolean */ text_compare(struct Text example, struct Text comparison) {
  conditional(example.length != comparison.length, return lie_WORD);
  conditional(example.letters == comparison.letters, return truth_WORD);
  down_repeat(example.length, conditional(comparison.letters[iterator - 1] !=
                                              example.letters[iterator - 1],
                                          return lie_WORD));
  return truth_WORD;
}
uint16_t /*boolean*/ line_compare(const line_t line, const line_t comparison) {
  repeat(LINE_LONG,
         conditional(line[iterator] != comparison[iterator], return lie_WORD));
  return truth_WORD;
}

uint16_t /*boolean*/ tablet_compare(const uint16_t tablet_long,
                                    const line_t tablet[],
                                    const uint16_t comparison_long,
                                    const line_t comparison[]) {
  conditional(tablet_long != comparison_long, return lie_WORD);
  repeat(tablet_long,
         conditional(line_compare(tablet[iterator], comparison[iterator]) ==
                         lie_WORD,
                     return lie_WORD);
  );
  return truth_WORD;
}

/** define is_thePhrase_aQuote fyakna_kyitka_ri*/
word_t fyakna_kyitka_ri(struct Phrase phrase) {
  word_t word = phrase_word_read(phrase, 0);
  conditional((word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE, return TRUTH);
  return LIE;
}

// is_thePhrase_aPerfectIndependentClause
word_t fyakna_hwafhtinka_ri(const struct Phrase input) {
  // Algorithm:
  // use retrospective htin found, using begin == 1
  // if has same being and same end line then is perfect  htin

  conditional((input.begin & LINE_LONG_MASK) > 1, return lie_WORD);
  struct Phrase search = input;
  txik_t max_input_txik = input.begin + input.length;
  search.begin = 1;
  search.length = max_input_txik - search.begin;
  struct Sentence htin = retrospective_htin_found(search);
  conditional(htin.length == 0, return lie_WORD);
  conditional(htin.begin != input.begin, return lie_WORD);
  return truth_WORD;
}
// #define is_thePhrase_aParagraphQuote fyakna_tyafkyitka_ri
word_t fyakna_tyafkyitka_ri(const struct Phrase phrase) {
  struct Phrase verb_phrase = retrospective_verb_phrase_found(phrase);
  //phrase_print(verb_phrase);
  conditional(verb_phrase.length == 0, return lie_WORD);
  //text_phrase_print(verb_phrase);
  word_t perspective_word = phrase_word_read(verb_phrase, 
      verb_phrase.length - 1);
  word_t quote =  0; 
  if (perspective_word == declarative_mood_GRAMMAR) {
    quote = phrase_word_read(verb_phrase, 0);
    //DEBUGPRINT(("%X quote\n", quote));
    switch(quote) {
      example(PARAGRAPH_LETTER_QUOTE, return truth_WORD);
    }
  }
  return lie_WORD;
}

