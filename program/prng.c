/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*SPEL virtual machine
Copyright (C) 2016  Logan Streondj

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact: streondj at gmail dot com
*/
/*  Written in 2015 by Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */

#include "prng.h"

/* This is a fixed-increment version of Java 8's SplittableRandom generator
   See http://dx.doi.org/10.1145/2714064.2660195 and
   http://docs.oracle.com/javase/8/docs/api/java/util/SplittableRandom.html

   It is a very fast generator passing BigCrush, and it can be useful if
   for some reason you absolutely want 64 bits of state; otherwise, we
   rather suggest to use a xoroshiro128+ (for moderately parallel
   computations) or xorshift1024* (for massively parallel computations)
   generator. */

uint64_t splitMix64(uint64_t *seed) {
  uint64_t z = (*seed += 0x9E3779B97F4A7C15);
  z = (z ^ (z >> 30)) * 0xBF58476D1CE4E5B9;
  z = (z ^ (z >> 27)) * 0x94D049BB133111EB;
  return z ^ (z >> 31);
}
uint32_t splitMix32(uint32_t *seed) {
  uint64_t z = (*seed += 0x9E3779B9 );
  z = (z ^ (z >> 15)) * 0xBF58476D ;
  z = (z ^ (z >> 13)) * 0x94D049BB ;
  return z ^ (z >> 16);
}
// big crush
uint64_t xorshift64star(uint64_t state[static 1]) {
	uint64_t x = state[0];
	x ^= (uint64_t)(x >> 12); // a
	x ^= (uint64_t)(x << 25); // b
	x ^= (uint64_t)(x >> 27); // c
	state[0] = x;
	return x * 0x2545F4914F6CDD1D;
}
uint64_t xorshift128plus(uint64_t *s) {
	uint64_t x = s[0];
	uint64_t const y = s[1];
	s[0] = y;
	x ^= (uint64_t)(x << 23); // a
	s[1] = (uint64_t)(x ^ y ^ (x >> 17) ^ (y >> 26)); // b, c
	return s[1] + y;
}
uint64_t seed_random(uint64_t *seed) {
  //guarantee(seed != 0);
  //return (uint64_t) (rand());
  //return splitMix64(seed);
  //return xorshift64star(seed);
  return xorshift128plus(seed);
  //return *seed;
}
void random_seed_establish64(const uint64_t starter, const uint8_t seed_long, uint64_t *seed) {
  uint64_t inner_seed = starter;
  //uint64_t inner_seed =  0;
  uint8_t iteration = 0;
  for (; iteration < seed_long; ++iteration) {
    seed[iteration] += splitMix64(&inner_seed);
  }
}
/* implements universal hashing using random bit-vectors in x */
/* assumes number of elements in x is at least BITS_PER_CODE_NAME *
 * MAX_STRING_SIZE
 */
#define BITS_PER_CODE_NAME (64) /* not true on all machines! */
#define MAX_STRING_SIZE (16)    /* we'll stop hashing after this many */
#define MAX_BITS (BITS_PER_CODE_NAME * MAX_STRING_SIZE)
#define SEED_NUMBER 0x123456789ABCDEF

// hash function from
// http://www.cs.yale.edu/homes/aspnes/pinewiki/C(2f)HashTables.html
uint64_t hash64(const uint8_t array_length, const uint64_t *array) {
  uint64_t hash;
  uint8_t array_indexFinger = 0;
  /*uint8_t bit_indexFinger = 0;*/
  uint64_t random_seed = SEED_NUMBER;
  uint64_t c;
  uint64_t hash_number = 0;
  int shift;

  /* cast s to unsigned const char * */
  /* this ensures that elements of s will be treated as having values >= 0 */

  // printf("array_indexFinger %X\n", array_indexFinger);
  hash = 0;
  for (array_indexFinger = 0; array_indexFinger < array_length;
       ++array_indexFinger) {
    c = array[array_indexFinger];
    for (shift = 0; shift < BITS_PER_CODE_NAME;
         ++shift /*, ++bit_indexFinger*/) {
      /* is low bit of c set? */
      hash_number = splitMix64(&random_seed);
      if (c & 0x1) {
        hash ^= hash_number;
        // printf("hash %lX\n", hash);
      }

      /* shift c to get new bit in lowest position */
      c >>= 1;
    }
  }

  return hash;
}

uint32_t hash32(const uint8_t array_length, const uint64_t *array) {
  uint32_t hash;
  uint8_t array_indexFinger = 0;
  /*uint8_t bit_indexFinger = 0;*/
  uint32_t random_seed = (uint32_t)SEED_NUMBER;
  uint32_t c;
  uint32_t hash_number = 0;
  int shift;

  /* cast s to unsigned const char * */
  /* this ensures that elements of s will be treated as having values >= 0 */

  // printf("array_indexFinger %X\n", array_indexFinger);
  hash = 0;
  for (array_indexFinger = 0; array_indexFinger < array_length;
       ++array_indexFinger) {
    c = array[array_indexFinger];
    for (shift = 0; shift < BITS_PER_CODE_NAME;
         ++shift /*, ++bit_indexFinger*/) {
      /* is low bit of c set? */
      hash_number = splitMix32(&random_seed);
      if (c & 0x1) {
        hash ^= hash_number;
        // printf("hash %lX\n", hash);
      }

      /* shift c to get new bit in lowest position */
      c >>= 1;
    }
  }

  return hash;
}


