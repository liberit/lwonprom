/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*SPEL virtual machine
Copyright (C) 2016  Logan Streondj

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact: streondj at gmail dot com
*/

#include <assert.h> // NOT opencl compatible
#include <stdio.h>  // NOT opencl compatible
#include <string.h> // NOT opencl compatible// uses memset and memcmp

#include "dictionary.h"
#include "pyash.h"
#include "seed.h"
#include "sort.h"
#include "translation.h"

void delete_empty_glyph(const uint16_t ACC_GEN_magnitude, const char *text,
                        uint16_t *DAT_magnitude, char *DAT_text) {
  uint16_t i = 0;
  uint16_t j = 0;
  char glyph;
  assert(ACC_GEN_magnitude <= *DAT_magnitude);
  assert(text != NULL);
  assert(DAT_text != NULL);
  assert(text != DAT_text);
  for (i = 0; i < ACC_GEN_magnitude; ++i) {
    glyph = text[i];
    if (consonant_Q(glyph) == TRUE || vowel_Q(glyph) == TRUE ||
        tone_Q(glyph) == TRUE) {
      DAT_text[j] = glyph;
      ++j;
    } else {
    }
  }
  *DAT_magnitude = j;
}

extern inline void copy_ACC_text_DAT_tablet(const char *text,
                                            const uint8_t text_magnitude,
                                            line_t *tablet,
                                            const uint8_t tablet_offset,
                                            const uint8_t tablet_magnitude) {
  uint8_t text_indexFinger;
  uint8_t tablet_indexFinger = 0;
  assert(tablet_magnitude >= text_magnitude / 2 / LINE_LONG);
  assert(text != NULL);
  assert(tablet != NULL);
  for (text_indexFinger = 0; text_indexFinger < text_magnitude;
       ++text_indexFinger) {
    if (text_magnitude > text_indexFinger + 1) {
      line_t_write(tablet_indexFinger + tablet_offset,
                  (uint16_t)(text[text_indexFinger] |
                             (text[text_indexFinger + 1] << 8)),
                  tablet);
      ++text_indexFinger;
    } else {
      line_t_write(tablet_indexFinger + tablet_offset,
                  (uint16_t)text[text_indexFinger], tablet);
    }
    // printf("tablet %04X \n", (uint) tablet[0][tablet_indexFinger +
    // tablet_offset]);
    ++tablet_indexFinger;
  }
}

/* remember if binary_phrase_list beginning is one
    then is last tablet or only of independentClause
    else if binary_phrase_list begining is zero,
    then is part tablet of independentClause
*/

extern inline void fit_quote_magnitude(const uint8_t quote_magnitude,
                                       uint8_t *quote_tablet_magnitude) {
  assert(quote_tablet_magnitude != NULL);
  assert(quote_magnitude > 0);
  assert(quote_magnitude < 16);
  if (quote_magnitude == 1) {
    *quote_tablet_magnitude = 0x0;
  } else if (quote_magnitude == 2) {
    *quote_tablet_magnitude = 0x1;
  } else if (quote_magnitude > 2 && quote_magnitude <= 4) {
    *quote_tablet_magnitude = 0x2;
  } else if (quote_magnitude > 4 && quote_magnitude <= 8) {
    *quote_tablet_magnitude = 0x4;
  } else if (quote_magnitude > 8 && quote_magnitude <= 16) {
    *quote_tablet_magnitude = 0x8;
  }
}

inline void play(const v4us coded_name, v8us *hook_list) {
  void *accusative = NULL;
  void *instrumental = NULL;
  // void *dative =  NULL;
  assert(((uint16_t *)&coded_name)[VERB_INDEXFINGER] != 0);
  assert(hook_list != NULL);
  // quizing hash key name
  printf("coded_name play %04X%04X%04X%04X\n",
         (uint)((uint16_t *)&coded_name)[3], (uint)((uint16_t *)&coded_name)[2],
         (uint)((uint16_t *)&coded_name)[1],
         (uint)((uint16_t *)&coded_name)[0]);
  // switch (coded_name[ACCUSATIVE_INDEXFINGER]) {
  // case UNSIGNED_CHAR_QUOTED:
  //  accusative = (unsigned char *)&(hook_list[ACCUSATIVE_INDEXFINGER]);
  //  break;
  // case SIGNED_CHAR_QUOTED:
  //  accusative = (char *)&(hook_list[ACCUSATIVE_INDEXFINGER]);
  //  break;
  // case SHORT_NUMBER_QUOTED:
  //  accusative = (uint16_t *)&(hook_list[ACCUSATIVE_INDEXFINGER]);
  //  break;
  // case ERROR_BINARY:
  //  break;
  // default:
  //  printf("unrecognized type %04X",
  //  (uint)coded_name[ACCUSATIVE_INDEXFINGER]);
  //  assert(0 != 0);
  //  break;
  //}
  switch (((uint16_t *)&coded_name)[INSTRUMENTAL_INDEXFINGER]) {
  case UNSIGNED_CHAR_QUOTED:
    instrumental = (unsigned char *)&(hook_list[INSTRUMENTAL_INDEXFINGER]);
    break;
  case SIGNED_CHAR_QUOTED:
    instrumental = (char *)&(hook_list[INSTRUMENTAL_INDEXFINGER]);
    break;
  // case SHORT_NUMBER_QUOTED:
  //  instrumental = (uint16_t *)&(hook_list[INSTRUMENTAL_INDEXFINGER]);
  //  break;
  case ERROR_BINARY:
    break;
  default:
    printf("unrecognized type %04X",
           (uint)((uint16_t *)&coded_name)[ACCUSATIVE_INDEXFINGER]);
    assert(0 != 0);
    break;
  }
  switch (*((uint64_t *)&coded_name)) {
  case 0x6048009D00000000: /* say unsigned char* */
    x6048009D00000000((unsigned char *)accusative);
    break;
  case 0x6048029D00000000: /* say signed char* */
    x6048029D00000000((signed char *)accusative);
    break;
  case 0x4124000000000000: /* equal */
    x4124000000000000((v8us *)hook_list);
    break;
  case 0x8006000000000000: /* different */
    x8006000000000000((v8us *)hook_list);
    break;
  case 0xA130143D143D0000: /* not CCNOT */
    xA130143D143D0000((uint16_t *)accusative, (uint16_t *)instrumental);
    break;
  case 0xC450143D143D0000: /* not CCNOT */
    xC450143D143D0000((uint16_t *)accusative, (uint16_t *)instrumental);
    break;
  case 0x8006143D143D0000: /* not CCNOT */
    x8006143D143D0000((uint16_t *)accusative, (uint16_t *)instrumental);
    break;
  default:
    printf(
        "unrecognized coded_name %04X%04X%04X%04X\n",
        (uint)((uint16_t *)&coded_name)[3], (uint)((uint16_t *)&coded_name)[2],
        (uint)((uint16_t *)&coded_name)[1], (uint)((uint16_t *)&coded_name)[0]);
    assert(0 != 0);
    break;
  }
}
extern inline void play_quote(const line_t *tablet,
                              const uint8_t tablet_indexFinger,
                              const uint8_t tablet_magnitude,
                              uint16_t *quote_word, v8us *quote_fill) {
  uint16_t word;
  uint8_t quote_indexFinger = 0;
  uint8_t quote_magnitude;
  assert(tablet != NULL);
  assert(tablet_indexFinger < LINE_LONG);
  assert(tablet_magnitude < MAX_INDEPENDENTCLAUSE_PAGE);
  assert(quote_word != NULL);
  assert(quote_fill != NULL);
  word = line_t_read(tablet_indexFinger, *tablet);
  // printf("quote quizing, word %04X\n", (uint) (*tablet)[tablet_indexFinger]);
  if ((word & CONSONANT_ONE_MASK) == QUOTED_DENOTE) {
    // then is quote
    *quote_word = word;
    // printf("quote detected %04X\n", (uint)word);
    quote_magnitude = (uint8_t)(
        1 << (((*quote_word >> CONSONANT_ONE_THICK) & 7 /* 3 bit mask */) - 1));
    // printf("quote_magnitude %X \n", (uint)quote_magnitude);
    // printf("tablet_indexFinger %X \n", (uint)tablet_indexFinger);
    assert(quote_magnitude < tablet_magnitude * LINE_LONG * WORD_THICK);
    // printf("quote_fill ");
    // if (quote_magnitude == 0) {
    //  ((uint16_t *)quote_fill)[0] = (uint16_t)(word >> QUOTED_LITERAL_BEGIN);
    //  // printf("%04X ", (uint)(*quote_fill)[0]);
    //}
    for (quote_indexFinger = 0; quote_indexFinger < quote_magnitude;
         ++quote_indexFinger) {
      ((uint16_t *)quote_fill)[quote_indexFinger] =
          line_t_read(tablet_indexFinger + quote_indexFinger + 1, *tablet);
      // printf("%04X ", (uint)(*quote_fill)[quote_indexFinger]);
    }
    // printf("\n");
  }
}
// void burden_hook_list(const uint8_t tablet_magnitude, const line_t *tablet,
//                       uint8_t *tablet_indexFinger, v4us *coded_name,
//                       v8us *hook_list) {
//   assert(tablet_magnitude != 0);
//   assert(tablet != NULL);
//   assert(coded_name != NULL);
//   assert(hook_list != NULL);
//   assert(tablet_indexFinger != NULL);
//   assert(*tablet_indexFinger >= 1);
//   uint16_t indicator_list = 0;
//   uint8_t indicator = 0;
//   uint8_t tablet_number = 0;
//   uint8_t exit = FALSE;
//   uint16_t word = 0;
//   uint16_t quote_word = 0;
//   v8us quote_fill = {0};
//   indicator_list = line_t_read(0, *tablet);
//   indicator = (uint8_t)1 & indicator_list;
//   // printf("indicator %X\n", (uint) indicator);
//   // printf("indicator_list %X\n", (uint) indicator_list);
//   for (tablet_number = 0; tablet_number < tablet_magnitude; ++tablet_number) {
//     for (; *tablet_indexFinger < LINE_LONG; ++*tablet_indexFinger) {
//       // printf("BHL tablet_indexFinger %X\n", (uint)*tablet_indexFinger);
//       // if previous is indicated then quiz if is quote
//       if (((indicator_list & (1 << (*tablet_indexFinger - 1))) >>
//            (*tablet_indexFinger - 1)) == indicator) {
//         // printf("quote's word %X \n", (uint)tablet[0][*tablet_indexFinger]);
//         play_quote(tablet, *tablet_indexFinger, tablet_magnitude, &quote_word,
//                    &quote_fill);
//       }
//       // if current is indicated then quiz if is case or
//       // verb
//       if (((indicator_list & (1 << *tablet_indexFinger)) >>
//            *tablet_indexFinger) == indicator) {
//         word = ((uint16_t **)&tablet)[tablet_number][*tablet_indexFinger];
//         // printf("BHL word %X\n", (uint)word);
//         switch (word) {
//         case accusative_case_GRAMMAR:
//           // printf("detected accusative case\n");
//           if (quote_word != 0) {
//             ((uint16_t *)coded_name)[ACCUSATIVE_INDEXFINGER] = quote_word;
//             // printf("coded_name ACC %04X%04X%04X%04X\n",
//             //       (uint)(*coded_name)[3], (uint)(*coded_name)[2],
//             //       (uint)(*coded_name)[1], (uint)(*coded_name)[0]);
//             hook_list[ACCUSATIVE_INDEXFINGER] = quote_fill;
//             // printf("ACC quote_fill %X\n", (uint)quote_fill[0]);
//             // printf("ACC hook_list %X\n",
//             // (uint)hook_list[ACCUSATIVE_INDEXFINGER][0]);
//             quote_word = 0;
//           }
//           break;
//         case dative_case_GRAMMAR:
//           if (quote_word != 0) {
//             ((uint16_t *)coded_name)[DATIVE_INDEXFINGER] = quote_word;
//             hook_list[DATIVE_INDEXFINGER] = quote_fill;
//             quote_word = 0;
//           }
//           break;
//         case instrumental_case_GRAMMAR:
//           if (quote_word != 0) {
//             ((uint16_t *)coded_name)[INSTRUMENTAL_INDEXFINGER] = quote_word;
//             hook_list[INSTRUMENTAL_INDEXFINGER] = quote_fill;
//             quote_word = 0;
//           }
//           break;
//         default:
//           exit = TRUE;
//           break;
//         }
//       }
//       if (exit == TRUE)
//         break;
//     }
//     if (exit == TRUE)
//       break;
//   }
// }

// inline void play_independentClause(const uint8_t tablet_magnitude,
//                                    const line_t *tablet, v4us *coded_name,
//                                    v8us *hook_list) {
//   /* go through coded independentClause,
//       loading quotes into temporary register,
//       append to case list,
//       when get to case, move to appropriate case register,
//       add to case counter, and append to case list,
//       when get to verb,
//       match to available functions by number of cases,
//       match to available functions by case list,
//       make 64bit hash key, ACC DAT INS verb,
//       with appropriate quotes filling in place of ACC DAT INS
//       or a 0 if there is none.
//       execute proper function.
//   */
//   uint16_t indicator_list = 0;
//   uint8_t indicator = 0;
//   uint8_t tablet_number = 0;
//   uint8_t tablet_indexFinger = 1;
//   uint8_t exit = FALSE;
//   uint16_t word = 0;
//   assert(tablet_magnitude != 0);
//   assert(tablet != NULL);
//   assert(coded_name != NULL);
//   assert(hook_list != NULL);
// #ifdef ARRAYLINE
//   indicator_list = (uint16_t)(*tablet)[0];
// #else
//   indicator_list = (uint16_t)(*tablet).s0;
// #endif
//   indicator = (uint8_t)1 & indicator_list;
//   // printf("indicator %X\n", (uint) indicator);
//   // printf("indicator_list %X\n", (uint) indicator_list);
//   burden_hook_list(tablet_magnitude, tablet, &tablet_indexFinger, coded_name,
//                    hook_list);
//   // printf("coded_name burden %04X%04X%04X%04X\n", (uint)(*coded_name)[3],
//   //       (uint)(*coded_name)[2], (uint)(*coded_name)[1],
//   //       (uint)(*coded_name)[0]);
//   for (tablet_number = 0; tablet_number < tablet_magnitude; ++tablet_number) {
//     for (; tablet_indexFinger < LINE_LONG; ++tablet_indexFinger) {
//       // if current is indicated then quiz if is case or
//       // verb
//       if (((indicator_list >> tablet_indexFinger) & (1)) == indicator) {
//         word = ((uint16_t **)&tablet)[tablet_number][tablet_indexFinger];
//         // printf("word %X\n", (uint)word);
//         switch (word) {
//         case conditional_mood_GRAMMAR:
//           word = ((uint16_t **)&tablet)[tablet_number][tablet_indexFinger - 1];
//           // printf("COND word %04X \n", (uint) word);
//           ((uint16_t *)coded_name)[VERB_INDEXFINGER] = word;
//           // printf("coded_name COND %04X%04X%04X%04X\n",
//           //       (uint)(*coded_name)[3], (uint)(*coded_name)[2],
//           //       (uint)(*coded_name)[1], (uint)(*coded_name)[0]);
//           play(*coded_name, hook_list);
//           // if dative is error_WORD then skip to next independentClause
//           if (((uint16_t **)&hook_list)[DATIVE_INDEXFINGER][0] == error_WORD) {
//             exit = TRUE;
//           } else {
//             ++tablet_indexFinger;
//             burden_hook_list(tablet_magnitude, tablet, &tablet_indexFinger,
//                              coded_name, hook_list);
//             printf("coded_name burden2 %04X%04X%04X%04X\n",
//                    (uint)((uint16_t *)&coded_name)[3],
//                    (uint)((uint16_t *)&coded_name)[2],
//                    (uint)((uint16_t *)&coded_name)[1],
//                    (uint)((uint16_t *)&coded_name)[0]);
//             printf("tablet_indexFinger %X\n", (uint)tablet_indexFinger);
//             --tablet_indexFinger;
//           }
//           break;
//         case deontic_mood_GRAMMAR:
//           // quizing verb
//           word = line_t_read(tablet_indexFinger - 1, tablet[tablet_number]);
//           ((uint16_t *)&coded_name)[VERB_INDEXFINGER] = word;
//           // printf("coded_name DEO %04X%04X%04X%04X\n",
//           //       (uint)(*coded_name)[3], (uint)(*coded_name)[2],
//           //       (uint)(*coded_name)[1], (uint)(*coded_name)[0]);
//           // printf("realizing");
//           play((*coded_name), hook_list);
//           exit = TRUE;
//           break;
//         case realis_mood_GRAMMAR:
//           // quizing verb
//           word = line_t_read(tablet_indexFinger - 1, tablet[tablet_number]);
//           ((uint16_t *)coded_name)[VERB_INDEXFINGER] = word;
//           printf("coded_name REAL %04X%04X%04X%04X\n",
//                  (uint)((uint16_t *)&coded_name)[3],
//                  (uint)((uint16_t *)&coded_name)[2],
//                  (uint)((uint16_t *)&coded_name)[1],
//                  (uint)((uint16_t *)&coded_name)[0]);
//           printf("realizing");
//           play((*coded_name), hook_list);
//           exit = TRUE;
//           break;
//         case ERROR_BINARY:
//           assert(ERROR_BINARY != ERROR_BINARY);
//           break;
//         default:
//           // printf("tablet_indexFinger %X\n", (uint)tablet_indexFinger);
//           // assert(1 == 0); // indicated wrong point
//           break;
//         }
//       }
//       if (exit == TRUE)
//         break;
//     }
//     if (indicator == 1 || exit == TRUE)
//       break;
//   }
//   assert(indicator == 1); /* must finish properly */
//                           // quizing grammtical-case list
//   // printf("\n");
// }

inline void play_text(const uint16_t max_tablet_magnitude, const line_t *tablet,
                      v4us *coded_name, v8us *hook_list) {
  /*
    identify independentClause tablet,
    then pass to independentClause_play,
    and so on until reach end.
  */
  uint16_t tablet_indexFinger = 0;
  assert(tablet != NULL);
  assert(max_tablet_magnitude > 0);
  assert(coded_name != NULL);
  assert(hook_list != NULL);
  for (; tablet_indexFinger < max_tablet_magnitude; ++tablet_indexFinger) {
    play_independentClause((uint8_t)(max_tablet_magnitude - tablet_indexFinger),
                           &tablet[tablet_indexFinger], coded_name, hook_list);
  }
}

uint16_t
grammaticalCase_code_word_translate(const uint16_t grammaticalCase_code) {
  switch (grammaticalCase_code) {
  case 0:
    return nominative_case_GRAMMAR;
    break;
  case 0xB:
    return topic_case_GRAMMAR;
    break;
  default:
    assert(1 == 0); // wrong grammaticalCase_code
    return 0;
  }
}

void derive_filename(const uint16_t filename_long, const char *filename,
                     const uint16_t file_sort, uint16_t *gross_filename_long,
                     char *gross_filename) {
  assert(gross_filename_long != NULL);
  assert(gross_filename != NULL);
  assert(*gross_filename_long >= filename_long);
  memcpy(gross_filename, filename, filename_long);
  switch (file_sort) {
  case cardinal_WORD:
    memcpy(gross_filename + filename_long, ".c", 2);
    break;
  default:
    break;
  }
  *gross_filename_long = (uint16_t)strlen(gross_filename);
}
