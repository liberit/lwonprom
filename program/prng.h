/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*SPEL virtual machine
Copyright (C) 2016  Logan Streondj

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

contact: streondj at gmail dot com
*/
#ifndef PRNG_H
#define PRNG_H
#include "sort.h"
//#ifndef uint64_t
//#define uint64_t unsigned long long
//#define uint32_t unsigned int
//#endif
//#ifndef uint8_t
//#define uint8_t unsigned char
//#endif
//#ifndef uint16_t
//#define uint16_t unsigned short
//#endif
uint64_t seed_random(uint64_t *seed);
uint64_t splitMix64(uint64_t *seed);
uint32_t splitMix32(uint32_t *seed);
void random_seed_establish64(const uint64_t starter, const uint8_t seed_long,
                             uint64_t *seed);
void random_seed_establish32(const uint32_t starter, const uint8_t seed_long,
                             uint32_t *seed);
#define random_seed_establish random_seed_establish64
#define random_t uint64_t
uint64_t hash64(const uint8_t array_length, const uint64_t *array);
uint32_t hash32(const uint8_t array_length, const uint64_t *array);
#define hash hash32
#endif
