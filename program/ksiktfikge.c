/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "ksiktfikge.h"
//#ifdef EMSCRIPTEN
//#include "strcasecmp.c"
//#endif

ulong letter_fall(const ulong text_long, char *text) {
  text[text_long - 1] = 0;
  return text_long - 1;
}
ulong return_letter_delete(const ulong text_long, char *text) {
  const ulong finally_places = text_long - 1;
  const char finally_letter = text[finally_places];
  return finally_letter == '\n' ? letter_fall(text_long, text) : text_long;
}

#define TEXT_LONG 80
#define TEXT_PAD_LONG 0x10

uint16_t line_process(struct Paragraph program,
    const struct Text input, 
    const uint16_t produce_text_long, char produce_text[]) {
  uint16_t produce_long = 0;

  //printf(_("%slaka nrufli\n"), line);
  //printf(_("htetlyanna zihnuc_%0X_hnuczika\n"), line_long);
  produce_long = pyash_to_pyash_interpret(program,
      input, fluent_WORD, produce_text_long, produce_text);
  printf("%s\n", produce_text);
  linenoiseHistoryAdd(input.letters);           /* Add to the history. */
  linenoiseHistorySave("history.txt"); /* Save the history on disk. */
  return produce_long;
}

void reflect(struct Paragraph program) {

  uint16_t produce_text_long = TEXT_LONG;
  uint16_t text_long = TEXT_LONG;
  char produce_text[TEXT_LONG] = {0};
  char *line = linenoise("pyachbyah ");

  conditional(line == NULL, return);
  text_long = (uint16_t)stringlen(line, TEXT_LONG);
  if (line[0] != '\0' && line[0] != '/') {
    struct Text input;
    input.length = text_long;
    input.letters = line;
    line_process(program, input, produce_text_long, produce_text);
  }
  free(line);
}

void completion(const char *buf, linenoiseCompletions *lc) {
  if (buf[0] == 'h') {
    linenoiseAddCompletion(lc, "hello");
    linenoiseAddCompletion(lc, "hello there");
  }
}

//char *hints(const char *buf, int *color, int *bold) {
//  if (!strcasecmp(buf, "hello")) {
//    *color = 35;
//    *bold = 0;
//    return " World";
//  }
//  return NULL;
//}

int main(int argc, char **argv) {
//int main() {
  //char text_pad[TEXT_PAD_LONG][TEXT_LONG] = {{0}};
  //uint16_t text_metadata[2] = {0};
  // char *prgname = argv[0];

  ///* Parse options, with --multiline we enable multi line editing. */
  // Load file argument and turn it into program
  char * filename;
  uint16_t file_long = 0x1000;
  char file[0x1000] = {0};
  NewPagePhrase(program, 100);
  uint16_t text_remainder = 0;
  uint16_t program_finally = 0;


  /* Set the completion callback. This will be called every time the
   * user uses the <tab> key. */
  linenoiseSetCompletionCallback(completion);
  //linenoiseSetHintsCallback(hints);

  /* Load history from file. The history file is just a plain text file
   * where entries are separated by newlines. */
  linenoiseHistoryLoad("history.txt"); /* Load the history at startup */

  repeat(5, reflect(program));
  return (0);
}
