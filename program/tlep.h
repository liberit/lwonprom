/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
// file for repeatedly (frak) macro
#ifndef FRAK_H
#define FRAK_H
#define repeat(count, function)                                                \
  do {                                                                         \
    for (ulong iterator = 0; iterator < count; ++iterator) {                   \
      function;                                                                \
    }                                                                          \
  } while (0)
#define down_repeat(_count, function)                                          \
  do {                                                                         \
    const ulong count = (ulong)_count;                                         \
    ulong prevIterator = count + 1;                                            \
    for (ulong iterator = count; iterator > 0 && iterator <= count;            \
         --iterator) {                                                         \
      conditional(!(iterator < prevIterator),                                  \
                  DEBUGPRINT(("%lx iterator, %lx prevIterator\n", iterator,    \
                              prevIterator)));                                 \
      guarantee(iterator < prevIterator);                                      \
      prevIterator = iterator;                                                 \
      function;                                                                \
    }                                                                          \
  } while (0)

#define down_to_zero_repeat(_count, function)                                  \
  do {                                                                         \
    const ulong count = (ulong)_count;                                         \
    ulong prevIterator = count + 1;                                            \
    for (ulong iterator = count; iterator <= count; --iterator) {              \
      conditional(!(iterator < prevIterator),                                  \
                  DEBUGPRINT(("%lx iterator, %lx prevIterator\n", iterator,    \
                              prevIterator)));                                 \
      guarantee(iterator < prevIterator);                                      \
      prevIterator = iterator;                                                 \
      function;                                                                \
    }                                                                          \
  } while (0)
// unsigned long Max_Iterator;
#define down_repeat_with(iterator, function)                                   \
  do {                                                                         \
    const unsigned long Max_Iterator = (unsigned long)iterator;                \
    unsigned long prevIterator = Max_Iterator + 1;                             \
    for (; iterator > 0 && iterator <= Max_Iterator; --iterator) {             \
      guarantee(iterator < prevIterator);                                      \
      prevIterator = iterator;                                                 \
      function;                                                                \
    }                                                                          \
  } while (0)

#define down_repeat_except(count, except, function)                            \
  for (ulong iterator = count; iterator > 0 && iterator <= count;              \
       --iterator) {                                                           \
    function;                                                                  \
    if (except)                                                                \
      break;                                                                   \
  }

#define repeat_from_until(from, until, function)                               \
  for (ulong iterator = from; iterator < until; ++iterator) {                  \
    function;                                                                  \
  }

#define repeat_except(count, except, function)                                 \
  for (ulong iterator = 0; iterator < count; ++iterator) {                     \
    function;                                                                  \
    if (except)                                                                \
      break;                                                                   \
  }

#define repeat_with_except(count, iterator, except, function)                  \
  for (; iterator < count; ++iterator) {                                       \
    function;                                                                  \
    if (except)                                                                \
      break;                                                                   \
  }

#define down_repeat_with_except(iterator, except, function)                    \
  for (; iterator > 0; --iterator) {                                           \
    function;                                                                  \
    if (except)                                                                \
      break;                                                                   \
  }

#define repeat_with_until(iterator, until, function)                           \
  for (; iterator < until; ++iterator) {                                       \
    function;                                                                  \
  }

#define repeat_with(count, iterator, function)                                 \
  for (; iterator < count; ++iterator) {                                       \
    function;                                                                  \
  }

#define extradition_dose_from_to(dose, from, to)                               \
  __typeof__(dose) _dose = dose;                                               \
  from -= _dose;                                                               \
  to += _dose;

#define example(example, function)                                             \
  case example:                                                                \
    function;                                                                  \
    break;

#define conditional(condition, function)                                       \
  if (condition) {                                                             \
    function;                                                                  \
  }
#endif
