/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*
   Pyash is a  human computer programming langauge based on linguistic universals.
      Copyright (C) 2020  Andrii Logan Zvorygin

      This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

      You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PYASH_H
#define PYASH_H

#include "pyashWords.h"
#include "sort.h"
#include "parser.h"
#include "tlep.h"
#ifndef OPENCL
#include <stdint.h>
#define constant const
#endif

#define NUMBER_WORD_SEQUENCE_LONG 21
#define VECTOR_THICK_BEGIN 0x8
#define VECTOR_THICK_MASK 0x700
#define VECTOR_THICK_1 0
#define VECTOR_THICK_2 1
#define VECTOR_THICK_4 2
#define VECTOR_THICK_8 3
#define VECTOR_THICK_16 4
#define VECTOR_THICK_LONG 6
#define VECTOR_THICK_3 7
#define HOLLOW_SUMMARY 1
#define NUMBER_TIDBIT_LONG 0x10 // uint16_t
#define MAXIMUM_VECTOR_CODE 7
#define DEFAULT_NUMBER_BASE 0x10

#define MAXIMUM_WORD_SEQUENCE_LONG 0x10
#define WORD_THICK 2
#define GLOTTAL_STOP 0xC
#define MAX_LONG 0xFFU
#define INDEPENDENTCLAUSE_LONG 0xFF
#define WORD_LONG 0x05
#define CODE_WORD_TIDBIT_LONG 0x10
#define ERROR_BINARY 0
#define LONG_ROOT 1
#define SHORT_ROOT 2
#define LONG_GRAMMAR 3
#define SHORT_GRAMMAR 4
#define SHORT_ROOT_DENOTE 0
#define QUOTE_DENOTE 0x1D
#define QUOTE_DENOTE_MASK 0x1F
#define SHORT_GRAMMAR_DENOTE 0x1E
#define SHORT_GRAMMAR_DENOTE_MASK 0x1F
#define SHORT_SORT_MASK 0x7
#define LONG_SORT_MASK 0x1F
#define SHORT_ROOT_CONSONANT_ONE_MASK 0xF8
#define SHORT_ROOT_CONSONANT_ONE_BEGIN 0x3
#define SHORT_ROOT_VOWEL_BEGIN 0x8
#define SHORT_ROOT_VOWEL_MASK 0x700
#define SHORT_ROOT_TONE_BEGIN 11
#define SHORT_ROOT_TONE_MASK 0x1800
#define SHORT_ROOT_CONSONANT_THREE_BEGIN 13
#define SHORT_ROOT_CONSONANT_THREE_MASK 0xE000
#define LONG_GRAMMAR_DENOTE 7
#define LONG_GRAMMAR_DENOTE_MASK 0x7
#define LONG_GRAMMAR_CONSONANT_ONE_MASK 0xF8
#define LONG_GRAMMAR_CONSONANT_ONE_BEGIN 0x3
#define LONG_GRAMMAR_CONSONANT_TWO_MASK 0x700
#define LONG_GRAMMAR_CONSONANT_TWO_BEGIN 0x8
#define LONG_GRAMMAR_VOWEL_BEGIN 0xB
#define LONG_GRAMMAR_VOWEL_MASK 0x3800
#define LONG_GRAMMAR_TONE_BEGIN 0xE
#define LONG_GRAMMAR_TONE_MASK 0xC000
#define SHORT_GRAMMAR_CONSONANT_ONE_BEGIN 0x5
#define SHORT_GRAMMAR_CONSONANT_ONE_MASK 0x3E0
#define SHORT_GRAMMAR_VOWEL_BEGIN 0xA
#define SHORT_GRAMMAR_VOWEL_MASK 0x1C00
#define SHORT_GRAMMAR_TONE_BEGIN 0xD
#define SHORT_GRAMMAR_TONE_MASK 0x6000
#define LONG_ROOT_CONSONANT_ONE_BEGIN 0x0
#define LONG_ROOT_CONSONANT_ONE_MASK 0x1F
#define LONG_ROOT_CONSONANT_ONE_THICK 0x05
#define LONG_ROOT_CONSONANT_TWO_BEGIN 0x5
#define LONG_ROOT_CONSONANT_TWO_MASK 0xE0
#define LONG_ROOT_VOWEL_BEGIN 0x8
#define LONG_ROOT_VOWEL_MASK 0x700
#define LONG_ROOT_TONE_BEGIN 11
#define LONG_ROOT_TONE_MASK 0x1800
#define LONG_ROOT_CONSONANT_THREE_BEGIN 13
#define LONG_ROOT_CONSONANT_THREE_MASK 0xE000
#define CODE_SORT_MASK 0x7
#define BANNER_THICK 3
#define CONSONANT_TWO_THICK 3
#define VOWEL_THICK 3
#define TONE_THICK 2
#define DICTIONARY_DATABASE_LONG 0x50000
#define DICTIONARY_LINE_LONG MAXIMUM_FOREIGN_WORD_LONG + CODE_LONG
#define CODE_NAME_WORD_LONG 0x4
#define PAGE_WORD_LONG 0xF
#define MAX_INDEPENDENTCLAUSE_PAGE 0x4
#define QUOTED_DENOTE 0x1D
#define SINGLE_BYTE_QUOTED 0x0
#define TWO_BYTE_QUOTED 0x1
#define FOUR_BYTE_QUOTED 0x2
#define EIGHT_BYTE_QUOTED 0x3
#define SIXTEEN_BYTE_QUOTED 0x4
#define SILENCE_GLYPH '.'
#define SILENCE_GLYPH_LONG 1

#define NUMBER_QUOTE 0x881D
#define LETTER_QUOTE 0x081D
#define PARAGRAPH_LETTER_QUOTE 0x0E1D

// quote sorts
#define NAME_LITERAL_BEGIN 0x5
#define NAME_LITERAL_MASK 0x0020
#define NAME_NAME_LITERAL 0x1
#define LITERAL_NAME_LITERAL 0x0

#define POINTED_BEGIN 0x6
#define POINTED_MASK 0x40
#define BASE_POINTED 0x0
#define POINTED_POINTED 0x1

#define SEQUENCE_BEGIN 0x7
#define SEQUENCE_MASK 0x80
#define BASE_SEQUENCE 0x0
#define SEQUENCE_SEQUENCE 0x1

// quote sorts
#define SORT_DENOTE_BEGIN 0xD
#define SORT_DENOTE_MASK 0xE000
#define LETTER_SORT_DENOTE 0
#define WORD_SORT_DENOTE 1
#define SHORT_TERM_SORT_DENOTE 2
//#define INDEPENDENT_CLAUSE_SORT_DENOTE 2
#define BINARY_SORT_DENOTE 3
#define UINT_SORT_DENOTE 4
#define INT_SORT_DENOTE 5
#define FLOAT_SORT_DENOTE 6
#define CEREMONY_SORT_DENOTE 6

// scalar thick
#define SCALAR_THICK_BEGIN 0xB
#define SCALAR_THICK_MASK 0x1800
#define EIGHT_TIDBIT_SCALAR_THICK 0
#define SIXTEEN_TIDBIT_SCALAR_THICK 1
#define THIRTY_TWO_TIDBIT_SCALAR_THICK 2
#define SIXTY_FOUR_TIDBIT_SCALAR_THICK 3

#define POSTURE_TIDBIT 14
#define SCENE_TIDBIT 11
#define NAME_TIDBIT 5
#define REGION_TIDBIT 6
#define VECTOR_THICK_TIDBIT 8
#define SCALAR_THICK_TIDBIT 11
#define SORT_DENOTE_TIDBIT 13
#define SOURCE_CASE 0
#define WAY_CASE 1
#define DESTINATION_CASE 2
#define LOCATION_CASE 3
#define BASE_CONTEXT 0
#define SPACE_CONTEXT 1
#define GENITIVE_CONTEXT 2
#define DISCOURSE_CONTEXT 3
#define SOCIAL_CONTEXT 4
#define SURFACE_CONTEXT 5
#define INTERIOR_CONTEXT 6
#define TIME_CONTEXT 7

#define CODE_LONG 2
#define SPACE_LETTER 0x20
#define TAB_SIZE 1
#define MAXIMUM_FOREIGN_WORD_LONG 0x1E

#define QUOTED_WORD_LONG 2
#define MAX_GRAMMATICALCASE_INE_INDEPENDENTCLAUSE 8
#define HOOK_LIST_LONG 3
#define HOOK_LIST_THICK 8
#define BINARY_PHRASE_LIST_LENGTH 0x10
#define VERB_INDEXFINGER 3
#define ACCUSATIVE_INDEXFINGER 2
#define INSTRUMENTAL_INDEXFINGER 1
#define DATIVE_INDEXFINGER 0

#define TEXT_CLASS 0x0
#define NUMBER_CLASS 0x1
#define INTEGER_CLASS 0x2
#define FLOAT_CLASS 0x3
#define RATIONAL_CLASS 0x4
#define COMPLEX_CLASS 0x5

#define UNSIGNED_CHAR_QUOTED 0x009D
#define SIGNED_CHAR_QUOTED 0x029D
//#define SHORT_NUMBER_QUOTED 0x143D
//

#define MAX_KNOWLEDGE_LONG 0xFF
#define GRAMMATICALCASE_WORD_SEQUENCE_LONG 59
#ifndef OPENCL
extern constant uint16_t grammaticalCase_word_sequence[GRAMMATICALCASE_WORD_SEQUENCE_LONG];
#else
extern constant uint16_t grammaticalCase_word_sequence[] = {
    nominative_case_GRAMMAR,
    accusative_case_GRAMMAR,
    topic_case_GRAMMAR,
    vocative_case_GRAMMAR,
    genitive_case_GRAMMAR,
    destination_case_GRAMMAR,
    dative_case_GRAMMAR,
    instrumental_case_GRAMMAR,
    comitative_case_GRAMMAR,
    benefactive_case_GRAMMAR,
    essive_case_GRAMMAR,
    adpositional_case_GRAMMAR,
    location_case_GRAMMAR,
    way_case_GRAMMAR,
    source_case_GRAMMAR,
    ablative_case_GRAMMAR,
    elative_case_GRAMMAR,
    illative_case_GRAMMAR,
    temporal_case_GRAMMAR,
    perlative_case_GRAMMAR,
    delative_case_GRAMMAR,
    causal_case_GRAMMAR,
    terminative_case_GRAMMAR,
    inessive_case_GRAMMAR,
    allative_case_GRAMMAR,
    vialis_case_GRAMMAR,
    quotative_case_GRAMMAR,
    distributive_case_GRAMMAR,
    evidential_case_GRAMMAR,
    multiplicative_case_GRAMMAR,
    initiative_case_GRAMMAR,
    sublative_case_GRAMMAR,
    exessive_case_GRAMMAR,
    pegative_case_GRAMMAR,
    prosecutive_case_GRAMMAR,
    superessive_case_GRAMMAR,
    partitive_case_GRAMMAR,
    prolative_case_GRAMMAR,
    modal_case_GRAMMAR,
    centric_case_GRAMMAR,
    oblique_case_GRAMMAR,
    patient_case_GRAMMAR,
    lative_case_GRAMMAR,
    locative_case_GRAMMAR,
    abessive_case_GRAMMAR,
    adessive_case_GRAMMAR,
    agentive_case_GRAMMAR,
    ergative_case_GRAMMAR,
    evitative_case_GRAMMAR,
    subessive_case_GRAMMAR,
    possessed_case_GRAMMAR,
    antessive_case_GRAMMAR,
    adverbial_case_GRAMMAR,
    exocentric_case_GRAMMAR,
    absolutive_case_GRAMMAR,
    associative_case_GRAMMAR,
    superlative_case_GRAMMAR,
    postpositional_case_GRAMMAR,
    locative_directional_case_GRAMMAR};
#endif

#define PERSPECTIVE_WORD_SEQUENCE_LONG 36
#ifndef OPENCL
extern constant uint16_t perspective_word_sequence[PERSPECTIVE_WORD_SEQUENCE_LONG];
#else
extern constant uint16_t perspective_word_sequence[] = {realis_mood_GRAMMAR,
                                               deontic_mood_GRAMMAR,
                                               finally_GRAMMAR, // technically is pilcrow
                                               conditional_mood_GRAMMAR,
                                               epistemic_mood_GRAMMAR,
                                               interrogative_mood_GRAMMAR,
                                               directive_mood_GRAMMAR,
                                               hortative_mood_GRAMMAR,
                                               volitive_mood_GRAMMAR,
                                               deliberative_mood_GRAMMAR,
                                               desiderative_mood_GRAMMAR,
                                               imperative_mood_GRAMMAR,
                                               optative_mood_GRAMMAR,
                                               potential_mood_GRAMMAR,
                                               dubitative_mood_GRAMMAR,
                                               precative_mood_GRAMMAR,
                                               jussive_mood_GRAMMAR,
                                               permissive_mood_GRAMMAR,
                                               commissive_mood_GRAMMAR,
                                               eventive_mood_GRAMMAR,
                                               speculative_mood_GRAMMAR,
                                               benedictive_mood_GRAMMAR,
                                               inductive_mood_GRAMMAR,
                                               admonitive_mood_GRAMMAR,
                                               apprehensive_mood_GRAMMAR,
                                               imprecative_mood_GRAMMAR,
                                               assumptive_mood_GRAMMAR,
                                               prohibitive_mood_GRAMMAR,
                                               declarative_mood_GRAMMAR,
                                               affirmative_mood_GRAMMAR,
                                               irrealis_mood_GRAMMAR,
                                               sensory_evidential_mood_GRAMMAR,
                                               gnomic_mood_GRAMMAR,
                                               propositive_mood_GRAMMAR,
                                               necessitative_mood_GRAMMAR,
                                               paragraph_GRAMMAR};
#endif

#define INDEPENDENTCLAUSE_PERSPECTIVE_WORD_SEQUENCE_LONG 34
#ifndef OPENCL
extern constant uint16_t independentClause_perspective_word_sequence[INDEPENDENTCLAUSE_PERSPECTIVE_WORD_SEQUENCE_LONG];
#else
extern constant uint16_t independentClause_perspective_word_sequence[] = {realis_mood_GRAMMAR,
                                               deontic_mood_GRAMMAR,
                                               finally_GRAMMAR, // technically is pilcrow
                                               epistemic_mood_GRAMMAR,
                                               interrogative_mood_GRAMMAR,
                                               directive_mood_GRAMMAR,
                                               hortative_mood_GRAMMAR,
                                               volitive_mood_GRAMMAR,
                                               deliberative_mood_GRAMMAR,
                                               desiderative_mood_GRAMMAR,
                                               imperative_mood_GRAMMAR,
                                               optative_mood_GRAMMAR,
                                               potential_mood_GRAMMAR,
                                               dubitative_mood_GRAMMAR,
                                               precative_mood_GRAMMAR,
                                               jussive_mood_GRAMMAR,
                                               permissive_mood_GRAMMAR,
                                               commissive_mood_GRAMMAR,
                                               eventive_mood_GRAMMAR,
                                               speculative_mood_GRAMMAR,
                                               benedictive_mood_GRAMMAR,
                                               inductive_mood_GRAMMAR,
                                               admonitive_mood_GRAMMAR,
                                               apprehensive_mood_GRAMMAR,
                                               imprecative_mood_GRAMMAR,
                                               assumptive_mood_GRAMMAR,
                                               prohibitive_mood_GRAMMAR,
                                               declarative_mood_GRAMMAR,
                                               affirmative_mood_GRAMMAR,
                                               irrealis_mood_GRAMMAR,
                                               sensory_evidential_mood_GRAMMAR,
                                               gnomic_mood_GRAMMAR,
                                               propositive_mood_GRAMMAR,
                                               necessitative_mood_GRAMMAR};
#endif

//#ifndef OPENCL
//constant uint16_t number_word_sequence[NUMBER_WORD_SEQUENCE_LONG];
//#else
//constant uint16_t number_word_sequence[] = {
//    zero_WORD,    one_WORD,     two_WORD,       three_WORD,    four_WORD,
//    five_WORD,    six_WORD,     seven_WORD,     eight_WORD,    nine_WORD,
//    ten_WORD,     eleven_WORD,  twelve_WORD,    thirteen_WORD, fourteen_WORD,
//    fifteen_WORD, sixteen_WORD, seventeen_WORD, eighteen_WORD, nineteen_WORD,
//    twenty_WORD,
//};
//#endif

uint16_t number_word_INT(const uint16_t word);

#define GRAMMATICALCASE_LIST_LONG 6
#ifndef OPENCL
extern constant uint16_t grammaticalCase_list[GRAMMATICALCASE_LIST_LONG];
#else
extern constant uint16_t grammaticalCase_list[GRAMMATICALCASE_LIST_LONG] = {
    accusative_case_GRAMMAR, instrumental_case_GRAMMAR, dative_case_GRAMMAR,
    topic_case_GRAMMAR,      nominative_case_GRAMMAR,   ablative_case_GRAMMAR};
#endif

#define PERSPECTIVE_LIST_LONG 4
#ifndef OPENCL
extern const uint16_t perspective_list[PERSPECTIVE_LIST_LONG];
#else
constant uint16_t perspective_list[PERSPECTIVE_LIST_LONG] = {
    realis_mood_GRAMMAR, conditional_mood_GRAMMAR, interrogative_mood_GRAMMAR,
    deontic_mood_GRAMMAR};
#endif

#define NUMBER_WORD_LONG 16
#define MAXIMUM_WORD_NUMBER 0xFF
#ifndef OPENCL
extern constant uint16_t number_word_sequence[NUMBER_WORD_SEQUENCE_LONG];
extern constant char pyash_number_dictionary [NUMBER_WORD_SEQUENCE_LONG][NUMBER_WORD_LONG];
#else
constant uint16_t number_word_sequence[NUMBER_WORD_SEQUENCE_LONG] = {
    zero_WORD,    one_WORD,     two_WORD,       three_WORD,    four_WORD,
    five_WORD,    six_WORD,     seven_WORD,     eight_WORD,    nine_WORD,
    ten_WORD,     eleven_WORD,  twelve_WORD,    thirteen_WORD, fourteen_WORD,
    fifteen_WORD, sixteen_WORD, seventeen_WORD, eighteen_WORD, nineteen_WORD,
    twenty_WORD,
};
constant char pyash_number_dictionary[NUMBER_WORD_SEQUENCE_LONG][NUMBER_WORD_LONG] = {
    "zron", "hyik", "tyut", "tyin", "ksas", "hfak", "hlis",
    "hsip", "hwap", "tcuf", "htip", "slen", "tfat", "tses",
    "hses", "hpet", "hsos", "hdap", "nlic", "syis"};
#endif

word_t is_grammatical_case_word(word_t word);
word_t is_grammar_word(word_t word);
word_t is_quote_code(word_t word);
#define hgaftlatri is_grammar_word
word_t is_perspective_word(word_t word);
word_t is_letter(char glyph);
#endif
