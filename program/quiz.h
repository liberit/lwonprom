/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/* file: minunit.h */
#ifndef MINUNIT_H
#define MINUNIT_H
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "interpret.h"
#include "lwonprom.h"
#include "pyashWords.h"
#include "genericOpenCL.h"
#include "sort.h"
#include "compare.h"
#include "compile.h"
// #include "json.h"

#define mu_assert(message, quiz)                                               \
  do {                                                                         \
    quizs_run++;                                                               \
    if (!(quiz)) {                                                             \
      printf("not ok %d - %s:%d %s\n", quizs_run, __FUNCTION__, __LINE__,      \
             message);                                                         \
    } else {                                                                   \
      printf("ok %d - %s:%d %s\n", quizs_run, __FUNCTION__, __LINE__,          \
             message);                                                         \
    }                                                                          \
  } while (0)
//  mu
#define mu_run_quiz(quiz)                                                      \
  do {                                                                         \
    const char *message = quiz();                                              \
    if (message) {                                                             \
      return message;                                                          \
    }                                                                          \
  } while (0)
extern int cardinal_quiz();
#ifndef EMSCRIPTEN 
#define TODO(_x) _x;
#else
#define TODO(_x) 
#endif

#endif \
#endif
