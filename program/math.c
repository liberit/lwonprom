/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "math.h"
uint16_t number_plus(uint16_t acc, uint16_t ins) {
  if (acc + ins < 0xFFFF) {
    return acc + ins;
  } else {
    return 0xFFFF;
  }
}
uint64_t number_subtract(uint64_t acc, uint64_t ins) {
  if (acc > ins) {
    return acc - ins;
  } else {
    return 0;
  }
}
uint64_t number_multiply(uint64_t acc, uint64_t ins) {
    return acc * ins;
}
uint64_t number_divide(uint64_t acc, uint64_t ins) {
    return acc / ins;
}
uint64_t number_xor(uint64_t acc, uint64_t ins) {
    return acc ^ ins;
}
uint64_t number_neg(uint64_t acc) {
    return ~acc;
}
uint64_t number_exponent(uint64_t acc, uint64_t ins) {
    uint64_t produce = 1;
    repeat(ins, produce *= acc);
    return produce;
}
