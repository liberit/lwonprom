/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "compile.h"

/**
 * is the index finger in a final independent clause line
 * tlichtinhlasnweh txikka ri
 * */
uint16_t /*boolean*/ tlichtinhlasnweh_txikka_ri(const uint16_t txik,
                                                const uint16_t input_long,
                                                const line_t input[]) {
  guarantee(input_long > 0);
  // DEBUGPRINT(("%x txik\n", txik));
  const uint16_t line_txik = txik / LINE_LONG;
  const uint16_t word_txik = txik % LINE_LONG;
  const uint16_t indicator_list = input[line_txik][0];
  if ((indicator_list & 1) == 1 && ((indicator_list >> word_txik) & 1) == 1) {
    return truth_WORD;
  }
  if ((indicator_list & 1) == 0 && ((indicator_list >> word_txik) & 1) == 0) {
    return truth_WORD;
  }
  return lie_WORD;
}
void extract_words_ins_htinka_addenda(const uint16_t input_long,
                                      const line_t input[],
                                      const uint16_t words_long,
                                      const uint16_t in_words_begin,
                                      const uint16_t produce_long,
                                      line_t produce[]) {
  guarantee(words_long > 0);
  guarantee(input != NULL);
  guarantee(produce != NULL);
  uint16_t words_begin = in_words_begin == 0 ? 1 : in_words_begin;
  uint16_t indexFinger = htinti_final_word_found(produce_long, produce);
  conditional(tablet_read(indexFinger, (uint8_t)produce_long, produce) != 0,
              ++indexFinger);
  // DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  // tablet_print(produce_long, produce);
  uint16_t word = 0;
  uint16_t temporary = 0;
  uint16_t grammar_index = lie_WORD;
  // DEBUGPRINT(("0x%X words_long, 0x%X words_begin\n", words_long,
  // words_begin));
  if (indexFinger == 0)
    ++indexFinger;
  // tablet_print(input_long, input);
  repeat(
      words_long,
      word = tablet_upcoming_word_read((uint16_t)(words_begin + iterator),
                                       input_long, input, &temporary);
      // DEBUGPRINT(("0x%lX iterator, 0x%X words_begin, 0x%X words_long, 0x%X
      // temporary,0x%X word\n",
      //           iterator, words_begin, words_long, temporary, word));
      // check is word is grammar word
      grammar_index = grammar_txik_verify(temporary, input_long, input);
      // DEBUGPRINT(("0x%X grammar_index, 0x%X truth_WORD \n", grammar_index,
      // truth_WORD));
      if (((iterator + words_begin) & 0xF) == 0)++ iterator;
      guarantee(temporary == words_begin + iterator);
      // tablet_print(produce_long, produce);
      if (grammar_index == truth_WORD) {
        tablet_grammar_write(indexFinger, word, produce_long, produce,
                             &indexFinger);
        // tablet_print(produce_long, produce);
        ++indexFinger;
      } else {
        tablet_word_write(indexFinger, word, produce_long, produce,
                          &indexFinger);
        // tablet_print(produce_long, produce);
        ++indexFinger;
      }

  );
}

void literal_grammar_word_ins_htinka_addenda(const uint16_t grammar_word,
                                             const uint16_t produce_long,
                                             line_t produce[]) {
  uint16_t indexFinger = htinti_final_word_found(produce_long, produce) + 1;
  // DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  if (indexFinger == 0)
    ++indexFinger;
  tablet_grammar_write(indexFinger, grammar_word, produce_long, produce,
                       &indexFinger);
};
void literal_word_ins_htinka_addenda(const uint16_t word, struct Page produce) {
  uint16_t indexFinger =
      htinti_final_word_found(produce.plength, produce.lines) + 1;
  // DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  if (indexFinger == 0)
    ++indexFinger;
  tablet_word_write(indexFinger, word, produce.plength, produce.lines,
                    &indexFinger);
}
void literal_number_ins_htinka_addenda(const uint16_t number,
                                       struct Phrase produce) {
  uint16_t indexFinger =
      htinti_final_word_found(produce.page.plength, produce.page.lines) + 1;
  DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  if (indexFinger == 0)
    ++indexFinger;
  tablet_word_write(indexFinger, NUMBER_QUOTE, produce.page.plength,
                    produce.page.lines, &indexFinger);
  ++indexFinger;
  tablet_word_write(indexFinger, number, produce.page.plength,
                    produce.page.lines, &indexFinger);
}
void literal_letter_ins_htinka_addenda(const uint16_t number,
                                       struct Phrase produce) {
  uint16_t indexFinger =
      htinti_final_word_found(produce.page.plength, produce.page.lines) + 1;
  // DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  if (indexFinger == 0)
    ++indexFinger;
  tablet_word_write(indexFinger, LETTER_QUOTE, produce.page.plength,
                    produce.page.lines, &indexFinger);
  ++indexFinger;
  tablet_word_write(indexFinger, number, produce.page.plength,
                    produce.page.lines, &indexFinger);
}

void string_zero(const size_t string_long, char *string) {
  repeat(string_long, string[iterator] = 0);
}
// clear tablet, make tablet all zeros
void tablet_zero(const uint16_t tablet_long, line_t tablet[]) {
  uint16_t maximum_tablet_long = tablet_long * LINE_LONG;
  repeat(maximum_tablet_long, tablet_write(iterator, 0, tablet_long, tablet));
}

struct Phrase /* psas */ wohgafka_fyakyi_dva2ttu(const word_t hgaftlat,
                                                 struct Phrase psas) {
  conditional(psas.begin == 0, psas.begin += 1);
  const txik_t cwasci_txik = psas.begin + psas.length;
  txik_t hnatci_txik = cwasci_txik;
  // DEBUGPRINT(("%X hnatci_txik\n", hnatci_txik));
  tablet_grammar_write(hnatci_txik, hgaftlat, psas.page.plength,
                       psas.page.lines, &hnatci_txik);
  psas.length += hnatci_txik - cwasci_txik + 1;
  // DEBUGPRINT(("%x psas.length, %X hnatci_txik, %X cwasci_txik\n",
  //      psas.length, hnatci_txik, cwasci_txik));
  return psas;
}
struct Phrase /* psas */ wosramka_fyakyi_dva2ttu(const word_t sramtlat,
                                                 struct Phrase psas) {
  conditional(psas.begin == 0, psas.begin += 1);
  const txik_t cwasci_txik = psas.begin + psas.length;
  txik_t hnatci_txik = cwasci_txik;
  // DEBUGPRINT(("%X hnatci_txik\n", hnatci_txik));
  tablet_word_write(hnatci_txik, sramtlat, psas.page.plength, psas.page.lines,
                    &hnatci_txik);
  psas.length += hnatci_txik - cwasci_txik + 1;
  // DEBUGPRINT(("%x psas.length, %X hnatci_txik, %X cwasci_txik\n",
  //      psas.length, hnatci_txik, cwasci_txik));
  return psas;
}

struct Phrase /*htikya*/
fyakyi_fyaktlatka_dva2ttu(struct Phrase htikya, const struct Phrase hkakya) {
  conditional(htikya.begin == 0, ++htikya.begin);
  word_t word = phrase_word_read(hkakya, 0);
  word_t grammar_boolean = fyaknweh_txikka_hgaftxikri(hkakya, 0);
  if (grammar_boolean == truth_WORD) {
    htikya = addenda_theGrammarWord_toThePhrase(word, htikya);
  } else {
    htikya = addenda_theWord_toThePhrase(word, htikya);
  }
  return htikya;
}

struct Phrase /*htikya*/
fyakyi_fyaktlatka_dva2ttu_old(struct Phrase htikya,
                              const struct Phrase hkakya) {
  conditional(htikya.begin == 0, ++htikya.begin);
  txik_t htiktxik = htikya.begin + htikya.length;
  DEBUGPRINT(("%X htiktxik\n", htiktxik));
  const txik_t cwashtiktxik = htiktxik;
  word_t word = 0;
  txik_t hnattxik = hkakya.length + hkakya.begin;
  txik_t hnathtincwas = hnattxik / LINE_LONG;
  txik_t hnathtiktxik = hnattxik & LINE_LONG_MASK;
  txik_t cwashnathtiktxik = hnathtiktxik;
  word_t grammar_boolean = lie_WORD;
  conditional(hnattxik == 0, return htikya);
  if ((hnattxik & LINE_LONG_MASK) == 0) {
    DEBUGPRINT(("%X hnattxik\n", hnattxik));
    // found newindex
    // if previous index is partial continue
    conditional(is_theIndexfingerZLine_aPartialIndependentClause(
                    hnattxik - LINE_LONG, hkakya) == truth_WORD,
                return htikya);
    // else start a new sentence in the produce.
    conditional((htiktxik & LINE_LONG_MASK) != 0,
                htiktxik = grow_theIndexFinger_untilTheUpcomingLine(htiktxik));
    hnathtincwas = htiktxik / LINE_LONG;
    hnathtiktxik = 1;
    // bump the lines and length that is written to
  }
  word = phrase_word_read(hkakya, 0);
  cwashnathtiktxik = hnathtiktxik;
  grammar_boolean = fyaknweh_txikka_hgaftxikri(hkakya, 0);
  if (grammar_boolean == truth_WORD) {
    tablet_grammar_write(hnathtiktxik, word, htikya.page.plength - hnathtincwas,
                         htikya.page.lines + hnathtincwas, &hnathtiktxik);
  } else {
    tablet_word_write(hnathtiktxik, word, htikya.page.plength - hnathtincwas,
                      htikya.page.lines + hnathtincwas, &hnathtiktxik);
  }
  ++hnathtiktxik;
  htiktxik += hnathtiktxik - cwashnathtiktxik;
  DEBUGPRINT(("0x%X cwashtiktxik\n", cwashtiktxik));
  htikya.length += htiktxik - cwashtiktxik;
  return htikya;
}

word_t /* boolean */ hpifci_txikna_hlassyoslwoh_ri(txik_t hnakya) {
  conditional((hnakya & LINE_LONG_MASK) == 0, return truth_WORD);
  return lie_WORD;
}

word_t /*boolean*/
fyakti_tlicci_htinna_tcunci_htinka_ri(struct Phrase hnakhnikya) {
  // TODO(logan) manage connectives that come after a htin perspective
  // check if previous grammar word in hnakhnikya is independent clause
  // perspective
  // const txik_t hnattxik = hnakhnikya.begin+hnakhnikya.length -1;
  // txik_t syostxik = hnattxik / LINE_LONG * LINE_LONG;
  // word_t hlassyos = syostxik);
  // word_t word  = phrase_word_read(hnakhnikya, hnakhnikya.length -1);
  struct Phrase grammarWord = neo_tablet_retrospective_grammar_read(hnakhnikya);
  // DEBUGPRINT(("%X grammarWord\n", phrase_word_read(grammarWord, 0)));
  word_t word = phrase_word_read(grammarWord, 0);
  conditional(gen_indexFinger(word,
                              INDEPENDENTCLAUSE_PERSPECTIVE_WORD_SEQUENCE_LONG,
                              independentClause_perspective_word_sequence) <
                  INDEPENDENTCLAUSE_PERSPECTIVE_WORD_SEQUENCE_LONG,
              return truth_WORD;);
  conditional(word == paragraph_GRAMMAR, return truth_WORD);
  return lie_WORD;
}

/** phrase_addenda 
 * addenda_thePhrase_toThePhrase*/
struct Phrase /* fyakyi */ fyakyi_fyakka_dva2ttu(struct Phrase htikya,
                                                 struct Phrase hkakya) {
  guarantee(htikya.page.plength * LINE_LONG > hkakya.length);
  if (hkakya.begin == 0) {
    hkakya.begin++;
    if (hkakya.length >= 1)
      hkakya.length--;
  }
  //Page_print(htikya.page);
  //Page_print(hkakya.page);
  txik_t phrase_long = hkakya.length;
  struct Phrase phraseWord = hkakya;
  phraseWord.length = 1;
  struct Phrase prevPhraseWord = phraseWord;
  txik_t txik;
  // DEBUGPRINT(("%X indexFinger\n", htikya.begin + htikya.length));
  repeat(
      phrase_long, phraseWord.begin = hkakya.begin + iterator;
     // DEBUGPRINT(("%X indexFinger, %X word\n", htikya.begin + htikya.length,
     //     phrase_word_read(phraseWord, 0)));
      
      if (is_thePageZIndexfinger_atTheLineSummary(phraseWord.begin) ==
          truth_WORD) {
        conditional(is_theIndexfingerZLine_aPartialIndependentClause(
                        number_subtract(phraseWord.begin, LINE_LONG), hkakya) ==
                        truth_WORD,
                    continue);
      } 
      if (is_thePhraseZFinalIndependentClause_aDoneIndependentClause(htikya) ==
            truth_WORD) {
        htikya = addenda_aHollowIndependentClause_toThePhrase(htikya);
      }
      //DEBUGPRINT(("%X phraseWord.begin, %X phraseWord.length, %X phraseWord,"
      //    " %lX iterator \n",
      //    phraseWord.begin, phraseWord.length, 
      //    phrase_word_read(phraseWord, 0), iterator));
      htikya = addenda_thePhraseWord_toThePhrase(phraseWord, htikya);
      //Page_print(htikya.page);
      prevPhraseWord.begin = phraseWord.begin;
      --prevPhraseWord.begin;
      //DEBUGPRINT(("%X prevPhraseWord.begin, %X phraseWord.begin, %X htikya.length\n", 
      //      prevPhraseWord.begin, phraseWord.begin, htikya.length)); 
      if (is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
              prevPhraseWord) == truth_WORD) {
        // adjust hkakya to not read the blanks after end of sentence.
        txik = grow_theIndexFinger_untilTheUpcomingLine(phraseWord.begin);
        // DEBUGPRINT(("%X indexFinger\n", htikya.begin + htikya.length));
        phraseWord.begin = txik;
        iterator = phraseWord.begin - hkakya.begin;
        //DEBUGPRINT(("%X iterator\n", iterator)); 
      }
      // detect paragraph quote
      if (is_thePhrase_aParagraphQuote(htikya) == truth_WORD) {
        //    skip contents
        // find contents length
     //   DEBUGPRINT(("paragraph quote detected\n"));
        struct Phrase verb_phrase = retrospective_verb_phrase_found(htikya);
        struct Phrase quote_phrase = verb_phrase;
        quote_phrase.length -= 1;
        uint16_t quote_length = derive_aQuoteLength_byThePhrase(quote_phrase);
        //    add to length of output
        htikya.length += quote_length;
        //DEBUGPRINT(("%X htikya.length\n", htikya.length));
        iterator = grow_theIndexFinger_untilTheUpcomingLine(iterator);
        iterator += quote_length;
        iterator -= hkakya.begin;
      });
  // vacant tail line delete
  //htikya = delete_vacantLine(htikya);
  // DEBUGPRINT(("%X htikya.length\n", htikya.length));
  return htikya;
}

/** quote_addenda */
struct Phrase /* fyakyi */ fyakyi_kyitka_dva2ttu(struct Phrase htikya,
                                                 struct Phrase hkakya) {
  // if not enough room for quote then move to next line
  // guarantee quote is not larger than tablet long -1
  //text_phrase_print(htikya);
  //text_phrase_print(hkakya);
  guarantee(htikya.length < LINE_LONG);
  guarantee(hkakya.length < LINE_LONG);
  uint8_t phrase_long = phrase_long_diagnose(hkakya);
  conditional(!is_thePhrase_aQuote(hkakya),
              return addenda_thePhrase_toThePhrase(hkakya, htikya));
  uint8_t lineVacancy =
      howMuch_lineVacancy_remains_atThePhraseTermination(htikya);
  conditional(lineVacancy < hkakya.length,
              htikya = grow_thePhrase_untilTheUpcomingLine(htikya));
  // DEBUGPRINT(("%X lineVacancy, %X phrase_long, %X hkakya.length, %X htikya
  // txik\n",
  //      lineVacancy, phrase_long, hkakya.length, htikya.begin+htikya.length));
  htikya = addenda_thePhrase_toThePhrase(hkakya, htikya);
  return htikya;
}

word_t /* blu7n */ fyaknweh_txikka_hgaftxikri(const struct Phrase nwikya,
                                              const txik_t hkakya) {
  return grammar_txik_verify(nwikya.begin + hkakya, nwikya.page.plength,
                             nwikya.page.lines);
}

txik_t hvatci_hlaslweh_txikka_nwattu(txik_t htikya) {
  htikya |= LINE_LONG_MASK;
  htikya += 1;
  // DEBUGPRINT(("%X htikya\n", htikya));
  return htikya;
}

/**
 * grow_thePhrase_untilTheUpcomingLine(theIndexFinger)               \
 */
struct Phrase hvatci_hlaslweh_fyakka_nwattu(struct Phrase phrase) {
  txik_t txik = phrase.begin + phrase.length;
  txik = hvatci_hlaslweh_txikka_nwattu(txik);
  phrase.length = txik - phrase.begin;
  return phrase;
}

// #define is_theIndexfingerZLine_aPartialIndependentClause(indexfinger, line)
word_t /*boolean*/ txikti_hlasna_pfunhtinka_ri(txik_t txikya,
                                               struct Phrase hnakya) {
  word_t indicator_list = read_thePage_byTheIndexFinger_toTheWord(
      hnakya.page, (txikya / LINE_LONG) * LINE_LONG);
  DEBUGPRINT(("%X indicator_list, %X txikya / LINE_LONG\n", indicator_list,
              txikya / LINE_LONG));
  if ((indicator_list & 1) == 0) {
    DEBUGPRINT(("truth\n"));
    return truth_WORD;
  }
  return lie_WORD;
}

/* define addenda_theGrammarWord_toThePhrase(theGrammarWord, toThePhrase) \
 */
struct Phrase /*htikya*/ fyakyi_hgaftlatka_dva2ttu(struct Phrase htikya,
                                                   const word_t hkakya) {
  conditional(htikya.begin == 0, htikya.begin++);
  const txik_t cwastxik = htikya.begin + htikya.length;
  txik_t txik = 0;
  tablet_grammar_write(cwastxik, hkakya, htikya.page.plength, htikya.page.lines,
                       &txik);
  htikya.length += 1 + txik - cwastxik;
  return htikya;
}
// addenda_theWord_toThePhrase
struct Phrase /*htikya*/ fyakyi_tlatka_dva2ttu(struct Phrase htikya,
                                               const word_t hkakya) {
  conditional(htikya.begin == 0, htikya.begin++);
  const txik_t cwastxik = htikya.begin + htikya.length;
  txik_t txik = 0;
  tablet_word_write(cwastxik, hkakya, htikya.page.plength, htikya.page.lines,
                    &txik);
  htikya.length += 1 + txik - cwastxik;
  return htikya;
}

struct Phrase /*htikya*/ fyakyi_hcochtinka_hva2ttu(struct Phrase htikya) {
  conditional(htikya.begin == 0, htikya.begin = 1);
  const txik_t cwastxik = htikya.begin + htikya.length;
  txik_t txik = cwastxik;
  if (cwastxik > 1) {
    conditional((txik & LINE_LONG_MASK) != 0,
                txik = grow_theIndexFinger_untilTheUpcomingLine(txik));
    htikya.length += 1 + txik - cwastxik;
  } else {
    txik = 0;
    htikya.length = 0;
  }
  tablet_write(txik, HOLLOW_SUMMARY, htikya.page.plength, htikya.page.lines);
  return htikya;
}
struct Phrase copy_thePhrase_toThePhrase(const struct Phrase input_phrase,
                                         struct Phrase produce_phrase) {
  return fyakyi_fyakka_dva2ttu(produce_phrase, input_phrase);
}

// drop_theParagraphZFirstSentence_toTheParagraph( theParagraph)
struct Paragraph tyafyi_tyafti_hpamci_htinka_hfuttu(struct Paragraph tyafya) {
  // neo_independentClause_long_found();
  // okay so what was it hmmm.....
  struct Sentence firstSentence =
      find_theParagraphZFirstSentence_toTheSentence(tyafya);
  tyafya.begin += firstSentence.length;
  tyafya.length -= firstSentence.length;
  return tyafya;
}
// drop_theParagraphZFinalSentence_toTheParagraph( theParagraph)
// tyafyi_tyafti_tlicci_htinka_hfuttu
struct Paragraph
drop_theParagraphZFinalSentence_toTheParagraph(struct Paragraph tyafya) {
  text_phrase_print(tyafya);
  struct Sentence lastSentence =
      find_theParagraphZLastSentence_toTheSentence(tyafya);
  text_phrase_print(lastSentence);
  DEBUGPRINT(("%X tyafya.begin+tyafya.length\n", tyafya.begin + tyafya.length));
  DEBUGPRINT(("%X lastSentence.begin \n", lastSentence.begin));
  // uint16_t maximum_input_length = tyafya.begin+tyafya.length;
  // lastSentence.begin - tyafya.begin;
  uint16_t amount_to_reduce_length = lastSentence.length; //
  DEBUGPRINT(("%X lastSentence.length\n", lastSentence.length));
  DEBUGPRINT(("%X tyafya.length\n", tyafya.length));
  tyafya.length -= amount_to_reduce_length;
  DEBUGPRINT(("%X tyafya.length\n", tyafya.length));
  return tyafya;
}

/* what does it do? */
uint16_t /*Sequence_finally*/ tablet_addenda(const uint16_t tablet_long,
                                             const line_t tablet[],
                                             uint16_t knowledge_attribute[],
                                             line_t knowledge[]) {
  uint16_t sequence_long = knowledge_long_found(tablet_long, tablet);
  uint16_t knowledge_max_long = knowledge_attribute[Sequence_max_long];
  uint16_t knowledge_long = knowledge_long_found(knowledge_max_long, knowledge);
  // make sure it will fit
  DEBUGPRINT(("0x%X knowledge_long, 0x%X kasf\n", knowledge_long,
              knowledge_attribute[Sequence_finally]));
  // tablet_print(knowledge_max_long, knowledge);
  // tablet_print(tablet_long, tablet);
  guarantee(knowledge_long == knowledge_attribute[Sequence_finally]);
  guarantee(knowledge_max_long > sequence_long);
  guarantee(knowledge_max_long - knowledge_long >= tablet_long);
  tablet_copy(sequence_long, tablet, knowledge_max_long - knowledge_long,
              knowledge + knowledge_long);
  return knowledge_long + sequence_long;
}

/** clears the contents between begin and length of the phrase
 */
struct Phrase hollow_thePhrase_byThePhrase(struct Phrase input,
                                           struct Phrase phrase) {
  guarantee(phrase.page.lines == input.page.lines);
  if (phrase.begin % LINE_LONG == 1) {
    phrase.begin -= 1;
    phrase.length += 1;
  }
  // #TODO compress out empty space
  // if the phrase length plus begin is less than input length plus begin
  // and it is a full htin
  const txik_t phrase_max_txik = phrase.begin + phrase.length;
  const txik_t input_max_txik = input.begin + input.length;
  guarantee(input_max_txik / LINE_LONG <= input.page.plength);
  if (phrase_max_txik < input_max_txik &&
      is_thePhrase_aPerfectIndependentClause(phrase)) {
    // then copy the htin's ahead of it, onto it,
    // how many lines to copy?
    uint8_t begin_line = phrase.begin / LINE_LONG;
    uint8_t clone_line = ((phrase_max_txik | LINE_LONG_MASK) + 1) / LINE_LONG;
    uint8_t final_line = ((input_max_txik | LINE_LONG_MASK) + 1) / LINE_LONG;
    final_line =
        final_line > input.page.plength ? input.page.plength : final_line;
    repeat(clone_line - begin_line,
           line_copy(input.page.lines[clone_line + iterator],
                     input.page.lines[begin_line + iterator]););
    // then clear the last htin
    struct Phrase final_phrase = input;
    final_phrase.begin =
        ((input_max_txik - phrase.length) | LINE_LONG_MASK) + 1;
    final_phrase.length = input_max_txik - final_phrase.begin;
    repeat(final_phrase.length, phrase_word_write(0, final_phrase, iterator));
  } else { // if phrase is at the end
    repeat(phrase.length, phrase_word_write(0, phrase, iterator));
  }
  input.length = (input.page.plength * LINE_LONG) - input.begin;
  input.length = phrase_long_diagnose(input);
  return input;
}

struct Text text_addenda(struct Text input, struct Text produce) {
  guarantee(produce.max_length - produce.length > input.length);
  repeat(input.length,
         produce.letters[produce.length] = input.letters[iterator];
         produce.length++);
  return produce;
}

/** addenda_theText_toThePhrase */
struct Phrase fyakyi_hwuska_dva2ttu(struct Phrase phrase, struct Text text) {
  guarantee(phrase.page.plength*LINE_LONG > phrase.begin);
  const uint phrase_vacancy = phrase.page.plength*LINE_LONG - phrase.begin;
  guarantee(phrase_vacancy >= text.length);
  repeat(text.length,
      //DEBUGPRINT(("%c, %X, %X iterator\n", text.letters[iterator], 
      //     text.letters[iterator], iterator));
      phrase = 
      addenda_theLetter_toThePhrase(text.letters[iterator], phrase);
      );
  return phrase;
}
/** addenda_theLetter_toThePhrase(letter, phrase)*/
struct Phrase fyakyi_lyatka_dva2ttu(const struct Phrase phrase, uint16_t letter) {
  guarantee(phrase.begin+phrase.length < phrase.page.plength*LINE_LONG);
  struct Phrase produce = phrase;
  const uint16_t inject_point = phrase.begin + phrase.length;
  const uint8_t inject_row = inject_point/LINE_LONG;
  const uint8_t inject_column =  inject_point % LINE_LONG;
  produce.page.lines[inject_row][inject_column] = letter;
  produce.length++;
  return produce;
}
