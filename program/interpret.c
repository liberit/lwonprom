/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//#include "encoding.h"
#include "interpret.h"
//#include "assert.h"
//#include "tlep.h"
//#include "pyashWords.h"
//#include "translation.h"
//#include <stdio.h>
//
//
uint16_t phrase_number_extract(const struct Phrase htin,
                               const uint16_t phrase_code) {
  guarantee(htin.page.lines != NULL);
  struct Phrase phrase;
  phrase = neo_retrospective_phrase_situate(htin, phrase_code);
  DEBUGPRINT(
      ("0x%X phrase.length, 0x%X phrase_begin\n", phrase.length, phrase.begin));
  phrase_print(phrase);
  guarantee(tablet_read(phrase.begin, htin.page.plength, htin.page.lines) ==
            NUMBER_QUOTE);
  if (tablet_read(phrase.begin, htin.page.plength, htin.page.lines) !=
      NUMBER_QUOTE)
    return 0;
  return tablet_read(phrase.begin + 1, htin.page.plength, htin.page.lines);
}
uint16_t phrase_letter_extract(const struct Phrase htin,
                               const uint16_t phrase_code) {
  guarantee(htin.page.lines != NULL);
  struct Phrase phrase;
  phrase = neo_retrospective_phrase_situate(htin, phrase_code);
  // DEBUGPRINT(
  //    ("0x%X phrase.length, 0x%X phrase_begin\n", phrase.length,
  //    phrase.begin));
  // phrase_print(phrase);
  guarantee(tablet_read(phrase.begin, htin.page.plength, htin.page.lines) ==
            LETTER_QUOTE);
  if (tablet_read(phrase.begin, htin.page.plength, htin.page.lines) !=
      LETTER_QUOTE)
    return 0;
  return tablet_read(phrase.begin + 1, htin.page.plength, htin.page.lines);
}

uint16_t number_extract(const uint16_t program_long, const line_t program[],
                        const uint16_t phrase_begin,
                        const uint16_t phrase_long) {
  guarantee(phrase_long > 0);
  guarantee(tablet_read(phrase_begin, (uint8_t)program_long, program) ==
                NUMBER_QUOTE ||
            LETTER_QUOTE);
  return tablet_read(phrase_begin + 1, (uint8_t)program_long, program);
}

/**
 * multiply interpret
 * @param knowledge --  looks for the ka in it,
 *                      default behaviour is that the ka is 0.
 * @param input --  this should include the instrumental case, otherwise fail.
 * @param produce -- this has the result, in the form of number dokali
 */
struct Phrase /* produce */ multiply_interpret(struct Phrase knowledge,
                                               struct Phrase input,
                                               struct Phrase produce) {
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  // phrase_print(input);
  // text_phrase_print(knowledge);
  // text_phrase_print(input);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, instrumental_case_GRAMMAR);
  // DEBUGPRINT(
  //     ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin,
  //     phrase.length));
  // text_phrase_print(phrase);
  hyak = neo_number_extract(input, phrase);
  // phrase_print(knowledge);
  // text_phrase_print(knowledge);
  // text_page_print(knowledge.page);
  // Page_print(knowledge.page);
  phrase = neo_retrospective_phrase_situate(knowledge, accusative_case_GRAMMAR);
  // text_phrase_print(phrase);
  // DEBUGPRINT(
  //     ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin,
  //     phrase.length));
  if (phrase.begin != 0) {
    hkak = neo_number_extract(knowledge, phrase);
    // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
    hkak = number_multiply(hkak, hyak);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }
#ifdef ARRAYLINE
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = NUMBER_QUOTE;
  (*produce.page.lines)[2] = hkak;
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = hkak;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
  // text_phrase_print(produce);
  return produce;
}
/**
 * subtract interpret
 * @param knowledge --  looks for the ka in it,
 *                      default behaviour is that the ka is 0.
 * @param input --  this should include the instrumental case, otherwise fail.
 * @param produce -- this has the result, in the form of number dokali
 */
struct Phrase /* produce */ neo_subtract_interpret(struct Phrase knowledge,
                                                   struct Phrase input,
                                                   struct Phrase produce) {
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  // phrase_print(input);
  // text_phrase_print(knowledge);
  // text_phrase_print(input);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, instrumental_case_GRAMMAR);
  // DEBUGPRINT(
  //     ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin,
  //     phrase.length));
  // text_phrase_print(phrase);
  hyak = neo_number_extract(input, phrase);
  // phrase_print(knowledge);
  // text_phrase_print(knowledge);
  // text_page_print(knowledge.page);
  // Page_print(knowledge.page);
  phrase = neo_retrospective_phrase_situate(knowledge, accusative_case_GRAMMAR);
  // text_phrase_print(phrase);
  // DEBUGPRINT(
  //     ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin,
  //     phrase.length));
  if (phrase.begin != 0) {
    hkak = neo_number_extract(knowledge, phrase);
    // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
    hkak = number_subtract(hkak, hyak);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }
#ifdef ARRAYLINE
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = NUMBER_QUOTE;
  (*produce.page.lines)[2] = hkak;
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = hkak;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
  // text_phrase_print(produce);
  return produce;
}
/**
 * plus interpret
 * @param knowledge --  looks for the ka in it,
 *                      default behaviour is that the ka is 0.
 * @param input --  this should include the instrumental case, otherwise fail.
 * @param produce -- this has the result, in the form of number dokali
 */
struct Phrase /* produce */ neo_plus_interpret(struct Phrase knowledge,
                                               struct Phrase input,
                                               struct Phrase produce) {
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, instrumental_case_GRAMMAR);
  //text_phrase_print(phrase);
  hyak = neo_number_extract(input, phrase);
  //DEBUGPRINT(("%X hyak\n", hyak));
  phrase = neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  //text_phrase_print(phrase);
  // if not found then look in knowledge
  if (phrase.length == 0) {
    phrase =
        neo_retrospective_phrase_situate(knowledge, accusative_case_GRAMMAR);
    hkak = neo_number_extract(knowledge, phrase);
  } else {
    hkak = neo_number_extract(input, phrase);
  }
  //DEBUGPRINT(("%X hkak\n", hkak));
  hkak = number_plus(hkak, hyak);
#ifdef ARRAYLINE
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = NUMBER_QUOTE;
  (*produce.page.lines)[2] = hkak;
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = hkak;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
  // text_phrase_print(produce);
  return produce;
}

uint16_t /*final line number*/ tablet_final_line_find(struct Page tablet) {
  repeat(tablet.plength, conditional(tablet.lines[iterator][0] == 0,
                                     return (uint16_t)iterator));
  return tablet.plength;
}

void plus_interpret(const uint16_t input_long, const uint16_t input_indexFinger,
                    const line_t input[], uint16_t knowledge_attribute[],
                    line_t knowledge[], const uint16_t produce_long,
                    line_t produce[]) {
  guarantee(produce_long > 0);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  uint16_t phrase_begin = 0;
  uint8_t phrase_long = 0;
  uint16_t knowledge_long = knowledge_attribute[Sequence_max_long];
  uint16_t knowledge_indexFinger =
      knowledge_attribute[Sequence_finally] * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  // DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  // tablet_print(input_long, input);
  retrospective_phrase_situate((uint8_t)input_long, input, input_indexFinger,
                               instrumental_case_GRAMMAR, &phrase_long,
                               &phrase_begin);
  // DEBUGPRINT(
  //     ("0x%X phrase_begin, 0x%X phrase_long\n", phrase_begin, phrase_long));
  hyak = number_extract(input_long, input, phrase_begin, phrase_long);
  // DEBUGPRINT(("0x%X hkak, 0x%X hyak, 0x%X knowledge_indexFingger\n", hkak,
  // hyak,
  //             knowledge_indexFinger));
  // tablet_print(knowledge_long, knowledge);

  retrospective_phrase_situate((uint8_t)knowledge_long, knowledge,
                               knowledge_indexFinger, accusative_case_GRAMMAR,
                               &phrase_long, &phrase_begin);
  // DEBUGPRINT(
  //     ("0x%X phrase_begin, 0x%X phrase_long\n", phrase_begin, phrase_long));
  if (phrase_begin != 0) {
    hkak = number_extract(knowledge_long, knowledge, phrase_begin, phrase_long);
    DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
    hkak = number_plus(hkak, hyak);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }
#ifdef ARRAYLINE
  (*produce)[0] = 0x19;
  (*produce)[1] = NUMBER_QUOTE;
  (*produce)[2] = hkak;
  (*produce)[3] = accusative_case_GRAMMAR;
  (*produce)[4] = realis_mood_GRAMMAR;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = hkak;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
}

struct Phrase /*produce*/ dat_plus_interpret(struct Phrase knowledge,
                                             struct Phrase input_phrase,
                                             struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  // DEBUGPRINT(("%s\n", "hello"));
  // DEBUGPRINT(("0x%X hkak, 0x%X hyak, 0x%X knowledge_indexFingger\n", hkak,
  // hyak,
  //            knowledge_indexFinger));
  Page_print(knowledge.page);

  text_phrase_print(knowledge);
  text_phrase_print(input_phrase);
  phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, phrase);
    hkak = number_plus(hkak, htik);
  }
#ifdef ARRAYLINE
  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.length = 7;
  produce.begin = 1;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = hkak;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  return produce;
}

struct Phrase maximize_variable(struct Phrase variable_phrase,
                                struct Phrase produce) {
  struct Phrase variable_sort =
      upcoming_grammar_or_quote_found(variable_phrase);
  guarantee(variable_sort.length == 1); // later can support more complex sorts
  word_t sort_word = phrase_word_read(variable_sort, 0);
  word_t probe_word = 0;
  word_t produce_word = 0;
  txik_t sort_offset =
      (variable_phrase.length + variable_phrase.begin) - variable_sort.begin;
  switch (sort_word) {
    example(
        NUMBER_QUOTE, produce = phrase_addenda(variable_phrase, produce);
        probe_word = phrase_word_read(produce, produce.length - sort_offset);
        guarantee(probe_word == sort_word);
        phrase_word_write(0xFFFF, produce, produce.length - sort_offset + 1));
    example(
        LETTER_QUOTE, produce = phrase_addenda(variable_phrase, produce);
        probe_word = phrase_word_read(produce, produce.length - sort_offset);
        guarantee(probe_word == sort_word);
        phrase_word_write(0xFFFF, produce, produce.length - sort_offset + 1));
    example(boolean_GRAMMAR, produce = phrase_addenda(variable_phrase, produce);
            probe_word =
                phrase_word_read(produce, produce.length - sort_offset - 1);
            guarantee(probe_word == truth_WORD || probe_word == lie_WORD);
            produce_word = truth_WORD; phrase_word_write(
                produce_word, produce, produce.length - sort_offset - 1));
  default:
    DEBUGPRINT(("%X unsupported type", sort_word));
    guarantee(1 == 0);
  }
  return produce;
}
struct Phrase /*produce*/ gika_invert_interpret(struct Phrase knowledge,
                                                struct Phrase input_phrase,
                                                struct Phrase produce) {
  // first we get the gika variable
  // then we put that into a yigika sentence
  // and  pass it to base_command_interpret
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  struct Phrase variable_phrase =
      neo_retrospective_phrase_situate(variable_htin, accusative_case_GRAMMAR);
  NewPagePhrase(max_variable, 1);
  max_variable = maximize_variable(variable_phrase, max_variable);
  max_variable.length -= 1;

  produce = phrase_addenda(max_variable, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(instrumental_case_GRAMMAR, produce);
  produce = addenda_thePhrase_toThePhrase(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(exclusive_or_WORD, produce);
  produce = addenda_theGrammarWord_toThePhrase(deontic_mood_GRAMMAR, produce);
  struct Phrase produce_remainder = produce;
  guarantee(produce.page.plength >= 2);
  produce_remainder.begin =
      ((produce.begin + produce.length) | LINE_LONG_MASK) + 2;
  produce_remainder.length = 0;
  produce_remainder =
      base_command_interpret(knowledge, produce, produce_remainder);
  return produce_remainder;
}

struct Phrase /*produce*/ bluhyugika_xor_interpret(struct Phrase knowledge,
                                                   struct Phrase input_phrase,
                                                   struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(variable_htin, accusative_case_GRAMMAR);
  hkak = phrase_word_read(hkak_phrase, 0);
  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = phrase_word_read(hyak_phrase, 0);
  }
  hkak = (hyak == truth_WORD && hkak == truth_WORD) ||
                 (hyak == lie_WORD && hkak == lie_WORD)
             ? lie_WORD
             : truth_WORD;
  produce = phrase_addenda(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(hkak, produce);
  produce = addenda_theWord_toThePhrase(boolean_GRAMMAR, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, produce);
  return produce;
}

struct Phrase /*produce*/ yugika_xor_interpret(struct Phrase knowledge,
                                               struct Phrase input_phrase,
                                               struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  phrase_print(variable_htin);
  hkak = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);
  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
    hkak = number_xor(hkak, hyak);
  }
  produce = phrase_addenda(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(NUMBER_QUOTE, produce);
  produce = addenda_theWord_toThePhrase(hkak, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, produce);
  text_phrase_print(produce);
  return produce;
}

struct Phrase /*produce*/ yidoka_xor_interpret(struct Phrase knowledge,
                                               struct Phrase input_phrase,
                                               struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  text_phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  text_phrase_print(hkak_phrase);
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    // get variable sentence from knowledge
    DEBUGPRINT(("%X hkak, %X htik\n", hkak, htik));
    hkak = number_xor(hkak, htik);
  }
  DEBUGPRINT(
      ("%X produce.length, %X produce.begin\n", produce.length, produce.begin));
  produce = addenda_thePhrase_toThePhrase(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(NUMBER_QUOTE, produce);
  DEBUGPRINT(
      ("%X produce.length, %X produce.begin\n", produce.length, produce.begin));
  produce = addenda_theWord_toThePhrase(hkak, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, produce);
  DEBUGPRINT(
      ("%X produce.length, %X produce.begin\n", produce.length, produce.begin));
  text_phrase_print(produce);
  text_page_print(produce.page);
  return produce;
}

struct Phrase /*produce*/ yugika_divide_interpret(struct Phrase knowledge,
                                                  struct Phrase input_phrase,
                                                  struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  hkak = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);
  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
    hkak = number_divide(hkak, hyak);
  }
  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
  return produce;
}

struct Phrase /*produce*/ yugika_subtract_interpret(struct Phrase knowledge,
                                                    struct Phrase input_phrase,
                                                    struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  text_phrase_print(knowledge);
  text_phrase_print(input_phrase);
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  hkak = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);
  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
    hkak = number_subtract(hkak, hyak);
  }
  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
  return produce;
}

struct Phrase /*produce*/ yugika_multiply_interpret(struct Phrase knowledge,
                                                    struct Phrase input_phrase,
                                                    struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  hkak = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);
  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
    hkak = number_multiply(hkak, hyak);
  }
  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, hyak));
  return produce;
}

struct Phrase /*produce*/ yugika_plus_interpret(struct Phrase knowledge,
                                                struct Phrase input_phrase,
                                                struct Phrase produce) {
  // first need to identify the type of the gika
  // then run it through a case thing, and output the proper return value
  // for now it is all short uints... so we can assume that
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  hkak = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);

  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
    hkak = number_plus(hkak, hyak);
  }
  //  DEBUGPRINT("0x%X hyak found\n", hyak);

  produce = addenda_thePhrase_toThePhrase(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(NUMBER_QUOTE, produce);
  produce = addenda_theWord_toThePhrase(hkak, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, produce);
  return produce;
}
struct Phrase /*produce*/ yibluhka_invert_interpret(struct Phrase knowledge,
                                                    struct Phrase input_phrase,
                                                    struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  word_t htik = truth_WORD;
  word_t hkak = lie_WORD;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  if (variable_htin.length > 0) { // we want to make sure it is same as ka,
    // so as not to lose information
    htik = phrase_word_read(variable_htin, 0);
  }
  Page_print(knowledge.page);
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  text_phrase_print(input_phrase);
  if (hkak_phrase.begin != 0) {
    hkak = phrase_word_read(hkak_phrase, 0);
  }
  guarantee(hkak == truth_WORD || hkak == lie_WORD);

  conditional(hkak == truth_WORD, htik = lie_WORD);
  conditional(hkak == lie_WORD, htik = truth_WORD);

  produce.begin = 1;
  neo_extract_words_ins_htinka_addenda(name, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  text_phrase_print(produce);
  Page_print(produce.page);
  produce = word_addenda(htik, produce);
  produce = word_addenda(boolean_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  return produce;
}

struct Phrase /*produce*/ yika_invert_interpret(struct Phrase knowledge,
                                                struct Phrase input_phrase,
                                                struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    hkak = number_neg(hkak);
    hkak = number_plus(hkak, htik);
  }
  produce = phrase_addenda(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(NUMBER_QUOTE, produce);
  produce = addenda_theWord_toThePhrase(hkak, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, produce);
  text_phrase_print(produce);
  return produce;
}

struct Phrase /*produce*/ yuyika_xor_interpret(struct Phrase knowledge,
                                               struct Phrase input_phrase,
                                               struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);
  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
  }
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
    hkak = number_xor(hkak, hyak);
    hkak = number_plus(hkak, htik);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }
  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
  return produce;
}
struct Phrase /*produce*/ yuyika_divide_interpret(struct Phrase knowledge,
                                                  struct Phrase input_phrase,
                                                  struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);

  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
  }
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
    hkak = number_divide(hkak, hyak);
    hkak = number_plus(hkak, htik);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }

  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
  return produce;
}
struct Phrase /*produce*/ yuyika_multiply_interpret(struct Phrase knowledge,
                                                    struct Phrase input_phrase,
                                                    struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);

  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
  }
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
    hkak = number_multiply(hkak, hyak);
    hkak = number_plus(hkak, htik);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }

  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
  return produce;
}
struct Phrase /*produce*/ yuyika_plus_interpret(struct Phrase knowledge,
                                                struct Phrase input_phrase,
                                                struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);

  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
  }
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
    hkak = number_plus(hkak, hyak);
    hkak = number_plus(hkak, htik);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }

  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
  return produce;
}
struct Phrase /*produce*/
yuyika_letter_subtract_interpret(struct Phrase knowledge,
                                 struct Phrase input_phrase,
                                 struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  uint16_t htik = 0;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  if (variable_htin.length > 0) {
    htik = phrase_letter_extract(variable_htin, accusative_case_GRAMMAR);
  }

  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
  }
  // DEBUGPRINT(("%X hyak, %c hyak\n", hyak, (unsigned char) hyak));
  // text_phrase_print(input_phrase);
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  // DEBUGPRINT(
  //     ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin,
  //     phrase.length));
  // DEBUGPRINT(("0x%X hkak_phrase.begin, 0x%X hkak_phrase.length\n",
  //            hkak_phrase.begin, hkak_phrase.length));
  // text_phrase_print(hkak_phrase);
  // text_page_print(hkak_phrase.page);
  // Page_print(hkak_phrase.page);
  if (hkak_phrase.begin != 0) {
    hkak = phrase_letter_extract(hkak_phrase, accusative_case_GRAMMAR);
    // DEBUGPRINT(("%X hkak, %c hkak\n", hkak, (unsigned char) hkak));
    // DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
    hkak = number_subtract(hkak, hyak);
    hkak = number_plus(hkak, htik);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }

  produce = phrase_addenda(name, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, produce);
  produce = addenda_theWord_toThePhrase(LETTER_QUOTE, produce);
  produce = addenda_theWord_toThePhrase(hkak, produce);
  produce =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, produce);
  return produce;
}
struct Phrase /*produce*/ yuyika_subtract_interpret(struct Phrase knowledge,
                                                    struct Phrase input_phrase,
                                                    struct Phrase produce) {
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  // uint16_t knowledge_final_line = tablet_final_line_find(knowledge.page);
  // uint16_t knowledge_indexFinger = knowledge_final_line * LINE_LONG - 1;
  uint16_t htik = 0;
  uint16_t hyak = 0;
  uint16_t hkak = 0;
  txik_t input_indexFinger = input_phrase.begin + input_phrase.length;
  DEBUGPRINT(("0x%X input_indexFinger\n", input_indexFinger));
  Page_print(input_phrase.page);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  // get current value in dative case
  struct Phrase name = phrase;
  name.length = phrase.length - 1;
  phrase_print(name);
  struct Phrase variable_htin =
      retrospective_variable_htin_found(knowledge, name);
  DEBUGPRINT(("%s\n", "hello"));
  phrase_print(variable_htin);
  htik = phrase_number_extract(variable_htin, accusative_case_GRAMMAR);
  Page_print(knowledge.page);

  struct Phrase hyak_phrase =
      neo_retrospective_phrase_situate(input_phrase, instrumental_case_GRAMMAR);
  if (hyak_phrase.begin != 0) {
    hyak = neo_number_extract(input_phrase, hyak_phrase);
  }
  struct Phrase hkak_phrase =
      neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("0x%X phrase.begin, 0x%X phrase.length\n", phrase.begin, phrase.length));
  if (hkak_phrase.begin != 0) {
    hkak = neo_number_extract(input_phrase, hkak_phrase);
    DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
    hkak = number_subtract(hkak, hyak);
    hkak = number_plus(hkak, htik);
    //  DEBUGPRINT("0x%X hyak found\n", hyak);
  }

  neo_extract_words_ins_htinka_addenda(name, produce);
  neo_literal_grammar_word_ins_htinka_addenda(nominative_case_GRAMMAR, produce);
  literal_number_ins_htinka_addenda(hkak, produce);
  neo_literal_grammar_word_ins_htinka_addenda(accusative_case_GRAMMAR, produce);
  neo_literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce);
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("0x%X hkak, 0x%X hyak\n", hkak, htik));
  return produce;
}

void subtract_interpret(const line_t input, const uint8_t program_long,
                        const line_t *program,
                        const uint16_t tablet_indexFinger, line_t *produce) {
  // find instrumental case, to find how much to add
  // if no instrumental case, add one
  uint16_t phrase_begin = 0;
  uint8_t phrase_long = 0;
  uint16_t hyak = 0;
  uint16_t htik = 0;
  retrospective_phrase_situate(program_long, program, tablet_indexFinger,
                               instrumental_case_GRAMMAR, &phrase_long,
                               &phrase_begin);
  // DEBUGPRINT("0x%X phrase_begin, 0x%X phrase_long", phrase_begin,
  // phrase_long);
  if (phrase_begin != 0) {
    hyak = number_extract(program_long, program, phrase_begin, phrase_long);
#ifdef ARRAYLINE
    htik = input[0] - hyak;
    if (htik <= input[0]) {
      produce[0][0] = htik;
    } else {
      produce[0][0] = 0;
    }
#else
    htik = input.s0 - hyak;
    if (htik <= input.s0) {
      produce[0].s0 = htik;
    } else {
      produce[0].s0 = 0;
    }
#endif
    // DEBUGPRINT("0x%X hyak found\n", hyak);
  } else {
#ifdef ARRAYLINE
    produce[0][0] = input[0] - 1;
#else
    produce[0].s0 = input.s0 - 1;
#endif
  }
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
}
// void return_interpret(const line_t input, const uint8_t program_long,
//                      const line_t *program, line_t *produce) {
//  // deprecated
//  guarantee(program != NULL);
//  guarantee(program_long != 0);
//  *produce = input;
//}
void invert_interpret(const line_t input, const uint8_t program_long,
                      const line_t *program, const uint16_t tablet_indexFinger,
                      line_t *produce) {
  guarantee(program != NULL);
  guarantee(program_long != 0);
  guarantee(tablet_indexFinger != 0);
// dat is inverse of acc
#ifdef ARRAYLINE
  produce[0][0] = ~input[0];
#else
  produce[0].s0 = ~input.s0;
#endif
}
void down_conditional_interpret(const line_t input, const uint8_t program_long,
                                const line_t *program,
                                const uint16_t input_tablet_indexFinger,
                                line_t *produce) {
  // down conditional with ablative case
  guarantee(program != NULL);
  guarantee(program_long != 0);
  guarantee(produce != NULL);
  // guarantee(input.s0 != 0);
  // find ablative case, to find how much down
  uint16_t phrase_begin = 0;
  uint8_t phrase_long = 0;
  uint16_t hpik = 0;
  retrospective_phrase_situate(program_long, program, input_tablet_indexFinger,
                               ablative_case_GRAMMAR, &phrase_long,
                               &phrase_begin);
  // DEBUGPRINT("0x%X phrase_begin, 0x%X phrase_long, 0x%X
  // tablet_indexFinger\n", phrase_begin,
  // phrase_long, input_tablet_indexFinger);
  //  tablet_print(program_long, program);
  if (phrase_begin != 0) {
    hpik = tablet_read(phrase_begin + 1, program_long, program);
#ifdef ARRAYLINE
    if (input[0] < hpik) {
      produce[0][0] = truth_WORD;
    } else {
      produce[0][0] = lie_WORD;
    }
#else
    if (input.s0 < hpik) {
      produce[0].s0 = truth_WORD;
    } else {
      produce[0].s0 = lie_WORD;
    }
#endif
    // DEBUGPRINT("0x%X hpik found\n", hpik);
  } else {
    tablet_print(program_long, program);
    guarantee(1 == 0);
  }
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
}

void high_conditional_interpret(const line_t input, const uint8_t program_long,
                                const line_t *program,
                                const uint16_t tablet_indexFinger,
                                line_t *produce) {
  // down conditional with ablative case
  guarantee(program != NULL);
  guarantee(program_long != 0);
  guarantee(produce != NULL);
  // guarantee(input.s0 != 0);
  // find ablative case, to find how much down
  uint16_t phrase_begin = 0;
  uint8_t phrase_long = 0;
  uint16_t hpik = 0;
  retrospective_phrase_situate(program_long, program, tablet_indexFinger,
                               ablative_case_GRAMMAR, &phrase_long,
                               &phrase_begin);
  // DEBUGPRINT("0x%X phrase_begin, 0x%X phrase_long\n", phrase_begin,
  // phrase_long);
  if (phrase_begin != 0) {
    hpik = tablet_read(phrase_begin + 1, program_long, program);
#ifdef ARRAYLINE
    if (input[0] > hpik) {
      produce[0][0] = truth_WORD;
    } else {
      produce[0][0] = lie_WORD;
    }
#else
    if (input.s0 > hpik) {
      produce[0].s0 = truth_WORD;
    } else {
      produce[0].s0 = lie_WORD;
    }
#endif
  } else {
    // DEBUGPRINT("0x%X hpik found\n", hpik);
    guarantee(1 == 0);
  }
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
}

// variable_declaration_by_name_found
//

// uint16_t /*produce_long*/ var_acc_num_ins_plus_interpret(
//    uint16_t knowledge_attribute[], line_t *knowledge,
//    const uint16_t input_long, const line_t input[],
//    const uint16_t max_produce_long, line_t produce[]) {
//  /* find variable, construct full sentence and pass to appropriate handler
//  */
//
//  //uint16_t knowledge_long = knowledge_attribute[Sequence_finally];
//  //
//  // get acc contents (variable name)
//  //
//  // nominal_value_by_name_found() {
//  // }
//  //
//  // get the value
//  // construct sentence
//  // pass to appropriate handler
//  //
//  // retrospective_phrase_situate(knowledge_long, knowledge,
//  // nominative_case_GRAMMAR);
//  // return tablet_copy(input_long, input, max_produce_long, produce);
//  // retrospective_phrase_situate(knowledge_long, knowledge,
//  // nominative_case_GRAMMAR);
//  return 0;
//}
struct Phrase /*produce*/
dopwih_doka_giant_interpret(const struct Phrase knowledge,
                            const struct Phrase input, struct Phrase produce) {

  guarantee(input.page.lines[0][0] != 0);
  guarantee(knowledge.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  DEBUGPRINT(("0x%X input\n", input.page.lines[0][0]));
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  uint16_t acc_number = neo_number_extract(input, phrase);
  phrase = neo_retrospective_phrase_situate(input, ablative_case_GRAMMAR);
  uint16_t abl_number = neo_number_extract(input, phrase);
  word_t produce_boolean = acc_number > abl_number ? truth_WORD : lie_WORD;
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = produce_boolean;
  (*produce.page.lines)[2] = boolean_GRAMMAR;
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
  return produce;
}

struct Phrase /*produce*/
boolean_interpret(const struct Phrase knowledge, const struct Phrase input,
                  struct Phrase produce) {

  guarantee(input.page.lines[0][0] != 0);
  guarantee(knowledge.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  phrase_addenda(input, produce);
  return produce;
}

struct Phrase /*produce*/
dopwih_prihzika_giant_interpret(const struct Phrase knowledge,
                                const struct Phrase input,
                                struct Phrase produce) {

  guarantee(input.page.lines[0][0] != 0);
  guarantee(knowledge.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  uint16_t acc_number = 0;
  conditional(phrase.length > 0, acc_number = phrase_letter_extract(
                                     input, accusative_case_GRAMMAR));
  phrase = neo_retrospective_phrase_situate(input, ablative_case_GRAMMAR);
  uint16_t abl_number = neo_number_extract(input, phrase);
  word_t produce_boolean = acc_number > abl_number ? truth_WORD : lie_WORD;
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = produce_boolean;
  (*produce.page.lines)[2] = boolean_GRAMMAR;
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
  return produce;
}

struct Phrase /*produce*/
kayu_subtract_interpret(const struct Phrase knowledge,
                        const struct Phrase input, struct Phrase produce) {
  guarantee(input.page.lines[0][0] != 0);
  guarantee(knowledge.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // DEBUGPRINT(("0x%X input\n", input.page.lines[0][0]));
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  uint16_t acc_number = neo_number_extract(input, phrase);
  phrase = neo_retrospective_phrase_situate(input, instrumental_case_GRAMMAR);
  uint16_t ins_number = neo_number_extract(input, phrase);
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = NUMBER_QUOTE;
  (*produce.page.lines)[2] = number_subtract(acc_number, ins_number);
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
  // Page_print(produce.page);
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
  return produce;
}
struct Phrase /*produce*/
kayu_multiply_interpret(const struct Phrase knowledge,
                        const struct Phrase input, struct Phrase produce) {
  guarantee(input.page.lines[0][0] != 0);
  guarantee(knowledge.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
  // DEBUGPRINT(("0x%X input\n", input.page.lines[0][0]));
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  uint16_t acc_number = neo_number_extract(input, phrase);
  phrase = neo_retrospective_phrase_situate(input, instrumental_case_GRAMMAR);
  uint16_t ins_number = neo_number_extract(input, phrase);
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = NUMBER_QUOTE;
  (*produce.page.lines)[2] = number_multiply(acc_number, ins_number);
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
  // Page_print(produce.page);
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
  return produce;
}
struct Phrase /*produce*/
neo_num_acc_ins_plus_interpret(const struct Phrase knowledge,
                               const struct Phrase input,
                               struct Phrase produce) {
#ifdef ARRAYLINE
  guarantee(input.page.lines[0][0] != 0);
#else
  guarantee(input.s0 != 0);
#endif
  guarantee(knowledge.page.lines != NULL);
  guarantee(produce.page.lines != NULL);
#ifdef ARRAYLINE
  // DEBUGPRINT(("0x%X input\n", input.page.lines[0][0]));
  struct Phrase phrase =
      neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  uint16_t acc_number = neo_number_extract(input, phrase);
  phrase = neo_retrospective_phrase_situate(input, instrumental_case_GRAMMAR);
  uint16_t ins_number = neo_number_extract(input, phrase);
#else
  DEBUGPRINT(("0x%X input\n", input.s0));
  retrospective_phrase_situate(1, &input, 15, accusative_case_GRAMMAR,
                               &phrase_begin, &phrase_long);
  uint16_t acc_number = number_extract(1, &input, phrase_begin, phrase_long);
  retrospective_phrase_situate(1, &input, 15, instrumental_case_GRAMMAR,
                               &phrase_begin, &phrase_long);
  uint16_t ins_number = number_extract(1, &input, phrase_begin, phrase_long);
#endif
#ifdef ARRAYLINE
  (*produce.page.lines)[0] = 0x19;
  (*produce.page.lines)[1] = NUMBER_QUOTE;
  (*produce.page.lines)[2] = ins_number + acc_number;
  (*produce.page.lines)[3] = accusative_case_GRAMMAR;
  (*produce.page.lines)[4] = realis_mood_GRAMMAR;
  produce.begin = 1;
  produce.length = 4;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = ins_number + acc_number;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  // Page_print(produce.page);
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
  return produce;
}

void num_acc_ins_plus_interpret(const uint16_t input_long, const line_t input[],
                                const uint8_t program_long,
                                const line_t *program,
                                const uint16_t tablet_indexFinger,
                                line_t *produce) {
  guarantee(program_long > 1);
  guarantee(tablet_indexFinger != 0);
#ifdef ARRAYLINE
  guarantee(input[0] != 0);
#else
  guarantee(input.s0 != 0);
#endif
  guarantee(program != NULL);
  guarantee(produce != NULL);
  uint16_t phrase_begin = 0;
  uint8_t phrase_long = 0;
#ifdef ARRAYLINE
  DEBUGPRINT(("0x%X input\n", input[0][0]));
  retrospective_phrase_situate((uint8_t)input_long, input, 15,
                               accusative_case_GRAMMAR, &phrase_long,
                               &phrase_begin);
  uint16_t acc_number =
      number_extract(input_long, input, phrase_begin, phrase_long);
  retrospective_phrase_situate((uint8_t)input_long, input, 15,
                               instrumental_case_GRAMMAR, &phrase_long,
                               &phrase_begin);
  uint16_t ins_number =
      number_extract(input_long, input, phrase_begin, phrase_long);
#else
  DEBUGPRINT(("0x%X input\n", input.s0));
  retrospective_phrase_situate(1, &input, 15, accusative_case_GRAMMAR,
                               &phrase_begin, &phrase_long);
  uint16_t acc_number = number_extract(1, &input, phrase_begin, phrase_long);
  retrospective_phrase_situate(1, &input, 15, instrumental_case_GRAMMAR,
                               &phrase_begin, &phrase_long);
  uint16_t ins_number = number_extract(1, &input, phrase_begin, phrase_long);
#endif
#ifdef ARRAYLINE
  (*produce)[0] = 0x19;
  (*produce)[1] = NUMBER_QUOTE;
  (*produce)[2] = ins_number + acc_number;
  (*produce)[3] = accusative_case_GRAMMAR;
  (*produce)[4] = realis_mood_GRAMMAR;
#else
  (*produce).s0 = 0x19;
  (*produce).s1 = NUMBER_QUOTE;
  (*produce).s2 = ins_number + acc_number;
  (*produce).s3 = accusative_case_GRAMMAR;
  (*produce).s4 = realis_mood_GRAMMAR;
#endif
  // DEBUGPRINT("0x%X input, 0x%X produce\n", input.s0, produce[0].s0);
}

uint16_t connector_probe(const uint8_t max_program_long, const line_t *program,
                         const uint16_t tablet_indexFinger,
                         uint16_t *connector_indexFinger) {
  guarantee(program != NULL);
  guarantee(max_program_long > 0);
  guarantee(tablet_indexFinger < LINE_LONG * max_program_long);
  guarantee(connector_indexFinger != NULL);
  // connector either directly after current tablet_indexFinger,
  uint16_t word = 0;
  uint16_t connector = 0;
  word = tablet_upcoming_word_read(tablet_indexFinger + 1, max_program_long,
                                   program, connector_indexFinger);
  switch (word) {
  case and_GRAMMAR:
    connector = word;
    break;
  case and_or_GRAMMAR:
    connector = word;
    break;
  case exclusive_or_GRAMMAR:
    connector = word;
    break;
  case negatory_quantifier_GRAMMAR:
    connector = word;
    break;
  default:
    *connector_indexFinger = 0;
  }
  // optional:
  //  or if remainder of tablet is blank, then the next word in next tablet
  return connector;
}

uint16_t /*produce_long*/ realisMood_interpret(const uint16_t input_long,
                                               const line_t input[],
                                               const uint16_t max_produce_long,
                                               line_t produce[]) {
  return tablet_copy(input_long, input, max_produce_long, produce);
}
struct Phrase /*produce*/ neo_realisMood_interpret(struct Phrase input,
                                                   struct Phrase produce) {
  return phrase_addenda(input, produce);
}

/* if next phrase is has a named argument,
 * then it attempts to look up the name's worth
 * if it defined then it replaces the name with its worth in produce.
 * otherwise it simply adds the phrase to produce
 */
uint16_t /*produce_final_indexFinger_plus*/
upcoming_variable_to_worth_translate(
    const uint16_t knowledge_long, const line_t knowledge[],
    const uint16_t input_long, const line_t input[], uint16_t *indexFinger,
    const uint16_t produce_indexFinger, const uint16_t produce_long,
    line_t produce[]) {
  guarantee(produce_indexFinger < produce_long * LINE_LONG);
  // get first variable
  guarantee(indexFinger != NULL);
  guarantee(*indexFinger < input_long * LINE_LONG);
  guarantee(*indexFinger > 0);
  uint16_t phrase_begin = *indexFinger;
  uint16_t phrase_long =
      first_phrase_long_found(phrase_begin, input_long, input, indexFinger);
  guarantee(*indexFinger >= phrase_begin);
  if (!(*indexFinger < input_long * LINE_LONG))
    return 0;
  guarantee(*indexFinger < input_long * LINE_LONG);
  uint16_t phrase_case_indexFinger = 0;
  // DEBUGPRINT(("0x%X phrase_begin, 0x%X phrase_long, 0x%X input_long\n",
  //      phrase_begin, phrase_long, input_long));
  // tablet_print(input_long, input);
  if (phrase_long == 0) {
    return 0;
  }
  // DEBUGPRINT(("0x%X *indexFinger + phrase_long - 1\n", *indexFinger +
  // phrase_long - 1));
  // DEBUGPRINT(("0x%X *indexFinger, 0x%X phrase_long, 0x%X input_long",
  //            *indexFinger, phrase_long, input_long));
  // tablet_print(input_long, input);
  uint16_t phrase_case = tablet_retrospective_grammar_read(
      *indexFinger, (uint8_t)input_long, input, &phrase_case_indexFinger);
  // DEBUGPRINT(
  //   ("0x%X phrase_begin, 0x%X phrase_long\n", phrase_begin, phrase_long));
  // if is dative_case then return as is
  // identify if it is a variable by checking that end of phrase -1 is
  // name_GRAMMAR
  uint16_t name_indexFinger = phrase_long + phrase_begin - 2;
  // DEBUGPRINT(("0x%X name_indexFinger\n", name_indexFinger));
  // DEBUGPRINT(("0x%X phrase_case\n", phrase_case));
  if (tablet_retrospective_word_read(name_indexFinger, (uint8_t)input_long,
                                     input,
                                     &name_indexFinger) != name_GRAMMAR ||
      phrase_case == dative_case_GRAMMAR) {
    // copy phrase as is and return length
    // DEBUGPRINT(
    //    ("0x%X phrase_long, 0x%X phrase_begin\n", phrase_long,
    //    phrase_begin));
    // tablet_print(input_long, input);
    extract_words_ins_htinka_addenda(input_long, input, phrase_long,
                                     phrase_begin, produce_long, produce);
    return phrase_long;
  }
  //  // found the name, now create the nominative_case phrase, and search for
  //  // declaration in knowledge or input, check input first.
  struct Page knowledge_tablet;
  knowledge_tablet.lines = (line_t *)knowledge;
  knowledge_tablet.plength = (uint8_t)(uint8_t)knowledge_long;
  PagePhrase(knowledge_phrase, knowledge_tablet);

  struct Page input_tablet;
  input_tablet.lines = (line_t *)input;
  input_tablet.plength = (uint8_t)(uint8_t)input_long;
  PagePhrase(input_phrase, input_tablet);

  struct Page variable_tablet;
  variable_tablet.plength = (uint8_t)(uint8_t)input_long;
  variable_tablet.lines = (line_t *)input;
  struct Phrase variable_name;
  variable_name.page = variable_tablet;
  variable_name.length = (uint8_t)(phrase_long - 1);
  variable_name.begin = phrase_begin;

  struct Phrase variable_htin =
      retrospective_variable_htin_found(input_phrase, variable_name);
  conditional(variable_htin.length == 0,
              variable_htin = retrospective_variable_htin_found(
                  knowledge_phrase, variable_name));
  // DEBUGPRINT(("%X variable_htin.length\n", variable_htin.length));
  // text_phrase_print(variable_htin);
  // uint16_t cousin_long = cousin.length;
  // uint16_t cousin_begin = cousin.begin;
  // cousin_long = retrospective_htin_cousin_found(
  //    question_long, &question, knowledge_long, knowledge, &cousin_begin);
  // cousin_long =
  // DEBUGPRINT(("0x%X cousin_begin, 0x%X cousin_long\n", cousin_begin,
  // cousin_long));
  // if no cousin found then leave as is and continue
  if (variable_htin.length == 0) {
    extract_words_ins_htinka_addenda(input_long, input, phrase_long,
                                     phrase_begin, produce_long, produce);
    return phrase_long;
  } else {
    // tablet_print(cousin_long, knowledge+cousin_begin);
  }

  // check if value is set (accusative_case) phrase situate
  //   if not available then return 0;
  uint16_t worth_phrase_begin = 0;
  uint8_t worth_phrase_long = 0;
  phrase_situate(variable_htin.page.plength, variable_htin.page.lines,
                 accusative_case_GRAMMAR, &worth_phrase_long,
                 &worth_phrase_begin);
  // DEBUGPRINT(("0x%X worth_phrase_long, 0x%X worth_phrase_begin\n",
  //             worth_phrase_long, worth_phrase_begin));
  conditional(worth_phrase_long == 0, return 0);
  // get the value and add it to the produce
  //  assume it is everything up until the case
  //

  extract_words_ins_htinka_addenda(
      variable_htin.page.plength, variable_htin.page.lines,
      worth_phrase_long - 1, worth_phrase_begin, produce_long, produce);
  // tablet_print(produce_long, produce);
  literal_grammar_word_ins_htinka_addenda(phrase_case, produce_long, produce);
  struct Page produce_page;
  produce_page.lines = produce;
  produce_page.plength = produce_long;
  // tablet_print(produce_long, produce);
  // text_page_print(produce_page);
  //
  return worth_phrase_long;
}

struct Phrase /* produce */
neo_all_variable_to_worth_translate(const struct Phrase knowledge,
                                    const struct Phrase input,
                                    struct Phrase produce) {
  conditional(input.length == 0, return produce);
  // only work with first independent clause
  struct Phrase first_htin = forward_htin_found(input);
  NewPagePhrase(temporary_produce, 1);
  struct Phrase found_phrase;
  struct Phrase found_gvak;
  struct Phrase input_remains = first_htin;
  // if dative_case found then do all except that
  const struct Phrase dative_phrase =
      neo_retrospective_phrase_situate(input, dative_case_GRAMMAR);
  // DEBUGPRINT(("%X dative_phrase.length\n", dative_phrase.length));
  // text_phrase_print(dative_phrase);
  phrase_zero(produce);
  struct Phrase perspective_phrase;
  // if not found, then ignore accusative case
  repeat(
      input.length, // text_phrase_print(input_remains);
      // phrase_zero(found_phrase);
      // Page_print(input_remains.page); found_phrase.length = 0;
      // text_phrase_print(found_phrase);
      // DEBUGPRINT(("%X input_remains.begin, %X input_remains.length\n",
      //              input_remains.begin, input_remains.length));
      found_phrase = upcoming_phrase_found(input_remains);
      perspective_phrase = upcoming_perspective_found(input_remains);
      word_t perspective = phrase_word_read(perspective_phrase, 0);
      // text_phrase_print(found_phrase);
      // text_phrase_print(produce);
      // text_phrase_print(input_remains);
      conditional(found_phrase.length == 0,
                  produce = phrase_addenda(input_remains, produce);
                  break);
      guarantee(found_phrase.begin >= input_remains.begin);
      input_remains.begin = found_phrase.begin + found_phrase.length;
      input_remains.length =
          (first_htin.begin + first_htin.length) - input_remains.begin;
      // text_phrase_print(found_phrase); Page_print(found_phrase.page);
      found_gvak = retrospective_gvak_found(found_phrase);
      // text_phrase_print(found_gvak);
      if (perspective != conditional_mood_GRAMMAR &&
          dative_phrase.length == 0 &&
          phrase_word_read(found_gvak, 0) == accusative_case_GRAMMAR) {
        produce = phrase_addenda(found_phrase, produce);
        continue;
      } phrase_zero(temporary_produce);
      //text_phrase_print(found_phrase);
      temporary_produce = neo_upcoming_variable_to_worth_translate(
          knowledge, found_phrase, temporary_produce);
      //text_phrase_print(temporary_produce);
      if (temporary_produce.length == 0) break; // text_phrase_print(produce);
      if (is_thePhrase_aQuote(temporary_produce)) {
       // text_phrase_print(temporary_produce);
        //text_phrase_print(produce);
        produce = quote_addenda(temporary_produce, produce);
      } else { produce = phrase_addenda(temporary_produce, produce); });
  return produce;
}

uint16_t /*produce_long*/
interrogativeMood_interpret(const uint16_t input_long, const line_t input[],
                            const uint16_t max_produce_long, line_t produce[]) {
  /* algorithm:
   * go through knowledge to find matching sentence, with hwat as wild card,
   * return matching sentences.
   */
  // uint16_t knowledge_long = knowledge_attribute[Sequence_finally];
  // retrospective_phrase_situate(knowledge_long, knowledge,
  // nominative_case_GRAMMAR);
  return tablet_copy(input_long, input, max_produce_long, produce);
}
struct Phrase /*produce*/
neo_interrogativeMood_interpret(const struct Phrase input,
                                struct Phrase produce) {
  return phrase_addenda(input, produce);
}

struct Phrase /* produce */
base_command_interpret(const struct Phrase knowledge, const struct Phrase input,
                       struct Phrase produce) {
  // get code name
  txik_t txik = input.begin;
  // code_name_t code_name = neo_code_name_derive(input, &txik);
  code_name_t code_name = code_name_derive(input);
  // text_page_print(input.page);
  // text_phrase_print(input);
  // DEBUGPRINT(("%X code_name\n", code_name));
  switch (code_name) {
    // example(0xB6B4A19F,
    //         produce = neo_plus_interpret(knowledge, input, produce));
    example(0x55BF9003, // yu plustu
            produce = neo_plus_interpret(knowledge, input, produce));
    example(0x3D81E3DC, // yi yu plustu
            produce = dat_plus_interpret(knowledge, input, produce));
    example(0x6EEB831E, // ka yu plustu
            produce =
                neo_num_acc_ins_plus_interpret(knowledge, input, produce));
    example(0xDCFF47C9, // ka yu yi plustu
            produce = yuyika_plus_interpret(knowledge, input, produce));
    example(0x3E510DCB, // yu gika plustu
            produce = yugika_plus_interpret(knowledge, input, produce));
    example(0xA851174F, // doyu grettu - subtracy by
            produce = neo_subtract_interpret(knowledge, input, produce));
    example(0xF965E237, // doka doyu grettu -- subtract ka by
            produce = kayu_subtract_interpret(knowledge, input, produce));
    example(0xA9DF6CE2, // gika doyu grettu
            produce = yugika_subtract_interpret(knowledge, input, produce));
    example(0x61D5941F, // yu yi ka grettu subtract
            produce = yuyika_subtract_interpret(knowledge, input, produce));
    example(0xC209F63B, // yu yi prihka subtract
            produce =
                yuyika_letter_subtract_interpret(knowledge, input, produce));
    example(0x5543629B, // dopwih doka djancu (compare greater)
            produce = dopwih_doka_giant_interpret(knowledge, input, produce));
    example(0xC078466C, // dopwih prih.zika djancu (compare greater)
            produce =
                dopwih_prihzika_giant_interpret(knowledge, input, produce));
    example(0x944C49EA, // doyu hgoctu - multiply by
            produce = multiply_interpret(knowledge, input, produce));
    example(0x9414097E, // gika yu hgoctu (multiply)
            produce = yugika_multiply_interpret(knowledge, input, produce));
    example(0xC4AE87AB, // doka doyu hgoctu - multiply by
            produce = kayu_multiply_interpret(knowledge, input, produce));
    example(0xB330809D, // yuyika hgoctu (multiply)
            produce = yuyika_multiply_interpret(knowledge, input, produce));
    example(0x21001873, // gika yu dvistu
            produce = yugika_divide_interpret(knowledge, input, produce));
    example(0xEB9C3266, // yuyika dvistu divide
            produce = yuyika_divide_interpret(knowledge, input, produce));
    example(0xBD6DF93D, // doyu gika hkittu (xor)
            produce = yugika_xor_interpret(knowledge, input, produce));
    example(0xFF5E70DB, // bluhyu gika hkittu (xor)
            produce = bluhyugika_xor_interpret(knowledge, input, produce));
    example(0xBEBD172A,
            produce = yidoka_xor_interpret(knowledge, input, produce));
    example(0x7938E7D2, // yuyika hkittu  (xor)
            produce = yuyika_xor_interpret(knowledge, input, produce));
    example(0xA51CE5F, // gika gruttu
            produce = gika_invert_interpret(knowledge, input, produce));
    example(0x1A354E2, // yidoka gruttu
            produce = yika_invert_interpret(knowledge, input, produce));
    example(0x4390DD04, // yibluhka gruttu
            produce = yibluhka_invert_interpret(knowledge, input, produce));
    // example(0x0xA1C41945, // bluhka gruttu
    //         produce = bluhka_invert_interpret(knowledge, input, produce));
    example(0x4746DAA3, // bluhka
            produce = boolean_interpret(knowledge, input, produce));
  default:
    DEBUGPRINT(("0x%X code_name, command not found\n", code_name));
    text_phrase_print(input);
    Page_print(input.page);
    guarantee(1 == 0);
    // tablet_print(1, knowledge + knowledge_long - 1);
    // text_long = tablet_translate(1, knowledge + knowledge_long - 1,
    // fluent_WORD,
    //                             text_long, text);
  }
  // text_phrase_print(produce);
  return produce;
}

struct Phrase /* input remainder */
conditionalMood_interpret_truth(const struct Phrase input, txik_t txik) {
  ++txik;
  struct Phrase produce = input;
  produce.length = (uint8_t)((input.length + input.begin) - txik);
  produce.begin = txik;
  return produce;
}

struct Phrase /* input remainder */
conditionalMood_interpret_lie(const struct Phrase input, txik_t txik) {
  struct Phrase produce = input;
  // find next sentence and skip it
  struct Phrase search = input;
  ++txik;
  search.begin = txik;
  search.length = (uint8_t)((input.length + input.begin) - txik);
  produce = forward_htin_found(search);
  if (produce.length > 0) {
    produce.begin += produce.length;
    produce.length = (uint8_t)((input.length + input.begin) - produce.begin);
  }
  return produce;
}

struct Phrase /* input remainder */
conditionalMood_interpret(const struct Phrase knowledge,
                          const struct Phrase input) {
  // interpret the conditional sentence
  // get code for conditional clause and act accordingly.
  struct Phrase perspective = upcoming_perspective_found(input);
  conditional(phrase_word_read(perspective, 0) != conditional_mood_GRAMMAR,
              return input);
  struct Phrase conditional_clause = input;
  conditional_clause.length = (perspective.begin + 1 - input.begin);
  NewPagePhrase(produce, 1);
  struct Phrase conditional_produce =
      base_command_interpret(knowledge, conditional_clause, produce);
  word_t boolean = conditional_produce.page.lines[0][1];
  guarantee(boolean == truth_WORD || boolean == lie_WORD);
  produce = input;
  txik_t txik = perspective.begin;
  if (boolean == truth_WORD) {
    produce = conditionalMood_interpret_truth(input, txik);
    // neo_all_variable_to_worth_translate(knowledge, temp_input, produce);
  } else {
    produce = conditionalMood_interpret_lie(input, txik);
  }
  // all variables to phrase
  // phrase_print(produce);
  return produce;
}

struct Phrase /*produce phrase*/
deonticMood_interpret(struct Phrase knowledge, struct Phrase raw_input,
                      struct Phrase produce) {
  // resolve all variables
  // variable worth found
  NewPagePhrase(input, HTIN_LONG);
  // text_phrase_print(knowledge);
  // text_phrase_print(raw_input);
  // text_phrase_print(raw_input);
  input = neo_all_variable_to_worth_translate(knowledge, raw_input, input);
  // text_phrase_print(input);
  // text_phrase_print(input);
  input = conditionalMood_interpret(knowledge, input);
  // NewPagePhrase(input, HTIN_LONG);
  // text_phrase_print(input);
  // input = neo_all_variable_to_worth_translate(knowledge, input,
  //    input);
  // text_phrase_print(input);

  guarantee(input.page.lines != produce.page.lines);
  guarantee(knowledge.page.lines != produce.page.lines);
  struct Phrase neo_produce = produce;
  if (input.length == 0) {
    neo_produce.length = 0;
    return neo_produce;
  }
  // text_phrase_print(knowledge);
  neo_produce = base_command_interpret(knowledge, input, produce);
  // text_phrase_print(neo_produce);
  return neo_produce;
}

struct Phrase /*phrase*/ neo_independentClause_interpret(
    struct Paragraph knowledge_phrase, uint16_t *done,
    const struct Phrase input_phrase, struct Phrase produce_phrase) {
  // wrapper function for independent clause interpret
  guarantee(*done != truth_WORD);
  guarantee(input_phrase.page.plength > 0);
  guarantee(input_phrase.page.lines != NULL);
  guarantee(produce_phrase.page.plength > 0);
  guarantee(produce_phrase.page.lines != NULL);
  guarantee(knowledge_phrase.page.lines != produce_phrase.page.lines);
  conditional(input_phrase.length == 0, return produce_phrase);
  uint16_t perspective = 0;
  struct Phrase final_perspective = neo_final_perspective_found(input_phrase);
  // text_phrase_print(input_phrase);
  // text_phrase_print(final_perspective);
  // Page_print(final_perspective.page);
  perspective = read_thePhraseZWord_atThePlace(final_perspective, 0);
  // DEBUGPRINT(("0x%X perspective, 0x%X htin_long\n", perspective,
  // htin_long));
  if (perspective == realis_mood_GRAMMAR ||
      perspective == declarative_mood_GRAMMAR) {
    return neo_realisMood_interpret(input_phrase, produce_phrase);
  } else if (perspective == deontic_mood_GRAMMAR) {
    // text_phrase_print(input_phrase);
    // text_phrase_print(knowledge_phrase);
    produce_phrase =
        deonticMood_interpret(knowledge_phrase, input_phrase, produce_phrase);
    // text_phrase_print(produce_phrase);
    return produce_phrase;
  } else if (perspective == interrogative_mood_GRAMMAR) {
    return neo_interrogativeMood_interpret(input_phrase, produce_phrase);
  }
  // return knowledge_phrase;
  text_phrase_print(produce_phrase);
  return produce_phrase;
}
/**reform_theParagraph_byTheVariable
 *
 * comments:
2019-03-28 12:26:27     thaf    I've come to the conclusion that I need a
variable update function at this point. which is earlier than I expected.
thought I might be able to get away with only a single variable update,
but that doesn't seem to be the case.
2019-03-28 12:27:10     thaf    does anyone have any pointer or pitfalls
in terms of variable update procedures?
2019-03-28 12:28:09     thaf    simpson says I have a pre-Turing language
or something. basically I store my variables in a sentence codelet, and
then the corresponding sentence that defines a variable has to be
updated.  I've also considered atomic updates where a new sentence will
be added onto the end of the cache, and the earlier instance removed.
2019-03-28 12:36:00     thaf    the first one is easier to implement, but
the second one is better in an asynchronous environment where multiple
workers are sharing the cache.
2019-03-28 12:37:46     thaf    the main complexity is that the second
variation has a rolling cache, so it would be more complicated to read,
since it would read backwards starting from wherever the current position
is of the read-write head. alternatively I guess it could read from the
begining or something... but that has it's own problems in an
asynchronous environment, though I can't remember what they are at this
time.
2019-03-28 12:38:46     thaf    ah yeah the problem is that the most
recently written is likely the most recently used, so it would increase
seek times if had to read the whole memory from the beginig each time,
particularly in cases where there are many variables.
2019-03-28 12:43:48     thaf    I guess an atomic rolling cache update is
probably a premature optimization for the MVP, as long as the API stays
the same can update it later.

  // if there is a nominative case find matching nominative case sentence in
  // produce_phrase
  // if there is no nominative case find matching case sentence in
  // produce_phrase.
  // search from back to front.
  // replace the found sentence with the variable_phrase
  // I guess there is that cousin thing,
  //
  // if there is a verb then return original because it is not a variable
  // is this a cousin found issue or a variable update issue?
  // can make a variable htin cousin found that would exclude non-variable
  // cousins
 */
struct Paragraph
reform_theParagraph_byTheVariable(struct Paragraph produce_phrase,
                                  const struct Sentence variable_phrase) {
  // guarantee(variable_phrase.page.lines == produce_phrase.page.lines);
  if (variable_phrase.length == 0) {
    return produce_phrase;
  }
  struct Sentence cousin =
      retrospective_variable_htin_cousin_found(variable_phrase, produce_phrase);
  // wipe cousin
  conditional(cousin.length > 0, produce_phrase = hollow_thePhrase_byThePhrase(
                                     produce_phrase, cousin));
  // addenda new sentence
  // // maybe an issue with phrase addenda?
  produce_phrase = phrase_addenda(variable_phrase, produce_phrase);
  return produce_phrase;
}

/** drops first htin
 */
struct Paragraph drop_htin(struct Paragraph paragraph, const uint16_t quantum) {
  struct Paragraph gross_input = paragraph;
  struct Paragraph htin;
  uint16_t htin_lines = 0;
  uint16_t new_begin = 0;
  uint16_t subtraction = 0;
  repeat(quantum, htin = forward_htin_found(gross_input);
         htin_lines = (uint16_t)(htin.begin + htin.length) / LINE_LONG;
         new_begin = (uint16_t)(htin_lines + 1) * LINE_LONG + 1;
         subtraction = number_subtract(new_begin, gross_input.begin);
         gross_input.begin += subtraction;
         gross_input.length =
             number_subtract(gross_input.length, subtraction););
  return gross_input;
}

struct Sentence /*psashtin*/ hkuttyafmwah_psashtinyi_hkinhtinka_tyiftu(struct Paragraph hkut, 
		struct Sentence psas, const struct Sentence nrup){
  return psas;
}

/** knowledge interpret, interprets a sequence of program statements, and
 * appends the results to knowledge, the final result is also found in
 * produce_phrase.
 * @param knowledge -- the transaction history
 * @param program -- the queue for program statements
 * @param produce -- where the final result will be output
 * @return produce -- returns the final produce struct
 */
// uint16_t /* produce_long*/
struct Paragraph knowledge_interpret(struct Paragraph knowledge_phrase,
                                     const struct Phrase input_phrase,
                                     struct Phrase produce_phrase) {
  struct Page input_page = input_phrase.page;
  guarantee(knowledge_phrase.page.plength > 0);
  guarantee(input_page.plength > 0);
  guarantee(produce_phrase.page.plength > 0);
  guarantee(knowledge_phrase.page.lines != NULL);
  guarantee(knowledge_phrase.page.lines != produce_phrase.page.lines);
  guarantee(input_page.lines != NULL);
  guarantee(produce_phrase.page.lines != NULL);
  // input and produce has to be an independentClause
  // gather types, cases, verb and mood, for hash (code_name_derive)
  // then run proper function based on hash name
  // if not final then repeat after mood finished
  // code_name_derive should catch conditionals
  uint8_t iteration = 0;
  uint16_t done = lie_WORD;
  //text_page_print(input_page);
  uint16_t input_long =
      knowledge_long_found(input_page.plength, input_page.lines);
  NewPagePhrase(variable_phrase, 2);
  NewPagePhrase(input_htin_phrase, 2);
  struct Phrase input_htin;
  struct Phrase gross_input = input_phrase;
  struct Phrase final_perspective;
  uint16_t perspective;
  repeat_with_except(
      input_long, iteration, done == truth_WORD, phrase_zero(variable_phrase);
      phrase_zero(input_htin_phrase);
      input_htin = forward_htin_found(gross_input);
      input_htin_phrase = phrase_addenda(input_htin, input_htin_phrase);
      gross_input = drop_htin(gross_input, 1);
      //text_phrase_print(input_htin_phrase);
      variable_phrase = neo_independentClause_interpret(
          knowledge_phrase, &done, input_htin_phrase, variable_phrase);
      //phrase_print(variable_phrase);
      //Page_print(variable_phrase.page);

      // if variable sentence is from realis_mood genotype
      // then update knowledge phrase and produce phrase with variable phrase
      //hyitpwih_prifhtinka_jyipcu_prifhtinmwah_hkuttyafwa_psashtinka_hroktu(
      //  variable_phrase, knowledge_phrase, produce_phrase);

      final_perspective = neo_final_perspective_found(variable_phrase);
      perspective = read_thePhraseZWord_atThePlace(final_perspective, 0);
      if (perspective == realis_mood_GRAMMAR ||
          perspective == declarative_mood_GRAMMAR) {
        //phrase_print(knowledge_phrase);
        //text_phrase_print(knowledge_phrase);
        //text_phrase_print(variable_phrase);
        knowledge_phrase = reform_theParagraph_byTheVariable(knowledge_phrase,
                                                             variable_phrase);
        //text_phrase_print(produce_phrase);
        produce_phrase =
            reform_theParagraph_byTheVariable(produce_phrase, variable_phrase);
        //phrase_print(produce_phrase);
      } else { 
       // if it is a go or repeat instruction, then interpret here.
       // easiest if go instruction, as can simply set where it went
       // if reversible go then would leave some kind of tracer or something.
       // are there reversible repeats? are they easier to reverse? nah..
       // repeats are harder to implement, especially the nested ones.
       // how about BobISA how is that?
       // maybe gotta think how people talk. how is that?
       // go there, do that, come back. 
       // issue i thought of is possibility of name collisions, 
       // I guess if we had a namespace it could help, for called functions to
       // use a namespace anyways... but yeah that's more overhead.. though
       // easier to debug..
       // another option would be to have like some kind of seperate knowledge
       // base for calls, and only the returned value would be added to the
       // regular knowledge..
       // though that wouldn't work very well with the pen and paper model we're
       // working with, the namespace one would work better. 
      //DEBUGPRINT(("go instruction assumed\n"));
      //text_phrase_print(input_phrase);
      //phrase_print(variable_phrase); 
      //text_phrase_print(knowledge_phrase);
      produce_phrase = hkuttyafmwah_psashtinyi_hkinhtinka_tyiftu(
          knowledge_phrase, produce_phrase, input_phrase);
      }
  );
  produce_phrase.length = phrase_long_diagnose(produce_phrase);
  return produce_phrase;
}

#ifndef OPENCL
uint16_t pyash_to_pyash_interpret(struct Paragraph knowledge,
                                  const struct Text input, const uint16_t sort,
                                  const uint16_t produce_text_long,
                                  char produce_text[]) {
  guarantee(produce_text != NULL);
  guarantee(produce_text[0] == (char)(0)); // presume string is zeroed
  guarantee(produce_text_long >= WORD_LONG);
  guarantee(input.length >= 0);
  guarantee(input.letters != NULL);
  conditional(input.length < 2,
              return (uint16_t)error_text_copy(
                  "zihtet_%_htetzipwah ksantlatlyanpwih htetlyanka cyonmu",
                  input.letters, produce_text_long, produce_text));

  // DEBUGPRINT(("0x%X text_long, '%s' text\n", text_long, text));
  NewTextPhrase(tablet_phrase, HTIN_LONG + 4, input.letters);

  NewPagePhrase(produce_phrase, HTIN_LONG + 4);
  text_phrase_print(knowledge);
  text_phrase_print(tablet_phrase);
  Page_print(tablet_phrase.page);
  text_phrase_print(produce_phrase);
  produce_phrase =
      knowledge_interpret(knowledge, tablet_phrase, produce_phrase);

  text_page_print(produce_phrase.page);
  text_phrase_print(produce_phrase);
  // tablet_print((uint8_t)produce_long, produce);
  // knowledge_attribute[Sequence_finally] = tablet_addenda(1, &produce,
  // knowledge_attribute, knowledge);
  // DEBUGPRINT(("0x%X produce_long\n", produce_long));
  // DEBUGPRINT(("produce"));
  // tablet_print(produce_long, produce);
  uint16_t text_long = tablet_translate(
      (uint8_t)((produce_phrase.length | LINE_LONG_MASK) + 1) / LINE_LONG,
      produce_phrase.page.lines, sort, produce_text_long, produce_text);

  DEBUGPRINT(("%X text_long\n", text_long));
  return text_long;
}

struct Text /* produce_text */
neo_pyash_to_pyash_interpret(struct Phrase program, const struct Text input,
                             const uint16_t sort, struct Text produce) {
  uint16_t produce_length = pyash_to_pyash_interpret(
      program, input, sort, produce.max_length, produce.letters);
  DEBUGPRINT(("%X produce_length\n", produce_length));
  text_print(produce);
  produce.length = produce_length;
  return produce;
}

#endif

word_t /* gvakhbistlat */ hbistlatyi_gvakka_ryantu(word_t gvak) {
  switch (gvak) {
    example(accusative_case_GRAMMAR, return accusative_case_WORD);
    example(nominative_case_GRAMMAR, return nominative_case_WORD);
    example(instrumental_case_GRAMMAR, return instrumental_case_WORD);
    example(dative_case_GRAMMAR, return dative_case_WORD);
    example(ablative_case_GRAMMAR, return ablative_case_WORD);
  }
  return 0;
}

struct Phrase /* ksim phrase */
kwonyi_rwekgvakfyakti_nwonhtinka_plus(const struct Phrase ksim,
                                      const struct Phrase input,
                                      struct Phrase dictionary) {
  // get previous gvak from ksim
  DEBUGPRINT(("%x ksim.length, %x, ksim.begin\n", ksim.length, ksim.begin));
  text_phrase_print(ksim);
  struct Phrase ksim_gvak = retrospective_gvak_found(ksim);
  DEBUGPRINT(("%x ksim_gvak.length, %x, ksim_gvak.begin\n", ksim_gvak.length,
              ksim_gvak.begin));
  phrase_print(ksim_gvak);
  text_phrase_print(ksim_gvak);
  conditional(ksim_gvak.length == 0, return ksim_gvak);
  // get same gvak from input
  word_t example_gvak = phrase_word_read(ksim_gvak, 0);
  struct Phrase input_gvakfyak =
      retrospective_example_gvakfyak_found(input, example_gvak);
  phrase_print(input_gvakfyak);
  // struct Phrase ksim_gvakfyak =
  //    retrospective_example_gvakfyak_found(ksim, example_gvak);
  // phrase_print(ksim_gvakfyak);
  // make nominal htin and add it to dictionary
  word_t gvak_baseWord =
      hbistlatyi_gvakka_ryantu(phrase_word_read(ksim_gvak, 0));
  guarantee(gvak_baseWord != 0);
  // setup new dictionary line
  const txik_t dictionary_final_line_txik =
      ((dictionary.begin + dictionary.length) & LINE_LONG_MASK);
  if (dictionary_final_line_txik > 1) {
    const txik_t dictionary_line_difference =
        LINE_LONG - dictionary_final_line_txik;
    dictionary.length += dictionary_line_difference;
    guarantee(bwitnweh_fyakka_ri(dictionary) == truth_WORD);
  }
  guarantee((dictionary.begin & LINE_LONG_MASK) <= 1);
  dictionary = addenda_theLiteralWord_toThePhrase(gvak_baseWord, dictionary);
  dictionary = addenda_theLiteralWord_toThePhrase(it_GRAMMAR, dictionary);
  dictionary =
      addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, dictionary);
  input_gvakfyak.length -= 1;
  if (gvak_baseWord == dative_case_WORD) {
    dictionary = phrase_addenda(input_gvakfyak, dictionary);
  } else {
    dictionary =
        translate_theUpcomingVariablePhrase_ToWorthPhrase_ByKnowledgePhrase(
            input_gvakfyak, dictionary, ksim);
    dictionary.length--;
  }
  dictionary =
      addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, dictionary);
  Page_print(dictionary.page);
  text_phrase_print(dictionary);
  dictionary =
      addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, dictionary);
  phrase_print(dictionary);

  // kwonyi_nwonhtinka_plus(ksim, input, name_dictionary);
  return dictionary;
}

struct Phrase /* produce */
kwonyi_wigvakfyakti_nwonhtinka_plus(const struct Phrase hkut,
                                    const struct Phrase input,
                                    struct Phrase dictionary) {
  guarantee(hkut.page.plength > 0);
  uint16_t max_txik = input.length + input.begin;
  uint16_t produce_boolean = lie_WORD;
  uint8_t nwonhtin_count = 0;
  struct Phrase ksimti_gvak;
  struct Phrase input_gvak;
  down_repeat(max_txik, input_gvak = retrospective_gvak_found(input);
              // I'm thinking instead of a dictionary can simply add
              // everything to knowledge. unless we create a harvard
              // architecture, though that would be premature optimization,
              // can always split it up later. dictionary =
              // hkutyi_fyakpwih_rwekgvakfyakti_nwonhtinka_plus(
              //                       hkut, input, dictionary);
              max_txik = ksimti_gvak.begin;
              ksimti_gvak.length = (uint8_t)max_txik;
              nwonhtin_count += produce_boolean == truth_WORD ? 1 : 0;
              conditional(produce_boolean == lie_WORD, break;););
  conditional(nwonhtin_count > 0, return dictionary);
  return dictionary;
}

struct Phrase /* command_list*/ recipe_load(const struct Phrase knowledge,
                                            const struct Phrase input,
                                            struct Phrase command_list) {
  // retrospective search for declarative_mood htin in knowledge
  // copy verb and declarative sentence
  struct Paragraph recipe = retrospective_recipe_found(knowledge, input);
  text_phrase_print(recipe);
  // if not found then return lie_WORD
  if (recipe.length == 0) {
    return command_list;
  }
  //  [ ] recipe load
  //  [ ] make translation statements from ksim variables to input variables
  command_list =
      nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu(command_list, input);

  //  [ ] load function to worker scratch space of paragraph size
  text_phrase_print(command_list);
  recipe = find_theParagraphZContents_toTheParagraph(recipe);
  command_list = addenda_thePhrase_toThePhrase(recipe, command_list);

  //  [ ] name update the recipe sentences,
  //  [ ] lookup to see if name translation exists then replace
  //  [ ] if name does not exist, replace with universal hash and add to
  //       translation list.
  //
  // if found then load transformed contents to command_list
  // translate the input variables to the slogan ksim variables
  // put them into the places where they are called in the function
  // function defined in SSA form.

  // putting in the variables from the input as appropriate
  return command_list;
}
struct Phrase /* cousin */
neo_retrospective_htin_cousin_found(const struct Phrase example,
                                    const struct Phrase knowledge) {
  guarantee(example.page.lines != NULL);
  guarantee(knowledge.page.lines != NULL);
  struct Page cousin;
  uint16_t cousin_begin = 0;
  uint16_t knowledge_length = knowledge.length / LINE_LONG + 1;
  // DEBUGPRINT(("%X knowledge_length, %X knowledge.page.plength\n",
  //            knowledge_length, knowledge.page.plength));
  // knowledge_length = knowledge.page.plength;
  uint16_t cousin_long = retrospective_htin_cousin_found(
      example.page.plength, example.page.lines, knowledge_length,
      knowledge.page.lines, &cousin_begin);
  if (cousin_long == 0) {
    NewHollowPhrase(empty);
    empty.page = knowledge.page;
    return empty;
  }
  // Page_print(knowledge.page);
  // Page_print(example.page);
  // DEBUGPRINT(("%X cousin_long, %X cousin_begin\n", cousin_long,
  // cousin_begin));
  cousin.lines = knowledge.page.lines + cousin_begin;
  cousin.plength = (uint8_t)cousin_long;
  // Page_print(cousin);
  PagePhrase(phrase, cousin);
  conditional(phrase.length == 0, phrase.begin = 0);
  return phrase;
}

struct Phrase /* produce phrase */
neo_upcoming_variable_to_worth_translate(const struct Phrase knowledge,
                                         const struct Phrase input,
                                         struct Phrase produce) {
  // translates input phrase to its value else return 0 length
  //text_phrase_print(input);
  if (phrase_word_read(input, 0) == paragraph_GRAMMAR) {
    produce.length = 0;
    return produce;
  }
  txik_t input_indexFinger = input.begin;
  txik_t produce_addenda_length = upcoming_variable_to_worth_translate(
      knowledge.page.plength, knowledge.page.lines, input.page.plength,
      input.page.lines, &input_indexFinger, produce.length + produce.begin,
      produce.page.plength, produce.page.lines);
  struct Phrase input_phrase = input;
  input_phrase.begin = input_indexFinger;
  input_phrase.length =
      (uint8_t)(input.length - (input_indexFinger - input.begin));
  struct Phrase produce_phrase = produce;
  conditional(produce_phrase.begin == 0, produce_phrase.begin++);
  // DEBUGPRINT(("0x%X produce_addenda_length\n", produce_addenda_length));
  produce_phrase.length += produce_addenda_length;
  //text_phrase_print(produce_phrase);
  return produce_phrase;
}

struct Phrase /* produce */
tlasci_fyakyi_tsinci_fyakyu_hvatprifci_fyakka_ryantu(struct Phrase htikya,
                                                     struct Phrase hyakya,
                                                     struct Phrase hkakya) {
  txik_t txik = hkakya.begin;
  // uint16_t amount_added_to_produce_indexFinger =
  htikya.length += upcoming_variable_to_worth_translate(
      hyakya.page.plength, hyakya.page.lines, hkakya.page.plength,
      hkakya.page.lines, &txik, htikya.length + htikya.begin,
      htikya.page.plength, htikya.page.lines);
  return htikya;
}

// 
// nominal independent clause paucal number to
// final independent clause 's
// grammatical phrase paucal number,
// translate
//
// what does it do exactly?
// for each case in the given sentence
// it makes a list of sentences of the form:
// case_grammar+it+na case_grammar_contents+ka li
struct Phrase /*produce */
nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu(struct Phrase htikya,
                                            const struct Phrase hkakya) {
  uint16_t max_txik = hkakya.length + hkakya.begin;
  uint8_t nwonhtin_count = 0;
  struct Phrase hkakya_gvakfyak;
  struct Phrase search = hkakya;
  text_phrase_print(htikya);
  text_phrase_print(hkakya);
  down_repeat(
      max_txik, 
      DEBUGPRINT(("%X search.length, %X search.begin\n", search.length, search.begin));
      hkakya_gvakfyak = rwekci_gvakfyakka_hfantu(search);
      text_phrase_print(hkakya_gvakfyak); 
      DEBUGPRINT(("%X htikya.begin, %X htikya.length\n", htikya.begin, htikya.length));
      htikya = nwonhtinyi_gvakfyakka_ryantu(htikya, hkakya_gvakfyak);
      page_text_print(htikya.page); 
      text_phrase_print(htikya);
      text_phrase_print(search); 
      DEBUGPRINT( ("%X search.length, %X search.begin\n", search.length, search.begin));
      search.length = (uint8_t)(hkakya_gvakfyak.begin - search.begin);
      if (search.length > max_txik || search.length == 0) { break; });
  conditional(nwonhtin_count > 0, return htikya);
  return htikya;
}

struct Phrase /*htikya */
nwonhtinyi_gvakfyakka_ryantu(struct Phrase htikya, const struct Phrase hkakya) {
  struct Phrase input_gvakfyak = hkakya;
  // phrase_print(input_gvakfyak);
  // text_phrase_print(hkakya);
  // struct Phrase hkakyafyak =
  //    retrospective_example_gvakfyak_found(ksim, example_gvak);
  // phrase_print(hkakyafyak);
  // make nominal htin and add it to htikya
  word_t gvak_baseWord =
      hbistlatyi_gvakka_ryantu(phrase_word_read(hkakya, hkakya.length - 1));
  guarantee(gvak_baseWord != 0);
  // setup new htikya line
  htikya = addenda_aHollowIndependentClause_toThePhrase(htikya);
  //    Page_print(htikya.page);

  // setup new sentence
  htikya = addenda_theLiteralWord_toThePhrase(gvak_baseWord, htikya);
  htikya = addenda_theLiteralWord_toThePhrase(it_GRAMMAR, htikya);
  htikya = addenda_theGrammarWord_toThePhrase(nominative_case_GRAMMAR, htikya);
  input_gvakfyak.length -= 1;
  // presume it's already been variable translated
  // if (gvak_baseWord == dative_case_WORD) {
  // text_phrase_print(htikya);
  htikya = phrase_addenda(input_gvakfyak, htikya);
  // text_phrase_print(input_gvakfyak);
  // text_phrase_print(htikya);
  // Page_print(htikya.page);
  htikya = addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, htikya);
  // Page_print(htikya.page);
  // text_phrase_print(htikya);
  htikya = addenda_theGrammarWord_toThePhrase(realis_mood_GRAMMAR, htikya);
  // phrase_print(htikya);

  // kwonyi_nwonhtinka_plus(ksim, input, name_htikya);
  return htikya;
}
