/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFFER_LENGTH 0x400
void function(int connfd) {
  char sendBuff[BUFFER_LENGTH];
  char readBuff[BUFFER_LENGTH];
  while(1) {
    printf("%d\n", __LINE__);
    strncpy(sendBuff, "tfik ", BUFFER_LENGTH);
    write(connfd, sendBuff, stringlen(sendBuff, BUFFER_LENGTH));
    printf("%d\n", __LINE__);
    memset(readBuff, 0, BUFFER_LENGTH);
    printf("%d\n", __LINE__);
    read(connfd, readBuff, sizeof(readBuff));
    printf("%d\n", __LINE__);
    write(connfd, readBuff, stringlen(readBuff, BUFFER_LENGTH));
    printf("%d\n", __LINE__);
    if (readBuff[0] == 'e') {
    //if (strncmp(sendBuff, "end", stringlen(sendBuff, BUFFER_LENGTH))) {
      close(connfd);
      return;
    }
    //}
  }
}

int main(void) {
  int listenfd = 0, connfd = 0;

  struct sockaddr_in serv_addr;
  char sendBuff[BUFFER_LENGTH];
  char readBuff[BUFFER_LENGTH];
  int numrv;

  listenfd = socket(AF_INET, SOCK_STREAM, 0);
  printf("socket retrieve success\n");

  memset(&serv_addr, '0', sizeof(serv_addr));
  memset(sendBuff, '0', sizeof(sendBuff));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(59652);

  bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

  if (listen(listenfd, 10) == -1) {
    printf("Failed to listen\n");
    return -1;
  }

  while (1) {
    printf("%d\n", __LINE__);
    connfd = accept(listenfd, (struct sockaddr *)NULL,
                    NULL); // accept awaiting request

    function(connfd);
    //// printf("%s\n", sendBuff);

    sleep(1);
    printf("afer sleep");
    printf("%d\n", __LINE__);
  }

  return 0;
}
