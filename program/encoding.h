/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef ENCODING_H
#define ENCODING_H
#include "compile.h"
#include "parser.h"
#include "seed.h"
#include "sort.h"
void word_code_encode(const uint8_t word_size, const char *word,
                      uint16_t *number);
word_t neo_word_code_encode(struct Text word);
void tablet_encoding(const uint8_t code_text_size, const uint16_t *code_text,
                     uint8_t *tablet_size, uint16_t *tablet,
                     uint8_t *remainder);

uint8_t /*tablet_long*/ independentClause_encoding(const uint16_t text_size,
                                                   const char *text,
                                                   const uint8_t tablet_size,
                                                   line_t *tablet,
                                                   uint16_t *remainder);
void code_ACC_word_PL(const uint8_t ACC_GEN_size,
                      const char *ACC_independentClause, uint8_t *DAT_GEN_size,
                      uint16_t *DAT_code_independentClause,
                      uint8_t *DAT_GEN_remainder);

void code_ACC_consonant_one(const uint8_t type, const uint8_t consonant_one,
                            uint16_t *number);
void code_ACC_vowel(const uint8_t type, const uint8_t vowel, uint16_t *number);
void code_ACC_tone(const uint8_t type, const uint8_t tone, uint16_t *number);
void code_ACC_consonant_three(const uint8_t type, const uint8_t consonant_three,
                              const uint8_t tone, uint16_t *number);
uint16_t /*tablet_long*/ old_text_encoding(const uint16_t max_text_magnitude,
                                           const char *text,
                                           uint16_t tablet_magnitude,
                                           line_t *tablet,
                                           uint16_t *text_remainder);
struct Paragraph /* produce*/ text_encoding(const struct Text text,
                                            struct Paragraph tablet,
                                            uint16_t *text_remainder);
uint16_t /*text_remainder*/ neo_text_encoding(const struct Text text,
                                              struct Page tablet);
uint16_t text_encoding_quiz(const uint16_t max_text_magnitude,
                            const char *text);

struct Phrase /* htikya */
fyakyi_hwusti_hpamhtinka_kfontu(struct Phrase htikya, struct Text hkakhnikya);

#define encode_theTextZFirstIndependentClause_toThePhrase(theText,             \
                                                          toThePhrase)         \
  fyakyi_hwusti_hpamhtinka_kfontu(toThePhrase, theText)

struct Phrase /* produce */ number_grammar_encode(struct Phrase input);
struct Paragraph /* produce */
neo_independentClause_encoding(const struct Text text, struct Paragraph tablet,
                               uint16_t *text_remainder);

struct Phrase process_quote(struct Phrase tablet, struct Text text,
                            uint8_t *text_indexFinger);
struct Phrase encode_paragraph_quote(struct Phrase tablet, struct Text text,
                                     uint8_t *text_indexFinger);

/**
 * how much line vacancy remains at the phrase termination
 * fyakhzotlwoh  kehlaskfiskfamka twicri
 */
uint8_t fyakhzotlwoh_kehlaskfiskfamka_twicri(struct Phrase phrase);
#define howMuch_lineVacancy_remains_atThePhraseTermination                     \
  fyakhzotlwoh_kehlaskfiskfamka_twicri

struct Phrase process_letter_quote(struct Phrase tablet, const struct Text text,
                                   uint8_t *text_indexFinger);

struct Text find_letter_quote_contents(const struct Text text);
struct Text find_letter_quote(const struct Text text);
// practlattyeh tlattyeh kyithwustyeh hnucdo hwustyeh hwustxikpo blettyaftri tlatkfontu
struct Text word_encoding(struct Text derived_word, 
    struct Text quote_text, uint16_t *number, struct Text text, 
    uint8_t *text_indexFinger, struct Paragraph tablet, uint8_t *repeat_stop_eh);
#endif
