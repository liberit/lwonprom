/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef SORT_H
#define SORT_H


#define DEBUGING
#define MAX_STRING_LENGTH 0x100
#define MAX_LONG_STRING_LENGTH 0x1000
#define MAX_LINE_TEXT_LENGTH 0x50
#define LINE_LONG 0x10
#define LINE_LONG_MASK 0xF
#define LINE_BYTE_LONG 0x20
#define MAX_PARAGRAPH_LONG 0x10
#define HTIN_LONG 0x8
#define HTIN_LONG_MASK 0x3
#define PAGE_LONG 0x80
#define HTIN_FINALLY_INDICATOR 1
#ifndef NULL
#define NULL 0
#endif
#define V16_LONG 0x10
#define V16_INIT 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
#define TRUE 1
#define FALSE 0
//#define EMSCRIPTEN

//#ifdef __APPLE__
//  #include <OpenCL/opencl.h>
//#else
//  #include <CL/cl.h>
//#endif
#ifndef OPENCL
typedef unsigned int uint;
typedef unsigned long ulong;
#include <assert.h>
#include <stdio.h>
#define BREAK()                                                                \
  print_backtrace;                                                             \
  assert(1 == 0);
#define guarantee(_condition)                                                  \
  if (!(_condition)) {                                                         \
    print_backtrace();                                                         \
    assert(_condition);                                                        \
  }
#ifdef DEBUGING
#define DEBUGPRINT(x)                                                          \
  fprintf(stdout, "[%s:%d:%s]: \t", __FILE__, __LINE__, __FUNCTION__);         \
  printf x
#else
#define DEBUGPRINT(x)
#endif

#define constant const
#define private
#define global
#define local
#define generic

#else
#define uint unsigned int
#define ulong unsigned long
#ifndef LLVM
#define guarantee(_condition)                                                  \
  if (!(_condition)) {                                                         \
    print_backtrace();                                                         \
    printf("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);                    \
  }
#define DEBUGPRINT(x) printf x
/*
// printf("[%s:%d:%s]: \t", __FILE__, __LINE__, __FUNCTION__);     \
// printf(x);
*/
#else
//#define DEBUGPRINT(x) printf(x)
#define guarantee(_condition)                                                  \
  if (!(_condition)) {                                                         \
  }
#endif

#endif
#ifndef OPENCL
#ifndef __has_extension
#define __has_extension __has_feature // Compatibility with pre-3.0 compilers.
#endif
#ifndef __has_feature      // Optional of course.
#define __has_feature(x) 0 // Compatibility with non-clang compilers.
#endif
#ifndef __has_builtin      // Optional of course.
#define __has_builtin(x) 0 // Compatibility with non-clang compilers.
#endif
#if __has_builtin(__builtin_shufflevector)
#define shuffle __builtin_shufflevector
#endif
#if defined(_MSC_VER)
/* Microsoft C/C++-compatible compiler */
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
/* GCC-compatible compiler, targeting x86/x86-64 */
//#include <x86intrin.h>
#elif defined(__GNUC__) && defined(__ARM_NEON__)
/* GCC-compatible compiler, targeting ARM with NEON */
#include <arm_neon.h>
#elif defined(__GNUC__) && defined(__IWMMXT__)
/* GCC-compatible compiler, targeting ARM with WMMX */
#include <mmintrin.h>
#elif (defined(__GNUC__) || defined(__xlC__)) &&                               \
    (defined(__VEC__) || defined(__ALTIVEC__))
/* XLC or GCC-compatible compiler, targeting PowerPC with VMX/VSX */
#include <altivec.h>
#elif defined(__GNUC__) && defined(__SPE__)
/* GCC-compatible compiler, targeting PowerPC with SPE */
#include <spe.h>
#endif

#ifdef EMSCRIPTEN
#include <stdint.h>
// typedef int v4si __attribute__((ext_vector_type(4)));
// typedef unsigned char v16uc __attribute__((ext_vector_type(16)));
// typedef unsigned int v4us __attribute__((vector_size(8)));
// typedef unsigned short v8us //__attribute__((vector_size(16)));
typedef union {
  uint16_t s0;
  uint16_t s1;
  uint16_t s2;
  uint16_t s3;
} v4us;
typedef union {
  uint16_t s0;
  uint16_t s1;
  uint16_t s2;
  uint16_t s3;
  uint16_t s4;
  uint16_t s5;
  uint16_t s6;
  uint16_t s7;
  uint16_t s8;
} v8us;
typedef union {
  uint16_t s0;
  uint16_t s1;
  uint16_t s2;
  uint16_t s3;
  uint16_t s4;
  uint16_t s5;
  uint16_t s6;
  uint16_t s7;
  uint16_t s8;
  uint16_t s9;
  uint16_t sA;
  uint16_t sB;
  uint16_t sC;
  uint16_t sD;
  uint16_t sE;
  uint16_t sF;
} v16us;
#ifndef OPENCL
typedef unsigned long ulong;
#else
#define ulong cl_ulong;
#define ulong cl_ulong;
#endif
#elif __has_feature(attribute_ext_vector_type)
// This code will only be compiled with the -std=c++11 and -std=gnu++11
// options, because rvalue references are only standardized in C++11.
#define CLANGCL
#include <stdint.h>
typedef __uint16_t uint16_t;
typedef int v4si __attribute__((ext_vector_type(4)));
typedef unsigned char v16uc __attribute__((ext_vector_type(16)));
typedef unsigned short v16us __attribute__((ext_vector_type(16)));
typedef unsigned short v8us __attribute__((ext_vector_type(8)));
typedef unsigned int v4us __attribute__((ext_vector_type(4)));
#elif __OPENCL_CL_H
typedef cl_uchar uint8_t;
typedef cl_ushort uint16_t;
typedef cl_uint uint32_t;
typedef cl_ulong uint64_t;
typedef cl_long int64_t;
typedef cl_ulong ulong;
typedef cl_ulong size_t;
typedef cl_short4 v4si;
typedef cl_uchar16 v16uc;
typedef cl_ushort16 v16us;
typedef cl_ushort8 v8us;
typedef cl_ushort4 v4us;
typedef unsigned long ulong;
#endif

// typedef unsigned long ulong;
//#define ulong ulong;
#define V8US_LONG 16
/*#define NULL 0*/

#endif
#ifdef OPENCL
typedef uchar uint8_t;
typedef ushort uint16_t;
typedef uint uint32_t;
typedef ulong uint64_t;
typedef long int64_t;
// typedef ulong size_t;
typedef short4 v4si;
typedef uchar16 v16uc;
typedef ushort16 v16us;
typedef ushort8 v8us;
typedef ushort4 v4us;
#endif
#ifdef OPENCL_1_2_COMPAT
#define generic
#endif

#define ARRAYLINE

typedef uint16_t txik_t;
#ifdef ARRAYLINE
typedef unsigned short line_t[LINE_LONG];
#else
#define line_t v16us
#endif

struct Page {
  line_t *lines;
  uint8_t plength;
};

struct cPage {
  const line_t *lines;
  uint8_t length;
};

struct Phrase {
  struct Page page;
  txik_t begin;
  uint8_t length;
};

// struct Sentence {
// struct Page page;
// txik_t  begin;
// uint8_t length;
//};

#define Sentence Phrase
#define Paragraph Phrase
#define Paragraphs Phrase

// struct Paragraph {
// struct Page page;
// txik_t  begin;
// uint8_t length;
//};

// struct Paragraphs {
// struct Page page;
// txik_t  begin;
// uint8_t length;
//};

struct Phrase_2_tuple {
  struct Phrase p0;
  struct Phrase p1;
};

#define NewPhrase(name, _begin, _length)                                       \
  struct Phrase name;                                                          \
  name.begin = _begin;                                                         \
  name.length = _length;

#define PagePhrase(name, _page)                                                \
  struct Phrase name;                                                          \
  name.page = _page;                                                           \
  name.begin = 1;                                                              \
  name.length = (uint8_t)htinti_final_word_found(_page.plength, _page.lines);

#define NewPagePhrase(name, _length)                                           \
  NewPage(name##_page, _length);                                               \
  struct Phrase name;                                                          \
  name.page = name##_page;                                                     \
  name.begin = 0;                                                              \
  name.length = 0;

struct Text {
  char *letters;
  uint16_t length;
  uint16_t max_length;
};

#define NewText(name, string)                                                  \
  char name##_letters[MAX_STRING_LENGTH];                                      \
  constant_text_copy(string, MAX_STRING_LENGTH, name##_letters);               \
  struct Text name;                                                            \
  name.letters = name##_letters;                                               \
  name.length = (uint16_t)stringlen(name##_letters,                            \
                                    MAX_STRING_LENGTH); /*flawfinder:ignore*/  \
  name.max_length = MAX_STRING_LENGTH;

#define NewLongText(name, string)                                                  \
  char name##_letters[MAX_LONG_STRING_LENGTH];                                      \
  constant_text_copy(string, MAX_LONG_STRING_LENGTH, name##_letters);               \
  struct Text name;                                                            \
  name.letters = name##_letters;                                               \
  name.length = (uint16_t)stringlen(name##_letters,                            \
                                    MAX_LONG_STRING_LENGTH); /*flawfinder:ignore*/  \
  name.max_length = MAX_LONG_STRING_LENGTH;

#define NewTextPad(_name, _pad_length)                                         \
  char _name##_letters[_pad_length] = {0};                                     \
  struct Text _name;                                                           \
  _name.letters = _name##_letters;                                             \
  _name.length = 0; \
  _name.max_length = _pad_length;

#define NewPage(name, tablet_length)                                           \
  line_t name##_lines[tablet_length] = {{0}};                                  \
  struct Page name;                                                            \
  name.lines = name##_lines;                                                   \
  name.plength = (uint8_t)tablet_length;

#define NewTextPage(name, tablet_length, string)                               \
  NewPage(name, tablet_length);                                                \
  NewText(name##_text, string);                                                \
  neo_text_encoding(name##_text, name);

#define NewLongTextPage(name, tablet_length, string)                               \
  NewPage(name, tablet_length);                                                \
  NewLongText(name##_text, string);                                                \
  neo_text_encoding(name##_text, name);

#define NewTextParagraph NewTextPhrase

#define NewTextPhrase(name, tablet_length, string)                             \
  NewTextPage(name##_page, tablet_length, string);                             \
  PagePhrase(name, name##_page);

#define NewLongTextPhrase(name, tablet_length, string)                             \
  NewLongTextPage(name##_page, tablet_length, string);                             \
  PagePhrase(name, name##_page);

#define NewHollowPage(name)                                                    \
  struct Page name;                                                            \
  name.lines = 0;                                                              \
  name.plength = 0;

#define NewHollowPhrase(name)                                                  \
  NewHollowPage(name##_page);                                                  \
  struct Phrase name;                                                          \
  name.page = name##_page;                                                     \
  name.length = 0;                                                             \
  name.begin = 0;

#define NewHollowParagraph(name)                                               \
  NewHollowPage(name##_page);                                                  \
  struct Paragraph name;                                                       \
  name.page = name##_page;                                                     \
  name.length = 0;                                                             \
  name.begin = 0;
#ifndef uint8_t
#ifndef EMSCRIPTEN
#define uint8_t __uint8_t
#endif 
#endif 

#ifndef uint16_t
#ifndef EMSCRIPTEN
#define uint16_t __uint16_t
#endif
#endif 

void v4us_write(uint8_t code_indexFinger, uint8_t maximum_code_indexFinger,
                uint16_t code_number, v4us *code_name);

uint64_t v4us_uint64_translation(const v4us vector);
uint16_t line_t_read(const uint8_t indexFinger, const line_t vector);
void line_t_write(const uint8_t indexFinger, const uint16_t number,
                  line_t *vector);


#define word_t uint16_t
word_t tablet_read(const uint16_t indexFinger, const uint8_t series_long,
                   const line_t *tablet);
#define tlatyi_txikyu_hpifka_tyactu(hyakya, hkakya)                            \
  tablet_read(hyakya, hkakya.plength, hkakya.lines)

#define read_thePage_byTheIndexFinger_toTheWord(thePage, theIndexFinger)       \
  tlatyi_txikyu_hpifka_tyactu(theIndexFinger, thePage)

#define neo_tablet_read(txik, tablet)                                          \
  tablet_read(txik, tablet.plength, tablet.lines)
void tablet_write(const uint16_t indexFinger, const uint16_t number,
                  const uint8_t series_long, line_t *tablet);

void tablet_word_write(const uint16_t indexFinger, const uint16_t number,
                       const uint8_t series_long, line_t *tablet,
                       uint16_t *neo_indexFinger);
uint16_t tablet_upcoming_word_read(const uint16_t indexFinger,
                                   const uint8_t series_long,
                                   const line_t *tablet,
                                   uint16_t *neo_indexFinger);
uint16_t upcoming_word_read(const uint16_t indexFinger,
                                   const uint8_t series_long,
                                   const line_t *tablet,
                                   uint16_t *neo_indexFinger);
#define neo_tablet_upcoming_word_read(txik, tablet, neo_txik)                  \
  tablet_upcoming_word_read(txik, tablet.plength, tablet.lines, neo_txik)
uint16_t tablet_retrospective_word_read(const uint16_t indexFinger,
                                        const uint8_t series_long,
                                        const line_t *tablet,
                                        uint16_t *neo_indexFinger);

void tablet_grammar_write(const uint16_t indexFinger, const uint16_t number,
                          const uint8_t series_long, line_t *tablet,
                          uint16_t *neo_indexFinger);

uint16_t tablet_upcoming_grammar_read(const uint16_t indexFinger,
                                      const uint8_t series_long,
                                      const line_t *tablet,
                                      uint16_t *neo_indexFinger);

struct Phrase /*grammar_word*/
neo_tablet_upcoming_grammar_read(const struct Phrase input);

struct Phrase /* grammar phrase */
upcoming_grammar_read(const struct Phrase input);
uint16_t tablet_upcoming_grammar_or_quote_read(const uint16_t indexFinger,
                                               const uint8_t series_long,
                                               const line_t *tablet,
                                               uint16_t *neo_indexFinger);
word_t /*grammar word or 0*/ tablet_retrospective_grammar_read(
    const uint16_t indexFinger, const uint8_t series_long, const line_t *tablet,
    uint16_t neo_indexFinger[]);

struct Phrase /*grammar_word*/
neo_tablet_retrospective_grammar_read(const struct Phrase input);

int line_t_print(const line_t tablet);
void line_t_forward_copy(const uint8_t abl, const uint8_t dat,
                         const uint8_t ins, line_t *tablet);

void tablet_upcoming_copy(const uint16_t abl, const uint16_t dat,
                          const uint16_t ins, const uint8_t series_long,
                          line_t *tablet);
#ifndef OPENCL
#ifdef DEBUGING
#define tablet_print(series_long, tablet)                                      \
  fprintf(stdout, "[%s:%d:%s]: \t", __FILE__, __LINE__, __FUNCTION__);         \
  _tablet_print((uint8_t)series_long, tablet);
#define Page_print(tablet)                                                     \
  fprintf(stdout, "[%s:%d:%s]: \n\t", __FILE__, __LINE__, __FUNCTION__);       \
  _tablet_print(tablet.plength, tablet.lines);
#else
#define tablet_print(series_long, tablet) _tablet_print(series_long, tablet);
#define Page_print(tablet) ;
#endif

#else
#define tablet_print(series_long, tablet) _tablet_print(series_long, tablet);
#define Page_print(tablet) ;
#endif

#ifndef OPENCL
#define text_page_print page_text_print
#else
#define text_page_print Page_print
#endif

int _tablet_print(const uint8_t series_long, const line_t *tablet);
ulong perfect_copy(const ulong text_long, const char *text,
                   const ulong produce_text_long, char *produce_text);
ulong text_copy(const ulong text_long, constant char *text,
                const ulong produce_text_long, char *produce_text);
ulong constant_text_copy(constant char *text, const ulong produce_text_long,
                         char *produce_text);
ulong error_text_copy(constant char *error_text, const char *topic_text,
                      const ulong produce_text_long, char *produce_text);

/** copies Text to produce
 * @param input -- text to be copied
 * @param produce -- text pad to put input text into
 * @return produce has length set to input
 */
struct Text /* produce */ neo_text_copy(struct Text input, struct Text produce);

ulong stringlen(const char *str, ulong max_len);
ulong cstringlen(constant char *str, ulong max_len);
#ifndef LLVM
#ifndef OPENCL
int form_print(const ulong pad_long, char *pad, constant char *form, ...);
#endif
#endif
ulong number_situate(const uint16_t number, const ulong number_series_long,
                     constant uint16_t *number_series);
#define series_print(_format, _long, _series)                                  \
  for (iteration = 0; iteration < _long; ++iteration) {                        \
    printf(_format, _series[iteration]);                                       \
  }                                                                            \
  printf("\n");

uint64_t modulo(const uint64_t value, const uint64_t modulo);
int64_t absolute(const int64_t a);
ulong text_long_derive(const char *str, ulong max_len);

//#define sequence_attribute uint16_t[2]
#define SEQUENCE_ATTRIBUTE_LONG 3
#define Sequence_max_long 0
#define Sequence_indexFinger 1
#define Sequence_finally 2

// indexOf returns sequence_long if not found
ulong gen_indexFinger(const uint16_t example, const ulong sequence_long,
                      constant uint16_t sequence[]);
uint16_t /* produce_long */ tablet_copy(const uint16_t tablet_long,
                                        const line_t tablet[],
                                        const uint16_t max_produce_long,
                                        line_t produce[]);
uint8_t code_tablet_long_found(const uint16_t recipe_long,
                               const line_t *recipe);
uint8_t knowledge_long_found(const uint16_t knowledge_long,
                             const line_t *knowledge);

#define maximum(example, example2, result)                                     \
  result = example > example2 ? example : example2;
#ifdef ARRAYLINE
void line_copy(const line_t input, line_t produce);
#else
#define line_copy(input, produce) produce = input
#endif

void global_line_copy(const global line_t input, global line_t produce);

void print_backtrace(void);

void _phrase_print(const struct Phrase phrase);

#define phrase_print(phrase)                                                   \
  DEBUGPRINT(("%s", ""));                                                      \
  _phrase_print(phrase);

// voidpage_print(const struct Page page);
#define page_text_print(page)                                                  \
  do {                                                                         \
    NewTextPad(_text, 0x100);                                                  \
    _text = page_translate(page, fluent_WORD, _text);                          \
    DEBUGPRINT(("'%s'\n", _text.letters));                                     \
  } while (0);

#define long_text_phrase_print(_phrase)                                             \
  do {                                                                         \
    NewTextPad(_text, MAX_LONG_STRING_LENGTH);                                                  \
    _text = neo_phrase_translate(_phrase, _text, fluent_WORD);                 \
    DEBUGPRINT(("'%s'\n", _text.letters));                                     \
  } while (0);


#define text_phrase_print(_phrase)                                             \
  do {                                                                         \
    NewTextPad(_text, MAX_STRING_LENGTH);                                                  \
    _text = neo_phrase_translate(_phrase, _text, fluent_WORD);                 \
    DEBUGPRINT(("'%s'\n", _text.letters));                                     \
  } while (0);

#define text_print(_text)                                                      \
  do {                                                                         \
  fprintf(stdout, "[%s:%d:%s]: \t", __FILE__, __LINE__, __FUNCTION__);         \
    repeat(_text.length, printf("%c", _text.letters[iterator]));               \
    printf("\n");                                                              \
  } while (0);

#define phrase_word_read read_thePhraseZWord_atThePlace
word_t /* word */ read_thePhraseZWord_atThePlace(struct Phrase phrase,
                                                 txik_t word_txik);

#define phrase_word_write write_theWord_toThePhrase_atThePlace
struct Phrase /* modified phrse */
write_theWord_toThePhrase_atThePlace(word_t word, struct Phrase phrase,
                                     txik_t place);

struct Phrase /* modified phrse */
write_theGrammarWord_toThePhrase_atThePlace(word_t word, struct Phrase phrase,
                                     txik_t place);

#ifndef OPENCL
#define max(a, b)                                                              \
  ({                                                                           \
    __typeof__(a) _a = (a);                                                    \
    __typeof__(b) _b = (b);                                                    \
    _a > _b ? _a : _b;                                                         \
  })
#define min(a, b)                                                              \
  ({                                                                           \
    __typeof__(a) _a = (a);                                                    \
    __typeof__(b) _b = (b);                                                    \
    _a < _b ? _a : _b;                                                         \
  })
#endif

word_t /* blu7n */ bwitnweh_fyakka_ri(struct Phrase fyak);
#define is_thePhrase_inBoundary(phrase) bwitnweh_fyakka_ri(phrase)
uint8_t program_long_diagnose(const uint8_t max_program_long,
                              const line_t *program);
#define page_long_diagnose(_page)                                              \
  program_long_diagnose(_page.plength, _page.lines);
uint16_t phrase_long_diagnose(struct Phrase phrase);

#define txik_t uint16_t
#define TRUTH (1==1)
#define LIE (1==0)

struct Text text_tail(struct Text text, uint16_t length);

struct Text text_zero(struct Text text);

struct Phrase hrathlasma_fyakka_hyuctu(struct Phrase);
#define round_thePhrase_toRoofLine hrathlasma_fyakka_hyuctu

txik_t hrathlasma_txikka_hyuctu(txik_t);
#define round_theIndexFinger_toRoofLine hrathlasma_txikka_hyuctu

struct Phrase fyaknweh_tcaslwoh_hgaftlatka_dlastu(struct Phrase phrase, 
    txik_t place);
#define delete_aGrammarWord_atThePlace_inThePhrase(_place, _phrase) \
 fyaknweh_tcaslwoh_hgaftlatka_dlastu(_phrase, _place)

#define NewWord(word) \
  NewTextPad(word, WORD_LONG);

#include "tlep.h"
#include "pyash.h"

#endif
