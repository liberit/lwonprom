/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "pyashWords.h"
#include "pyash.h"
#ifndef OPENCL
const uint16_t grammaticalCase_word_sequence[] = {
    nominative_case_GRAMMAR,
    accusative_case_GRAMMAR,
    topic_case_GRAMMAR,
    vocative_case_GRAMMAR,
    genitive_case_GRAMMAR,
    destination_case_GRAMMAR,
    dative_case_GRAMMAR,
    instrumental_case_GRAMMAR,
    comitative_case_GRAMMAR,
    benefactive_case_GRAMMAR,
    essive_case_GRAMMAR,
    adpositional_case_GRAMMAR,
    location_case_GRAMMAR,
    way_case_GRAMMAR,
    source_case_GRAMMAR,
    ablative_case_GRAMMAR,
    elative_case_GRAMMAR,
    illative_case_GRAMMAR,
    temporal_case_GRAMMAR,
    perlative_case_GRAMMAR,
    delative_case_GRAMMAR,
    causal_case_GRAMMAR,
    terminative_case_GRAMMAR,
    inessive_case_GRAMMAR,
    allative_case_GRAMMAR,
    vialis_case_GRAMMAR,
    quotative_case_GRAMMAR,
    distributive_case_GRAMMAR,
    evidential_case_GRAMMAR,
    multiplicative_case_GRAMMAR,
    initiative_case_GRAMMAR,
    sublative_case_GRAMMAR,
    exessive_case_GRAMMAR,
    pegative_case_GRAMMAR,
    prosecutive_case_GRAMMAR,
    superessive_case_GRAMMAR,
    partitive_case_GRAMMAR,
    prolative_case_GRAMMAR,
    modal_case_GRAMMAR,
    centric_case_GRAMMAR,
    oblique_case_GRAMMAR,
    patient_case_GRAMMAR,
    lative_case_GRAMMAR,
    locative_case_GRAMMAR,
    abessive_case_GRAMMAR,
    adessive_case_GRAMMAR,
    agentive_case_GRAMMAR,
    ergative_case_GRAMMAR,
    evitative_case_GRAMMAR,
    subessive_case_GRAMMAR,
    possessed_case_GRAMMAR,
    antessive_case_GRAMMAR,
    adverbial_case_GRAMMAR,
    exocentric_case_GRAMMAR,
    absolutive_case_GRAMMAR,
    associative_case_GRAMMAR,
    superlative_case_GRAMMAR,
    postpositional_case_GRAMMAR,
    locative_directional_case_GRAMMAR};

const uint16_t perspective_word_sequence[] = {realis_mood_GRAMMAR,
                                              deontic_mood_GRAMMAR,
                                              conditional_mood_GRAMMAR,
                                              epistemic_mood_GRAMMAR,
                                              interrogative_mood_GRAMMAR,
                                              directive_mood_GRAMMAR,
                                              hortative_mood_GRAMMAR,
                                              volitive_mood_GRAMMAR,
                                              deliberative_mood_GRAMMAR,
                                              desiderative_mood_GRAMMAR,
                                              imperative_mood_GRAMMAR,
                                              optative_mood_GRAMMAR,
                                              potential_mood_GRAMMAR,
                                              dubitative_mood_GRAMMAR,
                                              precative_mood_GRAMMAR,
                                              jussive_mood_GRAMMAR,
                                              permissive_mood_GRAMMAR,
                                              commissive_mood_GRAMMAR,
                                              eventive_mood_GRAMMAR,
                                              speculative_mood_GRAMMAR,
                                              benedictive_mood_GRAMMAR,
                                              inductive_mood_GRAMMAR,
                                              admonitive_mood_GRAMMAR,
                                              apprehensive_mood_GRAMMAR,
                                              imprecative_mood_GRAMMAR,
                                              assumptive_mood_GRAMMAR,
                                              prohibitive_mood_GRAMMAR,
                                              declarative_mood_GRAMMAR,
                                              affirmative_mood_GRAMMAR,
                                              irrealis_mood_GRAMMAR,
                                              sensory_evidential_mood_GRAMMAR,
                                              gnomic_mood_GRAMMAR,
                                              finally_GRAMMAR,
                                              propositive_mood_GRAMMAR,
                                              necessitative_mood_GRAMMAR,
                                              paragraph_GRAMMAR};

constant uint16_t number_word_sequence[NUMBER_WORD_SEQUENCE_LONG] = {
    zero_WORD,    one_WORD,     two_WORD,       three_WORD,    four_WORD,
    five_WORD,    six_WORD,     seven_WORD,     eight_WORD,    nine_WORD,
    ten_WORD,     eleven_WORD,  twelve_WORD,    thirteen_WORD, fourteen_WORD,
    fifteen_WORD, sixteen_WORD, seventeen_WORD, eighteen_WORD, nineteen_WORD,
    twenty_WORD,
};
constant uint16_t grammaticalCase_list[GRAMMATICALCASE_LIST_LONG] = {
    accusative_case_GRAMMAR, instrumental_case_GRAMMAR, dative_case_GRAMMAR,
    topic_case_GRAMMAR,      nominative_case_GRAMMAR,   ablative_case_GRAMMAR};

constant uint16_t perspective_list[PERSPECTIVE_LIST_LONG] = {
    realis_mood_GRAMMAR, conditional_mood_GRAMMAR, interrogative_mood_GRAMMAR,
    deontic_mood_GRAMMAR};
#endif
constant uint8_t number_word_sequence_long =
    sizeof(number_word_sequence) / sizeof(number_word_sequence[0]);

uint16_t word_belongs_INT(const uint16_t word, const uint16_t sequence_long,
                          constant uint16_t *sequence) {
  uint16_t indexFinger = 0;
  for (; indexFinger < sequence_long; ++indexFinger) {
    if (sequence[indexFinger] == word) {
      return truth_WORD;
    }
  }
  return lie_WORD;
}

uint16_t number_word_INT(const uint16_t word) {
  return word_belongs_INT(word, number_word_sequence_long,
                          number_word_sequence);
}

#ifndef OPENCL
constant char pyash_number_dictionary[NUMBER_WORD_SEQUENCE_LONG]
                                     [NUMBER_WORD_LONG] = {
                                         "zron", "hyik", "tyut", "tyin",
                                         "ksas", "hfak", "hlis", "hsip",
                                         "hwap", "twun", "htip", "slen",
                                         "tfat", "tses", "hses", "hpet",
                                         "hsos", "sret", "hdap", "dzin", 
                                         "syis"};
#endif

#ifndef OPENCL
constant uint16_t independentClause_perspective_word_sequence[] = {realis_mood_GRAMMAR,
                                               deontic_mood_GRAMMAR,
                                               finally_GRAMMAR, // technically is pilcrow
                                               epistemic_mood_GRAMMAR,
                                               interrogative_mood_GRAMMAR,
                                               directive_mood_GRAMMAR,
                                               hortative_mood_GRAMMAR,
                                               volitive_mood_GRAMMAR,
                                               deliberative_mood_GRAMMAR,
                                               desiderative_mood_GRAMMAR,
                                               imperative_mood_GRAMMAR,
                                               optative_mood_GRAMMAR,
                                               potential_mood_GRAMMAR,
                                               dubitative_mood_GRAMMAR,
                                               precative_mood_GRAMMAR,
                                               jussive_mood_GRAMMAR,
                                               permissive_mood_GRAMMAR,
                                               commissive_mood_GRAMMAR,
                                               eventive_mood_GRAMMAR,
                                               speculative_mood_GRAMMAR,
                                               benedictive_mood_GRAMMAR,
                                               inductive_mood_GRAMMAR,
                                               admonitive_mood_GRAMMAR,
                                               apprehensive_mood_GRAMMAR,
                                               imprecative_mood_GRAMMAR,
                                               assumptive_mood_GRAMMAR,
                                               prohibitive_mood_GRAMMAR,
                                               declarative_mood_GRAMMAR,
                                               affirmative_mood_GRAMMAR,
                                               irrealis_mood_GRAMMAR,
                                               sensory_evidential_mood_GRAMMAR,
                                               gnomic_mood_GRAMMAR,
                                               propositive_mood_GRAMMAR,
                                               necessitative_mood_GRAMMAR};
#endif

word_t is_grammatical_case_word(word_t word) {
  if (gen_indexFinger(word, GRAMMATICALCASE_WORD_SEQUENCE_LONG,
     grammaticalCase_word_sequence) != GRAMMATICALCASE_WORD_SEQUENCE_LONG) {
    return truth_WORD;
  }
  return lie_WORD;
}

word_t is_perspective_word(word_t word) {
  if (gen_indexFinger(word, PERSPECTIVE_WORD_SEQUENCE_LONG,
     perspective_word_sequence) != PERSPECTIVE_WORD_SEQUENCE_LONG) {
    return truth_WORD;
  }
  return lie_WORD;
}

word_t is_letter(char glyph){
  if (consonant_Q(glyph) == TRUE || vowel_Q(glyph) == TRUE ||
         tone_Q(glyph) == TRUE) {
    return truth_WORD;
  }
  return lie_WORD;
}
word_t is_grammar_word(word_t word){
  word_t grammar_denote_subset = word & SHORT_GRAMMAR_DENOTE_MASK;
  conditional(grammar_denote_subset == SHORT_GRAMMAR_DENOTE, return truth_WORD);
  grammar_denote_subset = word & LONG_GRAMMAR_DENOTE_MASK;
  conditional(grammar_denote_subset == LONG_GRAMMAR_DENOTE, return truth_WORD);
  return lie_WORD;
}
word_t is_quote_code(word_t word){
  word_t grammar_denote_subset = word & QUOTE_DENOTE_MASK;
  conditional(grammar_denote_subset == QUOTE_DENOTE, return truth_WORD);
  return lie_WORD;
}
