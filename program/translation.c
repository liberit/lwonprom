/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "tlep.h"
#include "parser.h"
#include "prng.h"
#include "pyash.h"
#include "pyashWords.h"
#include "sort.h"
#include "translation.h"
//#include <guarantee.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#define SORT_ARRAY_LONG 0x10
int phrase_code_compare(const void *a, const void *b) {
  return (*(uint16_t *)a <= *(uint16_t *)b);
}
void consonant_one_code_translation(const uint8_t consonant_one_code,
                                    uint8_t *vocal, char *text) {
  guarantee(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  guarantee(text != NULL);
  uint8_t indexFinger = 0;
  text[0] = (char)consonant_one_code_group[consonant_one_code][0];
  *vocal = FALSE;
  for (; indexFinger < CONSONANT_ONE_VOCAL_LONG; ++indexFinger) {
    if (consonant_one_code == consonant_one_vocal_group[indexFinger][1]) {
      *vocal = TRUE;
      break;
    }
  }
}

void consonant_two_code_translation(const uint8_t consonant_two_code,
                                    const uint8_t vocal, char *text) {
  guarantee(consonant_two_code < CONSONANT_TWO_ENCODE_LONG);
  guarantee(vocal == TRUE || vocal == FALSE);
  guarantee(text != NULL);
  if (vocal == TRUE) {
    text[0] = (char)consonant_two_vocal_group[consonant_two_code][0];
  } else {
    text[0] = (char)consonant_two_aspirate_group[consonant_two_code][0];
  }
}
void vowel_code_translation(const uint8_t vowel_code, char *text) {
  guarantee(vowel_code < VOWEL_ENCODE_LONG);
  guarantee(text != NULL);
  text[0] = (char)vowel_code_group[vowel_code][0];
}
void consonant_three_code_translation(const uint8_t consonant_three_code,
                                      char *text) {
  guarantee(consonant_three_code < VOWEL_ENCODE_LONG);
  guarantee(text != NULL);
  text[0] = (char)consonant_three_code_group[consonant_three_code][0];
}
void tone_code_translation(const uint8_t tone_code, uint8_t *tone_letter,
                           char *text) {
  guarantee(tone_code < TONE_ENCODE_LONG);
  guarantee(tone_letter != NULL);
  guarantee(text != NULL);
  char tone = (char)tone_code_group[tone_code][0];
  if (tone == 'M') {
    *tone_letter = FALSE;
  } else {
    text[0] = tone;
    *tone_letter = TRUE;
  }
}

// return word_long
uint16_t long_grammar_code_translation(const uint16_t word_code,
                                       const uint16_t text_long, char *text) {
  guarantee(text_long != 0);
  guarantee(text != NULL);
  // set first h
  uint16_t word_long = 0;
  const uint8_t consonant_one_code =
      (word_code & LONG_GRAMMAR_CONSONANT_ONE_MASK) >>
      LONG_GRAMMAR_CONSONANT_ONE_BEGIN;
  guarantee(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text + word_long);
  ++word_long;
  const uint8_t consonant_two_code =
      (word_code & LONG_GRAMMAR_CONSONANT_TWO_MASK) >>
      LONG_GRAMMAR_CONSONANT_TWO_BEGIN;
  consonant_two_code_translation(consonant_two_code, vocal, text + word_long);
  ++word_long;
  const uint8_t vowel_code =
      (word_code & LONG_GRAMMAR_VOWEL_MASK) >> LONG_GRAMMAR_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & LONG_GRAMMAR_TONE_MASK) >> LONG_GRAMMAR_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  text[word_long] = 'h';
  ++word_long;
  return word_long;
}
uint16_t short_grammar_code_translation(const uint16_t word_code,
                                        const uint16_t text_long, char *text) {
  guarantee(text_long != 0);
  guarantee(text != NULL);
  // set first h
  uint16_t word_long = 0;
  const uint8_t consonant_one_code =
      (word_code & SHORT_GRAMMAR_CONSONANT_ONE_MASK) >>
      SHORT_GRAMMAR_CONSONANT_ONE_BEGIN;
  guarantee(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text + word_long);
  ++word_long;
  const uint8_t vowel_code =
      (word_code & SHORT_GRAMMAR_VOWEL_MASK) >> SHORT_GRAMMAR_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & SHORT_GRAMMAR_TONE_MASK) >> SHORT_GRAMMAR_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  return word_long;
}
uint16_t long_root_code_translation(const uint16_t word_code,
                                    const uint16_t text_long, char *text) {
  guarantee(text_long != 0);
  guarantee(text != NULL);
  // set first h
  uint8_t word_long = 0;
  const uint8_t consonant_one_code =
      (word_code & LONG_ROOT_CONSONANT_ONE_MASK) >>
      LONG_ROOT_CONSONANT_ONE_BEGIN;
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text);
  ++word_long;
  const uint8_t consonant_two_code =
      (word_code & LONG_ROOT_CONSONANT_TWO_MASK) >>
      LONG_ROOT_CONSONANT_TWO_BEGIN;
  consonant_two_code_translation(consonant_two_code, vocal, text + word_long);
  ++word_long;
  // printf("consonant_two_code %X, text %s\n", consonant_two_code, text);
  const uint8_t vowel_code =
      (word_code & LONG_ROOT_VOWEL_MASK) >> LONG_ROOT_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & LONG_ROOT_TONE_MASK) >> LONG_ROOT_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  const uint8_t consonant_three_code =
      (word_code & LONG_ROOT_CONSONANT_THREE_MASK) >>
      LONG_ROOT_CONSONANT_THREE_BEGIN;
  consonant_three_code_translation(consonant_three_code, text + word_long);
  ++word_long;
  return word_long;
}
void phrase_quote_copy(const uint16_t phrase_termination_indexFinger,
                       const uint8_t tablet_long, const line_t *tablet,
                       uint64_t *phrase) {
  // copies the remainder of phrase from back,
  //  stops if indicator says start of a phrase
  guarantee(phrase != NULL);
  // printf("%s:%d:%s 0x%X pti\n", __FILE__, __LINE__, __FUNCTION__,
  // phrase_termination_indexFinger);
  guarantee(phrase_termination_indexFinger > 0 &&
            phrase_termination_indexFinger < LINE_LONG * tablet_long);
  //  const uint16_t indicator_list =
  //  (uint16_t)tablet[phrase_termination_indexFinger/LINE_LONG].s0;
  //  const uint8_t indicator = 1 & indicator_list;
  //  uint16_t code_word = 0;
  uint16_t indexFinger = phrase_termination_indexFinger - 1;
  uint8_t phrase_number_indexFinger = 0;
  uint16_t retrospective_phrase_indexFinger = 0;
  // uint64_t temporary_phrase = 0;
  tablet_retrospective_grammar_read(indexFinger, tablet_long, tablet,
                                    &retrospective_phrase_indexFinger);
  *phrase = 0;
  *phrase = (uint64_t)tablet_retrospective_word_read(
      phrase_termination_indexFinger, tablet_long, tablet, &indexFinger);
  // printf("%s:%d:%s 0x%lX phrase\n", __FILE__, __LINE__, __FUNCTION__,
  // *phrase);
  ++phrase_number_indexFinger;
  uint16_t quote_word_indexFinger = 0;
  uint16_t quote_word =
      tablet_upcoming_word_read(retrospective_phrase_indexFinger + 1,
                                tablet_long, tablet, &quote_word_indexFinger);
  // printf("%s:%d:%s 0x%X quote_word\n", __FILE__, __LINE__, __FUNCTION__,
  // quote_word);
  if ((quote_word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) {
    //   temporary_phrase = (
    //       ((uint64_t) quote_word) << (phrase_number_indexFinger *
    //       CODE_WORD_TIDBIT_LONG));
    // printf("%s:%d:%s 0x%lX tphrase\n", __FILE__, __LINE__, __FUNCTION__,
    // temporary_phrase);
    *phrase |= (((uint64_t)quote_word)
                << (phrase_number_indexFinger * CODE_WORD_TIDBIT_LONG));
  }
}

void verb_copy(const uint16_t phrase_termination_indexFinger,
               const uint8_t tablet_long, const line_t *tablet,
               uint64_t *phrase) {
  // copies the remainder of phrase from back,
  //  stops if indicator says start of a phrase
  guarantee(phrase != NULL);
  guarantee(phrase_termination_indexFinger > 0 &&
            phrase_termination_indexFinger < LINE_LONG * tablet_long);
  uint16_t indexFinger = phrase_termination_indexFinger - 1;
  uint16_t retrospective_phrase_indexFinger = 0;
  uint16_t sort_word_indexFinger = 0;
  word_t word = 0;
  tablet_retrospective_grammar_read(phrase_termination_indexFinger - 1,
                                    tablet_long, tablet,
                                    &retrospective_phrase_indexFinger);
  // DEBUGPRINT(("%X retrospective_phrase_indexFinger, %X
  // phrase_termination_indexFinger\n",
  //      retrospective_phrase_indexFinger, phrase_termination_indexFinger));
  *phrase = 0;
  for (; indexFinger > retrospective_phrase_indexFinger; --indexFinger) {
    guarantee(sort_word_indexFinger < 4);
    word = tablet_retrospective_word_read(indexFinger, tablet_long, tablet,
                                          &indexFinger);
    // DEBUGPRINT(("%X word\n", word));
    *phrase |= (code_name_t)word
               << (CODE_WORD_TIDBIT_LONG * sort_word_indexFinger);
    ++sort_word_indexFinger;
  }
}

uint16_t short_root_code_translation(const uint16_t word_code,
                                     const uint16_t text_long, char *text) {
  guarantee(text_long != 0);
  guarantee(text != NULL);
  // set first h
  uint16_t word_long = 0;
  text[0] = 'h';
  ++word_long;
  const uint8_t consonant_one_code =
      (word_code & SHORT_ROOT_CONSONANT_ONE_MASK) >>
      SHORT_ROOT_CONSONANT_ONE_BEGIN;
  guarantee(consonant_one_code < CONSONANT_ONE_ENCODE_LONG);
  uint8_t vocal = FALSE;
  consonant_one_code_translation(consonant_one_code, &vocal, text + word_long);
  ++word_long;
  const uint8_t vowel_code =
      (word_code & SHORT_ROOT_VOWEL_MASK) >> SHORT_ROOT_VOWEL_BEGIN;
  vowel_code_translation(vowel_code, text + word_long);
  ++word_long;
  const uint8_t tone_code =
      (word_code & SHORT_ROOT_TONE_MASK) >> SHORT_ROOT_TONE_BEGIN;
  uint8_t tone_letter = FALSE;
  tone_code_translation(tone_code, &tone_letter, text + word_long);
  if (tone_letter == TRUE) {
    ++word_long;
  }
  const uint8_t consonant_three_code =
      (word_code & SHORT_ROOT_CONSONANT_THREE_MASK) >>
      SHORT_ROOT_CONSONANT_THREE_BEGIN;
  consonant_three_code_translation(consonant_three_code, text + word_long);
  ++word_long;
  return word_long;
}

uint16_t quote_word_translate(uint16_t quote_code, uint16_t text_long,
                              char *text);
// uint16_t letter_code_translate(const uint16_t word_code, const uint16_t text_long,
//                              char *text) {
//   guarantee(text != NULL);
//   // guarantee(text_long != NULL);
//   guarantee(text_long >= WORD_LONG);
//   // const uint16_t short_sort = word_code & SHORT_SORT_MASK;
//   // const uint16_t long_sort = word_code & LONG_ROOT_CONSONANT_ONE_MASK;
//   // // printf("%s:%d 0x%04X word_code, 0x%X long_sort, 0x%X short_sort \n",
//   // // __FILE__,
//   // //    __LINE__, word_code, long_sort, short_sort);
//   // uint16_t word_long = 0;
//   // if (word_code == 0) {
//   //   word_long = 0;
//   // } else if (short_sort == SHORT_ROOT_DENOTE) {
//   //   word_long = short_root_code_translation(word_code, text_long, text);
//   // } else if (short_sort == LONG_GRAMMAR_DENOTE) {
//   //   word_long = long_grammar_code_translation(word_code, text_long, text);
//   // } else if (long_sort == SHORT_GRAMMAR_DENOTE) {
//   //   word_long = short_grammar_code_translation(word_code, text_long, text);
//   // } else if (long_sort == QUOTE_DENOTE) {
//   //   word_long = quote_word_translate(word_code, text_long, text);
//   // } else if (word_code > 0x800) {
//   //   word_long = long_root_code_translation(word_code, text_long, text);
//   // } else {
//   //   word_long = 0;
//   // }
//   return word_long;
// }
uint16_t word_code_translate(const uint16_t word_code, const uint16_t text_long,
                             char *text) {
  guarantee(text != NULL);
  // guarantee(text_long != NULL);
  guarantee(text_long >= WORD_LONG);
  const uint16_t short_sort = word_code & SHORT_SORT_MASK;
  const uint16_t long_sort = word_code & LONG_ROOT_CONSONANT_ONE_MASK;
  // printf("%s:%d 0x%04X word_code, 0x%X long_sort, 0x%X short_sort \n",
  // __FILE__,
  //    __LINE__, word_code, long_sort, short_sort);
  uint16_t word_long = 0;
  if (word_code == 0) {
    word_long = 0;
  } else if (short_sort == SHORT_ROOT_DENOTE) {
    word_long = short_root_code_translation(word_code, text_long, text);
  } else if (short_sort == LONG_GRAMMAR_DENOTE) {
    word_long = long_grammar_code_translation(word_code, text_long, text);
  } else if (long_sort == SHORT_GRAMMAR_DENOTE) {
    word_long = short_grammar_code_translation(word_code, text_long, text);
  } else if (long_sort == QUOTE_DENOTE) {
    word_long = quote_word_translate(word_code, text_long, text);
  } else if (word_code > 0x800) {
    word_long = long_root_code_translation(word_code, text_long, text);
  } else {
    word_long = 0;
  }
  return word_long;
}

void language_include_establish(uint16_t *produce_text_long,
                                char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(produce_text_long != NULL);
  const char recipe_text[0x20] = "#include \"pyac.h\"\n";
  const uint16_t recipe_text_magnitude = (uint16_t)stringlen(recipe_text, 0x20);
  // printf("%s:%d recipe_text_magnitude 0x%X\n", __FILE__, __LINE__,
  //       recipe_text_magnitude);
  perfect_copy(recipe_text_magnitude, recipe_text, *produce_text_long,
               produce_text);
  *produce_text_long = recipe_text_magnitude;
}

void cardinal_translate(uint16_t *produce_text_long, char *produce_text,
                        uint16_t *file_sort) {
  guarantee(produce_text != NULL);
  guarantee(produce_text_long != NULL);
  guarantee(file_sort != NULL);
  const char recipe_text[0x100] = "int main() {\n"
                                  "setlocale(LC_ALL,\"C\");\n"
                                  "bindtextdomain(\"pyac\",\".\");\n"
                                  "textdomain(\"pyac\");\n";
  const uint16_t recipe_text_magnitude =
      (uint16_t)stringlen(recipe_text, 0x100);
  // printf("%s:%d recipe_text_magnitude 0x%X\n", __FILE__, __LINE__,
  //       recipe_text_magnitude);
  perfect_copy(recipe_text_magnitude, recipe_text, *produce_text_long,
               produce_text);
  *produce_text_long = recipe_text_magnitude;
  *file_sort = cardinal_WORD;
}

#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
void return_translate(const uint8_t recipe_long, const line_t *recipe,
                      uint16_t *produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(produce_text[0] == (char)0); // must be clean
  // get number quote from recipe
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  phrase_situate(recipe_long, recipe, return_GRAMMAR, &phrase_long,
                 &phrase_place);
  uint16_t number_place = phrase_place + phrase_long - 2;
  const int return_number = (int)tablet_retrospective_word_read(
      number_place, recipe_long, recipe, &number_place);
  form_print(*produce_text_long, produce_text, "return 0x%X;\n", return_number);
  *produce_text_long = (uint16_t)stringlen(produce_text, *produce_text_long);
}
void natural_number_translate(const uint8_t recipe_long, const line_t *recipe,
                              uint16_t *produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(produce_text[0] == (char)0); // must be clean
                                         // get number quote from recipe
  // printf("%s:%d %s \n", __FILE__, __LINE__, __FUNCTION__);
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  phrase_situate(recipe_long, recipe, accusative_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  uint16_t number_place = phrase_place + 1;
  printf("%s:%d number_place %x \n", __FILE__, __LINE__, number_place);
  const int return_number = (int)tablet_retrospective_word_read(
      number_place, recipe_long, recipe, &number_place);
  uint16_t name_indexFinger = 0;
  char name[WORD_LONG * 8] = {0};
  const uint16_t name_long_maximum = WORD_LONG * 8;
  uint16_t name_remains = name_long_maximum;
  uint16_t name_long = 0;
  phrase_place = 0xF;
  // tablet_print(recipe_long, recipe);
  phrase_situate(recipe_long, recipe, nominative_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  // printf("%s:%d phrase_place 0x%X \n", __FILE__, __LINE__, phrase_place);
  for (name_indexFinger = phrase_place;
       name_indexFinger < phrase_place + phrase_long - 1; ++name_indexFinger) {
    name_remains = word_code_translate(
        tablet_upcoming_word_read(name_indexFinger, recipe_long, recipe,
                                  &name_indexFinger),
        name_remains, name + name_long);
    printf("%s:%d name %s name_remains 0x%X \n", __FILE__, __LINE__, name,
           name_remains);
    name_long += name_remains;
    name_remains = name_long_maximum - name_long;
  }
  form_print(*produce_text_long, produce_text, "uint16_t %s = 0x%X;\n", name,
             return_number);
  *produce_text_long = (uint16_t)stringlen(produce_text, *produce_text_long);
}
#endif
void name_extract(const uint8_t recipe_long, const line_t *recipe,
                  const uint16_t phrase, uint16_t *name_text_long,
                  char *name_text) {
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  const uint16_t name_long_maximum = WORD_LONG * 8;
  uint16_t name_indexFinger = 0;
  uint16_t name_remains = name_long_maximum;
  uint16_t name_long = 0;
  phrase_situate(recipe_long, recipe, phrase, &phrase_long, &phrase_place);
  uint16_t word = 0;
  printf("%s:%d:%s 0x%X phrase_place, 0x%X phrase_long\n", __FILE__, __LINE__,
         __FUNCTION__, phrase_place, phrase_long);
  for (name_indexFinger = phrase_place;
       name_indexFinger < phrase_long + phrase_place - 1; ++name_indexFinger) {
    guarantee(name_remains >= WORD_LONG);
    word = tablet_upcoming_word_read(name_indexFinger, recipe_long, recipe,
                                     &name_indexFinger);
    printf("%s:%d:%s 0x%X indexFinger, 0x%X max_indexFinger, 0x%X word\n",
           __FILE__, __LINE__, __FUNCTION__, name_indexFinger,
           phrase_long + phrase_place, word);
    name_remains =
        word_code_translate(word, name_remains, name_text + name_long);
    printf("%s:%d:%s  %s name, 0x%X remains\n", __FILE__, __LINE__,
           __FUNCTION__, name_text, name_remains);
    name_long += name_remains;
    name_remains = name_long_maximum - name_long;
  }
  *name_text_long = name_long;
}

void filename_establish(const uint8_t recipe_long, const line_t *recipe,
                        uint16_t *produce_filename_long, char *filename) {
  guarantee(filename != NULL);
  guarantee(produce_filename_long != NULL);
  name_extract(recipe_long, recipe, nominative_case_GRAMMAR,
               produce_filename_long, filename);
}

int minimum(int x, int y) { return (x < y) ? x : y; }

/* Function to merge the two haves arr[l..m] and arr[m+1..r] of array arr[] */
void merge(generic uint64_t arr[], int l, int m, int r) {
  int i, j, k;
  int n1 = m - l + 1;
  int n2 = r - m;

  /* create temp arrays */
  // uint64_t L[n1], R[n2];
  uint64_t L[SORT_ARRAY_LONG], R[SORT_ARRAY_LONG];

  /* Copy data to temp arrays L[] and R[] */
  for (i = 0; i < n1; i++)
    L[i] = arr[l + i];
  for (j = 0; j < n2; j++)
    R[j] = arr[m + 1 + j];

  /* Merge the temp arrays back into arr[l..r]*/
  i = 0;
  j = 0;
  k = l;
  while (i < n1 && j < n2) {
    // if (L[i] <= R[j])
    if (phrase_code_compare(L + i, R + j)) {
      arr[k] = L[i];
      i++;
    } else {
      arr[k] = R[j];
      j++;
    }
    k++;
  }

  /* Copy the remaining elements of L[], if there are any */
  while (i < n1) {
    arr[k] = L[i];
    i++;
    k++;
  }

  /* Copy the remaining elements of R[], if there are any */
  while (j < n2) {
    arr[k] = R[j];
    j++;
    k++;
  }
}

///* Function to print an array */
// void printArray(int A[], int size)
//{
//    int i;
//    for (i=0; i < size; i++)
//        printf("%d ", A[i]);
//    printf("\n");
//}

///* Driver program to test above functions */
// int main()
//{
//    int arr[] = {12, 11, 13, 5, 6, 7};
//    int n = sizeof(arr)/sizeof(arr[0]);
//
//    printf("Given array is \n");
//    printArray(arr, n);
//
//    mergeSort(arr, n);
//
//    printf("\nSorted array is \n");
//    printArray(arr, n);
//    return 0;
//}
/* Iterative mergesort function to sort arr[0...n-1] */
void mergeSort(generic uint64_t arr[], int n) {
  int curr_size;  // For current size of subarrays to be merged
                  // curr_size varies from 1 to n/2
  int left_start; // For picking starting index of left subarray
                  // to be merged

  // Merge subarrays in bottom up manner.  First merge subarrays of
  // size 1 to create sorted subarrays of size 2, then merge subarrays
  // of size 2 to create sorted subarrays of size 4, and so on.
  for (curr_size = 1; curr_size <= n - 1; curr_size = 2 * curr_size) {
    // Pick starting point of different subarrays of current size
    for (left_start = 0; left_start < n - 1; left_start += 2 * curr_size) {
      // Find ending point of left subarray. mid+1 is starting
      // point of right
      int mid = left_start + curr_size - 1;

      int right_end = minimum(left_start + 2 * curr_size - 1, n - 1);

      // Merge Subarrays arr[left_start...mid] & arr[mid+1...right_end]
      merge(arr, left_start, mid, right_end);
    }
  }
}

void sort_array_sort(const uint8_t array_long, uint64_t *sort_array) {
  guarantee(sort_array != NULL);
  if (array_long > 2) {
    // uint iteration;
    // printf("before: ");
    // sequence_print("%lX ", array_long, sort_array);
    // qsort(sort_array, array_long - 1 /* last one is verb, don't sort it!*/,
    //      sizeof(uint64_t), phrase_code_compare);
    mergeSort(sort_array, array_long - 1);
    // printf("after: ");
    // sequence_print("%lX ", array_long, sort_array);
  }
}
void sort_array_establish(const uint8_t tablet_long, const line_t *tablet,
                          const uint16_t input_tablet_indexFinger,
                          uint8_t *sort_array_long, uint64_t *sort_array,
                          uint16_t *produce_tablet_indexFinger) {
  guarantee(sort_array_long != NULL);
  guarantee(sort_array != NULL);
  guarantee(tablet != NULL);
  guarantee(tablet_long > 0);
  // algorithm
  // include cases, types, topic, and verb
  // TODO include nouns before a case as a type
  // include
  uint16_t tablet_indexFinger = input_tablet_indexFinger;
  uint16_t indicator_list = 0;
  uint8_t indicator = 0;
  uint8_t exit = FALSE;
  uint16_t word = 0;
  uint8_t sort_array_indexFinger = 0;
  uint16_t quote_sort = 0;
  uint64_t phrase = 0;
  uint16_t maximum_tablet_long = tablet_long * LINE_LONG;
  // DEBUGPRINT("0x%X tablet_indexFinger\n", tablet_indexFinger);
  guarantee(tablet_indexFinger < maximum_tablet_long);
  // uint8_t code_indexFinger = 0;
  // v8us quote_fill = {{0}};
  indicator_list = line_t_read(0, *tablet);
  indicator = (uint8_t)1 & indicator_list;
  uint16_t new_tablet_indexFinger = 0;
  for (; tablet_indexFinger < maximum_tablet_long; ++tablet_indexFinger) {
    phrase = 0;
    word = tablet_upcoming_grammar_or_quote_read(
        tablet_indexFinger, tablet_long, tablet, &new_tablet_indexFinger);
    // DEBUGPRINT(("0x%X tablet_indexFinger, 0x%X input_tablet_indexFinger"
    //            "0x%X word\n",
    //            tablet_indexFinger, input_tablet_indexFinger, word));
    // DEBUGPRINT("0x%X tablet_indexFinger, 0x%X input_tablet_indexFinger\n",
    //    tablet_indexFinger, input_tablet_indexFinger);
    //     continue;
    tablet_indexFinger = new_tablet_indexFinger;
    if (tablet_indexFinger > maximum_tablet_long) {
      // DEBUGPRINT(("0x%X tablet_indexFinger > 0x%X maximum_tablet_long",
      //            tablet_indexFinger, maximum_tablet_long));
      // tablet_indexFinger = maximum_tablet_long;
      guarantee(tablet_indexFinger != 0xFFFF);
      break;
    }
    if ((word & QUOTE_DENOTE_MASK) == QUOTED_DENOTE) {
      quote_sort = word;
      continue;
    }
    // DEBUGPRINT("0x%X word, 0x%X indexFinger, 0x%lX phrase\n",
    // word, tablet_indexFinger, phrase);
    // DEBUGPRINT(("word %04X SAI%X\n", word, sort_array_indexFinger));
    // tablet_print(tablet_long, tablet);
    switch (word) {
    case topic_case_GRAMMAR:
      verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      phrase = (phrase << CODE_WORD_TIDBIT_LONG) | topic_case_GRAMMAR;
      sort_array[sort_array_indexFinger] |= phrase;
      // printf("topic case set %016lX\n",
      // sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    case return_GRAMMAR:
      word = tablet_read(tablet_indexFinger, tablet_long, tablet);
      sort_array[sort_array_indexFinger] = (uint64_t)0;
      sort_array[sort_array_indexFinger] = word;
      sort_array[sort_array_indexFinger] |= ((uint64_t)quote_sort)
                                            << CODE_WORD_TIDBIT_LONG;
      exit = TRUE;
      break;
    case finally_GRAMMAR:
      phrase_quote_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      sort_array[sort_array_indexFinger] |= phrase;
      exit = TRUE;
      break;
    case 0:
      guarantee(1 == 0);
      break;
    default:
      if (is_perspective_word(word) == truth_WORD) {
        verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
        sort_array[sort_array_indexFinger] |= phrase;
        exit = TRUE;
        break;
      }
      sort_array[sort_array_indexFinger] = (uint64_t)0;
      if (quote_sort != 0) {
        sort_array[sort_array_indexFinger] = word;
        sort_array[sort_array_indexFinger] |=
            ((uint64_t)quote_sort << CODE_WORD_TIDBIT_LONG);
        // attach the grammar words if no quote sort
        quote_sort = 0;
      } else {
        phrase_quote_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
        sort_array[sort_array_indexFinger] |= phrase;
      }
      ++sort_array_indexFinger;
      break;
    }

    if (exit == TRUE) {
      // load verb
      ++sort_array_indexFinger;
      break;
    }
    // DEBUGPRINT("0x%X tablet_indexFinger, 0x%X sort_array_indexfinger\n",
    //    tablet_indexFinger, sort_array_indexFinger);
  }
  // DEBUGPRINT("0x%X tablet_indexFinger, 0x%X sort_array_indexfinger\n",
  //    tablet_indexFinger, sort_array_indexFinger);
  sort_array_sort(sort_array_indexFinger, sort_array);
  *sort_array_long = sort_array_indexFinger;
  *produce_tablet_indexFinger = tablet_indexFinger;
}

uint16_t vector_long_translate(uint16_t vector_code) {
  switch (vector_code) {
  case VECTOR_THICK_1:
    return 1;
  case VECTOR_THICK_2:
    return 2;
  case VECTOR_THICK_3:
    return 3;
  case VECTOR_THICK_4:
    return 4;
  case VECTOR_THICK_8:
    return 8;
  case VECTOR_THICK_16:
    return 16;
  default:
    guarantee(1 == 0);
    return 0;
  }
}
#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
void slogan_argument_translate(const uint16_t previous_indicator,
                               const uint16_t tablet_indexFinger,
                               const uint8_t tablet_long, const line_t *tablet,
                               uint16_t *text_long, char *text) {
  guarantee(text != NULL);
  guarantee(text_long != NULL);
  guarantee(*text_long >= 0);
  guarantee(tablet != NULL);
  guarantee(tablet_long > 0);
  guarantee(tablet_indexFinger > previous_indicator);
  // get type
  uint16_t indexFinger = 0;
  uint16_t quote_sort = tablet_upcoming_word_read(
      (uint16_t)previous_indicator + 1, tablet_long, tablet, &indexFinger);

  printf("%s:%d:%s 0x%X quote_sort \n", __FILE__, __LINE__, __FUNCTION__,
         quote_sort);
  uint16_t sort_denote = (quote_sort & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t vector_long = vector_long_translate(
      (quote_sort & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN);
  uint16_t word = 0;
  uint16_t gross_text_long = 0;
  uint16_t vacant_text_long = *text_long;
  uint16_t word_long;
  uint16_t sprintf_long = 0;
  // act appropriately
  switch (sort_denote) {
  // if text then return string quote of the length of the vector
  // if word then translate each to ASCII, and make string quote
  case WORD_SORT_DENOTE:
    // get length
    text[gross_text_long] = '_';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '(';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    for (indexFinger = previous_indicator + 2; indexFinger < vector_long;
         ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet,
                                       &indexFinger);
      word_long = vacant_text_long;
      word_long = word_code_translate(word, word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = ')';
    ++gross_text_long;
    --vacant_text_long;
    *text_long = gross_text_long;
    break;
  // if number then convert to hexadecimal number
  case UINT_SORT_DENOTE:
    sprintf_long += form_print(*text_long - gross_text_long,
                               text + gross_text_long, "uint16_t ");
    gross_text_long += sprintf_long;
    vacant_text_long -= sprintf_long;
    // tablet_print(tablet_long, tablet);
    for (indexFinger = previous_indicator + 2; indexFinger < tablet_indexFinger;
         ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet,
                                       &indexFinger);
      printf("%s:%d:%s 0x%X previous_indicator, 0x%X indexFinger, 0x%X word \n",
             __FILE__, __LINE__, __FUNCTION__, previous_indicator, indexFinger,
             word);
      word_long = vacant_text_long;
      word_long = word_code_translate(word, word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
      printf("%s:%d:%s 0x%X previous_indicator, 0x%X indexFinger, 0x%X word \n",
             __FILE__, __LINE__, __FUNCTION__, previous_indicator, indexFinger,
             word);
    }
    word = tablet_upcoming_word_read(previous_indicator + 1, tablet_long,
                                     tablet, &indexFinger);
    word_long = vacant_text_long;
    word_long = word_code_translate(word, word_long, text + gross_text_long);
    gross_text_long += word_long;
    vacant_text_long -= word_long;
    // DEBUGPRINT(("%s text\n", text));
    break;
  case INT_SORT_DENOTE:
    sprintf_long += form_print(*text_long - gross_text_long,
                               text + gross_text_long, "int16_t ");
    gross_text_long += sprintf_long;
    vacant_text_long -= sprintf_long;
    for (indexFinger = previous_indicator; indexFinger < tablet_indexFinger;
         ++indexFinger) {
      word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet,
                                       &indexFinger);
      word_long = vacant_text_long;
      word_long = word_code_translate(word, word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    break;
  // case INDEPENDENT_CLAUSE_SORT_DENOTE:
  //   sprintf_long += form_print(*text_long - gross_text_long, text +
  //   gross_text_long, "line_t ");
  //   gross_text_long += sprintf_long;
  //   vacant_text_long -= sprintf_long;
  //   for (indexFinger = previous_indicator; indexFinger < tablet_indexFinger;
  //   ++indexFinger) {
  //     word = tablet_upcoming_word_read(indexFinger, tablet_long, tablet,
  //     &indexFinger);
  //     word_long = vacant_text_long;
  //     word_code_translate(word, &word_long, text + gross_text_long);
  //     gross_text_long += word_long;
  //     vacant_text_long -= word_long;
  //   }
  //   break;
  default:
    printf("%s:%d:%s\t 0x%X sort_denote\n", __FILE__, __LINE__, __FUNCTION__,
           sort_denote);
    tablet_print(tablet_long, tablet);
    guarantee(1 == 0);
  }
  *text_long = gross_text_long;
}
#endif
void summon_argument_translate(uint8_t previous_indicator,
                               uint8_t tablet_indexFinger, line_t tablet,
                               uint16_t *text_long, char *text) {
  guarantee(tablet_indexFinger > previous_indicator);
  // get type
  uint16_t quote_sort = line_t_read(previous_indicator + 1, tablet);
  uint16_t sort_denote = (quote_sort & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t vector_long = vector_long_translate(
      (quote_sort & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN);
  uint8_t indexFinger = 0;
  uint16_t word = 0;
  uint16_t gross_text_long = 0;
  uint16_t vacant_text_long = *text_long;
  uint16_t word_long;
  // act appropriately
  switch (sort_denote) {
  // if text then return string quote of the length of the vector
  // if word then translate each to ASCII, and make string quote
  case WORD_SORT_DENOTE:
    // get length
    text[gross_text_long] = '_';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '(';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    for (indexFinger = 0; indexFinger < vector_long; ++indexFinger) {
      word = line_t_read(indexFinger + previous_indicator + 2, tablet);
      word_long = vacant_text_long;
      word_long = word_code_translate(word, word_long, text + gross_text_long);
      gross_text_long += word_long;
      vacant_text_long -= word_long;
    }
    text[gross_text_long] = '"';
    ++gross_text_long;
    --vacant_text_long;
    text[gross_text_long] = ')';
    ++gross_text_long;
    --vacant_text_long;
    *text_long = gross_text_long;
    break;
  // if number then convert to hexadecimal number
  default:
    printf("%s:%d:%s\n\t 0x%X sort_denote\n", __FILE__, __LINE__, __FUNCTION__,
           sort_denote);
    guarantee(1 == 0);
  }
}
#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
void slogan_input_translate(const uint8_t tablet_long, const line_t *tablet,
                            uint16_t *text_long, char *text) {
  guarantee(tablet != NULL);
  guarantee(text_long != NULL);
  guarantee(text != NULL);
  // for each grammatical case return the literal C values
  uint16_t tablet_indexFinger = 1;
  uint16_t word_code = 0;
  uint16_t previous_indicator = 0;
  uint8_t finally = FALSE;
  uint16_t max_tablet_long = tablet_long * LINE_LONG;
  uint16_t phrase_indicator = 0;
  uint8_t input_number = 0;
  uint16_t input_long = 0;
  uint16_t argument_long = *text_long;
  for (tablet_indexFinger = 1; tablet_indexFinger < max_tablet_long;
       ++tablet_indexFinger) {
    tablet_upcoming_grammar_read(tablet_indexFinger, tablet_long, tablet,
                                 &tablet_indexFinger);
    phrase_indicator = tablet_indexFinger;
    word_code = tablet_upcoming_word_read(tablet_indexFinger, tablet_long,
                                          tablet, &tablet_indexFinger);
    printf("%s:%d:%s 0x%X previous_indicator, 0x%X tablet_indexFinger, 0x%X "
           "word \n",
           __FILE__, __LINE__, __FUNCTION__, previous_indicator,
           tablet_indexFinger, word_code);
    switch (word_code) {
    case topic_case_GRAMMAR:
      break;
    case nominative_case_GRAMMAR:
      break;
    case epistemic_mood_GRAMMAR:
      finally = TRUE;
      break;
    case deontic_mood_GRAMMAR:
      finally = TRUE;
      break;
    case realis_mood_GRAMMAR:
      finally = TRUE;
      break;
    default:
      printf("%s:%d:%s 0x%X previous_indicator, 0x%X tablet_indexFinger, 0x%X "
             "word \n",
             __FILE__, __LINE__, __FUNCTION__, previous_indicator,
             tablet_indexFinger, word_code);
      if (input_number > 0) {
        input_long +=
            form_print(*text_long - input_long, text + input_long, ", ");
      }
      slogan_argument_translate(previous_indicator, tablet_indexFinger,
                                tablet_long, tablet, &argument_long,
                                text + input_long);
      input_long += argument_long;
      argument_long = (uint16_t)*text_long - input_long;
      printf("%s:%d:%s 0x%X word_code, %s text \n", __FILE__, __LINE__,
             __FUNCTION__, word_code, text);
      ++input_number;
      break;
    }
    previous_indicator = phrase_indicator;
    if (finally == TRUE) {
      break;
    }
  }
}
#endif

void slogan_sort_array_establish(const uint8_t tablet_long,
                                 const line_t *tablet, uint8_t *sort_array_long,
                                 uint64_t *sort_array) {
  guarantee(sort_array_long != NULL);
  guarantee(sort_array != NULL);
  guarantee(tablet != NULL);
  guarantee(tablet_long > 0);
  // algorithm
  // include cases, types, topic, and verb
  // include
  uint16_t tablet_indexFinger = 1;
  uint16_t indicator_list = 0;
  uint8_t indicator = 0;
  uint8_t exit = FALSE;
  uint16_t word = 0;
  uint8_t sort_array_indexFinger = 0;
  uint16_t quote_sort = 0;
  uint64_t phrase = 0;
  uint16_t phrase_place = 0;
  uint8_t phrase_long = 0;
  uint16_t maximum_tablet_long = tablet_long * LINE_LONG;
  // uint8_t code_indexFinger = 0;
  // v8us quote_fill = {{0}};
  indicator_list = line_t_read(0, *tablet);
  indicator = (uint8_t)1 & indicator_list;
  // uint8_t indexFinger = 0;
  // printf("indicator %X\n", (uint) indicator);
  // printf("indicator_list %X\n", (uint)indicator_list);
  // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger 0x%lX phrase\n", __FILE__,
  // __LINE__,
  //   __FUNCTION__, word, tablet_indexFinger, phrase);
  for (tablet_indexFinger = 1; tablet_indexFinger < maximum_tablet_long;
       ++tablet_indexFinger) {
    // sort_array[sort_array_indexFinger] = 0;
    phrase = 0;
    // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger 0x%lX phrase\n",
    // __FILE__,
    //       __LINE__, __FUNCTION__, word, tablet_indexFinger, (ulong)phrase);
    word = tablet_upcoming_grammar_or_quote_read(
        tablet_indexFinger, tablet_long, tablet, &tablet_indexFinger);
    // printf("%s:%d:%s\n\t 0x%X tablet_indexFinger\n", __FILE__, __LINE__,
    // __FUNCTION__, tablet_indexFinger);
    if (tablet_indexFinger > maximum_tablet_long) {
      printf("%s:%d:%s\n\t breaking\n", __FILE__, __LINE__, __FUNCTION__);
      break;
    }
    if ((word & QUOTE_DENOTE_MASK) == QUOTED_DENOTE) {
      quote_sort = word;
      continue;
    }
    // printf("%s:%d:%s\n\t 0x%X word, 0x%X indexFinger, 0x%lX phrase\n",
    // __FILE__, __LINE__,
    //    __FUNCTION__, word, tablet_indexFinger, phrase);
    // printf("word %04X SAI%X\n", word, sort_array_indexFinger);
    switch (word) {
    case topic_case_GRAMMAR:
      // verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      // phrase = (phrase << CODE_WORD_TIDBIT_LONG) | topic_case_GRAMMAR;
      // sort_array[sort_array_indexFinger] |= phrase;
      //// printf("topic case set %016lX\n",
      //// sort_array[sort_array_indexFinger]);
      //++sort_array_indexFinger;
      break;
    case nominative_case_GRAMMAR:
      // verb_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
      // phrase = (phrase << CODE_WORD_TIDBIT_LONG) | nominative_case_GRAMMAR;
      // sort_array[sort_array_indexFinger] |= phrase;
      //// printf("topic case set %016lX\n",
      //// sort_array[sort_array_indexFinger]);
      //++sort_array_indexFinger;
      break;
    case realis_mood_GRAMMAR:
      exit = TRUE;
      break;
      example(declarative_mood_GRAMMAR, exit = TRUE);
    default:
      sort_array[sort_array_indexFinger] = (uint64_t)0;
      // printf("%s:%d\n\t 0x%X quote_sort, 0x%lX phrase\n", __FILE__, __LINE__,
      // quote_sort, phrase);
      if (quote_sort != 0) {
        sort_array[sort_array_indexFinger] = word;
        sort_array[sort_array_indexFinger] |=
            ((uint64_t)quote_sort << CODE_WORD_TIDBIT_LONG);
        // attach the grammar words if no quote sort
        quote_sort = 0;
      } else {
        phrase_quote_copy(tablet_indexFinger, tablet_long, tablet, &phrase);
        sort_array[sort_array_indexFinger] |= phrase;
      }
      printf("%s:%d:%s\n\t 0x%X quote_sort, 0x%lX phrase\n", __FILE__, __LINE__,
             __FUNCTION__, quote_sort, (ulong)phrase);
      printf("case detected %016lX\n", sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    }

    if (exit == TRUE) {
      // load verb
      // printf("verb set %016lX\n", sort_array[sort_array_indexFinger]);
      ++sort_array_indexFinger;
      break;
    }
  }
  // get nominative case contents and put it at end.
  phrase_situate(tablet_long, tablet, nominative_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  verb_copy(phrase_place + phrase_long - 1, tablet_long, tablet, &phrase);
  // phrase = (phrase << CODE_WORD_TIDBIT_LONG) | nominative_case_GRAMMAR;
  sort_array[sort_array_indexFinger] |= phrase;
  // printf("topic case set %016lX\n",
  // sort_array[sort_array_indexFinger]);
  ++sort_array_indexFinger;

  // printf("exit %X\n", exit);
  sort_array_sort(sort_array_indexFinger, sort_array);
  *sort_array_long = sort_array_indexFinger;
}

void sort_array_tablet_translate(uint16_t sort_array_long, uint64_t *sort_array,
                                 line_t *sort_tablet) {
  guarantee(sort_tablet != NULL);
  guarantee(sort_array_long >= 0);
  guarantee(sort_array != NULL);
  uint16_t indexFinger = 0;
  uint16_t sort_indexFinger = 0;
  uint8_t tablet_indexFinger = 1;
  uint16_t code_word = 0;
  for (; indexFinger < sort_array_long; ++indexFinger) {
    for (sort_indexFinger = 3; sort_indexFinger < 4; --sort_indexFinger) {
      code_word =
          (uint16_t)(sort_array[indexFinger] >> (sort_indexFinger * 16));
      if (code_word > 0) {
        // printf("%s code_word 0x%X\n", __FILE__, code_word);
        line_t_write(tablet_indexFinger, code_word, sort_tablet);
        ++tablet_indexFinger;
        guarantee(tablet_indexFinger < LINE_LONG);
      }
    }
  }
}

void sort_array_name_translate(const uint8_t sort_array_long,
                               const uint64_t *sort_array, uint8_t *name_long,
                               char *name) {
  guarantee(sort_array_long > 0);
  guarantee(sort_array != NULL);
  guarantee(name_long != NULL);
  guarantee(*name_long > sort_array_long * WORD_LONG);
  guarantee(name != NULL);
  uint16_t indexFinger = 0;
  uint16_t sort_indexFinger = 0;
  uint8_t tablet_indexFinger = 1;
  uint16_t code_word = 0;
  // char word[WORD_LONG] = {0};
  const uint16_t max_name_long = *name_long;
  uint16_t word_long = max_name_long;
  uint16_t assigned_name_long = 0;
  // const uint16_t word_long_max = WORD_LONG;
  // uint16_t word_long_remains = WORD_LONG;
  //     printf("%s:%d:%s 0x%X code_word, %s word \n", __FILE__, __LINE__,
  //      __FUNCTION__, code_word);
  for (; indexFinger < sort_array_long; ++indexFinger) {
    printf("%s:%d:%s 0x%lX sort item\n", __FILE__, __LINE__, __FUNCTION__,
           sort_array[indexFinger]);
    for (sort_indexFinger = 3; sort_indexFinger < 4; --sort_indexFinger) {
      printf("%s:%d:%s 0x%X sort_indexFinger, 0x%lX\n", __FILE__, __LINE__,
             __FUNCTION__, sort_indexFinger, sort_array[indexFinger]);
      code_word = (uint16_t)(
          (uint64_t)(sort_array[indexFinger] >> (sort_indexFinger * 16)));
      if (code_word > 0) {
        // need quote translate for quotes
        guarantee(word_long >= WORD_LONG);
        word_long = word_code_translate(code_word, word_long,
                                        name + assigned_name_long);
        assigned_name_long += word_long;
        word_long = max_name_long - assigned_name_long;
        // printf("%s:%d:%s 0x%X code_word, %d word_long, 0x%X
        // assigned_name_long\n", __FILE__, __LINE__,
        // __FUNCTION__, code_word, word_long, assigned_name_long );
        // printf("%s:%d:%s 0x%X code_word, %d word_long, %s word, %s name\n",
        // __FILE__, __LINE__,
        // __FUNCTION__, code_word, word_long, word, name);
        ++tablet_indexFinger;
        // word_long = word_long_max;
        // memset(word, 0, word_long);
        guarantee(tablet_indexFinger < LINE_LONG);
      }
    }
  }
  *name_long = (uint8_t)assigned_name_long;
}

#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
void recipe_slogan_translate(const uint8_t tablet_long, const line_t *recipe,
                             uint16_t *produce_text_long, char *produce_text) {
  guarantee(tablet_long > 0);
  guarantee(recipe != NULL);
  guarantee(produce_text_long != NULL);
  guarantee(produce_text != NULL);
  const uint16_t max_produce_text_long = *produce_text_long;
  uint16_t text_long = 0;
  uint8_t sort_array_long = SORT_ARRAY_LONG;
  uint64_t sort_array[SORT_ARRAY_LONG] = {0};
  uint8_t recipe_name_long = 16 * WORD_LONG;
  // char input_phrases[16*WORD_LONG] = {0};
  char recipe_name[20 * WORD_LONG] = {0};
  char input_arguments[20 * WORD_LONG] = {0};
  uint16_t input_arguments_long = 20 * WORD_LONG;
  // skipping topic
  // make function word
  slogan_sort_array_establish(tablet_long, recipe, &sort_array_long,
                              sort_array);
  printf("%s:%d:%s 0x%X sort_array_long\n", __FILE__, __LINE__, __FUNCTION__,
         sort_array_long);
  sort_array_name_translate(sort_array_long, sort_array, &recipe_name_long,
                            recipe_name);
  // put in arguments, const for input, pointers for output
  printf("%s:%d:%s 0x%X recipe_name_long\n", __FILE__, __LINE__, __FUNCTION__,
         recipe_name_long);
  printf("%s:%d:%s %s recipe_name\n", __FILE__, __LINE__, __FUNCTION__,
         recipe_name);
  slogan_input_translate(tablet_long, recipe, &input_arguments_long,
                         input_arguments);
  text_long += (uint16_t)form_print(*produce_text_long, produce_text,
                                    "void %s(", recipe_name);
  text_long +=
      (uint16_t)form_print(*produce_text_long - text_long,
                           produce_text + text_long, "%s){\n", input_arguments);
  guarantee(text_long < max_produce_text_long);
  printf("%s:%d:%s %s recipe_name, %s produce_text\n", __FILE__, __LINE__,
         __FUNCTION__, recipe_name, produce_text);
  *produce_text_long = text_long;
  // exit(19);
}
void import_translate(const uint8_t recipe_long, const line_t *recipe,
                      uint16_t *produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(produce_text[0] == (char)0); // must be clean
  // get number quote from recipe
  // printf("%s:%d %s \n", __FILE__, __LINE__, __FUNCTION__);
  char name[WORD_LONG * 8] = {0};
  uint16_t name_long = WORD_LONG * 8;
  name_extract(recipe_long, recipe, accusative_case_GRAMMAR, &name_long, name);
  form_print(*produce_text_long, produce_text, "#include \"%s.h\"\n", name);
  *produce_text_long = (uint16_t)stringlen(produce_text, *produce_text_long);
}
void cycle_translate(const uint8_t recipe_long, const line_t *recipe,
                     uint16_t *produce_text_long, char *produce_text) {
  guarantee(produce_text != NULL);
  guarantee(produce_text[0] == (char)0); // must be clean
  // get variable from recipe
  uint16_t phrase_place = 0xF;
  uint8_t phrase_long = 0;
  printf("%s:%d:%s phrase_place 0x%X\n", __FILE__, __LINE__, __FUNCTION__,
         phrase_place);
  phrase_situate(recipe_long, recipe, ablative_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  // printf("%s:%d phrase_place 0x%X\n", __FILE__, __LINE__, phrase_place);
  // tablet_print(recipe_long, recipe);
  uint16_t number_place = phrase_place + 1;
  const int ablativeCase_number = (int)tablet_upcoming_word_read(
      number_place, recipe_long, recipe, &number_place);
  phrase_place = 0xF;
  phrase_situate(recipe_long, recipe, instrumental_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  printf("%s:%d:%s phrase_place 0x%X\n", __FILE__, __LINE__, __FUNCTION__,
         phrase_place);
  number_place = phrase_place + 1;
  const int instrumentalCase_number = (int)tablet_upcoming_word_read(
      number_place, recipe_long, recipe, &number_place);
  phrase_place = 0xF;
  phrase_situate(recipe_long, recipe, allative_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  printf("%s:%d:%s phrase_place 0x%X\n", __FILE__, __LINE__, __FUNCTION__,
         phrase_place);
  number_place = phrase_place + 1;
  const int allativeCase_number = (int)tablet_upcoming_word_read(
      number_place, recipe_long, recipe, &number_place);
  uint16_t name_indexFinger = 0;
  char name[WORD_LONG * 8] = {0};
  const uint16_t name_long_maximum = WORD_LONG * 8;
  uint16_t name_remains = name_long_maximum;
  uint16_t name_long = 0;
  phrase_place = 0xF;
  phrase_situate(recipe_long, recipe, accusative_case_GRAMMAR, &phrase_long,
                 &phrase_place);
  printf("%s:%d phrase_place 0x%X\n", __FILE__, __LINE__, phrase_place);
  for (name_indexFinger = phrase_place;
       name_indexFinger < phrase_long + phrase_place - 1; ++name_indexFinger) {
    name_remains = word_code_translate(
        tablet_upcoming_word_read(name_indexFinger, recipe_long, recipe,
                                  &name_indexFinger),
        name_remains, name + name_long);
    name_long += name_remains;
    name_remains = name_long_maximum - name_long;
  }
  form_print(*produce_text_long, produce_text,
             "for(%s = 0x%X; %s < 0x%X; %s += 0x%X ){\n", name,
             ablativeCase_number, name, allativeCase_number, name,
             instrumentalCase_number);
  *produce_text_long = (uint16_t)stringlen(produce_text, *produce_text_long);
}
#endif

#define quote_word_number_modernize                                            \
  gross_text_long += word_long;                                                \
  vacancy_text_long -= word_long;                                              \
  word_long = vacancy_text_long;                                               \
  guarantee(word_long >= WORD_LONG);
uint16_t quote_word_translate(uint16_t quote_code, const uint16_t text_long,
                              char *text) {
  // guarantee(text_long != NULL);
  guarantee(text_long > WORD_LONG);
  guarantee(text != NULL);
  // translate the quote sections and append them
  // const uint16_t pointed_tidbit =
  //    (quote_code & POINTED_MASK) >> POINTED_BEGIN;
  // const uint16_t sequence_tidbit =
  //    (quote_code & SEQUENCE_MASK) >> SEQUENCE_BEGIN;
  const uint16_t vector_thick =
      (quote_code & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN;
  const uint16_t scalar_thick =
      (quote_code & SCALAR_THICK_MASK) >> SCALAR_THICK_BEGIN;
  const uint16_t sort_denote =
      (quote_code & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t gross_text_long = 0;
  uint16_t vacancy_text_long = text_long;
  uint16_t word_long = vacancy_text_long;
  // printf("%s:%d:%s 0x%X sort_denote\n", __FILE__, __LINE__, __FUNCTION__,
  //    sort_denote);
  guarantee(word_long >= WORD_LONG);
  //struct Text produce_text;
  // produce_text.letters = text;
  // produce_text.max_length = text_long;
  // produce_text.length = 0;
  //struct Phrase input_phrase;
  //input_phrase.page.plength = 
  switch (sort_denote) {
  case UINT_SORT_DENOTE:
    // word_code_translate(number_GRAMMAR, &word_long, text + gross_text_long);
    word_long = 0;
    // translate the number
    break;
  case WORD_SORT_DENOTE:
    word_long =
        word_code_translate(word_GRAMMAR, word_long, text + gross_text_long);
    break;
  case LETTER_SORT_DENOTE:
   // produce_text =
   //     letter_quote_translate(input_phrase, produce_text);
    //word_long = produce_text.length;
    word_long =
        word_code_translate(quoted_GRAMMAR, word_long, text + gross_text_long);
    break;
  default:
    guarantee(1 == 0);
    break;
  }
  quote_word_number_modernize;
  switch (vector_thick) {
  case VECTOR_THICK_2:
    word_long =
        word_code_translate(two_WORD, word_long, text + gross_text_long);
    break;
  case VECTOR_THICK_1:
    word_long = 0;
    break;
  default:
    printf("%s:%d\t vector_thick 0x%X\n", __FILE__, __LINE__, vector_thick);
    guarantee(1 == 0);
    break;
  }
  quote_word_number_modernize;
  switch (scalar_thick) {
  case SIXTEEN_TIDBIT_SCALAR_THICK:
    word_long =
        word_code_translate(number_GRAMMAR, word_long, text + gross_text_long);
    break;
  default:
    guarantee(1 == 0);
    break;
  }
  quote_word_number_modernize;
  return gross_text_long;
}


uint64_t phrase_code_establish(const struct Phrase input) {
  // grab types, cases and quotes
  //     for verbs also grab the verb
  word_t word = 0;
  struct Phrase perspective_phrase;
  uint8_t phrase_code_txik = 0;
  uint64_t phrase_code_number = 0;
  uint16_t* phrase_code = (uint16_t*) &phrase_code_number;
  NewPagePhrase(list, 1);
  list = derive_aListOfQuoteWordsAndGrammarWords_fromThePhrase(list, input);
  //Page_print(list.page);
  conditional(list.length == 0, return 0);
  
  word = phrase_word_read(list, list.length - 1);
  word_t temp_word = 0;

  conditional(is_perspective_word(word) == truth_WORD, 
    phrase_zero(list);
    list.begin = 0;
    repeat(input.length,
      temp_word = phrase_word_read(input, iterator);
      conditional(temp_word == 0, continue);
      conditional(is_perspective_word(temp_word) == truth_WORD, break);
      list = addenda_theWord_toThePhrase(temp_word, list));
    );
  
  down_repeat(list.length,
      // check if phrase is a verb
      word = phrase_word_read(list, iterator - 1);
      //DEBUGPRINT(("%X word\n", word));
      guarantee(phrase_code_txik <= 4 /*64bit/16bit*/);
      phrase_code[phrase_code_txik] ^= word;
      //phrase_code_number ^= word;
      //DEBUGPRINT(("%X phrse_code, %lX phrase_code_number\n", 
      //    phrase_code[phrase_code_txik], phrase_code_number));
      phrase_code_txik++;
      );
  return phrase_code_number;
}


struct SortSeries sort_series_establish(const struct Phrase input, 
    struct SortSeries sort_series){
  // make sure only using first sentence

  // upcoming phrase read
  // for each phrase grab types cases and quotes
  //     for verbs also grab the verb
  // put each into a 64bit number, with the case at the sorting place
  
  struct Phrase htin = upcoming_htin_found(input);
  conditional(htin.length == 0, htin = input);
  struct Phrase phrase;
  uint64_t phrase_code = 0;
  struct Phrase search = htin;
  const txik_t max_txik = input.begin+input.length;
  repeat(sort_series.max_length, 
      //text_phrase_print(search);
      //Page_print(search.page);
      phrase = upcoming_phrase_found(search);
      conditional(phrase.length == 0, break);
      //text_phrase_print(phrase);
      phrase_code = phrase_code_establish(phrase);
      //DEBUGPRINT(("%lX, %X search.begin, %X search.length\n", phrase_code,
      //    search.begin, search.length));
      search.begin = phrase.begin + phrase.length;
      search.length = max_txik - search.begin;
      conditional(phrase_code == 0, continue);
      sort_series.series[iterator] = phrase_code;
      sort_series.length += 1;
      );
  // order the phrases?
  //struct Phrase upcoming_grammar_or_quote_read(input);
  // then sort the array of 32bit numbers with qsort
  sort_array_sort(sort_series.length, sort_series.series);
  return sort_series;
}

code_name_t code_name_derive(struct Phrase input){
  // get types cases and quotes
  // get verb
  // put each into a 32bit number
  // then sort the array of 32bit numbers with qsort
  // then get a 64bit hash of the array from hashlittle64 and return it
  guarantee(input.page.plength != 0);
  guarantee(input.page.lines != NULL);
  //text_phrase_print(input);
  uint64_t sort_array[0x10] = {0};
  struct SortSeries sort_series; 
  sort_series.length = 0;
  sort_series.max_length = 0x10;
  sort_series.series = sort_array;
  sort_series = sort_series_establish(input, sort_series);
  //tablet_print(sort_series.length, (const line_t *)  sort_array);
  // repeat(sort_series.length, DEBUGPRINT(("%lX \n" , sort_series.series[iterator])));
  code_name_t code_name = hash64(sort_series.length, sort_array);
  return code_name;
}


//void old_code_name_derive(const uint8_t tablet_long, const line_t *tablet,
//                      const uint16_t input_tablet_indexFinger,
//                      code_name_t *code_name,
//                      uint16_t *produce_tablet_indexFinger) {
//  // new derive_code_name function
//  // get cases and quotes
//  // get topic name along with topic
//  // get verb
//  // put each into a 32bit number
//  // then sort the array of 32bit numbers with qsort
//  // then get a 64bit hash of the array from hashlittle64 and return it
//  guarantee(tablet_long != 0);
//  guarantee(tablet != NULL);
//  guarantee(code_name != NULL);
//  uint8_t sort_array_long = 0;
//  uint64_t sort_array[0x10] = {0};
//  // tablet_print(tablet_long, tablet);
//  sort_array_establish(tablet_long, tablet, input_tablet_indexFinger,
//                       &sort_array_long, sort_array,
//                       produce_tablet_indexFinger);
//
//  tablet_print(sort_array_long, (const line_t *)  sort_array);
//  *code_name = hash(sort_array_long, sort_array);
//}
uint16_t number_to_word(const uint16_t number, const uint16_t base) {
  guarantee(number < base);
  return number_word_sequence[number];
}

#define SWAP(x, y)                                                             \
  do {                                                                         \
    __typeof__(y) _y = y;                                                      \
    y = x;                                                                     \
    x = _y;                                                                    \
  } while (0)

#define least_supreme_swap(sequence_long, sequence)                            \
  __typeof__(sequence[0]) swap_temporary = sequence[0];                        \
  sequence[0] = sequence[sequence_long - 1];                                   \
  sequence[sequence_long - 1] = swap_temporary;

#define sequence_reverse(sequence_long, sequence)                              \
  repeat(sequence_long / 2, least_supreme_swap(sequence_long - iterator * 2,   \
                                               (sequence + iterator)););

#define safe_div(divisor, dividend, quotient, remainder)                       \
  remainder = divisor % dividend;                                              \
  quotient = divisor / dividend;

void number_to_sequence(const uint16_t number, const uint16_t base,
                        const uint16_t endian, uint8_t *sequence_long,
                        uint16_t *sequence) {
  // assert(number < NUMBER_WORD_SEQUENCE_LONG);
  uint16_t temporary_number = number;
  uint16_t iterator = 0;
  // if ((0x10 % base) > 0 || produce_long == 0)
  //  ++produce_long;
  // DEBUGPRINT(("1 0x%X temporary_number, 0x%X base, 0x%X
  // sequence[iterator]\n",
  //          temporary_number, base, sequence[iterator]));
  repeat_with_except(
      0x10, iterator, temporary_number == 0,
      safe_div(temporary_number, base, temporary_number, sequence[iterator]);
      // DEBUGPRINT(("2 0x%X temporary_number, 0x%X base, 0x%X
      // sequence[iterator]\n",
      //          temporary_number, base, sequence[iterator]));
  );
  //  DEBUGPRINT(("0x%X iterator\n", iterator));
  *sequence_long = (uint8_t)(iterator + 1);
  // DEBUGPRINT(("B 0x%X sequence[0], 0x%X [1]\n", sequence[0], sequence[1]));
  if (endian == giant_WORD)
    sequence_reverse(*sequence_long, sequence);

  // DEBUGPRINT(("A 0x%X sequence[0], 0x%X [1]\n", sequence[0], sequence[1]));
}

uint16_t number_to_text(const uint16_t number, const uint16_t base,
                        const uint16_t endian,
                        const uint16_t number_dictionary_long,
                        constant char number_dictionary[][NUMBER_WORD_LONG],
                        const uint16_t text_long, char text[]) {
  guarantee(number_dictionary_long != 0);
  guarantee(text_long > 0);
  guarantee(text != NULL);
  uint16_t sequence[NUMBER_TIDBIT_LONG] = {0};
  uint8_t sequence_long = NUMBER_TIDBIT_LONG;
  number_to_sequence(number, base, endian, &sequence_long, sequence);
  // DEBUGPRINT(
  //    ("0x%X sequence_long, 0x%X text_long\n", sequence_long, text_long));
  const uint16_t max_text_long = text_long;
  uint16_t number_text_long = 0;
  repeat(sequence_long,
         number_text_long += text_copy(
             number_dictionary_long, number_dictionary[sequence[iterator]],
             max_text_long - number_text_long, text + number_text_long));
  guarantee(number_text_long < max_text_long);
  // DEBUGPRINT(("0x%X number_text_long, %s text\n", number_text_long, text));
  return number_text_long;
}
struct Text letter_quote_translate(struct Phrase input, 
    struct Text produce, txik_t *indexFinger) {
  // decoding words:
  //  how do I do it?
  //  going from tablet to text
  //  okay, start with zi, then the letter code, prih. 
  NewText(begin, "zi.prih.");
  produce = text_addenda(begin, produce);
  //  zi.prih. then copy the contents inside literally to the output,
  //// find the contents length
  //Page_print(input.page);
  word_t letter = phrase_word_read(input, 1);
  // DEBUGPRINT(("%X indexfinger, %X input.begin\n", *indexFinger, 
  //       input.begin));
  *indexFinger = input.begin + 1;
  produce.letters[produce.length] = letter;
  produce.length++;
  //  then .prih.zi
  NewText(final, ".prih.zi");
  // conditional(letter == 'X', text_print(produce));
  //final.length++;
  produce = text_addenda(final, produce);
  // conditional(letter == 'X', text_print(produce));
  // DEBUGPRINT(("%X produce.length\n", produce.length));
  //  done
  //  hope that helps,
  //  good luck man,
  //  hope this works out for you..
  //  test the evolutionary programmer with letters next.
  //  k yw
  //*indexFinger += 1;
  return produce;
}

struct Text number_to_text_translate(const uint64_t number, const uint8_t base, 
    struct Text produce_text) {
    guarantee(produce_text.max_length > 0);
    produce_text.length = number_to_text(
        number, base, giant_WORD, NUMBER_WORD_SEQUENCE_LONG,
        pyash_number_dictionary, produce_text.max_length, produce_text.letters);
    return produce_text;
}
uint16_t /* produce_long */ 
quote_phrase_translate(uint16_t *tablet_indexFinger,
                                const uint8_t sequence_long,
                                const line_t *tablet, const uint16_t text_long,
                                char *text) {
  // guarantee(text_long != NULL);
  guarantee(text_long > WORD_LONG);
  guarantee(text != NULL);
  guarantee(text[0] == 0);
  uint16_t indexFinger = 0;
  uint16_t quote_code = tablet_upcoming_word_read(
      *tablet_indexFinger, sequence_long, tablet, &indexFinger);
  guarantee((quote_code & QUOTE_DENOTE_MASK) == QUOTE_DENOTE);
  // translate the quote sections and append them
  // const uint16_t pointed_tidbit =
  //    (quote_code & POINTED_MASK) >> POINTED_BEGIN;
  // const uint16_t sequence_tidbit =
  //    (quote_code & SEQUENCE_MASK) >> SEQUENCE_BEGIN;
  const uint16_t vector_thick =
      (quote_code & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN;
  const uint16_t scalar_thick =
      (quote_code & SCALAR_THICK_MASK) >> SCALAR_THICK_BEGIN;
  const uint16_t sort_denote =
      (quote_code & SORT_DENOTE_MASK) >> SORT_DENOTE_BEGIN;
  uint16_t gross_text_long = 0;
  uint16_t vacancy_text_long = text_long;
  uint16_t word_long = vacancy_text_long;

  // printf("%s:%d:%s 0x%X sort_denote\n", __FILE__, __LINE__, __FUNCTION__,
  //    sort_denote);
  guarantee(word_long >= WORD_LONG);
  uint16_t number = 0;
  struct Text produce_text;
  produce_text.letters = text;
  produce_text.length = 0;
  produce_text.max_length = text_long;
  struct Phrase input_phrase;
  input_phrase.page.lines = tablet;
  input_phrase.page.plength = sequence_long;
  input_phrase.begin = indexFinger;
  input_phrase.length = sequence_long*LINE_LONG - indexFinger ;
  switch (sort_denote) {
  case UINT_SORT_DENOTE:
    // word_code_translate(number_GRAMMAR, &word_long, text + gross_text_long);
    number = tablet_upcoming_word_read(indexFinger + 1, sequence_long, tablet,
                                       &indexFinger);
    word_long = number_to_text(
        number, DEFAULT_NUMBER_BASE, giant_WORD, NUMBER_WORD_SEQUENCE_LONG,
        pyash_number_dictionary, word_long, text + gross_text_long);
    // DEBUGPRINT(("0x%X word_long, %s text\n", word_long, text));
    // word_code_translate(number_word, &word_long, text + gross_text_long);
    // word_long = 0;
    // translate the number
    break;
  case WORD_SORT_DENOTE:
    number = tablet_upcoming_word_read(indexFinger + 1, sequence_long, tablet,
                                       &indexFinger);
    word_long =
         word_code_translate(word_GRAMMAR, word_long, text + gross_text_long);
    break;
  case LETTER_SORT_DENOTE:
    word_long =
        word_code_translate(letter_GRAMMAR, word_long, text + gross_text_long);
    // struct Text produce_text = 
    produce_text = letter_quote_translate(input_phrase, produce_text, 
        tablet_indexFinger);
    word_long = produce_text.length;
    // DEBUGPRINT(("%X word_long\n", word_long));
    break;
  default:
    guarantee(1 == 0);
    break;
  }
  quote_word_number_modernize;
  if (sort_denote == UINT_SORT_DENOTE) {
  switch (scalar_thick) {
  case SIXTEEN_TIDBIT_SCALAR_THICK:
    word_long =
        word_code_translate(number_GRAMMAR, word_long, text + gross_text_long);
    *tablet_indexFinger += 1;
    break;
  default:
    guarantee(1 == 0);
    break;
  }
  quote_word_number_modernize;
  }
  return gross_text_long;
}

uint16_t phrase_translate(uint16_t *tablet_indexFinger,
                          const uint8_t sequence_long, const line_t tablet[],
                          const uint16_t sort, uint16_t text_long,
                          char text[]) {

  uint16_t word_code = tablet_upcoming_word_read(
      *tablet_indexFinger, sequence_long, tablet, tablet_indexFinger);
  //DEBUGPRINT(("0x%X tablet_indexFinger, 0x%X word_code\n",
  // *tablet_indexFinger, word_code));
  //tablet_print(sequence_long, tablet);
  if (word_code == 0)
    return 0;
  //repeat(text_long, text[iterator] = 0);
  uint16_t word_long = 0;
  // tablet_print(sequence_long, tablet);
  if ((word_code & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) {
    word_long = quote_phrase_translate(tablet_indexFinger, sequence_long,
                                       tablet, text_long, text);
    // DEBUGPRINT(("%X word_long, %s text\n", word_long, text));
  } else {
    word_long = word_code_translate(word_code, text_long, text);
    conditional(
        sort == fluent_WORD &&
            (is_grammatical_case_word(word_code) == truth_WORD||
             is_perspective_word(word_code) == truth_WORD),
        word_long += constant_text_copy(" ", text_long, text + word_long));
    // DEBUGPRINT(("%X word_long, %s text\n", word_long, text));
  }
  // DEBUGPRINT(("0x%X word_long, %s text\n", word_long, text));
  return word_long;
}

struct Phrase derive_paragraph_quote_phrase(struct Phrase input_phrase) {
  word_t quote = phrase_word_read(input_phrase, 0); 
  guarantee(quote == PARAGRAPH_LETTER_QUOTE);
  word_t quote_length = phrase_word_read(input_phrase, 1);
  DEBUGPRINT(("%X quote_length\n", quote_length));
  struct Phrase produce = input_phrase;
  produce.begin =  round_theIndexFinger_toRoofLine(input_phrase.begin);
  produce.length = quote_length;
  
  return produce;
}
struct Text paragraph_letter_quote_translate(struct Phrase quote_phrase, 
    struct Text produce_text) {
  guarantee(produce_text.max_length > 0);
  //DEBUGPRINT(("%X quote_phrase.length\n", quote_phrase.length));
  produce_text = number_to_text_translate(quote_phrase.length, 
      DEFAULT_NUMBER_BASE, produce_text);
  NewText(begin, "do lyatkyitksuh ");
  produce_text = text_addenda(begin, produce_text);
  // check if blank, if not then quote it
  return produce_text;
}

/* sorts accepted, brief, fluent (spaces after cases) and fancy (IPA) */
uint16_t tablet_translate(const uint8_t sequence_long, const line_t *tablet,
                          const uint16_t sort, const uint16_t text_long,
                          char *text) {
  guarantee(text != NULL);
  guarantee(*text == (char)0);
  // guarantee(text_long != NULL);
  guarantee(text_long >= LINE_LONG * WORD_LONG);
  uint16_t gross_text_long = 0;
  uint16_t vacancy_text_long = text_long;
  uint16_t maximum_tablet_long = LINE_LONG * sequence_long;
  uint16_t iterator = 1;
  uint16_t temp_text_long = 0;
  word_t upcoming_word = 0;
  repeat_with_until(
      iterator, maximum_tablet_long,
      // DEBUGPRINT(("%X iterator\n", iterator));
      // DEBUGPRINT(("0x%X gross_text_long, %s text\n", gross_text_long, text));
      uint16_t neo_iterator = 0;
      upcoming_word =  tablet_upcoming_grammar_or_quote_read(iterator, sequence_long,
          tablet, &neo_iterator);
      if (upcoming_word == 0){
        iterator |= LINE_LONG_MASK;
        continue;
      } 
      else if (upcoming_word == PARAGRAPH_LETTER_QUOTE) {
      DEBUGPRINT(("%s text\n", text));
      struct Page input_page;
      input_page.lines = tablet;
      input_page.plength = sequence_long;
      struct Phrase input;
      input.page = input_page;
      input.begin = iterator;
      input.length = sequence_long-iterator;
      struct Phrase paragraph_quote = derive_paragraph_quote_phrase(input);
      // translate the quote
      struct Text vacancy_text;
      DEBUGPRINT(("%s text\n", text));
      vacancy_text.letters  = text+gross_text_long;
      vacancy_text.length = vacancy_text_long;
      vacancy_text.max_length = vacancy_text.length;
      struct Text quote_text = paragraph_letter_quote_translate(paragraph_quote, 
          vacancy_text);
      temp_text_long = quote_text.length;
      // // adjust iterator to be after the quote 
       iterator = round_theIndexFinger_toRoofLine(paragraph_quote.length +
             paragraph_quote.begin);
      DEBUGPRINT(("%s text\n", text));
      } 
      if (upcoming_word != PARAGRAPH_LETTER_QUOTE) {
      temp_text_long =  phrase_translate(&iterator, sequence_long,
                                                tablet, sort, vacancy_text_long,
                                                text + gross_text_long);
      }
      // add to iterator if quote, for duration of quote
      //DEBUGPRINT(("%X temp_text_long, %X gross_text_long, %s text\n", 
      //     temp_text_long, gross_text_long, text));
      extradition_dose_from_to(temp_text_long,
                               vacancy_text_long, gross_text_long));
  //DEBUGPRINT(("0x%X gross_text_long, %s text\n", gross_text_long, text));
  return gross_text_long;
}



#if defined(OPENCL) == 0 && defined(EMSCRIPTEN) == 0
void dictionary_database_word_found(const uint32_t dictionary_long,
                                    const char *dictionary,
                                    const uint16_t code_word,
                                    uint8_t *word_long, char *word) {
  guarantee(dictionary_long > 0);
  guarantee(dictionary != NULL);
  guarantee(code_word != 0);
  guarantee(word_long != NULL);
  guarantee(*word_long >= MAXIMUM_FOREIGN_WORD_LONG);
  guarantee(word != NULL);
  uint32_t dictionary_indexFinger = 0;
  uint16_t dictionary_code_word = 0;
  uint8_t word_indexFinger = 0;
  uint8_t max_word_long = 0;
  uint8_t deviation = 0;
  char letter = (char)0;
  for (; dictionary_indexFinger < dictionary_long;
       dictionary_indexFinger += DICTIONARY_LINE_LONG) {
    // match to first uint16_t of dictionary line, if no match, skip to next
    // line.
    dictionary_code_word =
        (uint16_t)((uint8_t)dictionary[dictionary_indexFinger] |
                   (uint16_t)(dictionary[dictionary_indexFinger + 1] << 8));
    if (dictionary_code_word == code_word) {
      // printf("%s:%d code_word 0x%X\n ",__FILE__, __LINE__, code_word);
      // found the definition, return the foreign word
      // if grammar word, start with underscore, and set max to four letters
      if ((code_word & SHORT_GRAMMAR_DENOTE_MASK) == SHORT_GRAMMAR_DENOTE ||
          (code_word & LONG_GRAMMAR_DENOTE_MASK) == LONG_GRAMMAR_DENOTE) {
        word[0] = '_';
        max_word_long = 4;
        word_indexFinger = 1;
        deviation = 1;
      } else {
        max_word_long = MAXIMUM_FOREIGN_WORD_LONG;
        word_indexFinger = 0;
        deviation = 0;
      }

      for (; word_indexFinger < max_word_long; ++word_indexFinger) {
        // DEBUGPRINT("0x%X word_long, %s word\n", word_indexFinger, word);
        letter = dictionary[dictionary_indexFinger + CODE_LONG +
                            word_indexFinger - deviation];
        if (letter != 0 && letter != ' ' && letter != '\n') {
          word[word_indexFinger] = letter;
          // printf("%c", letter);
        } else {
          break;
        }
      }
      *word_long = word_indexFinger;
      break;
    }
  }
}
void code_file_translation(const uint32_t dictionary_long,
                           const char *dictionary, const uint32_t code_long,
                           const line_t *code, uint32_t *produce_pad_long,
                           char *produce_pad) {
  //  produce buffer evenly spaced, maximum foreign word length (30 bytes) times
  //  vector long (16), times number of vectors.
  //  use maximum that will fit in cache
  guarantee(produce_pad_long != NULL);
  guarantee(*produce_pad_long >=
            MAXIMUM_FOREIGN_WORD_LONG * LINE_LONG * code_long);
  guarantee(dictionary_long > 0);
  guarantee(dictionary != NULL);
  guarantee(code_long > 0);
  guarantee(code != NULL);
  guarantee(produce_pad != NULL);
  // parallel egg import
  // parallel egg fertilize
  // parallel seed birth
  // parallel children observation
  // parallel children discharge
  // language unique allotment _nom begin _rea
  //    each worker _nom single independentClause _acc processing _rea
  //    translate one phrase at a time
  uint8_t code_indexFinger = 0;
  uint8_t tablet_indexFinger = 0;
  uint8_t vector_long = 0;
  uint8_t scalar_thick = 0;
  uint16_t code_word = 0;
  // uint16_t quote_word = 0;
  uint16_t indicator_list = 0;
  uint8_t indicator = 0;
  line_t tablet = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  uint8_t quote_long = 0;
  uint8_t word_long = MAXIMUM_FOREIGN_WORD_LONG;
  uint32_t pad_indexFinger = 0;
  for (code_indexFinger = 0; code_indexFinger < code_long; ++code_indexFinger) {
    line_copy(code[code_indexFinger], tablet);
#ifdef ARRAYLINE
    indicator_list = (uint16_t)tablet[0];
#else
    indicator_list = (uint16_t)tablet.s0;
#endif
    indicator = indicator_list & 1;
    for (tablet_indexFinger = 1; tablet_indexFinger < LINE_LONG;
         ++tablet_indexFinger) {
      code_word = line_t_read(tablet_indexFinger, tablet);
      if (((indicator_list >> (tablet_indexFinger - 1)) & 1) == indicator &&
          (code_word & QUOTE_DENOTE_MASK) == QUOTE_DENOTE) {
        // is at a potential quote place
        // find quote length
        vector_long = (uint8_t)vector_long_translate(
            (code_word & VECTOR_THICK_MASK) >> VECTOR_THICK_BEGIN);
        scalar_thick = (code_word & SCALAR_THICK_MASK) >> SCALAR_THICK_BEGIN;
        // DEBUGPRINT("scalar_thick 0x%X, vector_long 0x%X\n", scalar_thick,
        // vector_long);
        quote_long = scalar_thick * vector_long;
        // translate quote
        // put indexFinger after the quote
        tablet_indexFinger += quote_long;
        //  DEBUGPRINT("check if quote_long exceeds end of tablet, code_long
        //  0x%X\n",
        //      code_long);
        ++code_indexFinger;
        code_word = line_t_read(tablet_indexFinger, tablet);
        pad_indexFinger +=
            (uint)form_print(*produce_pad_long - pad_indexFinger,
                             produce_pad + pad_indexFinger, "0x%X ", code_word);
        continue;
      }
      if (code_word == 0)
        break;
      // if (((indicator_list >> tablet_indexFinger) & 1) == indicator) {
      // is end of a phrase
      // translate it look it up in the dictionary  database
      // printf(
      //    "%s:%d code_word 0x%X\n",
      //    __FILE__, __LINE__, code_word);
      dictionary_database_word_found(dictionary_long, dictionary, code_word,
                                     &word_long, produce_pad + pad_indexFinger);
      // DEBUGPRINT("word_long 0x%X word %s\n", word_long,
      //       produce_pad + pad_indexFinger);
      // DEBUGPRINT("%s produce_pad\n", produce_pad);
      // translate words or quote in phrase
      //}

      //    each translation dictionary code _nom each code word _acc comparison
      //    _rea

      //      translated
      //        counter.
      //    if code word remains counter equals zero then skip to next word if
      //    available.
      //    if same then put translation into produce buffer
      pad_indexFinger += word_long;
      pad_indexFinger += (uint)form_print(*produce_pad_long - pad_indexFinger,
                                          produce_pad + pad_indexFinger, " ");
      word_long = MAXIMUM_FOREIGN_WORD_LONG;
    }
    // DEBUGPRINT("%d pad_indexFinger\n", pad_indexFinger);
  }
  // language unique allotment _nom done _rea
  //
  //
}

#endif
struct Text /* produce */ page_translate(const struct Page input, uint16_t sort,
                                         struct Text produce_pad) {
  produce_pad.length =
      tablet_translate(input.plength, input.lines, sort, produce_pad.max_length,
                       produce_pad.letters);
  return produce_pad;
}

struct Text /* translated text */
neo_phrase_translate(const struct Phrase phrase, struct Text text,
                     word_t sort) {
  // TODO fillin with call to tablet_phrase_translate
  NewPagePhrase(produce, 0x10);
  produce = phrase_addenda(phrase, produce);
  text = page_translate(produce.page, sort, text);
  return text;
}

uint8_t derive_scalar_thick_from_quote_code(word_t quote_code) {
  return (SCALAR_THICK_MASK & quote_code) >> SCALAR_THICK_BEGIN;
}
uint8_t derive_vector_thick_from_quote_code(word_t quote_code) {
  return (VECTOR_THICK_MASK & quote_code) >> VECTOR_THICK_BEGIN;
}
/** derive_quote_byte_length from quote word 
 * kyithkompwih kyitlyanka practu
 * **/
uint32_t kyitbrethkompwih_kyitlyanka_practu(struct Phrase quote_code_phrase) {
  guarantee(quote_code_phrase.length == 1);
  // get the number and vector numbers, and return length based on them
  word_t quote_code = (phrase_word_read(quote_code_phrase, 0));
  conditional(is_quote_code(quote_code) == lie_WORD, return 0);
  uint8_t scalar_thick = derive_scalar_thick_from_quote_code(quote_code);
  uint8_t vector_thick = derive_vector_thick_from_quote_code(quote_code);
  // check documentation for how to calculate it properly
  uint32_t quote_length = 1;
  switch(scalar_thick) {
    example(0, quote_length = 1);
    example(1, quote_length = 2);
    example(2, quote_length = 4);
    example(3, quote_length = 8);
    default: guarantee(1 == 0); // invalid quote scalar thick
  }
  switch(vector_thick) {
    example(0, quote_length *= 1);
    example(1, quote_length *= 2);
    example(2, quote_length *= 4);
    example(3, quote_length *= 8);
    example(4, quote_length *= 16);
    example(7, quote_length *= 3);
    default: guarantee(1 == 0); // invalid quote scalar thick
  }
  return quote_length; 
}

