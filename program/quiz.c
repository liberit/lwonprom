/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*
   Pyash is a  human computer programming langauge based on linguistic universals.
      Copyright (C) 2020  Andrii Logan Zvorygin

      This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

      You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/* file minunit_example.c */
#include "quiz.h"

#define TEXT_LONG 0x100

int quizs_run = 0;

#define p2p_quiz(name, input, output, error_text)                              \
  const char *name() {                                                         \
    DEBUGPRINT(("%s begin\n", ""));                                            \
    NewPagePhrase(program, 10);                                                \
    uint16_t output_long = (uint16_t)stringlen(output, 0x100);                 \
    NewText(input_text, input);                                                \
    NewTextPad(produce_text, 0x100);                                           \
    produce_text = neo_pyash_to_pyash_interpret(program, input_text,           \
        fluent_WORD, produce_text);    \
    DEBUGPRINT(("%s input, '%s' output_text, '%s' produce_text.letters, 0x%X " \
          "comparison\n",                                                \
          input, output, produce_text.letters,                           \
          strncmp(output, produce_text.letters, produce_text.length)));  \
    DEBUGPRINT(("0x%X output_long, 0x%X produce_text_long\n", output_long,     \
          produce_text.length));                                         \
    mu_assert(error_text,                                                      \
        output_long == produce_text.length &&                            \
        0 == strncmp(output, produce_text.letters, output_long));    \
    return 0;                                                                  \
  }

p2p_quiz(word_pyacli, "pyac li ", "pyacli ", "er, pyacli != pyacli ");
p2p_quiz(word_pyacli2, "pya c li ", "pyacli ", "er, pyacli != pyacli ");

p2p_quiz(number_zero, "zron do ka li ", "zrondoka li ", "er, 0 != 0")
p2p_quiz(number_zero_zero, "zron zrondo ka li ", "zrondoka li ",
    "er, 00 != 0");
p2p_quiz(number_one, "hyik do ka li ", "hyikdoka li ", "er, 1 != 1")
p2p_quiz(number_one_one, "hyik hyikdo ka li ", "hyikhyikdoka li ",
    "er, 11 != 11");
p2p_quiz(number_one_zero, "hyik zrondo ka li ", "hyikzrondoka li ",
    "er, 10 != 10");
p2p_quiz(number_one_two_zero, "hyik tyutzrondo ka li ", "hyiktyutzrondoka li ",
    "er, 120 != 120");
p2p_quiz(number_one_two_three_four, "hyik tyuttyin ksasdo ka li ",
    "hyiktyuttyinksasdoka li ", "er, 1234 != 1234");

p2p_quiz(math_one_plus_one, "hyikdoka hyikdoyu plustu", "tyutdoka li ",
    "er, 1+1 != 2");
p2p_quiz(math_two_plus_two, "tyutdoka tyutdoyu plustu", "ksasdoka li ",
    "er, 2+2 != 4");
p2p_quiz(math_one_eleven_fifteen_plus_two_three,
    "hyikslenhpetdoka tyuttyindoyu "
    "plustu",
    "hyikhsestyutdoka li ", "er, 0x1BF+0x23 != 0x1E2");

p2p_quiz(variable_declaration, "hnucgina hyikdoka htapli ",
    "hnucgina hyikdoka htapli ", "er, 1+1 != 2");
p2p_quiz(variable_what, "hnucgina hyikdoka hnucgika hnucgina hwatkari",
    "hnucgina hyikdoka li hnucgina hyikdoka li ", "");
p2p_quiz(variable_plus, "hnucgina hyikdoka li hnucgika hnucgiyi plustu ",
    "hnucgina tyutdoka li ", "");
p2p_quiz(variable_plus2, "hnucgina hyikdoka li tyindoka hnucgiyu plustu ",
    "hnucgina hyikdoka li ksasdoka li ", "");
p2p_quiz(variable_plus3, "hnucgina hyikdoka li tyindokali hnucgiyu plustu ",
    "hnucgina hyikdoka li ksasdoka li ", "");

p2p_quiz(simple_multisentence, "pyacli hfacli ", "pyacli hfacli ",
    "er, simple multisentence fail");
p2p_quiz(double_multisentence, "pyacli hfacli  ksasli", "pyacli hfacli ksasli ",
    "er, double multisentence fail");

const char *math_multisentence() {
  DEBUGPRINT(("%s begin\n", ""));
  NewPagePhrase(program, 10);
  char input[] = "hyikdoka hyikdoyu plustu tyutdoyu plustu ";
  char output[] = "ksasdoka li ";
  uint16_t output_long = (uint16_t)stringlen(output, 0x100);
  NewText(input_text, input);
  NewTextPad(produce_text, 0x100);
  produce_text = neo_pyash_to_pyash_interpret(program, input_text, fluent_WORD,
      produce_text);
  DEBUGPRINT(("%s input, '%s' output_text, '%s' produce_text.letters, 0x%X "
        "comparison\n",
        input, output, produce_text.letters,
        strncmp(output, produce_text.letters, produce_text.length)));
  DEBUGPRINT(("0x%X output_long, 0x%X produce_text_long\n", output_long,
        produce_text.length));
  mu_assert("", output_long == produce_text.length &&
      0 == strncmp(output, produce_text.letters, output_long));
  return 0;
}

p2p_quiz(knowledge_plus, "hyikdoka li hyikdoyu plustu", "tyutdoka li ", "");

const char *knowledge_plus_two() {
  DEBUGPRINT(("%s begin\n", ""));
  NewPagePhrase(program, 10);
  char input[] = "hyikdoka li hyikdoyu plustu tyindoyu plustu";
  char output[] = "hfakdoka li ";
  uint16_t output_long = (uint16_t)stringlen(output, 0x100);
  NewText(input_text, input);
  NewTextPad(produce_text, 0x100);
  produce_text = neo_pyash_to_pyash_interpret(program, input_text, fluent_WORD,
      produce_text);
  DEBUGPRINT(("%s input, '%s' output_text, '%s' produce_text.letters, 0x%X "
        "comparison\n",
        input, output, produce_text.letters,
        strncmp(output, produce_text.letters, produce_text.length)));
  DEBUGPRINT(("0x%X output_long, 0x%X produce_text_long\n", output_long,
        produce_text.length));
  mu_assert("", output_long == produce_text.length &&
      0 == strncmp(output, produce_text.letters, output_long));
  return 0;
}


const char *quiz_delete_aGrammarWord_atThePlace_inThePhrase_1() {
  NewTextPhrase(input, 1, "pyacli");
  mu_assert("", input.length == 2);
  struct Phrase produce = delete_aGrammarWord_atThePlace_inThePhrase(1,
      input);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 1);
  mu_assert("", phrase_letters_compare(produce, "pyac") == truth_WORD);
  mu_assert("", tablet_read(0, input.page.plength, input.page.lines) == 1);
  return 0;
}
const char *quiz_delete_aGrammarWord_atThePlace_inThePhrase_2() {
  NewTextPhrase(input, 2, "pyacli pyacli");
  struct Phrase produce = delete_aGrammarWord_atThePlace_inThePhrase(0x11,
      input);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 0x11);
  mu_assert("", phrase_letters_compare(produce, "pyacli pyac") == truth_WORD);
  mu_assert("", tablet_read(0x10, input.page.plength, input.page.lines) == 1);
  produce = delete_aGrammarWord_atThePlace_inThePhrase(produce.length -1,
      produce);
  mu_assert("", produce.length == 0x10);
  mu_assert("", phrase_letters_compare(produce, "pyacli ") == truth_WORD);
  mu_assert("", tablet_read(0x10, input.page.plength, input.page.lines) == 1);
  return 0;
}
const char *quiz_delete_aGrammarWord_atThePlace_inThePhrase() {
  mu_run_quiz(quiz_delete_aGrammarWord_atThePlace_inThePhrase_1);
  mu_run_quiz(quiz_delete_aGrammarWord_atThePlace_inThePhrase_2);
  return 0;
}

const char *quiz_word() {
  mu_run_quiz(word_pyacli);
  mu_run_quiz(word_pyacli2);
  mu_run_quiz(quiz_delete_aGrammarWord_atThePlace_inThePhrase);
  return 0;
}
const char *number_hollow() {
  NewTextPage(fyak, 1, "doka");
  Page_print(fyak);
  line_t perfect_tablet[1] = {{0x9, 0x881D, 0x0, 0x245E}};
  mu_assert("", tablet_compare(fyak.plength, fyak.lines, 1, perfect_tablet) ==
      truth_WORD);
  return 0;
}

const char *quiz_number() {
  mu_run_quiz(number_one);
  mu_run_quiz(number_zero);
  mu_run_quiz(number_zero_zero);
  mu_run_quiz(number_one_one);
  mu_run_quiz(number_one_zero);
  mu_run_quiz(number_one_two_zero);
  mu_run_quiz(number_one_two_three_four);
  mu_run_quiz(number_hollow);
  return 0;
}
const char *quiz_math() {
  mu_run_quiz(math_one_plus_one);
  mu_run_quiz(math_one_eleven_fifteen_plus_two_three);
  mu_run_quiz(math_two_plus_two);
  return 0;
}
const char *quiz_knowledge() {
  mu_run_quiz(knowledge_plus);
  mu_run_quiz(knowledge_plus_two);
  // mu_run_quiz(knowledge_delete);
  // mu_run_quiz(knowledge_view);
  return 0;
}
const char *quiz_multisentence() {
  mu_run_quiz(simple_multisentence);
  mu_run_quiz(double_multisentence);
  mu_run_quiz(math_multisentence);
  return 0;
}

const char *quiz_retrospective_phrase_cousin_found() {

  DEBUGPRINT(("%s begin\n", ""));
  uint16_t text_long = TEXT_LONG;
  char text[TEXT_LONG] = "hnucgina li";
  uint16_t comparison_text_long = TEXT_LONG;
  char comparison_text[TEXT_LONG] = "hnucgina hyikdoka li";
  uint16_t program_attribute[SEQUENCE_ATTRIBUTE_LONG] = {0};
  program_attribute[Sequence_max_long] = 0x10;

  uint16_t tablet_long = LINE_LONG;
  line_t tablet[LINE_LONG] = {0};
  uint16_t comparison_tablet_long = LINE_LONG;
  line_t comparison_tablet[LINE_LONG] = {0};
  uint16_t text_remainder = 0;

  tablet_long =
    old_text_encoding(text_long, text, tablet_long, tablet, &text_remainder);
  comparison_tablet_long = old_text_encoding(
      comparison_text_long, comparison_text, comparison_tablet_long,
      comparison_tablet, &text_remainder);

  uint16_t phrase_long = 3;
  uint16_t phrase_begin = 1;
  uint16_t cousin_begin;
  uint16_t cousin_long = retrospective_phrase_cousin_found(
      tablet_long, tablet, phrase_long, phrase_begin, comparison_tablet_long,
      comparison_tablet, &cousin_begin);

  tablet_print(tablet_long, tablet);
  tablet_print(comparison_tablet_long, comparison_tablet);
  DEBUGPRINT(
      ("0x%x cousin_long, 0x%X cousin_begin\n", cousin_long, cousin_begin));
  mu_assert("", cousin_begin == 1 && cousin_long == 3);
  return 0;
}
const char *quiz_htin_subset_comparison_same() {
  NewTextPage(example, 1, "hnucgina li");
  mu_assert("", neo_htin_subset_comparison(example, example) == truth_WORD);
  return 0;
}
const char *quiz_htin_subset_comparison_1() {
  NewTextPage(example, 1, "hnucgina li");
  NewTextPage(example2, 1, "hnucgina li");
  mu_assert("", neo_htin_subset_comparison(example, example2) == truth_WORD);
  return 0;
}
const char *quiz_htin_subset_comparison_2() {
  NewTextPage(example, 1, "hnucgina li");
  NewTextPage(example2, 1, "hnucgina hyikdokali li");
  mu_assert("", neo_htin_subset_comparison(example, example2) == truth_WORD);
  return 0;
}

const char *quiz_htin_subset_comparison_legacy() {
  DEBUGPRINT(("%s begin\n", ""));
  uint16_t text_long = TEXT_LONG;
  char text[TEXT_LONG] = "hnucgina li";
  uint16_t comparison_text_long = TEXT_LONG;
  char comparison_text[TEXT_LONG] = "hnucgina hyikdoka li";
  uint16_t program_attribute[SEQUENCE_ATTRIBUTE_LONG] = {0};
  program_attribute[Sequence_max_long] = 0x10;

  uint16_t tablet_long = LINE_LONG;
  line_t tablet[LINE_LONG] = {0};
  uint16_t comparison_tablet_long = LINE_LONG;
  line_t comparison_tablet[LINE_LONG] = {0};
  uint16_t text_remainder = 0;

  tablet_long =
    old_text_encoding(text_long, text, tablet_long, tablet, &text_remainder);
  comparison_tablet_long = old_text_encoding(
      comparison_text_long, comparison_text, comparison_tablet_long,
      comparison_tablet, &text_remainder);

  uint16_t cousin_result = htin_subset_comparison(
      tablet_long, tablet, comparison_tablet_long, comparison_tablet);

  tablet_print(tablet_long, tablet);
  tablet_print(comparison_tablet_long, comparison_tablet);
  DEBUGPRINT(("0x%X cousin_result\n", cousin_result));
  mu_assert("", cousin_result == truth_WORD);
  return 0;
}

const char *quiz_htin_subset_comparison() {
  mu_run_quiz(quiz_htin_subset_comparison_same);
  mu_run_quiz(quiz_htin_subset_comparison_1);
  mu_run_quiz(quiz_htin_subset_comparison_2);
  mu_run_quiz(quiz_htin_subset_comparison_legacy);
  return 0;
}

const char *quiz_retrospective_htin_cousin_found_basic() {

  DEBUGPRINT(("%s begin\n", ""));
  uint16_t text_long = TEXT_LONG;
  char text[TEXT_LONG] = "hnucgina li";
  uint16_t comparison_text_long = TEXT_LONG;
  char comparison_text[TEXT_LONG] = "pyacli hnucgina hyikdoka li blapli";
  uint16_t program_attribute[SEQUENCE_ATTRIBUTE_LONG] = {0};
  program_attribute[Sequence_max_long] = 0x10;

  uint16_t tablet_long = LINE_LONG;
  line_t tablet[LINE_LONG] = {0};
  uint16_t comparison_tablet_long = LINE_LONG;
  line_t comparison_tablet[LINE_LONG] = {0};
  uint16_t text_remainder = 0;

  tablet_long =
    old_text_encoding(text_long, text, tablet_long, tablet, &text_remainder);
  comparison_tablet_long = old_text_encoding(
      comparison_text_long, comparison_text, comparison_tablet_long,
      comparison_tablet, &text_remainder);

  DEBUGPRINT(("0x%X tablet_long\n", tablet_long));
  tablet_print(tablet_long, tablet);
  DEBUGPRINT(("0x%X comparison_tablet_long\n", comparison_tablet_long));
  tablet_print(comparison_tablet_long, comparison_tablet);
  uint16_t cousin_begin = 0;

  uint16_t cousin_long = retrospective_htin_cousin_found(
      tablet_long, tablet, comparison_tablet_long, comparison_tablet,
      &cousin_begin);

  DEBUGPRINT(
      ("0x%X cousin_begin, 0x%X cousin_long\n", cousin_begin, cousin_long));
  mu_assert("", cousin_begin == 1 && cousin_long == 1);
  return 0;
}

const char *quiz_retrospective_htin_cousin_found_hollow() {
  NewTextPhrase(knowledge, 3, "pyacli hnucgina hyikdoka li blapli");
  NewTextPhrase(question, 1, "hnupgina li");
  struct Phrase cousin =
    neo_retrospective_htin_cousin_found(question, knowledge);
  DEBUGPRINT(
      ("%X cousin.length, %X cousin.begin\n", cousin.length, cousin.begin));
  mu_assert("", cousin.begin == 0 && cousin.length == 0);
  return 0;
}
const char *quiz_retrospective_htin_cousin_found() {
  // mu_run_quiz(quiz_retrospective_htin_cousin_found_basic);
  mu_run_quiz(quiz_retrospective_htin_cousin_found_hollow);
  return 0;
}

const char *quiz_upcoming_variable_to_worth_translate1() {

  DEBUGPRINT(("%s begin\n", ""));
  uint16_t text_long = TEXT_LONG;
  char text[TEXT_LONG] = "hnucgipwih hyikdoyu li";
  uint16_t comparison_text_long = TEXT_LONG;
  char comparison_text[TEXT_LONG] = "pyacli hnucgina tyutdoka li blapli";
  char output_text[TEXT_LONG] = "tyutdopwih ";
  uint16_t program_attribute[SEQUENCE_ATTRIBUTE_LONG] = {0};
  program_attribute[Sequence_max_long] = 0x10;

  uint16_t tablet_long = LINE_LONG;
  line_t tablet[LINE_LONG] = {0};
  uint16_t comparison_tablet_long = LINE_LONG;
  line_t comparison_tablet[LINE_LONG] = {0};
  uint16_t produce_tablet_long = LINE_LONG;
  line_t produce_tablet[LINE_LONG] = {0};
  uint16_t text_remainder = 0;
  uint16_t output_tablet_long = LINE_LONG;
  line_t output_tablet[LINE_LONG] = {0};

  // test case 1, a name with a definition
  tablet_long =
    old_text_encoding(text_long, text, tablet_long, tablet, &text_remainder);
  comparison_tablet_long = old_text_encoding(
      comparison_text_long, comparison_text, comparison_tablet_long,
      comparison_tablet, &text_remainder);
  output_tablet_long =
    old_text_encoding(TEXT_LONG, output_text, output_tablet_long,
        output_tablet, &text_remainder);

  DEBUGPRINT(("0x%X tablet_long\n", tablet_long));
  tablet_print(tablet_long, tablet);
  DEBUGPRINT(("0x%X comparison_tablet_long\n", comparison_tablet_long));
  tablet_print(comparison_tablet_long, comparison_tablet);
  uint16_t produce_indexFinger = 0;
  uint16_t tablet_indexFinger = 1;

  produce_indexFinger = upcoming_variable_to_worth_translate(
      comparison_tablet_long, comparison_tablet, tablet_long, tablet,
      &tablet_indexFinger, produce_indexFinger, produce_tablet_long,
      produce_tablet);

  DEBUGPRINT(("0x%X produce_tablet_long, 0x%X cousin_indexFinger\n",
        produce_indexFinger, produce_indexFinger));
  tablet_print(produce_tablet_long, produce_tablet);
  tablet_print(produce_tablet_long, output_tablet);
  mu_assert("",
      produce_indexFinger > 0 &&
      line_compare(*produce_tablet, *output_tablet) == truth_WORD);
  return 0;
}

const char *quiz_upcoming_variable_to_worth_translate2() {
  DEBUGPRINT(("%s begin undeclared name keep \n", ""));
  NewTextPhrase(input, 1, "hnucgipwih hyikdoyu li");
  NewTextPhrase(knowledge, 3, "pyacli hnupgina tyutdoka li blapli");
  NewPagePhrase(produce, 1);
  produce.length = 0;

  struct Phrase fresh_produce =
    neo_upcoming_variable_to_worth_translate(knowledge, input, produce);

  DEBUGPRINT(("%X input.begin, %X input.length\n", input.begin, input.length));
  phrase_print(input);
  DEBUGPRINT(("%X fresh_produce.length\n", fresh_produce.length));
  Page_print(fresh_produce.page);
  phrase_print(fresh_produce);
  text_phrase_print(fresh_produce);
  mu_assert("", fresh_produce.length > produce.length);
  mu_assert("", fresh_produce.length == 3);
  mu_assert("", fresh_produce.begin == 1);
  mu_assert("",
      phrase_letters_compare(fresh_produce, "hnucgipwih") == truth_WORD);
  return 0;
}

const char *quiz_upcoming_variable_to_worth_translate3() {
  DEBUGPRINT(("%s begin hollow verb case\n", ""));
  char output_text3[TEXT_LONG] = "li ";
  uint16_t program_attribute[SEQUENCE_ATTRIBUTE_LONG] = {0};
  program_attribute[Sequence_max_long] = 0x10;
  uint16_t produce_tablet_long = LINE_LONG;
  line_t produce_tablet[LINE_LONG] = {0};
  uint16_t text_remainder = 0;
  uint16_t output_tablet_long = LINE_LONG;
  line_t output_tablet[LINE_LONG] = {0};
  uint16_t produce_indexFinger = 0;
  uint16_t tablet_indexFinger = 1;
  // test case 3 hollow verb phrase
  tablet_zero(produce_tablet_long, produce_tablet);
  tablet_zero(output_tablet_long, output_tablet);
  output_tablet_long =
    old_text_encoding(TEXT_LONG, output_text3, output_tablet_long,
        output_tablet, &text_remainder);
  tablet_indexFinger = 1;
  produce_indexFinger = 0;
  produce_indexFinger = upcoming_variable_to_worth_translate(
      output_tablet_long, output_tablet, output_tablet_long, output_tablet,
      &tablet_indexFinger, produce_indexFinger, produce_tablet_long,
      produce_tablet);

  DEBUGPRINT(("0x%X produce_tablet_long, 0x%X cousin_indexFinger\n",
        produce_indexFinger, produce_indexFinger));
  tablet_print(produce_tablet_long, produce_tablet);
  tablet_print(produce_tablet_long, output_tablet);
  mu_assert("",
      produce_indexFinger > 0 &&
      line_compare(*produce_tablet, *output_tablet) == truth_WORD);
  return 0;
}

const char *quiz_upcoming_variable_to_worth_translate4() {
  DEBUGPRINT(("%s begin\n", ""));
  uint16_t program_attribute[SEQUENCE_ATTRIBUTE_LONG] = {0};
  program_attribute[Sequence_max_long] = 0x10;

  uint16_t comparison_tablet_long = LINE_LONG;
  line_t comparison_tablet[LINE_LONG] = {0};
  uint16_t produce_tablet_long = LINE_LONG;
  line_t produce_tablet[LINE_LONG] = {0};
  uint16_t text_remainder = 0;
  uint16_t output_tablet_long = LINE_LONG;
  line_t output_tablet[LINE_LONG] = {0};
  uint16_t produce_indexFinger = 0;
  uint16_t tablet_indexFinger = 1;
  // test case 4: don't convert if dative case, because is output variable.
  char output_text4[TEXT_LONG] = "hnucgiyi ";
  tablet_zero(produce_tablet_long, produce_tablet);
  tablet_zero(output_tablet_long, output_tablet);
  output_tablet_long =
    old_text_encoding(TEXT_LONG, output_text4, output_tablet_long,
        output_tablet, &text_remainder);
  tablet_indexFinger = 1;
  produce_indexFinger = 0;
  produce_indexFinger = upcoming_variable_to_worth_translate(
      comparison_tablet_long, comparison_tablet, output_tablet_long,
      output_tablet, &tablet_indexFinger, produce_indexFinger,
      produce_tablet_long, produce_tablet);

  DEBUGPRINT(("0x%X produce_tablet_long, 0x%X cousin_indexFinger\n",
        produce_indexFinger, produce_indexFinger));
  tablet_print(produce_tablet_long, produce_tablet);
  tablet_print(produce_tablet_long, output_tablet);
  mu_assert("",
      produce_indexFinger > 0 &&
      line_compare(*produce_tablet, *output_tablet) == truth_WORD);
  return 0;
}
const char *quiz_upcoming_variable_to_worth_translate5() {
  NewTextPhrase(knowledge, 5, "nrupgina hyikdokali");
  NewTextPhrase(input, 5, "nrupgiyu");
  NewPagePhrase(produce, 2);
  produce = neo_upcoming_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdoyu") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_variable_to_worth_translate6() {
  NewTextPhrase(knowledge, 5, "nrupgina hyikdokali tyindokali");
  NewTextPhrase(input, 5, "nrupgiyu");
  NewPagePhrase(produce, 2);
  produce = neo_upcoming_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdoyu") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_variable_to_worth_translate7() {
  NewTextPhrase(knowledge, 5, "ksasdokali nrupgina tyutdokali tyindokali");
  NewTextPhrase(input, 5, "nrupgiyu");
  NewPagePhrase(produce, 2);
  produce = neo_upcoming_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdoyu") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_variable_to_worth_translate_letter() {
  NewTextPhrase(knowledge, 5, "nrupgina zi.prih.c.prih.zikali tyindokali");
  NewTextPhrase(input, 5, "nrupgiyu");
  NewPagePhrase(produce, 3);
  produce = neo_upcoming_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "zi.prih.c.prih.ziyu") ==
      truth_WORD);
  return 0;
}
const char *quiz_upcoming_variable_to_worth_translate_letter_2() {
  NewTextPhrase(knowledge, 5, "nrupgina zi.prih.c.prih.zikali tyindokali");
  NewTextPhrase(input, 5, "nrupgika");
  NewPagePhrase(produce, 2);
  produce = neo_upcoming_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "zi.prih.c.prih.zika") ==
      truth_WORD);
  return 0;
}
const char *quiz_upcoming_variable_to_worth_translate_boundary() {
  NewTextPhrase(knowledge, 1, "lyatgina zi.prih.c.prih.zikali");
  NewTextPhrase(input, 2,
      "tyutdopwih lyatgika djancu"
      "tyutdoyu psasgiyi lyatgika grettu");
  NewPagePhrase(produce, 2);
  input.begin = 0xF;
  input.length -= 0xE;
  text_phrase_print(input);
  Page_print(input.page);
  produce = neo_upcoming_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "zi.prih.c.prih.zika") ==
      truth_WORD);
  return 0;
}

const char *quiz_upcoming_variable_to_worth_translate() {
  // mu_run_quiz(quiz_upcoming_variable_to_worth_translate1);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate2);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate3);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate4);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate5);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate6);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate7);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate_letter);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate_letter_2);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate_boundary);
  return 0;
}

const char *quiz_all_variable_to_worth_translate_null() {
  NewTextPhrase(knowledge, 1, "");
  NewTextPhrase(input, 1, "");
  NewTextPhrase(produce, 1, "");
  produce = neo_all_variable_to_worth_translate(knowledge, input, produce);
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_all_variable_to_worth_translate_null2() {
  NewTextPhrase(knowledge, 1, "");
  NewTextPhrase(input, 1, "hyikdokali");
  NewTextPhrase(produce, 1, "");
  produce = neo_all_variable_to_worth_translate(knowledge, input, produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_all_variable_to_worth_translate_letter() {
  NewTextPhrase(knowledge, 2, "hnucgina zi.prih.c.prih.zikali");
  NewTextPhrase(input, 2, "hnucgiyuli");
  NewTextPhrase(produce, 3, "");
  text_phrase_print(knowledge);
  produce = neo_all_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "zi.prih.c.prih.ziyuli") ==
      truth_WORD);
  return 0;
}
const char *quiz_all_variable_to_worth_translate_letter_grettu() {
  NewTextPhrase(knowledge, 1, "lyatgina zi.prih.c.prih.zikali");
  NewTextPhrase(input, 1, "tyutdoyu psasgiyi lyatgika grettu");
  NewTextPhrase(produce, 2, "");
  text_phrase_print(knowledge);
  produce = neo_all_variable_to_worth_translate(knowledge, input, produce);
  DEBUGPRINT(("%X produce.length, %X produce.begin\n", produce.length, 
        produce.begin));
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdoyu psasgiyi"
        "zi.prih.c.prih.zika grettu") ==
      truth_WORD);
  return 0;
}
const char *quiz_all_variable_to_worth_translate_conditional() {
  NewTextPhrase(knowledge, 1, "lyatgina zi.prih.c.prih.zikali");
  NewTextPhrase(input, 1, "djancu tyutdoyu psasgiyi lyatgika grettu");
  NewTextPhrase(produce, 1, "");
  text_phrase_print(knowledge);
  produce = neo_all_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "djancu tyutdoyu psasgiyi"
        "zi.prih.c.prih.zika grettu") ==
      truth_WORD);
  return 0;
}
const char *quiz_all_variable_to_worth_translate_conditional2() {
  NewTextPhrase(knowledge, 1, "lyatgina zi.prih.c.prih.zikali");
  NewTextPhrase(input, 2,
      "tyutdopwih lyatgika djancu"
      "tyutdoyu psasgiyi lyatgika grettu");
  NewTextPhrase(produce, 2, "");
  text_phrase_print(knowledge);
  produce = neo_all_variable_to_worth_translate(knowledge, input, produce);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(
        produce,
        "tyutdopwih zi.prih.c.prih.zika djancu tyutdoyu psasgiyi"
        "zi.prih.c.prih.zika grettu") == truth_WORD);
  return 0;
}
const char *quiz_all_variable_to_worth_translate() {
  mu_run_quiz(quiz_all_variable_to_worth_translate_null);
  mu_run_quiz(quiz_all_variable_to_worth_translate_null2);
  mu_run_quiz(quiz_all_variable_to_worth_translate_letter);
  mu_run_quiz(quiz_all_variable_to_worth_translate_letter_grettu);
  mu_run_quiz(quiz_all_variable_to_worth_translate_conditional);
  mu_run_quiz(quiz_all_variable_to_worth_translate_conditional2);
  return 0;
}

const char *quiz_tablet_retrospective_word_read() {
  uint16_t txik = 1;
  uint8_t tablet_long = LINE_LONG;
  line_t tablet[LINE_LONG] = {{1, 0xD909}, {1, 0xB888}};
  uint16_t word = tablet_retrospective_word_read(0, tablet_long, tablet, &txik);
  mu_assert("", word == 0 && txik == 0);
  word = tablet_retrospective_word_read(1, tablet_long, tablet, &txik);
  mu_assert("", word == 0xD909 && txik == 1);
  word = tablet_retrospective_word_read(0x10, tablet_long, tablet, &txik);
  mu_assert("", word == 0 && txik == 0xF);
  return 0;
}

const char *quiz_retrospective_htin_long_found_real() {
  NewTextPage(knowledge, HTIN_LONG,
      "hnucgidoyi tyutplos ksuh\n"
      "hnucgiyi tyutdoka plustufe\n"
      "tcangina hyikdoka li");
  struct Page page = retrospective_htin_long_found(knowledge);
  mu_assert("",
      tablet_letters_compare(page, "tcangina hyikdoka li") == truth_WORD);
  return 0;
}

const char *quiz_retrospective_htin_long_found_deo() {
  NewTextPage(knowledge, HTIN_LONG,
      "hnucgidoyi li\n"
      "hnucgidoyi tyutplos ksuh\n"
      "hnucgiyi tyutdoka plustu");
  struct Page page = retrospective_htin_long_found(knowledge);
  mu_assert("", tablet_letters_compare(page, "hnucgiyi tyutdoka plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_retrospective_htin_long_found_dec() {
  NewTextPage(knowledge, HTIN_LONG, "hnucgidoyi tyutplus ksuh\n");
  struct Page page = retrospective_htin_long_found(knowledge);
  mu_assert("", tablet_letters_compare(page, "hnucgidoyi tyutplusksuh") ==
      truth_WORD);
  return 0;
}

const char *quiz_retrospective_htin_long_found() {
  mu_run_quiz(quiz_retrospective_htin_long_found_real);
  mu_run_quiz(quiz_retrospective_htin_long_found_deo);
  mu_run_quiz(quiz_retrospective_htin_long_found_dec);
  return 0;
}

const char *quiz_htin_long_found_real() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "tcangina hyikdoka li"
      "hnucgidoyi tyutplos ksuh\n"
      "hnucgiyi tyutdoka plustufi\n");
  struct Phrase page = htin_long_found(knowledge);
  mu_assert("",
      phrase_letters_compare(page, "tcangina hyikdoka li") == truth_WORD);
  return 0;
}

const char *quiz_htin_long_found_deo() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "hnucgiyi tyutdoka plustu"
      "hnucgidoyi li\n"
      "hnucgidoyi tyutplos ksuh\n");
  struct Phrase page = htin_long_found(knowledge);
  mu_assert("", phrase_letters_compare(page, "hnucgiyi tyutdoka plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_htin_long_found_dec() {
  NewTextPhrase(knowledge, HTIN_LONG, "hnucgidoyi tyutplus ksuh\n");
  struct Phrase page = htin_long_found(knowledge);
  mu_assert("", phrase_letters_compare(page, "hnucgidoyi tyutplusksuh") ==
      truth_WORD);
  return 0;
}

const char *quiz_htin_long_found() {
  mu_run_quiz(quiz_htin_long_found_real);
  mu_run_quiz(quiz_htin_long_found_deo);
  mu_run_quiz(quiz_htin_long_found_dec);
  return 0;
}

const char *quiz_final_perspective_found_real() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus li\n");
  struct Phrase perspective = neo_final_perspective_found(page);
  DEBUGPRINT(("0x%X perspective.begin, 0x%X perspective.length\n",
        perspective.begin, perspective.length));
  phrase_print(perspective);
  mu_assert("", phrase_letters_compare(perspective, "li") == truth_WORD);
  return 0;
}
const char *quiz_final_perspective_found_deo() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus tu\n");
  struct Phrase perspective = neo_final_perspective_found(page);
  mu_assert("", phrase_letters_compare(perspective, "tu") == truth_WORD);
  return 0;
}
const char *quiz_final_perspective_found_dec() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus ksuh\n");
  struct Phrase perspective = neo_final_perspective_found(page);
  mu_assert("", phrase_letters_compare(perspective, "ksuh") == truth_WORD);
  return 0;
}
const char *quiz_final_perspective_found_fin() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus tufe\n");
  struct Phrase perspective = neo_final_perspective_found(page);
  mu_assert("", phrase_letters_compare(perspective, "fe") == truth_WORD);
  return 0;
}
const char *quiz_final_perspective_found() {
  mu_run_quiz(quiz_final_perspective_found_real);
  mu_run_quiz(quiz_final_perspective_found_deo);
  mu_run_quiz(quiz_final_perspective_found_dec);
  mu_run_quiz(quiz_final_perspective_found_fin);
  return 0;
}

const char *quiz_upcoming_perspective_found_real() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus li blahtu\n");
  struct Phrase perspective = upcoming_perspective_found(page);
  DEBUGPRINT(("0x%X perspective.begin, 0x%X perspective.length\n",
        perspective.begin, perspective.length));
  phrase_print(perspective);
  mu_assert("", phrase_letters_compare(perspective, "li") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_perspective_found_deo() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus tu blahli\n");
  struct Phrase perspective = upcoming_perspective_found(page);
  mu_assert("", phrase_letters_compare(perspective, "tu") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_perspective_found_dec() {
  NewTextPhrase(page, HTIN_LONG, "hnucgidoyi tyutplus ksuh \n");
  struct Phrase perspective = upcoming_perspective_found(page);
  mu_assert("", phrase_letters_compare(perspective, "ksuh") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_perspective_found_fin() {
  NewTextPhrase(input, HTIN_LONG, "hnucgidoyi tyutplus tufe\n");
  input.begin += 0xF;
  input.length -= 0xF;
  struct Phrase perspective = upcoming_perspective_found(input);
  mu_assert("", phrase_letters_compare(perspective, "fe") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_perspective_found_blah() {
  NewTextPhrase(input, HTIN_LONG,
      "tcangina hyikdoka li"
      "hnucgidoyi tyutplos ksuh\n"
      "hnucgiyi tyutdoka plustufi\n");
  struct Phrase perspective = upcoming_perspective_found(input);
  mu_assert("", phrase_letters_compare(perspective, "li") == truth_WORD);
  return 0;
}

const char *quiz_upcoming_perspective_found_blah2() {
  NewTextPhrase(input, 2, "cali sana hwat clacri");
  input.begin += 0x10;
  input.length -= 0x10;
  struct Phrase perspective = upcoming_perspective_found(input);
  mu_assert("", phrase_letters_compare(perspective, "ri") == truth_WORD);
  return 0;
}

const char *quiz_upcoming_perspective_found() {
  mu_run_quiz(quiz_upcoming_perspective_found_real);
  mu_run_quiz(quiz_upcoming_perspective_found_deo);
  mu_run_quiz(quiz_upcoming_perspective_found_dec);
  mu_run_quiz(quiz_upcoming_perspective_found_fin);
  mu_run_quiz(quiz_upcoming_perspective_found_blah);
  mu_run_quiz(quiz_upcoming_perspective_found_blah2);
  return 0;
}

const char *quiz_extract_words_ins_htinka_addenda1() {
  uint16_t input_long = 2;
  uint16_t input[2][16] = {0x9, 0x881d, 0x001, 0x24DE};
  uint16_t produce_long = 2;
  uint16_t produce[2][16] = {{0}};
  extract_words_ins_htinka_addenda(input_long, input, 3, 1, produce_long,
      produce);
  tablet_print(input_long, input);
  tablet_print(produce_long, produce);
  // DEBUGPRINT(("0x%X tablet_compare(input, produce)",
  //            tablet_compare(input_long, input, input_long, produce)));
  mu_assert("", tablet_compare(input_long, input, input_long, produce) ==
      truth_WORD);
  return 0;
}

const char *quiz_extract_words_ins_htinka_addenda2() {
  uint16_t input_long = 2;
  uint16_t input[2][16] = {0x9, 0x881d, 0x001, 0x24DE};
  uint16_t output[2][16] = {0x49, 0x881d, 0x001, 0x24DE, 0x881d, 0x001, 0x24DE};
  uint16_t produce_long = 2;
  uint16_t produce[2][16] = {{0}};
  extract_words_ins_htinka_addenda(input_long, input, 3, 1, produce_long,
      produce);
  tablet_print(input_long, input);
  tablet_print(produce_long, produce);
  extract_words_ins_htinka_addenda(input_long, input, 3, 1, produce_long,
      produce);
  tablet_print(produce_long, produce);
  mu_assert("", tablet_compare(input_long, output, input_long, produce) ==
      truth_WORD);
  return 0;
}
const char *quiz_extract_words_ins_htinka_addenda3() {

  uint16_t input_long = 2;
  line_t input[2] = {{0xB6B6, 0x881D, 0x0001, 0x4127, 0x881D, 0x0003, 0x245E,
    0x69B3, 0x29BE, 0x881D, 0x0001, 0x245E, 0x881D, 0x0003,
    0x287E, 0x8A64},
         {3, 0x295E}};
  uint16_t output[2][16] = {0x5, 0x8A64, 0x295E};
  uint16_t produce_long = 2;
  uint16_t produce[2][16] = {{0}};
  extract_words_ins_htinka_addenda(input_long, input, 3, 0xF, produce_long,
      produce);
  tablet_print(produce_long, produce);
  mu_assert("", tablet_compare(input_long, output, input_long, produce) ==
      truth_WORD);
  return 0;
}

const char *quiz_extract_words_ins_htinka_addenda() {
  mu_run_quiz(quiz_extract_words_ins_htinka_addenda1);
  mu_run_quiz(quiz_extract_words_ins_htinka_addenda2);
  mu_run_quiz(quiz_extract_words_ins_htinka_addenda3);
  return 0;
}

const char *quiz_line_compare() {
  uint16_t result = line_compare((line_t){0, 1}, (line_t){0, 1});
  mu_assert("", result == truth_WORD);
  result = line_compare((line_t){0, 1}, (line_t){0, 2});
  mu_assert("", result == lie_WORD);
  return 0;
}

const char *quiz_tablet_compare() {
  uint16_t tablet[2][16] = {{0}, {0}};
  uint16_t comparison[2][16] = {{0}, {2}};
  uint16_t result = tablet_compare(2, tablet, 2, tablet);
  mu_assert("", result == truth_WORD);
  result = tablet_compare(2, tablet, 2, comparison);
  mu_assert("", result == lie_WORD);
  result = tablet_compare(2, tablet, 1, tablet);
  mu_assert("", result == lie_WORD);
  return 0;
}

const char *quiz_htinti_final_word_found() {
  uint16_t tablet[2][16] = {{0}, {0}};
  uint16_t result = htinti_final_word_found(2, tablet);
  mu_assert("", result == 0);
  tablet[0][0] = 1;
  result = htinti_final_word_found(2, tablet);
  mu_assert("", result == 0);
  tablet[0][0] = 9;
  tablet[0][1] = hello_WORD;
  result = htinti_final_word_found(2, tablet);
  mu_assert("", result == 1);
  tablet[0][2] = hello_WORD;
  result = htinti_final_word_found(2, tablet);
  mu_assert("", result == 2);
  tablet[0][8] = hello_WORD;
  result = htinti_final_word_found(2, tablet);
  mu_assert("", result == 8);
  tablet[1][8] = hello_WORD;
  result = htinti_final_word_found(2, tablet);
  mu_assert("", result == 0x18);
  return 0;
}

const char *quiz_first_phrase_long_found() {

  const uint16_t begin_indexFinger = 1;
  const uint16_t input_long = 1;
  const line_t input = {0xC9,   0xEA30, 0x225E, 0x24DE,
    0x881D, 0x0001, 0x287E, 0x217E};
  uint16_t ending_indexFinger = 0;
  uint16_t phrase_long = first_phrase_long_found(begin_indexFinger, input_long,
      &input, &ending_indexFinger);
  DEBUGPRINT(("0x%X phrase_long, 0x%X ending_indexFinger\n", phrase_long,
        ending_indexFinger));
  mu_assert("", phrase_long == 3);
  phrase_long = first_phrase_long_found(ending_indexFinger + 1, input_long,
      &input, &ending_indexFinger);
  DEBUGPRINT(("0x%X phrase_long, 0x%X ending_indexFinger\n", phrase_long,
        ending_indexFinger));
  mu_assert("", phrase_long == 3);
  return 0;
}

const char *quiz_literal_grammar_word_ins_htinka_addenda() {
  const uint16_t grammar_word = accusative_case_GRAMMAR;
  const uint16_t produce_long = 1;
  line_t produce = {0};
  literal_grammar_word_ins_htinka_addenda(grammar_word, produce_long, &produce);
  tablet_print(produce_long, &produce);
  mu_assert("", line_compare(produce, (line_t){0x3, accusative_case_GRAMMAR}) ==
      truth_WORD);
  literal_grammar_word_ins_htinka_addenda(realis_mood_GRAMMAR, produce_long,
      &produce);
  tablet_print(produce_long, &produce);
  mu_assert("",
      line_compare(produce, (line_t){0x7, accusative_case_GRAMMAR,
        realis_mood_GRAMMAR}) == truth_WORD);
  return 0;
}

const char *quiz_tablet_zero() {
  uint16_t tablet[2][16] = {{2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3},
    {2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3}};
  uint8_t tablet_long = 2;
  tablet_zero(tablet_long, tablet);
  uint16_t maximum_tablet_long = tablet_long * LINE_LONG;
  repeat(
      maximum_tablet_long,
      mu_assert("", tablet_read((uint16_t)iterator, tablet_long, tablet) == 0));
  return 0;
}

const char *quiz_retrospective_variable_htin_found_basic() {
  DEBUGPRINT(("%s begin", ""));
  NewTextPhrase(knowledge, 3, "pyacli hnucgina tyutdoka li blapli");
  NewTextPhrase(variable_name, 1, "hnucgi");
  DEBUGPRINT(("%X variable_name.length\n", variable_name.length));
  struct Phrase variable_htin =
    retrospective_variable_htin_found(knowledge, variable_name);
  mu_assert("", phrase_letters_compare(variable_htin,
        " hnucgina tyutdoka li") == truth_WORD);
  return 0;
}

const char *quiz_retrospective_variable_htin_found_hollow() {
  DEBUGPRINT(("%s begin", ""));
  NewTextPhrase(knowledge, 3, "pyacli hnupgina tyutdoka li blapli");
  NewTextPhrase(variable_name, 1, "hnucgi");
  DEBUGPRINT(("%X variable_name.length\n", variable_name.length));
  struct Phrase variable_htin =
    retrospective_variable_htin_found(knowledge, variable_name);
  mu_assert("", variable_htin.length == 0 && variable_htin.begin == 0);
  mu_assert("", phrase_letters_compare(variable_htin, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_found() {
  mu_run_quiz(quiz_retrospective_variable_htin_found_basic);
  mu_run_quiz(quiz_retrospective_variable_htin_found_hollow);
  return 0;
}

const char *quiz_retrospective_phrase_situate_basic() {

  NewTextPhrase(input_phrase, 1, "hyakdoyi li");
  struct Phrase produce =
    neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyakdoyi") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_phrase_situate_hollow() {

  NewTextPhrase(input_phrase, 1, "hyakdoka li");
  struct Phrase produce =
    neo_retrospective_phrase_situate(input_phrase, dative_case_GRAMMAR);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  phrase_print(produce);
  mu_assert("", produce.begin == 0 && produce.length == 0);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_phrase_situate_2() {

  NewTextPhrase(input_phrase, 1, "tyutdoka li");
  struct Phrase produce =
    neo_retrospective_phrase_situate(input_phrase, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdoka") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_phrase_situate_pyacli() {

  NewTextPhrase(input_phrase, 1, "pyaclityutdoka tu");
  struct Phrase produce =
    neo_retrospective_phrase_situate(input_phrase, realis_mood_GRAMMAR);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "pyacli") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_phrase_situate_boundary() {

  NewTextPhrase(input, 2,
      "tyutdopwih bratgika djancu"
      "tyutdoyu psasgiyi lyatgika grettu");
  input.begin = 0xF;
  input.length -= 0xE;
  text_phrase_print(input);
  Page_print(input.page);
  struct Phrase produce =
    neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  phrase_print(produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "lyatgika") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_phrase_situate_boundary2() {

  NewTextPhrase(input, 2,
      "tyutdoyu hyikdoka djancu tyutzrondoyu psasgiyi "
      "zi.prih.a.prih.zika grettu");
  text_phrase_print(input);
  Page_print(input.page);
  struct Phrase produce =
    neo_retrospective_phrase_situate(input, accusative_case_GRAMMAR);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  phrase_print(produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "zi.prih.a.prih.zika") ==
      truth_WORD);
  mu_assert("", produce.begin == 0x11);
  mu_assert("", produce.length == 3);
  return 0;
}
const char *quiz_retrospective_phrase_situate() {
  mu_run_quiz(quiz_retrospective_phrase_situate_basic);
  mu_run_quiz(quiz_retrospective_phrase_situate_hollow);
  mu_run_quiz(quiz_retrospective_phrase_situate_2);
  mu_run_quiz(quiz_retrospective_phrase_situate_pyacli);
  mu_run_quiz(quiz_retrospective_phrase_situate_boundary);
  mu_run_quiz(quiz_retrospective_phrase_situate_boundary2);
  return 0;
}

const char *quiz_phrase_number_extract_basic() {
  NewTextPhrase(htin, 1, "hnucgina hyikdoka li");
  uint16_t number = phrase_number_extract(htin, accusative_case_GRAMMAR);
  mu_assert("", number == 1);
  return 0;
}
const char *quiz_phrase_number_extract_fool() {
  NewTextPhrase(htin, 1, "hnucgina hnucgika li");
  uint16_t number = phrase_number_extract(htin, accusative_case_GRAMMAR);
  mu_assert("", number == 0);
  return 0;
}
const char *quiz_phrase_number_extract_hollow() {
  NewTextPhrase(htin, 1, "hnucgina  li");
  uint16_t number = phrase_number_extract(htin, accusative_case_GRAMMAR);
  mu_assert("", number == 0);
  return 0;
}
const char *quiz_phrase_number_extract() {
  mu_run_quiz(quiz_phrase_number_extract_basic);
  // mu_run_quiz(quiz_phrase_number_extract_fool);
  // mu_run_quiz(quiz_phrase_number_extract_hollow);
  return 0;
}

const char *quiz_dat_plus_interpret_basic() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hyikdoka hnucgiyi plustu");
  produce = dat_plus_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", produce.begin == 1);
  DEBUGPRINT(("0x%X produce.length\n", produce.length));
  mu_assert("", produce.length == 7);
  mu_assert("", phrase_letters_compare(produce, "hnucgina tyutdoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_dat_plus_interpret() {
  mu_run_quiz(quiz_dat_plus_interpret_basic);
  return 0;
}

const char *quiz_variable() {
  mu_run_quiz(variable_declaration);
  mu_run_quiz(quiz_extract_words_ins_htinka_addenda);
  mu_run_quiz(quiz_first_phrase_long_found);
  mu_run_quiz(quiz_retrospective_phrase_situate);
  mu_run_quiz(quiz_tablet_zero);
  mu_run_quiz(quiz_literal_grammar_word_ins_htinka_addenda);
  mu_run_quiz(quiz_retrospective_variable_htin_found);
  mu_run_quiz(quiz_upcoming_variable_to_worth_translate);
  mu_run_quiz(quiz_all_variable_to_worth_translate);
  mu_run_quiz(quiz_phrase_number_extract);
  // mu_run_quiz(variable_what);
  mu_run_quiz(variable_plus);
  mu_run_quiz(variable_plus2);
  mu_run_quiz(variable_plus3);
  return 0;
}

const char *quiz_recipe_load() {
  // channel/area/queue for instructions
  // command list
  NewTextPhrase(knowledge, 8,
      "doyi tyutplus ksuh\n"
      "htikyayi tyutdoka plustufe\n"
      "tcangina hyikdoka li");
  Page_print(knowledge.page);
  NewPagePhrase(command, 8);
  NewTextPhrase(input, HTIN_LONG, "tcangiyi tyutplustu");
  ////  put it all together
  Page_print(input.page);
  command = recipe_load(knowledge, input, command);
  Page_print(command.page);
  page_text_print(command.page);
  text_phrase_print(command);
  mu_assert("", phrase_letters_compare(command, "htikyana tcangika li"
        "htikyayi tyutdoka plustu") ==
      truth_WORD);
  return 0;
}

const char *quiz_retrospective_perspective_found_1() {
  NewTextPhrase(input, 1, "htafna hyumli");
  const struct Phrase perspective = retrospective_perspective_found(input);
  mu_assert("", neo_tablet_read(perspective.begin, input.page) ==
      realis_mood_GRAMMAR &&
      perspective.length == 1 &&
      phrase_letters_compare(perspective, "li") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_perspective_found_2() {
  NewTextPhrase(input, 5, "hnucgina tyutzrondolyatkyitksuh prah");
  input.length = 0xE;
  const struct Phrase perspective = retrospective_perspective_found(input);
  text_phrase_print(perspective);
  mu_assert("", neo_tablet_read(perspective.begin, input.page) ==
      declarative_mood_GRAMMAR);
  mu_assert("", perspective.length == 1);
  mu_assert("", phrase_letters_compare(perspective, "ksuh") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_perspective_found() {
  mu_run_quiz(quiz_retrospective_perspective_found_1);
  mu_run_quiz(quiz_retrospective_perspective_found_2);
  return 0;
}

const char *quiz_retrospective_gvak_found1() {
  DEBUGPRINT(("%s", "testing for grammatical case before verb phrase\n"));
  NewTextPhrase(input, 1, "htafna hyumli");
  const struct Phrase gvak = retrospective_gvak_found(input);
  phrase_print(gvak);
  mu_assert("",
      neo_tablet_read(gvak.begin, input.page) == nominative_case_GRAMMAR);
  mu_assert("", gvak.length == 1);
  mu_assert("", phrase_letters_compare(gvak, "na") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_gvak_found2() {
  DEBUGPRINT(("%s", "testing for grammatical case\n"));
  NewTextPhrase(input, 1, "htafna");
  phrase_print(input);
  const struct Phrase gvak = retrospective_gvak_found(input);
  phrase_print(gvak);
  mu_assert("", neo_tablet_read(gvak.begin, input.page) ==
      nominative_case_GRAMMAR &&
      gvak.length == 1 &&
      phrase_letters_compare(gvak, "na") == truth_WORD);
  return 0;
}

const char *quiz_retrospective_gvak_found4() {
  DEBUGPRINT(("%s", "testing for final grammatical case\n"));
  NewTextPhrase(input, 1, "htafna hkakka");
  phrase_print(input);
  const struct Phrase gvak = retrospective_gvak_found(input);
  phrase_print(gvak);
  DEBUGPRINT(("%X gvak.begin\n", gvak.begin));
  mu_assert("",
      neo_tablet_read(gvak.begin, input.page) == accusative_case_GRAMMAR);
  mu_assert("", gvak.length == 1);
  mu_assert("", phrase_letters_compare(gvak, "ka") == truth_WORD);
  return 0;
}

const char *quiz_retrospective_gvak_found3() {
  DEBUGPRINT(("%s", "testing for not found\n"));
  NewTextPhrase(input, 1, "htafli");
  DEBUGPRINT(("0x%X input.length, 0x%X input.page.plength\n", input.length,
        input.page.plength));
  const struct Phrase gvak = retrospective_gvak_found(input);
  phrase_print(gvak);
  mu_assert("", gvak.begin == 0 && gvak.length == 0);
  return 0;
}
const char *quiz_retrospective_gvak_found() {
  mu_run_quiz(quiz_retrospective_gvak_found2); // plain case
  mu_run_quiz(quiz_retrospective_gvak_found3); // not found
  mu_run_quiz(quiz_retrospective_gvak_found1); // before verb
  mu_run_quiz(quiz_retrospective_gvak_found4); // not found
  return 0;
}

const char *quiz_upcoming_gvak_found4() {
  DEBUGPRINT(("%s", "testing for final grammatical case\n"));
  NewTextPhrase(input, 1, "htafna hkakka");
  phrase_print(input);
  const struct Phrase gvak = upcoming_gvak_found(input);
  phrase_print(gvak);
  mu_assert("", neo_tablet_read(gvak.begin, input.page) ==
      nominative_case_GRAMMAR &&
      gvak.length == 1 &&
      phrase_letters_compare(gvak, "na") == truth_WORD);
  return 0;
}

const char *quiz_upcoming_gvak_found3() {
  DEBUGPRINT(("%s", "testing for not found\n"));
  NewTextPhrase(input, 1, "htafli");
  DEBUGPRINT(("0x%X input.length, 0x%X input.page.plength\n", input.length,
        input.page.plength));
  const struct Phrase gvak = upcoming_gvak_found(input);
  phrase_print(gvak);
  mu_assert("", gvak.begin == 0 && gvak.length == 0);
  return 0;
}

const char *quiz_upcoming_gvak_found2() {
  DEBUGPRINT(("%s", "testing for grammatical case\n"));
  NewTextPhrase(input, 1, "htafna");
  phrase_print(input);
  const struct Phrase gvak = upcoming_gvak_found(input);
  phrase_print(gvak);
  mu_assert("", neo_tablet_read(gvak.begin, input.page) ==
      nominative_case_GRAMMAR &&
      gvak.length == 1 &&
      phrase_letters_compare(gvak, "na") == truth_WORD);
  return 0;
}

const char *quiz_upcoming_gvak_found1() {
  DEBUGPRINT(("%s", "testing for grammatical case after verb phrase\n"));
  NewTextPhrase(input, 2, "tlicli htafna hyumli");
  DEBUGPRINT(("%X input.length\n", input.length));
  const struct Phrase gvak = upcoming_gvak_found(input);
  phrase_print(gvak);
  mu_assert("",
      neo_tablet_read(gvak.begin, input.page) == nominative_case_GRAMMAR);
  mu_assert("", gvak.length == 1);
  mu_assert("", phrase_letters_compare(gvak, "na") == truth_WORD);
  // assert(1==0);
  return 0;
}

const char *quiz_upcoming_gvak_found5() {
  DEBUGPRINT(("%s", "testing for final grammatical case\n"));
  NewTextPhrase(input, 1, "na");
  phrase_print(input);
  text_phrase_print(input);
  const struct Phrase gvak = upcoming_gvak_found(input);
  phrase_print(gvak);
  mu_assert("",
      neo_tablet_read(gvak.begin, input.page) == nominative_case_GRAMMAR);
  mu_assert("", gvak.length == 1);
  mu_assert("", phrase_letters_compare(gvak, "na") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_gvak_found6() {
  DEBUGPRINT(("%s", "testing for final grammatical case\n"));
  NewTextPhrase(input, 1, "hyikdoka hnucgiyi li");
  input.begin = 4;
  phrase_print(input);
  const struct Phrase gvak = upcoming_gvak_found(input);
  phrase_print(gvak);
  mu_assert("", neo_tablet_read(gvak.begin, input.page) == dative_case_GRAMMAR);
  mu_assert("", gvak.length == 1);
  mu_assert("", phrase_letters_compare(gvak, "yi") == truth_WORD);
  return 0;
}

const char *quiz_upcoming_gvak_found() {
  mu_run_quiz(quiz_upcoming_gvak_found3); // not found
  mu_run_quiz(quiz_upcoming_gvak_found2); // plain case
  mu_run_quiz(quiz_upcoming_gvak_found4); // found before another
  mu_run_quiz(quiz_upcoming_gvak_found1); // found after verb phrase
  mu_run_quiz(quiz_upcoming_gvak_found5); // found after verb phrase
  mu_run_quiz(quiz_upcoming_gvak_found6);
  return 0;
}

const char *quiz_neo_tablet_compare() {
  NewTextPage(example, 1, "mina hcanka hsacli");
  NewTextPage(comparison, 1, "hcanka hsacli");
  mu_assert("", neo_tablet_compare(example, example) == truth_WORD);
  mu_assert("", neo_tablet_compare(example, comparison) == lie_WORD);
  return 0;
}
const char *quiz_tablet_letters_compare_true() {
  NewTextPage(example, 1, "mina hcanka hsacli");
  mu_assert("", tablet_letters_compare(example, "mina hcanka hsacli") ==
      truth_WORD);
  return 0;
}

const char *quiz_tablet_letters_compare_lie() {
  NewTextPage(example, 1, "mina hcanka hsacli");
  mu_assert("", tablet_letters_compare(example, "hcanka hsacli") == lie_WORD);
  return 0;
}

const char *quiz_tablet_letters_compare_hollow_something() {
  NewHollowPage(example);
  DEBUGPRINT(("%X el\n", example.plength));
  mu_assert("", tablet_letters_compare(example, "hcanka hsacli") == lie_WORD);
  return 0;
}

const char *quiz_tablet_letters_compare_hollow_hollow() {
  NewTextPage(example, 1, "");
  example.plength = 0;
  mu_assert("", tablet_letters_compare(example, "") == truth_WORD);
  return 0;
}
const char *quiz_tablet_letters_compare() {
  mu_run_quiz(quiz_tablet_letters_compare_true);
  mu_run_quiz(quiz_tablet_letters_compare_lie);
  mu_run_quiz(quiz_tablet_letters_compare_hollow_something);
  mu_run_quiz(quiz_tablet_letters_compare_hollow_hollow);
  return 0;
}

const char *quiz_phrase_letters_compare_basic() {
  NewTextPage(example, 1, "mina hcanka hsacli");
  NewTextPage(comparison, 1, "hcanka hsacli");
  struct Phrase phrase;
  phrase.page = example;
  phrase.begin = 3;
  phrase.length = 4;
  phrase_print(phrase);
  mu_assert("",
      phrase_letters_compare(phrase, "mina hcanka hsacli") == lie_WORD);
  mu_assert("", phrase_letters_compare(phrase, "hcanka hsacli") == truth_WORD);
  return 0;
}

const char *quiz_phrase_letters_compare_hollow() {
  NewPage(page, 1);
  NewHollowPhrase(phrase);
  phrase.page = page;
  mu_assert("",
      phrase_letters_compare(phrase, "mina hcanka hsacli") == lie_WORD);
  mu_assert("", phrase_letters_compare(phrase, "") == truth_WORD);
  return 0;
}
const char *quiz_phrase_letters_compare_giant() {
  NewTextPhrase(input, 2,
      "hyikdopwih tyindoka djancu hyikdoka tyindoyu plustu");
  input.begin = input.length + input.begin - 3;
  input.length = 3;
  text_phrase_print(input);
  mu_assert("", phrase_letters_compare(input, "plustu") == truth_WORD);
  return 0;
}
const char *quiz_phrase_letters_compare_multi() {
  NewTextPhrase(input, 3,
      "hyikdokali"
      "hyikdopwih tu");
  Page_print(input.page);
  mu_assert("", phrase_letters_compare(input, "hyikdokali hyikdopwihtu") ==
      truth_WORD);
  return 0;
}
const char *quiz_phrase_letters_compare_2() {
  /* so this is kindof a weird test, in that it wouldn't happen under normal circumstances, 
   * that you'd have a length longer than the actual length of any meaningful text. 
   * the main issue is in fyakyi_fyakka_dva2ttu and how can one identify that the empty zeros
   * which follow the sentence, are not meaningful and can simply be skipped. 
   *
   * If can figure that out, then can update fyakyi_fyakka_dva2ttu accordingly. 
   * until then may simply have to leave this be */
  NewTextPhrase(input, 3, "hyikdokali");
  Page_print(input.page);
  input.length = 0x14;
  text_phrase_print(input);
  Page_print(input.page);
  TODO(mu_assert("#TODO", phrase_letters_compare(input, "hyikdokali") == truth_WORD));
  return 0;
}
const char *quiz_phrase_letters_compare_3() {
  NewTextPhrase(input, 3,
      "hyikdokali"
      "hyikdopwih tu");
  input.begin = 0x11;
  input.length = 4;
  Page_print(input.page);
  mu_assert("", phrase_letters_compare(input, "hyikdopwihtu") == truth_WORD);
  return 0;
}

const char *quiz_phrase_letters_compare_4() {
  NewTextPhrase(input, 2, "hyikli hnucgina tfattfattfattfatdoka li");
  NewTextPhrase(output, 1, "hnucgina tfattfattfattfatdoka li");
  input.begin = 0x11;
  input.length = 0x9;
  Page_print(input.page);
  mu_assert("", phrase_letters_compare(
        input, "hnucgina tfattfattfattfatdoka li") == truth_WORD);
  return 0;
}
const char *quiz_phrase_letters_compare() {
  mu_run_quiz(quiz_phrase_letters_compare_basic);
  mu_run_quiz(quiz_phrase_letters_compare_hollow);
  mu_run_quiz(quiz_phrase_letters_compare_giant);
  mu_run_quiz(quiz_phrase_letters_compare_multi);
  mu_run_quiz(quiz_phrase_letters_compare_2);
  mu_run_quiz(quiz_phrase_letters_compare_3);
  mu_run_quiz(quiz_phrase_letters_compare_4);
  return 0;
}

const char *quiz_phrase_to_phrase_compare_1() {
  NewTextPhrase(input, 1, "mimonacwasli") NewTextPhrase(comparison, 1,
      "wimonacwasli")
    mu_assert("", phrase_to_phrase_compare(input, comparison) == lie_WORD);
  return 0;
}
const char *quiz_phrase_to_phrase_compare_2() {
  NewTextPhrase(input, 1, "wimonacwasli") NewTextPhrase(comparison, 1,
      "wimonacwasli")
    mu_assert("", phrase_to_phrase_compare(input, comparison) == truth_WORD);
  return 0;
}
const char *quiz_phrase_to_phrase_compare() {
  mu_run_quiz(quiz_phrase_to_phrase_compare_1);
  mu_run_quiz(quiz_phrase_to_phrase_compare_2);
  return 0;
}
const char *quiz_is_thePhrase_aQuote_truth() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.zi");
  uint8_t produce = is_thePhrase_aQuote(input);
  mu_assert("", produce);
  return 0;
}
const char *quiz_is_thePhrase_aQuote_lie() {
  NewTextPhrase(input, 1, "pyacli");
  uint8_t produce = is_thePhrase_aQuote(input);
  mu_assert("", !produce);
  return 0;
}
const char *quiz_is_thePhrase_aParagraphQuote_1() {
  NewTextPhrase(input, 5, "hyikgina tyutzrondo lyatkyitksuh prah");
  input.length = 0xE;
  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", truth_WORD == is_thePhrase_aParagraphQuote(input));
  return 0;
}
const char *quiz_is_thePhrase_aParagraphQuote() {
  mu_run_quiz(quiz_is_thePhrase_aParagraphQuote_1);
  return 0;
}

const char *quiz_is_thePhrase_aQuote() {
  mu_run_quiz(quiz_is_thePhrase_aQuote_truth);
  mu_run_quiz(quiz_is_thePhrase_aQuote_lie);
  mu_run_quiz(quiz_is_thePhrase_aParagraphQuote);
  return 0;
}
const char *quiz_compare() {
  mu_run_quiz(quiz_line_compare);
  mu_run_quiz(quiz_tablet_compare);
  mu_run_quiz(quiz_neo_tablet_compare);
  mu_run_quiz(quiz_phrase_to_phrase_compare);
  mu_run_quiz(quiz_tablet_letters_compare);
  mu_run_quiz(quiz_phrase_letters_compare);
  mu_run_quiz(quiz_is_thePhrase_aQuote);
  return 0;
}
const char *quiz_retrospective_verb_phrase_found1() {
  DEBUGPRINT(("%s", "testing for verb phrase preceded by grammatical case\n"));
  NewTextPhrase(example, 1, "mina hcanka hsacli");
  struct Phrase verbPhrase = retrospective_verb_phrase_found(example);
  mu_assert("", verbPhrase.length == 2);
  mu_assert("", phrase_letters_compare(verbPhrase, "hsacli"));
  return 0;
}
const char *quiz_retrospective_verb_phrase_found2() {
  DEBUGPRINT(("%s", "testing for plain verb  phrase\n"));
  NewTextPhrase(example, 1, "hsacli");
  struct Phrase verbPhrase = retrospective_verb_phrase_found(example);
  mu_assert("", verbPhrase.length == 2);
  mu_assert("", phrase_letters_compare(verbPhrase, "hsacli"));
  return 0;
}
const char *quiz_retrospective_verb_phrase_found3() {
  DEBUGPRINT(("%s", "testing for no verb  phrase\n"));
  NewTextPhrase(example, 1, "hsacna");
  struct Phrase verbPhrase = retrospective_verb_phrase_found(example);
  mu_assert("", verbPhrase.begin == 0 && verbPhrase.length == 0);
  return 0;
}
const char *quiz_retrospective_verb_phrase_found4() {
  DEBUGPRINT(("%s", "testing for paragraph quote\n"));
  NewTextPhrase(example, 5, "hnucgina tyutzrondolyatkyitksuh prah");
  example.length = 0xE;
  struct Phrase perspective = retrospective_perspective_found(example);
  DEBUGPRINT(("%X perspective.begin, %X perspective.length\n", 
        perspective.begin, perspective.length));
  phrase_print(example);
  struct Phrase verbPhrase = retrospective_verb_phrase_found(example);
  DEBUGPRINT(("%X verbPhrase.begin, %X verbPhrase.length\n", 
        verbPhrase.begin, verbPhrase.length));
  phrase_print(verbPhrase);
  mu_assert("", verbPhrase.begin == 4 && verbPhrase.length == 3);
  return 0;
}
const char *quiz_retrospective_verb_phrase_found() {
  mu_run_quiz(quiz_retrospective_verb_phrase_found1); // with case before
  mu_run_quiz(quiz_retrospective_verb_phrase_found2); // with only verb phrase
  mu_run_quiz(quiz_retrospective_verb_phrase_found3); // with no verb phrase
  mu_run_quiz(quiz_retrospective_verb_phrase_found4); // with paragraph quote
  return 0;
}

const char *quiz_neo_retrospective_htin_cousin_found_rea() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "hnucgidoyi tyutplusksuh\n"
      "hnucgiyi tyutdoka plustu\n"
      "tcangina hyikdoka blipli");
  NewTextPhrase(example, 1, "blipli");
  struct Phrase cousin =
    neo_retrospective_htin_cousin_found(example, knowledge);
  Page_print(cousin.page);
  mu_assert("", cousin.length > 0);
  mu_assert("", tablet_letters_compare(
        cousin.page, "tcangina hyikdoka blipli") == truth_WORD);
  return 0;
}
const char *quiz_neo_retrospective_htin_cousin_found_rea2() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "hnucgidoyi tyutplusksuh\n"
      "hnucgiyi tyutdoka plustu\n"
      "hnucgiyi tyutdoka blopli\n"
      "tcangina hyikdoka blipli");
  NewTextPhrase(example, 1, "blopli");
  struct Phrase cousin =
    neo_retrospective_htin_cousin_found(example, knowledge);
  Page_print(cousin.page);
  mu_assert("", cousin.length > 0);
  mu_assert("", phrase_letters_compare(cousin, "hnucgiyi tyutdoka blopli") ==
      truth_WORD);
  return 0;
}
const char *quiz_neo_retrospective_htin_cousin_found_deo() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "hnucgidoyi tyutplusksuh\n"
      "hnucgiyi tyutdoka plustu\n"
      "hnucgiyi tyutdoka blopli\n"
      "tcangina hyikdoka blipli");
  NewTextPhrase(example, 1, "plustu");
  struct Phrase cousin =
    neo_retrospective_htin_cousin_found(example, knowledge);
  Page_print(cousin.page);
  mu_assert("", cousin.length > 0);
  mu_assert("", phrase_letters_compare(cousin, "hnucgiyi tyutdoka plustu") ==
      truth_WORD);
  return 0;
}

const char *quiz_neo_retrospective_htin_cousin_found_dec() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "hnucgidoyi tyutplusksuh\n"
      "hnucgiyi tyutdoka plustu\n"
      "tcangina hyikdoka blipli");
  NewTextPhrase(example, 1, "tyutplusksuh");
  struct Phrase cousin =
    neo_retrospective_htin_cousin_found(example, knowledge);
  Page_print(cousin.page);
  mu_assert("", cousin.length > 0);
  mu_assert("", phrase_letters_compare(cousin, "hnucgidoyi tyutplusksuh") ==
      truth_WORD);
  return 0;
}

const char *quiz_neo_retrospective_htin_cousin_found() {
  mu_run_quiz(quiz_neo_retrospective_htin_cousin_found_rea);
  // mu_run_quiz(quiz_neo_retrospective_htin_cousin_found_rea2);
  mu_run_quiz(quiz_neo_retrospective_htin_cousin_found_deo);
  mu_run_quiz(quiz_neo_retrospective_htin_cousin_found_dec);
  return 0;
}

const char *quiz_retrospective_recipe_ksim_found_basic() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "doyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustufe\n"
      "tcangina hyikdoka li");
  NewTextPhrase(input, 1, "tyutplustu");
  NewPhrase(input_range, 1, LINE_LONG - 1);
  struct Phrase ksim = retrospective_recipe_ksim_found(knowledge, input);
  Page_print(ksim.page);
  mu_assert("", phrase_letters_compare(ksim, "zrondoyi tyutplus ksuh") ==
      truth_WORD);
  return 0;
}

const char *quiz_retrospective_recipe_ksim_found_basic2() {
  NewTextPhrase(knowledge, 8,
      "doyi tyutplus ksuh\n"
      "htikyayi tyutdoka plustufe\n"
      "hnucgidoyi hnucgidoka tyutplus ksuh\n"
      "htikyayi tyutdoka plustufe\n"
      "tcangina hyikdoka li");
  NewTextPhrase(input, 1, "tyutplustu");
  NewPhrase(input_range, 1, LINE_LONG - 1);
  struct Phrase ksim = retrospective_recipe_ksim_found(knowledge, input);
  phrase_print(ksim);
  mu_assert("", phrase_letters_compare(ksim, "zrondoyi tyutplusksuh") ==
      truth_WORD);
  return 0;
}

const char *quiz_retrospective_recipe_ksim_found() {
  mu_run_quiz(quiz_retrospective_recipe_ksim_found_basic);
  mu_run_quiz(quiz_retrospective_recipe_ksim_found_basic2);
  return 0;
}

const char *quiz_final_phrase_found_gram() {
  DEBUGPRINT(("%s\n", ""));
  NewTextPhrase(page, HTIN_LONG,
      "hnucgidoyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustu\n"
      "tcangina hyikdoka ");
  struct Phrase phrase = neo_final_phrase_found(page);
  Page_print(page.page);

  DEBUGPRINT(
      ("%X phrase.length, %X phrase.begin\n", phrase.length, phrase.begin));
  phrase_print(page);
  phrase_print(phrase);
  mu_assert("", phrase.length == 3);
  mu_assert("", phrase_letters_compare(phrase, "hyikdoka") == truth_WORD);
  return 0;
}
const char *quiz_final_phrase_found_gram_hollow() {
  DEBUGPRINT(("%s\n", ""));
  NewTextPhrase(page, HTIN_LONG,
      "hnucgidoyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustufe\n"
      "tcangina ka ");
  struct Phrase phrase = neo_final_phrase_found(page);
  Page_print(page.page);
  DEBUGPRINT(
      ("%X phrase.length, %X phrase.begin\n", phrase.length, phrase.begin));
  phrase_print(phrase);
  mu_assert("", phrase.length == 1);
  mu_assert("", phrase_letters_compare(phrase, "ka") == truth_WORD);
  return 0;
}

const char *quiz_final_phrase_found_mood() {
  NewTextPhrase(page, HTIN_LONG,
      "hnucgidoyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustufe\n"
      "tcangina hyikdoka plusli");
  struct Phrase phrase = neo_final_phrase_found(page);
  Page_print(page.page);
  DEBUGPRINT(
      ("%X phrase.length, %X phrase.begin\n", phrase.length, phrase.begin));
  mu_assert("", phrase.length == 2);
  phrase_print(phrase);
  mu_assert("", phrase_letters_compare(phrase, "plusli") == truth_WORD);
  return 0;
}
const char *quiz_final_phrase_found_mood_hollow() {
  NewTextPhrase(page, HTIN_LONG,
      "hnucgidoyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustu\n"
      "tcangina hyikdoka li");
  struct Phrase phrase = neo_final_phrase_found(page);
  Page_print(page.page);
  DEBUGPRINT(
      ("%X phrase.length, %X phrase.begin\n", phrase.length, phrase.begin));
  phrase_print(phrase);
  mu_assert("", phrase.length == 1);
  mu_assert("", phrase_letters_compare(phrase, "li") == truth_WORD);
  return 0;
}
//const char *quiz_final_phrase_found_fin() {
//  NewTextPhrase(page, 8,
//      "hnucgiyi tyutdoka plustu\n"
//      "tcangina hyikdoka life ");
//  struct Phrase phrase = neo_final_phrase_found(page);
//  Page_print(page.page);
//  DEBUGPRINT(
//      ("%X phrase.length, %X phrase.begin\n", phrase.length, phrase.begin));
//  phrase_print(phrase);
//  TODO(mu_assert("#TODO", phrase.length == 1));
//  TODO(mu_assert("#TODO", phrase_letters_compare(phrase, "fe") == truth_WORD));
//  return 0;
//}

const char *quiz_final_phrase_found() {
  // grammatical case
  mu_run_quiz(quiz_final_phrase_found_gram);
  mu_run_quiz(quiz_final_phrase_found_gram_hollow);
  // verb mood
  mu_run_quiz(quiz_final_phrase_found_mood);
  mu_run_quiz(quiz_final_phrase_found_mood_hollow);
//  mu_run_quiz(quiz_final_phrase_found_fin);
  return 0;
}

const char *quiz_retrospective_recipe_found_basic() {
  NewTextPhrase(knowledge, 4,
      "hnucgidoyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustufe\n"
      "tcangina hyikdoka li");
  NewTextPhrase(input, 1, "tyutplustu");
  struct Phrase recipe = retrospective_recipe_found(knowledge, input);
  Page_print(recipe.page);
  phrase_print(recipe);
  mu_assert("", tablet_letters_compare(
        recipe.page, "hnucgidoyi tyutplus ksuh"
        "hnucgiyi tyutdoka plustufe\n") == truth_WORD);
  return 0;
}

const char *quiz_retrospective_recipe_found() {
  mu_run_quiz(quiz_retrospective_recipe_found_basic);
  return 0;
}

const char *quiz_forward_finally_found_basic() {
  NewTextPhrase(knowledge, HTIN_LONG,
      "hnucgidoyi tyutplus ksuh\n"
      "hnucgiyi tyutdoka plustufe\n"
      "tcangina hyikdoka li");
  NewTextPhrase(input, 1, "tyutplustu");
  struct Phrase finally = forward_finally_found(knowledge);
  DEBUGPRINT(
      ("%X finally.begin, %X finally.length\n", finally.begin, finally.length));
  phrase_print(finally);
  Page_print(finally.page);
  mu_assert("", phrase_letters_compare(finally, "fe") == truth_WORD);
  return 0;
}

const char *quiz_forward_finally_found() {
  mu_run_quiz(quiz_forward_finally_found_basic);
  // mu_run_quiz(quiz_forward_finally_found_none);
  return 0;
}
const char *quiz_retrospective_perspective_htin_cousin_found_1() {
  NewTextPhrase(knowledge, 1, "hyikdokali");
  word_t perspective = realis_mood_GRAMMAR;
  struct Phrase produce =
    retrospective_perspective_htin_cousin_found(perspective, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  mu_assert("", produce.length + produce.begin < 0x10);
  return 0;
}
const char *quiz_retrospective_perspective_htin_cousin_found_2() {
  NewTextPhrase(knowledge, 2, "tyutdoyuplustu hyikdokali");
  word_t perspective = deontic_mood_GRAMMAR;
  struct Phrase produce =
    retrospective_perspective_htin_cousin_found(perspective, knowledge);
  text_phrase_print(produce);
  mu_assert("",
      phrase_letters_compare(produce, "tyutdoyuplustu") == truth_WORD);
  mu_assert("", produce.length + produce.begin < 0x10);
  return 0;
}
const char *quiz_retrospective_perspective_htin_cousin_found() {
  mu_run_quiz(quiz_retrospective_perspective_htin_cousin_found_1);
  mu_run_quiz(quiz_retrospective_perspective_htin_cousin_found_2);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_0() {
  NewTextPhrase(knowledge, 1, "");
  NewTextPhrase(example, 1, "kali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_1() {
  NewTextPhrase(knowledge, 1, "hyikdokali");
  NewTextPhrase(example, 1, "dokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_2() {
  NewTextPhrase(knowledge, 2, "tyutdokali hyikdokapyacli");
  NewTextPhrase(example, 1, "dokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_3() {
  NewTextPhrase(knowledge, 3, "tyindoka hfacli tyutdokali hyikdokapyacli");
  NewTextPhrase(example, 1, "dokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_4() {
  NewTextPhrase(knowledge, 3, "tyindoka li ");
  NewTextPhrase(example, 1, "ksasdokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_5() {
  NewTextPhrase(knowledge, 3, "pyac li ");
  NewTextPhrase(example, 1, "hfacli");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_6() {
  NewTextPhrase(knowledge, 3, "lyatgina hyikdokali ");
  NewTextPhrase(example, 1, "psasgina hyikdokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_7() {
  NewTextPhrase(knowledge, 3, "lyatgina hyikdokali ");
  NewTextPhrase(example, 1, "lyatgina dokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "lyatgina hyikdokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_8() {
  NewTextPhrase(knowledge, 3, "hnucgina hyikdokali tyindokali ");
  NewTextPhrase(example, 1, "ksasdokali");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found_9() {
  NewTextPhrase(knowledge, 3, " ");
  NewTextPhrase(example, 1, "hnucgina ksasdoka li");
  struct Phrase produce =
    retrospective_variable_htin_cousin_found(example, knowledge);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_variable_htin_cousin_found() {
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_0);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_1);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_2);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_3);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_4);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_5);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_6);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_7);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_8);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found_9);
  return 0;
}
const char *quiz_is_theSentence_aVariable_null() {
  NewTextPhrase(input, 1, "");
  mu_assert("", is_theSentence_aVariable(input) == lie_WORD);
  return 0;
}
const char *quiz_is_theSentence_aVariable_1() {
  NewTextPhrase(input, 1, "hyikdokali");
  mu_assert("", is_theSentence_aVariable(input) == truth_WORD);
  return 0;
}
const char *quiz_is_theSentence_aVariable_2() {
  NewTextPhrase(input, 1, "hyikdokahfacli");
  mu_assert("", is_theSentence_aVariable(input) == lie_WORD);
  return 0;
}
const char *quiz_is_theSentence_aVariable_3() {
  NewTextPhrase(input, 1, "hyikdokatu");
  mu_assert("", is_theSentence_aVariable(input) == lie_WORD);
  return 0;
}
const char *quiz_is_theSentence_aVariable_4() {
  NewTextPhrase(input, 1, "tyutdokali");
  mu_assert("", is_theSentence_aVariable(input) == truth_WORD);
  return 0;
}

const char *quiz_is_theSentence_aVariable() {
  mu_run_quiz(quiz_is_theSentence_aVariable_null);
  mu_run_quiz(quiz_is_theSentence_aVariable_1);
  mu_run_quiz(quiz_is_theSentence_aVariable_2);
  mu_run_quiz(quiz_is_theSentence_aVariable_3);
  mu_run_quiz(quiz_is_theSentence_aVariable_4);
  return 0;
}

const char *quiz_cousin_found() {
  mu_run_quiz(quiz_retrospective_phrase_cousin_found);
  mu_run_quiz(quiz_htin_subset_comparison);
  mu_run_quiz(quiz_is_theSentence_aVariable);
  mu_run_quiz(quiz_retrospective_perspective_htin_cousin_found);
  mu_run_quiz(quiz_retrospective_variable_htin_cousin_found);
  mu_run_quiz(quiz_retrospective_htin_cousin_found);
  mu_run_quiz(quiz_neo_retrospective_htin_cousin_found);
  mu_run_quiz(quiz_tablet_retrospective_word_read);
  //  mu_run_quiz(quiz_htinti_final_word_found);
  return 0;
}
const char *quiz_forward_htin_found_basic() {
  NewTextPhrase(input, 2, "cali sana hwat clacri");
  Page_print(input.page);
  phrase_print(input);
  struct Phrase htin = forward_htin_found(input);
  phrase_print(htin);
  mu_assert("", htin.length == 2);
  mu_assert("", phrase_letters_compare(htin, "cali") == truth_WORD);
  return 0;
}

const char *quiz_forward_htin_found_basic2() {
  NewTextPhrase(input, 2, "cali sana hwat clacri");
  input.begin += 0x10;
  input.length -= 0x10;
  Page_print(input.page);
  phrase_print(input);
  struct Phrase htin = forward_htin_found(input);
  Page_print(htin.page);
  phrase_print(htin);
  mu_assert("", phrase_letters_compare(htin, "sana hwat clacri") == truth_WORD);
  return 0;
}
const char *quiz_forward_htin_found_null() {
  NewTextPhrase(input, 2, "");
  Page_print(input.page);
  phrase_print(input);
  struct Phrase htin = forward_htin_found(input);
  DEBUGPRINT(("%X htin.begin, %X htin.length\n", htin.begin, htin.length));
  mu_assert("", htin.length == 0);
  Page_print(htin.page);
  phrase_print(htin);
  mu_assert("", phrase_letters_compare(htin, "") == truth_WORD);
  return 0;
}

const char *quiz_forward_htin_found() {
  mu_run_quiz(quiz_forward_htin_found_basic);
  mu_run_quiz(quiz_forward_htin_found_basic2);
  mu_run_quiz(quiz_forward_htin_found_null);
  return 0;
}
const char *quiz_find_thePrevious_GrammaticalCasePhrase() {
  NewTextPhrase(input, 1, "hkakka flicli");
  struct Phrase produce = find_thePrevious_GrammaticalCasePhrase(input);
  mu_assert("", phrase_letters_compare(produce, "hkakka") == truth_WORD);
  return 0;
}
const char *quiz_find_theNext_GrammaticalCasePhrase_1() {
  NewTextPhrase(input, 2, "plikli hkakka flicli");
  struct Phrase produce = find_theNext_GrammaticalCasePhrase(input);
  mu_assert("", phrase_letters_compare(produce, "hkakka") == truth_WORD);
  return 0;
}
const char *quiz_find_theNext_GrammaticalCasePhrase_2() {
  NewTextPhrase(input, 2, "hkakka flicli");
  struct Phrase produce = find_theNext_GrammaticalCasePhrase(input);
  mu_assert("", phrase_letters_compare(produce, "hkakka") == truth_WORD);
  return 0;
}
const char *quiz_find_theNext_GrammaticalCasePhrase_3() {
  NewTextPhrase(input, 2, "ksastyutdoka plustu");
  struct Phrase produce = find_theNext_GrammaticalCasePhrase(input);
  mu_assert("", phrase_letters_compare(produce, "ksastyutdoka") == truth_WORD);
  return 0;
}
const char *quiz_find_theNext_GrammaticalCasePhrase_4() {
  NewTextPhrase(input, 2, "ka");
  struct Phrase produce = find_theNext_GrammaticalCasePhrase(input);
  mu_assert("", phrase_letters_compare(produce, "ka") == truth_WORD);
  return 0;
}
const char *quiz_find_theNext_GrammaticalCasePhrase() {
  mu_run_quiz(quiz_find_theNext_GrammaticalCasePhrase_1);
  mu_run_quiz(quiz_find_theNext_GrammaticalCasePhrase_2);
  mu_run_quiz(quiz_find_theNext_GrammaticalCasePhrase_3);
  mu_run_quiz(quiz_find_theNext_GrammaticalCasePhrase_4);
  return 0;
}

const char *quiz_upcoming_independent_perspective_found_null() {
  NewTextPhrase(input, 2, "");
  struct Phrase perspective = upcoming_independent_perspective_found(input);
  mu_assert("", perspective.begin <= input.page.plength * LINE_LONG);
  mu_assert("", perspective.length == 0);
  mu_assert("", phrase_letters_compare(perspective, ""));
  DEBUGPRINT(("%X perspective.begin, %X perspective.length\n",
        perspective.begin, perspective.length));
  text_phrase_print(perspective);
  return 0;
}
const char *quiz_upcoming_independent_perspective_found_2() {
  NewTextPhrase(input, 2, "pyaccu");
  struct Phrase perspective = upcoming_independent_perspective_found(input);
  mu_assert("", perspective.begin <= input.page.plength * LINE_LONG);
  mu_assert("", perspective.length == 0);
  mu_assert("", phrase_letters_compare(perspective, ""));
  return 0;
}
const char *quiz_upcoming_independent_perspective_found_1() {
  NewTextPhrase(input, 2, "pyactu");
  struct Phrase perspective = upcoming_independent_perspective_found(input);
  mu_assert("", phrase_letters_compare(perspective, "tu"));
  return 0;
}

const char *quiz_upcoming_independent_perspective_found() {
  mu_run_quiz(quiz_upcoming_independent_perspective_found_null);
  mu_run_quiz(quiz_upcoming_independent_perspective_found_1);
  mu_run_quiz(quiz_upcoming_independent_perspective_found_2);
  return 0;
}

const char *quiz_upcoming_nounphrase_found_null() {
  NewTextPhrase(input, 1, "");
  struct Phrase produce = upcoming_nounphrase_found(input);
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_upcoming_nounphrase_found_simple() {
  NewTextPhrase(input, 1, "hyikdokali");
  struct Phrase produce = upcoming_nounphrase_found(input);
  mu_assert("", phrase_letters_compare(produce, "hyikdoka") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_nounphrase_found_2() {
  NewTextPhrase(input, 1, "hyikdoka hnucgiyi li");
  input.begin = 4;
  input.length = 5;
  struct Phrase produce = upcoming_nounphrase_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", produce.begin >= input.begin);
  mu_assert("", phrase_letters_compare(produce, "hnucgiyi") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_nounphrase_found() {
  mu_run_quiz(quiz_upcoming_nounphrase_found_null);
  mu_run_quiz(quiz_upcoming_nounphrase_found_simple);
  mu_run_quiz(quiz_upcoming_nounphrase_found_2);
  return 0;
}

const char *quiz_upcoming_phrase_found_null() {
  NewTextPhrase(input, 1, "");
  struct Phrase produce = upcoming_phrase_found(input);
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_upcoming_phrase_found_simple() {
  NewTextPhrase(input, 1, "hyikdokali");
  struct Phrase produce = upcoming_phrase_found(input);
  mu_assert("", phrase_letters_compare(produce, "hyikdoka") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_phrase_found_2() {
  NewTextPhrase(input, 1, "hyikdoka hnucgiyi li");
  input.begin = 4;
  input.length = 5;
  struct Phrase produce = upcoming_phrase_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", produce.begin >= input.begin);
  mu_assert("", phrase_letters_compare(produce, "hnucgiyi") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_phrase_found_3() {
  NewTextPhrase(input, 2, "pyacli hyikdoka hnucgiyi li");
  struct Phrase produce = upcoming_phrase_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "pyacli") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_phrase_found_4() {
  NewTextPhrase(input, 1, "tyutdoyu psasgiyi lyatgika grettu");
  text_phrase_print(input);
  input.begin = 7;
  input.length = 5;
  text_phrase_print(input);
  struct Phrase produce = upcoming_phrase_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "lyatgika") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_gvakkupwik_found_1() {
  NewTextPhrase(input, 1, "pyacka");
  struct Phrase produce = upcoming_gvakkupwik_found(input);
  text_phrase_print(produce);
  mu_assert("", phrase_word_read(produce, 0) == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_gvakkupwik_found_2() {
  NewTextPhrase(input, 1, "lyatgika");
  struct Phrase produce = upcoming_gvakkupwik_found(input);
  text_phrase_print(produce);
  mu_assert("", phrase_word_read(produce, 0) == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_gvakkupwik_found_3() {
  NewTextPhrase(input, 1, "tyutdoyu psasgiyi lyatgika");
  input.begin = 7;
  input.length = 5;
  text_phrase_print(input);
  struct Phrase produce = upcoming_gvakkupwik_found(input);
  text_phrase_print(produce);
  mu_assert("", phrase_word_read(produce, 0) == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_gvakkupwik_found() {
  mu_run_quiz(quiz_upcoming_gvakkupwik_found_1);
  mu_run_quiz(quiz_upcoming_gvakkupwik_found_2);
  mu_run_quiz(quiz_upcoming_gvakkupwik_found_3);
  return 0;
}
const char *quiz_upcoming_phrase_found_boundary() {
  NewTextPhrase(input, 2,
      "tyutdopwih bratgika djancu"
      "tyutdoyu psasgiyi lyatgika grettu");
  input.begin = 0xF;
  input.length -= 0xE;
  text_phrase_print(input);
  Page_print(input.page);
  struct Phrase produce = upcoming_phrase_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "lyatgika") == truth_WORD);
  return 0;
}
const char *quiz_upcoming_phrase_found() {
  mu_run_quiz(quiz_upcoming_gvakkupwik_found);
  mu_run_quiz(quiz_upcoming_phrase_found_null);
  mu_run_quiz(quiz_upcoming_phrase_found_simple);
  mu_run_quiz(quiz_upcoming_phrase_found_2);
  mu_run_quiz(quiz_upcoming_phrase_found_3);
  mu_run_quiz(quiz_upcoming_phrase_found_4);
  mu_run_quiz(quiz_upcoming_phrase_found_boundary);
  return 0;
}
const char *quiz_letter_found_null() {
  NewText(input, "");
  NewText(text, "");
  struct Text produce = letter_found(text, input);
  DEBUGPRINT(("%s, %X produce.length\n", produce.letters, produce.length));
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_letter_found1() {
  NewText(input, ".");
  NewText(text, "as.df");
  struct Text produce = letter_found(text, input);
  DEBUGPRINT(("%s\n", produce.letters));
  mu_assert("", produce.length == 1);
  mu_assert("", produce.letters[0] == '.');
  return 0;
}

const char *quiz_letter_found() {
  mu_run_quiz(quiz_letter_found1);
  mu_run_quiz(quiz_letter_found_null);
  return 0;
}
const char *quiz_retrospective_example_gvakfyak_found_boundary() {
  NewTextPhrase(input, 2,
      "tyutdopwih lyatgika djancu"
      "tyutdoyu psasgiyi lyatgika grettu");
  input.begin = 0xF;
  input.length -= 0xE;
  text_phrase_print(input);
  Page_print(input.page);
  word_t gvaktlat = accusative_case_GRAMMAR;
  struct Phrase produce = retrospective_example_gvakfyak_found(input, gvaktlat);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "lyatgika") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_example_gvakfyak_found() {
  mu_run_quiz(quiz_retrospective_example_gvakfyak_found_boundary);
  return 0;
}

const char *quiz_retrospective_example_word_in_phrase_found_1() {
  NewTextPhrase(input, 1, "hnucgina tyutzrondolyatkyitksuh");
  text_phrase_print(input);
  Page_print(input.page);
  word_t example_word = number_WORD;
  struct Phrase produce = 
    retrospective_example_word_in_phrase_found(example_word, input);
  word_t produce_word =  phrase_word_read(produce, 0);
  DEBUGPRINT(("%X produce_word\n", produce_word));
  mu_assert("",  produce_word == example_word); 
  return 0;
}
const char *quiz_upcoming_example_word_in_phrase_found_1() {
  NewTextPhrase(input, 1, "hnucgina tyutzrondolyatkyitksuh");
  text_phrase_print(input);
  Page_print(input.page);
  const word_t example_word = NUMBER_QUOTE;
  const word_t output = 0;
  struct Phrase produce = 
    upcoming_example_word_in_phrase_found(example_word, input);
  word_t produce_word =  phrase_word_read(produce,0);
  DEBUGPRINT(("%X produce_word\n", produce_word));
  mu_assert("",  produce_word == output); 
  return 0;
}
const char *quiz_upcoming_example_word_in_phrase_found_0() {
  NewTextPhrase(input, 1, "hnucgina tyutzrondolyatkyitksuh");
  text_phrase_print(input);
  Page_print(input.page);
  const word_t example_word = PARAGRAPH_LETTER_QUOTE;
  const word_t output = PARAGRAPH_LETTER_QUOTE;
  struct Phrase produce = 
    upcoming_example_word_in_phrase_found(example_word, input);
  word_t produce_word =  phrase_word_read(produce,0);
  DEBUGPRINT(("%X produce_word\n", produce_word));
  mu_assert("",  produce_word == output); 
  return 0;
}
const char *quiz_upcoming_example_word_in_phrase_found() {
  mu_run_quiz(quiz_upcoming_example_word_in_phrase_found_0);
  mu_run_quiz(quiz_upcoming_example_word_in_phrase_found_1);
  return 0;
}

const char *quiz_retrospective_example_word_in_phrase_found() {
  mu_run_quiz(quiz_retrospective_example_word_in_phrase_found_1);
  return 0;
}
const char *quiz_found() {
  mu_run_quiz(quiz_htinti_final_word_found);
  mu_run_quiz(quiz_letter_found);
  mu_run_quiz(quiz_final_phrase_found);
  mu_run_quiz(quiz_upcoming_independent_perspective_found);
  mu_run_quiz(quiz_retrospective_perspective_found);
  mu_run_quiz(quiz_retrospective_gvak_found);
  mu_run_quiz(quiz_retrospective_verb_phrase_found);
  mu_run_quiz(quiz_retrospective_example_word_in_phrase_found);
  mu_run_quiz(quiz_upcoming_example_word_in_phrase_found);
  mu_run_quiz(quiz_forward_htin_found);
  mu_run_quiz(quiz_find_thePrevious_GrammaticalCasePhrase);
  mu_run_quiz(quiz_upcoming_gvak_found);
  mu_run_quiz(quiz_upcoming_nounphrase_found);
  mu_run_quiz(quiz_retrospective_example_gvakfyak_found);
  mu_run_quiz(quiz_upcoming_phrase_found);
  mu_run_quiz(quiz_find_theNext_GrammaticalCasePhrase);
  return 0;
}

const char *quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus_dat() {
  NewTextPhrase(ksim, 2,
      "prifgina hyikdoka li"
      "doyi tyutplusksuh ");

  NewTextPhrase(input, 1, "prifgika prifgiyi tyutplustu");
  NewPagePhrase(dictionary, 4);
  // struct Phrase ksim_fyak =
  dictionary = kwonyi_rwekgvakfyakti_nwonhtinka_plus(ksim, input, dictionary);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doyi") == truth_WORD);
  page_text_print(dictionary.page);
  phrase_print(dictionary);
  text_phrase_print(dictionary);
  mu_assert("", phrase_letters_compare(dictionary, "htikyana prifgika li") ==
      truth_WORD);
  return 0;
}

const char *quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus_acc() {
  NewTextPhrase(ksim, 2,
      "prifgina hyikdoka li"
      "doyi doka tyutplusksuh ");

  NewTextPhrase(input, 1, "prifgika prifgiyi tyutplustu");
  NewPagePhrase(dictionary, 4);
  dictionary = kwonyi_rwekgvakfyakti_nwonhtinka_plus(ksim, input, dictionary);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doka") == truth_WORD);
  page_text_print(dictionary.page);
  mu_assert("", phrase_letters_compare(dictionary, "hkakyana hyikdoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus_ins() {
  NewTextPhrase(ksim, 4,
      "brifgina tyindoka li"
      "prifgina hyikdoka li"
      "doyi doyu tyutplusksuh ");

  NewTextPhrase(input, 1, "prifgika brifgiyu tyutplustu");
  NewPagePhrase(dictionary, 4);
  dictionary = kwonyi_rwekgvakfyakti_nwonhtinka_plus(ksim, input, dictionary);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doyu") == truth_WORD);
  page_text_print(dictionary.page);
  mu_assert("", phrase_letters_compare(dictionary, "hyakyana tyindoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus() {
  mu_run_quiz(quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus_dat);
  mu_run_quiz(quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus_acc);
  mu_run_quiz(quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus_ins);
  return 0;
}

const char *quiz_find_theParagraphZContents_toThePhrase() {
  NewTextPhrase(recipe, 8,
      "doyi tyutplus ksuh\n"
      "htikyayi tyutdoka plustu"
      "fe\n");
  recipe = find_theParagraphZContents_toTheParagraph(recipe);
  DEBUGPRINT(
      ("%X recipe.begin, %X recipe.length\n", recipe.begin, recipe.length));
  text_phrase_print(recipe);
  mu_assert("", phrase_letters_compare(recipe, "htikyayi tyutdoka plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_drop_theParagraphZFirstSentence_toTheParagraph_basic1() {
  NewTextParagraph(input, 4, "hfanli hpanli");
  input = drop_theParagraphZFirstSentence_toTheParagraph(input);
  mu_assert("", phrase_letters_compare(input, "hpanli") == truth_WORD);
  return 0;
}
const char *quiz_drop_theParagraphZFirstSentence_toTheParagraph_basic2() {
  NewTextParagraph(input, 4, "hfanli tcanli hpanli");
  input = drop_theParagraphZFirstSentence_toTheParagraph(input);
  mu_assert("", phrase_letters_compare(input, "tcanli hpanli") == truth_WORD);
  return 0;
}
const char *quiz_drop_theParagraphZFirstSentence_toTheParagraph() {
  mu_run_quiz(quiz_drop_theParagraphZFirstSentence_toTheParagraph_basic1);
  mu_run_quiz(quiz_drop_theParagraphZFirstSentence_toTheParagraph_basic2);
  return 0;
}
const char *quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic1() {
  NewTextParagraph(input, 4, "hfanli hpanli");
  input = drop_theParagraphZFinalSentence_toTheParagraph(input);
  mu_assert("", phrase_letters_compare(input, "hfanli") == truth_WORD);
  return 0;
}
const char *quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic3() {
  NewTextParagraph(input, 4, "hfanli hpanli tcanli");
  input = drop_theParagraphZFinalSentence_toTheParagraph(input);
  text_phrase_print(input);
  mu_assert("", phrase_letters_compare(input, "hfanli hpanli") == truth_WORD);
  return 0;
}
const char *quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic2() {
  NewTextParagraph(input, 4, "hfanli hpanli fe ");
  input = drop_theParagraphZFinalSentence_toTheParagraph(input);
  // DEBUGPRINT(("%
  text_phrase_print(input);
  mu_assert("", phrase_letters_compare(input, "hfanli hpanli") == truth_WORD);
  return 0;
}
const char *quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic4() {
  NewTextParagraph(input, 4,
      "htikyayi tyutdoka plusksuh"
      "htikyayi tyutdoka plustu"
      "fe\n");

  input = drop_theParagraphZFirstSentence_toTheParagraph(input);
  DEBUGPRINT(("%X input.begin, %X input.length\n", input.begin, input.length));
  text_phrase_print(input);
  input = drop_theParagraphZFinalSentence_toTheParagraph(input);
  DEBUGPRINT(("%X input.begin, %X input.length\n", input.begin, input.length));
  text_phrase_print(input);
  mu_assert("", phrase_letters_compare(input, "htikyayi tyutdoka plustu") ==
      truth_WORD);
  // assert(1==0);
  return 0;
}
const char *quiz_drop_theParagraphZFinalSentence_toTheParagraph() {
  mu_run_quiz(quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic1);
  mu_run_quiz(quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic3);
  mu_run_quiz(quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic2);
  mu_run_quiz(quiz_drop_theParagraphZFinalSentence_toTheParagraph_basic4);
  return 0;
}
const char *quiz_find_theParagraphZFirstSentence_toTheSentence_basic1() {
  NewTextParagraph(input, 4, "hfanli hpanli");
  struct Sentence firstSentence =
    find_theParagraphZFirstSentence_toTheSentence(input);
  text_phrase_print(firstSentence);
  DEBUGPRINT(("%X firstSentence.length\n", firstSentence.length));
  mu_assert("", phrase_letters_compare(firstSentence, "hfanli") == truth_WORD);
  return 0;
}
const char *quiz_find_theParagraphZFirstSentence_toTheSentence_basic2() {
  NewTextParagraph(input, 4, "hfanli hcapli hpanli");
  struct Sentence firstSentence =
    find_theParagraphZFirstSentence_toTheSentence(input);
  text_phrase_print(firstSentence);
  DEBUGPRINT(("%X firstSentence.length\n", firstSentence.length));
  mu_assert("", phrase_letters_compare(firstSentence, "hfanli") == truth_WORD);
  return 0;
}
const char *quiz_find_theParagraphZFirstSentence_toTheSentence() {
  mu_run_quiz(quiz_find_theParagraphZFirstSentence_toTheSentence_basic1);
  mu_run_quiz(quiz_find_theParagraphZFirstSentence_toTheSentence_basic2);
  return 0;
}
const char *quiz_find_theParagraphZLastSentence_toTheSentence_basic1() {
  NewTextParagraph(input, 4, "hfanli hpanli");
  struct Sentence lastSentence =
    find_theParagraphZLastSentence_toTheSentence(input);
  text_phrase_print(lastSentence);
  DEBUGPRINT(("%X lastSentence.length\n", lastSentence.length));
  mu_assert("", lastSentence.length + lastSentence.begin <=
      input.length + input.begin);
  mu_assert("", phrase_letters_compare(lastSentence, "hpanli") == truth_WORD);
  return 0;
}
const char *quiz_find_theParagraphZLastSentence_toTheSentence_basic2() {
  NewTextParagraph(input, 4, "hfanli tcapli hpanli");
  struct Sentence lastSentence =
    find_theParagraphZLastSentence_toTheSentence(input);
  text_phrase_print(lastSentence);
  DEBUGPRINT(("%X lastSentence.length\n", lastSentence.length));
  mu_assert("", lastSentence.length + lastSentence.begin <=
      input.length + input.begin);
  mu_assert("", phrase_letters_compare(lastSentence, "hpanli") == truth_WORD);
  return 0;
}
const char *quiz_find_theParagraphZLastSentence_toTheSentence_basic3() {
  NewTextParagraph(input, 8,
      "htikyayi tyutdoka plustu"
      "fe\n");
  struct Sentence lastSentence =
    find_theParagraphZLastSentence_toTheSentence(input);
  text_phrase_print(lastSentence);
  DEBUGPRINT(("%X lastSentence.length\n", lastSentence.length));
  mu_assert("", lastSentence.length + lastSentence.begin <=
      input.length + input.begin);
  mu_assert("", phrase_letters_compare(lastSentence, "fe") == truth_WORD);
  return 0;
}
const char *quiz_find_theParagraphZLastSentence_toTheSentence_basic4() {
  NewTextParagraph(input, 8,
      "htikyayi tyutdoka plusksuh"
      "htikyayi tyutdoka plustu"
      "htikyayi tyindoka plustu"
      "fe\n");

  input = drop_theParagraphZFirstSentence_toTheParagraph(input);
  struct Sentence lastSentence =
    find_theParagraphZLastSentence_toTheSentence(input);
  text_phrase_print(lastSentence);
  DEBUGPRINT(("%X lastSentence.length\n", lastSentence.length));
  mu_assert("", lastSentence.length + lastSentence.begin <=
      input.length + input.begin);
  mu_assert("", phrase_letters_compare(lastSentence, "fe") == truth_WORD);
  // assert(1==0);
  return 0;
}

const char *quiz_find_theParagraphZLastSentence_toTheSentence() {
  mu_run_quiz(quiz_find_theParagraphZLastSentence_toTheSentence_basic1);
  mu_run_quiz(quiz_find_theParagraphZLastSentence_toTheSentence_basic2);
  mu_run_quiz(quiz_find_theParagraphZLastSentence_toTheSentence_basic3);
  mu_run_quiz(quiz_find_theParagraphZLastSentence_toTheSentence_basic4);
  return 0;
}
const char *quiz_recipe() {
  // mu_run_quiz(quiz_independentClause_long_found);
  mu_run_quiz(quiz_find_theParagraphZFirstSentence_toTheSentence);
  mu_run_quiz(quiz_find_theParagraphZLastSentence_toTheSentence);
  mu_run_quiz(quiz_drop_theParagraphZFirstSentence_toTheParagraph);
  mu_run_quiz(quiz_drop_theParagraphZFinalSentence_toTheParagraph);
  mu_run_quiz(quiz_find_theParagraphZContents_toThePhrase);
  mu_run_quiz(quiz_recipe_load);
  // mu_run_quiz(quiz_worker);
  // function quiz
  // number _num _dat two plus _dec
  // two _num _acc number _nam _dat plus _deo
  // _fin
  //
  // turning it into a function
  // call:
  //  number _nam _dat two plus _deo
  // transforms into:
  //  two _num _acc number _nam _dat plus _deo
  return 0;
}

const char *quiz_tablet_retrospective_grammar_read_mood() {
  NewTextPage(input, 1, "pyacli");
  uint16_t indexFinger = 1 * LINE_LONG - 1;
  Page_print(input);
  word_t grammar_word = tablet_retrospective_grammar_read(
      indexFinger, input.plength, input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_tablet_retrospective_grammar_read_mood_partial() {
  NewTextPage(input, 1, "pyac cu pyac li");
  uint16_t indexFinger = 3;
  Page_print(input);
  uint16_t grammar_word = tablet_retrospective_grammar_read(
      indexFinger, input.plength, input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == conditional_mood_GRAMMAR);
  return 0;
}
const char *quiz_tablet_retrospective_grammar_read_gram() {
  NewTextPage(input, 1, "pyacka");
  uint16_t indexFinger = 1 * LINE_LONG - 1;
  Page_print(input);
  uint16_t grammar_word = tablet_retrospective_grammar_read(
      indexFinger, input.plength, input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}

const char *quiz_tablet_retrospective_grammar_read_complex() {
  NewTextPhrase(input, 2,
      "hyikdopwih tyindoka djancu"
      "hyikdoyu tyindoka plustu ");
  uint16_t indexFinger = input.begin + input.length - 2;
  Page_print(input.page);
  DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  uint16_t grammar_word = tablet_retrospective_grammar_read(
      indexFinger, input.page.plength, input.page.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_tablet_retrospective_grammar_read_acc() {
  NewTextPhrase(input, 1, "tyutdoka ");
  uint16_t indexFinger = 0x10;
  Page_print(input.page);
  DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  uint16_t grammar_word = tablet_retrospective_grammar_read(
      indexFinger, input.page.plength, input.page.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_tablet_retrospective_grammar_read_boundary() {
  NewTextPhrase(input, 2,
      "tyutdopwih bratgika djancu"
      "tyutdoyu psasgiyi lyatgika grettu");
  input.begin = 0xF;
  input.length -= 0xE;
  text_phrase_print(input);
  Page_print(input.page);
  uint16_t indexFinger = 0x11;
  Page_print(input.page);
  DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  uint16_t grammar_word = tablet_retrospective_grammar_read(
      indexFinger, input.page.plength, input.page.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == dative_case_GRAMMAR);
  return 0;
}
const char *quiz_tablet_retrospective_grammar_read_boundary2() {
  DEBUGPRINT();
  NewTextPhrase(input, 2,
      "hnucginahyikdokali"
      "");
  input.page.lines[0][0] = 1;
  input.page.lines[0][1] = NUMBER_QUOTE;
  input.begin = 0x1;
  input.length = 0x11;
  text_phrase_print(input);
  Page_print(input.page);
  uint16_t indexFinger = 0x11;
  Page_print(input.page);
  DEBUGPRINT(("0x%X indexFinger\n", indexFinger));
  struct Phrase gvakfyak = neo_tablet_retrospective_grammar_read(input);
  word_t grammar_word = phrase_word_read(gvakfyak, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == 0);
  return 0;
}

const char *quiz_tablet_retrospective_grammar_read() {
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_mood);
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_mood_partial);
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_gram);
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_complex);
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_acc);
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_boundary);
  mu_run_quiz(quiz_tablet_retrospective_grammar_read_boundary2);
  return 0;
}

const char *quiz_tablet_upcoming_grammar_read_mood() {
  NewTextPage(input, 1, "pyacli");
  uint16_t indexFinger = 1;
  Page_print(input);
  word_t grammar_word = tablet_upcoming_grammar_read(indexFinger, input.plength,
      input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_tablet_upcoming_grammar_read_mood_partial() {
  NewTextPage(input, 1, "pyac cu pyac li");
  uint16_t indexFinger = 3;
  Page_print(input);
  uint16_t grammar_word = tablet_upcoming_grammar_read(
      indexFinger, input.plength, input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}

const char *quiz_tablet_upcoming_grammar_read_gram() {
  NewTextPage(input, 1, "pyacka");
  uint16_t indexFinger = 1;
  Page_print(input);
  uint16_t grammar_word = tablet_upcoming_grammar_read(
      indexFinger, input.plength, input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}

const char *quiz_tablet_upcoming_grammar_read_gram2() {
  NewTextPage(input, 2, "tlicli pyacka");
  uint16_t indexFinger = 4;
  Page_print(input);
  word_t grammar_word = tablet_upcoming_grammar_read(indexFinger, input.plength,
      input.lines, &indexFinger);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_tablet_upcoming_grammar_read_gram4() {
  NewTextPhrase(input, 2, "hyikdoka hnucgiyu plustu");
  input.begin = 4;
  struct Phrase produce = neo_tablet_upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(produce, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == instrumental_case_GRAMMAR);
  return 0;
}
const char *quiz_tablet_upcoming_grammar_read_lyat() {
  NewTextPhrase(input, 1, "tyutdoyu psasgiyi lyatgika");
  input.begin = 7;
  input.length = 5;
  struct Phrase produce = neo_tablet_upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(produce, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", produce.length == 1);
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_tablet_upcoming_grammar_read() {
  mu_run_quiz(quiz_tablet_upcoming_grammar_read_mood);
  mu_run_quiz(quiz_tablet_upcoming_grammar_read_mood_partial);
  mu_run_quiz(quiz_tablet_upcoming_grammar_read_gram);
  mu_run_quiz(quiz_tablet_upcoming_grammar_read_gram2);
  mu_run_quiz(quiz_tablet_upcoming_grammar_read_gram4);
  mu_run_quiz(quiz_tablet_upcoming_grammar_read_lyat);
  return 0;
}

const char *quiz_neo_tablet_upcoming_grammar_read_mood() {
  NewTextPhrase(input, 1, "pyacli");
  // uint16_t indexFinger = 1;
  phrase_print(input);
  struct Phrase gvak = neo_tablet_upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(gvak, 0);

  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_neo_tablet_upcoming_grammar_read_mood_partial() {
  NewTextPhrase(input, 1, "pyac cu pyac li");
  // uint16_t indexFinger = 3;
  phrase_print(input);
  input.begin += 3;
  input.length -= 3;
  struct Phrase gvak = neo_tablet_upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(gvak, 0);

  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}

const char *quiz_neo_tablet_upcoming_grammar_read_gram() {
  NewTextPhrase(input, 1, "pyacka");
  // uint16_t indexFinger = 1;
  phrase_print(input);
  struct Phrase gvak = neo_tablet_upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(gvak, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}

const char *quiz_neo_tablet_upcoming_grammar_read_gram2() {
  NewTextPhrase(input, 2, "tlicli pyacka");
  input.begin += 0x4;
  input.length -= 0x4;
  phrase_print(input);
  struct Phrase gvak = neo_tablet_upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(gvak, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_neo_tablet_upcoming_grammar_read() {
  mu_run_quiz(quiz_neo_tablet_upcoming_grammar_read_mood);
  mu_run_quiz(quiz_neo_tablet_upcoming_grammar_read_mood_partial);
  mu_run_quiz(quiz_neo_tablet_upcoming_grammar_read_gram);
  mu_run_quiz(quiz_neo_tablet_upcoming_grammar_read_gram2);
  return 0;
}

const char *quiz_upcoming_grammar_read_mood() {
  NewTextPhrase(input, 1, "pyacli");
  phrase_print(input);
  struct Phrase word = upcoming_grammar_read(input);
  DEBUGPRINT(("%X word.begin, %X word.length\n", word.begin, word.length));
  phrase_print(word);
  word_t grammar_word = phrase_word_read(word, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_grammar_read_mood_partial() {
  NewTextPhrase(input, 1, "pyac cu pyac li");
  input.begin = 3;
  Page_print(input.page);
  phrase_print(input);
  struct Phrase word = upcoming_grammar_read(input);
  DEBUGPRINT(("%X word.begin, %X word.length\n", word.begin, word.length));
  phrase_print(word);
  word_t grammar_word = phrase_word_read(word, 0);
  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == realis_mood_GRAMMAR);
  return 0;
}

const char *quiz_upcoming_grammar_read_gram() {
  NewTextPhrase(input, 1, "pyacka");
  phrase_print(input);
  struct Phrase word = upcoming_grammar_read(input);
  word_t grammar_word = phrase_word_read(word, 0);

  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_grammar_read_multi() {
  NewTextPhrase(input, 4, "cali sana hwat clacri");
  input.begin += 0x13;
  input.length -= 0x13;
  phrase_print(input);
  text_phrase_print(input);
  Page_print(input.page);
  struct Phrase word = upcoming_grammar_read(input);
  text_phrase_print(word);
  word_t grammar_word = phrase_word_read(word, 0);

  DEBUGPRINT(("%X grammar_word\n", grammar_word));
  mu_assert("", grammar_word == interrogative_mood_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_grammar_read_null() {
  NewTextPhrase(input, 4, "clac");
  phrase_print(input);
  text_phrase_print(input);
  struct Phrase word = upcoming_grammar_read(input);
  mu_assert("", word.length == 0);
  mu_assert("", word.begin <= word.page.plength * LINE_LONG);
  mu_assert("", phrase_letters_compare(word, ""));
  text_phrase_print(word);
  return 0;
}

const char *quiz_upcoming_grammar_read() {
  mu_run_quiz(quiz_upcoming_grammar_read_null);
  mu_run_quiz(quiz_upcoming_grammar_read_mood);
  mu_run_quiz(quiz_upcoming_grammar_read_mood_partial);
  mu_run_quiz(quiz_upcoming_grammar_read_gram);
  mu_run_quiz(quiz_upcoming_grammar_read_multi);
  return 0;
}

const char *quiz_retrospective_htin_found_1() {
  NewTextPhrase(input, 2, "hyikdokali tyutdokali");
  struct Phrase produce = retrospective_htin_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_htin_found_2() {
  NewTextPhrase(input, 2, "hyikdokali tyutdokali");
  input = retrospective_htin_found(input);
  mu_assert("", phrase_letters_compare(input, "tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_htin_found_null() {
  NewTextPhrase(input, 2, "");
  struct Phrase produce = retrospective_htin_found(input);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_htin_found_3() {
  NewTextPhrase(input, 2, "tyutdokali");
  struct Phrase produce = retrospective_htin_found(input);
  text_phrase_print(produce);
  text_page_print(produce.page);
  // Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "tyutdokali") == truth_WORD);
  mu_assert("", produce.page.lines[0][1] == 0x881D);
  return 0;
}
const char *quiz_retrospective_htin_found_4() {
  NewTextPhrase(input, 2, "hyikdokali tyutdokali");
  input.begin = 1;
  input.length = 0xE;
  struct Phrase produce = retrospective_htin_found(input);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_retrospective_htin_found() {
  mu_run_quiz(quiz_retrospective_htin_found_1);
  mu_run_quiz(quiz_retrospective_htin_found_null);
  mu_run_quiz(quiz_retrospective_htin_found_2);
  mu_run_quiz(quiz_retrospective_htin_found_3);
  mu_run_quiz(quiz_retrospective_htin_found_4);
  return 0;
}

//const char *quiz_tablet_upcoming_grammar_or_quote_read_1() {
//  NewTextPhrase(input, 1, "bluh");
//  word_t word;
//  TODO(mu_assert("#TODO", word == boolean_GRAMMAR));
//  return 0;
//}
const char *quiz_tablet_upcoming_grammar_or_quote_read() {
//  mu_run_quiz(quiz_tablet_upcoming_grammar_or_quote_read_1);
  return 0;
}

const char *quiz_tablet_upcoming_word_read_word(){
  NewTextPage(input, 1, "hwacwu");
  txik_t txik;
  word_t word = tablet_upcoming_word_read(1, 1, input.lines, &txik);
  Page_print(input);
  DEBUGPRINT(("%X word, %X txik\n", word, txik));
  mu_assert("", word == world_WORD);
 return 0;
}

const char *quiz_upcoming_word_read_paragraph_quote(){
  NewTextPage(input, 6, "hnimgina tyutzrondo lyatkyitksuh kyitprah");
  txik_t txik;
  Page_print(input);
  word_t word = upcoming_word_read(0x10, 6, input.lines, &txik);
  DEBUGPRINT(("%X word, %X txik\n", word, txik));
  mu_assert("", word == quoted_WORD);
 return 0;
}

const char *quiz_tablet_upcoming_word_read(){
  mu_run_quiz(quiz_tablet_upcoming_word_read_word);
  mu_run_quiz(quiz_upcoming_word_read_paragraph_quote);
 return 0;
}
const char *quiz_grammar() {
  mu_run_quiz(quiz_tablet_retrospective_grammar_read);
  mu_run_quiz(quiz_tablet_upcoming_word_read);
  mu_run_quiz(quiz_tablet_upcoming_grammar_read);
  mu_run_quiz(quiz_neo_tablet_upcoming_grammar_read);
  mu_run_quiz(quiz_tablet_upcoming_grammar_or_quote_read);
  mu_run_quiz(quiz_upcoming_grammar_read);
  mu_run_quiz(quiz_final_perspective_found);
  mu_run_quiz(quiz_retrospective_htin_long_found);
  mu_run_quiz(quiz_retrospective_htin_found);
  mu_run_quiz(quiz_upcoming_perspective_found);
  mu_run_quiz(quiz_htin_long_found);
  return 0;
}
const char *quiz_kwonyi_wigvakfyakti_nwonhtinka_plus() {
  NewTextPhrase(ksim, 2,
      "prifgina hyikdoka li"
      "doyi doka tyutplusksuh ");

  NewTextPhrase(input, 1, "prifgika prifgiyi tyutplustu");
  NewPagePhrase(dictionary, 4);
  dictionary = kwonyi_wigvakfyakti_nwonhtinka_plus(ksim, input, dictionary);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doka") == truth_WORD);
  page_text_print(dictionary.page);
  mu_assert("", phrase_letters_compare(dictionary, "hkakyana hyikdoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_nwonhtinyi_gvakfyakka_ryantu_dat() {
  NewTextPhrase(ksim, 2,
      "prifgina hyikdoka li"
      "doyi tyutplusksuh ");

  NewTextPhrase(input, 1, "prifgiyi");
  NewTextPhrase(output, 1, "htikyana prifgika li");
  NewPagePhrase(dictionary, 4);
  // struct Phrase ksim_fyak =
  dictionary = nwonhtinyi_gvakfyakka_ryantu(dictionary, input);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doyi") == truth_WORD);
  page_text_print(dictionary.page);
  phrase_print(dictionary);
  text_phrase_print(dictionary);
  Page_print(dictionary.page);
  Page_print(output.page);
  mu_assert("", phrase_letters_compare(dictionary, "htikyana prifgika li") ==
      truth_WORD);
  return 0;
}

const char *quiz_nwonhtinyi_gvakfyakka_ryantu_acc() {
  NewTextPhrase(ksim, 2,
      "prifgina hyikdoka li"
      "doyi doka tyutplusksuh ");

  NewTextPhrase(input, 1, "hyikdoka");
  NewPagePhrase(dictionary, 4);
  dictionary = nwonhtinyi_gvakfyakka_ryantu(dictionary, input);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doka") == truth_WORD);
  page_text_print(dictionary.page);
  mu_assert("", phrase_letters_compare(dictionary, "hkakyana hyikdoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_nwonhtinyi_gvakfyakka_ryantu_ins() {
  NewTextPhrase(input, 1, "tyindoyu");
  NewPagePhrase(dictionary, 4);
  dictionary = nwonhtinyi_gvakfyakka_ryantu(dictionary, input);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doyu") == truth_WORD);
  page_text_print(dictionary.page);
  mu_assert("", phrase_letters_compare(dictionary, "hyakyana tyindoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_nwonhtinyi_gvakfyakka_ryantu_ins2() {
  NewTextPhrase(input, 1, "tyindoyu");
  NewTextPhrase(dictionary, 4, "tyutdokali");
  Page_print(dictionary.page);
  dictionary = nwonhtinyi_gvakfyakka_ryantu(dictionary, input);
  page_text_print(dictionary.page);
  text_phrase_print(dictionary);
  Page_print(dictionary.page);
  mu_assert("", page_letters_compare(dictionary.page,
        "tyutdokali hyakyana tyindoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_nwonhtinyi_gvakfyakka_ryantu_multi() {
  NewTextPhrase(input, 1, "tyindoyu");
  NewTextPhrase(dictionary, 4, "tyutdokali");
  Page_print(dictionary.page);
  dictionary = nwonhtinyi_gvakfyakka_ryantu(dictionary, input);
  dictionary = nwonhtinyi_gvakfyakka_ryantu(dictionary, input);
  page_text_print(dictionary.page);
  text_phrase_print(dictionary);
  Page_print(dictionary.page);
  mu_assert("", page_letters_compare(
        dictionary.page,
        "tyutdokali hyakyana tyindoka lihyakyana tyindoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_nwonhtinyi_gvakfyakka_ryantu() {
  mu_run_quiz(quiz_nwonhtinyi_gvakfyakka_ryantu_dat);
  mu_run_quiz(quiz_nwonhtinyi_gvakfyakka_ryantu_acc);
  mu_run_quiz(quiz_nwonhtinyi_gvakfyakka_ryantu_ins);
  mu_run_quiz(quiz_nwonhtinyi_gvakfyakka_ryantu_ins2);
  mu_run_quiz(quiz_nwonhtinyi_gvakfyakka_ryantu_multi);
  return 0;
}

const char *quiz_nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu_1() {
  NewTextPhrase(input, 1, "tyutdoka tyindoyu htikgiyi plustu");
  NewPagePhrase(dictionary, 4);
  dictionary = nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu(dictionary, input);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doyu") == truth_WORD);
  page_text_print(dictionary.page);
  text_phrase_print(dictionary);
  mu_assert("", phrase_letters_compare(dictionary,
        "htikyana htikgika li"
        "hyakyana tyindoka li"
        "hkakyana tyutdoka li") == truth_WORD);
  return 0;
}

const char *quiz_nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu_2() {
  NewTextPhrase(input, 1, "tyutdoka tyindoyu htikgiyi plustu");
  NewPagePhrase(dictionary, 4);
  dictionary = nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu(dictionary, input);
  // phrase_print(ksim_fyak);
  // mu_assert("",
  //          phrase_letters_compare(ksim_fyak, "doyu") == truth_WORD);
  page_text_print(dictionary.page);
  text_phrase_print(dictionary);
  mu_assert("", phrase_letters_compare(dictionary,
        "htikyana htikgika li"
        "hyakyana tyindoka li"
        "hkakyana tyutdoka li") == truth_WORD);
  return 0;
}

const char *quiz_nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu() {
  mu_run_quiz(quiz_nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu_1);
  return 0;
}

const char *quiz_nwonhtin() {
  mu_run_quiz(quiz_kwonyi_rwekgvakfyakti_nwonhtinka_plus);
  mu_run_quiz(quiz_nwonhtinyi_gvakfyakka_ryantu);
  mu_run_quiz(quiz_nwonhtinpoyi_tlichtinti_gvakfyakpoka_ryantu);
  return 0;
}

const char *quiz_page_translate_basic() {

  NewTextPage(input, 1, "hcanli");
  NewTextPad(produce, LINE_LONG * WORD_LONG);
  NewText(perfect_produce, "hcanli ");
  produce = page_translate(input, fluent_WORD, produce);
  DEBUGPRINT(("'%s' produce.letters, %X produce.length\n", produce.letters,
        produce.length));
  DEBUGPRINT(("'%s' perfect_produce.letters, %X perfect_produce.length\n",
        perfect_produce.letters, perfect_produce.length));
  mu_assert("", text_compare(produce, perfect_produce) == truth_WORD);
  return 0;
}
const char *quiz_page_translate_multiarg() {

  NewTextPage(input, 1, "hnucyi hcanli");
  NewTextPad(produce, LINE_LONG * WORD_LONG);
  NewText(perfect_produce, "hnucyi hcanli ");
  produce = page_translate(input, fluent_WORD, produce);
  DEBUGPRINT(("'%s' produce.letters, %X produce.length\n", produce.letters,
        produce.length));
  DEBUGPRINT(("'%s' perfect_produce.letters, %X perfect_produce.length\n",
        perfect_produce.letters, perfect_produce.length));
  mu_assert("", text_compare(produce, perfect_produce) == truth_WORD);
  return 0;
}
const char *quiz_page_translate_number() {

  NewTextPage(input, 1, "hyiktyutdoka hnucyi hcanli");
  NewTextPad(produce, LINE_LONG * WORD_LONG);
  NewText(perfect_produce, "hyiktyutdoka hnucyi hcanli ");
  produce = page_translate(input, fluent_WORD, produce);
  DEBUGPRINT(("'%s' produce.letters, %X produce.length\n", produce.letters,
        produce.length));
  DEBUGPRINT(("'%s' perfect_produce.letters, %X perfect_produce.length\n",
        perfect_produce.letters, perfect_produce.length));
  mu_assert("", text_compare(produce, perfect_produce) == truth_WORD);
  return 0;
}

const char *quiz_page_translate_paragraph_quote() {
  NewTextPage(input, 8, "hnimgina tyutzrondo lyatkyitksuh prah");
  NewTextPad(produce, LINE_LONG * WORD_LONG);
  NewText(perfect_produce, "hnimgina tyutzrondo lyatkyitksuh prah ");
  produce = page_translate(input, fluent_WORD, produce);
  Page_print(input);
  DEBUGPRINT(("'%s' produce.letters, %X produce.length\n", produce.letters,
        produce.length));
  DEBUGPRINT(("'%s' perfect_produce.letters, %X perfect_produce.length\n",
        perfect_produce.letters, perfect_produce.length));
  mu_assert("", text_compare(produce, perfect_produce) == truth_WORD);
  return 0;
}
const char *quiz_derive_paragraph_quote_phrase() {
  NewTextPhrase(input, 6, 
      "hnucgina tyutzrondolyatkyitksuh prah");
  input.begin += 3;
  input.length -= 3;
  struct Phrase paragraph_quote = derive_paragraph_quote_phrase(input);
  mu_assert("", paragraph_quote.length == 0x20);
  mu_assert("", paragraph_quote.begin == 0x10);
  return 0;
}

const char *quiz_number_to_text_translate() {
  NewTextPad(text, 0x30);
  text = number_to_text_translate(0x24, 0x10, text);
  NewText(output, "tyutksas");
  mu_assert("", text_compare(text, output) == truth_WORD);
  return 0;
}

const char *quiz_paragraph_letter_quote_translate() {
  // fill me
  NewTextPhrase(input, 6, 
      "hnucgina tyutzrondolyatkyitksuh prah");
  input.begin += 0x10;
  input.length = 0x20;
  NewTextPad(text_pad, 0x20);
  struct Text produce = paragraph_letter_quote_translate(input, text_pad);
  NewText(example, "tyutzrondo lyatkyitksuh ");
  text_print(example);
  text_print(produce);
  mu_assert("", text_compare(produce, example) == truth_WORD);
  return 0;
}
const char *quiz_find_letter_quote() {
  NewText(input, "zi.prih.bonjour.prih.zi");
  //input.letters += 2;
  //input.length -= 2;
  struct Text produce = find_letter_quote(input);
  text_print(produce);
  mu_assert("", produce.length > 0);
  mu_assert("", produce.length <= input.length);
  return 0;
}
const char *quiz_find_letter_quote_null() {
  NewText(input, "pyacli");
  //input.letters += 2;
  //input.length -= 2;
  struct Text produce = find_letter_quote(input);
  text_print(produce);
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_find_letter_quote_null2() {
  NewText(input, "");
  struct Text produce = find_letter_quote(input);
  text_print(produce);
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_find_letter_quote_1() {
  NewText(input, "zi.prih.bonjour.prih.zi pyacli");
  NewText(example, "zi.prih.bonjour.prih.zi");
  struct Text produce = find_letter_quote(input);
  text_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length > 0);
  mu_assert("", text_compare(produce, example) == truth_WORD);
  mu_assert("", produce.length == 23);
  return 0;
}
const char *quiz_find_letter_quote_contents() {
  NewText(input, "zi.prih.bonjour.prih.zi pyacli");
  NewText(example, "bonjour");
  struct Text produce = find_letter_quote_contents(input);

  text_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length > 0);
  mu_assert("", text_compare(produce, example) == truth_WORD);
  mu_assert("", produce.length == 7);
  return 0;
}
const char *quiz_process_letter_quote() {
  NewText(input, "zi.prih.b.prih.zi");
  NewPagePhrase(phrase, 1);
  uint8_t text_indexFinger = 0;
  struct Phrase produce = process_letter_quote(phrase, input, &text_indexFinger);
  phrase_print(produce);
  DEBUGPRINT(("%X text_indexFinger\n", text_indexFinger));
  mu_assert("", produce.length > 0);
  mu_assert("", text_indexFinger >= 17);
  mu_assert("", phrase_word_read(produce, 0) == LETTER_QUOTE);
  mu_assert("", phrase_word_read(produce, 1) == 'b');
  return 0;
}

const char *quiz_letter_quote() {
  mu_run_quiz(quiz_find_letter_quote);
  mu_run_quiz(quiz_find_letter_quote_null);
  mu_run_quiz(quiz_find_letter_quote_null2);
  mu_run_quiz(quiz_find_letter_quote_1);
  mu_run_quiz(quiz_find_letter_quote_contents);
  mu_run_quiz(quiz_process_letter_quote);
  return 0;
}

const char *quiz_page_translate() {
  mu_run_quiz(quiz_page_translate_basic);
  mu_run_quiz(quiz_page_translate_multiarg);
  mu_run_quiz(quiz_page_translate_number);
  mu_run_quiz(quiz_number_to_text_translate);
  mu_run_quiz(quiz_paragraph_letter_quote_translate);
  mu_run_quiz(quiz_derive_paragraph_quote_phrase);
  mu_run_quiz(quiz_page_translate_paragraph_quote);
  return 0;
}

const char *quiz_translate() {
  mu_run_quiz(quiz_page_translate);
  return 0;
}
const char *quiz_ksim_encoding_basic() {
  NewTextPage(ksim, 1, "doyi tyutplos ksuh");
  Page_print(ksim);
  line_t perfect_tablet[1] = {
    {0x49, 0x881D, 0x0, 0x207E, 0xAA0A, 0x8C64, 0x5217}};
  mu_assert("", tablet_compare(ksim.plength, ksim.lines, 1, perfect_tablet) ==
      truth_WORD);
  return 0;
}

const char *quiz_ksim_encoding_two_param() {
  NewTextPage(ksim, 1, "doyi doka tyutplos ksuh");
  Page_print(ksim);
  line_t perfect_tablet[1] = {{0x249, 0x881D, 0x0, 0x207E, 0x881D, 0x0, 0x245E,
    0xAA0A, 0x8C64, 0x5217}};
  tablet_print(1, perfect_tablet);
  mu_assert("", tablet_compare(ksim.plength, ksim.lines, 1, perfect_tablet) ==
      truth_WORD);
  return 0;
}

const char *quiz_ksim_encoding() {
  mu_run_quiz(quiz_ksim_encoding_basic);
  mu_run_quiz(quiz_ksim_encoding_two_param);
  return 0;
}

const char *quiz_ksim_translate_basic() {
  NewTextPhrase(ksim, 1, "doyi tyutplus ksuh");
  NewText(perfect_produce, "zrondoyi tyutplusksuh ");
  NewTextPad(produce_pad, 0x100);
  produce_pad = page_translate(ksim.page, fluent_WORD, produce_pad);
  phrase_print(ksim);
  DEBUGPRINT(("%s\n", produce_pad.letters));
  mu_assert("", text_compare(perfect_produce, produce_pad) == truth_WORD);
  return 0;
}

const char *quiz_ksim_translate_two_param() {
  NewTextPhrase(ksim, 1, "doyi doka tyutplus ksuh");
  NewText(perfect_produce, "zrondoyi zrondoka tyutplusksuh ");
  NewTextPad(produce_pad, 0x100);
  produce_pad = page_translate(ksim.page, fluent_WORD, produce_pad);
  phrase_print(ksim);
  DEBUGPRINT(("%s\n", produce_pad.letters));
  mu_assert("", text_compare(perfect_produce, produce_pad) == truth_WORD);
  return 0;
}

const char *quiz_ksim_translate() {
  mu_run_quiz(quiz_ksim_translate_basic);
  mu_run_quiz(quiz_ksim_translate_two_param);
  return 0;
}
const char *quiz_ksim() {
  mu_run_quiz(quiz_ksim_encoding);
  mu_run_quiz(quiz_ksim_translate);
  mu_run_quiz(quiz_retrospective_recipe_ksim_found);
  mu_run_quiz(quiz_forward_finally_found);
  mu_run_quiz(quiz_retrospective_recipe_found);
  return 0;
}

const char *quiz_conditional_encoding_basic() {
  NewTextPage(input, 1, "cuplustu");
  Page_print(input);
  line_t perfect_tablet[1] = {{0xB, 0x29BE, 0x8A64, 0x295E}};
  mu_assert("", tablet_compare(input.plength, input.lines, 1, perfect_tablet) ==
      truth_WORD);
  return 0;
}
const char *quiz_conditional_encoding_basic2() {
  NewTextPage(input, 1, "hyikdopwih prifgika djancu plustu");
  Page_print(input);
  line_t perfect_tablet[1] = {{0x0549, 0x881D, 0x0001, 0x4127, 0xC8C4, 0x225E,
    0x245E, 0x69B3, 0x29BE, 0x8A64, 0x295E}};
  mu_assert("", tablet_compare(input.plength, input.lines, 1, perfect_tablet) ==
      truth_WORD);
  return 0;
}

const char *quiz_conditional_encoding() {
  mu_run_quiz(quiz_conditional_encoding_basic);
  mu_run_quiz(quiz_conditional_encoding_basic2);
  return 0;
}

p2p_quiz(quiz_conditional_giant_basic,
    "hyikdopwih tyindoka djancu"
    "hyikdoka tyindoyu plustu ",
    "ksasdoka li ", "");
p2p_quiz(quiz_conditional_giant_basic2,
    "ksasdopwih tyindoka djancu"
    "hyikdoka tyindoyu plustu ",
    "", "");
p2p_quiz(quiz_conditional_giant_letter,
    "hyikdopwih zi.prih.A.prih.zika djancu"
    "hyikdoka tyindoyu plustu ",
    "ksasdoka li ", "");
p2p_quiz(quiz_conditional_giant_letter2,
    "lyatgina zi.prih.A.prih.zikali hyikdopwih lyatgika djancu"
    "hyikdoka tyindoyu plustu ",
    "lyatgina zi.prih.A.prih.zika li ksasdoka li ", "");
const char *quiz_conditional_giant_letter3() {
  NewTextPhrase(input, 2, "lyatgina zi.prih.A.prih.zikali ");
  NewTextPhrase(program, 2,
      "hyikdopwih lyatgika djancu hyikdoka tyindoyu plustu ");
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(input, program, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdoka li ") == truth_WORD);
  return 0;
}
const char *quiz_conditional_giant_letter3_no() {
  NewTextPhrase(input, 2, "lyatgina zi.prih.A.prih.zikali ");
  NewTextPhrase(program, 2,
      "hpethpetdopwih lyatgika djancu hyikdoka tyindoyu plustu ");
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(input, program, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_conditional_giant_letter4() {
  NewTrainingSequence(training_sequence, 1, "lyatgina zi.prih.A.prih.zikali ",
      "lyatgina zi.prih.a.prih.zikali ");
  NewTextPhrase(program, 2,
      "hyikdopwih lyatgika djancu hyikdoka tyindoyu plustu ");
  NewPagePhrase(produce, 4);
  produce = program_quiz(training_sequence, program, produce);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "ksasdoka li ") == truth_WORD);
  return 0;
}
const char *quiz_conditional_giant_letter4_no() {
  NewTrainingSequence(training_sequence, 1, "lyatgina zi.prih.A.prih.zikali ",
      "lyatgina zi.prih.a.prih.zikali ");
  NewTextPhrase(program, 2,
      "hpethpetdopwih lyatgika djancu hyikdoka tyindoyu plustu ");
  NewPagePhrase(produce, 4);
  produce = program_quiz(training_sequence, program, produce);
  text_phrase_print(produce);
  text_page_print(produce.page);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  mu_assert("", produce.length == 0);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_conditional_giant_letter5() {
  NewTrainingSequence(training_sequence, 1, "lyatgina zi.prih.a.prih.zikali ",
      "psasgina zi.prih.A.prih.zikali ");
  NewTextPhrase(program, 3,
      "tyutdopwih lyatgika djancu "
      "tyutzrondoyu psasgiyi lyatgika grettu ");
  NewPagePhrase(produce, 6);
  produce = program_quiz(training_sequence, program, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
        produce, "psasgina zi.prih.A.prih.zikali") == truth_WORD);
  return 0;
}
const char *quiz_conditional_giant_letter6() {
  NewLongTrainingSequence(training_sequence, 8, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.m.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.!.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.0.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.Z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.A.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.`.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.M.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.Z.prih.zikali "
                      );
  NewTextPhrase(program, 3,
      "hliszrondopwih lyatgika djancu blu7ngika gruttu"
      "blu7ngika cu tyutzrondoyu psasgiyi lyatgika grettu");
  NewPagePhrase(produce, 6);
  produce = program_quiz(training_sequence, program, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
        produce, 
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.M.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.Z.prih.zikali "
        ) == truth_WORD);
  return 0;
}

const char *quiz_conditional_giant() {
  // mu_assert("TODO quiz_conditional_giant_basic", 1 == 0);
  mu_run_quiz(quiz_conditional_giant_basic);
  // mu_assert("TODO quiz_conditional_giant_basic2", 1 == 0);
  mu_run_quiz(quiz_conditional_giant_basic2);
  mu_run_quiz(quiz_conditional_giant_letter);
  mu_run_quiz(quiz_conditional_giant_letter2);
  mu_run_quiz(quiz_conditional_giant_letter3);
  mu_run_quiz(quiz_conditional_giant_letter3_no);
  mu_run_quiz(quiz_conditional_giant_letter4);
  mu_run_quiz(quiz_conditional_giant_letter4_no);
  // mu_assert("#TODO quiz_conditional_giant_letter5", 1 == 0);
  mu_run_quiz(quiz_conditional_giant_letter5);
  mu_run_quiz(quiz_conditional_giant_letter6);
  return 0;
}

const char *quiz_conditionalMood_interpret_basic() {
  NewTextPhrase(knowledge, 1, "");
  NewTextPhrase(input, 2,
      "hyikdopwih tyindoka djancu hyikdoka tyindoyu plustu");
  NewPagePhrase(produce, 2);
  struct Phrase input_remainder = conditionalMood_interpret(knowledge, input);
  mu_assert("", input_remainder.begin > input.begin &&
      input_remainder.length < input.length);
  DEBUGPRINT(("%s\n", "input"));
  phrase_print(input);
  DEBUGPRINT(("%s\n", "input_remainder"));
  phrase_print(input_remainder);
  mu_assert("", phrase_letters_compare(
        input_remainder, "hyikdoka tyindoyu plustu") == truth_WORD);
  return 0;
}
const char *quiz_conditionalMood_interpret_basic2() {
  NewTextPhrase(knowledge, 1, "");
  NewTextPhrase(input, 2,
      "tyindopwih hyikdoka djancu hyikdoka tyindoyu plustu");
  NewPagePhrase(produce, 2);
  struct Phrase input_remainder = conditionalMood_interpret(knowledge, input);
  DEBUGPRINT(("%X input_remainder.begin, %X input_remainder.length\n",
        input_remainder.begin, input_remainder.length));
  phrase_print(input_remainder);
  mu_assert("", input_remainder.begin > input.begin &&
      input_remainder.length < input.length);
  mu_assert("", phrase_letters_compare(input_remainder, "") == truth_WORD);
  return 0;
}
const char *quiz_conditionalMood_interpret_basic3() {
  NewTextPhrase(knowledge, 1, "hcutgina cyinbluhka li");
  NewTextPhrase(input, 1, "tyindopwih hyikdoka djancu hcutgika gruttu");
  NewPagePhrase(produce, 2);
  Page_print(knowledge.page);
  struct Phrase input_remainder = conditionalMood_interpret(knowledge, input);
  DEBUGPRINT(("%X input_remainder.begin, %X input_remainder.length\n",
        input_remainder.begin, input_remainder.length));
  phrase_print(input_remainder);
  mu_assert("", input_remainder.begin > input.begin &&
      input_remainder.length < input.length);
  mu_assert("", phrase_letters_compare(input_remainder, "") == truth_WORD);

  return 0;
}
const char *quiz_conditionalMood_interpret_basic_lie() {
  NewTextPhrase(knowledge, 1, "hcutgina cyinbluhka li");
  NewTextPhrase(input, 1, "cyinbluhkacu hcutgika gruttu");
  NewPagePhrase(produce, 2);
  Page_print(knowledge.page);
  struct Phrase input_remainder = conditionalMood_interpret(knowledge, input);
  DEBUGPRINT(("%X input_remainder.begin, %X input_remainder.length\n",
        input_remainder.begin, input_remainder.length));
  phrase_print(input_remainder);
  mu_assert("", input_remainder.begin > input.begin &&
      input_remainder.length < input.length);
  mu_assert("", phrase_letters_compare(input_remainder, "") == truth_WORD);

  return 0;
}
const char *quiz_conditionalMood_interpret_basic_truth() {
  NewTextPhrase(knowledge, 1, "hcutgina syanbluhka li");
  NewTextPhrase(input, 1, "syanbluhkacu hcutgika gruttu");
  NewPagePhrase(produce, 2);
  Page_print(knowledge.page);
  struct Phrase input_remainder = conditionalMood_interpret(knowledge, input);
  DEBUGPRINT(("%X input_remainder.begin, %X input_remainder.length\n",
        input_remainder.begin, input_remainder.length));
  phrase_print(input_remainder);
  mu_assert("", input_remainder.begin > input.begin &&
      input_remainder.length < input.length);
  mu_assert("", phrase_letters_compare(input_remainder, "hcutgika gruttu") ==
      truth_WORD);

  return 0;
}
const char *quiz_conditionalMood_interpret() {
  mu_run_quiz(quiz_conditionalMood_interpret_basic_lie);
  mu_run_quiz(quiz_conditionalMood_interpret_basic_truth);
  mu_run_quiz(quiz_conditionalMood_interpret_basic);
  mu_run_quiz(quiz_conditionalMood_interpret_basic2);
  mu_run_quiz(quiz_conditionalMood_interpret_basic3);
  return 0;
}

const char *quiz_conditionalMood_in_deonticMood_interpret_basic() {
  NewTextPhrase(input, 2,
      "hyikdopwih tyindoka djancu"
      "hyikdoka tyindoyu plustu ");
  phrase_print(input);
  NewPagePhrase(produce, 1);
  produce = deonticMood_interpret(input, input, produce);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdoka li") == truth_WORD);
  return 0;
}
const char *quiz_conditionalMood_in_deonticMood_interpret_basic2() {
  NewTextPhrase(input, 2,
      "ksasdopwih tyindoka djancu"
      "hyikdoka tyindoyu plustu ");
  DEBUGPRINT(("%X input.begin\n", input.begin));
  phrase_print(input);
  NewPagePhrase(produce, 1);
  produce = deonticMood_interpret(input, input, produce);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}

const char *quiz_conditionalMood_in_deonticMood_interpret() {
  mu_run_quiz(quiz_conditionalMood_in_deonticMood_interpret_basic);
  mu_run_quiz(quiz_conditionalMood_in_deonticMood_interpret_basic2);
  return 0;
}

const char *quiz_conditional() {
  mu_run_quiz(quiz_conditional_encoding);
  mu_run_quiz(quiz_conditionalMood_interpret);
  // mu_run_quiz(math_one_plus_one);
  mu_run_quiz(quiz_conditionalMood_in_deonticMood_interpret);
  mu_run_quiz(quiz_conditional_giant);
  return 0;
}

const char *quiz_hollow_encoding() {
  NewTextPhrase(hollow, 1, "");
  phrase_print(hollow);
  Page_print(hollow.page);
  DEBUGPRINT(("%X\n", hollow.page.lines[0][0]));
  mu_assert("", hollow.page.lines[0][0] == 0);
  return 0;
}

const char *quiz_final_consonant() {
  NewTextPhrase(input, 1, "hbophbothbokhboshbochbof");
  mu_assert("", phrase_letters_compare(input, "hbobhbodhboghbozhbojhbov") ==
      truth_WORD);
  return 0;
}

const char *quiz_text_encoding_basic() {
  NewText(input, "pyacli");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(input_page);
  text_phrase_print(produce);
  Page_print(produce.page);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == language_WORD);
  mu_assert("", produce.page.lines[0][2] == realis_mood_GRAMMAR);
  DEBUGPRINT(("%X produce.page.lines[1][0]\n",produce.page.lines[1][0]  ));
  mu_assert("", produce.page.lines[1][0] == 0);
  return 0;
}
const char *quiz_text_encoding_remainder() {
  NewText(input, "pyaclibali");
  NewPagePhrase(input_page, 1);
  uint16_t text_remainder;
  input_page = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(input_page);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 4);
  return 0;
}
const char *quiz_text_encoding_multisentence() {
  DEBUGPRINT(("start\n"));
  NewText(input, "pyaclihtafli");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == language_WORD);
  mu_assert("", produce.page.lines[0][2] == realis_mood_GRAMMAR);
  mu_assert("", produce.page.lines[1][1] == strength_WORD);
  mu_assert("", produce.page.lines[1][2] == realis_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}
const char *quiz_text_encoding_conditional() {
  DEBUGPRINT(("start\n"));
  NewText(input, "djancuhtafli");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == giant_WORD);
  mu_assert("", produce.page.lines[0][2] == conditional_mood_GRAMMAR);
  mu_assert("", produce.page.lines[0][3] == strength_WORD);
  mu_assert("", produce.page.lines[0][4] == realis_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}
const char *quiz_text_encoding_conditional1() {
  DEBUGPRINT(("start\n"));
  NewText(input, "lyatgikadjancuhtafli");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == letter_WORD);
  mu_assert("", produce.page.lines[0][2] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][3] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == giant_WORD);
  mu_assert("", produce.page.lines[0][5] == conditional_mood_GRAMMAR);
  mu_assert("", produce.page.lines[0][6] == strength_WORD);
  mu_assert("", produce.page.lines[0][7] == realis_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}
const char *quiz_text_encoding_conditional2() {
  DEBUGPRINT(("start\n"));
  NewText(input, "lyatgikadjancu lyatgika grettu");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == letter_WORD);
  mu_assert("", produce.page.lines[0][2] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][3] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == giant_WORD);
  mu_assert("", produce.page.lines[0][5] == conditional_mood_GRAMMAR);
  mu_assert("", produce.page.lines[0][6] == letter_WORD);
  mu_assert("", produce.page.lines[0][0x7] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][0x8] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][0x9] == subtract_WORD);
  mu_assert("", produce.page.lines[0][0xA] == deontic_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}
const char *quiz_text_encoding_conditional3() {
  DEBUGPRINT(("start\n"));
  NewText(input, "tyutdopwih lyatgikadjancu lyatgika grettu");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == NUMBER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == 2);
  mu_assert("", produce.page.lines[0][3] == ablative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == letter_WORD);
  mu_assert("", produce.page.lines[0][5] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][6] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][7] == giant_WORD);
  mu_assert("", produce.page.lines[0][8] == conditional_mood_GRAMMAR);
  mu_assert("", produce.page.lines[0][9] == letter_WORD);
  mu_assert("", produce.page.lines[0][0xA] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xB] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xC] == subtract_WORD);
  mu_assert("", produce.page.lines[0][0xD] == deontic_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}
const char *quiz_text_encoding_conditional4() {
  DEBUGPRINT(("start\n"));
  NewText(input, "tyutdopwih lyatgikadjancu tyutzrondoyu lyatgika grettu");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  Page_print(produce.page);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == NUMBER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == 2);
  mu_assert("", produce.page.lines[0][3] == ablative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == letter_WORD);
  mu_assert("", produce.page.lines[0][5] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][6] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][7] == giant_WORD);
  mu_assert("", produce.page.lines[0][8] == conditional_mood_GRAMMAR);
  mu_assert("", produce.page.lines[0][9] == NUMBER_QUOTE);
  mu_assert("", produce.page.lines[0][0xA] == 0x20);
  mu_assert("", produce.page.lines[0][0xB] == instrumental_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xC] == letter_WORD);
  mu_assert("", produce.page.lines[0][0xD] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xE] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xF] == subtract_WORD);
  mu_assert("", produce.page.lines[1][0x1] == deontic_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}
const char *quiz_text_encoding_conditional5() {
  DEBUGPRINT(("start\n"));
  NewText(input, "tyutdopwih lyatgikadjancu"
      "tyutzrondoyu psasgiyi lyatgika grettu");
  NewPagePhrase(input_page, 2);
  uint16_t text_remainder;
  struct Phrase produce = text_encoding(input, input_page, &text_remainder);
  text_phrase_print(produce);
  Page_print(produce.page);
  DEBUGPRINT(("%X text_remainder\n", text_remainder));
  mu_assert("", text_remainder == 0);
  mu_assert("", produce.page.lines[0][1] == NUMBER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == 2);
  mu_assert("", produce.page.lines[0][3] == ablative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == letter_WORD);
  mu_assert("", produce.page.lines[0][5] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][6] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][7] == giant_WORD);
  mu_assert("", produce.page.lines[0][8] == conditional_mood_GRAMMAR);
  mu_assert("", produce.page.lines[0][9] == NUMBER_QUOTE);
  mu_assert("", produce.page.lines[0][0xA] == 0x20);
  mu_assert("", produce.page.lines[0][0xB] == instrumental_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xC] == produce_WORD);
  mu_assert("", produce.page.lines[0][0xD] == name_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xE] == dative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][0xF] == letter_WORD);
  mu_assert("", produce.page.lines[1][0x1] == name_GRAMMAR);
  mu_assert("", produce.page.lines[1][0x2] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[1][0x3] == subtract_WORD);
  mu_assert("", produce.page.lines[1][0x4] == deontic_mood_GRAMMAR);
  mu_assert("", text_remainder == 0);
  return 0;
}

const char *quiz_text_encoding() {
  mu_run_quiz(quiz_text_encoding_basic);
  mu_run_quiz(quiz_text_encoding_remainder);
  mu_run_quiz(quiz_text_encoding_multisentence);
  mu_run_quiz(quiz_text_encoding_conditional);
  mu_run_quiz(quiz_text_encoding_conditional1);
  mu_run_quiz(quiz_text_encoding_conditional2);
  mu_run_quiz(quiz_text_encoding_conditional3);
  mu_run_quiz(quiz_text_encoding_conditional4);
  mu_run_quiz(quiz_text_encoding_conditional5);
  return 0;
}

const char *quiz_number_0() {
  NewTextPhrase(number, 1, "zrondokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_1() {
  NewTextPhrase(number, 1, "hyikdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 1);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_2() {
  NewTextPhrase(number, 1, "tyutdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 2);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_3() {
  NewTextPhrase(number, 1, "tyindokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 3);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_4() {
  NewTextPhrase(number, 1, "ksasdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 4);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_5() {
  NewTextPhrase(number, 1, "hfakdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 5);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_6() {
  NewTextPhrase(number, 1, "hlisdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 6);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_7() {
  NewTextPhrase(number, 1, "hsipdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 7);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_8() {
  NewTextPhrase(number, 1, "hwapdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 8);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_9() {
  NewTextPhrase(number, 1, "twundokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 9);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_A() {
  NewTextPhrase(number, 1, "htipdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0xA);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_B() {
  NewTextPhrase(number, 1, "slendokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0xB);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_C() {
  NewTextPhrase(number, 1, "tfatdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0xC);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_D() {
  NewTextPhrase(number, 1, "tsesdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0xD);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_E() {
  NewTextPhrase(number, 1, "hsesdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0xE);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_F() {
  NewTextPhrase(number, 1, "hpetdokali");
  Page_print(number.page);
  DEBUGPRINT(("%X \n", number.page.lines[0][2]));
  mu_assert("", number.page.lines[0][2] == 0xF);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(number, text, fluent_WORD);
  mu_assert("", phrase_letters_compare(number, text.letters) == truth_WORD);
  return 0;
}
const char *quiz_number_encoding() {
  mu_run_quiz(quiz_number_0);
  mu_run_quiz(quiz_number_1);
  mu_run_quiz(quiz_number_2);
  mu_run_quiz(quiz_number_3);
  mu_run_quiz(quiz_number_4);
  mu_run_quiz(quiz_number_5);
  mu_run_quiz(quiz_number_6);
  mu_run_quiz(quiz_number_7);
  mu_run_quiz(quiz_number_8);
  mu_run_quiz(quiz_number_9);
  mu_run_quiz(quiz_number_A);
  mu_run_quiz(quiz_number_B);
  mu_run_quiz(quiz_number_C);
  mu_run_quiz(quiz_number_D);
  mu_run_quiz(quiz_number_E);
  mu_run_quiz(quiz_number_F);
  return 0;
}
const char *quiz_independentClause_encoding_basic1() {
  NewTextPhrase(input, 4, "cali sana hwat clacri");
  mu_assert("", input.page.lines[2][0] == 0);
  return 0;
}
const char *quiz_independentClause_encoding() {
  mu_run_quiz(quiz_independentClause_encoding_basic1);
  return 0;
}
const char *quiz_neo_independentClause_encoding_basic1() {
  NewText(input, "pyacli");
  NewPagePhrase(produce, 2);
  uint16_t text_remainder = 0;
  produce = neo_independentClause_encoding(input, produce, &text_remainder);
  mu_assert("", produce.page.lines[1][0] == 0);
  mu_assert("", produce.page.lines[0][1] == language_WORD);
  mu_assert("", produce.page.lines[0][2] == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_neo_independentClause_encoding_conditional() {
  NewText(input, "cuplustu");
  NewPagePhrase(produce, 2);
  uint16_t text_remainder = 0;
  produce = neo_independentClause_encoding(input, produce, &text_remainder);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", produce.page.lines[1][0] == 0);
  mu_assert("", produce.page.lines[0][0] == 0xB);
  mu_assert("", produce.page.lines[0][1] == 0x29BE);
  mu_assert("", produce.page.lines[0][2] == plus_WORD);
  mu_assert("", produce.page.lines[0][3] == deontic_mood_GRAMMAR);
  return 0;
}
const char *quiz_neo_independentClause_encoding_number() {
  NewText(input, "hyikdokali");
  NewPagePhrase(produce, 2);
  uint16_t text_remainder = 0;
  produce = neo_independentClause_encoding(input, produce, &text_remainder);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", produce.page.lines[1][0] == 0);
  mu_assert("", produce.page.lines[0][2] == 1);
  mu_assert("", produce.page.lines[0][3] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_neo_independentClause_encoding_sentence() {
  NewText(input, "hyikdokaplustu");
  NewPagePhrase(produce, 2);
  uint16_t text_remainder = 0;
  produce = neo_independentClause_encoding(input, produce, &text_remainder);
  mu_assert("", produce.page.lines[1][0] == 0);
  mu_assert("", produce.page.lines[0][2] == 1);
  mu_assert("", produce.page.lines[0][3] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == plus_WORD);
  mu_assert("", produce.page.lines[0][5] == deontic_mood_GRAMMAR);
  return 0;
}
const char *quiz_neo_independentClause_encoding_letter() {
  DEBUGPRINT();
  NewText(input, "zi.prih.a.prih.zikali");
  NewPagePhrase(produce, 2);
  uint16_t text_remainder = 0;
  produce = neo_independentClause_encoding(input, produce, &text_remainder);
  Page_print(produce.page);
  mu_assert("", produce.page.lines[1][0] == 0);
  mu_assert("", produce.page.lines[0][1] == LETTER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == 'a');
  mu_assert("", produce.page.lines[0][3] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][4] == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_neo_independentClause_encoding() {
  mu_run_quiz(quiz_neo_independentClause_encoding_basic1);
  mu_run_quiz(quiz_neo_independentClause_encoding_conditional);
  mu_run_quiz(quiz_neo_independentClause_encoding_number);
  mu_run_quiz(quiz_neo_independentClause_encoding_sentence);
  mu_run_quiz(quiz_neo_independentClause_encoding_letter);
  return 0;
}

const char *quiz_letterquote_encoding_single() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.zi");
  Page_print(input.page);
  guarantee(LETTER_QUOTE != NUMBER_QUOTE);
  mu_assert("", input.page.lines[0][1] == LETTER_QUOTE);
  mu_assert("", input.page.lines[0][2] == 'a');
  mu_assert("",
      phrase_letters_compare(input, "zi.prih.a.prih.zi") == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_letterquote_encoding_variable() {
  NewTextPhrase(input, 1, "hnucgina zi.prih.a.prih.zika li");
  Page_print(input.page);
  text_phrase_print(input);
  guarantee(LETTER_QUOTE != NUMBER_QUOTE);
  mu_assert("", input.page.lines[0][4] == LETTER_QUOTE);
  mu_assert("", input.page.lines[0][5] == 'a');
  mu_assert("", phrase_letters_compare(
        input, "hnucgina zi.prih.a.prih.zika li") == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_letterquote_encoding_boundary() {
  NewTextPhrase(input, 2,
      "tyutdopwih zi.prih.a.prih.zika djancu tyutzrondoyu "
      "psasgiyi zi.prih.a.prih.zika grettu");
  Page_print(input.page);
  text_phrase_print(input);
  struct Phrase compare_phrase = input;
  compare_phrase.begin = 0x11;
  compare_phrase.length = 2;
  guarantee(LETTER_QUOTE != NUMBER_QUOTE);
  mu_assert("", input.page.lines[1][1] == LETTER_QUOTE);
  mu_assert("", input.page.lines[1][2] == 'a');
  text_phrase_print(compare_phrase);
  mu_assert("", phrase_letters_compare(compare_phrase, "zi.prih.a.prih.zi") ==
      truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_letterquote_encoding() {
  mu_run_quiz(quiz_letter_quote);
  mu_run_quiz(quiz_letterquote_encoding_single);
  mu_run_quiz(quiz_letterquote_encoding_variable);
  mu_run_quiz(quiz_letterquote_encoding_boundary);
  return 0;
}
const char *quiz_paragraph_quote_encoding_2() {
  NewTextPhrase(input, 10, "hnimgina ksaszrondo lyatkyitksuh "
      "zi.prih."
      "The goal is not to be better than the other man, but your previous self.\n"
      "To conquer oneself is a greater victory than to conquer thousands in a battle.\n"
      "My religion is very simple. My religion is kindness.\n"
      "The roots of all goodness lie in the soil of appreciation for goodness.\n"
      "-- Dalai Lama"
      ".prih.zi"
      "prah");
  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", input.page.lines[0][1] == name_WORD);
  mu_assert("", input.page.lines[0][2] == name_GRAMMAR);
  mu_assert("", input.page.lines[0][3] == nominative_case_GRAMMAR);
  mu_assert("", input.page.lines[0][4] == PARAGRAPH_LETTER_QUOTE);
  mu_assert("", input.page.lines[0][5] == 0x20);
  mu_assert("", input.page.lines[0][6] == declarative_mood_GRAMMAR);
  mu_assert("", input.page.lines[1][0] == 0);
  mu_assert("", input.page.lines[2][0] == 0);
  mu_assert("", input.page.lines[3][1] == paragraph_GRAMMAR);
  DEBUGPRINT(("%X input.length\n", input.length));
  mu_assert("", input.length >= 0x31);
  NewTextPhrase(compare_phrase, 4, "hnimgina tyutzrondo lyatkyitksuh prah");
  mu_assert("", phrase_to_phrase_compare(input, compare_phrase) == truth_WORD);
  Page_print(compare_phrase.page);
  mu_assert("", phrase_letters_compare(compare_phrase, 
      "zi.prih."
      "The goal is not to be better than the other man, but your previous self.\n"
      "To conquer oneself is a greater victory than to conquer thousands in a battle.\n"
      "My religion is very simple. My religion is kindness.\n"
      "The roots of all goodness lie in the soil of appreciation for goodness.\n"
      "-- Dalai Lama"
      ".prih.ziprah"
        ) == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_paragraph_quote_encoding_1() {
  NewTextPhrase(input, 11, "hnimgina hwapzrondo lyatkyitksuh "
      "zi.prih."
      "The goal is not to be better than the other man, but your previous self.\n"
      "-- Dalai Lama"
      ".prih.zi"
      "prah");
  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", input.page.lines[0][1] == name_WORD);
  mu_assert("", input.page.lines[0][2] == name_GRAMMAR);
  mu_assert("", input.page.lines[0][3] == nominative_case_GRAMMAR);
  mu_assert("", input.page.lines[0][4] == PARAGRAPH_LETTER_QUOTE);
  mu_assert("", input.page.lines[0][5] == 0x80);
  mu_assert("", input.page.lines[0][6] == declarative_mood_GRAMMAR);
  mu_assert("", input.page.lines[1][0] == 'T');
  mu_assert("", input.page.lines[1][1] == 'h');
  mu_assert("", input.page.lines[8][1] == paragraph_GRAMMAR);
  // DEBUGPRINT(("%X input.length\n", input.length));
  // mu_assert("", input.length >= 0x31);
  // NewTextPhrase(compare_phrase, 4, "hnimgina tyutzrondo lyatkyitksuh prah");
  // mu_assert("", phrase_to_phrase_compare(input, compare_phrase) == truth_WORD);
  // Page_print(compare_phrase.page);
  // mu_assert("", phrase_letters_compare(compare_phrase, 
  //     "zi.prih."
  //     "The goal is not to be better than the other man, but your previous self.\n"
  //     "To conquer oneself is a greater victory than to conquer thousands in a battle.\n"
  //     "My religion is very simple. My religion is kindness.\n"
  //     "The roots of all goodness lie in the soil of appreciation for goodness.\n"
  //     "-- Dalai Lama"
  //     ".prih.ziprah"
  //       ) == truth_WORD);
  // text_phrase_print(input);
  return 0;
}
const char *quiz_paragraph_quote_encoding_null() {
  NewTextPhrase(input, 4, "hnimgina tyutzrondo lyatkyitksuh prah");
  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", input.page.lines[0][1] == name_WORD);
  mu_assert("", input.page.lines[0][2] == name_GRAMMAR);
  mu_assert("", input.page.lines[0][3] == nominative_case_GRAMMAR);
  mu_assert("", input.page.lines[0][4] == PARAGRAPH_LETTER_QUOTE);
  mu_assert("", input.page.lines[0][5] == 0x20);
  mu_assert("", input.page.lines[0][6] == declarative_mood_GRAMMAR);
  mu_assert("", input.page.lines[1][0] == 0);
  mu_assert("", input.page.lines[2][0] == 0);
  mu_assert("", input.page.lines[3][1] == paragraph_GRAMMAR);
  DEBUGPRINT(("%X input.length\n", input.length));
  mu_assert("", input.length >= 0x31);
  NewTextPhrase(compare_phrase, 4, "hnimgina tyutzrondo lyatkyitksuh prah");
  mu_assert("", phrase_to_phrase_compare(input, compare_phrase) == truth_WORD);
  Page_print(compare_phrase.page);
  mu_assert("", phrase_letters_compare(compare_phrase, 
        "hnimgina tyutzrondo lyatkyitksuh prah") == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_addenda_theLetter_toThePhrase() {
  NewPagePhrase(phrase, 1);
  phrase.begin = 0;
  phrase.length = 0;
  struct Phrase produce = addenda_theLetter_toThePhrase('h', phrase);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", produce.page.lines[0][0] == 'h');
  mu_assert("", produce.length == 1);
  return 0;
}

const char *quiz_addenda_theText_toThePhrase() {
  NewText(text, "hello world");
  NewPagePhrase(phrase, 1);
  struct Phrase produce = addenda_theText_toThePhrase(text, phrase);
  text_print(text);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", produce.page.lines[0][0] == 'h');
  mu_assert("", produce.page.lines[0][1] == 'e');
  mu_assert("", produce.page.lines[0][2] == 'l');
  mu_assert("", produce.page.lines[0][3] == 'l');
  mu_assert("", produce.page.lines[0][4] == 'o');
  mu_assert("", produce.page.lines[0][5] == ' ');
  mu_assert("", produce.page.lines[0][6] == 'w');
  mu_assert("", produce.page.lines[0][7] == 'o');
  mu_assert("", produce.page.lines[0][8] == 'r');
  mu_assert("", produce.page.lines[0][9] == 'l');
  mu_assert("", produce.page.lines[0][0xA] == 'd');
  mu_assert("", produce.page.lines[0][0xB] == 0);
  mu_assert("", produce.length =  11);
  return 0;
}

const char *quiz_paragraph_quote_encoding() {
  mu_run_quiz(quiz_paragraph_quote_encoding_null);
  mu_run_quiz(quiz_addenda_theLetter_toThePhrase);
  mu_run_quiz(quiz_addenda_theText_toThePhrase);
  //TODO(mu_assert("#TODO quiz_paragraph_quote_encoding_1", 1==0));
  mu_run_quiz(quiz_paragraph_quote_encoding_1);
  mu_run_quiz(quiz_paragraph_quote_encoding_null);
  return 0;
}

const char *quiz_paragraph_encoding_1() {
  // the goal here is to have two paragraphs encoding next to each other
  // each will be encoded on their own line
  NewTextPhrase(input, 4, "prah prah");
  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", input.page.lines[0][1] == paragraph_GRAMMAR);
  mu_assert("", input.page.lines[1][1] == paragraph_GRAMMAR);
  NewTextPhrase(compare_phrase, 4, "prah prah");
  mu_assert("", phrase_to_phrase_compare(input, compare_phrase) == truth_WORD);
  Page_print(compare_phrase.page);
  mu_assert("", phrase_letters_compare(compare_phrase, "prah prah ") == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_paragraph_encoding_2() {
  // the goal here is to have two paragraphs encoding next to each other
  // each will be encoded on their own line
  // also each will have a sentence in the paragraph
  NewTextPhrase(input, 4, "zrondoka li prah hyikdoka li prah");
  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", input.page.lines[0][1] == NUMBER_QUOTE);
  mu_assert("", input.page.lines[0][2] == 0);
  mu_assert("", input.page.lines[0][3] == accusative_case_GRAMMAR);
  mu_assert("", input.page.lines[0][4] == realis_mood_GRAMMAR);
  mu_assert("", input.page.lines[1][1] == paragraph_GRAMMAR);
  mu_assert("", input.page.lines[2][1] == NUMBER_QUOTE);
  mu_assert("", input.page.lines[2][2] == 1);
  mu_assert("", input.page.lines[2][3] == accusative_case_GRAMMAR);
  mu_assert("", input.page.lines[2][4] == realis_mood_GRAMMAR);
  mu_assert("", input.page.lines[3][1] == paragraph_GRAMMAR);
  DEBUGPRINT(("%X input.length\n", input.length));
  mu_assert("", input.length >= 0x31);
  NewTextPhrase(compare_phrase, 4, "zrondoka li prah hyikdoka li prah");
  mu_assert("", phrase_to_phrase_compare(input, compare_phrase) == truth_WORD);
  Page_print(compare_phrase.page);
  mu_assert("", phrase_letters_compare(compare_phrase, 
        "zrondoka li prah hyikdoka li prah") == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_paragraph_encoding_3() {
  // the goal here is to have two paragraphs encoding next to each other
  // each will be encoded on their own line
  // also each will have a sentence in the paragraph
  NewTextPhrase(input, 2, "hnucgina tyutzrondolyatkyitksuh prah");

  Page_print(input.page);
  text_phrase_print(input);
  mu_assert("", input.page.lines[1][1] == number_WORD);
  mu_assert("", input.page.lines[1][2] == name_GRAMMAR);
  mu_assert("", input.page.lines[2][1] == paragraph_GRAMMAR);
  DEBUGPRINT(("%X input.length\n", input.length));
  mu_assert("", input.length >= 0x31);
  NewTextPhrase(compare_phrase, 4, "zrondoka li prah hyikdoka li prah");
  mu_assert("", phrase_to_phrase_compare(input, compare_phrase) == truth_WORD);
  Page_print(compare_phrase.page);
  mu_assert("", phrase_letters_compare(compare_phrase, 
        "zrondoka li prah hyikdoka li prah") == truth_WORD);
  text_phrase_print(input);
  return 0;
}
const char *quiz_paragraph_encoding() {
  mu_run_quiz(quiz_paragraph_encoding_1);
  mu_run_quiz(quiz_paragraph_encoding_2);
  return 0;
}


const char *quiz_encoding() {
  mu_run_quiz(quiz_text_encoding);
  mu_run_quiz(quiz_number_encoding);
  mu_run_quiz(quiz_hollow_encoding);
  mu_run_quiz(quiz_final_consonant);
  mu_run_quiz(quiz_letterquote_encoding);
  mu_run_quiz(quiz_independentClause_encoding);
  mu_run_quiz(quiz_neo_independentClause_encoding);
  return 0;
}

const char *quiz_neo_plus_interpret() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hyikdoka tyutdoyu plustu");
  Page_print(input.page);
  produce = neo_plus_interpret(knowledge, input, produce);
  Page_print(produce.page);
  phrase_print(produce);
  text_phrase_print(produce);
  mu_assert("",
      phrase_letters_compare(produce, "tyindoka li") == truth_WORD);
  return 0;
}

const char *quiz_neo_num_acc_ins_plus_interpret() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hyikdoka tyutdoyu plustu");
  produce = neo_num_acc_ins_plus_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "tyindoka li") == truth_WORD);
  return 0;
}

const char *quiz_dopwih_doka_giant_interpret_truth() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(test, 1, "syanbluhkali");
  text_phrase_print(test);
  Page_print(test.page);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hyikdopwih tyindoka djantu");
  produce = dopwih_doka_giant_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  DEBUGPRINT(
      ("%X produce.length, %X produce.begin\n", produce.length, produce.begin));
  mu_assert("", phrase_letters_compare(produce, "syanbluhka li") == truth_WORD);
  return 0;
}

const char *quiz_dopwih_doka_giant_interpret_lie() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "tyutdopwih hyikdoka djantu");
  produce = dopwih_doka_giant_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "cyinbluhka li") == truth_WORD);
  return 0;
}
const char *quiz_dopwih_doka_giant_interpret_giant() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 2, "hyikdopwih tyindoka djancu ");
  produce = dopwih_doka_giant_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "syanbluhka li") == truth_WORD);
  return 0;
}

const char *quiz_dopwih_doka_giant_interpret() {
  mu_run_quiz(quiz_dopwih_doka_giant_interpret_truth);
  mu_run_quiz(quiz_dopwih_doka_giant_interpret_lie);
  mu_run_quiz(quiz_dopwih_doka_giant_interpret_giant);
  return 0;
}

const char *quiz_base_command_interpret_plus_kayu() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hyikdoka tyutdoyu plustu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "tyindoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_plus_yugika_number() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hnucgika tyutdoyu plustu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina tyindoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_plus_yugika_letter() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina zi.prih.a.prih.zika li");
  NewTextPhrase(input, 1, "hnucgika tyutdoyu plustu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(
        produce, "hnucgina zi.prih.c.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_plus_yugika() {
  mu_run_quiz(quiz_base_command_interpret_plus_yugika_number);
  // mu_run_quiz(quiz_base_command_interpret_plus_yugika_letter);
  return 0;
}
const char *quiz_base_command_interpret_plus_yu() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina twundoka li hyikdokali");
  NewTextPhrase(input, 1, " tyutdoyu plustu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "tyindoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_plus_yuyika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, "hyikdoka tyutdoyu hnucgiyi plustu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hsipdoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_plus_yika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina tyindoka li");
  NewTextPhrase(input, 1, "hyikdoka hnucgiyi plustu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina ksasdoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_base_command_interpret_plus() {
  mu_run_quiz(quiz_base_command_interpret_plus_kayu);
  mu_run_quiz(quiz_base_command_interpret_plus_yu);
  mu_run_quiz(quiz_base_command_interpret_plus_yika);
  mu_run_quiz(quiz_base_command_interpret_plus_yuyika);
  mu_run_quiz(quiz_base_command_interpret_plus_yugika);
  mu_run_quiz(quiz_base_command_interpret_plus_yugika);
  //  mu_run_quiz(quiz_base_command_interpret_plus_variable3);
  return 0;
}
const char *quiz_base_command_interpret_subtract_kayu() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "tyindoka tyutdoyu grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hyikdoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract_yu() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li tyindoka li");
  NewTextPhrase(input, 1, "tyutdoyu grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hyikdoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract_yugika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hfakdoka li");
  NewTextPhrase(input, 1, "hnucgika tyutdoyu grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina tyindoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract_yuyika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, "tyindoka tyutdoyu hnucgiyi grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hfakdoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract_yuyika_letter() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina zi.prih..prih.zika li");
  NewTextPhrase(input, 1, "tyindoyu hnucgiyi zi.prih.d.prih.zika grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce) mu_assert(
      "", phrase_letters_compare(produce, "hnucgina zi.prih.a.prih.zika li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract_yuyika_letter0() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina zi.prih..prih.zika li");
  NewTextPhrase(input, 1, "zrondoyu psasgiyi zi.prih.a.prih.zika grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce) mu_assert(
      "", phrase_letters_compare(produce, "psasgina zi.prih.a.prih.zika li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract_yuyika_letter01() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina zi.prih.a.prih.zika li");
  NewTextPhrase(input, 1, "htipzrondoyu psasgiyi zi.prih.a.prih.zika grettu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce) mu_assert(
      "", phrase_letters_compare(produce, "psasgina zi.prih..prih.zika li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_subtract() {
  mu_run_quiz(quiz_base_command_interpret_subtract_kayu);
  mu_run_quiz(quiz_base_command_interpret_subtract_yugika);
  mu_run_quiz(quiz_base_command_interpret_subtract_yu);
  mu_run_quiz(quiz_base_command_interpret_subtract_yuyika);
  mu_run_quiz(quiz_base_command_interpret_subtract_yuyika_letter);
  mu_run_quiz(quiz_base_command_interpret_subtract_yuyika_letter0);
  mu_run_quiz(quiz_base_command_interpret_subtract_yuyika_letter01);
  return 0;
}
const char *quiz_base_command_interpret_multiply_kayu() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "tyindoka tyutdoyu hgoctu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hlisdoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_multiply_yu() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li tyindokali");
  NewTextPhrase(input, 1, " tyutdoyu hgoctu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hlisdoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_multiply_yugika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina tyindoka li");
  NewTextPhrase(input, 1, "hnucgika tyutdoyu hgoctu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hlisdoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_multiply_yuyika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, "tyindoka tyutdoyu hnucgiyi hgoctu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina htipdoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_multiply() {
  mu_run_quiz(quiz_base_command_interpret_multiply_kayu);
  mu_run_quiz(quiz_base_command_interpret_multiply_yu);
  mu_run_quiz(quiz_base_command_interpret_multiply_yugika);
  mu_run_quiz(quiz_base_command_interpret_multiply_yuyika);
  return 0;
}
const char *quiz_base_command_interpret_divide_yugika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hlisdoka li");
  NewTextPhrase(input, 1, "hnucgika tyutdoyu dvistu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina tyindoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_divide_yuyika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, "hlisdoka tyutdoyu hnucgiyi dvistu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hsipdoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_divide() {
  mu_run_quiz(quiz_base_command_interpret_divide_yugika);
  mu_run_quiz(quiz_base_command_interpret_divide_yuyika);
  return 0;
}
const char *quiz_base_command_interpret_xor_yugika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina tyindoka li");
  NewTextPhrase(input, 1, "hnucgika hfakdoyu hkittu");
  produce = base_command_interpret(knowledge, input, produce);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hlisdoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_xor_yidoka() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina tyindoka li");
  NewTextPhrase(input, 1, "hnucgiyi hpethpethpethpetdoka hkittu");
  produce = base_command_interpret(knowledge, input, produce);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(
        produce, "hnucgina hpethpethpettfatdoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_xor_yuyika() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, "hlisdoka tyutdoyu hnucgiyi hkittu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hwapdoka li") ==
      truth_WORD);
  return 0;
}

const char *quiz_maximize_variable_letter() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.zika");
  NewPagePhrase(produce, 1);
  produce = maximize_variable(input, produce);
  Page_print(produce.page);
  mu_assert("", produce.page.lines[0][2] == 0xFFFF);
  return 0;
}
const char *quiz_maximize_variable_number() {
  NewTextPhrase(input, 1, "hyikdoka");
  NewPagePhrase(produce, 1);
  produce = maximize_variable(input, produce);
  Page_print(produce.page);
  mu_assert("", produce.page.lines[0][2] == 0xFFFF);
  return 0;
}
const char *quiz_maximize_variable_boolean() {
  NewTextPhrase(input, 1, "cyinbluhka");
  NewPagePhrase(produce, 1);
  produce = maximize_variable(input, produce);
  Page_print(produce.page);
  mu_assert("", produce.page.lines[0][1] == truth_WORD);
  mu_assert("", phrase_letters_compare(produce, "syanbluhka"));
  return 0;
}
const char *quiz_maximize_variable_boolean_1() {
  NewTextPhrase(input, 1, "syanbluhka");
  NewPagePhrase(produce, 1);
  produce = maximize_variable(input, produce);
  Page_print(produce.page);
  mu_assert("", produce.page.lines[0][1] == truth_WORD);
  mu_assert("", phrase_letters_compare(produce, "syanbluhka"));
  return 0;
}
const char *quiz_maximize_variable() {
  mu_run_quiz(quiz_maximize_variable_letter);
  mu_run_quiz(quiz_maximize_variable_number);
  mu_run_quiz(quiz_maximize_variable_boolean);
  mu_run_quiz(quiz_maximize_variable_boolean_1);
  return 0;
}
const char *quiz_base_command_interpret_xor() {
  mu_run_quiz(quiz_maximize_variable);
  mu_run_quiz(quiz_base_command_interpret_xor_yidoka);
  mu_run_quiz(quiz_base_command_interpret_xor_yugika);
  mu_run_quiz(quiz_base_command_interpret_xor_yuyika);
  return 0;
}
const char *quiz_base_command_interpret_invert_gika_number() {
  NewPagePhrase(produce, 2);
  NewTextPhrase(knowledge, 2, "hnucgina tyintyintyintyindoka li");
  NewTextPhrase(input, 1, "hnucgika gruttu");
  NewTextPhrase(output, 1, "hnucgina tfattfattfattfatdoka li");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  Page_print(produce.page);
  phrase_print(produce);
  phrase_print(output);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
        produce, "hnucgina tfattfattfattfatdoka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_gika_boolean() {
  NewPagePhrase(produce, 2);
  NewTextPhrase(knowledge, 2, "hnucgina cyinbluhka li");
  NewTextPhrase(input, 1, "hnucgika gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  Page_print(produce.page);
  phrase_print(produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina syanbluhka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_gika_boolean_0() {
  NewPagePhrase(produce, 2);
  NewTextPhrase(knowledge, 2, "hnucgina syanbluhka li");
  NewTextPhrase(input, 1, "hnucgika gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  Page_print(produce.page);
  phrase_print(produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina cyinbluhka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_gika() {
  mu_run_quiz(quiz_base_command_interpret_invert_gika_number);
  mu_run_quiz(quiz_base_command_interpret_invert_gika_boolean);
  mu_run_quiz(quiz_base_command_interpret_invert_gika_boolean_0);
  return 0;
}
const char *quiz_base_command_interpret_invert_yidoka() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, "tyintyintyintyindoka hnucgiyi gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina"
        "tfattfattseszrondoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_yidoka1() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina ksasdoka li");
  NewTextPhrase(input, 1, " hnucgiyi tyintyintyintyindoka gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina"
        "tfattfattseszrondoka li") ==
      truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_yibluhka() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina cyinbluhka li");
  NewTextPhrase(input, 1, "cyinbluhka hnucgiyi gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina"
        "syanbluhka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_yibluhka_invert() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina syanbluhka li");
  NewTextPhrase(input, 1, "syanbluhka hnucgiyi gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina"
        "cyinbluhka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert_bluhgika() {
  NewPagePhrase(produce, 2);
  NewTextPhrase(knowledge, 2, "hnucgina syanbluhka li");
  NewTextPhrase(input, 1, "hnucgika gruttu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina"
        "cyinbluhka li") == truth_WORD);
  return 0;
}
const char *quiz_base_command_interpret_invert() {
  // mu_assert("#TODO  quiz_base_command_interpret_invert_gika", 1==0);
  mu_run_quiz(quiz_base_command_interpret_invert_gika);
  mu_run_quiz(quiz_base_command_interpret_invert_yidoka);
  mu_run_quiz(quiz_base_command_interpret_invert_yidoka1);
  mu_run_quiz(quiz_base_command_interpret_invert_yibluhka);
  mu_run_quiz(quiz_base_command_interpret_invert_yibluhka_invert);
  // mu_assert("#TODO  quiz_base_command_interpret_invert_bluhgika", 1==0);
  mu_run_quiz(quiz_base_command_interpret_invert_bluhgika);
  return 0;
}
const char *quiz_base_command_interpret_giant() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "tyutdopwih hyikdoka djantu");
  produce = base_command_interpret(knowledge, input, produce);
  Page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "cyinbluhka li") == truth_WORD);
  return 0;
}

const char *quiz_base_command_interpret() {
  mu_run_quiz(quiz_dat_plus_interpret);
  mu_run_quiz(quiz_neo_plus_interpret);
  mu_run_quiz(quiz_neo_num_acc_ins_plus_interpret);
  mu_run_quiz(quiz_dopwih_doka_giant_interpret);
  mu_run_quiz(quiz_base_command_interpret_plus);
  mu_run_quiz(quiz_base_command_interpret_giant);
  mu_run_quiz(quiz_base_command_interpret_subtract);
  mu_run_quiz(quiz_base_command_interpret_multiply);
  mu_run_quiz(quiz_base_command_interpret_divide);
  mu_run_quiz(quiz_base_command_interpret_xor);
  // mu_run_quiz(quiz_base_command_interpret_neg);
  mu_run_quiz(quiz_base_command_interpret_invert);
  return 0;
}

const char *bwitnweh_fyakka_ri_basic() {
  NewTextPhrase(input, 1, "pyacka");
  mu_assert("", bwitnweh_fyakka_ri(input) == truth_WORD);
  return 0;
}
const char *bwitnweh_fyakka_ri_lie() {
  NewTextPhrase(input, 1, "pyacka");
  input.begin = 0x10;
  mu_assert("", bwitnweh_fyakka_ri(input) == lie_WORD);
  return 0;
}

const char *quiz_bwitnweh_fyakka_ri() {
  mu_run_quiz(bwitnweh_fyakka_ri_basic);
  mu_run_quiz(bwitnweh_fyakka_ri_lie);
  return 0;
}

const char *quiz_is() {
  mu_run_quiz(quiz_bwitnweh_fyakka_ri);
  return 0;
}

const char *quiz_addenda_theGrammarWord_toThePhrase_basic() {
  NewPagePhrase(produce, 1);
  produce =
    addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", produce.page.lines[0][0] = 3);
  mu_assert("", phrase_letters_compare(produce, "ka") == truth_WORD);
  return 0;
}
const char *quiz_addenda_theGrammarWord_toThePhrase_basic2() {
  NewPagePhrase(produce, 1);
  produce =
    addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce =
    addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", produce.page.lines[0][0] = 7);
  mu_assert("", phrase_letters_compare(produce, "kaka") == truth_WORD);
  return 0;
}

const char *quiz_addenda_theLiteralWord_toThePhrase_basic() {
  NewPagePhrase(produce, 1);
  produce = addenda_theLiteralWord_toThePhrase(world_WORD, produce);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", produce.page.lines[0][0] = 1);
  mu_assert("", phrase_letters_compare(produce, "hwac") == truth_WORD);
  return 0;
}
const char *quiz_addenda_theLiteralWord_toThePhrase_basic2() {
  NewPagePhrase(produce, 1);
  produce = addenda_theLiteralWord_toThePhrase(world_WORD, produce);
  produce = addenda_theLiteralWord_toThePhrase(hello_GRAMMAR, produce);
  Page_print(produce.page);
  phrase_print(produce);
  mu_assert("", produce.page.lines[0][0] = 1);
  mu_assert("", phrase_letters_compare(produce, "hwacca") == truth_WORD);
  return 0;
}

const char *quiz_addenda_theLiteralWord_toThePhrase_basic3() {
  NewTextPhrase(produce, 2, "hyikdoli");
  NewTextPhrase(output, 2, "hyikdoli hwacca");
  produce.length = 0x10;
  produce = addenda_theLiteralWord_toThePhrase(world_WORD, produce);
  produce = addenda_theLiteralWord_toThePhrase(hello_GRAMMAR, produce);
  DEBUGPRINT(("%X output.begin, %X produce.begin, %X output.length, %X "
        "produce.length\n",
        output.begin, produce.begin, output.length, produce.length));
  text_phrase_print(output);
  text_phrase_print(produce);
  Page_print(produce.page);
  Page_print(output.page);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", produce.length == output.length);
  mu_assert("", phrase_word_read(produce, 0) == phrase_word_read(output, 0));
  mu_assert("",
      phrase_word_read(produce, 0xF) == phrase_word_read(output, 0xF));
  mu_assert("",
      phrase_word_read(produce, 0x10) == phrase_word_read(output, 0x10));
  mu_assert("",
      phrase_word_read(produce, 0x11) == phrase_word_read(output, 0x11));
  mu_assert("",
      phrase_word_read(produce, 0x10) == phrase_word_read(output, 0x10));
  return 0;
}

const char *quiz_addenda_theLiteralWord_toThePhrase() {
  mu_run_quiz(quiz_addenda_theLiteralWord_toThePhrase_basic);
  mu_run_quiz(quiz_addenda_theLiteralWord_toThePhrase_basic2);
  mu_run_quiz(quiz_addenda_theLiteralWord_toThePhrase_basic3);
  return 0;
}

const char *quiz_addenda_thePhrase_toThePhrase_basic() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(input, 1, "hwacca mina sawika pyanli");
  produce = fyakyi_fyakka_dva2ttu(produce, input);
  Page_print(input.page);
  phrase_print(input);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 9);
  mu_assert("", produce.begin == 1);
  mu_assert("", phrase_letters_compare(produce, "hwacca mina sawika pyanli") ==
      truth_WORD);
  return 0;
}

const char *quiz_addenda_thePhrase_toThePhrase_giant() {
  NewTextPhrase(input, 2,
      "hyikdopwih tyindoka djancu hyikdoka tyindoyu plustu");
  input.begin = input.length + input.begin - 3;
  input.length = 3;
  NewPagePhrase(produce, 1);
  produce = addenda_thePhrase_toThePhrase(input, produce);
  text_phrase_print(input);
  Page_print(input.page);
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", produce.length == 2);
  mu_assert("", phrase_letters_compare(produce, "plustu") == truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_multi() {
  NewTextPhrase(produce, 1, "hwacca mina");
  NewTextPhrase(input, 1, "sawika pyanli");
  produce = fyakyi_fyakka_dva2ttu(produce, input);
  Page_print(input.page);
  phrase_print(input);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 9);
  mu_assert("", produce.begin == 1);
  mu_assert("", phrase_letters_compare(produce, "hwacca mina sawika pyanli") ==
      truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_multi2() {
  NewTextPhrase(output, 3,
      "hyikdokali"
      "hyikdopwih tu");
  NewTextPhrase(produce, 3, "hyikdokali");
  NewTextPhrase(addenda, 1, "hyikdopwih tu");
  produce = fyakyi_fyakka_dva2ttu(produce, addenda);
  Page_print(output.page);
  phrase_print(output);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(produce.page, "hyikdokali hyikdopwihtu") ==
      truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_multi3() {
  NewTextPhrase(output, 3,
      "hyikdokali"
      "hyikdopwih tu");
  NewPagePhrase(produce, 2);
  NewTextPhrase(addenda, 2,
      "hyikdokali"
      "hyikdopwih tu");
  produce = fyakyi_fyakka_dva2ttu(produce, addenda);
  Page_print(output.page);
  phrase_print(output);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(produce.page, "hyikdokali hyikdopwihtu") ==
      truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_multi4() {
  NewTextPhrase(output, 3,
      "tyutdoka li"
      "hyakyana tyindoka li");
  NewTextPhrase(produce, 4, "tyutdoka li");
  NewTextPhrase(addenda, 2, "hyakyana tyindoka li");
  produce = fyakyi_fyakka_dva2ttu(produce, addenda);
  Page_print(output.page);
  phrase_print(output);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(produce.page, "tyutdoka li"
        "hyakyana tyindoka li"
        "") == truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_null() {
  NewPagePhrase(output, 1);
  NewPagePhrase(produce, 1);
  NewPagePhrase(addenda, 1);
  produce = fyakyi_fyakka_dva2ttu(produce, addenda);
  Page_print(output.page);
  phrase_print(output);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(produce.page, "") == truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_9() {
  DEBUGPRINT(("\n"));
  NewTextPhrase(output, 3, "hnucgina hyikdokali ksasdokali");
  NewTextPhrase(produce, 4, "hnucgina hyikdokali");
  NewTextPhrase(addenda, 2, "ksasdokali");
  produce.length = 0xF;
  produce = fyakyi_fyakka_dva2ttu(produce, addenda);
  Page_print(output.page);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(produce.page, "hnucgina hyikdoka li"
        "ksasdoka li"
        "") == truth_WORD);
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_quote_boundary() {
  DEBUGPRINT(("\n"));
  NewTextPhrase(output, 3,
      "tyutdopwih zi.prih.c.prih.zika djancu tyutdoyu psasgiyi"
      "zi.prih.c.prih.zi");
  NewTextPhrase(produce, 4,
      "tyutdopwih zi.prih.c.prih.zika djancu "
      "tyutdoyu psasgiyi");
  NewTextPhrase(addenda, 2, "zi.prih.c.prih.zi");
  produce.length = 0xE;
  Page_print(produce.page);
  produce = fyakyi_fyakka_dva2ttu(produce, addenda);
  Page_print(output.page);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(
        produce.page,
        "tyutdopwih zi.prih.c.prih.zika djancu tyutdoyu psasgiyi"
        "zi.prih.c.prih.zi") == truth_WORD);
  mu_assert("", produce.page.lines[1][1] = LETTER_QUOTE);
  mu_assert("", produce.page.lines[1][2] = 'c');
  return 0;
}

const char *quiz_addenda_thePhrase_toThePhrase_null_paragraph_quote() {
  DEBUGPRINT(("\n"));
  NewTextPhrase(input, 4,
      "hnucgina tyutzrondolyatkyitksuh prah"
      );
  NewPagePhrase(produce, 4);
  Page_print(input.page);
  produce = fyakyi_fyakka_dva2ttu(produce, input);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X input.length\n", input.length));
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", produce.length == input.length);
  mu_assert("", produce.begin == input.begin);
  mu_assert("", page_letters_compare(
        produce.page,
      "hnucgina tyutzrondolyatkyitksuh prah") == truth_WORD);
  //mu_assert("", produce.page.lines[1][1] = LETTER_QUOTE);
  //mu_assert("", produce.page.lines[1][2] = 'c');
  return 0;
}
const char *quiz_addenda_thePhrase_toThePhrase_paragraph_quote() {
  DEBUGPRINT(("\n"));
  NewTextPhrase(input, 4,
      "hnucgina tyutzrondolyatkyitksuh zi.prih.unconditional love.prih.zi prah"
      );
  NewPagePhrase(produce, 4);
  Page_print(input.page);
  produce = fyakyi_fyakka_dva2ttu(produce, input);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X input.length\n", input.length));
  text_phrase_print(produce);
  Page_print(produce.page);
  mu_assert("", produce.length == input.length);
  mu_assert("", produce.begin == input.begin);
  mu_assert("", page_letters_compare(
        produce.page,
      "hnucgina tyutzrondolyatkyitksuh zi.prih.unconditional love.prih.ziprah") 
        == truth_WORD);
  //mu_assert("", produce.page.lines[1][1] = LETTER_QUOTE);
  //mu_assert("", produce.page.lines[1][2] = 'c');
  return 0;
}

const char *quiz_addenda_thePhrase_toThePhrase() {
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_basic);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_giant);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_multi);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_multi2);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_multi3);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_multi4);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_null);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_9);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_null_paragraph_quote);
  //mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_paragraph_quote);
  // mu_run_quiz(quiz_addenda_thePhrase_toThePhrase_quote_boundary);
  return 0;
}
const char *quiz_grow_theIndexFinger_untilTheUpcomingLine() {
  txik_t txik = 0x8;
  txik = grow_theIndexFinger_untilTheUpcomingLine(txik);
  DEBUGPRINT(("%X txik\n", txik));
  mu_assert("", txik == 0x10);
  return 0;
}
const char *quiz_addenda_thePhraseWord_toThePhrase_basic() {
  NewTextPhrase(phraseWord, 1, "cwas");
  NewPagePhrase(produce, 1);
  produce = addenda_thePhraseWord_toThePhrase(phraseWord, produce);
  mu_assert("", produce.length == 1);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.page.lines[0][1] == begin_WORD);
  return 0;
}

const char *quiz_addenda_thePhraseWord_toThePhrase() {
  mu_run_quiz(quiz_addenda_thePhraseWord_toThePhrase_basic);
  return 0;
}
const char *quiz_is_theIndexfingerZLine_aPartialIndependentClause_true() {
  NewTextPhrase(input, 2,
      "hyikdocwasnahyikdocwaskahyikdocwasyicwasyucwashcanli");
  Page_print(input.page);
  mu_assert("", is_theIndexfingerZLine_aPartialIndependentClause(4, input) ==
      truth_WORD);
  return 0;
}

const char *quiz_is_theIndexfingerZLine_aPartialIndependentClause_lie() {
  NewTextPhrase(input, 1, "cwaccali");
  mu_assert("", is_theIndexfingerZLine_aPartialIndependentClause(4, input) ==
      lie_WORD);
  return 0;
}

const char *quiz_is_theIndexfingerZLine_aPartialIndependentClause() {
  mu_run_quiz(quiz_is_theIndexfingerZLine_aPartialIndependentClause_true);
  mu_run_quiz(quiz_is_theIndexfingerZLine_aPartialIndependentClause_lie);
  return 0;
}
const char *quiz_addenda_theGrammarWord_toThePhrase_zero() {
  NewPagePhrase(produce, 1);
  produce =
    addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.length == 1);
  mu_assert("", produce.page.lines[0][0] == 3);
  mu_assert("", produce.page.lines[0][1] == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_addenda_theGrammarWord_toThePhrase_one() {
  NewPagePhrase(produce, 1);
  produce =
    addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  produce =
    addenda_theGrammarWord_toThePhrase(accusative_case_GRAMMAR, produce);
  Page_print(produce.page);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.length == 2);
  mu_assert("", produce.page.lines[0][0] == 7);
  mu_assert("", produce.page.lines[0][1] == accusative_case_GRAMMAR);
  mu_assert("", produce.page.lines[0][2] == accusative_case_GRAMMAR);
  return 0;
}
const char *quiz_addenda_theGrammarWord_toThePhrase() {
  mu_run_quiz(quiz_addenda_theGrammarWord_toThePhrase_basic);
  mu_run_quiz(quiz_addenda_theGrammarWord_toThePhrase_basic2);
  mu_run_quiz(quiz_addenda_theGrammarWord_toThePhrase_zero);
  mu_run_quiz(quiz_addenda_theGrammarWord_toThePhrase_one);
  return 0;
}
const char *quiz_addenda_theWord_toThePhrase_zero() {
  NewPagePhrase(produce, 1);
  produce = addenda_theWord_toThePhrase(accusative_case_WORD, produce);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.length == 1);
  mu_assert("", produce.page.lines[0][0] == 1);
  mu_assert("", produce.page.lines[0][1] == accusative_case_WORD);
  return 0;
}
const char *quiz_addenda_theWord_toThePhrase_one() {
  NewPagePhrase(produce, 1);
  produce = addenda_theWord_toThePhrase(accusative_case_WORD, produce);
  produce = addenda_theWord_toThePhrase(accusative_case_WORD, produce);
  Page_print(produce.page);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.length == 2);
  mu_assert("", produce.page.lines[0][0] == 1);
  mu_assert("", produce.page.lines[0][1] == accusative_case_WORD);
  mu_assert("", produce.page.lines[0][2] == accusative_case_WORD);
  return 0;
}
const char *quiz_addenda_theWord_toThePhrase() {
  mu_run_quiz(quiz_addenda_theWord_toThePhrase_zero);
  mu_run_quiz(quiz_addenda_theWord_toThePhrase_one);
  return 0;
}
const char *
quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_truth() {
  NewTextPhrase(input, 1, "pyacli");
  mu_assert("", is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
        input) == truth_WORD);
  return 0;
}
const char *
quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_truth2() {
  NewTextPhrase(input, 1, "pyacli");
  input.length = 5;
  mu_assert("", is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
        input) == truth_WORD);
  return 0;
}
const char *
quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_truth3() {
  NewTextPhrase(input, 2, "pyacli pyacca");
  input.begin = 0xE;
  input.length = 1;
  mu_assert("", is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
        input) == truth_WORD);
  return 0;
}
const char *
quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_lie() {
  NewTextPhrase(input, 1, "pyacca");
  mu_assert("", is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
        input) == lie_WORD);
  return 0;
}
const char *
quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_lie2() {
  NewTextPhrase(input, 2, "pyacli pyacca");
  mu_assert("", is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
        input) == lie_WORD);
  return 0;
}
const char *
quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_lie3() {
  NewTextPhrase(input, 2, "pyacli pyacca");
  input.begin = 0x10;
  input.length = 1;
  mu_assert("", is_thePhraseZFinalIndependentClause_aDoneIndependentClause(
        input) == lie_WORD);
  return 0;
}

const char *quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause() {
  mu_run_quiz(
      quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_truth);
  mu_run_quiz(
      quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_truth2);
  mu_run_quiz(
      quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_truth3);
  mu_run_quiz(
      quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_lie);
  mu_run_quiz(
      quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_lie2);
  mu_run_quiz(
      quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause_lie3);
  return 0;
}

const char *quiz_addenda_aHollowIndependentClause_toThePhrase_basic() {
  NewPagePhrase(produce, 1);
  produce = addenda_aHollowIndependentClause_toThePhrase(produce);
  DEBUGPRINT(("0x%X produce.length\n", produce.length));
  DEBUGPRINT(("0x%X produce.begin\n", produce.begin));
  Page_print(produce.page);
  mu_assert("", produce.length == 0);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.page.lines[0][0] == 1);
  return 0;
}
const char *quiz_addenda_aHollowIndependentClause_toThePhrase_basic2() {
  NewTextPhrase(produce, 2, "pyacli");
  produce = addenda_aHollowIndependentClause_toThePhrase(produce);
  Page_print(produce.page);
  phrase_print(produce);
  DEBUGPRINT(("0x%X produce.length\n", produce.length));
  DEBUGPRINT(("0x%X produce.begin\n", produce.begin));
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.page.lines[1][0] == 1);
  mu_assert("", produce.length == 0x10);
  return 0;
}

const char *quiz_addenda_aHollowIndependentClause_toThePhrase() {
  mu_run_quiz(quiz_addenda_aHollowIndependentClause_toThePhrase_basic);
  mu_run_quiz(quiz_addenda_aHollowIndependentClause_toThePhrase_basic2);
  mu_run_quiz(quiz_addenda_aHollowIndependentClause_toThePhrase_basic2);
  return 0;
}

const char *quiz_quote_addenda_1() {
  DEBUGPRINT(("\n"));
  NewTextPhrase(output, 3,
      "tyutdopwih zi.prih.c.prih.zika djancu tyutdoyu psasgiyi"
      "zi.prih.c.prih.zi");
  NewTextPhrase(produce, 4,
      "tyutdopwih zi.prih.c.prih.zika djancu "
      "tyutdoyu psasgiyi");
  NewTextPhrase(addenda, 2, "zi.prih.c.prih.zi");
  produce.length = 0xE;
  Page_print(produce.page);
  produce = fyakyi_kyitka_dva2ttu(produce, addenda);
  Page_print(output.page);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  DEBUGPRINT(("%X output.length\n", output.length));
  text_phrase_print(produce);
  text_phrase_print(output);
  text_page_print(produce.page);
  mu_assert("", produce.length == output.length);
  mu_assert("", produce.begin == output.begin);
  mu_assert("", page_letters_compare(
        produce.page,
        "tyutdopwih zi.prih.c.prih.zika djancu tyutdoyu psasgiyi"
        "zi.prih.c.prih.zi") == truth_WORD);
  mu_assert("", produce.page.lines[1][1] = LETTER_QUOTE);
  mu_assert("", produce.page.lines[1][2] = 'c');
  return 0;
}

const char *quiz_quote_addenda() { mu_run_quiz(quiz_quote_addenda_1); return 0;}


const char *quiz_addenda_theParagraph_1() { 
  DEBUGPRINT(("tluk\n"));
  NewTextPhrase(input, 2, "prah");
  NewPagePhrase(produce, HTIN_LONG);
  produce = fyakyi_fyakka_dva2ttu(produce, input);
  Page_print(input.page);
  Page_print(produce.page);
  DEBUGPRINT(("%X input.length, %X produce.length\n", input.length, produce.length));
  mu_assert("", phrase_to_phrase_compare(input, produce) == truth_WORD);
  return 0;
}

const char *quiz_addenda_theParagraph_2() { 
  // testing if it works for empty sentence
  NewTextPhrase(input, 2, "li");
  NewPagePhrase(produce, HTIN_LONG);
  produce = fyakyi_fyakka_dva2ttu(produce, input);
  Page_print(input.page);
  Page_print(produce.page);
  DEBUGPRINT(("%X input.length, %X produce.length\n", input.length, produce.length));
  mu_assert("", phrase_to_phrase_compare(input, produce) == truth_WORD);
  return 0;
}

const char *quiz_addenda_theParagraph() { 
	mu_run_quiz(quiz_addenda_theParagraph_1); 
	mu_run_quiz(quiz_addenda_theParagraph_2); 
	return 0;
}
const char *quiz_addenda() {
  mu_run_quiz(quiz_is_theIndexfingerZLine_aPartialIndependentClause);
  mu_run_quiz(quiz_grow_theIndexFinger_untilTheUpcomingLine);
  mu_run_quiz(quiz_is_thePhraseZFinalIndependentClause_aDoneIndependentClause);
  mu_run_quiz(quiz_addenda_aHollowIndependentClause_toThePhrase);
  mu_run_quiz(quiz_addenda_theWord_toThePhrase);
  mu_run_quiz(quiz_addenda_thePhraseWord_toThePhrase);
  mu_run_quiz(quiz_addenda_theGrammarWord_toThePhrase);
  mu_run_quiz(quiz_addenda_thePhrase_toThePhrase);
  mu_run_quiz(quiz_quote_addenda);
  mu_run_quiz(quiz_addenda_theLiteralWord_toThePhrase);
  mu_run_quiz(quiz_addenda_theParagraph);
  return 0;
}
#ifndef EMSCRIPTEN
const char *quiz_opencl_CPU_hardware() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  hardware_discharge(hardware);
  return 0;
}

const char *quiz_opencl_compile_kernel() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom",
      &software);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_kernel() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom",
      &software);
  parallel_program_begin(hardware, software);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_lwonprom2() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom2",
      &software);
  random_t random_seed = 0x12345678;
  clSetKernelArg(software.kernel, 0, sizeof(random_t), &random_seed);
  parallel_program_begin(hardware, software);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_lwonprom3() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom3",
      &software);
  random_t random_seed = 12;
  cl_mem output = clCreateBuffer(hardware.context, CL_MEM_WRITE_ONLY,
      sizeof(char) * 12, NULL, NULL);
  clSetKernelArg(software.kernel, 0, sizeof(random_t), &random_seed);
  clSetKernelArg(software.kernel, 1, sizeof(cl_mem), &output);
  parallel_program_begin(hardware, software);
  struct Text produce;
  char produce_letters[12];
  clEnqueueReadBuffer(hardware.queue, output, CL_TRUE, 0, sizeof(char) * 12,
      produce_letters, 0, NULL, NULL);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  produce.letters = produce_letters;
  produce.length = 12;
  NewText(example, "aaaaaaaaaaaa");
  repeat(hardware.nComputeUnits,
      mu_assert("", produce_letters[iterator] == 'a'));
  // mu_assert("", text_compare(example, produce) == truth_WORD);
  clReleaseMemObject(output);
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_lwonprom4() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom4",
      &software);
  random_t random_seed = 12;
  cl_mem output = clCreateBuffer(hardware.context, CL_MEM_WRITE_ONLY,
      sizeof(uint16_t) * 0x100 * 12, NULL, NULL);
  clSetKernelArg(software.kernel, 0, sizeof(random_t), &random_seed);
  clSetKernelArg(software.kernel, 1, sizeof(cl_mem), &output);
  parallel_program_begin(hardware, software);
  uint16_t produce_letters[12 * 0x100];
  clEnqueueReadBuffer(hardware.queue, output, CL_TRUE, 0,
      sizeof(uint16_t) * 0x100 * 12, produce_letters, 0, NULL,
      NULL);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  repeat(hardware.nComputeUnits,
      mu_assert("", produce_letters[iterator * 0x100] == 'a'));
  // mu_assert("", text_compare(example, produce) == truth_WORD);
  clReleaseMemObject(output);
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_lwonprom5() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom5",
      &software);
  random_t random_seed = 12;
  cl_mem output = clCreateBuffer(hardware.context, CL_MEM_WRITE_ONLY,
      sizeof(line_t) * 12, NULL, NULL);
  clSetKernelArg(software.kernel, 0, sizeof(random_t), &random_seed);
  clSetKernelArg(software.kernel, 1, sizeof(cl_mem), &output);
  parallel_program_begin(hardware, software);
  line_t produce_letters[12] = {{0}};
  clEnqueueReadBuffer(hardware.queue, output, CL_TRUE, 0, sizeof(line_t) * 12,
      produce_letters, 0, NULL, NULL);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  uint16_t j = 0;
  repeat(hardware.nComputeUnits,
      mu_assert("", produce_letters[iterator][0] == 'a'));

  // mu_assert("", text_compare(example, produce) == truth_WORD);
  clReleaseMemObject(output);
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_lwonprom6() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom6",
      &software);
  random_t random_seed = 12;
  cl_mem output = clCreateBuffer(hardware.context, CL_MEM_WRITE_ONLY,
      sizeof(line_t) * 12, NULL, NULL);
  cl_mem input_lines = clCreateBuffer(hardware.context, CL_MEM_READ_ONLY,
      sizeof(line_t), NULL, NULL);
  NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  clEnqueueWriteBuffer(hardware.queue, input_lines, CL_TRUE, 0, sizeof(line_t),
      recipe.page.lines, 0, NULL, NULL);
  clSetKernelArg(software.kernel, 0, sizeof(random_t), &random_seed);
  clSetKernelArg(software.kernel, 1, sizeof(cl_mem), &input_lines);
  clSetKernelArg(software.kernel, 2, sizeof(cl_mem), &output);
  parallel_program_begin(hardware, software);
  line_t produce_letters[12] = {{0}};
  clEnqueueReadBuffer(hardware.queue, output, CL_TRUE, 0, sizeof(line_t) * 12,
      produce_letters, 0, NULL, NULL);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  // repeat(hardware.nComputeUnits,
  //     mu_assert("", produce_letters[iterator][0] == 'a'));
  //
  // mu_assert("", text_compare(example, produce) == truth_WORD);
  clReleaseMemObject(output);
  clReleaseMemObject(input_lines);
  opencl_discharge(hardware, software);
  return 0;
}
const char *quiz_opencl_run_lwonprom7() {
  sclHard hardware;
  centre_hardware_grab(&hardware, 0);
  DEBUGPRINT(("%ld hardware.nComputeUnits\n", hardware.nComputeUnits));
  mu_assert("", hardware.nComputeUnits != 0);
  sclSoft software;
  // memset(software.kernelName, 98, 0);
  parallel_program_establish(hardware, "program/lwonprom.cl", "lwonprom7",
      &software);
  random_t random_seed = 12;
  cl_mem output = clCreateBuffer(hardware.context, CL_MEM_WRITE_ONLY,
      sizeof(line_t) * 12, NULL, NULL);
  cl_mem recipe_lines = clCreateBuffer(hardware.context, CL_MEM_READ_ONLY,
      sizeof(line_t), NULL, NULL);
  NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  clEnqueueWriteBuffer(hardware.queue, recipe_lines, CL_TRUE, 0, sizeof(line_t),
      recipe.page.lines, 0, NULL, NULL);
  NewTrainingSequence(training_sequence, 2, "hlishyikdokali hlistyutdokali ",
      "ksashyikdokali ksastyutdokali ");
  cl_mem input_lines = clCreateBuffer(hardware.context, CL_MEM_READ_ONLY,
      sizeof(line_t) * 2, NULL, NULL);
  clEnqueueWriteBuffer(hardware.queue, input_lines, CL_TRUE, 0,
      sizeof(line_t) * 2, training_sequence.input.page.lines,
      0, NULL, NULL);
  cl_mem produce_lines = clCreateBuffer(hardware.context, CL_MEM_READ_ONLY,
      sizeof(line_t) * 2, NULL, NULL);
  clEnqueueWriteBuffer(hardware.queue, produce_lines, CL_TRUE, 0,
      sizeof(line_t) * 2, training_sequence.produce.page.lines,
      0, NULL, NULL);

  clSetKernelArg(software.kernel, 0, sizeof(random_t), &random_seed);
  clSetKernelArg(software.kernel, 1, sizeof(cl_mem), &recipe_lines);
  clSetKernelArg(software.kernel, 2, sizeof(cl_mem), &input_lines);
  clSetKernelArg(software.kernel, 3, sizeof(cl_mem), &produce_lines);
  clSetKernelArg(software.kernel, 4, sizeof(cl_mem), &output);
  parallel_program_begin(hardware, software);
  line_t produce[12] = {{0}};
  clEnqueueReadBuffer(hardware.queue, output, CL_TRUE, 0, sizeof(line_t) * 12,
      produce, 0, NULL, NULL);
  struct Page produce_page;
  produce_page.lines = produce;
  produce_page.plength = 12;
  text_page_print(produce_page);
  // DEBUGPRINT(("%s kernelName\n", software.kernelName));
  mu_assert("", software.kernelName[0] == 'l');
  // repeat(hardware.nComputeUnits,
  //     mu_assert("", produce_letters[iterator][0] == 'a'));
  //
  // mu_assert("", text_compare(example, produce) == truth_WORD);
  clReleaseMemObject(output);
  clReleaseMemObject(input_lines);
  clReleaseMemObject(recipe_lines);
  clReleaseMemObject(produce_lines);
  opencl_discharge(hardware, software);
  return 0;
}
#endif
const char *quiz_random_seed_establish_1() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  DEBUGPRINT(("%lX %lX\n", random_seed[0], random_seed[1]));
  mu_assert("", random_seed[0] == 0x910A2DEC89025CC1 &&
      random_seed[1] == 0xBEEB8DA1658EEC67);
  return 0;
}
const char *quiz_random_seed_establish_2() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  DEBUGPRINT(("%lX %lX\n", random_seed[0], random_seed[1]));
  mu_assert("", random_seed[0] == 0x910A2DEC89025CC1 &&
      random_seed[1] == 0xBEEB8DA1658EEC67);
  random_seed_establish(2, 2, random_seed);
  mu_assert("", random_seed[0] != 0x910A2DEC89025CC1 &&
      random_seed[1] != 0xBEEB8DA1658EEC67);
  return 0;
}
const char *quiz_random_seed_establish() {
  mu_run_quiz(quiz_random_seed_establish_1);
  mu_run_quiz(quiz_random_seed_establish_2);
  return 0;
}
const char *quiz_phrase_improve() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 300;
  NewTextPhrase(phrase, 1, "hyikdokaplustu");
  NewPagePhrase(phrase_result, 1);
  phrase_improve(climate, phrase);
  text_phrase_print(phrase);
  phrase_print(phrase);
  mu_assert("", phrase_letters_compare(phrase, "tyutdokaplustu") == truth_WORD);
  climate.temperature = 0;
  phrase_improve(climate, phrase);
  text_phrase_print(phrase);
  phrase_print(phrase);
  mu_assert("", phrase_letters_compare(phrase, "tyutdokaplustu") == truth_WORD);
  return 0;
}
const char *quiz_recipe_improve_null() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.temperature = 300;
  climate.random_seed = random_seed;
  climate.radioactivity = 0;
  NewTextPhrase(phrase, 1, "hyikdokaplustu");
  NewPagePhrase(phrase_result, 1);
  // recipe_improve
  NewPagePhrase(person, 0x1);
  NewPagePhrase(fresh_person, 0x1);
  NewTextPhrase(recipe, 0x1, "hyikdoka plustu");
  uint8_t fresh_line = 0;
  person = neo_person_establish(climate, recipe, person);
  fresh_person =
    recipe_improve(climate, fresh_line, person, fresh_line, fresh_person);

  text_phrase_print(person);
  text_phrase_print(fresh_person);
  mu_assert("", phrase_letters_compare(fresh_person, "hyikdoka plustu") ==
      truth_WORD);
  return 0;
}

const char *quiz_recipe_improve_1() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.temperature = 300;
  climate.random_seed = random_seed;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(phrase, 1, "hyikdokaplustu");
  NewPagePhrase(phrase_result, 1);
  // recipe_improve
  NewPagePhrase(person, 0x1);
  NewPagePhrase(fresh_person, 0x1);
  NewTextPhrase(recipe, 0x1, "hyikdoka plustu");
  uint8_t fresh_line = 0;
  person = neo_person_establish(climate, recipe, person);
  text_phrase_print(person);
  fresh_person =
    recipe_improve(climate, fresh_line, person, fresh_line, fresh_person);

  text_phrase_print(person);
  text_phrase_print(fresh_person);
  mu_assert("", phrase_letters_compare(
        fresh_person, "tyutksastyindoka plustu") == truth_WORD);
  return 0;
}
const char *quiz_recipe_improve_multiline() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  NewClimate(climate, 0, 300, 0xFFFE);
  NewTextPhrase(phrase, 1, "hyikdokaplustu");
  NewPagePhrase(phrase_result, 1);
  // recipe_improve
  NewPagePhrase(person, 0x2);
  NewPagePhrase(fresh_person, 0x2);
  NewTextPhrase(recipe, 0x2,
      "hyikzrondopwih lyatgika djancu "
      "tyutzrondoyu psasgiyi lyatgika grettu");
  uint8_t fresh_line = 0;
  person = neo_person_establish(climate, recipe, person);
  text_phrase_print(person);
  fresh_person =
    recipe_improve(climate, fresh_line, person, fresh_line, fresh_person);

  text_phrase_print(person);
  text_phrase_print(fresh_person);
  TODO(mu_assert("#TODO",
      phrase_letters_compare(fresh_person,
        "hyikzrondopwih lyatgika djancu "
        "tyutzrondoyu psasgiyi lyatgika grettu") ==
      truth_WORD));
  return 0;
}
const char *quiz_recipe_improve_multiarg() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  NewClimate(climate, 0, 300, 0xFFFE);
  NewTextPhrase(phrase, 1, "blu7ngika cu tyuttyuthyikdoyu psasgiyi");
  NewPagePhrase(phrase_result, 1);
  // recipe_improve
  NewPagePhrase(person, 0x2);
  NewPagePhrase(fresh_person, 0x2);
  NewTextPhrase(recipe, 0x1,
      "blu7ngika cu tyuttyuthyikdoyu psasgiyi");
  uint8_t fresh_line = 0;
  person = neo_person_establish(climate, recipe, person);
  text_phrase_print(person);
  fresh_person =
    recipe_improve(climate, fresh_line, person, fresh_line, fresh_person);

  text_phrase_print(person);
  text_phrase_print(fresh_person);
  mu_assert("",
      phrase_letters_compare(fresh_person,
        "blu7ngika cu tyuttyuthyikdoyu psasgiyi") ==
      lie_WORD);
  return 0;
}

const char *quiz_recipe_improve() {
  mu_run_quiz(quiz_recipe_improve_null);
  mu_run_quiz(quiz_recipe_improve_1);
  mu_run_quiz(quiz_recipe_improve_multiline);
  mu_run_quiz(quiz_recipe_improve_multiarg);
  return 0;
}
const char *quiz_person_establish_null() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 300;
  climate.radioactivity = 0;
  NewPagePhrase(person, 0x1);
  NewTextPhrase(recipe, 0x1, "hyikdoka plustu");
  person = neo_person_establish(climate, recipe, person);
  text_phrase_print(person);
  text_page_print(person.page);
  DEBUGPRINT(("%X person.length, %X person.begin, %X person.page.plength\n",
        person.length, person.begin, person.page.plength));
  phrase_print(person);
  // text_phrase_print(person);
  mu_assert("",
      phrase_letters_compare(person, "hyikdoka plustu") == truth_WORD);
  return 0;
}
const char *quiz_person_establish_1() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 300;
  climate.radioactivity = 0xFFFE;
  NewPagePhrase(person, 0x1);
  NewTextPhrase(recipe, 0x1, "hyikdoka plustu");
  person = neo_person_establish(climate, recipe, person);
  DEBUGPRINT(("%X person.length, %X person.begin, %X person.page.plength\n",
        person.length, person.begin, person.page.plength));
  phrase_print(person);
  text_phrase_print(person);
  mu_assert("", phrase_letters_compare(person, "ksastyutdoka plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_establish_2() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xACDFEACD, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 20;
  climate.radioactivity = 0xFFFE;
  NewPagePhrase(person, 0x1);
  NewTextPhrase(recipe, 0x1, "hyikdoka plustu");
  person = neo_person_establish(climate, recipe, person);
  DEBUGPRINT(("%X person.length, %X person.begin, %X person.page.plength\n",
        person.length, person.begin, person.page.plength));
  phrase_print(person);
  text_phrase_print(person);
  mu_assert("",
      phrase_letters_compare(person, "twundoka plustu") == truth_WORD);
  return 0;
}

const char *quiz_person_establish_multiline_recipe() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xACDFEACD, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 20;
  climate.radioactivity = 0xFFFE;
  NewPagePhrase(person, 0x1);
  NewTextPhrase(recipe, 0x2,
      "hyikzrondopwih lyatgika djancu "
      "tyutzrondoyu psasgiyi lyatgika grettu");
  person = neo_person_establish(climate, recipe, person);
  DEBUGPRINT(("%X person.length, %X person.begin, %X person.page.plength\n",
        person.length, person.begin, person.page.plength));
  phrase_print(person);
  text_phrase_print(person);
  TODO(mu_assert("#TODO",
      phrase_letters_compare(
        person, "hyikzrondopwih lyatgika djancu "
        "tyutzrondoyu psasgiyi lyatgika grettu") == truth_WORD));
  return 0;
}

const char *quiz_person_establish() {
  mu_run_quiz(quiz_person_establish_null);
  mu_run_quiz(quiz_person_establish_1);
  mu_run_quiz(quiz_person_establish_2);
  mu_run_quiz(quiz_person_establish_multiline_recipe);
  return 0;
}
const char *quiz_population_establish_hollow() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  NewHollowPopulation(population, 0x10 /*population_length*/,
      2 /*person_length*/);
  down_to_zero_repeat(
      0x10 - 2, DEBUGPRINT(("%lX &population.people[iterator]\n",
          (uint64_t) & (population.people[iterator])));
      mu_assert("", &population.people[iterator] !=
        &population.people[iterator + 1]);
      mu_assert("", population.people[iterator].page.lines !=
        population.people[iterator + 1].page.lines);

      );
  return 0;
}

const char *quiz_population_establish_null() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 300;
  climate.radioactivity = 0x0;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      2 /*person_length*/);
  NewTextPhrase(recipe, 0x1, "hyikdoka plustu");
  DEBUGPRINT(("%X population.length\n", population.length));
  neo_population_establish(climate, recipe, population);
  text_phrase_print(population.people[0]);
  text_page_print(population.people[0].page);
  Page_print(population.people[0].page);
  mu_assert("", phrase_letters_compare(population.people[0],
        "hyikdokaplustu"
        "hyikdokaplustu") == truth_WORD);
  return 0;
}

const char *quiz_population_establish_1() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xE, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 4;
  climate.radioactivity = 0xF000;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      3 /*person_length*/);
  NewTextPhrase(recipe, 0x1, "twundoka plustu");
  DEBUGPRINT(("%X population.length\n", population.length));
  text_phrase_print(population.people[0]);
  population = neo_population_establish(climate, recipe, population);
  text_phrase_print(population.people[3]);
  text_page_print(population.people[3].page);
  mu_assert("",
      phrase_letters_compare(population.people[3], "hyikhyikdoka plustu "
        "") == truth_WORD);
  return 0;
}

const char *quiz_population_establish_2() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0x21ABCDF, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 300;
  climate.radioactivity = 0xFFFE;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      3 /*person_length*/);
  NewTextPhrase(recipe, 0x1, "hyikzronzrondoka plustu");
  DEBUGPRINT(("%X population.length\n", population.length));
  text_page_print(population.people[0].page);
  neo_population_establish(climate, recipe, population);
  // Page_print(population.people[0].page);
  text_page_print(population.people[0].page);
  text_phrase_print(population.people[0]);
  DEBUGPRINT(("%X length\n", population.people[0].length));
  mu_assert(
      "",
      phrase_letters_compare(
        population.people[0],
        "hyikhyiktwundoka plustu hseshpetdoka plustu hseshsipdoka plustu") ==
      truth_WORD);
  return 0;
}

const char *quiz_NewHollowPopulation() {
  NewHollowPopulation(population, 0x10, 0x10);
  mu_assert("", population.length == 0x10);
  down_to_zero_repeat(
      population.length - 1,
      mu_assert("", population.people[iterator].page.plength == 0x10));
  return 0;
}
const char *quiz_population_establish_multi_line() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xE, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 4;
  climate.radioactivity = 0xF000;
  NewHollowPopulation(population, 30 /*population_length*/,
      4 /*person_length*/);
  NewTextPhrase(recipe, 0x2,
      "hyikzrondopwih lyatgika djancu blu7ngika gruttu "
      "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  DEBUGPRINT(("%X population.length\n", population.length));
  text_phrase_print(population.people[0]);
  population = neo_population_establish(climate, recipe, population);
  uint16_t max_person_long = 0;
  repeat(30,
      max_person_long = max(population.people[iterator].length,
        max_person_long);
      DEBUGPRINT(("%X max_person_long, %X population.people[iterator].length\n",
          max_person_long, population.people[iterator].length));
      conditional(population.people[iterator].length > 0x10,
        text_phrase_print(population.people[iterator]));
      );
  mu_assert("", max_person_long > 0x30);
  return 0;
}

const char *quiz_population_establish() {
  mu_run_quiz(quiz_NewHollowPopulation);
  mu_run_quiz(quiz_population_establish_hollow);
  mu_run_quiz(quiz_population_establish_null);
  mu_run_quiz(quiz_population_establish_1);
  mu_run_quiz(quiz_population_establish_2);
  mu_run_quiz(quiz_population_establish_multi_line);
  return 0;
}
const char *quiz_program_quiz_4() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "tyutdokali", "hfakdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 10, "tyutdoyu plustu hyikdoyu plustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  Page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "hfakdokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_5() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "ksasdokali hlisdokali ");
  NewTextPhrase(program_produce, 4, "");
  NewTextPhrase(program, 10, "tyutdoyu plustu hyikdoyu plustu");

  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce,
        "ksasdokali hlisdokali") == truth_WORD);
  return 0;
}
const char *quiz_program_quiz_3() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "hyikdokali", "tyindokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 4, "tyutdoyu plustu");

  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "tyindokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_8() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "tyutdokali", "ksasdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 4, "tyutdoyu plustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "ksasdokali") ==
      truth_WORD);
  return 0;
}

const char *quiz_program_quiz_2() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 2, "tyutdokali hyikdokali",
      "ksasdokali tyindokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 4, "tyutdoyu plustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce,
        "ksasdokali tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_program_quiz_1() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "hyikdokali", "hyikdokali");
  NewTextPhrase(program_produce, 4, "");
  NewTextPhrase(program, 4, "hyikdokali"); //"hyikdoyuhyikdokaplustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "hyikdokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_null() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 2, "", "");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 2, "");

  program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  mu_assert("", phrase_letters_compare(program_produce, "") == truth_WORD);
  return 0;
}
const char *quiz_program_quiz_6() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "hyikdokali", "hlisdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 10,
      "hfakdoyu plustu zrondoyu plustu twundoyu plustu"
      "zrondoyuplustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "hpetdokali") ==
      truth_WORD);
  return 0;
}

const char *quiz_program_quiz_7() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "hyikdokali", "hlisdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 10,
      "hfakdoyu plustu twundoyu plustu"
      "");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "hpetdokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_9() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "hyikdokali", "hlisdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 10,
      "zrondoyu plustu tyindoyu plustu zrondoyu plustu "
      "tyindoyu plustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "hsipdokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_A() {
  DEBUGPRINT(("%s\n", "hello"));
  NewTrainingSequence(training_sequence, 1, "hyikdokali", "hlisdokali");
  NewTextPhrase(program, 10,
      "zrondoyu plustu tyindoyu plustu zrondoyu plustu "
      "tyindoyu plustu");
  NewPagePhrase(program_produce, 2);
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  text_page_print(program_produce.page);
  mu_assert("", phrase_letters_compare(program_produce, "hsipdokali") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_multi_grettu() {
  NewTrainingSequence(training_sequence, 1, "lyatgina zi.prih.A.prih.zika li",
      "psasgina zi.prih.a.prih.zika li");
  NewTextPhrase(program, 4,
      "hyiktyutdoyu psasgiyi lyatgika grettu tyutzron"
      "doyu psasgiyi lyatgika grettu");
  NewPagePhrase(program_produce, 4);
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  mu_assert("", phrase_letters_compare(program_produce,
        "psasgina zi.prih.a.prih.zika li") ==
      lie_WORD);
  return 0;
}
const char *quiz_program_quiz_multi_input_conditional() {
  NewLongTrainingSequence(
      training_sequence, 1, 2,
      "blu7ngina cyinbluhka li lyatgina zi.prih.a.prih.zika li",
      "psasgina zi.prih.A.prih.zika li");
  DEBUGPRINT(("%X input_length\n", training_sequence.input_length));
  NewTextPhrase(program, 4,
      "blu7ngika gruttu"
      "blu7ngika cu tyutzrondoyu psasgiyi lyatgika grettu");
  NewPagePhrase(program_produce, 4);
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  mu_assert("", phrase_letters_compare(program_produce,
        "blu7ngina syanbluhka li "
        "psasgina zi.prih.A.prih.zika li") ==
      truth_WORD);
  return 0;
}
const char *quiz_program_quiz_multi_input_conditional2() {
  NewLongTrainingSequence(
      training_sequence, 1, 2,
      "blu7ngina cyinbluhka li lyatgina zi.prih.a.prih.zika li"
      "blu7ngina cyinbluhka li lyatgina zi.prih.b.prih.zika li",
      "psasgina zi.prih.A.prih.zika li"
      "psasgina zi.prih.B.prih.zika li"
      );
  DEBUGPRINT(("%X input_length\n", training_sequence.input_length));
  NewTextPhrase(program, 4,
      "blu7ngika gruttu"
      "blu7ngika cu tyutzrondoyu psasgiyi lyatgika grettu");
  NewPagePhrase(program_produce, 4);
  program_produce = program_quiz(training_sequence, program, program_produce);
  text_phrase_print(program_produce);
  mu_assert("", phrase_letters_compare(program_produce,
        "blu7ngina syanbluhka li "
        "psasgina zi.prih.A.prih.zika li") ==
      truth_WORD);
  return 0;
}

const char *quiz_program_quiz() {
  mu_run_quiz(quiz_program_quiz_null);
  mu_run_quiz(quiz_program_quiz_1);
  mu_run_quiz(quiz_program_quiz_8);
  mu_run_quiz(quiz_program_quiz_2);
  mu_run_quiz(quiz_program_quiz_3);
  mu_run_quiz(quiz_program_quiz_4);
  mu_run_quiz(quiz_program_quiz_5);
  mu_run_quiz(quiz_program_quiz_6);
  mu_run_quiz(quiz_program_quiz_7);
  mu_run_quiz(quiz_program_quiz_9);
  mu_run_quiz(quiz_program_quiz_A);
  mu_run_quiz(quiz_program_quiz_multi_grettu);
  mu_run_quiz(quiz_program_quiz_multi_input_conditional);
  mu_run_quiz(quiz_program_quiz_multi_input_conditional2);
  return 0;
}

const char *quiz_population_quiz_1() {
  // population establish
  uint64_t random_seed[2] = {0};
  random_seed_establish(0x3, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x2;
  climate.radioactivity = 0xFFFE;
  NewHollowPopulation(population, 0x1 /*population_length*/,
      1 /*person_length*/);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  text_phrase_print(population.people[0]);
  neo_population_establish(climate, recipe, population);
  Page_print(population.people[0].page);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali ",
      "hfakdokali hsipdokali ");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  repeat(0x1, text_phrase_print(population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  mu_assert("", population_health[0] == 0xFFF0);
  return 0;
}

const char *quiz_population_quiz_2() {
  // population establish
  NewClimate(climate, 0xA2A2FD, 5, 0xFFFE);
  NewHollowPopulation(population, 0x10 /*population_length*/,
      1 /*person_length*/);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x1, "hyikdoyu plustu");
  text_phrase_print(population.people[0]);
  neo_population_establish(climate, recipe, population);
  Page_print(population.people[0].page);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali ",
      "hfakdokali hsipdokali ");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  repeat(0x10, DEBUGPRINT(("%lX iterator\n", iterator));
      text_phrase_print(population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  mu_assert("", population_health[0x4] >= 0xFFF0);
  return 0;
}

const char *quiz_population_quiz_3() {
  // population establish
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0x2F9DA4319721C729;
  climate.random_seed[1] = 0x6EA60BD46973DB0B;
  NewHollowPopulation(population, 60, 0x2);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x2,
                "ksaszrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu hyikhyikdoyu psasgiyi lyatgika grettu");
  text_phrase_print(population.people[0]);
  population =
  neo_population_establish(climate, recipe, population);
  Page_print(population.people[0].page);
  // training sequence
  NewLongTrainingSequence(training_sequence, 8, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.m.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.!.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.0.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.Z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.A.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.`.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.M.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.Z.prih.zikali "
                      );
  NewTextPhrase(program_produce, 0x10, "");
  NewTextPhrase(champion_program, 2, 
      "hliszrondopwih lyatgika djancu blu7ngika gruttu"
      "blu7ngika cu tyutzrondoyu psasgiyi lyatgika grettu");
  // population quiz
  //
  //
  population.people[0x8] = champion_program;
  uint64_t champion_iteration_sequence[60];
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
        champion_choose(population.length, population_health,
                        champion_iteration_sequence);
  //
  repeat(0x10, DEBUGPRINT(("%lX iterator\n", iterator));
      text_phrase_print(population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  return 0;
}

const char *quiz_population_quiz() {
  mu_run_quiz(quiz_population_quiz_1);
  mu_run_quiz(quiz_population_quiz_2);
  mu_run_quiz(quiz_population_quiz_3);
  return 0;
}

const char *quiz_champion_choose_1() {
  // population establish
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      4 /*person_length*/);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x1, "hyikdoyu plustu");
  text_phrase_print(population.people[0]);
  neo_population_establish(climate, recipe, population);
  Page_print(population.people[0].page);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali ",
      "hfakdokali hsipdokali ");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  repeat(0x1, text_phrase_print(population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  uint64_t champion[0x10];
  champion_choose(0x10, population_health, &champion);
  DEBUGPRINT(("%lX champion, %X champion health\n", champion,
        population_health[champion[0]]));

  mu_assert("", population_health[champion[0]] == 0xFFFC);
  return 0;
}
const char *quiz_champion_choose_2() {
  // population establish
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      4 /*person_length*/);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x1, "hyikdoyu plustu");
  text_phrase_print(population.people[0]);
  neo_population_establish(climate, recipe, population);
  Page_print(population.people[0].page);
  // training sequence
  NewLongTrainingSequence(training_sequence, 8, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.m.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.!.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.0.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.Z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.A.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.`.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.M.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.Z.prih.zikali "
                      );
  NewTextPhrase(champion, 0x2,
                "hliszrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  NewTextPhrase(program_produce, 16, "");
  // population quiz
  population.people[0] = champion;
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  repeat(0x1, text_phrase_print(population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  uint64_t champion_iteration_sequence[0x10];
  champion_choose(0x10, population_health, champion_iteration_sequence);
  DEBUGPRINT(("%lX champion, %X champion health\n", champion_iteration_sequence[0],
        population_health[champion_iteration_sequence[0]]));

  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  return 0;
}
const char *quiz_champion_choose() {
  mu_run_quiz(quiz_champion_choose_1);
  mu_run_quiz(quiz_champion_choose_2);
  return 0;
}
const char *quiz_person_copy() {
  NewTextPhrase(input, 2, "hyikdokali tyindokali ");
  NewPagePhrase(produce, 2);
  produce = person_copy(input, produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali tyindokali") ==
      truth_WORD);
  return 0;
}

const char *quiz_population_copy() {
  // population establish
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      4 /*person_length*/);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x1, "hyikdoyu plustu");
  text_phrase_print(population.people[0]);
  neo_population_establish(climate, recipe, population);
  // copy population
  NewHollowPopulation(produce, 0x10 /*population_length*/, 4 /*person_length*/);
  produce = population_copy(population, produce);
  uint16_t max_length = 0;
  down_to_zero_repeat(
      population.length - 1,
      max_length = max(max_length, produce.people[iterator].length);
      mu_assert("", phrase_to_phrase_compare(population.people[iterator],
          produce.people[iterator]) ==
        truth_WORD));
  mu_assert("", max_length > 0x30);
  return 0;
}
const char *quiz_champion_copy() {
  // population establish
  uint64_t random_seed[2] = {0};
  random_seed_establish(1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewHollowPopulation(population, 0x10 /*population_length*/,
      4 /*person_length*/);
  uint32_t population_health[0x10];
  NewTextPhrase(recipe, 0x1, "hyikdoyu plustu");
  text_phrase_print(population.people[0]);
  neo_population_establish(climate, recipe, population);
  Page_print(population.people[0].page);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "hfakdokali hsipdokali ");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  NewHollowPopulation(fresh_population, 0x5 /*population_length*/,
      4 /*person_length*/);
  uint64_t champion_iteration_sequence[0x10];
  champion_choose(0x10, population_health, champion_iteration_sequence);
  fresh_population = champion_copy(population, champion_iteration_sequence, 0x5,
      fresh_population);
  population_quiz(fresh_population, training_sequence, program_produce,
      population_health);
  repeat(
      0x5, mu_assert("", fresh_population.people[iterator].length > 0);
      mu_assert("",
        phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[champion_iteration_sequence[iterator]]) ==
        truth_WORD);
      mu_assert("",
        &fresh_population.people[iterator].page.lines !=
        &population.people[champion_iteration_sequence[iterator]]
        .page.lines);
      text_phrase_print(fresh_population.people[iterator]);
      text_page_print(fresh_population.people[iterator].page);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  mu_assert("", population_health[0] >= 0xFFF8);

  return 0;
}
const char *quiz_recipe_switch() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(recipe, 0x3, "hyikdoyu grettu tyutdoyu plustu tyindoyu hgoctu");
  NewPagePhrase(fresh_phrase, 1);
  fresh_phrase = recipe_switch(climate, recipe, fresh_phrase);
  text_phrase_print(fresh_phrase);
  mu_assert("", phrase_letters_compare(fresh_phrase, "ksasdoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_recipe_merge_plustu() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(recipe, 0x3, "hyikdoyu plustu tyutdoyu plustu tyindoyu plustu");
  NewPagePhrase(fresh_phrase, 1);
  fresh_phrase =
    recipe_merge(climate, recipe, 2, instrumental_case_GRAMMAR, fresh_phrase);
  text_phrase_print(fresh_phrase);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hfakdoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_recipe_merge_grettu() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(recipe, 0x3, "hyikdoyu grettu tyutdoyu grettu tyindoyu grettu");
  NewPagePhrase(fresh_phrase, 1);
  fresh_phrase =
    recipe_merge(climate, recipe, 2, instrumental_case_GRAMMAR, fresh_phrase);
  text_phrase_print(fresh_phrase);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hfakdoyu grettu") ==
      truth_WORD);
  return 0;
}
const char *quiz_recipe_merge() {
  mu_run_quiz(quiz_recipe_merge_plustu);
  mu_run_quiz(quiz_recipe_merge_grettu);
  return 0;
}
const char *quiz_person_compression_recipe_merge_plustu() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(recipe, 0x3, "tyutdoyu plustu tyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase = person_compression_recipe_merge(climate, recipe, fresh_phrase);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hfakdoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_compression_recipe_merge_plustu_2() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x4, "zrondoyu plustu tyindoyuplustu tyindoyuplustu");
  NewPagePhrase(fresh_phrase, 0x4);
  fresh_phrase = person_compression_recipe_merge(climate, recipe, fresh_phrase);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hlisdoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_compression_recipe_merge_grettu() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(recipe, 0x3, "tyutdoyu grettu tyindoyu grettu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase = person_compression_recipe_merge(climate, recipe, fresh_phrase);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hfakdoyu grettu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_compression_recipe_merge_grettu_2() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFFE;
  NewTextPhrase(recipe, 0x3, "hyikzrondoyu grettu hyikzrondoyu grettu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase = person_compression_recipe_merge(climate, recipe, fresh_phrase);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "tyutzrondoyu grettu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_compression_recipe_merge() {
  mu_run_quiz(quiz_person_compression_recipe_merge_plustu);
  mu_run_quiz(quiz_person_compression_recipe_merge_plustu_2);
  mu_run_quiz(quiz_person_compression_recipe_merge_grettu);
  mu_run_quiz(quiz_person_compression_recipe_merge_grettu_2);
  return 0;
}
const char *quiz_recipe_delete() {
  NewTextPhrase(recipe, 0x3, "hyikdoyu plustu tyutdoyu plustu tyindoyu plustu");
  NewPagePhrase(produce, 2);
  produce = recipe_delete(recipe, 1, produce);
  mu_assert("",
      phrase_letters_compare(produce, "hyikdoyu plustu"
        "tyindoyu plustu") == truth_WORD);
  return 0;
}
const char *quiz_person_compression_recipe_delete() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x3, "hyikdoyu plustu tyutdoyu plustu tyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase =
    person_compression_recipe_delete(climate, recipe, fresh_phrase);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hyikdoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_htin_transmutation_1() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x3, "tyindoyu plustu");
  NewTextPhrase(program, 0x3, "tyintyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 0
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("",
      phrase_letters_compare(fresh_phrase, "") == lie_WORD &&
      phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation_2() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xAFDC, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 5;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x3, "tyindoyu plustu");
  NewTextPhrase(program, 0x3, "tyintyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 1
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("",
      phrase_letters_compare(fresh_phrase, "") == lie_WORD &&
      phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation_3() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xBCDEF, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x80;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x3, "tyindoyu plustu");
  NewTextPhrase(program, 0x3, "tyintyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 2
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "") != truth_WORD);
  mu_assert("", phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation_4() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0x1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x80;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x1, "tyindoyu plustu");
  NewTextPhrase(program, 0x3, "tyintyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 3
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "") == lie_WORD);
  mu_assert("", phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation_5() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xAF, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x80;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x1, "tyindoyu plustu");
  NewTextPhrase(program, 0x3, "tyintyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 4
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert(
      "",
      phrase_letters_compare(fresh_phrase, "") == lie_WORD &&
      phrase_letters_compare(
        fresh_phrase,
        "hyiktyintyindoyu plustu hfakdoyu plustu tyintyindoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_htin_transmutation_7() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0x1, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x80;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x1, "tyindoyu plustu");
  NewTextPhrase(program, 0x3, "");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 3
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "") == lie_WORD);
  mu_assert("", phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation_8() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0x187C05531442EB40, 0x4B69D33D80C5B263};
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0xFC;
  climate.radioactivity = 0xC000;
  NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  NewTextPhrase(program, 0x3, "");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 3
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", fresh_phrase.page.lines[0][1] != 0);
  mu_assert("", phrase_letters_compare(fresh_phrase, "") == lie_WORD);
  mu_assert("", phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation_9() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0x187C05531442EB40, 0x4B69D33D80C5B263};
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0xFC;
  climate.radioactivity = 0xC000;
  NewTextPhrase(recipe, 0x1, "blu7ngika cu tyuttyuthyikdoyu psasgiyi");
  NewTextPhrase(program, 0x3, "blu7ngika cu tyuttyuthyikdoyu psasgiyi");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 3
  fresh_phrase = htin_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", fresh_phrase.page.lines[0][1] != 0);
  mu_assert("", phrase_letters_compare(fresh_phrase, 
        "blu7ngika cu tyuttyuthyikdoyu psasgiyi") == lie_WORD);
  mu_assert("", phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  return 0;
}
const char *quiz_htin_transmutation() {
  mu_run_quiz(quiz_htin_transmutation_1);
  mu_run_quiz(quiz_htin_transmutation_2);
  mu_run_quiz(quiz_htin_transmutation_3);
  mu_run_quiz(quiz_htin_transmutation_4);
  mu_run_quiz(quiz_htin_transmutation_7);
  mu_run_quiz(quiz_htin_transmutation_8);
  mu_run_quiz(quiz_htin_transmutation_9);
  // mu_run_quiz(quiz_htin_transmutation_5);
  return 0;
}
const char *quiz_person_transmutation_1() {
  // DEBUGPRINT(("asdf %s ","hello"));
  uint64_t random_seed[2] = {0};
  random_seed_establish(0x2CAA06A, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x100;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x1, "hfakdoyu plustu");
  NewTextPhrase(program, 0x3, "tyintyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 2
  fresh_phrase = person_transmutation(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  mu_assert("", phrase_letters_compare(fresh_phrase, "") == lie_WORD);
  mu_assert("", phrase_to_phrase_compare(fresh_phrase, program) == lie_WORD);
  mu_assert("", fresh_phrase.length > 0x10);
  return 0;
}
const char *quiz_person_transmutation() {
  mu_run_quiz(quiz_person_transmutation_1);
}
const char *quiz_population_transmutation_1() {
  // DEBUGPRINT(("asdf %s ","hello"));
  NewClimate(climate, 0xACDAE5393, 0xFF, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "hfakdoyu plustu");
  NewPopulation(population, 0x10, 0x5, climate, recipe);
  NewHollowPopulation(fresh_population, 0x10, 0x5);
  // branch 2
  fresh_population =
    population_transmutation(climate, recipe, population, fresh_population);
  repeat(0x10, text_phrase_print(fresh_population.people[iterator]);
      text_page_print(fresh_population.people[iterator].page);
      mu_assert("", phrase_letters_compare(fresh_population.people[iterator],
          "") == lie_WORD);
      mu_assert("", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[iterator]) == lie_WORD);
      if (iterator >= 1) mu_assert(
        "", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          fresh_population.people[iterator - 1]) == lie_WORD));
  return 0;
}
const char *quiz_population_transmutation_2() {
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  NewTextPhrase(recipe, 0x2,
      "hyikzrondopwih lyatgika djancu blu7ngika gruttu "
      "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  NewHollowPopulation(population, 30, 0x4);
  neo_population_establish(climate, recipe, population);
  NewHollowPopulation(fresh_population, 30, 0x4);
  // branch 2
  fresh_population =
    population_transmutation(climate, recipe, population, fresh_population);
  uint16_t max_length = 0;
  repeat(0x10, text_phrase_print(fresh_population.people[iterator]);
      text_page_print(fresh_population.people[iterator].page);
      max_length = max(max_length,fresh_population.people[iterator].length); 
      mu_assert("", phrase_letters_compare(fresh_population.people[iterator],
          "") == lie_WORD);
      mu_assert("", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[iterator]) == lie_WORD);
      if (iterator >= 1) mu_assert(
        "", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          fresh_population.people[iterator - 1]) == lie_WORD));
  mu_assert("", max_length > 0x30);
  return 0;
}
const char *quiz_population_transmutation() {
  mu_run_quiz(quiz_population_transmutation_1);
  mu_run_quiz(quiz_population_transmutation_2);
  return 0;
}
const char *quiz_person_compression_plustu() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xB, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x0;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x1, "hfakdoyu plustu");
  NewTextPhrase(program, 0x3, "tyindoyu plustu tyindoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 2
  fresh_phrase = person_compression(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  Page_print(fresh_phrase.page);
  NewTextPhrase(result, 0x1, "hlisdoyu plustu");
  Page_print(result.page);
  DEBUGPRINT(("%X result.length, %X fresh_phrase.length\n", result.length,
        fresh_phrase.length));
  mu_assert("", phrase_letters_compare(fresh_phrase, "") == lie_WORD);
  mu_assert("", phrase_letters_compare(fresh_phrase, "hlisdoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_compression_grettu() {
  uint64_t random_seed[2] = {0};
  random_seed_establish(0xACDFE, 2, random_seed);
  struct Climate climate;
  climate.random_seed = random_seed;
  climate.temperature = 0x80;
  climate.radioactivity = 0xFFF0;
  NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  NewTextPhrase(program, 0x3, "hyikzrondoyu plustu hyikzrondoyu plustu");
  NewPagePhrase(fresh_phrase, 0x3);
  fresh_phrase.begin = 1;
  // branch 2
  fresh_phrase = person_compression(climate, recipe, program, fresh_phrase);
  text_phrase_print(program);
  text_phrase_print(fresh_phrase);
  text_page_print(fresh_phrase.page);
  Page_print(fresh_phrase.page);
  // NewTextPhrase(result, 0x1, "hlisdoyu plustu");
  // Page_print(result.page);
  // DEBUGPRINT(("%X result.length, %X fresh_phrase.length\n", result.length,
  //             fresh_phrase.length));
  mu_assert("", phrase_letters_compare(fresh_phrase, "") == lie_WORD);
  mu_assert("", phrase_letters_compare(fresh_phrase, "tyutzrondoyu plustu") ==
      truth_WORD);
  return 0;
}
const char *quiz_person_compression() {
  mu_run_quiz(quiz_person_compression_plustu);
  mu_run_quiz(quiz_person_compression_grettu);
  return 0;
}
const char *quiz_population_refresh_1() {
  NewClimate(climate, 0xACF33, 0x80, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewPopulation(population, 5, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 5, 0x4);
  // training sequence
  NewTextPhrase(program_produce, 2, "");
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "hlisdokali hsipdokali");
  // population quiz
  uint32_t population_health[5];
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  uint64_t champion_iteration_sequence[5];
  champion_choose(5, population_health, champion_iteration_sequence);
  seed_random(climate.random_seed);
  fresh_population =
    population_refresh(climate, recipe, champion_iteration_sequence,
        population, fresh_population);
  repeat(5, DEBUGPRINT(("%X iterator\n", iterator));
      text_phrase_print(fresh_population.people[iterator]);
      text_page_print(fresh_population.people[iterator].page);
      mu_assert("", phrase_letters_compare(fresh_population.people[iterator],
          "") == lie_WORD);
      text_page_print(fresh_population.people[iterator].page);
      mu_assert("", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[iterator]) == lie_WORD);
      if (iterator >= 1) mu_assert(
        "", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          fresh_population.people[iterator - 1]) == lie_WORD));
  mu_assert("", phrase_to_phrase_compare(
        fresh_population.people[0],
        population.people[champion_iteration_sequence[0]]) ==
      truth_WORD);
  return 0;
}
const char *quiz_population_refresh_2() {
  NewClimate(climate, 0x33BEF73, 0x80, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
  // training sequence
  NewTextPhrase(program_produce, 2, "");
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "hlisdokali hsipdokali");
  // population quiz
  uint32_t population_health[30];
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  uint64_t champion_iteration_sequence[30];
  champion_choose(30, population_health, champion_iteration_sequence);
  seed_random(climate.random_seed);
  fresh_population =
    population_refresh(climate, recipe, champion_iteration_sequence,
        population, fresh_population);
  repeat(5, text_phrase_print(fresh_population.people[iterator]);
      text_page_print(fresh_population.people[iterator].page);
      mu_assert("", phrase_letters_compare(fresh_population.people[iterator],
          "") == lie_WORD);
      mu_assert("", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[iterator]) == lie_WORD);
      if (iterator >= 1) mu_assert(
        "", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          fresh_population.people[iterator - 1]) == lie_WORD));
  repeat(3, mu_assert(
        "",
        phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[champion_iteration_sequence[iterator]]) ==
        truth_WORD););
  // fresh population health must be equal to or greater than previous
  // generation.
  uint32_t fresh_population_health[30];
  uint64_t fresh_champion_iteration_sequence[30];
  population_quiz(fresh_population, training_sequence, program_produce,
      fresh_population_health);
  champion_choose(fresh_population.length, fresh_population_health,
      fresh_champion_iteration_sequence);
  DEBUGPRINT(("%X fresh_health, %X old health\n",
        fresh_population_health[fresh_champion_iteration_sequence[0]],
        population_health[champion_iteration_sequence[0]]));
  mu_assert("", fresh_population_health[fresh_champion_iteration_sequence[0]] >=
      population_health[champion_iteration_sequence[0]]);
  return 0;
}
const char *quiz_population_refresh_3() {
  NewClimate(climate, 0x33BEF73, 0x80, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
  // training sequence
  NewTextPhrase(program_produce, 2, "");
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "hlisdokali hsipdokali");
  // population quiz
  uint32_t population_health[30];
  population_quiz(population, training_sequence, program_produce,
      population_health);
  // check population health
  //
  uint64_t champion_iteration_sequence[30];
  champion_choose(30, population_health, champion_iteration_sequence);
  seed_random(climate.random_seed);
  fresh_population =
    population_refresh(climate, recipe, champion_iteration_sequence,
        population, fresh_population);
  repeat(5, text_phrase_print(fresh_population.people[iterator]);
      text_page_print(fresh_population.people[iterator].page);
      mu_assert("", phrase_letters_compare(fresh_population.people[iterator],
          "") == lie_WORD);
      mu_assert("", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[iterator]) == lie_WORD);
      if (iterator >= 1) mu_assert(
        "", phrase_to_phrase_compare(
          fresh_population.people[iterator],
          fresh_population.people[iterator - 1]) == lie_WORD));
  repeat(3, mu_assert(
        "",
        phrase_to_phrase_compare(
          fresh_population.people[iterator],
          population.people[champion_iteration_sequence[iterator]]) ==
        truth_WORD););
  // fresh population health must be equal to or greater than previous
  // generation.
  uint32_t fresh_population_health[30];
  uint64_t fresh_champion_iteration_sequence[30];
  population_quiz(fresh_population, training_sequence, program_produce,
      fresh_population_health);
  champion_choose(fresh_population.length, fresh_population_health,
      fresh_champion_iteration_sequence);
  DEBUGPRINT(("%X fresh_health, %X old health\n",
        fresh_population_health[fresh_champion_iteration_sequence[0]],
        population_health[champion_iteration_sequence[0]]));
  mu_assert("", fresh_population_health[fresh_champion_iteration_sequence[0]] >=
      population_health[champion_iteration_sequence[0]]);
  return 0;
}

const char *quiz_population_refresh() {
  mu_run_quiz(quiz_population_refresh_1);
  mu_run_quiz(quiz_population_refresh_2);
  mu_run_quiz(quiz_population_refresh_3);
  return 0;
}

const char *quiz_champion_compression_1() {
  NewClimate(climate, 0xACDFE29, 0x80, 0xFFF0);
  NewTextPhrase(program, 0x4, "hyikdoyu plustu, tyutdoyu plustu");
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewHollowPopulation(fresh_population, 0x10, 0x4);
  DEBUGPRINT();
  champion_compression(climate, recipe, program, fresh_population);
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "ksasdokali hlisdokali");
  NewPagePhrase(program_produce, 2);
  uint32_t population_health[5];
  population_quiz(fresh_population, training_sequence, program_produce,
      population_health);
  uint64_t champion_iteration_sequence[0x10];
  champion_choose(fresh_population.length, population_health,
      champion_iteration_sequence);
  repeat(0x10, text_phrase_print(fresh_population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFFC);
  return 0;
}
const char *quiz_champion_compression_2() {
  NewClimate(climate, 0xACDFE29, 0x80, 0xFFF0);
  NewTextPhrase(program, 0x4, "hyikdoyu plustu, tyutdoyu plustu");
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewHollowPopulation(fresh_population, 0x10, 0x4);
  champion_compression(climate, recipe, program, fresh_population);
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
      "ksasdokali hlisdokali");
  NewPagePhrase(program_produce, 2);
  uint32_t population_health[5];
  population_quiz(fresh_population, training_sequence, program_produce,
      population_health);
  uint64_t champion_iteration_sequence[0x10];
  champion_choose(fresh_population.length, population_health,
      champion_iteration_sequence);
  repeat(0x10, text_phrase_print(fresh_population.people[iterator]);
      DEBUGPRINT(("%X health\n", population_health[iterator])););
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFFC);
  return 0;
}

const char *quiz_champion_compression() {
  mu_run_quiz(quiz_champion_compression_1);
  return 0;
}

const char *quiz_evolve_generation_1() {
  NewClimate(climate, 0xAC, 5, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "ksasdoyu plustu");
  NewPopulation(population, 30, 0x2, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x2);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali ",
                      "hlisdokali hwapdokali ");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  evolve_generation(climate, recipe, training_sequence, population,
                    fresh_population, population_health,
                    champion_iteration_sequence);
  repeat(3, text_phrase_print(fresh_population.people[iterator]);
         text_phrase_print(
             population.people[champion_iteration_sequence[iterator]]);
         DEBUGPRINT(
             ("%X health \n",
              (population_health[champion_iteration_sequence[iterator]]))););
  mu_assert("", phrase_to_phrase_compare(
                    population.people[champion_iteration_sequence[0]],
                    fresh_population.people[0]) == truth_WORD);
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  return 0;
}
const char *quiz_evolve_generation_2() {
  NewClimate(climate, 0xA, 5, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "ksasdoyu plustu");
  NewPopulation(population, 5, 0x2, climate, recipe);
  NewHollowPopulation(fresh_population, 5, 0x2);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali ",
                      "hlisdokali hwapdokali");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolve_generation(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  // fresh population health must be equal to or greater than previous
  // generation.
  uint32_t fresh_population_health[30];
  uint64_t fresh_champion_iteration_sequence[30];
  population_quiz(fresh_population, training_sequence, program_produce,
                  fresh_population_health);
  champion_choose(fresh_population.length, fresh_population_health,
                  fresh_champion_iteration_sequence);
  mu_assert("", phrase_to_phrase_compare(
                    population.people[champion_iteration_sequence[0]],
                    fresh_population.people[0]) == truth_WORD);
  DEBUGPRINT(("%X fresh_health, %X old health, %X 0 health\n",
              fresh_population_health[fresh_champion_iteration_sequence[0]],
              population_health[champion_iteration_sequence[0]],
              fresh_population_health[0]));
  mu_assert("", fresh_population_health[fresh_champion_iteration_sequence[0]] >=
                    population_health[champion_iteration_sequence[0]]);
  repeat(3, DEBUGPRINT(("%X iterator\n", iterator)); text_phrase_print(
             population.people[champion_iteration_sequence[iterator]]);
         text_phrase_print(fresh_population.people[iterator]);
         text_page_print(fresh_population.people[iterator].page); DEBUGPRINT(
             ("%X health \n",
              (population_health[champion_iteration_sequence[iterator]]))););
  // mu_assert("#TODO", population_health[champion_iteration_sequence[0]]
  //     >= 0xFFF0);
  return 0;

}


const char *quiz_evolve_generation_3() {
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  NewTextPhrase(recipe, 0x2,
                "hyikzrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  // training sequence
  NewLongTrainingSequence(training_sequence, 4, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.b.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.c.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.d.prih.zikali ",
                      "psasgina zi.prih.A.prih.zikali "
                      "psasgina zi.prih.B.prih.zikali "
                      "psasgina zi.prih.C.prih.zikali "
                      "psasgina zi.prih.D.prih.zikali ");
  NewHollowPopulation(population, 30, 0x4);
  neo_population_establish(climate, recipe, population);
  NewHollowPopulation(fresh_population, 30, 0x4);
  text_phrase_print(training_sequence.input);
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolve_generation(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  uint16_t max_length = 0;
  repeat(30, 
      max_length = max(max_length,  fresh_population.people[iterator].length);
      DEBUGPRINT(("%X length, %lX iterator\n", 
          fresh_population.people[iterator].length,
          iterator));
      text_phrase_print(
        fresh_population.people[iterator]);
      );
  mu_assert("", max_length > 0x30);
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x10);
  return 0;
}

const char *quiz_evolve_generation() {
  mu_run_quiz(quiz_evolve_generation_1);
  mu_run_quiz(quiz_evolve_generation_2);
  mu_run_quiz(quiz_evolve_generation_3);
  return 0;
}

const char *quiz_evolutionary_island_establish_1() {
  NewClimate(climate, 0xACDFE29, 0x80, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali",
                      "hlisdokali hwapdokali");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  repeat(3,
         DEBUGPRINT(("%lX iterator, %X \n", iterator,
                     population_health[champion_iteration_sequence[iterator]]));
         text_phrase_print(
             fresh_population.people[champion_iteration_sequence[iterator]]););

  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  mu_assert("", phrase_letters_compare(
                    fresh_population.people[champion_iteration_sequence[0]],
                    "hfakdoyuplustu") == truth_WORD);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x10);
  return 0;
}
const char *quiz_evolutionary_island_establish_2() {
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  NewTextPhrase(recipe, 0x2,
                "hyikhyikzrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  // training sequence
  NewLongTrainingSequence(training_sequence, 4, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.b.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.c.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.d.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.B.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.C.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.D.prih.zikali ");
  NewHollowPopulation(population, 30, 0x4);
  neo_population_establish(climate, recipe, population);
  NewHollowPopulation(fresh_population, 30, 0x4);
  text_phrase_print(training_sequence.input);
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  uint16_t max_length = 0;
  repeat(30, 
      max_length = max(max_length,  fresh_population.people[iterator].length);
      DEBUGPRINT(("%X length, %lX iterator\n", 
          fresh_population.people[iterator].length,
          iterator));
      text_phrase_print(
        fresh_population.people[iterator]);
      );
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x20);
  return 0;
}

const char *quiz_evolutionary_island_establish(){
  mu_run_quiz(quiz_evolutionary_island_establish_1);
  mu_run_quiz(quiz_evolutionary_island_establish_2);
  return 0;
}

const char *quiz_health_assess_1() {
  // simple test case, a plus one program
  NewTrainingSequence(training_sequence, 2, "tyutdokali hyikdokali",
                      "tyindokali tyutdokali");
  NewTextPhrase(program_produce, 2, "tyindokali tyutdokali");
  NewTextPhrase(program, 4, "hyikdoyi plustu");

  uint32_t health;
  health_assess(training_sequence, program, program_produce, &health);
  DEBUGPRINT(("%X health\n", health));
  mu_assert("", health == 0xFFFC);
  return 0;
}
const char *quiz_health_assess_null() {
  // simple test case, a plus one program
  NewTrainingSequence(training_sequence, 2, "tyutdokali hyikdokali",
                      "tyindokali tyutdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 4, "hyikdoyi plustu");

  uint32_t health;
  health_assess(training_sequence, program, program_produce, &health);
  DEBUGPRINT(("%X health\n", health));
  mu_assert("", health == 0xC);
  return 0;
}
const char *quiz_health_assess_2() {
  // simple test case, a plus one program
  NewTrainingSequence(training_sequence, 2, "tyutdokali hyikdokali",
                      "tyindokali tyutdokali");
  NewTextPhrase(program_produce, 2, "tyindokali tyutdokali");
  NewTextPhrase(program, 4, "hyikdoyi plustu");
  uint32_t health;
  health_assess(training_sequence, program, program_produce, &health);
  DEBUGPRINT(("%X health\n", health));
  mu_assert("", health == 0xFFFC);
  return 0;
}
const char *quiz_health_assess_with_program_quiz() {
  // simple test case, a plus one program
  NewTrainingSequence(training_sequence, 2, "tyutdokali hyikdokali",
                      "tyindokali tyutdokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 4, "hyikdoyu plustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  uint32_t health;
  health_assess(training_sequence, program, program_produce, &health);
  text_phrase_print(program_produce);
  DEBUGPRINT(("%X health\n", health));
  mu_assert("", health == 0xFFFC);
  return 0;
}
const char *quiz_health_assess_with_multiline_program_quiz() {
  // simple test case, a plus one program
  DEBUGPRINT();
  NewTrainingSequence(training_sequence, 2, "tyutdokali hyikdokali",
                      "ksasdokali tyindokali");
  NewTextPhrase(program_produce, 2, "");
  NewTextPhrase(program, 4, "hyikdoyu plustu hyikdoyu plustu");
  program_produce = program_quiz(training_sequence, program, program_produce);
  uint32_t health;
  health_assess(training_sequence, program, program_produce, &health);
  text_phrase_print(program_produce);
  DEBUGPRINT(("%X health\n", health));
  mu_assert("", health == 0xFFF8);
  return 0;
}

const char *quiz_health_assess() {
  mu_run_quiz(quiz_health_assess_1);
  mu_run_quiz(quiz_health_assess_2);
  mu_run_quiz(quiz_health_assess_null);
  mu_run_quiz(quiz_health_assess_with_program_quiz);
  mu_run_quiz(quiz_health_assess_with_multiline_program_quiz);
  return 0;
}

const char *quiz_evolutionary_programmer() {
  mu_run_quiz(quiz_random_seed_establish);
  mu_run_quiz(quiz_phrase_improve);
  mu_run_quiz(quiz_person_establish);
#ifndef EMSCRIPTEN
  mu_run_quiz(quiz_population_establish);
#endif
  mu_run_quiz(quiz_recipe_improve);
  mu_run_quiz(quiz_program_quiz);
  mu_run_quiz(quiz_health_assess);
  mu_run_quiz(quiz_population_quiz);
  mu_run_quiz(quiz_champion_choose);
  mu_run_quiz(quiz_person_copy);
  mu_run_quiz(quiz_population_copy);
  mu_run_quiz(quiz_champion_copy);
  mu_run_quiz(quiz_recipe_switch);
  mu_run_quiz(quiz_recipe_merge);
  mu_run_quiz(quiz_person_compression_recipe_merge);
  mu_run_quiz(quiz_recipe_delete);
  mu_run_quiz(quiz_person_compression_recipe_delete);
  mu_run_quiz(quiz_htin_transmutation);
  mu_run_quiz(quiz_person_transmutation);
  mu_run_quiz(quiz_population_transmutation);
  mu_run_quiz(quiz_person_compression);
  mu_run_quiz(quiz_population_refresh);
  // mu_assert("#TODO quiz_champion_compression", 1 == 0);
  mu_run_quiz(quiz_champion_compression);
  mu_run_quiz(quiz_evolve_generation);
  // mu_assert("#TODO quiz_evolutionary_island_establish", 1 == 0);
  mu_run_quiz(quiz_evolutionary_island_establish);
  return 0;
}
#ifndef EMSCRIPTEN
#ifndef VALGRIND
const char *quiz_opencl() {
  mu_run_quiz(quiz_opencl_CPU_hardware);
  // mu_run_quiz(quiz_opencl_compile_kernel);
  // mu_run_quiz(quiz_opencl_run_kernel);
  // mu_run_quiz(quiz_opencl_run_lwonprom2);
  // mu_run_quiz(quiz_opencl_run_lwonprom3);
  // mu_run_quiz(quiz_opencl_run_lwonprom4);
  // mu_run_quiz(quiz_opencl_run_lwonprom5);
  // mu_run_quiz(quiz_opencl_run_lwonprom6);
  // mu_run_quiz(quiz_opencl_run_lwonprom7);
  return 0;
}
#endif
#endif

const char *quiz_emscripten() {
  mu_assert("#TODO", 0 == 1);
  return 0;
}

const char *quiz_write_theWord_toThePhrase_atThePlace() {
  NewTextPhrase(phrase, 1, "hwacca");
  write_theWord_toThePhrase_atThePlace(one_WORD, phrase, 1);
  mu_assert("", phrase_letters_compare(phrase, "hyikca"));
  write_theWord_toThePhrase_atThePlace(two_WORD, phrase, 3);
  mu_assert("", phrase_letters_compare(phrase, "hyikcatyut"));
  return 0;
}

const char *quiz_write() {
  mu_run_quiz(quiz_write_theWord_toThePhrase_atThePlace);
  return 0;
}

const char *quiz_neo_phrase_translate_null() {
  NewHollowPhrase(input);
  NewTextPad(text, 0x100);
  text = neo_phrase_translate(input, text, fluent_WORD);
  mu_assert("", text.length == 0);
  return 0;
};
const char *quiz_text_phrase_print_null() {
  NewHollowPhrase(input);
  mu_assert("", phrase_letters_compare(input, "") == truth_WORD);
  return 0;
};
const char *quiz_text_phrase_print() {
  mu_run_quiz(quiz_neo_phrase_translate_null);
  mu_run_quiz(quiz_text_phrase_print_null);
  return 0;
}
const char *quiz_knowledge_interpret_null() {
  NewPagePhrase(knowledge, 1);
  NewPagePhrase(input, 1);
  NewPagePhrase(produce, 1);
  knowledge_interpret(knowledge, input, produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}

const char *quiz_knowledge_interpret_1() {
  NewPagePhrase(knowledge, 2);
  NewTextPhrase(input, 1, "pyacli");
  NewPagePhrase(produce, 1);
  produce = knowledge_interpret(knowledge, input, produce);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  text_page_print(input.page);
  mu_assert("", phrase_letters_compare(produce, "pyacli") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_2() {
  NewPagePhrase(knowledge, 2);
  NewTextPhrase(input, 1, "hyikdoka hyikdoyu plustu");
  NewPagePhrase(produce, 1);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(input.page);
  text_phrase_print(input);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_3() {
  NewTextPhrase(knowledge, 2, "tyutdokali");
  NewTextPhrase(input, 1, " hyikdoyu plustu");
  NewPagePhrase(produce, 1);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(input.page);
  text_phrase_print(input);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_4() {
  NewTextPhrase(knowledge, 2, "");
  NewTextPhrase(input, 1, "hyikdokali");
  NewPagePhrase(produce, 1);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(input.page);
  text_phrase_print(input);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_5() {
  NewTextPhrase(knowledge, 4, "tyutdokali");
  NewTextPhrase(input, 2, " hyikdoyu plustu ksasdoyu plustu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  Page_print(produce.page);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hsipdokali") == truth_WORD);
  return 0;
}

const char *quiz_knowledge_interpret_6() {
  NewTextPhrase(knowledge, 4, "tyutdokali");
  NewTextPhrase(input, 3, " hyikdoyu plustu zrondoyuplustu hyikdoyu plustu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdokali") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_7() {
  NewTextPhrase(knowledge, 4, "");
  NewTextPhrase(input, 3, "pyacli");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "pyacli") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_1() {
  NewTextPhrase(produce, 2, "tyutdokali");
  NewTextPhrase(variable, 1, "tyindokali");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}

const char *quiz_reform_theParagraph_byTheVariable_2() {
  NewTextPhrase(produce, 2, "tyindokali");
  NewTextPhrase(variable, 1, "tyindokali");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_3() {
  NewTextPhrase(produce, 2, "tyindokali");
  NewTextPhrase(variable, 1, "");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_4() {
  NewTextPhrase(produce, 2, "pyacli");
  NewTextPhrase(variable, 1, "hfacli");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "pyacli hfacli") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_5() {
  NewTextPhrase(produce, 2, "nrupgika tyutdokali");
  NewTextPhrase(variable, 1, "hyikdokali");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "nrupgika tyutdokali hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_6() {
  NewTextPhrase(produce, 2, "ksasdokali");
  NewTextPhrase(variable, 1, "nrupgika tyutdokali");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "ksasdokali nrupgika tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_7() {
  NewTextPhrase(produce, 2, "lyatgina zi.prih.a.prih.zika li");
  NewTextPhrase(variable, 1, "psasgina zi.prih.a.prih.zika li");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "lyatgina zi.prih.a.prih.zika li"
                             "psasgina zi.prih.a.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_reform_theParagraph_byTheVariable_8() {
  NewTextPhrase(produce, 4, "hnucgina hyikdokali tyindokali");
  NewTextPhrase(variable, 1, "ksasdokali");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hyikdoka li"
                                                "ksasdoka li") == truth_WORD);
  return 0;
}

const char *quiz_reform_theParagraph_byTheVariable_boolean() {
  NewTextPhrase(produce, 4,
                "blu7ngina cyinbluhka li "
                "lyatgina zi.prih.A.prih.zikali");
  NewTextPhrase(variable, 1, "blu7ngina syanbluhka li ");
  produce = reform_theParagraph_byTheVariable(produce, variable);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "lyatgina zi.prih.A.prih.zikali"
                                                "blu7ngina syanbluhka li") ==
                    truth_WORD);
  return 0;
}

const char *quiz_reform_theParagraph_byTheVariable() {
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_1);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_2);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_3);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_4);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_5);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_6);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_7);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_8);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable_boolean);
  return 0;
}
const char *quiz_hollow_thePhrase_byThePhrase_1() {
  NewTextPhrase(input, 2, "hyikdokali hyikdoyuli");
  struct Phrase test_input = input;
  test_input.length -= 0x10;
  test_input.begin += 0x10;
  input = hollow_thePhrase_byThePhrase(input, test_input);
  text_phrase_print(input);
  Page_print(input.page);
  DEBUGPRINT(("%X\n", input.page.lines[0][0]));
  text_page_print(input.page);
  DEBUGPRINT(("%X input.length\n", input.length));
  // input.length = 0x10;
  mu_assert("", phrase_letters_compare(input, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_hollow_thePhrase_byThePhrase_2() {
  NewTextPhrase(input, 2, "tyindokali");
  input = hollow_thePhrase_byThePhrase(input, input);
  text_phrase_print(input);
  Page_print(input.page);
  DEBUGPRINT(("%X\n", input.page.lines[0][0]));
  text_page_print(input.page);
  DEBUGPRINT(("%X input.length\n", input.length));
  // input.length = 0x10;
  mu_assert("", phrase_letters_compare(input, "") == truth_WORD);
  repeat(LINE_LONG, mu_assert("", input.page.lines[0][iterator] == 0));
  return 0;
}
const char *quiz_hollow_thePhrase_byThePhrase_3() {
  NewTextPhrase(input, 3, "hnucgina hyikdokali tyindokali");
  struct Phrase variable = input;
  variable.length = 0x11;
  variable.begin = 0x11;
  input = hollow_thePhrase_byThePhrase(input, variable);
  text_phrase_print(input);
  Page_print(input.page);
  DEBUGPRINT(("%X\n", input.page.lines[0][0]));
  text_page_print(input.page);
  DEBUGPRINT(("%X input.length\n", input.length));
  // input.length = 0x10;
  mu_assert("",
            phrase_letters_compare(input, "hnucgina hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_hollow_thePhrase_byThePhrase_4() {
  NewTextPhrase(input, 2, "hnucgina hyikdokali tyindokali");
  struct Phrase variable = input;
  variable.length = 0x5;
  variable.begin = 0x11;
  input = hollow_thePhrase_byThePhrase(input, variable);
  text_phrase_print(input);
  Page_print(input.page);
  DEBUGPRINT(("%X\n", input.page.lines[0][0]));
  text_page_print(input.page);
  DEBUGPRINT(("%X input.length\n", input.length));
  // input.length = 0x10;
  mu_assert("",
            phrase_letters_compare(input, "hnucgina hyikdokali") == truth_WORD);
  mu_assert("", input.begin == 1 && input.length <= 0xF);
  repeat(LINE_LONG, mu_assert("", input.page.lines[1][iterator] == 0));
  return 0;
}
const char *quiz_hollow_thePhrase_byThePhrase_compress() {
  NewTextPhrase(input, 2, "hnucgina hyikdokali tyindokali");
  struct Phrase variable = input;
  variable.length = 0xE;
  variable.begin = 0x1;
  input = hollow_thePhrase_byThePhrase(input, variable);
  text_phrase_print(input);
  Page_print(input.page);
  DEBUGPRINT(("%X\n", input.page.lines[0][0]));
  text_page_print(input.page);
  DEBUGPRINT(("%X input.length\n", input.length));
  // input.length = 0x10;
  text_phrase_print(input);
  mu_assert("", phrase_letters_compare(input, "tyindokali") == truth_WORD);
  mu_assert("", input.begin == 1 && input.length <= 0xF);
  repeat(LINE_LONG, mu_assert("", input.page.lines[1][iterator] == 0));
  return 0;
}
const char *quiz_is_thePhrase_aPerfectIndependentclause() {
  NewTextPhrase(input, 1, "hyikdokali");
  mu_assert("", is_thePhrase_aPerfectIndependentClause(input) == truth_WORD);
  input.begin += 1;
  mu_assert("", is_thePhrase_aPerfectIndependentClause(input) == lie_WORD);
  NewTextPhrase(input2, 2, "hyikdokali cyinbluhkali");
  mu_assert("", is_thePhrase_aPerfectIndependentClause(input2) == lie_WORD);
  input2.length = 0xE;
  mu_assert("", is_thePhrase_aPerfectIndependentClause(input2) == truth_WORD);
  input2.begin = 0x11;
  mu_assert("", is_thePhrase_aPerfectIndependentClause(input2) == truth_WORD);

  return 0;
}
const char *quiz_hollow_thePhrase_byThePhrase() {
  mu_run_quiz(quiz_hollow_thePhrase_byThePhrase_1);
  mu_run_quiz(quiz_hollow_thePhrase_byThePhrase_2);
  mu_run_quiz(quiz_hollow_thePhrase_byThePhrase_3);
  mu_run_quiz(quiz_hollow_thePhrase_byThePhrase_4);
  mu_run_quiz(quiz_is_thePhrase_aPerfectIndependentclause);
  mu_run_quiz(quiz_hollow_thePhrase_byThePhrase_compress);
  return 0;
}
const char *quiz_knowledge_interpret_8() {
  NewTextPhrase(knowledge, 4, "hnucgina hlisdoka li");
  NewTextPhrase(input, 2, "hnucgika tyutdoyu plustu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina hwapdokali") ==
                    truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_9() {
  NewTextPhrase(knowledge, 4, "");
  NewTextPhrase(input, 4,
                "hnucgina hyikdoka li tyindoka li"
                "hnucgiyu plustu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "hnucgina hyikdokali ksasdokali") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_10() {
  NewTextPhrase(knowledge, 4, "hnucgina hyikdoka li tyindoka li");
  NewTextPhrase(input, 4, "hnucgiyu plustu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdokali") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_11() {
  NewTextPhrase(knowledge, 4, "lyatgina zi.prih.a.prih.zika li ");
  NewTextPhrase(input, 4,
                "tyutzronhtipdoyu psasgiyi lyatgika grettu "
                "twundoyu psasgiyi lyatgika grettu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "psasgina zi.prih.X.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_12() {
  NewTextPhrase(knowledge, 4, "lyatgina zi.prih.a.prih.zika li ");
  NewTextPhrase(input, 4,
                "tyutzrondoyu psasgiyi lyatgika grettu "
                "");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "psasgina zi.prih.A.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_multi_conditional() {
  NewTextPhrase(knowledge, 4, "blu7ngina cyinbluhkali"
                               "lyatgina zi.prih.a.prih.zikali");
  NewTextPhrase(input, 4,
      "hliszrondopwih lyatgika djancu blu7ngika gruttu"
      "blu7ngika cu tyutzrondoyu psasgiyi lyatgika grettu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret_multi_conditional2() {
  NewTextPhrase(knowledge, 4, "blu7ngina cyinbluhkali"
                               "lyatgina zi.prih.`.prih.zikali");
  NewTextPhrase(input, 4,
      "hliszrondopwih lyatgika djancu blu7ngika gruttu"
      "blu7ngika cu tyutzrondoyu psasgiyi lyatgika grettu");
  text_phrase_print(input);
  text_page_print(input.page);
  NewPagePhrase(produce, 4);
  produce = knowledge_interpret(knowledge, input, produce);
  text_page_print(knowledge.page);
  text_phrase_print(knowledge);
  text_page_print(produce.page);
  text_phrase_print(produce);
  phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "") == truth_WORD);
  return 0;
}
const char *quiz_knowledge_interpret() {
  mu_run_quiz(quiz_hollow_thePhrase_byThePhrase);
  mu_run_quiz(quiz_reform_theParagraph_byTheVariable);
  mu_run_quiz(quiz_knowledge_interpret_null);
  mu_run_quiz(quiz_knowledge_interpret_1);
  mu_run_quiz(quiz_knowledge_interpret_2);
  mu_run_quiz(quiz_knowledge_interpret_3);
  mu_run_quiz(quiz_knowledge_interpret_4);
  mu_run_quiz(quiz_knowledge_interpret_5);
  mu_run_quiz(quiz_knowledge_interpret_6);
  mu_run_quiz(quiz_knowledge_interpret_8);
  mu_run_quiz(quiz_knowledge_interpret_9);
  mu_run_quiz(quiz_knowledge_interpret_10);
  mu_run_quiz(quiz_knowledge_interpret_11);
  mu_run_quiz(quiz_knowledge_interpret_12);
  mu_run_quiz(quiz_knowledge_interpret_multi_conditional);
  mu_run_quiz(quiz_knowledge_interpret_multi_conditional2);
  return 0;
}
const char *quiz_realisMood_interpret() {
  NewTextPhrase(input, 2, "pyacli");
  NewPagePhrase(produce, 2);
  produce = neo_realisMood_interpret(input, produce);
  text_phrase_print(input);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "pyacli") == truth_WORD);
  return 0;
}
const char *quiz_interrogativeMood_interpret() {
  NewTextPhrase(input, 2, "pyacri");
  NewPagePhrase(produce, 2);
  produce = neo_realisMood_interpret(input, produce);
  text_phrase_print(input);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "pyacri") == truth_WORD);
  return 0;
}

const char *quiz_independentClause_interpret_1() {
  NewPagePhrase(knowledge, 2);
  NewTextPhrase(input, 2, "hyikdokali");
  NewPagePhrase(produce, 1);
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  text_phrase_print(produce);
  text_page_print(produce.page);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_2() {
  NewPagePhrase(knowledge, 2);
  NewTextPhrase(input, 2, "pyacli");
  NewPagePhrase(produce, 1);
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  mu_assert("", phrase_letters_compare(produce, "pyacli") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_3() {
  NewTextPhrase(knowledge, 2, "hyikdokali");
  NewTextPhrase(input, 2, "tyutdoyu plustu");
  NewPagePhrase(produce, 1);
  word_t done = lie_WORD;
  text_phrase_print(input);
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyindokali") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_4() {
  NewTextPhrase(knowledge, 2, "hyikdokali");
  NewTextPhrase(input, 2, "hyikdoyu plustu zrondoyu plustu");
  NewPagePhrase(produce, 1);
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "tyutdokali") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_5() {
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li tyindokali ");
  NewTextPhrase(input, 2, "hnucgiyu plustu");
  NewPagePhrase(produce, 1);
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdokali") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_6() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li");
  NewTextPhrase(input, 1, "hnucgika tyutdoyu plustu");
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hnucgina tyindoka li") ==
                    truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_letter_subtract() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2,
                "lyatgina zi.prih.c.prih.zikali psasgina zi.prih..prih.zikali");
  text_phrase_print(knowledge);
  Page_print(knowledge.page);
  NewTextPhrase(input, 1, "tyutdoyu psasgiyi lyatgika grettu");
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "psasgina zi.prih.a.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_letter_subtract_psas() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina zi.prih.c.prih.zikali");
  text_phrase_print(knowledge);
  Page_print(knowledge.page);
  NewTextPhrase(input, 1, "hnucgika tyutdoyu psasgiyi grettu");
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "psasgina zi.prih.a.prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_letter_subtract2() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 4, "lyatgina zi.prih.a.prih.zika li ");
  NewTextPhrase(input, 4, "tyutzronhtipdoyu psasgiyi lyatgika grettu ");
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(
                    produce, "psasgina zi.prih..prih.zika li") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_variable3() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "hnucgina hyikdoka li tyindoka li");
  NewTextPhrase(input, 1, "hnucgiyu plustu");
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdoka li") == truth_WORD);
  return 0;
}
const char *quiz_independentClause_interpret_boolean() {
  NewPagePhrase(produce, 1);
  NewTextPhrase(knowledge, 2, "blu7ngina cyinbluhka li tyindoka li");
  NewTextPhrase(input, 1, "hnucgiyu plustu");
  word_t done = lie_WORD;
  produce = neo_independentClause_interpret(knowledge, &done, input, produce);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "ksasdoka li") == truth_WORD);
  return 0;
}

const char *quiz_independentClause_interpret() {
  mu_run_quiz(quiz_independentClause_interpret_1);
  mu_run_quiz(quiz_independentClause_interpret_2);
  mu_run_quiz(quiz_independentClause_interpret_3);
  mu_run_quiz(quiz_independentClause_interpret_4);
  mu_run_quiz(quiz_independentClause_interpret_5);
  mu_run_quiz(quiz_independentClause_interpret_6);
  mu_run_quiz(quiz_independentClause_interpret_letter_subtract);
  mu_run_quiz(quiz_independentClause_interpret_letter_subtract2);
  mu_run_quiz(quiz_independentClause_interpret_letter_subtract_psas);
  mu_run_quiz(quiz_independentClause_interpret_variable3);
  // mu_run_quiz(quiz_independentClause_interpret_boolean);
  return 0;
}
const char *quiz_drop_htin_null() {
  NewTextPhrase(input, 2, "hyikdokali");
  NewPagePhrase(produce, 2);
  produce = drop_htin(input, 0);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}
const char *quiz_drop_htin_1() {
  NewTextPhrase(input, 2, "hyikdokali");
  NewPagePhrase(produce, 2);
  produce = drop_htin(input, 1);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "") == truth_WORD);
  return 0;
}
const char *quiz_drop_htin_2() {
  NewTextPhrase(input, 2, "tyindokali hyikdokali");
  NewPagePhrase(produce, 2);
  produce = drop_htin(input, 1);
  text_phrase_print(produce);
  mu_assert("", phrase_letters_compare(produce, "hyikdokali") == truth_WORD);
  return 0;
}

const char *quiz_drop_htin() {
  mu_run_quiz(quiz_drop_htin_null);
  mu_run_quiz(quiz_drop_htin_1);
  mu_run_quiz(quiz_drop_htin_2);
  return 0;
}

const char *quiz_phrase_code_establish_verb() {
  NewTextPhrase(input, 1, "plustu");
  uint64_t produce = phrase_code_establish(input);
  uint16_t *phrase_code = (uint16_t *)&produce;
  DEBUGPRINT(("%lX phrase_code\n", produce));
  mu_assert("", produce != 0);
  // mu_assert("", phrase_code[0] == deontic_mood_GRAMMAR);
  mu_assert("", phrase_code[0] == plus_WORD);
  return 0;
}
const char *quiz_phrase_code_establish_verb1() {
  NewTextPhrase(input, 1, "hyikplustu");
  uint64_t produce = phrase_code_establish(input);
  uint16_t *phrase_code = (uint16_t *)&produce;
  DEBUGPRINT(("%lX phrase_code\n", produce));
  mu_assert("", produce != 0);
  // mu_assert("", phrase_code[0] == deontic_mood_GRAMMAR);
  mu_assert("", phrase_code[0] == plus_WORD);
  mu_assert("", phrase_code[1] == one_WORD);
  return 0;
}
const char *quiz_phrase_code_establish_number() {
  NewTextPhrase(input, 1, "hyikdoka");
  uint64_t produce = phrase_code_establish(input);
  uint16_t *phrase_code = (uint16_t *)&produce;
  DEBUGPRINT(("%lX phrase_code\n", produce));
  mu_assert("", produce != 0);
  mu_assert("", phrase_code[0] == accusative_case_GRAMMAR);
  mu_assert("", phrase_code[1] == NUMBER_QUOTE);
  return 0;
}
const char *quiz_phrase_code_establish_boolean() {
  NewTextPhrase(input, 1, "cyinbluhyu");
  uint64_t produce = phrase_code_establish(input);
  uint16_t *phrase_code = (uint16_t *)&produce;
  DEBUGPRINT(("%lX phrase_code\n", produce));
  mu_assert("", produce != 0);
  mu_assert("", phrase_code[0] == instrumental_case_GRAMMAR);
  mu_assert("", phrase_code[1] == boolean_GRAMMAR);
  return 0;
}
const char *quiz_phrase_code_establish_letter() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.ziyu");
  uint64_t produce = phrase_code_establish(input);
  uint16_t *phrase_code = (uint16_t *)&produce;
  DEBUGPRINT(("%lX phrase_code\n", produce));
  mu_assert("", produce != 0);
  mu_assert("", phrase_code[0] == instrumental_case_GRAMMAR);
  mu_assert("", phrase_code[1] == LETTER_QUOTE);
  return 0;
}

const char *quiz_phrase_code_establish_name() {
  NewTextPhrase(input, 1, "hnucgiyu");
  uint64_t produce = phrase_code_establish(input);
  uint16_t *phrase_code = (uint16_t *)&produce;
  DEBUGPRINT(("%lX phrase_code\n", produce));
  mu_assert("", produce != 0);
  mu_assert("", phrase_code[0] == instrumental_case_GRAMMAR);
  mu_assert("", phrase_code[1] == name_GRAMMAR);
  return 0;
}

const char *quiz_derive_quote_byte_length_number() {
  NewTextPhrase(input, 1, "hyikdo");
  input.length = 1;
  uint8_t quote_length = derive_quote_byte_length(input);
  DEBUGPRINT(("%X quote_length\n", quote_length));
  mu_assert("", quote_length == 2);
  return 0;
}
const char *quiz_derive_quote_byte_length_hollow() {
  NewTextPhrase(input, 1, "li");
  input.length = 1;
  uint8_t quote_length = derive_quote_byte_length(input);
  DEBUGPRINT(("%X quote_length\n", quote_length));
  mu_assert("", quote_length == 0);
  return 0;
}
const char *quiz_derive_quote_byte_length() {
  mu_run_quiz(quiz_derive_quote_byte_length_number);
  mu_run_quiz(quiz_derive_quote_byte_length_hollow);
  return 0;
}

const char *quiz_derive_aListOfQuoteAndGrammar_fromPhrase_mood() {
  NewTextPhrase(input, 1, "pyacli");
  NewPagePhrase(produce, 1);
  produce =
      derive_aListOfQuoteWordsAndGrammarWords_fromThePhrase(produce, input);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", produce.length == 1);
  mu_assert("", produce.page.lines[0][1] == realis_mood_GRAMMAR);
  return 0;
}
const char *quiz_derive_aListOfQuoteAndGrammar_fromPhrase_boolean() {
  NewTextPhrase(input, 1, "cyinbluhyu");
  NewPagePhrase(produce, 1);
  produce =
      derive_aListOfQuoteWordsAndGrammarWords_fromThePhrase(produce, input);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", produce.length == 2);
  mu_assert("", produce.page.lines[0][1] == boolean_GRAMMAR);
  mu_assert("", produce.page.lines[0][2] == instrumental_case_GRAMMAR);
  return 0;
}
const char *quiz_derive_aListOfQuoteAndGrammar_fromPhrase_number() {
  NewTextPhrase(input, 1, "hyikdoyu");
  NewPagePhrase(produce, 1);
  produce =
      derive_aListOfQuoteWordsAndGrammarWords_fromThePhrase(produce, input);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", produce.length == 2);
  mu_assert("", produce.page.lines[0][1] == NUMBER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == instrumental_case_GRAMMAR);
  return 0;
}
const char *quiz_derive_aListOfQuoteAndGrammar_fromPhrase_letter() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.ziyu");
  NewPagePhrase(produce, 1);
  produce =
      derive_aListOfQuoteWordsAndGrammarWords_fromThePhrase(produce, input);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", produce.length == 2);
  mu_assert("", produce.page.lines[0][1] == LETTER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == instrumental_case_GRAMMAR);
  return 0;
}
const char *quiz_derive_aListOfQuoteAndGrammar_fromPhrase() {
  mu_run_quiz(quiz_derive_aListOfQuoteAndGrammar_fromPhrase_mood);
  mu_run_quiz(quiz_derive_aListOfQuoteAndGrammar_fromPhrase_boolean);
  mu_run_quiz(quiz_derive_aListOfQuoteAndGrammar_fromPhrase_number);
  mu_run_quiz(quiz_derive_aListOfQuoteAndGrammar_fromPhrase_letter);
  return 0;
}
const char *quiz_is_grammar_word() {
  mu_assert("", is_grammar_word(realis_mood_GRAMMAR) == truth_WORD);
  mu_assert("", is_grammar_word(boolean_GRAMMAR) == truth_WORD);
  mu_assert("", is_grammar_word(boolean_WORD) == lie_WORD);
  mu_assert("", is_grammar_word(0) == lie_WORD);
  return 0;
}
const char *quiz_upcoming_grammar_or_quote_found_boolean() {
  NewTextPhrase(input, 1, "cyinbluhyu");
  struct Phrase produce = upcoming_grammar_or_quote_found(input);
  Page_print(produce.page);
  text_phrase_print(produce);
  mu_assert("", phrase_word_read(produce, 0) == boolean_GRAMMAR);
  return 0;
}

const char *quiz_upcoming_grammar_or_quote_found_letter() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.ziyu");
  struct Phrase produce = upcoming_grammar_or_quote_found(input);
  Page_print(produce.page);
  text_phrase_print(produce);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.length == 1);
  mu_assert("", phrase_word_read(produce, 0) == LETTER_QUOTE);
  return 0;
}
const char *quiz_upcoming_grammar_or_quote_found_number() {
  NewTextPhrase(input, 1, "hyikdoyu");
  struct Phrase produce = upcoming_grammar_or_quote_found(input);
  Page_print(produce.page);
  text_phrase_print(produce);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.length == 1);
  mu_assert("", phrase_word_read(produce, 0) == NUMBER_QUOTE);
  return 0;
}

const char *quiz_is_quote_code() {
  mu_assert("", is_quote_code(LETTER_QUOTE) == truth_WORD);
  mu_assert("", is_quote_code(0) == lie_WORD);
  mu_assert("", is_quote_code(234) == lie_WORD);
  return 0;
}
const char *quiz_upcoming_grammar_or_quote_found_case() {
  NewTextPhrase(input, 1, "pyacyu");
  struct Phrase produce = upcoming_grammar_or_quote_found(input);
  Page_print(produce.page);
  text_phrase_print(produce);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  mu_assert("", produce.begin == 2);
  mu_assert("", produce.length == 1);
  mu_assert("", phrase_word_read(produce, 0) == instrumental_case_GRAMMAR);
  return 0;
}
const char *quiz_upcoming_grammar_or_quote_found_hollow() {
  NewTextPhrase(input, 1, "pyacyu");
  input.begin = 3;
  struct Phrase produce = upcoming_grammar_or_quote_found(input);
  Page_print(produce.page);
  text_phrase_print(produce);
  DEBUGPRINT(
      ("%X produce.begin, %X produce.length\n", produce.begin, produce.length));
  mu_assert("", produce.begin == 3);
  mu_assert("", produce.length == 0);
  mu_assert("", phrase_word_read(produce, 0) == 0);
  return 0;
}
const char *quiz_upcoming_grammar_or_quote_found() {
  mu_run_quiz(quiz_is_quote_code);
  mu_run_quiz(quiz_upcoming_grammar_or_quote_found_boolean);
  mu_run_quiz(quiz_upcoming_grammar_or_quote_found_letter);
  mu_run_quiz(quiz_upcoming_grammar_or_quote_found_number);
  mu_run_quiz(quiz_upcoming_grammar_or_quote_found_case);
  mu_run_quiz(quiz_upcoming_grammar_or_quote_found_hollow);
  return 0;
}

const char *quiz_phrase_code_establish() {
  mu_run_quiz(quiz_upcoming_grammar_or_quote_found);
  mu_run_quiz(quiz_is_grammar_word);
  mu_run_quiz(quiz_derive_quote_byte_length);
  mu_run_quiz(quiz_derive_aListOfQuoteAndGrammar_fromPhrase);
  mu_run_quiz(quiz_phrase_code_establish_verb);
  mu_run_quiz(quiz_phrase_code_establish_verb1);
  mu_run_quiz(quiz_phrase_code_establish_number);
  mu_run_quiz(quiz_phrase_code_establish_boolean);
  mu_run_quiz(quiz_phrase_code_establish_letter);
  mu_run_quiz(quiz_phrase_code_establish_name);
  return 0;
}

const char *quiz_sort_series_establish_numberplus() {
  NewTextPhrase(input, 1, "hyikdoyu plustu");
  NewSortSeries(sort_series, 0x10);
  sort_series = sort_series_establish(input, sort_series);
  repeat(sort_series.length,
         DEBUGPRINT(("0x%lX sort_series[0x%lX]\n", sort_series.series[iterator],
                     iterator)););
  mu_assert("", sort_series.length = 2);
  mu_assert("", sort_series.series[0] == 0x881D287E);
  mu_assert("", sort_series.series[1] == 0x8A64);
  return 0;
}
const char *quiz_sort_series_establish_letternumberplus() {
  NewTextPhrase(input, 1, "hyikdoyu zi.prih.a.prih.zika plustu");
  NewSortSeries(sort_series, 0x10);
  sort_series = sort_series_establish(input, sort_series);
  repeat(sort_series.length,
         DEBUGPRINT(("0x%lX sort_series[0x%lX]\n", sort_series.series[iterator],
                     iterator)););
  mu_assert("", sort_series.length = 3);
  mu_assert("", sort_series.series[0] == 0x081D245E);
  mu_assert("", sort_series.series[1] == 0x881D287E);
  mu_assert("", sort_series.series[2] == 0x8A64);
  return 0;
}
const char *quiz_sort_series_establish_kayunumberplus() {
  NewTextPhrase(input, 1, " zi.prih.a.prih.zika hyikdoyu plustu");
  NewSortSeries(sort_series, 0x10);
  sort_series = sort_series_establish(input, sort_series);
  repeat(sort_series.length,
         DEBUGPRINT(("0x%lX sort_series[0x%lX]\n", sort_series.series[iterator],
                     iterator)););
  mu_assert("", sort_series.length = 3);
  mu_assert("", sort_series.series[0] == 0x081D245E);
  mu_assert("", sort_series.series[1] == 0x881D287E);
  mu_assert("", sort_series.series[2] == 0x8A64);
  return 0;
}
const char *quiz_sort_series_establish_boolean() {
  NewTextPhrase(input, 1, "cyinbluhkaculi");
  text_phrase_print(input);
  NewSortSeries(sort_series, 0x10);
  sort_series = sort_series_establish(input, sort_series);
  repeat(sort_series.length,
         DEBUGPRINT(("0x%lX sort_series[0x%lX]\n", sort_series.series[iterator],
                     iterator)););
  mu_assert("", sort_series.length = 1);
  mu_assert("", sort_series.series[0] == 0x538F245E);
  return 0;
}
const char *quiz_sort_series_establish_conditional() {
  NewTextPhrase(input, 1, "hyikdopwih tyindoka djancu");
  text_phrase_print(input);
  NewSortSeries(sort_series, 0x10);
  sort_series = sort_series_establish(input, sort_series);
  repeat(sort_series.length,
         DEBUGPRINT(("0x%lX sort_series[0x%lX]\n", sort_series.series[iterator],
                     iterator)););
  mu_assert("", sort_series.length = 3);
  mu_assert("", sort_series.series[0] == 0x881D245E);
  mu_assert("", sort_series.series[1] == 0x881D4127);
  mu_assert("", sort_series.series[2] == giant_WORD);
  return 0;
}
const char *quiz_sort_series_establish_number2() {
  NewTextPhrase(input, 2, "hyikdoka tyindoyu djancu hyikdoka tyindoyu plustu");
  input.begin = 9;
  input.length = 0xF;
  text_phrase_print(input);
  NewSortSeries(sort_series, 0x10);
  sort_series = sort_series_establish(input, sort_series);
  repeat(sort_series.length,
         DEBUGPRINT(("%08lX \n", sort_series.series[iterator])));
  mu_assert("", sort_series.length = 3);
  mu_assert("", sort_series.series[0] == 0x881D245E);
  mu_assert("", sort_series.series[1] == 0x881D287E);
  mu_assert("", sort_series.series[2] == plus_WORD);
  return 0;
}

const char *quiz_sort_series_establish() {
  mu_run_quiz(quiz_sort_series_establish_numberplus);
  mu_run_quiz(quiz_sort_series_establish_letternumberplus);
  mu_run_quiz(quiz_sort_series_establish_kayunumberplus);
  mu_run_quiz(quiz_sort_series_establish_number2);
  mu_run_quiz(quiz_sort_series_establish_boolean);
  mu_run_quiz(quiz_sort_series_establish_conditional);
  return 0;
}
const char *quiz_code_name_derive_number() {
  NewTextPhrase(input, 1, "hyikdoka plustu");
  code_name_t code_name = code_name_derive(input);
  DEBUGPRINT(("%X code_name\n", code_name));
  mu_assert("", code_name != 0);
  return 0;
}
const char *quiz_code_name_derive_dokayu() {
  NewTextPhrase(input, 1, "hyikdoka tyindoyu plustu");
  code_name_t code_name = code_name_derive(input);
  NewTextPhrase(input2, 1, "tyindoyu hyikdoka plustu");
  code_name_t code_name2 = code_name_derive(input2);
  DEBUGPRINT(("%X code_name\n", code_name));
  mu_assert("", code_name != 0);
  mu_assert("", code_name == code_name2);
  return 0;
}

const char *quiz_code_name_derive() {
  mu_run_quiz(quiz_code_name_derive_number);
  mu_run_quiz(quiz_code_name_derive_dokayu);
  return 0;
}

const char *quiz_code_establish() {
  mu_run_quiz(quiz_phrase_code_establish);
  mu_run_quiz(quiz_sort_series_establish);
  mu_run_quiz(quiz_code_name_derive);
  return 0;
}

const char *quiz_interpret() {
  mu_run_quiz(quiz_code_establish);
  mu_run_quiz(quiz_realisMood_interpret);
  mu_run_quiz(quiz_interrogativeMood_interpret);
  mu_run_quiz(quiz_independentClause_interpret);
  mu_run_quiz(quiz_drop_htin);
  mu_run_quiz(quiz_knowledge_interpret);
  return 0;
}

/*
// const char *quiz_pyash_to_json_null() {
//   NewText(model, "[{}]");
//   NewText(input, "");
//   NewTextPad(produce, 0x100);
//   produce = pyash_to_json(input, produce);
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_to_json_li() {
//   NewText(model, "[{\"mood\": \"li\"}]");
//   NewText(input, "li");
//   NewTextPad(produce, 0x100);
//   produce = pyash_to_json(input, produce);
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_to_json_pyacli() {
//   NewText(model, "[{\"mood\": \"li\", \"verb\": \"pyac\"}]");
//   NewText(input, "pyacli");
//   NewTextPad(produce, 0x100);
//   produce = pyash_to_json(input, produce);
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("#TODO", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_htin_to_json_li() {
//   NewText(model, "{\"mood\": \"li\"}");
//   NewTextPhrase(input, 1, "li");
//   json_t *root = json_object();
//   root = pyash_htin_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
// const char *quiz_pyash_htin_to_json_null() {
//   NewText(model, "{}");
//   NewTextPhrase(input, 1, "");
//   json_t *root = json_object();
//   root = pyash_htin_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_htin_to_json() {
//   mu_run_quiz(quiz_pyash_htin_to_json_null);
//   mu_run_quiz(quiz_pyash_htin_to_json_li);
//   return 0;
// }
// const char *quiz_pyash_phrase_to_json_null() {
//   NewText(model, "{}");
//   NewTextPhrase(input, 1, "");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
// const char *quiz_pyash_phrase_to_json_li() {
//   NewText(model, "{\"\": \"\"}");
//   NewTextPhrase(input, 1, "li");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
// const char *quiz_pyash_phrase_to_json_ka() {
//   NewText(model, "{\"ka\": \"\"}");
//   NewTextPhrase(input, 1, "ka");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_phrase_to_json_pyacka() {
//   NewText(model, "{\"ka\": \"pyac\"}");
//   NewTextPhrase(input, 1, "pyacka");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_phrase_to_json_hwacpyacka() {
//   NewText(model, "{\"ka\": \"hwacpyac\"}");
//   NewTextPhrase(input, 1, "hwacpyacka");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_phrase_to_json_htafdwesgina() {
//   NewText(model, "{\"na\": {\"gi\": \"htafdwes\"}}");
//   NewTextPhrase(input, 1, "htafdwesgina");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("#TODO", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
// const char *quiz_pyash_phrase_to_json_zikovlamda() {
//   NewText(model, "{\"filename\": \"hello.png\"}}");
//   NewTextPhrase(input, 1, "ziprih.hello.png.prihzikovlamda");
//   json_t *root = json_object();
//   root = pyash_phrase_to_json(input, root);
//   NewText(produce, json_dumps(root, 0));
//   DEBUGPRINT(("%s produce, %X produce.length\n", produce.letters,
produce.length));
//   DEBUGPRINT(("%s model, %X model.length\n", model.letters, model.length));
//   mu_assert("#TODO", text_compare(model, produce) == truth_WORD);
//   return 0;
// }
//
// const char *quiz_pyash_phrase_to_json() {
//   mu_run_quiz(quiz_pyash_phrase_to_json_null);
//   mu_run_quiz(quiz_pyash_phrase_to_json_li);
//   mu_run_quiz(quiz_pyash_phrase_to_json_ka);
//   mu_run_quiz(quiz_pyash_phrase_to_json_pyacka);
//   mu_run_quiz(quiz_pyash_phrase_to_json_hwacpyacka);
//   // mu_run_quiz(quiz_pyash_phrase_to_json_zikovlamda);
//   return 0;
// }
// const char *quiz_pyash_to_json() {
//   mu_run_quiz(quiz_pyash_phrase_to_json);
//   mu_run_quiz(quiz_pyash_htin_to_json);
//   mu_run_quiz(quiz_pyash_to_json_null);
//   mu_run_quiz(quiz_pyash_to_json_li);
//   mu_run_quiz(quiz_pyash_to_json_pyacli);
//   return 0;
// }
*/

const char *quiz_phrase_zero() {
  NewTextPhrase(input, 1, "hyikdokali");
  phrase_zero(input);
  repeat(LINE_LONG, mu_assert("", input.page.lines[0][iterator] == 0););
  mu_assert("", input.begin == 1);
  mu_assert("", input.length == 0);
  return 0;
}

const char *quiz_phrase_long_diagnose_1() {
  NewTextPhrase(produce, 1, "pyacli");
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 2);
  return 0;
}
const char *quiz_phrase_long_diagnose_2() {
  NewTextPhrase(produce, 1, "pyacli");
  produce.begin = 0;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 3);
  return 0;
}
const char *quiz_phrase_long_diagnose_3() {
  NewTextPhrase(produce, 1, "pyac");
  produce.begin = 1;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 1);
  return 0;
}
const char *quiz_phrase_long_diagnose_4() {
  NewTextPhrase(produce, 1, "hnucgi");
  produce.begin = 1;
  produce.length = 0;
  produce.length = phrase_long_diagnose(produce);
  Page_print(produce.page);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 2);
  return 0;
}
const char *quiz_phrase_long_diagnose_5() {
  NewTextPhrase(produce, 2, "hyikdokali hyikdoyu tyutdokali");
  produce.begin = 1;
  produce.length = 0xE;
  produce.length = phrase_long_diagnose(produce);
  DEBUGPRINT(("%X produce.length\n", produce.length));
  mu_assert("", produce.length == 4);
  return 0;
}

const char *quiz_phrase_long_diagnose() {
  mu_run_quiz(quiz_phrase_long_diagnose_1);
  mu_run_quiz(quiz_phrase_long_diagnose_2);
  mu_run_quiz(quiz_phrase_long_diagnose_3);
  mu_run_quiz(quiz_phrase_long_diagnose_4);
  mu_run_quiz(quiz_phrase_long_diagnose_5);
  return 0;
}

const char * quiz_round_thePhrase_toRoofLine() {
  struct Phrase phrase;
  phrase.length = 0xA;
  phrase = round_thePhrase_toRoofLine(phrase);
  DEBUGPRINT(("%X phrase.length\n", phrase.length));
  mu_assert("", phrase.length == 0x10);
  phrase = round_thePhrase_toRoofLine(phrase);
  DEBUGPRINT(("%X phrase.length\n", phrase.length));
  mu_assert("", phrase.length == 0x10);
  phrase.length = 0x11;
  phrase = round_thePhrase_toRoofLine(phrase);
  DEBUGPRINT(("%X phrase.length\n", phrase.length));
  mu_assert("", phrase.length == 0x20);
  return 0;
}

const char *quiz_phrase() {
  mu_run_quiz(quiz_phrase_zero);
  mu_run_quiz(quiz_phrase_long_diagnose);
  mu_run_quiz(quiz_round_thePhrase_toRoofLine);
  return 0;
}
const char *quiz_lwonprom_lettergiant_letter_basic_number() {
  NewClimate(climate, 0xACDFE2, 0x100, 0xC000);
  NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
  NewTextPhrase(program_produce, 2, "");
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hlishyikdokali hlistyutdokali ",
                      "ksashyikdokali ksastyutdokali ");
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  // repeat(30, DEBUGPRINT(("%lX iterator, %X \n", iterator,
  //                       population_health[iterator]));
  //       text_phrase_print(fresh_population.people[iterator]););

  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  // champion compressioin test
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFFC);
  // mu_assert("", phrase_letters_compare(
  //                   fresh_population.people[champion_iteration_sequence[0]],
  //                   "hfakdoyuplustu") == truth_WORD);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x10);
  return 0;
}

const char *quiz_lwonprom_lettergiant_letter_basic() {
  // make a new way of running evolutionary islands
  // where can give the training sequence, climate and recipe
  // and it will run through a bunch of islands,
  // and give the one that gives the result the fastest.
  // rapid_evolutionary_island_climate_found();
  NewClimate(climate, 0x5974C03C, 0x100, 0xC000);
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  NewTextPhrase(recipe, 0x1, "twundoyu psasgiyi lyatgika grettu");
  // training sequence
  NewTrainingSequence(training_sequence, 4,
                      "lyatgina zi.prih.a.prih.zikali "
                      "lyatgina zi.prih.b.prih.zikali "
                      "lyatgina zi.prih.c.prih.zikali "
                      "lyatgina zi.prih.d.prih.zikali ",
                      "psasgina zi.prih.A.prih.zikali "
                      "psasgina zi.prih.B.prih.zikali "
                      "psasgina zi.prih.C.prih.zikali "
                      "psasgina zi.prih.D.prih.zikali ");
  NewHollowPopulation(population, 30, 0x4);
  neo_population_establish(climate, recipe, population);
  NewHollowPopulation(fresh_population, 30, 0x4);
  text_phrase_print(training_sequence.input);
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  mu_assert("", phrase_letters_compare(
                    fresh_population.people[champion_iteration_sequence[0]],
                    "tyutzrondoyu psasgiyi lyatgika grettu") == truth_WORD);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x10);
  return 0;
}
const char *quiz_lwonprom_lettergiant_letter_conditional() {
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  NewTextPhrase(recipe, 0x2,
                "hyikzrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  // training sequence
  NewLongTrainingSequence(training_sequence, 4, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.b.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.c.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.d.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.B.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.C.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.D.prih.zikali ");
  NewHollowPopulation(population, 30, 0x4);
  neo_population_establish(climate, recipe, population);
  NewHollowPopulation(fresh_population, 30, 0x4);
  text_phrase_print(training_sequence.input);
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x20);
  return 0;
}
const char *quiz_lwonprom_lettergiant_letter_conditional2() {
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0x2F9DA4319721C729;
  climate.random_seed[1] = 0x6EA60BD46973DB0B;
  NewTextPhrase(recipe, 0x2,
                "ksaszrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu hyikhyikdoyu psasgiyi lyatgika grettu");
  // training sequence
  text_phrase_print(recipe);
  NewLongTrainingSequence(training_sequence, 8, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.m.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.!.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.0.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.Z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.A.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.`.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.M.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.Z.prih.zikali "
                      );
  long_text_phrase_print(training_sequence.input);
  NewHollowPopulation(population, 60, 0x2);
  neo_population_establish(climate, recipe, population);
  NewHollowPopulation(fresh_population, 60, 0x2);
  long_text_phrase_print(training_sequence.input);
  // population quiz
  uint32_t population_health[60];
  uint64_t champion_iteration_sequence[60];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  repeat(30, 
      text_phrase_print(
        fresh_population.people[champion_iteration_sequence[iterator]]);
      );
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x20);
  return 0;
}
const char *quiz_lwonprom_lettergiant_letter() {
  mu_run_quiz(quiz_lwonprom_lettergiant_letter_basic_number);
  mu_run_quiz(quiz_lwonprom_lettergiant_letter_basic);
  //mu_assert("#TODO quiz_lwonprom_lettergiant_letter_conditional", 1 == 0);
  mu_run_quiz(quiz_lwonprom_lettergiant_letter_conditional);
  mu_run_quiz(quiz_lwonprom_lettergiant_letter_conditional2);
  return 0;
}
const char *quiz_lwonprom_lettergiant_letterlist() {
  NewClimate(climate, 0xACDFE29, 0x80, 0xFFF0);
  NewTextPhrase(recipe, 0x1, "twundoyu plustu");
  NewPopulation(population, 30, 0x4, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x4);
  // training sequence
  NewTrainingSequence(training_sequence, 2, "hyikdokali tyindokali ",
                      "hlisdokali hwapdokali ");
  NewTextPhrase(program_produce, 2, "");
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  repeat(30, DEBUGPRINT(("%lX iterator, %X \n", iterator,
                         population_health[iterator]));
         text_phrase_print(fresh_population.people[iterator]););

  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  mu_assert("", phrase_letters_compare(
                    fresh_population.people[champion_iteration_sequence[0]],
                    "hfakdoyuplustu") == truth_WORD);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x10);
  return 0;
}
const char *quiz_lwonprom_lettergiant() {
  mu_run_quiz(quiz_lwonprom_lettergiant_letter);
  TODO(mu_assert("#TODO quiz_lwonprom_lettergiant_letterlist", 1 == 0));
  // mu_run_quiz(quiz_lwonprom_lettergiant_letterlist);
  return 0;
}

const char *quiz_lwonprom_A005846() {
  NewClimate(climate, 0xACDFE2, 0x80, 0xC000);
  NewTextPhrase(recipe, 0x3,
                "twundoyu plustu nrupgiyu plustu"
                "nrupgiyu hgoctu ");
  NewPopulation(population, 30, 0x3, climate, recipe);
  NewHollowPopulation(fresh_population, 30, 0x3);
  NewTextPhrase(program_produce, 2, "");
  // training sequence
  NewTrainingSequence(
      training_sequence, 3,
      "nrupgina zrondokali nrupgina hyikdokali nrupgina tyindokali ",
      "tyuttwundokali tyutslendokali tyuthpetdokali");
  // population quiz
  uint32_t population_health[30];
  uint64_t champion_iteration_sequence[30];
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  // repeat(30, DEBUGPRINT(("%lX iterator, %X \n", iterator,
  //                       population_health[iterator]));
  //       text_phrase_print(fresh_population.people[iterator]););

  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFF0);
  // champion compressioin test
  mu_assert("", population_health[champion_iteration_sequence[0]] >= 0xFFFC);
  // mu_assert("", phrase_letters_compare(
  //                   fresh_population.people[champion_iteration_sequence[0]],
  //                   "hfakdoyuplustu") == truth_WORD);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  DEBUGPRINT(("%X \n", population_health[champion_iteration_sequence[0]]));
  mu_assert("",
            fresh_population.people[champion_iteration_sequence[0]].length <=
                0x10);
  return 0;
}

const char *quiz_type_numbers() {
  const uint16_t number_sort =
      QUOTE_DENOTE | SIXTEEN_TIDBIT_SCALAR_THICK << SCALAR_THICK_BEGIN |
      UINT_SORT_DENOTE << SORT_DENOTE_BEGIN;
  DEBUGPRINT(("%X number_sort\n", number_sort));
  mu_assert("", number_sort == 0x881D);
  const uint16_t letter_sort =
      QUOTE_DENOTE | SIXTEEN_TIDBIT_SCALAR_THICK << SCALAR_THICK_BEGIN |
      LETTER_SORT_DENOTE << SORT_DENOTE_BEGIN;
  DEBUGPRINT(("%X letter_sort\n", letter_sort));
  mu_assert("", letter_sort == 0x81D);
  return 0;
}
const char *quiz_text_tail() {
  NewText(text, "hwacwu");
  struct Text produce_text = text_tail(text, 2);
  mu_assert("", produce_text.letters = text.letters + 4);
  mu_assert("", produce_text.length = 2);
  mu_assert("", produce_text.letters[0] = 'w');
  mu_assert("", produce_text.letters[1] = 'u');
  return 0;
}
const char *quiz_text() {
  mu_run_quiz(quiz_text_tail);
  return 0;
}
const char *quiz_process_quote_null() {
  NewText(text, "");
  NewPagePhrase(tablet, 1);
  uint8_t text_indexFinger = 0;
  struct Phrase produce = process_quote(tablet, text, &text_indexFinger);
  mu_assert("", produce.length == 0);
  return 0;
}
const char *quiz_process_quote_a() {
  NewText(text, ".prih.a.prih.zi");
  NewPagePhrase(tablet, 1);
  tablet.begin = 1;
  uint8_t text_indexFinger = 0;
  struct Phrase produce = process_quote(tablet, text, &text_indexFinger);
  DEBUGPRINT(("%X produce.length, %X produce.begin, %X text_indexFinger\n",
              produce.length, produce.begin, text_indexFinger));
  mu_assert("", produce.length == 2);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.page.lines[0][1] == LETTER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == 'a');
  mu_assert("", text_indexFinger == 15);
  return 0;
}
const char *quiz_process_quote_grave() {
  NewText(text, ".prih.`.prih.zi");
  NewPagePhrase(tablet, 1);
  tablet.begin = 1;
  uint8_t text_indexFinger = 0;
  struct Phrase produce = process_quote(tablet, text, &text_indexFinger);
  DEBUGPRINT(("%X produce.length, %X produce.begin, %X text_indexFinger\n",
              produce.length, produce.begin, text_indexFinger));
  mu_assert("", produce.length == 2);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.page.lines[0][1] == LETTER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == '`');
  mu_assert("", text_indexFinger == 15);
  return 0;
}
const char *quiz_process_quote_empty() {
  NewText(text, ".prih..prih.zi");
  NewPagePhrase(tablet, 1);
  tablet.begin = 1;
  uint8_t text_indexFinger = 0;
  struct Phrase produce = process_quote(tablet, text, &text_indexFinger);
  DEBUGPRINT(("%X produce.length, %X produce.begin, %X text_indexFinger\n",
              produce.length, produce.begin, text_indexFinger));
  mu_assert("", produce.length == 2);
  mu_assert("", produce.begin == 1);
  mu_assert("", produce.page.lines[0][1] == LETTER_QUOTE);
  mu_assert("", produce.page.lines[0][2] == 0);
  mu_assert("", text_indexFinger == 14);
  return 0;
}
const char *quiz_process_quote() {
  mu_run_quiz(quiz_process_quote_null);
  mu_run_quiz(quiz_process_quote_empty);
  mu_run_quiz(quiz_process_quote_a);
  mu_run_quiz(quiz_process_quote_grave);
  return 0;
}
const char *quiz_quote() {
  mu_run_quiz(quiz_process_quote);
  return 0;
}
const char *quiz_decoding_letter() {
  NewText(output, "zi.prih.a.prih.zi");
  NewTextPhrase(input, 1, "zi.prih.a.prih.zi");
  NewTextPad(produce, 0x100);
  produce = neo_phrase_translate(input, produce, fluent_WORD);
  text_print(produce);
  mu_assert("", text_compare(produce, output) == truth_WORD);
  return 0;
}
const char *quiz_decoding_letter_X() {
  NewText(output, "zi.prih.X.prih.zi");
  NewTextPhrase(input, 1, "zi.prih.X.prih.zi");
  NewTextPad(produce, 0x100);
  produce = neo_phrase_translate(input, produce, fluent_WORD);
  text_print(produce);
  mu_assert("", text_compare(produce, output) == truth_WORD);
  return 0;
}
const char *quiz_decoding_letter_A() {
  NewText(output, "zi.prih.A.prih.zi");
  NewTextPhrase(input, 1, "zi.prih.A.prih.zi");
  NewTextPad(produce, 0x100);
  produce = neo_phrase_translate(input, produce, fluent_WORD);
  text_print(produce);
  mu_assert("", text_compare(produce, output) == truth_WORD);
  return 0;
}
const char *quiz_decoding_letter_W() {
  NewText(output, "zi.prih.W.prih.zi");
  NewTextPhrase(input, 1, "zi.prih.W.prih.zi");
  NewTextPad(produce, 0x100);
  produce = neo_phrase_translate(input, produce, fluent_WORD);
  text_print(produce);
  mu_assert("", text_compare(produce, output) == truth_WORD);
  return 0;
}
const char *quiz_decoding_letter_Y() {
  NewText(output, "zi.prih.Y.prih.zi");
  NewTextPhrase(input, 1, "zi.prih.Y.prih.zi");
  NewTextPad(produce, 0x100);
  produce = neo_phrase_translate(input, produce, fluent_WORD);
  text_print(produce);
  mu_assert("", text_compare(produce, output) == truth_WORD);
  return 0;
}
const char *quiz_decoding_letters() {
  NewText(output, "zi.prih.A.prih.zi");
  NewTextPhrase(input, 1, "zi.prih.A.prih.zi");
  NewTextPad(produce, 0x100);
  repeat(58, ulong local_iterator = iterator + 0x41;
         conditional(local_iterator >= 0x5B && local_iterator < 0x61, continue)
             output.letters[8] = local_iterator;
         input.page.lines[0][2] = local_iterator; Page_print(input.page);
         produce = text_zero(produce);
         produce = neo_phrase_translate(input, produce, fluent_WORD);
         DEBUGPRINT(("%X produce.length\n", produce.length));
         text_print(produce);
         mu_assert("", text_compare(produce, output) == truth_WORD););
  return 0;
}
const char *quiz_decoding_multiline_sentence() {
  NewText(output, "tyutdopwih lyatgika djancu "
                  "tyutzrondoyu psasgiyi lyatgika grettu ");
  NewTextPhrase(input, 2,
                "tyutdopwih lyatgikadjancu"
                "tyutzrondoyu psasgiyi lyatgika grettu");
  NewTextPad(produce, 0x100);
  produce = neo_phrase_translate(input, produce, fluent_WORD);
  text_print(produce);
  mu_assert("", text_compare(produce, output) == truth_WORD);
  return 0;
}
const char *quiz_decoding() {
  mu_run_quiz(quiz_decoding_letter);
  mu_run_quiz(quiz_decoding_letter_W);
  mu_run_quiz(quiz_decoding_letter_X);
  mu_run_quiz(quiz_decoding_letter_Y);
  mu_run_quiz(quiz_decoding_letter_A);
  mu_run_quiz(quiz_decoding_letters);
  mu_run_quiz(quiz_decoding_multiline_sentence);
  return 0;
}
const char *quiz_rapid_evolutionary_island_climate_found() {
  NewClimate(climate, 0xACDFE2, 0x100, 0xC000);
  random_t success_random_seed[2];
  success_random_seed[0] = climate.random_seed[0];
  success_random_seed[1] = climate.random_seed[1];
  climate.max_population = 30;
  climate.max_program_length = 4;
  NewTextPhrase(recipe, 0x1, "twundoyu grettu");
  // NewPopulation(population, 30, 0x4, climate, recipe);
  // NewHollowPopulation(fresh_population, 30, 0x4);
  NewTrainingSequence(training_sequence, 2, "hlishyikdokali hlistyutdokali ",
                      "ksashyikdokali ksastyutdokali ");
  climate = rapid_evolutionary_island_climate_found(climate, recipe,
                                                    training_sequence);
  DEBUGPRINT(("%X climate.random_seed\n", climate.random_seed));
  mu_assert("", climate.random_seed[0] == success_random_seed[0]);
  mu_assert("", climate.random_seed[1] == success_random_seed[1]);
  return 0;
}

const char *quiz_rapid_evolutionary_island_climate_found_1() {
  NewClimate(climate, 0xACDFE2, 0x100, 0xC000);
  random_t success_random_seed[2];
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  climate.max_population = 30;
  climate.max_program_length = 4;
  NewTextPhrase(recipe, 0x1, "twundoyu psasgiyi lyatgika grettu");
  // training sequence
  NewTrainingSequence(training_sequence, 4,
                      "lyatgina zi.prih.a.prih.zikali "
                      "lyatgina zi.prih.b.prih.zikali "
                      "lyatgina zi.prih.c.prih.zikali "
                      "lyatgina zi.prih.d.prih.zikali ",
                      "psasgina zi.prih.A.prih.zikali "
                      "psasgina zi.prih.B.prih.zikali "
                      "psasgina zi.prih.C.prih.zikali "
                      "psasgina zi.prih.D.prih.zikali ");
  climate = rapid_evolutionary_island_climate_found(climate, recipe,
                                                    training_sequence);

  NewHollowPopulation(population, 0xFF, 0x20);
  NewHollowPopulation(fresh_population, 0xFF, 0x20);
  population.length = climate.max_population;
  repeat(climate.max_population,
         population.people[iterator].page.plength = climate.max_program_length);
  fresh_population.length = climate.max_population;
  repeat(climate.max_population,
         fresh_population.people[iterator].page.plength =
             climate.max_program_length);
  uint32_t population_health[0xFF];
  uint64_t champion_iteration_sequence[0xFF];
  DEBUGPRINT(("%lX %lX climate.random_seed, %X climate.temperature, "
              "%X climate.radioactivity\n",
              climate.random_seed[0], climate.random_seed[1],
              climate.temperature, climate.radioactivity));

  neo_population_establish(climate, recipe, population);
  //// generate the population based on the random seed
  //// evolutionary_island_establish
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  //// add to best champion climate list
  //// check if we have a passing champion
  uint8_t passing_champion =
      population_health[champion_iteration_sequence[0]] >= 0xFFF0;
  //// if yes, then break
  mu_assert("", passing_champion);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  text_phrase_print(population.people[champion_iteration_sequence[0]]);

  return 0;
}

const char *quiz_rapid_evolutionary_island_climate_found_2() {
  NewClimate(climate, 0x22743190, 0x100, 0xC000);
  climate.random_seed[0] = 0xD7B60223565AA773;
  climate.random_seed[1] = 0x12E0127215FDE945;
  climate.max_population = 60;
  climate.max_program_length = 2;
  NewTextPhrase(recipe, 0x2,
                "tyutzrondopwih lyatgika djancu blu7ngika gruttu "
                "blu7ngikacu tyutzrondoyu psasgiyi lyatgika grettu");
  // training sequence
  NewLongTrainingSequence(training_sequence, 8, 2,
                      "blu7ngina cyinbluhkali lyatgina zi.prih.a.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.m.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.!.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.0.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.Z.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.A.prih.zikali "
                      "blu7ngina cyinbluhkali lyatgina zi.prih.`.prih.zikali ",
                      "blu7ngina syanbluhkali psasgina zi.prih.A.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.M.prih.zikali "
                      "blu7ngina syanbluhkali psasgina zi.prih.Z.prih.zikali "
                      );
  climate = 
    rapid_evolutionary_island_climate_found(climate, recipe, training_sequence);
  DEBUGPRINT(("%lX %lX climate.random_seed, %X climate.temperature, "
              "%X climate.radioactivity\n",
              climate.random_seed[0], climate.random_seed[1],
              climate.temperature, climate.radioactivity));
  NewHollowPopulation(population, 0xFF, 0x20);
  NewHollowPopulation(fresh_population, 0xFF, 0x20);
  population.length = climate.max_population;
  repeat(climate.max_population,
         population.people[iterator].page.plength = climate.max_program_length);
  fresh_population.length = climate.max_population;
  repeat(climate.max_population,
         fresh_population.people[iterator].page.plength =
             climate.max_program_length);
  uint32_t population_health[0xFF];
  uint64_t champion_iteration_sequence[0xFF];
  DEBUGPRINT(("%lX %lX climate.random_seed, %X climate.temperature, "
              "%X climate.radioactivity\n",
              climate.random_seed[0], climate.random_seed[1],
              climate.temperature, climate.radioactivity));

  neo_population_establish(climate, recipe, population);
  //// generate the population based on the random seed
  //// evolutionary_island_establish
  fresh_population = evolutionary_island_establish(
      climate, recipe, training_sequence, population, fresh_population,
      population_health, champion_iteration_sequence);
  //// add to best champion climate list
  //// check if we have a passing champion
  uint8_t passing_champion =
      population_health[champion_iteration_sequence[0]] >= 0xFFF0;
  //// if yes, then break
  mu_assert("", passing_champion);
  text_phrase_print(fresh_population.people[champion_iteration_sequence[0]]);
  text_phrase_print(population.people[champion_iteration_sequence[0]]);
  return 0;
}


const char *quiz_climate_found() {
  mu_run_quiz(quiz_rapid_evolutionary_island_climate_found);
  mu_run_quiz(quiz_rapid_evolutionary_island_climate_found_1);
  //mu_assert("#TODO  mu_run_quiz(quiz_rapid_evolutionary_island_climate_found_2);"
  //    , 1== 0);
  //mu_run_quiz(quiz_rapid_evolutionary_island_climate_found_2);
  return 0;
}

const char *quiz_phrase_letter_extract() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.zika");
  word_t produce_letter = phrase_letter_extract(input, accusative_case_GRAMMAR);
  mu_assert("", produce_letter == 'a');
  return 0;
}
const char *quiz_phrase_letter_extract1() {
  NewTextPhrase(input, 1, "zi.prih.a.prih.zika");
  word_t produce_letter = phrase_letter_extract(input, accusative_case_GRAMMAR);
  mu_assert("", produce_letter == 'a');
  return 0;
}

const char *quiz_extract() {
  mu_run_quiz(quiz_phrase_letter_extract);
  mu_run_quiz(quiz_phrase_letter_extract1);
  return 0;
}
const char *quiz_phrase_word_read_boundary() {
  NewTextPhrase(input, 2, "hyikdoka tyindoyu djancu hyikdoka tyindoyu plustu");
  Page_print(input.page);
  input.begin = 0x10;
  input.length = 3;
  word_t produce = phrase_word_read(input, 0);
  mu_assert("", produce != input.page.lines[1][0]);
  return 0;
}
const char *quiz_phrase_word_read() {
  mu_run_quiz(quiz_phrase_word_read_boundary);
  return 0;
}

const char *quiz_read() {
  mu_run_quiz(quiz_phrase_word_read);
  return 0;
}


const char *quiz_go_incremental_subtract() {
  DEBUGPRINT(("hkinkwinka tlukli\n"));
  NewPagePhrase(program, 0x20);
  NewText(input, "hnucgina ksasdoka li"
  "hnimginaksuh "
  "hyikdoyu hnucgika grettu prah"
  "zrondopwih hnucgika djancu hnimgiyi hkintu");

  NewTextPad(produce, 0x100);
  neo_pyash_to_pyash_interpret(program, input, fluent_WORD, produce);
  mu_assert("#TODO finish making this test", 1 == 0); 
  // the place to add code is interpret.c:2210
  // also have to figure out what the result is supposed to be
  // result should be hnucgina zrondoka li
  DEBUGPRINT(("hkinkwinka tlicli\n"));
  return 0;
}

const char *quiz_repeat() {
  // quiz for the repeat or frak function
  return 0;
}

const char *quiz_hkuttyafmwah_psashtinyi_hkinhtinka_tyiftu() {
  
  return 0;
}

const char *quiz_go() {
  // this is for goto or jump instruction
  // in order for this to work with greatest compatibility with current
  // hardware, best if we have an instruction pointer, likely a 16bit one at
  // least. 
  // I'm thinking that a page really should have around 56-64 lines
  // and most books are up to about 256 pages
  // and each line has 16 uint16 words so
  // so if we are addressing each byte within a book then we'd need up to 
  // 262144 addressing, which would need 32 bit addressing.
  // though if we do addressing by line number, then 16bit addressing will
  // suffice for even a book of 1024 pages, which is  huge for a book.
  // 32bit addressing would then be for inter-book addressing, though would make
  // more sense to use a name of a book or something.
  //
  // Though yeah a regular memory page (4096 bytes) is more of a long paragraph 
  // since it has a mere 16 lines. 
  //
  // anyways so what will it look like? hkin means go so
  // hnimgiyi hkintu means "go to named Name!"
  // And that named name will likely have to be a named paragraph.
  // technically the end paragraph marker could be used as a return, 
  // but then we are implementing a call and return which is a little different. 
  //
  // Also it is good to have some exposed registers so we can work with those
  // like with any assembly langauge, as technically that is what this is...
  // Unless we make it in the style of static single assignment, which is
  // probably a better way to go, in terms of lowering unexpected consequences,
  // thus making it easier to debug.
  //
  // I guess this part should be going into the documentation.
  mu_run_quiz(quiz_hkuttyafmwah_psashtinyi_hkinhtinka_tyiftu);
  mu_run_quiz(quiz_go_incremental_subtract);
  return 0;
}
const char *quiz_is_theIndexfingerZLine_aPartialIndependentClause_paragraph() {
  NewTextPhrase(input, 3, "hnucgina tyutzrondolyatkyitksuh prah");
  mu_assert("", is_theIndexfingerZLine_aPartialIndependentClause(4, input) ==
      lie_WORD);
  return 0;
}
const char *quiz_paragraph() {
  mu_run_quiz(quiz_paragraph_encoding);
  mu_run_quiz(quiz_paragraph_quote_encoding);
  mu_run_quiz(quiz_is_theIndexfingerZLine_aPartialIndependentClause_paragraph);
  return 0;
}

const char *quiz_low_tone_words(){
	NewTextPhrase(input2, 1, "tsi2h");
	mu_assert("", phrase_word_read(input2, 0) == 0xC257);
	return 0;
}

const char *quiz_new_words() {
	mu_run_quiz(quiz_low_tone_words);
	NewTextPhrase(input, 3, "twi7hpyahpyuhkcuhkcu7hsyipsyihsyi7htrahtra7hmwuhmya7hmyah");
	text_phrase_print(input);
	phrase_print(input);
	return 0;
}

const char *all_quizs() {
  // mu_run_quiz(quiz_lwonprom_lettergiant);
  // mu_run_quiz(quiz_climate_found);
  // mu_run_quiz(quiz_lwonprom_lettergiant_letter_conditional);
  //mu_run_quiz(quiz_go);
  mu_run_quiz(quiz_addenda);
  mu_run_quiz(quiz_is);
  mu_run_quiz(quiz_encoding);
  mu_run_quiz(quiz_decoding);
  mu_run_quiz(quiz_text_phrase_print);
  mu_run_quiz(quiz_write);
  mu_run_quiz(quiz_found);
  mu_run_quiz(quiz_read);
  mu_run_quiz(quiz_phrase);
  mu_run_quiz(quiz_grammar);
  mu_run_quiz(quiz_paragraph);
  mu_run_quiz(quiz_quote);
  mu_run_quiz(quiz_variable);
  mu_run_quiz(quiz_interpret);
  mu_run_quiz(quiz_word);
  mu_run_quiz(quiz_number);
  mu_run_quiz(quiz_cousin_found);
  mu_run_quiz(quiz_extract);
  mu_run_quiz(quiz_math);
  mu_run_quiz(quiz_base_command_interpret);
  mu_run_quiz(quiz_multisentence);
  mu_run_quiz(quiz_knowledge);
  mu_run_quiz(quiz_compare);
  mu_run_quiz(quiz_translate);
  mu_run_quiz(quiz_ksim);
  mu_run_quiz(quiz_conditional);
  mu_run_quiz(quiz_nwonhtin);
  mu_run_quiz(quiz_recipe);
  mu_run_quiz(quiz_evolutionary_programmer);
  mu_run_quiz(quiz_climate_found);
//  mu_run_quiz(quiz_lwonprom_lettergiant);
  mu_run_quiz(quiz_type_numbers);
  mu_run_quiz(quiz_text);
  mu_run_quiz(quiz_repeat);
//  mu_run_quiz(quiz_pyash_to_json);
#ifndef EMSCRIPTEN
  mu_run_quiz(quiz_opencl);
#endif
  // mu_run_quiz(quiz_emscripten);
  TODO(mu_assert("#TODO quiz_lwonprom_A005846", 1 == 0));
  mu_run_quiz(quiz_new_words);
  // mu_run_quiz(quiz_lwonprom_A005846);
  return 0;
}

int cardinal_quiz() {

  const char *result = all_quizs();
  // if (result != 0) {
  //  printf("er in %s quiz!\n", result);
  //} else {
  //  printf("ALL QUIZES PASSED\n");
  //}
  printf("quizes run: %d\n", quizs_run);
  printf("1..%d\n", quizs_run);
  return result != 0;
}
