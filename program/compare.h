/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#ifndef COMPARE_H
#define COMPARE_H
#include "sort.h"
#include "tlep.h"
#include "pyashWords.h"
#include "pyash.h"
#include "encoding.h"

#ifndef OPENCL
word_t tablet_letters_compare(const struct Page example, constant char *letters);
#define page_letters_compare(example, letters)                                 \
  tablet_letters_compare(example, letters)

word_t phrase_letters_compare(const struct Phrase phrase, constant char *letters);
#endif

word_t phrase_to_phrase_compare(const struct Phrase phrase, 
    const struct Phrase compare);

uint16_t /*boolean*/ number_sequence_compare(const size_t example_long,
                                             const uint16_t example[],
                                             const size_t comparison_long,
                                             const uint16_t comparison[]);

uint16_t /* truth or lie WORD */
htin_subset_comparison(const uint16_t example_long, const line_t example[],
                       const uint16_t comparison_long,
                       const line_t comparison[]);
#define neo_htin_subset_comparison(example, comparison)                        \
  htin_subset_comparison(example.plength, example.lines, comparison.plength,   \
                         comparison.lines)
word_t /* boolean */ text_compare(struct Text example, struct Text comparison);


uint16_t /*boolean*/ line_compare(const line_t line,
    const line_t comparison);
word_t /*boolean*/ tablet_compare(const uint16_t tablet_long, const line_t tablet[],
    const uint16_t comparison_long, const line_t comparison[]);

#define neo_tablet_compare(tablet, comparison) \
  tablet_compare(tablet.plength, tablet.lines, comparison.plength, comparison.lines)

#define is_thePhrase_aQuote fyakna_kyitka_ri
word_t fyakna_kyitka_ri(struct Phrase);


word_t fyakna_hwafhtinka_ri(const struct Phrase);
#define is_thePhrase_aPerfectIndependentClause fyakna_hwafhtinka_ri

word_t fyakna_tyafkyitka_ri(const struct Phrase);
#define is_thePhrase_aParagraphQuote fyakna_tyafkyitka_ri


#endif
