/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
/*
 Pyash is a human computer programming langauge based on linguistic universals.
 Copyright (C) 2020  Andrii Logan Zvorygin
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
#include "analyst.h"
//#include <string.h>
#define derive_first_word_exit                                                 \
  *produce_word_long = (uint8_t)0;                                             \
  return;


struct Text /*produce*/ derive_first_word(const struct Text input, 
    struct Text produce) {
  //DEBUGPRINT(("%s input, \n", input.letters));
  const max_length = produce.max_length;
  old_derive_first_word(input.length, input.letters, &produce.max_length,
      produce.letters);
  produce.length = produce.max_length;
  produce.max_length = max_length;
  return produce;
}
void old_derive_first_word(const uint8_t ACC_GEN_magnitude,
                              const char *ACC_independentClause,
                              uint8_t *produce_word_long, char *produce_word) {
  uint8_t start = 0;
  guarantee(ACC_independentClause != NULL);
  if (ACC_GEN_magnitude < 2) {
    derive_first_word_exit;
  }
  guarantee(ACC_GEN_magnitude > 1);
  guarantee(produce_word != NULL);
  guarantee(*produce_word_long >= WORD_LONG);
  /* algorithm:
      if glyph zero ESS vowel
      then if glyph two not ESS consonant
          then answer ACC DEP wrong ACC glyph LOC two
          else restart ABL glyph two
      if glyph zero ESS consonant
      then
          if glyph one ESS consonant CNJ glyph two ESS vowel
              CNJ glyph three ESS consonant
          then copy ACC independentClause ABL glyph zero ALLA glyph
                  four DAT word
              CNJ copy ACC number four DAT size
              answer
          else if glyph one ESS vowel
          then copy ACC independentClause ABL glyph zero ALLA glyph two
              DAT word CNJ
              copy ACC number two DAT size
  */
  guarantee(vowel_Q(ACC_independentClause[start + 0]) == TRUE ||
         consonant_Q(ACC_independentClause[start + 0]) == TRUE);
  if (vowel_Q(ACC_independentClause[start]) == TRUE) {
    if (consonant_Q(ACC_independentClause[start + 1]) == FALSE ||
        consonant_Q(ACC_independentClause[start + 2]) == FALSE) {
      derive_first_word_exit;
    }
    guarantee(consonant_Q(ACC_independentClause[start + 1]) == TRUE);
    guarantee(consonant_Q(ACC_independentClause[start + 2]) == TRUE);
    start = 2;
  }
  if (consonant_Q(ACC_independentClause[start]) == TRUE) {
    if (consonant_Q(ACC_independentClause[start + 1]) == TRUE) {
      if (vowel_Q(ACC_independentClause[start + 2]) == FALSE) {
        derive_first_word_exit;
      }
      guarantee(vowel_Q(ACC_independentClause[start + 2]) == TRUE);
      if (tone_Q(ACC_independentClause[start + 3]) == TRUE) {
        if (consonant_Q(ACC_independentClause[start + 4]) == FALSE) {
          derive_first_word_exit;
        }
        guarantee(consonant_Q(ACC_independentClause[start + 4]) == TRUE);
        text_copy((uint8_t)(start + 5), ACC_independentClause + start,
            (size_t) produce_word_long,
                  produce_word);
        *produce_word_long = (uint8_t)5;
      } else {
        if (consonant_Q(ACC_independentClause[start + 3]) == FALSE) {
          derive_first_word_exit;
        }
        guarantee(consonant_Q(ACC_independentClause[start + 3]) == TRUE);
        text_copy((uint8_t)(start + 4), ACC_independentClause + start,
            (size_t) produce_word_long,
                  produce_word);
        *produce_word_long = (uint8_t)4;
      }
    } else if (vowel_Q(ACC_independentClause[start + 1]) == TRUE) {
      if (tone_Q(ACC_independentClause[start + 2]) == TRUE) {
        text_copy((uint8_t)(start + 3), ACC_independentClause + start,
            (size_t) produce_word_long,
                  produce_word);
        *produce_word_long = (uint8_t)3;
      } else {
        text_copy((uint8_t)(start + 2), ACC_independentClause + start,
            (size_t) produce_word_long,
                  produce_word);
        *produce_word_long = (uint8_t)2;
      }
    }
  }
  if (ACC_GEN_magnitude < *produce_word_long) {
    *produce_word_long = 0;
  }
}
extern void detect_ACC_quote_magnitude(const uint8_t text_magnitude,
                                              const char *text,
                                              uint8_t *quote_magnitude,
                                              uint8_t *quote_indexFinger) {
  uint8_t class_magnitude = 0;
  uint8_t text_indexFinger = 0;
  uint8_t class_indexFinger = 0;
  uint8_t found = FALSE;
  guarantee(text != NULL);
  guarantee(text_magnitude > 0);
  guarantee(quote_indexFinger != 0);
  guarantee(quote_magnitude != NULL);
  /* algorithm:
      detect silence glyph surrounding quote class word
      search for same word to conclude quote
      answer quote_indexFinger and quote_magnitude*/
  // assure silence glyph at zero indexFinger
  guarantee(text[0] == SILENCE_GLYPH);
  ++class_magnitude;
  // detect size of class word
  for (text_indexFinger = 1; text_indexFinger < text_magnitude;
       ++text_indexFinger) {
    ++class_magnitude;
    if (text[text_indexFinger] == SILENCE_GLYPH) {
      break;
    }
  }
  // printf("class_magnitude %X\n", (uint)class_magnitude);
  *quote_indexFinger = class_magnitude;
  // detect next class word COM quote word
  for (text_indexFinger = class_magnitude; text_indexFinger < text_magnitude;
       ++text_indexFinger) {
    if (text_indexFinger + class_magnitude > text_magnitude) {
      *quote_magnitude = 0;
      break;
    }
    for (class_indexFinger = 0; class_indexFinger <= class_magnitude;
         ++class_indexFinger) {
      if (class_indexFinger == class_magnitude) {
        // found
        *quote_magnitude = (uint8_t)(text_indexFinger - class_magnitude);
        found = TRUE;
      }
      if (text[text_indexFinger + class_indexFinger] !=
          text[class_indexFinger]) {
        break;
      }
    }
    if (found == TRUE) {
      break;
    }
  }
}
