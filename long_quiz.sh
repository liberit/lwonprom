#!/bin/bash
bash compile.sh
valgrind -q binary/lwonprom || exit 1
valgrind -q binary/kwim || exit 1
complexity --thresh=0 --scores --histogram program/*c || exit 1
valgrind -q binary/tfik || exit 1
./buildDocker.sh
docker run -ti lwonprom
