#!/bin/bash
devBranch=0
if ! ./long_quiz.sh 
then
 devBranch=1;
fi
if [ $devBranch == 0 ]
then
  git checkout master
  git add ./*
  git commit
  git push origin master
else
  git checkout dev
  git add ./*
  git commit
  git push origin dev
fi

